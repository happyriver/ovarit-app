(ns build
  (:require [clojure.tools.build.api :as b]))

(def lib :ovarit-app)
(def version "0.1.0")
(def class-dir "target/classes")
(def basis (b/create-basis {:project "deps.edn"
                            :aliases [:gql :gql-release]}))
(def uber-file (format "target/%s-%s-standalone.jar" (name lib) version))

(defn clean [_]
  (b/delete {:path "target"}))

(defn uber [_]
  (clean nil)
  (b/copy-dir {:src-dirs ["src/gql" "resources"]
               :target-dir class-dir})
  (b/compile-clj {:basis basis
                  :src-dirs ["src/gql"]
                  :class-dir class-dir})
  (b/uber {:class-dir class-dir
           :uber-file uber-file
           :basis basis
           :main 'ovarit.app.gql.system}))

;; `clojure -P -X:gql:build` doesn't download all the dependencies,
;; but calling this stub function does.
(defn prepare [_] nil)
