-- sql/votes.sql -- Vote-related SQL queries for ovarit-app
-- Copyright (C) 2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-votes :? :*
-- :doc Select votes with pagination
with
  voter as (
    select uid
      from "user"
     where name = :name
    ),
  all_votes as (
    select spv.pid,
           null as cid,
           positive,
           datetime
      from voter, sub_post_vote spv
     where 'POST' in (:v*:types)
           :snip:and-datetime-snip
       and spv.uid = voter.uid

             union

    select null as pid,
           spcv.cid,
           positive,
           datetime
      from voter, sub_post_comment_vote spcv
     where 'COMMENT' in (:v*:types)
           :snip:and-datetime-snip
       and spcv.uid = voter.uid
  )
select pid, cid, positive, datetime
  from all_votes
 order by datetime
--~ (if (:forward? params) "desc" "asc")
 limit :limit;

-- :name _upsert-vote :<
-- :doc Update or insert a post or comment vote, and update scores.
with
  existing_vote as (
    select (select positive
              from :i:vote-table-name
             where :i:primary-key = :content-id
               and uid = :uid) as positive,
           :content-id as content_id
  ),
  new_or_updated_vote as (
    insert into :i:vote-table-name (:i:primary-key, uid, positive, datetime)
    values (:content-id, :uid, :positive, :timestamp)
           on conflict (:i:primary-key, uid)
           do update
           set positive = :positive,
               datetime = :timestamp
 returning (case when :positive = :vote/UP then 1 else -1 end) as vote_value
  ),
  deltas as (
    select (case when ev.positive is null then nv.vote_value
                 when ev.positive = :positive then 0
                 else 2 * nv.vote_value end) as score_delta,
           (case when (ev.positive is null or ev.positive = :vote/DOWN) and :positive = :vote/UP then 1
                 when ev.positive = :vote/DOWN and :positive = :vote/UP then -1
                 else 0 end) as upvote_delta,
           (case when (ev.positive is null or ev.positive = :vote/UP) and :positive = :vote/DOWN then 1
                 when ev.positive = :vote/DOWN and :positive = :vote/UP then -1
                 else 0 end) as downvote_delta
      from existing_vote ev, new_or_updated_vote nv
  ),
  :snip:update-scores-snip
select * from update_users;

-- :name _delete-vote :<
-- :doc Remove a post or comment vote, and update scores.
with
  existing_vote as (
    select (select positive
              from :i:vote-table-name
             where :i:primary-key = :content-id
               and uid = :uid) as positive,
           :content-id as content_id
  ),
  remove_vote as (
    delete from :i:vote-table-name
     where :i:primary-key = :content-id
       and uid = :uid
  ),
  deltas as (
    select case when positive is null then 0
                when positive = :vote/UP then -1
                else 1 end as score_delta,
           case when positive = :vote/UP then -1 else 0 end as upvote_delta,
           case when positive = :vote/DOWN then -1 else 0 end as downvote_delta
      from existing_vote
  ),
  :snip:update-scores-snip
select * from update_users;

-- :name _admin-delete-vote :<
-- :doc Remove a vote, and update scores.
with
  existing_vote as (
    select xid, :i:primary-key as content_id, positive
      from :i:vote-table-name
     where uid = :uid
     limit 1
       for update skip locked
    ),
  remove_vote as (
    delete from :i:vote-table-name
     using existing_vote
     where --~ (str (:vote-table-name params) ".xid = existing_vote.xid")
  ),
  deltas as (
    select case when positive is null then 0
                when positive = :vote/UP then -1
                else 1 end as score_delta,
           case when positive = :vote/UP then -1 else 0 end as upvote_delta,
           case when positive = :vote/DOWN then -1 else 0 end as downvote_delta
      from existing_vote
  ),
  :snip:update-scores-snip
select xid from existing_vote;

-- :name count-votes :? :1
-- :doc Count rows in the votes tables for a user.
select (
  select count(xid)
    from sub_post_vote
   where uid = :uid
) + (
  select count(xid)
    from sub_post_comment_vote
   where uid = :uid
) as votes;

-- :name select-votes-given-by-name :? :1
-- :doc Get a user's uid and vote counts from their name.
select uid, upvotes_given, downvotes_given
  from "user"
 where name = :name;

-- :name sitewide-downvote-count :? :1
-- :doc Get the number of recent downvotes by a user.
with
  pv as (
    select count(xid) as post_count
      from sub_post_vote
     where datetime > :now::timestamp without time zone - (:days * interval '1 day')
       and positive = :vote/DOWN
       and uid = :uid
  ),
  cv as (
    select count(xid) as comment_count
      from sub_post_comment_vote
     where datetime > :now::timestamp without time zone - (:days * interval '1 day')
       and positive = :vote/DOWN
       and uid = :uid
  )
select (select comment_count from cv) + (select post_count from pv) as count;

-- :name per-user-vote-history :? :*
-- :doc Get the vote values of recent votes by one user on another's content.
(select positive, datetime
   from sub_post_comment_vote spcv
        left join sub_post_comment spc
            on spc.cid = spcv.cid
  where spcv.uid = :voter
    and spc.uid = :target
    and datetime > :now::timestamp without time zone - (:days * interval '1 day')
)
  union all
(select positive, datetime
   from sub_post_vote spv
        left join sub_post sp
            on sp.pid = spv.pid
  where spv.uid = :voter
    and sp.uid = :target
    and datetime > :now::timestamp without time zone - (:days * interval '1 day')
)
  order by datetime desc
  limit :num;

-- :name thread-downvote-percent :? :1
-- :doc Calculate the percentage of comments on a thread a user has downvoted.
with
  comment as (
    select count(cid) as count
      from sub_post_comment
     where pid = :pid
  ),
  downvote as (
    select count(spcv.xid) as count
      from sub_post_comment spc
           left join sub_post_comment_vote spcv
               on spcv.cid = spc.cid
     where spc.pid = :pid
       and spcv.uid = :uid
       and positive = :vote/DOWN
    )
select case when comment.count < :num then 0
            else downvote.count * 100 / comment.count end as percent
  from comment, downvote;

-- :snip update-content-scores-snip
-- :doc Update post/comment and user scores after a vote change.
-- :doc The `deltas` and `existing_vote` tables referenced below
-- :doc should be defined in the containing query.
  update_content as (
    update :i:content-table-name
       set score = score + score_delta,
           upvotes = upvotes + upvote_delta,
           downvotes = downvotes + downvote_delta
      from deltas, existing_vote
     where /*~
             (if (= (:content-table-name params) "sub_post")
               "sub_post.pid = existing_vote.content_id"
               "sub_post_comment.cid = existing_vote.content_id")
             ~*/
       returning uid, score, upvotes, downvotes
  ),
  -- If self-voting is turned on, voter and author might be the same,
  -- so they have to be updated in the same expression for predictable
  -- results.
  update_users as (
    update "user" u
       set score = u.score + (case when u.uid = update_content.uid then score_delta else 0 end),
           given = given + (case when u.uid = :uid then score_delta else 0 end),
           upvotes_given = upvotes_given + (case when u.uid = :uid then upvote_delta else 0 end),
           downvotes_given = downvotes_given + (case when u.uid = :uid then downvote_delta else 0 end)
      from deltas, update_content
     where u.uid in (update_content.uid, :uid)
 returning u.uid, u.score, given, upvotes_given, downvotes_given,
           update_content.score as content_score,
           update_content.upvotes,
           update_content.downvotes
  )

-- :snip forward-datetime-snip
and datetime < :cursor

-- :snip reverse-datetime-snip
and datetime > :cursor
