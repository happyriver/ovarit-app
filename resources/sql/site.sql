-- sql/site.sql -- Site and admin SQL queries for ovarit-app
-- Copyright (C) 2021-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-stats :? :1
-- :doc Get counts of site objects since a date
select
  (select count(*)
     from public.user
    :snip:user-where-timespan-snip)
    as users,

  (select count(*)
     from sub
    :snip:sub-where-timespan-snip)
    as subs,

  (select count(*)
     from sub_post :snip:post-where-timespan-snip)
    as posts,

  (select count(*)
     from sub_post_comment
    :snip:comment-where-timespan-snip)
    as comments,

  :snip:users-who-snip

  :snip:upvotes-snip
    as upvotes,

  :snip:downvotes-snip
    as downvotes;

-- :snip upvotes-snip
   ((select count(*)
      from sub_post_vote :snip:upvote-where-timespan-snip)
      + (select count(*)
           from sub_post_comment_vote :snip:upvote-where-timespan-snip))

-- :snip upvotes-all-time-snip
  ((select sum(upvotes) from sub_post)
   + (select sum(upvotes) from sub_post_comment))

-- :snip downvotes-snip
  ((select count(*)
     from sub_post_vote :snip:downvote-where-timespan-snip)
     +
     (select count(*)
        from sub_post_comment_vote :snip:downvote-where-timespan-snip))

-- :snip downvotes-all-time-snip
     ((select sum(downvotes) from sub_post)
     + (select sum(downvotes) from sub_post_comment))

-- :snip users-who-snip
  (select count(distinct(sub_post.uid))
     from sub_post
     :snip:post-where-timespan-snip)
    as users_who_posted,

  (select count(distinct(sub_post_comment.uid))
     from sub_post_comment
    :snip:comment-where-timespan-snip)
    as users_who_commented,

  (select count(distinct (uid))
     from (select uid
             from sub_post_comment_vote
             :snip:comment-vote-where-timespan-snip
            union
           select uid
             from sub_post_vote
             :snip:comment-vote-where-timespan-snip) as voters) as users_who_voted,

-- :name select-banned-username-strings :? :*
-- :doc Select banned strings in usernames with the users who are affected.
select value, u.name
  from site_metadata
       left join public.user as u
           on lower(u.name) like '%' || lower(value) || '%'
          and u.status in (0, 1)
  where key = 'banned_username_string'
  order by u.name;

-- :name insert-banned-username-string :? :*
-- :doc Insert a banned username string and return names of affected users.
with
  inserted as
    (
      insert into site_metadata (key, value)
      select key, value
        from (values ('banned_username_string', :value))
               as v (key, value)
       where not exists (select key, value from site_metadata
                          where key = 'banned_username_string'
                            and value = :value)
      returning *
    ),
  site_log_entry as
    (
      insert into site_log (action, uid, "desc", link, time)
      select action, uid, "desc", '', time
        from (values (:site-log/BAN_USERNAME_STRING,
                      :uid,
                      :value,
                      :timestamp :: timestamp without time zone))
               as v (action, uid, "desc", time)
       where (select count(*) from inserted) > 0
      )
select u.name
  from public.user as u
 where u.name like '%' || :value || '%'
   and u.status in (0, 1)
 order by u.name;

-- :name delete-banned-username-string :! :n
-- :doc Remove a string from the list of those banned in usernames.
with
  deleted as (
    delete from site_metadata
     where key = 'banned_username_string'
       and value = :value
    returning *
  )
insert into site_log (action, uid, "desc", link, time)
select action, uid, "desc", '', time
  from (values (:site-log/UNBAN_USERNAME_STRING,
                :uid,
                :value,
                :timestamp :: timestamp without time zone))
         as v (action, uid, "desc", time)
 where (select count(*) from deleted) > 0;

-- :name select-invite-code-settings :? :1
-- :doc Get the sitewide invite code settings.
select (select (value = '1')
          from site_metadata
         where key = 'site.require_invite_code') as required,
       (select (value = '1')
          from site_metadata
         where key = 'site.invitations_visible_to_users') as visible,
       (select value::integer
          from site_metadata
         where key = 'site.invite_level') as minimum_level,
       (select value::integer
          from site_metadata
         where key = 'site.invite_max') as per_user;

-- :name update-invite-code-settings :! :n
-- :doc Update the sitewide invite code settings
with
  update_required as (
    update site_metadata
       set value = (case when :required then '1' else '0' end)
     where key = 'site.require_invite_code'
       and value != (case when :required then '1' else '0' end)
    returning *
  ),
  update_visible as (
    update site_metadata
       set value = (case when :visible then '1' else '0' end)
     where key = 'site.invitations_visible_to_users'
       and value != (case when :visible then '1' else '0' end)
    returning *
  ),
  update_minimum_level as (
    update site_metadata
       set value = :minimum-level::text
     where key = 'site.invite_level'
       and value != :minimum-level::text
    returning *
  ),
  update_per_user as (
    update site_metadata
       set value = :per-user::text
     where key = 'site.invite_max'
       and value != :per-user::text
    returning *
    )
insert into site_log (action, uid, "desc", link, time)
select :site-log/ADMIN_CONFIG_CHANGE,
       :uid,
       concat(key, '/', value), '',
       :timestamp
  from (
    select key, value from update_required
     union
    select key, value from update_visible
     union
    select key, value from update_minimum_level
     union
    select key, value from update_per_user
  ) as values;

-- :name insert-invite-code :? :1
-- :doc insert a new invite code, returning the id of the new code.
insert into invite_code (uid, code, created, expires, uses, max_uses)
values (:uid, :code, :created, :expires, :uses, :max-uses)
       returning *;

-- :name select-invite-codes :? :*
-- :doc select invite codes with pagination and user id lists
select id, invite_code.uid, code, created, expires, uses, max_uses,
       array_agg(used_by.uid) as uids
  from invite_code
  left join lateral (
    select uid
      from user_metadata
     where key = 'invitecode'
           and value=invite_code.code
  ) as used_by on true
  --~ (when (or (:before params) (:search params)) "where")
  --~ (when (:before params) "created < :before")
  --~ (when (and (:before params) (:search params)) "and")
  --~ (when (:search params) "code = :search")
 group by id
 order by created desc
 limit :limit;

-- :name update-invite-codes-expire :! :n
-- :doc change the expiration date of invite codes
update invite_code
   set expires = :expires
 where id in (:v*:ids);

-- :name pick-a-user :? :1
-- :doc Get a uid of a single user
select uid, resets
  from "user"
 where status = 0
 limit 1;

-- :name select-sitelogs :? :*
-- :doc Get most recent sitelog entries (test-only)
select action, "desc", link, uid, target_uid, time
  from site_log
 order by time desc;
