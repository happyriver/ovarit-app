-- sql/visitor-counter.sql -- Visitor counting SQL queries for ovarit-app
-- Copyright (C) 2021-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name insert-visit :! :n
-- :doc Insert new user visit
insert into visit (datetime, ipaddr)
values (:timestamp, :ipaddr);

-- :name update-daily-uniques :? :1
-- :doc Batch update daily unique visitor table
with
  new_visitors as
    (
      delete from visit
       where id = any (
         select id
           from visit
          order by datetime
     for update skip locked
          limit :batchsize
       )
      returning cast(datetime as date) as date,
                hll_hash_text(ipaddr::text) as ip_hashes
    ),
  new_visitor_groups as
    (
      select date, hll_add_agg(ip_hashes) as ip_hashes
        from new_visitors
       group by date
    ),
  ins as
    (
      insert into daily_uniques
      select date, ip_hashes
        from new_visitor_groups
          on conflict (date)
          do update set ip_hashes = hll_union(daily_uniques.ip_hashes, excluded.ip_hashes)
                  where daily_uniques.date = excluded.date
      returning date
    )
select (select count(*) from new_visitors) as processed,
       (select count(*) from visit) - (select count(*) from new_visitors) as remaining,
       (select date from ins order by date desc limit 1) as latest;

-- :name select-visitor-counts :? :*
-- :doc Get counts of visitors over spans of days
select (extract(epoch from (date::timestamp - :startdate))::integer
         / (86400 * :days)::integer) * :days as day,
       round(# hll_union_agg(ip_hashes)) as count
  from daily_uniques
 where date >= :startdate
   and date < :enddate
 group by day
 order by day;
