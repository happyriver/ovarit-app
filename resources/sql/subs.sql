-- sql/subs.sql -- Sub-related SQL queries for ovarit-app
-- Copyright (C) 2021-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-subs :? :*
-- :doc Get multiple subs by name or by sid
:snip:start-cte-snip
:snip:banned-cte-snip
:snip:meta-cte-snip
:snip:mod-stats-cte-snip
:snip:moderators-cte-snip
:snip:post-type-config-cte-snip
:snip:user-flair-choices-cte-snip
:snip:user-flair-cte-snip
:snip:end-cte-snip
select
  :snip:banned-field-snip
  :snip:info-fields-snip
  :snip:meta-fields-snip
  :snip:mod-stats-fields-snip
  :snip:moderators-field-snip
  :snip:post-type-config-field-snip
  :snip:user-flair-choices-field-snip
  :snip:user-flair-field-snip
  sub.sid,
  sub.name
  from sub
  :snip:banned-join-snip
  :snip:meta-joins-snip
  :snip:mod-stats-joins-snip
  :snip:moderators-join-snip
  :snip:post-type-config-join-snip
  :snip:user-flair-choices-join-snip
  :snip:user-flair-join-snip
:snip:where-snip
:snip:order-by-snip
:snip:limit-snip
  ;

-- :name default-subs :*
-- :doc Get default subs
select name, sid
  from sub
 where sid in (select value
                 from site_metadata
                where key = 'default')
 order by lower(name)

-- :name select-rules :? :*
-- :doc Get rules for a sub
select rid as id, text
  from sub_rule
 where sid = :sid
 order by "order";

-- :name insert-rule :? :1
-- :doc Add a new sub rule
with
  largest as
    (select sid, max("order") as max_order, count("order") as num
       from sub_rule
      where sid = :sid
      group by sid)
insert into sub_rule (sid, text, "order")
select v.sid,
       v.text,
       case when max_order is null then 1 else max_order + 1 end
  from (values (:sid, :text)) as v (sid, text)
       left join largest
           on largest.sid = v.sid
 where exists (select sid from sub where sid = :sid)
       and (largest.num is null or largest.num < 100)
returning rid as id, text;

-- :name update-rule-order :? :*
-- :doc Change the order of rules in a sub.
with
  updated as
    (update sub_rule
        set "order" = val.new_order
            from (values :tuple*:ordering) as val(new_order, id)
      where val.id = sub_rule.rid
        and sub_rule.sid = :sid
  returning rid, text, "order")
select rid as id, text
  from updated
 order by "order";

-- :name delete-rule :! :n
-- :doc Delete a rule from a sub.
delete from sub_rule
 where rid = :id
   and sid = :sid;

-- :name select-post-flairs :? :*
-- :doc Get post flairs for a sub
select xid as id,
       text,
       mods_only,
       array(
         select ptype
           from disallow_flair_post_type
          where flair_id = xid
       ) as disallowed_ptypes
  from sub_flair
 where sid = :sid
 order by "order";

-- :name insert-post-flair :? :1
-- :doc Add a new sub post flair
with
  largest as
    (select sid, max("order") as max_order, count("order") as num
       from sub_flair
      where sid = :sid
      group by sid)
insert into sub_flair (sid, text, "order", mods_only)
select v.sid,
       v.text,
       case when max_order is null then 1 else max_order + 1 end,
       false
  from (values (:sid, :text)) as v (sid, text)
       left join largest
           on largest.sid = v.sid
 where exists (select sid from sub where sid = :sid)
       and (largest.num is null or largest.num < 100)
returning xid as id, text, false as mods_only;

-- :name update-post-flair-order :? :*
-- :doc Change the order of post flairs in a sub.
with
  updated as
    (update sub_flair
        set "order" = val.new_order
            from (values :tuple*:ordering) as val(new_order, id)
      where val.id = sub_flair.xid
        and sub_flair.sid = :sid
  returning xid, text, mods_only, "order")
select xid as id,
       text,
       mods_only,
       array(
         select ptype
           from disallow_flair_post_type
          where flair_id = xid
       ) as disallowed_ptypes
  from updated
 order by "order";

-- :name _update-post-flair :! :n
-- :doc Update the options for a flair
with
  flair as (
    select xid
      from sub_flair
     where xid = :id
       and sid = :sid
  ),
  allowed_update as (
    delete from disallow_flair_post_type as d
     using flair
     where d.flair_id = flair.xid
       and :snip:allowed-ptypes-snip
  ),
  disallowed_update as (
    insert into disallow_flair_post_type (flair_id, ptype)
    select xid as flair_id, ptype
      from flair,
           (select ptype
              from (values :t*:all-ptypes) as p (ptype)) as p
     where :snip:disallowed-ptypes-snip
           on conflict do nothing
  )
update sub_flair
   set mods_only = :mods-only
  from flair
 where sub_flair.xid = flair.xid;

-- :name delete-post-flair :! :n
-- :doc Delete a post flair from a sub.
delete from sub_flair
 where xid = :id
   and sid = :sid;

-- :name update-post-type-config :! :n
-- :doc Update the config for a post type.
with
    update_log as (
    insert into sub_log (action, "desc", link, sid, uid, target_uid, admin, time)
    select :report-log/EDIT_POST_TYPE_SETTINGS as action,
           :post-type as "desc",
           null as link,
           :sid as sid,
           :uid as uid,
           null as target_uid,
           :is-admin as admin,
           now() as time
    )
update post_type_config
   set mods_only = coalesce(:mods-only, mods_only),
       rules = coalesce(:rules, rules)
    where ptype = :ptype
      and sid = :sid;

-- :name delete-user-flair :! :n
-- :doc Delete a user's flair in a sub.
delete from sub_user_flair
 where uid = :uid
   and sid = :sid;

-- :name insert-or-update-user-flair :! :n
-- :doc Insert or update a user's flair in a sub.
with
  remove_existing as
    (
      delete from sub_user_flair
       where uid = :uid
         and sid = :sid
    )
insert into sub_user_flair (uid, sid, flair, flair_choice_id)
    values (:uid, :sid, :user-flair, :user-flair-id);

-- :name insert-user-flair-choice-returning-choices :!<
-- :doc Insert a new user flair choice, and return the list of them.
with
  inserted as
    (insert into sub_user_flair_choice (sid, flair)
     select sid, flair
       from (values (:sid, :flair)) as to_be_inserted (sid, flair)
      where flair not in (select flair
                            from sub_user_flair_choice
                           where sid = :sid)
     returning *)
select flair
  from sub_user_flair_choice
 where sid = :sid
 union
select flair from inserted;

-- :name select-sublogs :? :*
-- :doc Get most recent sublog entries (test-only)
select action, "desc", sid, link, uid, target_uid, admin, time
  from sub_log
 where sid = :sid
 order by time desc;

-- :snip pag-after-snip
  where lower(sub.name) > :after

-- :snip order-by-name-snip
 order by lower(sub.name)

-- :snip pag-limit-snip
 limit :limit

-- :snip in-sids-snip
 where sub.sid in (:v*:sids)

-- :snip in-names-snip
 where lower(sub.name) in (:v*:names)

-- :snip info-fields-snip
       sub.nsfw,
       sub.sidebar,
       sub.status,
       sub.title,
       sub.creation,
       sub.subscribers as subscriber_count,
       sub.posts as post_count,

-- :snip meta-cte-snip
  metadata as (
  select sub_metadata.sid,
         json_object_agg(sub_metadata.key, sub_metadata.value) as meta
    from sub_metadata
   where sub_metadata.key not in ('sticky', 'xmod2')
   group by sub_metadata.sid),
  agg_metadata as (
    select sub.sid,
           coalesce(json_object_agg(agg.key, agg.value)
                    filter (where agg.key is not null)) as agg_meta
      from sub
           left join lateral (
             select sub_metadata.key, json_agg(sub_metadata.value) as value
               from sub_metadata
              where sub_metadata.sid = sub.sid
                and sub_metadata.key in ('sticky', 'xmod2')
              group by sub_metadata.key
           ) as agg on true
     group by sub.sid),

-- :snip meta-fields-snip
  metadata.meta,
  agg_metadata.agg_meta,

-- :snip meta-joins-snip
       left join metadata
           on metadata.sid = sub.sid
       left join agg_metadata
           on agg_metadata.sid = sub.sid

-- :snip sub-group-by-snip
 group by sub.sid

-- :snip mod-stats-cte-snip
    -- Count open and closed post and comment reports.
    post_report_counts as (
      select sub_post.sid,
             json_build_object(
               'open', sum(case when sub_post_report.open then 1 else 0 end),
               'closed', sum(case when not sub_post_report.open then 1 else 0 end)
             ) as reports
      from sub_post_report
           inner join sub_post
               on sub_post.pid = sub_post_report.pid
       group by sub_post.sid),
    comment_report_counts as (
      select sub_post.sid,
             json_build_object(
               'open', sum(case when sub_post_comment_report.open then 1 else 0 end),
               'closed', sum(case when not sub_post_comment_report.open then 1 else 0 end)
             ) as reports
        from sub_post_comment_report
             left join sub_post_comment
                 on sub_post_comment.cid = sub_post_comment_report.cid
             left join sub_post
                 on sub_post.pid = sub_post_comment.pid
       group by sub_post.sid
    ),

    -- Count modmail conversations which are not archived and which are
    -- communications with users.
    all_modmail_count as (
      select message_thread.sid, count(message_thread.mtid) as count
        from message_thread
             inner join sub_message_mailbox as mailbox
                 on mailbox.mtid = message_thread.mtid
             inner join message as first_message
                 on first_message.mtid = message_thread.mtid
                 and first_message.first
       where mailbox.mailbox = :message/INBOX
         and first_message.mtype in (:message/USER_TO_MODS,
                                     :message/MOD_TO_USER_AS_MOD,
                                     :message/MOD_TO_USER_AS_USER,
                                     :message/USER_NOTIFICATION)
       group by message_thread.sid),

    -- Count modmail messages from users which haven't yet been
    -- replied to or archived by a mod.
    new_modmail_count as (
      select message_thread.sid,  count(message_thread.mtid) as count
        from message_thread
             inner join sub_message_mailbox as mailbox
                 on mailbox.mtid = message_thread.mtid
             inner join message as first_message
                 on first_message.mtid = message_thread.mtid
                 and first_message.first
       where (message_thread.replies = 0
              and first_message.mtype = :message/USER_TO_MODS
              -- Count user replies to notification messages
              -- with no mod interaction yet.
              or (message_thread.replies > 0
                  and first_message.mtype = :message/USER_NOTIFICATION
                  and not exists ( select *
                                     from message
                                    where message.mtid = message_thread.mtid
                                      and not message.first
                                      and message.mtype != :message/USER_TO_MODS)))
         and mailbox.mailbox = :message/INBOX
       group by message_thread.sid),

    unread_modmail_thread as (
      select message_thread.sid, message_thread.mtid, message_thread.replies,
             first_msg.mtype
         from message_thread
              inner join lateral ( select *
                                     from message
                                    where message.mtid = message_thread.mtid
                                    order by posted desc
                                    limit 1 ) as msg on true
              inner join lateral ( select *
                                     from message
                                    where message.mtid = message_thread.mtid
                                      and message.first
                                    limit 1 ) as first_msg on true
              inner join sub_message_mailbox
                  on sub_message_mailbox.mtid = message_thread.mtid
              inner join user_unread_message as unread
                  on unread.mid = msg.mid
        where unread.uid = :uid
          and mailbox = :message/INBOX
      ),

     -- Count modmail conversations where the newest message is unread.
    unread_modmail_count as (
      select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
       group by unread_modmail_thread.sid),

    -- Count modmail conversations that don't have a reply yet.
    new_unread_modmail_count as (
      select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
       where (unread_modmail_thread.replies = 0
              and unread_modmail_thread.mtype = :message/USER_TO_MODS)
          or (unread_modmail_thread.replies > 0
              and unread_modmail_thread.mtype = :message/USER_NOTIFICATION
              and not exists (
                select * from message
                 where message.mtid = unread_modmail_thread.mtid
                   and not message.first
                   and message.mtype != :message/USER_TO_MODS
              )
          )
       group by unread_modmail_thread.sid
    ),

    -- Count modmail conversations that are either from mods
    -- or were started by users and have a mod reply.
    in_progress_unread_modmail_count as (
      select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
       where (unread_modmail_thread.mtype = :message/USER_TO_MODS
              and unread_modmail_thread.replies > 0)
         or unread_modmail_thread.mtype in (:message/MOD_TO_USER_AS_USER,
                                            :message/MOD_TO_USER_AS_MOD)
         or (unread_modmail_thread.mtype = :message/USER_NOTIFICATION
             and unread_modmail_thread.replies > 0
             and exists (
               select *
                 from message
                where message.mtid = unread_modmail_thread.mtid
                  and not message.first
                  and message.mtype != :message/USER_TO_MODS))
       group by unread_modmail_thread.sid
    ),

    -- Count all conversations with users.
    all_unread_modmail_count as (
      select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
       where unread_modmail_thread.mtype in (:message/USER_TO_MODS,
                                             :message/USER_NOTIFICATION,
                                             :message/MOD_TO_USER_AS_MOD,
                                             :message/MOD_TO_USER_AS_USER)
       group by unread_modmail_thread.sid
    ),

    -- Count all mod discussions.
    discussion_unread_modmail_count as (
            select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
             where unread_modmail_thread.mtype = :message/MOD_DISCUSSION
             group by unread_modmail_thread.sid
    ),

    -- Count all mod notifications.
    notification_unread_modmail_count as (
      select unread_modmail_thread.sid, count(unread_modmail_thread.mtid) as count
        from unread_modmail_thread
       where unread_modmail_thread.mtype = :message/MOD_NOTIFICATION
       group by unread_modmail_thread.sid
      ),

-- :snip mod-stats-fields-snip
  post_report_counts.reports as post_report_counts,
  comment_report_counts.reports as comment_report_counts,
  coalesce(unread_modmail_count.count, 0) as unread_modmail_count,
  coalesce(new_modmail_count.count, 0) as new_modmail_count,
  coalesce(all_modmail_count.count, 0) as all_modmail_count,
  coalesce(new_unread_modmail_count.count, 0) as new_unread_modmail_count,
  coalesce(in_progress_unread_modmail_count.count, 0) as in_progress_unread_modmail_count,
  coalesce(all_unread_modmail_count.count, 0) as all_unread_modmail_count,
  coalesce(discussion_unread_modmail_count.count, 0) as discussion_unread_modmail_count,
  coalesce(notification_unread_modmail_count.count, 0) as notification_unread_modmail_count,

-- :snip mod-stats-joins-snip
       left join unread_modmail_count
           on unread_modmail_count.sid = sub.sid
       left join new_modmail_count
           on new_modmail_count.sid = sub.sid
       left join all_modmail_count
           on all_modmail_count.sid = sub.sid
       left join post_report_counts
           on post_report_counts.sid = sub.sid
       left join comment_report_counts
           on comment_report_counts.sid = sub.sid
       left join new_unread_modmail_count
           on new_unread_modmail_count.sid = sub.sid
       left join in_progress_unread_modmail_count
           on in_progress_unread_modmail_count.sid = sub.sid
       left join all_unread_modmail_count
           on all_unread_modmail_count.sid = sub.sid
       left join discussion_unread_modmail_count
           on discussion_unread_modmail_count.sid = sub.sid
       left join notification_unread_modmail_count
           on notification_unread_modmail_count.sid = sub.sid

-- :snip banned-cte-snip
    banned as (
      select sub.sid,
             count(bans.id) > 0 as banned
        from sub
             left join lateral (
               select id
                 from sub_ban
                where sub_ban.uid = :uid
                  and sub_ban.sid = sub.sid
                  and sub_ban.effective
                  and (sub_ban.expires is null
                       or sub_ban.expires > now())
             ) as bans on true
       group by sub.sid),

-- :snip banned-field-snip
  banned.banned,

-- :snip banned-join-snip
  left join banned
    on banned.sid = sub.sid

-- :snip user-flair-choices-cte-snip
    user_flair_choices as (
      select
        sub.sid,
        json_agg(json_build_object(id, flair)) filter (where id is not null) as flairs
        from sub
             left join sub_user_flair_choice
                 on sub_user_flair_choice.sid = sub.sid
       group by sub.sid),

-- :snip user-flair-choices-field-snip
  user_flair_choices.flairs as user_flair_choices,

-- :snip user-flair-choices-join-snip
  left join user_flair_choices
    on user_flair_choices.sid = sub.sid

-- :snip user-flair-cte-snip
    user_flair as (
      select sub.sid,
             flair
        from sub
             left join lateral (
               select flair
                 from sub_user_flair
                where sub_user_flair.sid = sub.sid
                  and sub_user_flair.uid = :uid
                limit 1
             ) as flairs on true),

-- :snip user-flair-field-snip
  user_flair.flair as user_flair,

-- :snip user-flair-join-snip
  left join user_flair
    on user_flair.sid = sub.sid

-- :snip moderators-cte-snip
  moderators as (
    select sub_mod.sid,
           json_agg(json_build_object('uid', sub_mod.uid,
                                      'power_level', sub_mod.power_level)) as mods
      from sub_mod
     where not sub_mod.invite
     group by sub_mod.sid
    ),

-- :snip moderators-field-snip
  moderators.mods as mods,

-- :snip moderators-join-snip
  left join moderators
    on moderators.sid = sub.sid

-- :snip post-type-config-cte-snip
  type_config as (
    select sid,
           json_agg(json_build_object('ptype', ptype,
                                      'rules', rules,
                                      'mods_only', mods_only)) as post_type_config
      from post_type_config
     group by sid
    ),

-- :snip post-type-config-field-snip
  type_config.post_type_config,

-- :snip post-type-config-join-snip
  left join type_config
    on type_config.sid = sub.sid

-- :snip ptype-list-snip
    ptype in (:v*:ptypes)

-- :snip false-snip
   false
