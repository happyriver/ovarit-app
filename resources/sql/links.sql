-- sql/links.sql -- Link data SQL queries for ovarit-app
-- Copyright (C) 2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name insert-url-metadata :! :n
with
  log as (
    insert into url_metadata_log (url, fetched, error)
    values (:url, now(), null)
    returning *
  )
insert into url_metadata (log_id, key, value)
select log.id as log_id, key, value
  from log, (values :t*:meta) as meta (key, value);

-- :name insert-url-metadata-error :! :n
insert into url_metadata_log (url, fetched, error)
values (:url, now(), :error);

-- :name recent-url-metadata :? :1
-- :doc Find metadata by url, if new enough
select url_metadata_log.id,
       json_object_agg(url_metadata.key, url_metadata.value) as meta
  from url_metadata_log
       left join url_metadata
           on url_metadata_log.id = url_metadata.log_id
 where url = :url
   and fetched > now() - :age * interval '1 second'
   and error is null
 group by url_metadata_log.id
 order by url_metadata_log.id desc
 limit 1;
