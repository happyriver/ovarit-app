-- sql/user.sql -- User SQL queries for ovarit-app
-- Copyright (C) 2020-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-users :? :*
-- :doc Get multiple users by name or uid
:snip:start-cte-snip
:snip:badges-cte-snip
:snip:notification-cte-snip
:snip:score-cte-snip
:snip:end-cte-snip
select
  :snip:badges-field-snip
  :snip:content-blocks-field-snip
  :snip:meta-fields-snip
  :snip:notification-field-snip
  :snip:personal-field-snip
  :snip:score-field-snip
  :snip:subs-moderated-field-snip
  :snip:subscription-field-snip
  :snip:username-history-field-snip
  u.given,
  u.upvotes_given,
  u.downvotes_given,
  u.joindate,
  u.name,
  u.status,
  u.uid
  from "user" as u
  :snip:badges-join-snip
  :snip:notification-join-snip
  :snip:score-join-snip
:snip:where-snip
-- group by u.uid

-- :name uid-by-name :? :1
-- :doc Get a single uid by name
select uid
  from public.user
 where lower(name) = lower(:name)

-- :name insert-user :! :n
-- :doc Insert a new user
insert into public.user (uid, joindate, name, status, upvotes_given, downvotes_given)
values (:uid, :joindate, :name, :status, 0, 0);

-- :name select-customer-update-log :? :*
-- :doc Select unfinished items from the customer update log
select log.id, log.action, log.value, u.uid, u.resets
  from customer_update_log as log
       left join "user" as u on u.uid = log.uid
 where log.completed is null;

-- :name update-customer-update-log :! :n
-- :doc Mark a customer update log entry as completed
update customer_update_log
   set completed = :completed,
       success = :success
 where id = :id;

-- :name _update-subscription-status :<!
-- :doc Replace any existing sub_subscriber records with a new one.
with
  diff as
    (
      select :sid as sid,
             count(xid) as total
        from (select xid
                from sub_subscriber
               where status = :subscription-status/SUBSCRIBED
                 and uid = :uid
                 and sid = :sid) as existing
    ),
  update_subscriber_count as
    (
      update sub s
         set subscribers = subscribers - total + :adjust
        from diff d
       where d.sid = s.sid
    ),
  remove_existing as
    (
      delete from sub_subscriber
       where uid = :uid
         and sid = :sid
    ),
  new_subscription as
    (
      insert into sub_subscriber (sid, uid, status, time)
      values (:sid, :uid, :status, now())
    )
select sub.sid,
       sub.name,
       :status as status
  from sub
 where sub.sid = :sid;

-- :name _delete-subscription-status :<!
-- :doc Delete an existing subscription or sub block.
with
  diff as
    (
      select :sid as sid,
             count(xid) as total
        from (select xid
                from sub_subscriber
               where status = :status
                 and uid = :uid
                 and sid = :sid) as existing
    ),
  update_subscriber_count as
    (
      update sub s
         set subscribers = subscribers - total * :adjust
             from diff d
       where d.sid = s.sid
    ),
  delete_existing as
    (
    delete from sub_subscriber
    where uid = :uid
    and sid = :sid
    )
select sub.sid,
       sub.name,
       ss.status,
       ss.order
  from sub
       left join (select *
                    from sub_subscriber
                   where sid = :sid
                     and uid = :uid
                     and status != :status
                   order by xid desc
                   limit 1) as ss
           on ss.sid = sub.sid
 where sub.sid = :sid;

-- :name update-preferences :? :*
-- :doc Update the user's preferences.  If the user had previously
-- :doc turned direct message blocking off, and is now turning it
-- :doc on, return the date on which it was turned off.
with
  new_vals as (
    select key, value
      from (values :tuple*:prefs) as v(key, value)
  ),
  to_update as (
    select key, value
    from new_vals
     where key in (select key
                     from user_metadata
                    where uid = :uid)
  ),
  to_insert as (
    select key, value
      from new_vals
     where key not in (select key
                         from user_metadata
                        where uid = :uid)
  ),
  old_block_dms as (
    select value from user_metadata
     where key = 'block_dms'
       and uid = :uid
  ),
  new_block_dms as (
    select value from new_vals
     where key = 'block_dms'
  ),
  existing_unblock as (
    select value from user_metadata
     where key = 'dms_unblocked'
       and uid = :uid
     order by value::timestamp desc
     limit 1
  ),
  insert_unblock as (
    select 'dms_unblocked' as key,
           :datetime as value
      from old_block_dms, new_block_dms
     where old_block_dms.value = '1'
       and new_block_dms.value = '0'
  ),
  insert_block as (
    select 'dms_blocked' as key,
           :datetime as value
      from new_block_dms
     where (not exists (select value from old_block_dms)
            or (select value from old_block_dms) = '0')
       and new_block_dms.value = '1'
  ),
  updates as (
    update user_metadata
       set value = to_update.value
      from to_update
     where to_update.key = user_metadata.key
       and uid = :uid
  ),
  inserts as (
    insert into user_metadata (uid, key, value)
                (select :uid as uid, key, value
                   from to_insert
                          union
                 select :uid as uid, key, value
                   from insert_block
                          union
                 select :uid as uid, key, value
                   from insert_unblock
                 )
  ),
  update_language as (
    update "user"
       set language = :language
     where uid = :uid
  )
select existing_unblock.value::timestamp as datetime
  from existing_unblock
 where exists (select value
                 from new_vals
                where key = 'block_dms'
                  and value = '1');

-- :name select-required-name-change :? :*
-- :doc Get a name change requirement, if it exists.
select required_name_change.uid, admin_uid, message, required_at
  from required_name_change
       left join "user" u
           on u.uid = required_name_change.uid
 where name = :name
   and completed_at is null;

-- :name insert-required-name-change :<!
-- :doc Impose a name change requirement.
-- :doc Leave an existing requirement unchanged and return its values.
with
  existing_requirement as (
    select uid, admin_uid, message, required_at
      from required_name_change
     where uid = :uid
       and completed_at is null
  ),
  new_requirement as (
    insert into required_name_change (uid, admin_uid, message, required_at)
                (select :uid as uid,
                        :admin-uid as admin_uid,
                        :message as message,
                        :datetime as required_at
                  where not exists (select uid from existing_requirement))
    returning *
  ),
  force_new_login as (
    update "user"
       set resets = resets + 1
     where uid = :uid
       and exists (select uid
                     from new_requirement)
  ),
  site_log as (
    insert into site_log (action, "desc", link, time, uid, target_uid)
                (select :site-log/REQUIRE_NAME_CHANGE as action,
                        (select name
                           from "user"
                          where uid = :uid) as desc,
                        '' as link,
                        required_at as time,
                        admin_uid as uid,
                        uid as target_uid
                   from new_requirement)
  )
select uid, admin_uid, message, required_at from new_requirement
union
select uid, admin_uid, message, required_at from existing_requirement;

-- :name delete-required-name-change :! :n
-- :doc Remove an existing name change requirement, if any.
with
  site_log as (
    insert into site_log (action, "desc", link, time, uid, target_uid)
                (select :site-log/CANCEL_REQUIRE_NAME_CHANGE as action,
                        '' as desc,
                        '' as link,
                        :datetime as time,
                        :admin-uid as uid,
                        :uid as target_uid
                  where exists (select message
                                  from required_name_change
                                 where uid = :uid))
  )
delete from required_name_change
    where uid = :uid;

-- :name select-required-name-change-for-notification :? :*
-- :doc Get a completed required name change with no notification yet.
select required_name_change.id,
       required_name_change.uid,
       (select name
          from username_history
         where uid = required_name_change.uid
           and required_id is not null
         order by changed desc
         limit 1
       ) as old_name,
       u.name as current_name,
       admin_user.name as admin_name,
       required_name_change.message
  from required_name_change
       left join "user" u
           on u.uid = required_name_change.uid
       left join "user" admin_user
           on admin_user.uid = required_name_change.admin_uid
 where required_name_change.completed_at is not null
   and required_name_change.notification_mtid is null
 limit 1
   for update of required_name_change skip locked;

-- :name update-required-name-change :! :n
-- :doc Set the notification_mtid field of a required_name_change record.
update required_name_change
   set notification_mtid = :notification-mtid
 where id = :id;

-- :snip by-uids-snip
where u.uid in (:v*:uids)

-- :snip by-name-snip
where lower(u.name) = :name
   or (:is-admin?
       and exists (select id
                     from username_history
                    where username_history.uid = u.uid
                      and lower(username_history.name) = :name))

-- :snip subscription-field-snip
   (select
      json_agg(json_build_object('order', sub_subscriber.order,
                                 'status', sub_subscriber.status,
                                 'sid', sub_subscriber.sid,
                                 'name', sub.name)) as subs
      from (select sub_subscriber.uid,
                   sub_subscriber.sid,
                   max(sub_subscriber.time) as maxtime
              from sub_subscriber
             group by uid, sid) as ss
           inner join sub_subscriber
               on sub_subscriber.sid = ss.sid
               and sub_subscriber.uid = ss.uid
               and sub_subscriber.time = ss.maxtime
           left join sub
               on sub.sid = sub_subscriber.sid
     where sub_subscriber.uid = u.uid
     group by sub_subscriber.uid
   ) as subs,

-- :snip notification-cte-snip
  message_count as (
    select :uid as uid,
           count(*)
     from message
          left join user_message_block
              on user_message_block.uid = :uid
              and user_message_block.target = message.sentby
          inner join user_unread_message
              on user_unread_message.uid = :uid
              and user_unread_message.mid = message.mid
          inner join user_message_mailbox
              on user_message_mailbox.uid = :uid
              and user_message_mailbox.mid = message.mid
    where user_message_mailbox.mailbox = :message/INBOX
      and (user_message_block.id is null
           or (message.mtype != :message/USER_TO_USER))
  ),
  notification_count as (
    select :uid as uid,
           count (*)
      from notification
           left join user_content_block
               on user_content_block.uid = :uid
               and user_content_block.target = notification.sentby
           left join sub_mod
               on sub_mod.uid = notification.sentby
               and sub_mod.sid = notification.sid
               and not sub_mod.invite
           left outer join sub_mod as sub_mod_current_user
               on sub_mod_current_user.uid = :uid
               and sub_mod_current_user.sid = notification.sid
               and not sub_mod_current_user.invite
     where notification.receivedby = :uid
       and notification.read is null
       and (user_content_block.id is null
            or not sub_mod.power_level is null
            or not sub_mod_current_user.power_level is null
            or not (notification.type in ('POST_REPLY',
                                          'COMMENT_REPLY',
                                          'POST_MENTION',
                                          'COMMENT_MENTION')))
  ),

-- :snip notification-field-snip
  message_count.count as unread_message_count,
  notification_count.count as unread_notification_count,

-- :snip notification-join-snip
  left join message_count
      on message_count.uid = u.uid
  left join notification_count
      on notification_count.uid = u.uid

-- :snip meta-fields-snip
   (select json_object_agg(user_metadata.key, user_metadata.value) as meta
      from user_metadata
     where user_metadata.key in ('admin', 'labrat', 'nochat', 'no_collapse',
                                 'invitecode', 'nsfw', 'nsfw_blur', 'block_dms')
           and user_metadata.uid = u.uid
     group by user_metadata.uid
   ) as meta,

-- :snip subs-moderated-field-snip
   (select json_agg(
     json_build_object('sid', sub_mod.sid,
                       'sub_name', sub.name,
                       'power_level', sub_mod.power_level))
      from sub_mod
           left join sub
               on sub.sid = sub_mod.sid
     where sub_mod.uid = u.uid
       and not sub_mod.invite
     group by u.uid) as mods,

-- :snip content-blocks-field-snip
  (select json_agg(
    json_build_object('uid', user_content_block.target,
                      'content_block', user_content_block.method))
     from user_content_block
      where user_content_block.uid = u.uid
    group by u.uid) as content_blocks,

-- :snip badges-cte-snip
  user_badges as (
    select user_metadata.uid,
           sum(badge.score) as score,
           json_agg(json_build_object('name', badge.name,
                                      'alt', badge.alt,
                                      'icon', badge.icon)) as info
      from user_metadata
           left join badge
               on badge.bid::text = user_metadata.value
           -- this join is just here to make using where-snip possible
           left join "user" u on u.uid = user_metadata.uid
      :snip:where-snip
        and user_metadata.key = 'badge'
     group by user_metadata.uid
  ),

-- :snip badges-field-snip
  user_badges.info as badges,

-- :snip badges-join-snip
  left join user_badges
      on user_badges.uid = u.uid

-- :snip personal-field-snip
  u.language,
  u.resets,

-- :snip score-cte-snip
  score as (
    select u.uid,
           u.score + coalesce(user_badges.score, 0) as score,
           case when (u.score + coalesce(user_badges.score, 0)) < 0
             then 0
           else trunc(sqrt((u.score + coalesce(user_badges.score, 0)) / 10.0))
           end as level
      from "user" u
           left join user_badges
               on user_badges.uid = u.uid
     :snip:where-snip
  ),

-- :snip score-field-snip
  u.score,
  score.level,
  case when score.score < 0
    then 0.0
  else (
    (score.score - score.level * score.level * 10.0)
    /
    (10.0 * ((score.level + 1) * (score.level + 1) - (score.level * score.level)))
  ) end as progress,

-- :snip score-join-snip
  left join score
  on score.uid = u.uid

-- :snip username-history-field-snip
  (select json_agg(json_build_object('name', name,
                                     'changed', extract(epoch from changed),
                                     'required_by_admin', required_id is not null)
                    order by changed desc)
      from username_history
     where username_history.uid = u.uid
     group by u.uid) as username_history,
