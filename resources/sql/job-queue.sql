-- sql/job-queue.sql -- Job queue stats queries for ovarit-app
-- Copyright (C) 2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name job-counts :? :1
-- :doc Get some job stats.
select (select count(*)
          from proletarian.job
         where queue = ':proletarian/default') as default_count,
       (select count(*)
          from proletarian.job) as job_count,
       (select count(*)
          from proletarian.archived_job) as archived_job_count;
