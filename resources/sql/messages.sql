-- sql/messages.sql -- Message-related SQL queries for ovarit-app
-- Copyright (C) 2020-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name select-messages-by-mid :? :*
-- :doc Get messages by mids
select message.mid,
       message.content,
       message.mtype,
       message.posted,
       message.receivedby,
       message.sentby,
       message.first,
       message.mtid,
       message_thread.sid
  from message
       left join message_thread
           on message_thread.mtid = message.mtid
       left join message as first_message
           on first_message.mtid = message.mtid
           and first_message.first
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message.mtid
 where message.mid in (:v*:mids);

-- :name select-message-thread :? :1
-- :doc Get a single message thread
select message_thread.*,
       first_message.mtype,
       (case when first_message.mtype = :message/USER_TO_MODS
         then first_message.sentby
        else first_message.receivedby end) as target_uid
  from message_thread
       left join message as first_message
           on first_message.mtid = :mtid
           and first_message.first
 where message_thread.mtid = :mtid;

-- :name _select-modmail-thread :? :1
-- :doc Get a single modmail message thread
select message_thread.*,
       (case when first_message.mtype = :message/USER_TO_MODS
         then first_message.sentby
        else first_message.receivedby end) as target_uid,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message,
       row_to_json(sub_post_report) as post_report,
       row_to_json(sub_post_comment_report) as comment_report,
       (select json_agg(json_build_object('action', log.action,
                                          'uid', log.uid,
                                          'time', extract(epoch from log.updated),
                                          'desc', log.desc)
                                          order by log.updated desc)
          from sub_message_log as log
         where log.mtid = :mtid) as log_entries
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
       left join sub_message_log as report_log
           on report_log.mtid = :mtid
           and report_log.action in (:message/REF_POST_REPORT,
                                     :message/REF_COMMENT_REPORT)
       left join sub_post_report
           on report_log.action = :message/REF_POST_REPORT
           and sub_post_report.id::text = report_log.desc
       left join sub_post_comment_report
           on report_log.action = :message/REF_COMMENT_REPORT
           and sub_post_comment_report.id::text = report_log.desc
 where message_thread.mtid = :mtid;

-- :name select-modmail-threads
-- :doc Get modmail threads ordered by most recent message
select message_thread.*,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~             (if (:unread-only params) "inner join" "left join")
                user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and first_message.mtype in (:v*:mtypes)
   and mailbox = :mailbox
--~ (when (:before params) "and latest_message.posted < :before")
 order by latest_message.posted desc
 limit :limit;

-- :name select-new-modmail-threads :? :*
-- :doc Get threads for the modmail "New" mailbox
-- :doc This includes threads from users to mods that don't have replies
-- :doc yet, and user notification threads with just one reply (the user)
select message_thread.*,
       first_message.posted,
       row_to_json(first_message) as first_message,
       latest_message.posted as latest_posted,
       row_to_json(latest_message) as latest_message,
       sub_message_mailbox.mailbox
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~ (if (:unread-only params) "inner join" "left join")
                  user_unread_message
                  on user_unread_message.uid = :uid
                  and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                  left join user_unread_message
                      on user_unread_message.uid = :uid
                      and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and mailbox = :message/INBOX
   and ((message_thread.replies = 0
         and first_message.mtype = :message/USER_TO_MODS)
        or (message_thread.replies > 0
            and first_message.mtype = :message/USER_NOTIFICATION
            and not exists (
              select * from message
               where message.mtid = message_thread.mtid
                 and not message.first
                 and message.mtype != :message/USER_TO_MODS
            )
        )
   )
--~ (when (:before params) "and first_message.posted < :before")
order by first_message.posted desc
limit :limit;

-- :name select-in-progress-modmail-threads :? :*
-- :doc Get threads which are either from users with replies, or from mods.
select message_thread.*,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~             (if (:unread-only params) "inner join" "left join")
                user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and ((first_message.mtype = :message/USER_TO_MODS
         and not latest_message.first)
         or first_message.mtype in (:message/MOD_TO_USER_AS_USER,
                                    :message/MOD_TO_USER_AS_MOD)
         or (first_message.mtype = :message/USER_NOTIFICATION
             and not latest_message.first
             and exists (
               select *
                 from message
                where message.mtid = message_thread.mtid
                  and not message.first
                  and message.mtype != :message/USER_TO_MODS)))
   and sub_message_mailbox.mailbox = :message/INBOX
--~ (when (:before params) "and latest_message.posted < :before")
 order by latest_message.posted desc
 limit :limit;

-- :name select-messages-in-thread :? :*
-- :doc Get messages in a conversation
select message.mid,
       message.content,
       message.mtype,
       message.posted,
       message.receivedby,
       message.sentby,
       message.first,
       message.mtid,
       message_thread.sid,
       user_unread_message.uid as unread
  from message
       left join message_thread
           on message_thread.mtid = :mtid
       left join message as first_message
           on first_message.mtid = :mtid
           and first_message.first
       left join user_unread_message
           on user_unread_message.uid = :uid
           and user_unread_message.mid = message.mid
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message.mtid
 where message.mtid = :mtid
--~ (when (:before params) "and message.posted < :before")
 order by message.posted desc
 limit :limit;

-- :name select-notification-messages :? :*
-- :doc Get messages in a notification thread
select message.mid,
       message.content,
       message.mtype,
       message.posted,
       message.receivedby,
       message.sentby,
       message.first,
       message.mtid,
       message_thread.sid,
       user_unread_message.uid as unread
  from sub_message_log
       left join "user" u
           on u.uid = sub_message_log.uid
       left join message_thread
           on message_thread.mtid = sub_message_log.mtid
       left join message
           on message.mtid = sub_message_log.mtid
       left join user_unread_message
           on user_unread_message.uid = :uid
           and user_unread_message.mid = message.mid
 where lower(u.name) = lower(:name)
   and sub_message_log.action = :message/DOWNVOTE_NOTIFICATION
   and message.mtype = :message/MOD_NOTIFICATION
--~ (when (:before params) "and message.posted < :before")
 order by message.posted desc
 limit :limit;

-- :name select-mods-with-notification-counts :? :*
-- :doc Get mods of a sub with their notification counts.
with
  mods as
    (
      select u.uid
        from public.user as u
             left join sub_mod
                 on sub_mod.uid = u.uid
       where sub_mod.sid = :sid
         and not sub_mod.invite
    ),
  post_report as (
    select open
      from sub_post_report
           inner join sub_post
               on sub_post.pid = sub_post_report.pid
     where sub_post.sid = :sid
  ),
  comment_report as (
    select open
        from sub_post_comment_report
             inner join sub_post_comment
                 on sub_post_comment.cid = sub_post_comment_report.cid
             left join sub_post
                 on sub_post.pid = sub_post_comment.pid
     where sub_post.sid = :sid
  ),
  report_counts as (
    select (select count(*) from post_report where post_report.open) +
             (select count(*) from comment_report where comment_report.open) as open_report_count,
           (select count(*) from post_report where not post_report.open) +
             (select count(*) from comment_report where not comment_report.open) as closed_report_count
  ),
  unread_messages as (
    select count(message.mid) as count, message.receivedby as uid
      from message
           inner join mods
               on mods.uid = message.receivedby
           left join user_message_block
               on user_message_block.uid = message.receivedby
               and user_message_block.target = message.sentby
           inner join user_unread_message
               on user_unread_message.uid = message.receivedby
               and user_unread_message.mid = message.mid
           inner join user_message_mailbox
               on user_message_mailbox.uid = message.receivedby
               and user_message_mailbox.mid = message.mid
     where user_message_mailbox.mailbox = :message/INBOX
       and user_message_block.id is null
       and message.mtype = :message/USER_TO_USER
     group by message.receivedby
  ),
  unread_notifications as (
    select count(notification.id), notification.receivedby as uid
      from notification
           inner join mods
               on mods.uid = notification.receivedby
           left join user_content_block
               on user_content_block.uid = notification.receivedby
               and user_content_block.target = notification.sentby
           left join sub_mod
               on sub_mod.uid = notification.sentby
               and sub_mod.sid = notification.sid
               and not sub_mod.invite
           left outer join sub_mod as sub_mod_current_user
               on sub_mod_current_user.uid = notification.receivedby
               and sub_mod_current_user.sid = notification.sid
               and not sub_mod_current_user.invite
     where notification.read is null
       and (user_content_block.id is null
            or not sub_mod.power_level is null
            or not sub_mod_current_user.power_level is null
            or not (notification.type in ('POST_REPLY',
                                          'COMMENT_REPLY',
                                          'POST_MENTION',
                                          'COMMENT_MENTION')))
     group by notification.receivedby
  ),
  modmail_thread as (
    select message_thread.mtid,
           msg.mid,
           first_msg.mtype,
           message_thread.replies
      from message_thread
           inner join lateral (
             select * from message
              where message.mtid = message_thread.mtid
              order by posted desc
              limit 1
           ) as msg on true
           inner join lateral (
             select *
               from message
              where message.mtid = message_thread.mtid
                and message.first
              limit 1 ) as first_msg on true
           left join sub_message_mailbox
                       on sub_message_mailbox.mtid = msg.mtid
     where message_thread.sid = :sid
       and mailbox = :message/INBOX
  ),
  unread_modmail_thread as
    (
      select modmail_thread.mtid,
             modmail_thread.mtype,
             modmail_thread.replies,
             user_unread_message.uid
	from user_unread_message
             inner join modmail_thread
		 on modmail_thread.mid = user_unread_message.mid
    ),
  unread_modmail_thread_count as (
    select uid, count(mtid) as count
      from unread_modmail_thread
     group by uid
    ),
  all_unread_modmail_thread as
    (
       select uid, count(mtid) as count
        from unread_modmail_thread
        where mtype in (:message/USER_TO_MODS,
                        :message/MOD_TO_USER_AS_MOD,
                        :message/MOD_TO_USER_AS_USER,
                        :message/USER_NOTIFICATION)
        group by uid
    ),
  new_unread_modmail_thread as (
    select uid, count(mtid) as count
      from unread_modmail_thread
     where (replies = 0 and mtype = :message/USER_TO_MODS)
      -- Count user replies to notification messages with no mod
      -- interaction yet.
        or (replies > 0
            and mtype = :message/USER_NOTIFICATION
            and not exists ( select *
                               from message
                              where message.mtid = unread_modmail_thread.mtid
                                and not message.first
                                and message.mtype != :message/USER_TO_MODS))
      group by uid
  ),
  in_progress_unread_modmail_thread as (
    select uid, count(mtid) as count
      from unread_modmail_thread
     where (mtype = :message/USER_TO_MODS and replies > 0)
           or mtype in (:message/MOD_TO_USER_AS_USER,
                        :message/MOD_TO_USER_AS_MOD)
        or (mtype = :message/USER_NOTIFICATION
            and replies > 0
            and exists (
              select *
                from message
               where message.mtid = unread_modmail_thread.mtid
                 and not message.first
                 and message.mtype != :message/USER_TO_MODS))
      group by uid
  ),
  discussion_unread_modmail_thread as (
    select uid, count(mtid) as count
      from unread_modmail_thread
     where mtype = :message/MOD_DISCUSSION
     group by uid
  ),
  notification_unread_modmail_thread as (
    select uid, count(mtid) as count
      from unread_modmail_thread
     where mtype = :message/MOD_NOTIFICATION
     group by uid
  )
select mods.uid,
       coalesce(unread_messages.count, 0) as messages,
       coalesce(unread_notifications.count, 0) as notifications,
       coalesce(unread_modmail_thread_count.count, 0) as unread_modmail_count,
       coalesce(all_unread_modmail_thread.count, 0) as all_unread_modmail_count,
       coalesce(new_unread_modmail_thread.count, 0) as new_unread_modmail_count,
       coalesce(in_progress_unread_modmail_thread.count, 0) as in_progress_unread_modmail_count,
       coalesce(discussion_unread_modmail_thread.count, 0) as discussion_unread_modmail_count,
       coalesce(notification_unread_modmail_thread.count, 0) as notification_unread_modmail_count,
       (select open_report_count from report_counts),
       (select closed_report_count from report_counts)
  from mods
       left join unread_messages
           on unread_messages.uid = mods.uid
       left join unread_notifications
           on unread_notifications.uid = mods.uid
       left join unread_modmail_thread_count
           on unread_modmail_thread_count.uid = mods.uid
       left join all_unread_modmail_thread
           on all_unread_modmail_thread.uid = mods.uid
       left join new_unread_modmail_thread
           on new_unread_modmail_thread.uid = mods.uid
       left join in_progress_unread_modmail_thread
           on in_progress_unread_modmail_thread.uid = mods.uid
       left join discussion_unread_modmail_thread
           on discussion_unread_modmail_thread.uid = mods.uid
       left join notification_unread_modmail_thread
           on notification_unread_modmail_thread.uid = mods.uid;

-- :name make-message-unread :! :n
-- :doc Insert unread message record if one does not already exist.
insert into user_unread_message (uid, mid)
select uid, mid
  from (values (:uid, :mid)) as to_be_inserted (uid, mid)
 where uid not in (select uid
                     from user_unread_message
                    where uid = :uid and mid = :mid);

-- :name make-thread-unread :! :n
-- :doc Insert unread message records for all messages in thread.
insert into user_unread_message (uid, mid)
select uid, mid
  from (select :uid, message.mid
          from message
               left join user_unread_message
                   on user_unread_message.uid = :uid
                   and user_unread_message.mid = message.mid
         where message.mtid = :mtid
               and user_unread_message.uid is null)
         as to_be_inserted (uid, mid);

-- :name make-message-read :! :n
-- :doc Delete unread message record if one exists.
delete from user_unread_message
 where uid = :uid and mid = :mid;

-- :name make-thread-read :! :n
-- :doc Delete unread message records for all messages in thread.
delete from user_unread_message
 where id in (select id
                from user_unread_message
                     left join message
                         on message.mid = user_unread_message.mid
               where message.mtid = :mtid
                 and user_unread_message.uid = :uid);

-- :name change-mailbox :! :n
-- :doc Change mailbox for a modmail conversation.
with
  log_entry as
    (
      insert into sub_message_log (action, mtid, "desc", uid, updated)
      values (:message/CHANGE_MAILBOX, :mtid, :mailbox::text, :uid, :updated)
      )
update sub_message_mailbox
   set mailbox = :mailbox
 where mtid = :mtid

-- :name insert-new-modmail-thread-returning :<!
-- :doc Insert a modmail message, creating a thread, mailboxes and unreads for it
with
  :snip:sub-snip
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0,
              :subject,
              coalesce((select sid from sub), :sid))
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content, :mtype, :posted, :receivedby, :sentby, True, (select mtid from thread))
      returning *
    ),
  sub_mbox as
    (
      insert into sub_message_mailbox (mtid, mailbox, highlighted)
      values ((select mtid from thread),
              (case when :mtype = :message/USER_NOTIFICATION
                then :message/PENDING
               else :message/INBOX end),
              False)
    ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select msg.mid, :message/INBOX as mailbox, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  --~ (when (:report-type params) ":snip:report-snip")
  --~ (when (:reference-thread-id params) ":snip:reference-thread-snip")
  recipient_unread as
    (
      insert into user_unread_message (mid, uid)
      select mid, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  other_mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = (select sid from thread)
         and uid <> :sentby
         and :mtype != :message/USER_NOTIFICATION
    )
select msg.mid,
       msg.content,
       msg.mtype,
       msg.posted,
       msg.receivedby,
       msg.sentby,
       (select sid from thread) as sid,
       msg.mtid
  from msg;

-- :name insert-or-append-admin-notification :<!
-- :doc Insert an admin notification, creating a thread or adding to an existing one.
with
  admin_sub as (
    select sid
      from sub, site_metadata
     where site_metadata.key = 'site.admin_sub'
       and sub.name = site_metadata.value
    ),
  existing_log_entry as (
    select mtid
      from sub_message_log
     where uid = :uid
       and action = :action
  ),
  update_replies as (
    update message_thread
       set replies = replies + 1,
           subject = :subject
      from existing_log_entry
     where message_thread.mtid = existing_log_entry.mtid
    ),
  new_thread as (
    insert into message_thread(replies, subject, sid)
    select 0, :subject, sid
      from admin_sub
     where not exists(select mtid from existing_log_entry)
    returning *
  ),
  new_log_entry as (
    insert into sub_message_log (uid, updated, action, "desc", mtid)
    select :uid, :posted, :action, null, mtid
      from new_thread
     where exists (select sid from admin_sub)
  ),
  sub_mbox as (
    insert into sub_message_mailbox (mtid, mailbox, highlighted)
    select mtid, :message/INBOX, False
      from new_thread
     where exists (select sid from admin_sub)
  ),
  update_existing_sub_mbox as (
    update sub_message_mailbox
       set mailbox = :message/INBOX
      from existing_log_entry
     where sub_message_mailbox.mtid = existing_log_entry.mtid
    ),
  thread as (
    select coalesce((select mtid from new_thread),
                    (select mtid from existing_log_entry)) as mtid
  ),
  msg as (
    insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
    select :content, :message/MOD_NOTIFICATION, :posted, null, :uid,
           exists (select mtid from new_thread), mtid
      from thread
     where mtid is not null
    returning *
  ),
  other_mods_unread as (
    insert into user_unread_message (uid, mid)
    select uid, mid
      from sub_mod, msg
     where sid = (select sid from admin_sub)
  )
select msg.mid, msg.mtid, admin_sub.sid
  from msg, admin_sub;

-- :name insert-contact-mods-thread-returning :<!
-- :doc Insert a modmail message from a user, creating a thread, mailboxes and unreads for it
with
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0, :subject, :sid)
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content,
              :message/USER_TO_MODS,
              :posted,
              :receivedby,
              :sentby,
              True,
              (select mtid from thread))
      returning *
    ),
  sub_mbox as
    (
      insert into sub_message_mailbox (mtid, mailbox, highlighted)
      values ((select mtid from thread), :message/INBOX, False)
    ),
  sender_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/SENT as mailbox, :sentby as uid
        from msg
       where :sentby::text is not null
    ),
  mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = :sid
      )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, :sid as sid,
       msg.mtid
  from msg;

-- :name insert-modmail-reply-returning :<!
-- :doc Insert a modmail message reply, creating a thread for it
with
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content, :mtype, :posted, :receivedby, :sentby, False, :mtid)
      returning *
    ),
  replies as
    (
      update message_thread
         set replies = replies + 1
       where mtid = :mtid
      ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select msg.mid, :message/INBOX as mailbox, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  recipient_unread as
    (
      insert into user_unread_message (mid, uid)
      select mid, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  other_mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = :sid
         and uid <> :sentby
      )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, :sid as sid, msg.mtid
  from msg;

-- :name insert-message-thread-returning :<!
-- :doc Insert a message, creating a thread, mailboxes and unreads for it
with
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0, :subject, null)
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content,
              :message/USER_TO_USER,
              :posted,
              :receivedby,
              :sentby,
              True,
              (select mtid from thread))
      returning *
    ),
  sender_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/SENT as mailbox, :sentby
        from msg
    ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/INBOX as mailbox, :receivedby
        from msg
    ),
  recipient_unread as
    (
      insert into user_unread_message (uid, mid)
      select :receivedby, mid
        from msg
    )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, null as sid,
       msg.mtid
  from msg;


-- :name select-post-report :? :*
-- :doc Return a post report matching id, sid and username if it exists."
select id
  from sub_post_report
       left join sub_post
           on sub_post.pid = sub_post_report.pid
       left join "user" as reporter
           on reporter.uid = sub_post_report.uid
       left join "user" as target
           on target.uid = sub_post.uid
 where id = :report-id
   and sub_post.sid = :sid
   and (reporter.name = :name
        or target.name = :name);

-- :name select-comment-report :? :*
-- :doc Return a comment report matching id, sid and username if it exists."
select id
  from sub_post_comment_report
       left join sub_post_comment
           on sub_post_comment.cid = sub_post_comment_report.cid
       left join sub_post
           on sub_post.pid = sub_post_comment.pid
       left join "user" as reporter
           on reporter.uid = sub_post_comment_report.uid
       left join "user" as target
           on target.uid = sub_post_comment.uid
 where id = :report-id
   and sub_post.sid = :sid
   and (reporter.name = :name
        or target.name = :name);

-- :name count-direct-messages-since :? :1
-- :doc Count the number of direct messages sent since a time.
select (select count(mid)
          from message
         where sentby = :uid
           and mtype = :message/USER_TO_USER
           and posted > :timestamp) as num,
       (select name
          from "user"
         where uid = :uid) as name;

-- :name select-direct-messages-inbox :? :*
-- :doc Get direct messages (test only, no pagination).
select message.*
  from message
       left join user_message_mailbox
           on user_message_mailbox.uid = :uid
           and user_message_mailbox.mid = message.mid
 where message.receivedby = :uid
   and user_message_mailbox.mailbox = :message/INBOX
 order by message.mid desc;

-- :snip create-report-logs-snip
  post_rpt_log as
    (
      insert into post_report_log (id, action, "desc", time, uid)
      select :report-id as id,
             (case
              when :receivedby = (select uid
                                    from sub_post_report
                                   where id = :report-id)
                then :report-log/MODMAIL_TO_REPORTER
              else :report-log/MODMAIL_TO_REPORTED_USER
              end) as action,
             msg.mtid::text as "desc",
             msg.posted as time,
             msg.sentby as uid
        from msg
       where :report-type = 'POST'
    ),
  comment_rpt_log as
    (
      insert into comment_report_log (id, action, "desc", time, uid)
      select :report-id as id,
             (case
              when :receivedby = (select uid
                                    from sub_post_comment_report
                                   where id = :report-id)
                then :report-log/MODMAIL_TO_REPORTER
              else :report-log/MODMAIL_TO_REPORTED_USER
              end) as action,
             msg.mtid::text as "desc",
             msg.posted as time,
             msg.sentby as uid
        from msg
       where :report-type = 'COMMENT'
    ),
  sub_msg_log as
    (
      insert into sub_message_log (action, mtid, uid, "desc", updated)
      select (case
              when :report-type = 'POST'
                then :message/REF_POST_REPORT
              else :message/REF_COMMENT_REPORT
              end) as action,
             msg.mtid,
             msg.sentby as uid,
             :report-id::text as "desc",
             msg.posted as updated
        from msg
    ),

-- :snip admin-sub-snip
  sub as (
    select sid
      from sub, site_metadata
     where site_metadata.key = 'site.admin_sub'
       and sub.name = site_metadata.value
    ),

-- :snip sub-snip
  sub as (
    select :sid as sid
  ),

-- :snip create-reference-thread-logs-snip
  reference_message_log as
    (
      insert into sub_message_log (action, mtid, uid, "desc", updated)
      values (:message/REF_NEW_THREAD, :reference-thread-id, :sentby,
              (select mtid::text from msg), :posted),
             (:message/REF_NOTIFICATION, (select mtid from msg), :sentby,
              :reference-thread-id::text, :posted)
    ),
