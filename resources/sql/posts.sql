-- sql/posts.sql -- Post-related SQL queries for ovarit-app
-- Copyright (C) 2020-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-post-by-pid :? :*
-- :doc Get posts by pids
:snip:start-cte-snip
:snip:meta-cte-snip
:snip:vote-cte-snip
:snip:content-history-cte-snip
:snip:open-reports-cte-snip
:snip:poll-cte-snip
:snip:title-history-cte-snip
:snip:end-cte-snip
  select
  :snip:info-field-snip
  :snip:meta-field-snip
  :snip:saved-field-snip
  :snip:sticky-post-field-snip
  :snip:viewed-field-snip
  :snip:vote-field-snip
  :snip:content-history-field-snip
  :snip:open-reports-field-snip
  :snip:poll-field-snip
  :snip:poll-vote-field-snip
  :snip:title-history-field-snip
  author.status as author_status,
  (case when author.status = :user-status/ACTIVE
    then sub_user_flair.flair
   else null end) as author_flair,
  sub_post.uid,
  sub_post.sid,
  sub_post.deleted,
  sub_post.pid
  from sub_post
  :snip:meta-join-snip
  :snip:saved-join-snip
  :snip:sticky-post-join-snip
  :snip:viewed-join-snip
  :snip:vote-join-snip
  :snip:content-history-join-snip
  :snip:open-reports-join-snip
  :snip:poll-join-snip
  :snip:poll-vote-join-snip
  :snip:title-history-join-snip
  left join "user" author
      on author.uid = sub_post.uid
  left join sub_user_flair -- Need to make sure there is only one
      on sub_user_flair.uid = author.uid
     and sub_user_flair.sid = sub_post.sid
  where sub_post.pid in (:v*:pids);

-- :name select-recent-post-counts :? :1
with
  recent_sub_posts as (
    select count(pid) as sub_post_count
      from sub_post
     where posted > :now::timestamp without time zone - interval '1 day'
       and uid = :uid
       and sid = :sid
  ),
  recent_site_posts as (
    select count(pid) as site_post_count
      from sub_post
     where posted > :now ::timestamp without time zone - interval '1 day'
       and uid = :uid
    )
select sub_post_count, site_post_count
  from recent_sub_posts, recent_site_posts;

-- :name delete-user-saved :! :n
delete from user_saved
 where uid = :uid
  and pid = :pid;

-- :name insert-user-saved :! :n
insert into user_saved (uid, pid)
select uid, pid
  from (values (:uid, :pid))
          as v(uid, pid)
 where not exists (select uid, pid
                     from user_saved
                    where uid = :uid
                      and pid = :pid);

-- :name update-distinguish :! :n
-- :doc Update the author distinguish field.
update sub_post
   set distinguish = :distinguish
 where pid = :pid;

-- :name set-nsfw :! :n
-- :doc Update a post's NSFW field.
update sub_post
   set nsfw = :nsfw
 where pid = :pid;

-- :snip post-order-by-posted-snip
 order by sub_post.posted desc

-- :snip post-order-by-score-snip
 order by sub_post.score, sub_post.pid desc

-- :snip post-order-by-hot-snip
 order by hot(sub_post.score, extract(epoch from sub_post.posted)) desc

-- :snip post-where-active-snip
 where sub_post.deleted = 0

-- :snip post-where-active-by-uid-snip
 where sub_post.deleted = 0 and sub_post.uid = :uid

-- :snip post-where-active-by-sid-snip
 where sub_post.deleted = 0 and sub_post.sid = :sid

-- :snip post-where-active-from-subscriptions-snip
       inner join public.sub_subscriber
       on sub_post.sid = sub_subscriber.sid
 where sub_subscriber.uid = :uid
   and sub_subscriber.status = 1
   and sub_post.deleted = 0

-- :snip post-where-active-from-default-subs-snip
       inner join sub on sub_post.sid = sub.sid
 where sub.sid in
       (select value from site_metadata where key = 'default')
   and sub_post.deleted = 0

-- :name select-posts :? :*
-- :doc Get active (non-deleted posts)
select content, deleted, link, nsfw, sub_post.pid, posted,
       edited, ptype, score, upvotes, downvotes, sid,
       thumbnail, title, comments, sub_post.uid, flair, distinguish
  from sub_post
:snip:where-snip
:snip:sort-snip
 limit :limit

-- :name delete-old-comment-views :? :1
-- :doc Delete old comment_views
with
  postids as
    (
      select pid
        from sub_post
             left join site_metadata as sm_archive
                 on sm_archive.key = 'site.archive_post_after'
             left join site_metadata as sm_announcement
                 on sm_announcement.key = 'site.announcement'
             left join site_metadata as sm_archive_sticky
                 on sm_archive_sticky.key = 'site.archive_sticky_posts'
       where pid > :startpid
         and (sm_announcement.value is null
              or pid != sm_announcement.value::integer)
         and (sm_archive_sticky.value = '1'
              or pid not in (select value::integer as pid
                               from sub_metadata
                              where sub_metadata.sid = sub_post.sid
                                and sub_metadata.key = 'sticky'))
         and posted <= now() - sm_archive.value::integer * interval '1 day'
    ),
  old_views as (
    select id
      from sub_post_comment_view
     where pid in (select pid
                     from postids)
       for update skip locked
  ),
  deleted as
    (
      delete from sub_post_comment_view
       where id in (select id
                      from old_views
                     limit :batch-size)
    returning id
    )
select (select count(*) from deleted) as deleted,
       (select count(*) from old_views) - (select count(*) from deleted) as remaining,
       (select max(pid) from postids) as pid;

-- :name insert-test-post :<
-- :doc Insert a post, for testing purposes
with
  new_post as (
    insert into sub_post (content, deleted, posted, ptype, score, upvotes,
                          downvotes, sid, title, slug, comments, uid, nsfw)
    values (:content, 0, :posted, :ptype, 1, 1, 0, :sid, :title, :slug, 0, :uid, :nsfw)
           returning *
  ),
  new_post_viewed as (
    insert into sub_post_view (uid, pid, datetime)
    select :uid as uid, new_post.pid, :posted as datetime
      from new_post
  )
select *,
       new_post.posted as viewed
  from new_post;

-- :name insert-test-post-report :<
-- :doc Insert a post report, for testing purposes
insert into sub_post_report (pid, uid, datetime, reason, open, send_to_admin)
values (:pid, :uid, :datetime, :reason, true, :send-to-admin)
       returning *;

-- :name update-post-content :! :n
-- :doc Change a post's content and add a history record.
-- :doc If the content hasn't changed, do nothing.
with
  old_content as (
    select pid, content
      from sub_post
     where pid = :pid
       and content != :content
  ),
  update_history as (
    insert into sub_post_content_history (pid, content, datetime)
    select pid, content, now() as datetime
      from old_content
  )
    update sub_post
    set content = :content, edited = now()
    from old_content
    where sub_post.pid = old_content.pid;

-- :name update-post-title :! :n
-- :doc Change a post's title and slug, and add a history record.
-- :doc If the title hasn't changed, do nothing.
with
  old_title as (
    select pid, uid, title
      from sub_post
     where pid = :pid
       and title != :title
  ),
  update_history as (
    insert into sub_post_title_history (pid, uid, title, datetime)
    select pid, uid, title, now() as datetime
      from old_title
  )
    update sub_post
    set title = :title, slug = :slug
    from old_title
    where sub_post.pid = old_title.pid;

-- :name update-post-title-by-mod :! :n
-- :doc Change a post's title and slug, and add a history record.
-- :doc Also make a sublog entry. If the title hasn't changed, do nothing.
with
  old_title as (
    select pid, uid, sid, title
      from sub_post
     where pid = :pid
       and title != :title
  ),
  update_history as (
    insert into sub_post_title_history (pid, uid, title, datetime)
    select pid, :mod-uid as uid, title, now() as datetime
      from old_title
  ),
  update_log as (
    insert into sub_log (action, "desc", link, sid, uid, target_uid, admin, time)
    select :report-log/EDIT_POST_TITLE as action,
           :reason as "desc",
           :link as link,
           sid,
           :mod-uid as uid,
           uid as target_uid,
           :is-admin as admin,
           now() as time
      from old_title
  )
    update sub_post
    set title = :title, slug = :slug
    from old_title
    where sub_post.pid = old_title.pid;

-- :name change-post-flair :!
update sub_post
   set flair = :flair
 where pid = :pid;

-- :name update-viewed :!
-- :doc Update or insert a view time record for a user and post.
insert into sub_post_view (uid, pid, datetime)
values (:uid, :pid, now())
       on conflict (uid, pid) do
       update set datetime = :timestamp;

-- :name _remove-post :! :n
-- :doc Mark a post as removed by mod or admin.
with
  :snip:sublog-entry-snip
  insert_report_logs as (
    insert into post_report_log (id, action, "desc", link, time, uid, target_uid)
    select sub_post_report.id,
           :report-log/POST_DELETED as action,
           :reason as "desc",
           null as link,
           :timestamp as time,
           :uid as uid,
           null as target_uid
      from sub_post_report
     where sub_post_report.pid = :pid
  ),
  unstick as (
    delete from sub_metadata
     where key = 'sticky'
       and value = :pid::text
    returning *
  ),
  unstick_log as (
    insert into sub_log (action, uid, sid, link, time)
    select :sub-log/STICKY_DEL as action,
           :uid as uid,
           unstick.sid,
           :postlink as link,
           :timestamp as time
      from unstick
  ),
  unannounce as (
    delete from site_metadata
     where key = 'announcement'
       and value = :pid::text
  ),
  update_sub_post_count as (
    update sub
       set posts = posts - 1
      from sub_post
     where sub_post.pid = :pid
       and sub.sid = sub_post.sid
  )
update sub_post
    set deleted = :deleted
    where pid = :pid;

-- :snip delete-by-mod-sublog-entry-snip
-- :doc Add a sublog entry when a post is deleted by a mod or admin.
  sublog_entry as (
    insert into sub_log (action, uid, sid, "desc", link, admin, time)
    values (:sub-log/DELETE_POST,
            :uid,
            :sid,
            :reason,
            :postlink,
            :admin,
            :timestamp)
    ),

-- :snip info-field-snip
  sub_post.comments as comment_count,
  sub_post.content,
  sub_post.distinguish,
  sub_post.downvotes,
  sub_post.edited,
  (select datetime
     from sub_post_title_history hist
    where hist.pid = sub_post.pid
      and hist.uid != sub_post.uid
    order by datetime desc
    limit 1) as title_edited,
  sub_post.flair,
  sub_post.link,
  sub_post.nsfw,
  sub_post.posted,
  sub_post.ptype,
  sub_post.score,
  sub_post.slug,
  case when sub_post.thumbnail = 'deferred'
    then null
    else sub_post.thumbnail end as thumbnail,
  sub_post.title,
  sub_post.upvotes,

-- :snip meta-cte-snip
  metadata as (
    select sub_post_metadata.pid,
           json_object_agg(sub_post_metadata.key, sub_post_metadata.value) as meta
      from sub_post_metadata
     where sub_post_metadata.pid in (:v*:pids)
     group by sub_post_metadata.pid),

-- :snip meta-field-snip
  metadata.meta,

-- :snip meta-join-snip
  left join metadata
    on metadata.pid = sub_post.pid

-- :snip saved-field-snip
    user_saved.xid is not null as is_saved,

-- :snip saved-join-snip
    left join user_saved
    on user_saved.uid = :uid
    and user_saved.pid = sub_post.pid

-- :snip sticky-post-field-snip
  sticky.xid is not null as sticky,

-- :snip sticky-post-join-snip
  left join sub_metadata as sticky
    on sticky.key = 'sticky'
    and sticky.value = sub_post.pid::text

-- :snip vote-cte-snip
    vote as (
      select sub_post_vote.pid,
             max(sub_post_vote.xid) as xid
        from sub_post_vote
       where sub_post_vote.uid = :uid
         and sub_post_vote.pid in (:v*:pids)
       group by sub_post_vote.pid
    ),

-- :snip vote-field-snip
    sub_post_vote.positive as vote,

-- :snip vote-join-snip
  left join vote
    on vote.pid = sub_post.pid
  left join sub_post_vote
    on sub_post_vote.xid = vote.xid

-- :snip title-history-cte-snip
    title_history as (
      select pid,
             json_agg(json_build_object('time', extract(epoch from datetime),
                                        'uid', uid,
                                        'content', title)
                      order by datetime asc) as history
        from sub_post_title_history
       where pid in (:v*:pids)
       group by pid
    ),

-- :snip title-history-field-snip
    title_history.history as title_history,

-- :snip title-history-join-snip
    left join title_history
        on title_history.pid = sub_post.pid

-- :snip content-history-cte-snip
    content_history as (
      select pid,
             json_agg(json_build_object('time', extract(epoch from datetime),
                                        'content', content)
                      order by datetime asc) as history
        from sub_post_content_history
       where pid in (:v*:pids)
       group by pid
    ),

-- :snip content-history-field-snip
    content_history.history as content_history,

-- :snip content-history-join-snip
    left join content_history
        on content_history.pid = sub_post.pid

-- :snip open-reports-cte-snip
    open_reports as (
      select pid,
             array_agg(id order by datetime desc) as ids
        from sub_post_report
       where pid in (:v*:pids)
             and sub_post_report.open
       group by pid
    ),

-- :snip open-reports-field-snip
    open_reports.ids as open_report_ids,

-- :snip open-reports-join-snip
    left join open_reports
        on open_reports.pid = sub_post.pid

-- :snip poll-cte-snip
    poll_vote as (
      select vid,
             count(id) as votes
        from sub_post_poll_vote
       where pid in (:v*:pids)
       group by vid
      ),
    poll_options as (
      select pid,
             json_agg(json_build_object('id', id,
                                        'text', text,
                                        'votes', coalesce(votes, 0))
                                        order by id asc) as options
        from sub_post_poll_option
             left join poll_vote
                 on poll_vote.vid = id
       where pid in (:v*:pids)
       group by pid
    ),
    poll_total as (
      select pid,
             count(id) as total
        from sub_post_poll_vote
       where pid in (:v*:pids)
       group by pid
      ),

-- :snip poll-field-snip
    poll_options.options as poll_options,
    coalesce(poll_total.total, 0) as poll_votes,

-- :snip poll-join-snip
    left join poll_options
        on poll_options.pid = sub_post.pid
    left join poll_total
        on poll_total.pid = sub_post.pid

-- :snip poll-vote-field-snip
    sub_post_poll_vote.vid as poll_vote,

-- :snip poll-vote-join-snip
    left join sub_post_poll_vote
        on sub_post_poll_vote.pid = sub_post.pid
       and sub_post_poll_vote.uid = :uid

-- :snip viewed-field-snip
    sub_post_view.datetime as viewed,

-- :snip viewed-join-snip
    left join sub_post_view
        on sub_post_view.pid = sub_post.pid
       and sub_post_view.uid = :uid
