-- sql/comments.sql -- Comment-related SQL queries for ovarit-app
-- Copyright (C) 2022-2023  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name select-ancestors
-- :doc Get cids of a comment and its ancestors.
with recursive
  ancestor (cid, parentcid, pid, status) as (
    select sub_post_comment.cid,
           sub_post_comment.parentcid,
           sub_post_comment.pid,
           sub_post_comment.status
      from sub_post_comment
     where cid = :cid
     union all
    select sub_post_comment.cid,
           sub_post_comment.parentcid,
           sub_post_comment.pid,
           sub_post_comment.status
      from ancestor, sub_post_comment
     where sub_post_comment.cid = ancestor.parentcid
    )
select cid,
       pid,
       coalesce(status, :comment/ACTIVE) as status
  from ancestor;


-- :name _select-comment-tree
-- :doc Get comments on a single post with their relationships.
with recursive
  comment_tree (cid, path, scores) as
    (
      select comment.cid,
             array[comment.cid::text] as path,
             array[:snip:sort-by-snip] as scores,
             comment.time,
             comment.uid,
             comment.status,
             :snip:checked-off-field-snip
             :snip:sticky-field-snip
        from sub_post_comment as comment
             :snip:checked-off-join-snip
       where comment.pid = :pid
         and comment.parentcid is null
    union all
      select comment.cid,
             array_append(ct.path, comment.cid::text) as path,
             array_append(ct.scores, :snip:sort-by-snip) as scores,
             comment.time,
             comment.uid,
             comment.status,
             :snip:checked-off-field-snip
             :snip:sticky-field-snip
        from sub_post_comment as comment
             :snip:checked-off-join-snip
             join comment_tree as ct on true
       where comment.pid = :pid
             and comment.parentcid = ct.cid
    )
select path,
       scores,
       (extract(epoch from time) * 1000)::bigint as time,
       uid,
       coalesce(status, :comment/ACTIVE) as status,
       :snip:checked-off-snip
       sticky
  from comment_tree;

-- :name _select-comments-by-cid :? :*
-- :doc Get comments by cid
:snip:start-cte-snip
:snip:history-cte-snip
:snip:vote-cte-snip
:snip:end-cte-snip
select sub_post_comment.cid,
       :snip:vote-field-snip
       :snip:checkoff-field-snip
       :snip:author-field-snip
       :snip:history-field-snip
       :snip:sticky-field-snip
       :snip:view-field-snip
       sub_post_comment.content,
       sub_post_comment.lastedit,
       sub_post_comment.parentcid as parent_cid,
       sub_post_comment.pid,
       sub_post_comment.score,
       sub_post_comment.upvotes,
       sub_post_comment.downvotes,
       coalesce(sub_post_comment.status, :comment/ACTIVE) as status,
       sub_post_comment.time,
       sub_post_comment.uid,
       sub_post.sid,
       sub_post_comment.distinguish
  from sub_post_comment
       left join sub_post
           on sub_post.pid = sub_post_comment.pid
  :snip:checkoff-join-snip
  :snip:history-join-snip
  :snip:sticky-join-snip
  :snip:view-join-snip
  :snip:vote-join-snip
  :snip:author-join-snip
where sub_post_comment.cid in (:v*:cids)
  :snip:sid-where-snip;

-- :name select-comments
select cid, content, lastedit, parentcid, pid, score, upvotes, downvotes,
       (cid = sub_post_metadata.value) as sticky,
       coalesce(status, :comment/ACTIVE) as status, time, uid
  from sub_post_comment
       left join sub_post_metadata
           on sub_post_metadata.pid = sub_post_comment.pid
           and sub_post_metadata.key = 'sticky_cid'
--~ (when (:where params) ":snip:where")
:snip:sort
 limit :limit;

-- :name _insert-comment :<!
-- :doc Insert a comment.
with
  :snip:insert-checkoff-snip
  :snip:insert-vote-snip
  :snip:mention-notification-cte-snip
  :snip:response-notification-snip
  update_post as (
    update sub_post
       set comments = comments + 1
     where pid = :pid
    returning comments
  ),
  new_comment as (
    insert into sub_post_comment (pid, uid, content, parentcid, time, cid,
                                  score, upvotes, downvotes, views)
    values (:pid, :uid, :content, :parent-cid, now(), :cid,
            :self-vote, :self-vote, 0, 0)
           returning *)
select :snip:insert-checkoff-field-snip
       :snip:mention-notification-field-snip
       pid,
       new_comment.uid,
       content,
       parentcid as parent_cid,
       new_comment.time,
       new_comment.cid,
       best_score(upvotes, downvotes, views),
       score, upvotes, downvotes, views,
       :comment/ACTIVE as status,
       false as sticky,
       receivedby
  from update_post, new_comment
       :snip:insert-checkoff-join-snip
       :snip:mention-notification-join-snip
       left join response_notification on true;

-- :name insert-test-comment-report :<
-- :doc Insert a comment report, for testing purposes
insert into sub_post_comment_report (cid, uid, datetime, reason, open, send_to_admin)
values (:cid, :uid, :datetime, :reason, true, :send-to-admin)
       returning *;

-- :name _update-comment-content :! :n
-- :doc Change a comment's content and add a history record.
-- :doc If the content hasn't changed, do nothing.
with
  old_content as (
    select cid,
           content
      from sub_post_comment
     where cid = :cid
       and content != :content
  ),
  :snip:checkoff-cte-snip
  update_history as (
    insert into sub_post_comment_history (cid, content, datetime)
    select cid, content, now() as datetime
      from old_content
  )
    update sub_post_comment
    set content = :content, lastedit = now()
    from old_content
    where sub_post_comment.cid = old_content.cid;

-- :name _insert-checkoff :<!
-- :doc Add an checkoff record and return it, or return the old one if
-- :doc there already is one.
with
  previous as (
    select uid, cid, datetime
      from sub_post_comment_checkoff
     where cid = :cid
  ),
  inserted as (
    insert into sub_post_comment_checkoff (uid, cid, datetime)
    select uid, cid, now() as datetime
      from (values (:uid, :cid))
             as v(uid, cid)
     where not exists (select cid from previous)
           returning *
  )
select uid, cid, datetime as time
  from previous
 union
select uid, cid, datetime as time
  from inserted;

-- :name _delete-checkoff :! :n
-- :doc Delete an checkoff record if one exists.
delete from sub_post_comment_checkoff
 where cid = :cid
   and uid = :uid;

-- :name update-distinguish :! :n
-- :doc Update the author distinguish field.
update sub_post_comment
   set distinguish = :distinguish
 where cid = :cid;

-- :snip delete-checkoff-snip
  checkoff as (
    delete from sub_post_comment_checkoff
     using old_content
     where sub_post_comment_checkoff.cid = old_content.cid
  ),

-- :snip insert-checkoff-snip
  checkoff as (
    insert into sub_post_comment_checkoff(cid, uid, datetime)
    values (:cid, :uid, now())
    returning *
  ),

-- :snip insert-checkoff-field-snip
    json_build_object('time', checkoff.datetime,
                      'uid', checkoff.uid) as checkoff,

-- :snip insert-checkoff-join-snip
    join checkoff on true

-- :snip update-checkoff-snip
  checkoff as (
    update sub_post_comment_checkoff
       set datetime = now()
      from old_content
     where sub_post_comment_checkoff.cid = old_content.cid
  ),

-- :snip checkoff-field-snip
    json_build_object('time', extract(epoch from sub_post_comment_checkoff.datetime),
                      'uid', sub_post_comment_checkoff.uid) as checkoff,

-- :snip checkoff-join-snip
    left join sub_post_comment_checkoff
        on sub_post_comment_checkoff.cid = sub_post_comment.cid

-- :snip insert-vote-snip
  vote as (
    insert into sub_post_comment_vote(cid, uid, positive, datetime)
    values (:cid, :uid, 1, now())
  ),
  update_user as (
    update "user"
       set given = given + 1,
           upvotes_given = upvotes_given + 1
     where uid = :uid
  ),

-- :snip comment-order-by-time-snip
 order by time desc

-- :snip comment-order-by-score-snip
 order by score, cid desc

-- :snip comment-where-pid-snip
 where pid = :pid

-- :snip comment-where-parentcid-snip
 where parentcid = :parent-cid

-- :snip comment-where-uid-snip
 where uid = :uid

-- :snip sort-by-best-snip
    case when coalesce(comment.status, :comment/ACTIVE) = :comment/ACTIVE
    then best_score(comment.upvotes, comment.downvotes, comment.views)
    else 0.0 end

-- :snip sort-by-top-snip
   comment.score::integer

-- :snip sort-by-age-snip
   comment.time

-- :snip vote-cte-snip
    vote as (
      select sub_post_comment_vote.cid,
             max(sub_post_comment_vote.xid) as xid
        from sub_post_comment_vote
       where sub_post_comment_vote.uid = :uid
         and sub_post_comment_vote.cid in (:v*:cids)
       group by sub_post_comment_vote.cid
    ),

-- :snip vote-field-snip
    sub_post_comment_vote.positive as vote,

-- :snip vote-join-snip
  left join vote
    on vote.cid = sub_post_comment.cid
  left join sub_post_comment_vote
    on sub_post_comment_vote.xid = vote.xid

-- :snip author-field-snip
    author.status as author_status,
    sub_user_flair.flair as author_flair,

-- :snip author-join-snip
    left join "user" author
        on author.uid = sub_post_comment.uid
    left join sub_user_flair
        on sub_user_flair.sid = sub_post.sid
       and sub_user_flair.uid = sub_post_comment.uid
       and author.status != :user-status/DELETED

-- :snip history-cte-snip
    history as (
      select cid,
             json_agg(json_build_object('time', extract(epoch from datetime),
                                        'content', content)
                      order by datetime asc) as content_history
        from sub_post_comment_history
       where cid in (:v*:cids)
       group by cid
    ),

-- :snip history-field-snip
    history.content_history,

-- :snip history-join-snip
    left join history
        on history.cid = sub_post_comment.cid

-- :snip sticky-field-snip
    (
      sub_post_metadata.value is not null
      and sub_post_comment.cid = sub_post_metadata.value
    ) as sticky,

-- :snip sticky-join-snip
    left join sub_post_metadata
        on sub_post_metadata.pid = sub_post_comment.pid
       and sub_post_metadata.key = 'sticky_cid'

-- :snip view-field-snip
    sub_post_comment_view.cid is not null as viewed,

-- :snip view-join-snip
    left join sub_post_comment_view
        on sub_post_comment_view.cid = sub_post_comment.cid
       and sub_post_comment_view.uid = :uid

-- :snip post-response-notification-snip
-- :doc Insert a notification to the author of a post that was just commented on.
    response_notification as (
      insert into notification (type, sid, pid, cid, sentby, receivedby, created)
      select 'POST_REPLY' as type,
             sub_post.sid as sid,
             :pid as pid,
             :cid as cid,
             :uid as sentby,
             sub_post.uid as receivedby,
             now() as created
        from sub_post
       where sub_post.pid = :pid
         and sub_post.uid != :uid
       returning receivedby
    ),

-- :snip comment-response-notification-snip
-- :doc Insert a notification to the author of a comment that was just replied to.
    response_notification as (
      insert into notification (type, sid, pid, cid, sentby, receivedby, created)
      select 'COMMENT_REPLY' as type,
             sub_post.sid as sid,
             :pid as pid,
             :cid as cid,
             :uid as sentby,
             sub_post_comment.uid as receivedby,
             now() as created
        from sub_post_comment
             left join sub_post
                 on sub_post.pid = sub_post_comment.pid
       where sub_post_comment.cid = :parent-cid
         and sub_post_comment.uid != :uid
       returning receivedby
      ),


-- :snip mention-notification-cte-snip
  mention_notification as (
    insert into notification (type, sid, pid, cid, sentby, receivedby, created)
    select 'COMMENT_MENTION' as type,
           (select sid
              from sub_post
             where sub_post.pid = :pid) as sid,
           :pid as pid,
           :cid as cid,
           :uid as sentby,
           u.uid as receivedby,
           now() as created
      from "user" u
     where lower(u.name) in (:v*:mentions)
       and u.uid != :uid
     returning receivedby
  ),
  mentioned_users as (
    select array_agg(receivedby) as uids
      from mention_notification
  ),


-- :snip mention-notification-field-snip
  mentioned_users.uids as mentioned_uids,

-- :snip mention-notification-join-snip
  left join mentioned_users on true

-- :snip sid-where-snip
  and sub_post.sid = :sid

-- :snip checked-off-snip
    checked,

-- :snip checked-off-field-snip
    (coalesce(comment.status, 0) != 0
     or sub_post_comment_checkoff is not null) as checked,

-- :snip checked-off-join-snip
    left join sub_post_comment_checkoff
        on sub_post_comment_checkoff.cid = comment.cid

-- :snip sticky-cid-snip
    (comment.cid = :cid) as sticky

-- :snip sticky-false-snip
    false as sticky
