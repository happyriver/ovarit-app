;; test_utils.clj -- Test utilities for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.test-utils
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clj-http.client :as client]
   [clojure.core.async :refer [alt!! chan close! put! timeout]]
   [clojure.core.reducers :as r]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [clojure.test :refer [is]]
   [clojure.walk :as walk]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [dotenv :refer [env]]
   [gniazdo.core :as ws]
   [graphql-builder.core :as graphql-builder]
   [graphql-builder.parser :as parser]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.message.resolve :as message]
   [ovarit.app.gql.elements.post.resolve :as post]
   [ovarit.app.gql.elements.site.resolve :as site]
   [ovarit.app.gql.elements.sub.resolve :as sub]
   [ovarit.app.gql.elements.user.resolve :as user]
   [ovarit.app.gql.protocols.asset-path :as asset-path]
   [ovarit.app.gql.protocols.payment-subscription :as payment-subscription]
   [ovarit.app.gql.protocols.ratelimit :as ratelimit]
   [ovarit.app.gql.resolver-helpers.stream :as stream]
   [ovarit.app.gql.schema :as schema]
   [ovarit.app.gql.system :as system]
   [ovarit.app.gql.system.server.flask-compat :as flask-compat]
   [ovarit.app.gql.util :as util]
   [taoensso.carmine :as car])
  (:import
   (clojure.lang IPersistentMap)))

;; Test database helpers

(defn execute-sql!
  "Execute the SQL commands in a string on a database."
  [db sql]
  (doseq [cmd (str/split sql #";\n")]
    (try
      (jdbc/execute! db (vector cmd))
      (catch org.postgresql.util.PSQLException e
        ;; pg_dump creates some SELECTs which run functions. execute!
        ;; doesn't expect to see their return values; but they still work.
        ;; Using jdbc/query instead would require parsing SQL comments.
        (when-not (= "A result was returned when none was expected."
                     (.getMessage e))
          (log/error {:cmd cmd :exception-message (.getMessage e)}
                     "Error loading SQL")
          (throw e))))))

(defn reset-database! [db]
  (log/info "Loading schema into test database")
  (execute-sql! (:ds db)
                "DROP SCHEMA public CASCADE;
                 CREATE SCHEMA public;
                 DROP SCHEMA IF EXISTS proletarian CASCADE;")
  (->> (io/resource "sql/schema.sql")
       slurp
       (execute-sql! (:ds db)))
  ;; Put a consistent best comment sort start date in the db.
  (execute-sql! (:ds db)
                "GRANT USAGE on SCHEMA public TO PUBLIC;")
  (jdbc/update! (:ds db) "site_metadata"
                {:value "1970-01-01T00:00:00Z"}
                ["key = ?" "best_comment_sort_init"]))

;;; Mock test publish and subscribe

;; The dev and prod servers rely on an external service (in Python
;; throat) to reflect /send messages as /snt ones.  Mimic that external
;; service.

(defrecord TestRepublish [uri pubsub]
  component/Lifecycle
  (start [this]
    (log/debug {:uri uri} "Starting test republish service")
    (let [conn {:pool {} :spec {:uri uri}}
          handler (fn [msg]
                    (let [[typ _ channel payload] msg]
                      (when (= typ "pmessage")
                        (let [address (-> channel
                                          (subs (count "/test/send"))
                                          (as-> $ (str "/test/snt" $)))]
                          (log/debug {:from channel :to address
                                      :payload payload}
                                     "Republishing notification message")
                          (car/wcar conn (car/publish address payload))))))]
      (assoc this :sub
             (car/with-new-pubsub-listener {:uri uri}
               {"/test/send*" handler}
               (car/psubscribe "/test/send*")))))

  (stop [{:keys [sub] :as this}]
    (log/debug {} "Stopping test republish service")
    (car/close-listener sub)
    (dissoc this :sub)))

;; Test schema extensions

(defn- get-quark []
  (get ["top" "bottom" "up" "down" "strange" "charm"]
       (rand-int 6)))

(defn- stream-announcement
  "Prototype for an endless streamer."
  [_context _args source-stream-callback]
  (let [action (stream/action-at-intervals
                #(source-stream-callback (get-quark)) 5000)]
    #(close! action)))

(defn csrf-token-from-cookie
  "Resolve a query for the CSRF token."
  [context _ _]
  (when (= (:env context) :dev)
    (get-in context [:request :signed-csrf-token])))

(defn test-resolver-map
  "Create a resolver map for the test-only queries."
  []
  (->> {:mutations/register-user          user/register-user
        :mutations/create-sub             sub/create-sub
        :mutations/create-private-message message/create-private-message
        :mutations/create-test-post       post/create-test-post
        :mutations/create-report          post/create-report
        :mutations/set-announcement-post  post/set-announcement-post
        :mutations/stick-post             post/stick-post
        :queries/csrf-token               csrf-token-from-cookie
        :queries/direct-message-inbox     message/direct-message-inbox
        :queries/sitelog-entries          site/sitelog-entries
        :queries/sublog-entries           sub/sublog-entries
        :SiteLogEntry/user                user/user-by-reference
        :SiteLogEntry/target-user         (user/user-by-reference-factory
                                           :target-uid)
        :SubLogEntry/user                 user/user-by-reference
        :SubLogEntry/target-user          (user/user-by-reference-factory
                                           :target-uid)}
       (s/transform [s/ALL] schema/wrap-resolver)
       (merge
        {:SiteLogEntry/time               (schema/to-java-time :time)
         :SubLogEntry/time                (schema/to-java-time :time)})))

(defn test-streamer-map
  []
  (let [wrapper #(schema/wrap-streamer % "test-secret" 2678400)]
    (s/transform
     [s/ALL] wrapper
     {:Post/stream-announcement stream-announcement})))

;; GraphQL support.

(defmacro defgraphql
  "Define a variable to contain the parsed contents of some graphql files."
  [var & paths]
  (let [content (->> paths
                     (map #(-> (io/resource %)
                               slurp))
                     (str/join "\n"))
        parsed (-> content
                   parser/parse
                   (graphql-builder/query-map {:inline-fragments true}))
        get-query (fn [query-fn]
                    (-> (query-fn) :graphql :query))
        queries (->> parsed
                     (s/transform [s/MAP-VALS s/MAP-VALS] get-query))]
    `(def ~var ~queries)))

(defgraphql graphql "test-graphql/shared.graphql")

(def m:create-user (-> graphql :mutation :create-user))
(def m:create-sub (-> graphql :mutation :create-sub))
(def m:create-test-post (-> graphql :mutation :create-test-post))
(def m:remove-post (-> graphql :mutation :remove-post))
(def m:create-comment (-> graphql :mutation :create-comment))

(defn hash-query-entry
  "Make a known queries table entry for a query."
  [query]
  {:key ::test
   :hash (util/short-hash query)
   :query query
   :date (jt/to-millis-from-epoch (jt/instant))})

(def known-queries
  (map hash-query-entry [m:create-user m:create-sub m:create-test-post
                         m:create-comment m:remove-post]))

(defn hash-query
  "Translate a query to its hashed version."
  [query]
  (let [hash (util/short-hash query)
        typ (first (str/split query #" "))]
    (str typ " " hash)))

(defn- query-hashes-for-test
  []
  (concat (system/query-hashes) known-queries))

(defn test-system-config-map
  "Describe a system for testing."
  ([] (test-system-config-map #{:webserver :image-manager}))
  ([roles]
   (let [query-hashes (query-hashes-for-test)]
     {:env :test
      :server {:host "localhost"
               :port 8889
               :env :test
               :redis-uri (dotenv/env :TEST_REDIS_URL)
               :request-timeout 2500
               :process-queue-size 16
               :waiting-queue-size 640
               :assets (reify asset-path/AssetPath
                         (asset-path [_ path] (str "/fe" path)))
               :manifest {:app "app.js"
                          :polyfill "polyfill.js"}
               :secret-key "test secret"
               :session-lifetime 2678400
               :pool-size 4
               :sub-prefix "o"
               :query-hashes query-hashes
               :clock (jt/system-clock "UTC")}
      :image-manager {:host "localhost"
                      :port 8899
                      :env :test}
      :schema-provider {:env :test
                        :extras {:schema  (-> (io/resource "test-schema.edn")
                                              slurp
                                              edn/read-string)
                                 :resolvers (test-resolver-map)
                                 :streamers (test-streamer-map)}
                        :secret-key "test secret"
                        :session-lifetime 2678400
                        :query-hashes query-hashes}
      :db {:host (env :TEST_DATABASE_HOST)
           :port (env :TEST_DATABASE_PORT)
           :name (env :TEST_DATABASE_NAME)
           :user (env :TEST_DATABASE_USER)
           :password (env :TEST_DATABASE_PASSWORD)}
      :cache {:uri (dotenv/env :TEST_REDIS_URL)}
      :pubsub {:uri (dotenv/env :TEST_REDIS_URL)}
      :bus {:prefix "test-mbus"}
      :notify {:env :test
               :prefixes {:outgoing "/test/send"
                          :incoming "/test/snt"}}
      :job-queue {:clock (jt/system-clock "UTC")
                  :env :test
                  :redis-uri (dotenv/env :TEST_REDIS_URL)
                  :tr format}
      :tasks (-> (system/get-tasks-config roles {})
                 (assoc :uri (dotenv/env :TEST_REDIS_URL)))})))

(defn add-queries
  "Add some queries to the known queries list in a config map."
  [config-map queries]
  (let [queries (if (map? queries)
                  (s/select [s/MAP-VALS s/MAP-VALS] queries)
                  queries)
        hashed-queries (map hash-query-entry queries)]
    (-> config-map
        (update-in [:schema-provider :query-hashes] concat hashed-queries)
        (update-in [:server :query-hashes] concat hashed-queries))))

(defn test-system-map
  "Return a system map based on the passed configuration, or on
  test-system-config-map if that is not provided."
  ([] (test-system-map (test-system-config-map) #{:webserver}))
  ([config-map] (test-system-map config-map #{:webserver}))
  ([config-map roles]
   (let [funding-progress-update? #(= (:task-name %) :funding-progress-update)]
     (-> (system/new-system-map roles config-map)
         (assoc :ratelimiter (reify ratelimit/RateLimitCheck
                               (enabled? [_] false)
                               (check! [_ _ _ _] true)))
         ;; Stop the funding update task from making requests to the
         ;; external subscription service.
         (update :taskrunner
                 #(s/setval [:tasks s/ALL funding-progress-update?
                             :options :payment-subscription]
                            (reify payment-subscription/PaymentSubscription
                              (update-email [_ _ _] :success)
                              (cancel [_ _] :success)
                              (stats [_ _] 0))
                            %))
         (assoc :test-republish
                (map->TestRepublish {:uri (dotenv/env :TEST_REDIS_URL)}))))))

(defn test-system
  "Create a new system suitable for testing.
  Use an HTTP port that doesn't conflict with the default running
  system, and create an empty test database.  Flush the Redis test
  database."
  ([] (test-system (test-system-map)))
  ([sysmap]
   (let [pg-admin-user (dotenv/env :PG_ADMIN_USER)
         pg-admin-password (dotenv/env :PG_ADMIN_PASSWORD)
         db-component (cond-> (:db sysmap)
                        (and pg-admin-user pg-admin-password)
                        (assoc :user pg-admin-user
                               :password pg-admin-password))
         db-system (-> (component/system-map :db db-component)
                       component/start-system)]
     (reset-database! (:db db-system))
     (component/stop db-system)
     (car/wcar
      {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
      (car/flushdb))
     (->> (assoc sysmap :db-admin db-component)
          r/flatten
          (into [])
          (apply component/system-map)))))

(defn disable-refresh-site-config [system-map]
  (let [refresh-site-config? #(= (:task-name %) :refresh-site-config)]
    (s/setval [:taskrunner :tasks s/ALL refresh-site-config? :options
               :test-mode?] true system-map)))

(defn simplify
  "Convert all ordered maps nested within the map into standard hash
  maps, and sequences into vectors, which makes for easier constants
  in the tests, and eliminates ordering problems."
  [m]
  (walk/postwalk
   (fn [node]
     (cond
       (instance? IPersistentMap node) (into {} node)

       (seq? node) (vec node)

       :else node))
   m))

(defn send-request
  "Sends a GraphQL request to the server and returns the response."
  ([query]
   (send-request query nil))
  ([query vars]
   (send-request query vars nil))
  ([query vars headers]
   (send-request query vars headers "http://localhost:8889/graphql"))
  ([query vars headers url]
   (-> {:method :post
        :url url
        :headers (merge {"Content-Type" "application/json"} headers)
        :body (cheshire/generate-string {:query (hash-query query)
                                         :variables vars})
        :throw-exceptions false}
       client/request
       (update :body #(try
                        (cheshire/parse-string % true)
                        (catch Exception _
                          %))))))

;; Websocket test helpers

(def ^:dynamic *websocket* nil)

(defn send-data
  [data]
  (log/debug {:data data} "Sending test data")
  (ws/send-msg (:session *websocket*)
               (cheshire/generate-string data)))

(defn send-init
  ([]
   (send-init nil))
  ([payload]
   (send-data {:type :connection_init
               :payload payload})))

(defn <message!!
  ([]
   (<message!! 100))
  ([timeout-ms]
   (alt!!
     (:message-chan *websocket*) ([message] message)
     (timeout timeout-ms) ::timed-out)))

(defmacro expect-message
  [expected]
  `(is (= ~expected
          (<message!!))))

(def websocketid (atom 0))

(defn open-websocket
  ([] (open-websocket nil))
  ([headers]
   (let [url "ws://localhost:8889/graphql-ws"]
     (log/debug {:url url} "Creating websocket session")
     (let [message-chan (chan 10)
           session (ws/connect
                       url
                     :on-receive
                     (fn [message-text]
                       (log/debug {:message message-text} "Receiving")
                       (put! message-chan (cheshire/parse-string message-text
                                                                 true)))
                     :on-connect (fn [_] (log/debug "Connected"))
                     :on-close #(log/debug {:code %1 :message %2} "Closed")
                     :on-error #(log/error % "Error")
                     :headers headers)]
       {:message-chan message-chan
        :session session
        :id (swap! websocketid inc)}))))

(defn close-websocket
  [ws]
  (log/debug "Closing websocket session")
  (ws/close ws))

(defmacro with-websocket
  [id websocket & body]
  `(binding [*websocket* ~websocket]
     (try
       (let [~id (:id *websocket*)]
         ~@body)
       (finally
         (close-websocket (:session *websocket*))))))

(defn make-session
  "Make a session map for a user."
  [uid]
  {:_fresh true
   :_id "session id"
   :csrf_token "csrf token"
   :language "en"
   :remember_me false
   :user_id uid
   :resets 0})

(defn sign-session-cookie
  "Sign a session cookie for a user."
  [session secret-key]
  (flask-compat/encode-session session secret-key))

(defn sign-csrf-token
  [session & secret-key]
  (flask-compat/sign-csrf-token (:csrf_token session)
                                (or secret-key "test secret")))
(defn request-cookies
  "Make a cookie map for send-request with a session cookie."
  [uid & secret-key]
  (let [session (make-session uid)]
    {"Cookie" (str "session="
                   (sign-session-cookie session
                                        (or secret-key "test secret")))}))

(defn ws-headers
  "Make headers for a websocket request."
  [session & secret-key]
  (let [secret (or secret-key "test secret")]
    {"Cookie"
     (str "session=" (sign-session-cookie session secret))}))

(defn promote-user-to-admin!
  "Promote a user to admin, for testing purposes."
  [system uid]
  (let [ds (get-in system [:db :ds])]
    (jdbc/insert! ds "public.user_metadata"
                  {:uid uid
                   :key "admin"
                   :value "1"})))

(defn ban-user!
  "Site-ban a user, for testing purposes."
  [system uid]
  (let [ds (get-in system [:db :ds])]
    (jdbc/update! ds "public.user"
                  {:status 5}
                  ["uid = ?" uid])))

(defn delete-user!
  "Delete a user, for testing purposes."
  [system uid]
  (let [ds (get-in system [:db :ds])]
    (jdbc/update! ds "public.user"
                  {:status 10}
                  ["uid = ?" uid])))

(def name-counter (atom 0))

(defn mod-user-and-sub
  "Put some things in the database for tests to use."
  [system]
  (let [prefix (str "test-" (swap! name-counter inc))
        mod-name (str prefix "-mod")
        mod2-name (str prefix "-mod2")
        user-name (str prefix "-user")
        admin-name (str prefix "-admin")
        sub-name (str prefix "-sub")
        sub2-name (str prefix "-sub2")
        admin-uid (-> m:create-user
                      (send-request {:name admin-name})
                      (get-in [:body :data :register_user :uid]))
        mod-uid (-> m:create-user
                    (send-request {:name mod-name})
                    (get-in [:body :data :register_user :uid]))
        mod2-uid (-> m:create-user
                     (send-request {:name mod2-name})
                     (get-in [:body :data :register_user :uid]))
        user-uid (-> m:create-user
                     (send-request {:name user-name})
                     (get-in [:body :data :register_user :uid]))
        sid (-> m:create-sub
                (send-request {:name sub-name
                               :title "things to talk about"
                               :nsfw false}
                              (request-cookies mod-uid))
                (get-in [:body :data :create_sub :sid]))
        sid2 (-> m:create-sub
                 (send-request {:name sub2-name
                                :title "some other things"
                                :nsfw false}
                               (request-cookies mod2-uid))
                 (get-in [:body :data :create_sub :sid]))]
    (promote-user-to-admin! system admin-uid)
    {:admin-uid admin-uid
     :admin-name admin-name
     :mod-uid mod-uid
     :mod-name mod-name
     :mod2-uid mod2-uid
     :mod2-name mod2-name
     :user-uid user-uid
     :user-name user-name
     :sid sid
     :sub-name sub-name
     :sid2 sid2
     :sub2-name sub2-name}))
