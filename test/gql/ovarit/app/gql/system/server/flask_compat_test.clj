;; gql/system/server/flask_compat_test.clj -- Testing Flask compatibility for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.flask-compat-test
  (:require
   [clojure.test :refer [deftest testing is]]
   [ovarit.app.gql.system.server.flask-compat :as flask-compat]
   [ring.middleware.cookies :as cookies]))

(def cookie
  (str
   "dayNight=dark; remember_token=015cad85-cdac-44e0-9067-ab195974d05f$1|4deff"
   "dce7ce85c2db24cb0a49f6b4d3ba509c6f4537a39a19a6ecd2fa25e889714bc99298643c61"
   "f2844076018ebaff49a29f51bdacb2c0cfdacbf19ac1933ae; session=.eJwlj0tqxEAMRO"
   "9isoxB_VFLPZcxklqahIwnYI9XIXdPQ5YqeFVPP8sWh58fy-11XP6-bJ9juS0E0BvrkBrGmRUo"
   "WMaAzOYlHJ0JvecyGtCQUTywtZLINEtOuXpTbUmQMxYRGFwDayGKZqLcu5dqpVkU6aVrQC2dUI"
   "2DyqxXW6bIdfrxbwMJTQbjakNsrdVh7dBoFU0dO9UBGG9pMnYesb2-v_w5qamRlCpK5ywBEhQJ"
   "VHOab6lb5qmfcdDkHvK8X3L3Sc3r8N13neP7DEIep__-AX88VTU.X9Awcg.5CCPisQH8Y0Xwsi"
   "dMCzL_z_uYUY"))

(def session-id
  (str "700968bda4fc828b07f8add028ce3fe5e875e923d607dad3ef566317cb2a2124e6bb61"
       "a58253aa0d84f54377f6cab899e34c36cf3a939bf043975bc8f733febc"))

(deftest test-cookie-unpacking
  (testing "A Flask session cookie can be decoded."
    (is (= (-> {:headers {"cookie" cookie}}
               cookies/cookies-request
               :cookies
               (flask-compat/decode-cookies "super secret key"
                                            ;; Permit cookies 60 seconds older than the
                                            ;; timestamp on the cookie above.
                                            (+ 60 (- (quot (System/currentTimeMillis)
                                                           1000)
                                                     1599861397)))
               :decoded-cookies)
           {:session {:_fresh true
                      :_id session-id
                      :csrf_token "1241b745a982af0af7f10bb21bdabec2823d25d7"
                      :language ""
                      :remember_me false
                      :user_id "015cad85-cdac-44e0-9067-ab195974d05f"
                      :resets 1}
            :remember-token {:user_id "015cad85-cdac-44e0-9067-ab195974d05f"
                             :resets 1}}))))

(deftest test-remember-me
  (testing "A missing session cookie can be recreated from a remember token"
    (is (= (-> {:headers {"cookie" cookie}}
               cookies/cookies-request
               :cookies
               (dissoc "session")
               (flask-compat/decode-cookies "super secret key"
                                            ;; Permit cookies 60 seconds older than the
                                            ;; timestamp on the cookie above.
                                            (+ 60 (- (quot (System/currentTimeMillis)
                                                           1000)
                                                     1599861397)))
               :decoded-cookies)
           {:session {:user_id "015cad85-cdac-44e0-9067-ab195974d05f"
                      :resets 1
                      :_fresh false}
            :remember-token {:user_id "015cad85-cdac-44e0-9067-ab195974d05f"
                             :resets 1}}))))

(def signed-csrf-token
  (str "IjRkYTY4NzcwMWQwOWFmMTk4YTUzMzM3ZDZmYmIyZDBhN2M4MDRhMTYi.X34h1w.3cgiJn"
       "7L7yTTfHnNyPvPgQrY0wY"))

(def csrf-token "4da687701d09af198a53337d6fbb2d0a7c804a16")

(deftest test-csrf-signing
  (testing "A CSRF token can be signed correctly."
    (with-redefs [flask-compat/now (fn [] 1602101719)]
      (is (= (flask-compat/sign-csrf-token csrf-token "super secret key")
             signed-csrf-token)))))

(def remember-token
  (str "015cad85-cdac-44e0-9067-ab195974d05f$3|2a242913577aa433916886f9d7df7112"
       "68fc03779c8fda054de95d208815f074b7004c48cc25dfcbd62330ee3ab3c8ee7158690"
       "678e1a6f1c6f1643ef6b4785f"))

(deftest test-remember-cookie-signing
  (testing "A remember me token can be unpacked and resigned")
  (let [token (flask-compat/decode-remember-token remember-token "super secret key")
        encoded (flask-compat/sign-remember-token token "super secret key")]
    (is (= token {:user_id "015cad85-cdac-44e0-9067-ab195974d05f"
                  :resets 3}))
    (is (= remember-token encoded))))
