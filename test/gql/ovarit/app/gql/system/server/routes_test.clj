;; gql/system/server/routes_test.clj -- Testing routes for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.routes-test
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clj-http.client :as client]
   [clojure.string :as str]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [dotenv]
   [ovarit.app.gql.system.server.flask-compat :as compat]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request
                                      request-cookies] :as utils]
   [reaver]))

(def ^:dynamic ^:private *system* nil)
(def ^:dynamic ^:private *fixtures* nil)
(defgraphql graphql "test-graphql/routes.graphql")

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries graphql)
      (assoc-in [:server :site-name] "example.com")
      utils/test-system-map))

(defn fixtures
  []
  (let [result (utils/mod-user-and-sub *system*)
        {:keys [sid user-uid]} result
        rsp (send-request utils/m:create-test-post
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        _ (is (nil? (get-in rsp [:body :errors])))
        pid (get-in rsp [:body :data :create_test_post :pid])]
    (assoc result :pid pid)))

(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (binding [*fixtures* (fixtures)]
          (test-fn))
        (catch Exception e
          (log/error e "error building fixtures"))
        (finally (component/stop-system *system*))))))

(def default-site-config
  "The default site configuration values."
  {"name" "Throat"
   "lema" "Throat: Open discussion ;D"
   "description" "A link aggregator and discussion platform"
   "sub_prefix" "o"
   "copyright" "Umbrella Corp"
   "footer"
   [{"name" "About Ovarit", "link" "/wiki/about"}
    {"name" "Sitewide Rules", "link" "/wiki/rules"}
    {"name" "Sitewide Guidelines", "link" "/wiki/guidelines"}
    {"name" "Terms of Service", "link" "/wiki/tos"}
    {"name" "Privacy Policy", "link" "/wiki/privacy"}
    {"name" "License", "link" "/license"}]
   "last_word" ""
   "logo" nil
   "mottos" []
   "enable_totp" false
   "donate_minimum" 1
   "donate_presets" [5 10 20]
   "funding_goal" 0
   "force_sublog_public" true
   "nsfw_anon_blur" true
   "nsfw_anon_show" true
   "self_vote_posts" true
   "self_vote_comments" false
   "expando_sites" ["youtube.com"
	            "www.youtube.com"
	            "youtu.be"
	            "gfycat.com"
	            "streamja.com"
	            "streamable.com"
	            "player.vimeo.com"
	            "vimeo.com"]
   "title_edit_timeout" 300
   "link_post_remove_timeout" 1800
   "admin_sub" nil})

(def expected-links
  #{{:href "/fe/img/icon.png?v9" :rel "icon" :type "image/png"}
    {:href "/static/img/site.webmanifest" :rel "manifest"}
    {:href "/static/img/favicon-32x32.png"
     :rel "icon"
     :sizes "32x32"
     :type "image/png"}
    {:href "/static/img/favicon-16x16.png"
     :rel "icon"
     :sizes "16x16"
     :type "image/png"}
    {:href "/static/img/apple-touch-icon.png"
     :rel "apple-touch-icon"
     :sizes "180x180"}})

(deftest test-index-page-content-anonymous
  (testing "The client serves the index page with expected content"
    (with-redefs [dotenv/env (fn [_] nil)]
      (doseq [url ["http://localhost:8889/something"
                   "http://localhost:8889/o/sub/not-a-number"]]
        (let [response (client/request {:method :get
                                        :url url
                                        :throw-exceptions false})
              _ (is (= (:status response) 200))
              page (-> (:body response)
                       reaver/parse
                       reaver/to-edn)
              doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
              head (s/select-any [:content s/ALL #(= (:tag %) :head)] doc)
              title (s/select-any [:content s/ALL #(= (:tag %) :title)] head)
              metas (->> head
                         (s/select [:content s/ALL #(= (:tag %) :meta) :attrs])
                         set)
              body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
              css (s/select [:content s/ALL :attrs
                             #(= (:type %) "text/css") :href] head)
              noscript (s/select-any [:content s/ALL #(= (:tag %) :noscript)]
                                     body)
              script (s/select-any [:content s/ALL #(= (:tag %) :script)
                                    :attrs :src] body)
              app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                    body)
              query (s/select-any [:content s/ALL
                                   #(= "data" (get-in % [:attrs :id]))
                                   :attrs :data-value]
                                  app-div)
              parsed-query (cheshire/parse-string query)
              csrf-token (s/select-any
                          [:content s/ALL #(= (:attrs %) {:id "app"})
                           :content s/ALL #(= (get-in % [:attrs :id]) "csrf")
                           :attrs :data-value] body)
              test-secret-key (get-in *system* [:server :secret-key])
              test-session-lifetime 60]
          (is (= (:tag doc) :html))
          (is (= ["Throat: Open discussion ;D"] (:content title)))
          (is (= #{{:charset "utf-8"}
	           {:name "viewport"
                    :content "width=device-width, initial-scale=1.0"}
	           {:name "description"
                    :content "A link aggregator and discussion platform"}
	           {:property "og:description"
                    :content "Throat: Open discussion ;D"}
	           {:property "og:site_name"
                    :content "Throat"}
                   {:name "apple-mobile-web-app-capable"
                    :content "yes"}
	           {:name "msapplication-navbutton-color"
                    :content "white"}
	           {:name "apple-mobile-web-app-status-bar-style"
                    :content "white-translucent"}
	           {:name "theme-color"
                    :content "white"}} metas))
          (is (pos? (count css)))
          (is (every? #(str/starts-with? % "/fe/") css))
          (is (every? #(str/ends-with? % ".css") css))
          (is (= [(str "Throat is a JavaScript app.  "
                       "Please enable JavaScript to continue.")]
                 (:content noscript)))
          (is (str/starts-with? script "/fe/"))
          (is (str/ends-with? script ".js"))
          (is (= {"data" {"current_user" nil
                          "site_configuration" default-site-config
                          "default_subs" []
                          "funding_progress" nil
                          "announcement_post_id" nil}}
                 parsed-query))

          (testing "with a csrf token in the page and the session cookie"
            (let [session-cookie (get-in response [:cookies "session" :value])
                  session (compat/decode-session session-cookie
                                                 test-secret-key
                                                 test-session-lifetime)]
              (is (string? (:csrf_token session)))
              (is (= (compat/decode-csrf-token csrf-token
                                               test-secret-key
                                               test-session-lifetime)
                     (:csrf_token session))))))))))

(deftest test-index-page-content-with-authorization
  (testing "The client serves the index page to a user with the user's data"
    (with-redefs [dotenv/env (fn [_] nil)]
      (let [{:keys [user-uid user-name]} (utils/mod-user-and-sub *system*)
            response (client/request {:method :get
                                      :url "http://localhost:8889/some/page"
                                      :headers (request-cookies user-uid)
                                      :throw-exceptions false})
            _ (is (= (:status response) 200))
            page (-> (:body response)
                     reaver/parse
                     reaver/to-edn)
            doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
            body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
            app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                  body)
            query (s/select-any [:content s/ALL
                                 #(= "data" (get-in % [:attrs :id]))
                                 :attrs :data-value]
                                app-div)
            parsed-query (cheshire/parse-string query)]
        (is (=  {"data" {"current_user"
                         {"name" user-name
                          "uid" user-uid
                          "language" nil,
                          "score" 0,
                          "unread_notification_count" 0,
                          "unread_message_count" 0,
                          "attributes" {"can_admin" false
                                        "totp_expiration" nil
                                        "nsfw" false
                                        "nsfw_blur" false
                                        "enable_collapse_bar" true}
                          "subscriptions" [],
                          "subs_moderated" []},
                         "site_configuration" default-site-config
                         "funding_progress" 0
                         "announcement_post_id" nil
                         "default_subs" []}}
                parsed-query))))))

(deftest test-index-page-content-by-mod
  (testing "The client serves the index page to a mod with subs moderated."
    (with-redefs [dotenv/env (fn [_] nil)]
      (let [{:keys [mod-uid mod-name sid
                    sub-name]} (utils/mod-user-and-sub *system*)
            response (client/request {:method :get
                                      :url "http://localhost:8889/some/page"
                                      :headers (request-cookies mod-uid)
                                      :throw-exceptions false})
            _ (is (= (:status response) 200))
            page (-> (:body response)
                     reaver/parse
                     reaver/to-edn)
            doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
            body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
            app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                  body)
            query (s/select-any [:content s/ALL
                                 #(= "data" (get-in % [:attrs :id]))
                                 :attrs :data-value]
                                app-div)
            parsed-query (cheshire/parse-string query)]
        (is (=  {"data" {"current_user"
                         {"name" mod-name
                          "uid" mod-uid
                          "language" nil
                          "score" 0
                          "unread_notification_count" 0
                          "unread_message_count" 0
                          "attributes" {"can_admin" false
                                        "totp_expiration" nil
                                        "nsfw" false
                                        "nsfw_blur" false
                                        "enable_collapse_bar" true}
                          "subscriptions" [{"name" sub-name
                                            "status" "SUBSCRIBED"
                                            "order" nil}]
                          "subs_moderated"
                          [{"sub"
	                    {"name" sub-name
	                     "sid" sid
	                     "open_report_count" 0
	                     "unread_modmail_count" 0
	                     "all_unread_modmail_count" 0
	                     "in_progress_unread_modmail_count" 0
	                     "notification_unread_modmail_count" 0
                             "discussion_unread_modmail_count" 0
                             "new_unread_modmail_count" 0}
	                    "moderation_level" "OWNER"}]}
                         "site_configuration" default-site-config
                         "funding_progress" 0
                         "announcement_post_id" nil
                         "default_subs" []}}
                parsed-query))))))

(deftest test-index-page-content-by-admin
  (testing "The client serves the index page to an admin."
    (with-redefs [dotenv/env (fn [_] nil)]
      (let [{:keys [admin-uid admin-name]} (utils/mod-user-and-sub *system*)
            response (client/request {:method :get
                                      :url "http://localhost:8889/some/page"
                                      :headers (request-cookies admin-uid)
                                      :throw-exceptions false})
            _ (is (= (:status response) 200))
            page (-> (:body response)
                     reaver/parse
                     reaver/to-edn)
            doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
            body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
            app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                  body)
            query (s/select-any [:content s/ALL
                                 #(= "data" (get-in % [:attrs :id]))
                                 :attrs :data-value]
                                app-div)
            parsed-query (cheshire/parse-string query)]
        (is (=  {"data" {"current_user"
                         {"name" admin-name
                          "uid" admin-uid
                          "language" nil
                          "score" 0
                          "unread_notification_count" 0
                          "unread_message_count" 0
                          "attributes" {"can_admin" true
                                        "totp_expiration" nil
                                        "nsfw" false
                                        "nsfw_blur" false
                                        "enable_collapse_bar" true}
                          "subscriptions" []
                          "subs_moderated" []}
                         "site_configuration" default-site-config
                         "funding_progress" 0
                         "announcement_post_id" nil
                         "default_subs" []}}
                parsed-query))))))

(deftest test-single-post-page-content-anon
  (with-redefs [dotenv/env (fn [k]
                             (get {:SITE_SERVER_NAME "example.com"}
                                  (keyword k)))]
    (let [{:keys [mod-name sid sub-name user-name user-uid pid]} *fixtures*
          urls (->> [["http://localhost:8889/o/" sub-name "/" pid]
                     ["http://localhost:8889/o/" sub-name "/" pid "/_"]
                     ["http://localhost:8889/o/" sub-name "/" pid "/test-post"]
                     ["http://localhost:8889/o/" sub-name "/" pid "/_/somecid"]
                     ["http://localhost:8889/o/" sub-name "/" pid
                      "/test-post/somecid"]]
                    (map #(apply str %)))]
      (doseq [url urls]
        (let [response (client/request {:method :get
                                        :url url
                                        :throw-exceptions false})
              _ (is (= 200 (:status response)))
              page (-> (:body response)
                       reaver/parse
                       reaver/to-edn)
              doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
              head (s/select-any [:content s/ALL #(= (:tag %) :head)] doc)
              title (s/select-any [:content s/ALL #(= (:tag %) :title)] head)
              metas (->> head
                         (s/select [:content s/ALL #(= (:tag %) :meta) :attrs])
                         set)
              links (s/select [:content s/ALL #(= (:tag %) :link) :attrs] head)
              css (s/select [s/ALL #(= (:type %) "text/css") :href] links)
              non-css-links (->> links
                                 (filter #(not= "text/css" (:type %)))
                                 set)
              body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
              noscript (s/select-any [:content s/ALL #(= (:tag %) :noscript)]
                                     body)
              script (s/select-any [:content s/ALL #(= (:tag %) :script)
                                    :attrs :src] body)
              app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                    body)
              query (-> (s/select-any [:content s/ALL
                                       #(= "data" (get-in % [:attrs :id]))
                                       :attrs :data-value]
                                      app-div)
                        cheshire/parse-string)
              queries (-> (s/select-any [:content s/ALL
                                         #(= "queries" (get-in % [:attrs :id]))
                                         :attrs :data-value]
                                        app-div)
                          cheshire/parse-string)]
          (is (= (:tag doc) :html))
          (is (= [(str "Test post | " sub-name)] (:content title)))
          (is (= #{{:charset "utf-8"}
                   {:name "viewport"
                    :content "width=device-width, initial-scale=1.0"}
                   {:name "description"
                    :content (str "Posted in " sub-name " by " user-name)}
                   {:property "og:site_name"
                    :content "Throat"}
                   {:property "og:description"
                    :content (str "Posted in " sub-name " by " user-name)}
                   {:property "og:title"
                    :content "Test post"}
	           {:property "og:type"
                    :content "article"}
                   {:name "apple-mobile-web-app-capable"
                    :content "yes"}
	           {:name "msapplication-navbutton-color"
                    :content "white"}
	           {:name "apple-mobile-web-app-status-bar-style"
                    :content "white-translucent"}
	           {:name "theme-color"
                    :content "white"}}
                 metas))
          (is (= expected-links non-css-links))
          (is (pos? (count css)))
          (is (every? #(str/starts-with? % "/fe/") css))
          (is (every? #(str/ends-with? % ".css") css))
          (is (= [(str "Throat is a JavaScript app.  "
                       "Please enable JavaScript to continue.")]
                 (:content noscript)))
          (is (str/starts-with? script "/fe/"))
          (is (str/ends-with? script ".js"))
          (is (= {"data" {"current_user" nil
                          "site_configuration" default-site-config
                          "default_subs" []
                          "funding_progress" nil
                          "announcement_post_id" nil}}
                 query))
          (let [qresult (first queries)
                posted (get-in qresult ["result" "data" "post_by_pid" "posted"])
                creation (get-in qresult ["result" "data" "post_by_pid" "sub"
                                          "creation"])]
            (is (string? posted))
            (is (string? creation))
            (is (= {"query" "get-single-post"
                    "variables" {"pid" pid
                                 "is_mod" false
                                 "is_authenticated" false}
                    "result" {"data"
	                      {"post_by_pid"
	                       {"author" {"name" user-name
	                                  "uid" user-uid
	                                  "status" "ACTIVE"}
	                        "author_flair" nil
	                        "best_sort_enabled" true
	                        "comment_count" 0
	                        "content" "testify"
	                        "default_sort" "BEST"
	                        "distinguish" nil
	                        "downvotes" 0
	                        "edited" nil
	                        "flair" nil
	                        "hide_results" false
	                        "is_archived" false
	                        "link" nil
	                        "locked" false
	                        "nsfw" false
	                        "pid" "1"
	                        "poll_closes_time" nil
	                        "poll_open" nil
	                        "poll_options" nil
	                        "poll_votes" 0
                                "posted" posted
	                        "score" 1
                                "slug" "test-post"
	                        "status" "ACTIVE"
	                        "sticky" false
	                        "sub" {"creation" creation
                                       "creator" {"name" mod-name
                                                  "status" "ACTIVE"}
	                               "freeform_user_flairs" false
	                               "moderators" [{"mod" {"name" mod-name
                                                             "status" "ACTIVE"}
	                                              "moderation_level"
                                                      "OWNER"}]
	                               "name" sub-name
	                               "nsfw" false
	                               "post_count" 0
	                               "post_flairs" []
	                               "restricted" false
	                               "sidebar" ""
	                               "sub_banned_users_private" false
	                               "sublog_private" false
	                               "subscriber_count" 1
                                       "text_post_min_length" 0
	                               "title" "things to talk about"
	                               "user_can_flair" false
	                               "user_can_flair_self" false
	                               "user_must_flair" false
                                       "sid" sid
                                       "user_flair_choices" nil}
                                "title_edited" nil
	                        "thumbnail" nil
	                        "title" "Test post"
	                        "type" "TEXT"
	                        "upvotes" 1}}}}
                   qresult))))))))

(deftest test-single-post-page-not-exists-content-anon
  (with-redefs [dotenv/env (fn [k]
                             (get {:SITE_SERVER_NAME "example.com"}
                                  (keyword k)))]
    (let [{:keys [sub-name]} *fixtures*
          pid 9999
          urls (->> [["http://localhost:8889/o/" sub-name "/" pid]
                     ["http://localhost:8889/o/" sub-name "/" pid "/_"]
                     ["http://localhost:8889/o/" sub-name "/" pid "/test-post"]
                     ["http://localhost:8889/o/" sub-name "/" pid "/_/somecid"]
                     ["http://localhost:8889/o/" sub-name "/" pid
                      "/test-post/somecid"]]
                    (map #(apply str %)))]
      (doseq [url urls]
        (let [response (client/request {:method :get
                                        :url url
                                        :throw-exceptions false})
              _ (is (= 200 (:status response)))
              page (-> (:body response)
                       reaver/parse
                       reaver/to-edn)
              doc (s/select-any [:content s/ALL #(= (:type %) :element)] page)
              head (s/select-any [:content s/ALL #(= (:tag %) :head)] doc)
              title (s/select-any [:content s/ALL #(= (:tag %) :title)] head)
              metas (->> head
                         (s/select [:content s/ALL #(= (:tag %) :meta) :attrs])
                         set)
              links (s/select [:content s/ALL #(= (:tag %) :link) :attrs] head)
              css (s/select [s/ALL #(= (:type %) "text/css") :href] links)
              non-css-links (->> links
                                 (filter #(not= "text/css" (:type %)))
                                 set)
              body (s/select-any [:content s/ALL #(= (:tag %) :body)] doc)
              noscript (s/select-any [:content s/ALL #(= (:tag %) :noscript)]
                                     body)
              script (s/select-any [:content s/ALL #(= (:tag %) :script)
                                    :attrs :src] body)
              app-div (s/select-any [:content s/ALL #(= (:attrs %) {:id "app"})]
                                    body)
              query (-> (s/select-any [:content s/ALL
                                       #(= "data" (get-in % [:attrs :id]))
                                       :attrs :data-value]
                                      app-div)
                        cheshire/parse-string)
              queries (-> (s/select-any [:content s/ALL
                                         #(= "queries" (get-in % [:attrs :id]))
                                         :attrs :data-value]
                                        app-div)
                          cheshire/parse-string)]
          (is (= (:tag doc) :html))
          (is (= ["Throat: Open discussion ;D"] (:content title)))
          (is (= #{{:charset "utf-8"}
                   {:name "viewport"
                    :content "width=device-width, initial-scale=1.0"}
                   {:name "description"
                    :content  "A link aggregator and discussion platform"}
                   {:property "og:site_name"
                    :content "Throat"}
                   {:property "og:description"
                    :content "Throat: Open discussion ;D"}
                   {:content "yes" :name "apple-mobile-web-app-capable"}
	           {:content "white" :name "msapplication-navbutton-color"}
	           {:content "white-translucent"
	            :name "apple-mobile-web-app-status-bar-style"}
                   {:content "white" :name "theme-color"}}
                 metas))
          (is (= expected-links non-css-links))
          (is (pos? (count css)))
          (is (every? #(str/starts-with? % "/fe/") css))
          (is (every? #(str/ends-with? % ".css") css))
          (is (= [(str "Throat is a JavaScript app.  "
                       "Please enable JavaScript to continue.")]
                 (:content noscript)))
          (is (str/starts-with? script "/fe/"))
          (is (str/ends-with? script ".js"))
          (is (= {"data" {"current_user" nil
                          "site_configuration" default-site-config
                          "default_subs" []
                          "funding_progress" nil
                          "announcement_post_id" nil}}
                 query))
          (let [qresult (first queries)]
            (is (= {"pid" (str pid)
                    "is_mod" false
                    "is_authenticated" false}
                   (get qresult "variables")))
            (is (= {"post_by_pid" nil}
                   (get-in qresult ["result" "data"])))
            (is (= "Not found"
                   (get-in qresult ["result" "errors" 0 "message"])))))))))

(comment
  (deftest test-load-site-config
    (testing "Test performance of the system under load."
      (with-redefs [dotenv/env (fn [_] nil)]
        (let [start-ts (.getTime (java.util.Date.))
              {:keys [user-uid]} *fixtures*
              responses (doall
                         (map (fn [_]
                                (<!! (timeout 1))
                                (future
                                  (send-request (-> graphql :query
                                                    :current-user)
                                                nil
                                                (request-cookies user-uid))))
                              (range 1000)))
              success (count (filter (fn [r]
                                       (= 200 (:status @r))) responses))
              statuses (map #(:status @%) responses)
              end-ts (.getTime (java.util.Date.))]
          (is (= (count responses) success))
          #_(is (nil? statuses))
          (println "Time taken:" (- end-ts start-ts))
          (println "Average:" (/ (- end-ts start-ts) (* 1.0 success))))))))
