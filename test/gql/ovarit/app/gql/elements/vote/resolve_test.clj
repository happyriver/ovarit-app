;; gql/elements/vote/resolve_test.clj -- Test vote resolvers for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.vote.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [java-time.api :as jt]
   [ovarit.app.gql.protocols.tasks :as tasks]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request] :as utils]))

(def ^:private clock (jt/mock-clock 0 "UTC"))
(def ^:dynamic ^:private *system* nil)
(defgraphql graphql
  "test-graphql/vote.graphql"
  "test-graphql/shared.graphql")

(defn- test-taskrunner [taskrunner]
  ;; Put the admin remove votes task in test mode for
  ;; `test-admin-remove-votes`.
  (let [admin-remove-votes? #(= (:task-name %) :admin-remove-votes)]
    (->> taskrunner
         (s/setval [:tasks s/ALL admin-remove-votes? :options :test-mode?] true)
         (s/setval [:tasks s/ALL admin-remove-votes? :options :batch-size] 6))))

(defn system-map []
  (-> (utils/test-system-config-map #{:webserver :taskrunner})
      (assoc-in [:server :clock] clock)
      (assoc-in [:job-queue :clock] clock)
      (utils/add-queries graphql)
      utils/test-system-map
      utils/disable-refresh-site-config
      (update :taskrunner test-taskrunner)))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-comment-vote-error-conditions
  (testing "comment voting fails"
    (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title "Test post"
                             :content "testify"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            (utils/request-cookies user-uid))
          pid (-> rsp :body :data :create_test_post :pid)
          rsp (send-request (-> graphql :mutation :create-comment)
                            {:content "Commentary"
                             :pid pid}
                            (utils/request-cookies mod-uid))
          _ (is (= 200 (:status rsp)))
          _ (is (nil? (-> rsp :body :errors)))
          cid (get-in rsp [:body :data :create_comment :cid])]
      (testing "for unauthenticated users"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "UP"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
      (testing "if comment does not exist"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid "hello"
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Not found" (get-in rsp [:body :errors 0 :message])))))
      (testing "if it would make user net votes go negative"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "DOWN"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Too many downvotes"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "if the comment has been deleted"
        (jdbc/update! (:ds (:db *system*)) "sub_post_comment"
                      {:status 1} ["cid = ?" cid])
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Comment deleted"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-post-vote-error-conditions
  (testing "post voting fails"
    (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title "Test post"
                             :content "testify"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            (utils/request-cookies mod-uid))
          pid (-> rsp :body :data :create_test_post :pid)]
      (testing "for unauthenticated users"
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid pid
                                 :vote "UP"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
      (testing "if post does not exist"
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid "10000"
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Not found" (get-in rsp [:body :errors 0 :message])))))
      (testing "if it would make user net votes go negative"
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid pid
                                 :vote "DOWN"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Too many downvotes"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "if the post has been deleted"
        (let [rsp (send-request (-> graphql :mutation :remove-post)
                                {:pid pid}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp))))
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Post deleted" (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-votes-by-banned-users
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies mod-uid))
        pid (-> rsp :body :data :create_test_post :pid)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "Commentary"
                           :pid pid}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        _ (is (nil? (-> rsp :body :errors)))
        cid (get-in rsp [:body :data :create_comment :cid])]
    (testing "banned users"
      (jdbc/insert! (:ds (:db *system*)) "sub_ban"
                    {:uid user-uid
                     :sid sid
                     :reason "banning"
                     :effective true
                     :created_by_id mod-uid})
      (testing "cannot make post votes"
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
      (testing "cannot make comment votes"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-votes-on-archived-content
  (testing "posts older than the archive date"
    (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
          days (-> (jdbc/query (:ds (:db *system*))
                               [(str "select value from site_metadata "
                                     "where key = 'site.archive_post_after'")])
                   first :value parse-long)
          sticky (-> (jdbc/query
                      (:ds (:db *system*))
                      [(str "select value from site_metadata "
                            "where key = 'site.archive_sticky_posts'")])
                     first :value)
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title "Test post"
                             :content "testify"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            (utils/request-cookies mod-uid))
          pid (-> rsp :body :data :create_test_post :pid)
          rsp (send-request (-> graphql :mutation :create-comment)
                            {:content "Commentary"
                             :pid pid}
                            (utils/request-cookies mod-uid))
          _ (is (= 200 (:status rsp)))
          _ (is (nil? (-> rsp :body :errors)))
          cid (get-in rsp [:body :data :create_comment :cid])]
      (jt/advance-clock! clock (jt/plus (jt/days (+ days 5))))
      (testing "can't be voted on"
        (let [rsp (send-request (-> graphql :mutation :vote-post)

                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Post archived" (get-in rsp [:body :errors 0 :message])))))
      (testing "don't allow comment voting"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Post archived" (get-in rsp [:body :errors 0 :message])))))
      (testing "when marked sticky"
        (jdbc/update! (:ds (:db *system*)) "site_metadata"
                      {:value "0"} ["key = 'site.archive_sticky_posts'"])
        (let [rsp (send-request (-> graphql :mutation :stick-post)
                                {:pid pid
                                 :sid sid}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp))))
        (testing "can be voted on"
          (let [rsp (send-request (-> graphql :mutation :vote-post)
                                  {:pid pid
                                   :vote "UP"}
                                  (utils/request-cookies user-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            ;; The score is 2 because self-voting is the default for posts.
            (is (= {:cast_post_vote {:score 2}} data))))
        (testing "allow comment voting"
          (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                  {:cid cid
                                   :vote "UP"}
                                  (utils/request-cookies user-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            ;; Self-voting is by default off for comments.
            (is (= {:cast_comment_vote {:score 1}} data))))
        (jdbc/update! (:ds (:db *system*)) "site_metadata"
                      {:value sticky} ["key = 'site.archive_sticky_posts'"])))))

(deftest test-self-voting-disallowed
  (testing "when self-voting is not allowed"
    (let [ds (-> *system* :db :ds)
          {:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
          settings (jdbc/query ds
                               ["select key, value
                                   from site_metadata
                                  where key in ('site.self_voting.posts',
                                                'site.self_voting.comments')"])
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title "Test post"
                             :content "testify"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            (utils/request-cookies user-uid))
          pid (-> rsp :body :data :create_test_post :pid)
          rsp (send-request (-> graphql :mutation :create-comment)
                            {:content "Commentary"
                             :pid pid}
                            (utils/request-cookies user-uid))
          cid (get-in rsp [:body :data :create_comment :cid])]
      (jdbc/update! ds "site_metadata" {:value "0"}
                    ["key in ('site.self_voting.posts',
                              'site.self_voting.comments')"])
      (testing "users cannot vote on their own posts"
        (let [rsp (send-request (-> graphql :mutation :vote-post)
                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "No self-voting" (get-in rsp [:body :errors 0 :message])))))
      (testing "users cannot vote on their own comments"
        (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                {:cid cid
                                 :vote "UP"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "No self-voting" (get-in rsp [:body :errors 0 :message])))))
      (run! (fn [{:keys [key value]}]
              (jdbc/update! ds "site_metadata"
                            {:value value}
                            ["key = ?" key])) settings))))

(deftest test-vote-score-updates
  (testing "score updates are sent to the author"
    (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
          user-session (utils/make-session user-uid)
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title "Test post"
                             :content "testify"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            (utils/request-cookies user-uid))
          pid (-> rsp :body :data :create_test_post :pid)
          rsp (send-request (-> graphql :mutation :create-comment)
                            {:content "Commentary"
                             :pid pid}
                            (utils/request-cookies user-uid))
          cid (-> rsp :body :data :create_comment :cid)]
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription :user-score
                                               utils/hash-query)}})
        (testing "on post vote"
          (send-request (-> graphql :mutation :vote-post)
                        {:pid pid :vote "UP"}
                        (utils/request-cookies mod-uid))
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:user_score_update 1}}})
          (testing "reversal"
            (send-request (-> graphql :mutation :vote-post)
                          {:pid pid :vote "DOWN"}
                          (utils/request-cookies mod-uid))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:user_score_update -1}}}))
          (testing "deletion"
            (send-request (-> graphql :mutation :vote-post) {:pid pid :vote nil}
                          (utils/request-cookies mod-uid))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:user_score_update 0}}})))
        (testing "on comment vote"
          (send-request (-> graphql :mutation :vote-comment)
                        {:cid cid :vote "UP"}
                        (utils/request-cookies mod-uid))
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:user_score_update 1}}})
          (testing "reversal"
            (send-request (-> graphql :mutation :vote-comment)
                          {:cid cid :vote "DOWN"}
                          (utils/request-cookies mod-uid))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:user_score_update -1}}}))
          (testing "deletion"
            (send-request (-> graphql :mutation :vote-comment)
                          {:cid cid :vote nil}
                          (utils/request-cookies mod-uid))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:user_score_update 0}}})))))))

(deftest test-mods-get-vote-breakdown
  (let [{:keys [user-uid mod-uid mod2-uid
                sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        pid (-> rsp :body :data :create_test_post :pid)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "Commentary"
                           :pid pid}
                          (utils/request-cookies user-uid))
        cid (-> rsp :body :data :create_comment :cid)]
    (testing "mods get upvote and downvote fields"
      (testing "on post votes"
        (let [rsp (send-request (-> graphql :mutation :vote-post-breakdown)
                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          ;; The score is 2 because self-voting is the default for posts.
          (is (= {:cast_post_vote {:score 2 :upvotes 2 :downvotes 0}} data))))
      (testing "on comment votes"
        (let [rsp (send-request (-> graphql :mutation :vote-comment-breakdown)
                                {:cid cid
                                 :vote "DOWN"}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          ;; Self-voting is by default off for comments.
          (is (= {:cast_comment_vote {:score -1
                                      :upvotes 0
                                      :downvotes 1}}
                 data)))))
    (testing "non-mods can't see upvote and downvote fields"
      (testing "on post votes"
        (let [rsp (send-request (-> graphql :mutation :vote-post-breakdown)
                                {:pid pid
                                 :vote "UP"}
                                (utils/request-cookies mod2-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in errors [0 :message])))
          (is (nil? (-> data :cast_post_vote :upvotes)))))
      (testing "on comment votes"
        (let [rsp (send-request (-> graphql :mutation :vote-comment-breakdown)
                                {:cid cid
                                 :vote "DOWN"}
                                (utils/request-cookies mod2-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in errors [0 :message])))
          (is (nil? (-> data :comment_post_vote :upvotes))))))))

(defn make-some-votes
  [test-data]
  (let [{:keys [user-uid admin-uid sid]} test-data
        user-cookies (utils/request-cookies user-uid)
        admin-cookies (utils/request-cookies admin-uid)
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post"
                           :content "test content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          admin-cookies)
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_test_post :pid])
        rsp (send-request (-> graphql :mutation :vote-post)
                          {:pid pid
                           :vote "UP"}
                          (utils/request-cookies user-uid))
        _ (jt/advance-clock! clock (jt/seconds 1))
        _ (is (= 200 (:status rsp)))
        cids (doall
              (map #(let [rsp (send-request (-> graphql :mutation
                                                :create-comment)
                                            {:content (str "comment " %)
                                             :pid pid}
                                            admin-cookies)
                          _ (is (nil? (-> rsp :body :errors)))
                          cid (get-in rsp [:body :data :create_comment :cid])
                          rsp (send-request (-> graphql :mutation :vote-comment)
                                            {:cid cid
                                             :vote (if (= 2 (mod % 3))
                                                     "DOWN" "UP")}
                                            user-cookies)]
                      (is (= 200 (:status rsp)))
                      (is (nil? (-> rsp :body :errors)))
                      (jt/advance-clock! clock (jt/seconds 1))
                      cid)
                   (range 10)))]
    [pid cids]))

(deftest test-admin-votes-query
  (testing "admins can fetch"
    (let [test-data (utils/mod-user-and-sub *system*)
          {:keys [user-name admin-uid admin-name sub-name]} test-data
          instant (jt/with-clock clock
                    (-> (jt/instant) jt/to-millis-from-epoch))
          [pid cids] (make-some-votes test-data)
          admin-cookies (utils/request-cookies admin-uid)
          rsp (send-request (-> graphql :query :votes)
                            {:name user-name
                             :types ["COMMENT"]
                             :first 5
                             :after nil}
                            admin-cookies)
          {:keys [data errors]} (:body rsp)
          items (-> data :votes :edges)]
      (testing "a page of votes"
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= 5 (count items)))
        (is (= {:direction "UP"
                :datetime (str (+ instant 10000))
                :comment {:cid (last cids)
                          :post {:pid pid
                                 :sub {:name sub-name}}
                          :author {:uid admin-uid
                                   :name admin-name}}}
               (-> data :votes :edges first :node)))
        (is (= (set (take-last 5 cids))
               (set (map #(-> % :node :comment :cid) items))))
        (is (-> data :votes :pageInfo :hasNextPage)))
      (let [rsp (send-request (-> graphql :query :votes)
                              {:name user-name
                               :types ["COMMENT"]
                               :first 5
                               :after (-> data :votes :pageInfo :endCursor)}
                              admin-cookies)
            {:keys [data errors]} (:body rsp)
            votes (:votes data)
            items2 (:edges votes)]
        (testing "a second page of votes"
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= 5 (count items)))
          (is (= (set (take 5 cids))
                 (set (map #(-> % :node :comment :cid) items2))))
          (is (false? (-> votes :pageInfo :hasNextPage))))
        (let [rsp (send-request (-> graphql :query :votes)
                                {:name user-name
                                 :types ["COMMENT"]
                                 :last 5
                                 :before (-> votes :pageInfo :startCursor)}
                                admin-cookies)
              {:keys [data errors]} (:body rsp)
              votes (:votes data)
              items3 (:edges votes)]
          (testing "a previous page of votes"
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= 5 (count items)))
            (is (= items items3))
            (is (false? (-> votes :pageInfo :hasPreviousPage)))
            (is (true? (-> votes :pageInfo :hasNextPage))))))
      (testing "post votes only"
        (let [rsp (send-request (-> graphql :query :votes)
                                {:name user-name
                                 :types ["POST"]
                                 :first 10}
                                admin-cookies)
              {:keys [data errors]} (:body rsp)
              votes (:votes data)
              items (:edges votes)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid pid, :slug "test-post", :sub {:name sub-name}}
                 (-> items first :node :post)))
          (is (false? (-> votes :pageInfo :hasPreviousPage)))
          (is (false? (-> votes :pageInfo :hasNextPage)))))
      (testing "both post and comment votes"
        (let [rsp (send-request (-> graphql :query :votes)
                                {:name user-name
                                 :types ["POST" "COMMENT"]
                                 :first 11}
                                admin-cookies)
              {:keys [data errors]} (:body rsp)
              votes (:votes data)
              items (:edges votes)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid pid, :slug "test-post", :sub {:name sub-name}}
                 (-> items last :node :post)))
          ;; The nil is because the post vote doesn't have a cid.
          (is (= (set (conj cids nil))
                 (set (map #(-> % :node :comment :cid) items))))
          (is (false? (-> votes :pageInfo :hasPreviousPage)))
          (is (false? (-> votes :pageInfo :hasNextPage))))))))

(deftest test-admin-remove-votes
  (let [test-data (utils/mod-user-and-sub *system*)
        {:keys [user-name admin-uid sid]} test-data
        [pid cids] (make-some-votes test-data)
        admin-cookies (utils/request-cookies admin-uid)
        admin-session (utils/make-session admin-uid)]
    (testing "admins can"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers admin-session))
        (utils/send-init {:token (utils/sign-csrf-token admin-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql :subscription
                                               :vote-removal-update
                                               utils/hash-query)
                                    :variables {:name user-name}}})

        (testing "use a subscription to check vote removal status "
          ;; Get the first websocket message, indicating that vote removal
          ;; is not in progress.
          (is (= {:type "data"
	          :id id
	          :payload {:data
	                    {:vote_removal_update {:total nil :remaining nil}}}}
                 (utils/<message!! 1000))))

        (testing "remove votes"
          (send-request (-> graphql :mutation :start-remove-votes)
                        {:name user-name}
                        admin-cookies)

          ;; The vote removal task is configured in the test fixture to
          ;; run only when called and to make 6 attempts to remove votes.
          (tasks/testrun (:taskrunner *system*) :admin-remove-votes)

          (testing "and subscribe to a progress report"
            ;; Get the progress message indicating votes have been removed.
            ;; We started with 11 votes. One of the removal attempts will be
            ;; a retry after all post votes are gone, so the number of votes
            ;; remaining should be 7.
            (is (= {:type "data"
	            :id id
	            :payload {:data
                              {:vote_removal_update {:remaining 7 :total 11}}}}
                   (utils/<message!! 1000)))))

        (testing "stop a vote removal in progress"
          (send-request (-> graphql :mutation :stop-remove-votes)
                        {:name user-name}
                        admin-cookies)

          (testing "and subscribe to a status report"
            ;; Get the message that vote removal is no longer in progress.
            (is (= {:type "data"
	            :id id
	            :payload {:data
	                      {:vote_removal_update {:total nil
                                                     :remaining nil}}}}
                   (utils/<message!! 1000)))))

        (testing "remove all votes"
          (send-request (-> graphql :mutation :start-remove-votes)
                        {:name user-name}
                        admin-cookies)

          ;; This pass will delete 4, because the first two iterations of
          ;; the loop look at post votes, and there aren't any.
          (tasks/testrun (:taskrunner *system*) :admin-remove-votes)

          (is (= {:type "data"
	          :id id
	          :payload {:data
                            {:vote_removal_update {:remaining 3 :total 7}}}}
                 (utils/<message!! 1000))))

        (testing "and subscribe to a report when removal is complete"
          ;; Complete the vote removal.
          (tasks/testrun (:taskrunner *system*) :admin-remove-votes)
          (is (= {:type "data"
	          :id id
	          :payload {:data
	                    {:vote_removal_update {:total nil :remaining nil}}}}
                 (utils/<message!! 1000))))

        (testing "query the results of vote removal"
          (is (= {:score 1
                  :upvotes 1
                  :downvotes 0
                  :author {:score 0}}
                 (-> (send-request (-> graphql :query :post-and-author-scores)
                                   {:pid pid} admin-cookies)
                     :body :data :post_by_pid)))
          (is (= (repeat 10 [0 0 0])
                 (->> (send-request (-> graphql :query :comment-scores)
                                    {:sid sid :cids cids} admin-cookies)
                      :body :data :comments_by_cid
                      (map (juxt :score :upvotes :downvotes)))))
          (is (= {:given 0 :upvotes_given 0 :downvotes_given 0}
                 (-> (send-request (-> graphql :query :user-votes-given)
                                   {:name user-name} admin-cookies)
                     :body :data :user_by_name))))))))

(deftest admin-downvote-notifications
  (testing "notifications are sent to mods on excessive downvotes"
    (let [test-data (utils/mod-user-and-sub *system*)
          {:keys [user-name user-uid mod-uid mod2-uid mod2-name
                  sid sub-name]} test-data
          poster (utils/request-cookies mod2-uid)
          mod-session (utils/make-session mod-uid)
          title  "Test post"
          rsp (send-request (-> graphql :mutation :create-test-post)
                            {:title title
                             :content "test content"
                             :sid sid
                             :nsfw false
                             :type :TEXT}
                            poster)
          _ (is (= 200 (:status rsp)))
          pid (get-in rsp [:body :data :create_test_post :pid])
          rsp (send-request (-> graphql :mutation :vote-post)
                            {:pid pid
                             :vote "UP"}
                            (utils/request-cookies user-uid))
          _ (is (= 200 (:status rsp)))
          _ (jt/advance-clock! clock (jt/seconds 1))
          cids (doall
                (map #(let [rsp (send-request (-> graphql :mutation
                                                  :create-comment)
                                              {:content (str "comment " %)
                                               :pid pid}
                                              poster)
                            _ (is (= 200 (:status rsp)))
                            _ (is (nil? (-> rsp :body :errors)))
                            cid (get-in rsp [:body :data :create_comment :cid])]
                        (jt/advance-clock! clock (jt/seconds 1))
                        cid)
                     (range 20)))]
      (jdbc/insert! (:ds (:db *system*)) "site_metadata"
                    {:key "site.admin_sub" :value sub-name})
      (doseq [[k v] {"site.downvotes.personal.count"  5
                     "site.downvotes.personal.days"   1
                     "site.downvotes.count"           6
                     "site.downvotes.days"            1
                     "site.downvotes.silence"         1
                     "site.downvotes.thread.percent"  40
                     "site.downvotes.thread.comments" 20}]
        (jdbc/update! (:ds (:db *system*)) "site_metadata"
                      {:value (str v)} ["key = ?" k]))
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers mod-session))
        (utils/send-init {:token (utils/sign-csrf-token mod-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql :subscription
                                               :notification-counts
                                               utils/hash-query)
                                    :variables {}}})

        (doseq [[num cid] (map-indexed list cids)]
          ;; upvote the first 10 comments so we keep our given count >= 0
          (let [rsp (send-request (-> graphql :mutation :vote-comment)
                                  {:cid cid
                                   :vote (if (< num 10) "UP" "DOWN")}
                                  (utils/request-cookies user-uid))
                {:keys [errors]} (:body rsp)
                notification (fn [val]
                               {:id id :type "data"
                                :payload
	                        {:data
	                         {:notification_counts
	                          {:sub_notifications
	                           [{:sid sid
	                             :unread_modmail_count val}]}}}})]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (case num
              ;; after 5 downvotes, expect a personal message
              14 (is (= (notification 1) (utils/<message!! 200)))
              ;; after 6 downvotes, expect a site-wide message
              15 (is (= (notification 1) (utils/<message!! 200)))
              ;; on the last downvote, expect a thread-wide message
              19 (is (= (notification 1) (utils/<message!! 200)))
              nil)
            (jt/advance-clock! clock (jt/seconds 1))))
        (let [rsp (send-request (-> graphql :query :modmail-notifications)
                                {:first 10
                                 :sids [sid]}
                                (utils/request-cookies mod-uid))
              {:keys [body status]} rsp
              {:keys [data errors]} body]
          (is (= 200 status))
          (is (nil? errors))
          (let [mtid (get-in data [:modmail_threads :edges 0 :node :id])]
            (is (= {:modmail_threads
	            {:edges
	             [{:node
	               {:id mtid
                        :subject (format "Excessive downvoting by %s"
                                         user-name)
	                :sub {:sid sid}
	                :reply_count 2
	                :first_message
	                {:content
                         (format
                          (str "[%s](/u/%s) exceeded limits "
                               "by consecutively downvoting [%s](/u/%s).")
                          user-name user-name mod2-name mod2-name)
                         :mtype "MOD_NOTIFICATION"
	                 :unread true}
	                :mailbox "INBOX"}}	            ]
	             :pageInfo {:hasNextPage false}}}
                   data))
            (let [rsp (send-request (-> graphql :query :modmail-thread)
                                    {:first 10 :thread_id mtid}
                                    (utils/request-cookies mod-uid))
                  {:keys [body status]} rsp
                  {:keys [data errors]} body]
              (is (= 200 status))
              (is (nil? errors))
              (is (= {:modmail_thread
	              {:edges
	               [{:node
                         {:content
                          (format
                           (str "[%s](/u/%s) exceeded limits for downvoting "
                                "comments on [%s](/o/%s/%s) in [%s](/o/%s).")
                           user-name user-name title sub-name pid
                           sub-name sub-name)
	                  :unread true}}
	                {:node
	                 {:content
                          (format
                           (str "[%s](/u/%s) exceeded site-wide downvoting "
                                "limits.")
                           user-name user-name)
	                  :unread true}}
	                {:node
	                 {:content
                          (format
                           (str "[%s](/u/%s) exceeded limits by "
                                "consecutively downvoting [%s](/u/%s).")
                           user-name user-name mod2-name mod2-name)
                          :unread true}}]
	               :pageInfo {:hasNextPage false}}}
                     data)))))))))
