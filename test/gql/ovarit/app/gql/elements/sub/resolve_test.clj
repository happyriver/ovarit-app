;; gql/elements/sub/resolve_test.clj -- Testing sub functionality for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.sub.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request] :as utils]
   [ovarit.app.gql.util :as util]))

(def ^:dynamic ^:private *system* nil)
(defgraphql graphql "test-graphql/sub.graphql")

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries graphql)
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-query-all-subs
  (let [{:keys [sid sub-name sid2 sub2-name]} (utils/mod-user-and-sub *system*)]
    (testing "anon user can query all subs"
      (let [response (send-request (-> graphql :query :all-subs)
                                   {:first 500}
                                   (utils/request-cookies nil))
            data (get-in response [:body :data])
            {:keys [edges pageInfo]} (:all_subs data)
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (false? (:hasNextPage pageInfo)))
        (is (some #(= % {:node {:sid sid
                                :name sub-name
                                :subscriber_count 1
                                :post_count 0}}) edges))
        (is (some #(= % {:node {:sid sid2
                                :name sub2-name
                                :subscriber_count 1
                                :post_count 0}}) edges))))))

(deftest test-query-sub-fields
  (let [{:keys [mod-name sid sub-name
                user-uid mod-uid admin-uid]} (utils/mod-user-and-sub *system*)
        expected {:sub_by_name
                  {
                   :creator {:name mod-name
                             :status "ACTIVE"}
                   :freeform_user_flairs false
                   :moderators [{:mod {:name mod-name
                                       :status "ACTIVE"}
                                 :moderation_level "OWNER"}]
                   :name sub-name
                   :nsfw false
                   :post_count 0
                   :post_flairs []
                   :restricted false
                   :sid sid
                   :sidebar ""
                   :sub_banned_users_private false
                   :sublog_private false
                   :subscriber_count 1
                   :text_post_min_length 0
                   :title "things to talk about"
                   :user_can_flair_self false
                   :user_flair_choices nil}}
        user-attributes {:banned false
                         :user_flair nil}]
    (testing "anons can query sub fields"
      (let [response (send-request (-> graphql :query :sub-with-fields-by-name)
                                   {:name sub-name
                                    :is_anon true})
            {:keys [data errors]} (:body response)]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= expected (update data :sub_by_name dissoc :creation)))))
    (testing "users can query sub fields"
      (doseq [uid [user-uid mod-uid admin-uid]]
        (let [response (send-request (-> graphql :query
                                         :sub-with-fields-by-name)
                                     {:name sub-name
                                      :is_anon false}
                                     (when uid (utils/request-cookies uid)))
              {:keys [data errors]} (:body response)
              expected-authed (assoc-in expected [:sub_by_name :user_attributes]
                                        user-attributes)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= expected-authed
                 (update data :sub_by_name dissoc :creation))))))))

(deftest test-create-sub-rules
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid sid sub-name]}
        (utils/mod-user-and-sub *system*)]
    (testing "anons can't create sub rules"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "Not a rule"}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't create sub rules"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "Not a rule"}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't create sub rules in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "Not a rule either"}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "sub rules can't be empty"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "   "}
                                   (utils/request-cookies mod-uid))]
        (is (= 400 (:status response)))
        (is (= "Invalid argument: text"
               (get-in response [:body :errors 0 :message])))))
    (testing "admins can create sub rules in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "admin rule"}
                                   (utils/request-cookies admin-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= {:text "admin rule"}
               (dissoc (:create_sub_rule data) :id)))))
    (testing "mods can create sub rules in their own sub"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid
                                    :text "mod rule"}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= {:text "mod rule"}
               (dissoc (:create_sub_rule data) :id)))))
    (testing "users can retrieve rules in order"
      (let [response (send-request (-> graphql :query :sub-rules)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= ["admin rule" "mod rule"]
               (map :text (get-in data [:sub_by_name :rules]))))))))

(defn create-a-rule
  "Create a rule using a GraphQL mutation."
  [sid text mod-uid]
  (let [response (send-request (-> graphql :mutation :create-sub-rule)
                               {:sid sid :text text}
                               (utils/request-cookies mod-uid))]
    (is (= 200 (:status response)))
    (get-in response [:body :data :create_sub_rule])))

(deftest test-create-rule-limit
  (let [{:keys [mod-uid sid]} (utils/mod-user-and-sub *system*)]
    (doseq [num (range 100)]
      (create-a-rule sid (str "rule " num) mod-uid))
    (testing "there is a limit on sub rules"
      (let [response (send-request (-> graphql :mutation :create-sub-rule)
                                   {:sid sid :text "one too many"}
                                   (utils/request-cookies mod-uid))
            {:keys [errors]} (:body response)]
        (is (= "Too many rules" (get-in errors [0 :message])))
        (is (= 400 (:status response)))))))

(deftest test-reorder-sub-rules
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid sid sub-name]}
        (utils/mod-user-and-sub *system*)

        rules (map #(create-a-rule sid % mod-uid)
                   ["rule one" "rule two" "rule three" "rule four"])
        reversed (reverse rules)]
    (testing "anons can't reorder rules"
      (let [response (send-request (-> graphql :mutation :reorder-sub-rules)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't reorder rules"
      (let [response (send-request (-> graphql :mutation :reorder-sub-rules)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't reorder rules in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :reorder-sub-rules)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "rules are in original order"
      (let [response (send-request (-> graphql :query :sub-rules)
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= rules (get-in data [:sub_by_name :rules])))))
    (testing "mods can reorder rules"
      (let [response (send-request (-> graphql :mutation :reorder-sub-rules)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= reversed (:reorder_sub_rules data)))))
    (testing "rules are reversed"
      (let [response (send-request (-> graphql :query :sub-rules)
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= reversed (get-in data [:sub_by_name :rules])))))
    (testing "admins can reorder rules"
      (let [response (send-request (-> graphql :mutation :reorder-sub-rules)
                                   {:sid sid
                                    :ids (map :id rules)}
                                   (utils/request-cookies admin-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= rules (:reorder_sub_rules data)))))))

(deftest test-delete-sub-rules
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid sid sid2
                sub-name]} (utils/mod-user-and-sub *system*)
        rules (map #(create-a-rule sid % mod-uid)
                   ["rule one" "rule two" "rule three" "rule four"])]
    (testing "anons can't delete rules"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id (:id (first rules))}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't delete rules"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id (:id (first rules))}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't delete rules in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id (:id (first rules))}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "can't delete a rule if the sid doesn't match"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid2
                                    :id (:id (first rules))}
                                   (utils/request-cookies mod2-uid))]
        (is (= 400 (:status response)))
        (is (= "Rule does not exist"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't delete rules that don't exist"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id "100000000"}
                                   (utils/request-cookies mod-uid))]
        (is (= 400 (:status response)))
        (is (= "Rule does not exist"
               (get-in response [:body :errors 0 :message])))))
    (testing "no rules have been deleted yet"
      (let [response (send-request (-> graphql :query :sub-rules)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= rules (get-in data [:sub_by_name :rules])))))
    (testing "mods can delete rules"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id (:id (first rules))}
                                   (utils/request-cookies mod-uid))
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (let [response (send-request (-> graphql :query :sub-rules)
                                     {:name sub-name}
                                     (utils/request-cookies nil))
              data (get-in response [:body :data])
              errors (get-in response [:body :errors])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= sid (get-in data [:sub_by_name :sid])))
          (is (= (rest rules) (get-in data [:sub_by_name :rules]))))))
    (testing "admins can delete rules"
      (let [response (send-request (-> graphql :mutation :delete-sub-rule)
                                   {:sid sid
                                    :id (:id (second rules))}
                                   (utils/request-cookies admin-uid))
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))))))

(deftest test-create-sub-post-flairs
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid sid
                sub-name]} (utils/mod-user-and-sub *system*)]
    (testing "anons can't create sub post flairs"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "Not a flair"}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't create sub post flairs"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "Not a flair"}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't create sub post flairs in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "Not a flair either"}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "sub flairs can't be empty"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "   "}
                                   (utils/request-cookies mod-uid))]
        (is (= 400 (:status response)))
        (is (= "Invalid argument: text"
               (get-in response [:body :errors 0 :message])))))
    (testing "admins can create sub flairs in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "admin flair"}
                                   (utils/request-cookies admin-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= {:text "admin flair"
                :mods_only false
                :post_types ["TEXT" "LINK" "UPLOAD" "POLL"]}
               (dissoc (:create_sub_post_flair data) :id)))))
    (testing "mods can create sub post flairs in their own sub"
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid
                                    :text "mod flair"}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= {:text "mod flair"
                :mods_only false
                :post_types ["TEXT" "LINK" "UPLOAD" "POLL"]}
               (dissoc (:create_sub_post_flair data) :id)))))
    (testing "users can retrieve post flairs in order"
      (let [response (send-request (-> graphql :query :sub-post-flairs)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= ["admin flair" "mod flair"]
               (map :text (get-in data [:sub_by_name :post_flairs]))))))))

(defn create-a-post-flair
  "Create a post flair using a GraphQL mutation."
  [sid text mod-uid]
  (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                               {:sid sid :text text}
                               (utils/request-cookies mod-uid))
        {:keys [data errors]} (:body response)]
    (is (nil? errors))
    (is (= 200 (:status response)))
    (let [result (:create_sub_post_flair data)]
      (is (and (:id result) (:text result)))
      (update result :post_types set))))

(deftest test-create-post-flair-limit
  (let [{:keys [mod-uid sid]} (utils/mod-user-and-sub *system*)]
    (testing "there is a limit on sub post flairs"
      (doseq [num (range 100)]
        (create-a-post-flair sid (str "flair " num) mod-uid))
      (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                                   {:sid sid :text "one too many"}
                                   (utils/request-cookies mod-uid))
            {:keys [errors]} (:body response)]
        (is (= "Too many flairs" (get-in errors [0 :message])))
        (is (= 400 (:status response)))))))

(deftest test-reorder-sub-post-flairs
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid sid
                sub-name]} (utils/mod-user-and-sub *system*)
        flairs (doall (map #(create-a-post-flair sid % mod-uid)
                           ["flair one" "flair two" "flair three"
                            "flair four"]))
        reversed (reverse flairs)
        flairs-with-types (map #(assoc % :post_types
                                       #{"LINK" "POLL" "TEXT" "UPLOAD"})
                               flairs)]
    (testing "anons can't reorder flairs"
      (let [response (send-request (-> graphql :mutation
                                       :reorder-sub-post-flairs)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't reorder flairs"
      (let [response (send-request (-> graphql :mutation
                                       :reorder-sub-post-flairs)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't reorder flairs in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation
                                       :reorder-sub-post-flairs)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "flairs are in original order"
      (let [response (send-request (-> graphql :query :sub-post-flairs)
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= flairs-with-types (->> (get-in data [:sub_by_name :post_flairs])
                                      (map #(update % :post_types set)))))))
    (testing "mods can reorder flairs"
      (let [response (send-request (-> graphql :mutation
                                       :reorder-sub-post-flairs)
                                   {:sid sid
                                    :ids (map :id reversed)}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= (map #(select-keys % [:id :text]) reversed)
               (:reorder_sub_post_flairs data)))))
    (testing "flairs are reversed"
      (let [response (send-request (-> graphql :query :sub-post-flairs)
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= (reverse flairs-with-types)
               (->> (get-in data [:sub_by_name :post_flairs])
                    (map #(update % :post_types set)))))))
    (testing "admins can reorder flairs"
      (let [response (send-request (-> graphql :mutation
                                       :reorder-sub-post-flairs)
                                   {:sid sid
                                    :ids (map :id flairs)}
                                   (utils/request-cookies admin-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= (map #(select-keys % [:id :text]) flairs)
               (:reorder_sub_post_flairs data)))))))

(deftest test-delete-sub-post-flairs
  (let [{:keys [admin-uid mod-uid mod2-uid user-uid
                sid sid2 sub-name]} (utils/mod-user-and-sub *system*)
        flairs (->> ["flair one" "flair two" "flair three" "flair four"]
                    (map #(create-a-post-flair sid % mod-uid))
                    (map #(assoc % :post_types
                                 #{"LINK" "POLL" "TEXT" "UPLOAD"})))]
    (testing "anons can't delete flairs"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id (:id (first flairs))}
                                   (utils/request-cookies nil))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "users can't delete flairs"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id (:id (first flairs))}
                                   (utils/request-cookies user-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't delete flairs in a sub that isn't theirs"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id (:id (first flairs))}
                                   (utils/request-cookies mod2-uid))]
        (is (= 403 (:status response)))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))
    (testing "can't delete a flair if the sid doesn't match"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid2
                                    :id (:id (first flairs))}
                                   (utils/request-cookies mod2-uid))]
        (is (= 400 (:status response)))
        (is (= "Flair does not exist"
               (get-in response [:body :errors 0 :message])))))
    (testing "mods can't delete flairs that don't exist"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id "100000000"}
                                   (utils/request-cookies mod-uid))]
        (is (= 400 (:status response)))
        (is (= "Flair does not exist"
               (get-in response [:body :errors 0 :message])))))
    (testing "no flairs have been deleted yet"
      (let [response (send-request (-> graphql :query :sub-post-flairs)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            data (get-in response [:body :data])
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= sid (get-in data [:sub_by_name :sid])))
        (is (= flairs (->> (get-in data [:sub_by_name :post_flairs])
                           (map #(update % :post_types set)))))))
    (testing "mods can delete flairs"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id (:id (first flairs))}
                                   (utils/request-cookies mod-uid))
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (let [response (send-request (-> graphql :query :sub-post-flairs)
                                     {:name sub-name}
                                     (utils/request-cookies nil))
              data (get-in response [:body :data])
              errors (get-in response [:body :errors])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= sid (get-in data [:sub_by_name :sid])))
          (is (= (rest flairs) (->> (get-in data [:sub_by_name :post_flairs])
                                    (map #(update % :post_types set))))))))
    (testing "admins can delete flairs"
      (let [response (send-request (-> graphql :mutation :delete-sub-post-flair)
                                   {:sid sid
                                    :id (:id (second flairs))}
                                   (utils/request-cookies admin-uid))
            errors (get-in response [:body :errors])]
        (is (= 200 (:status response)))
        (is (nil? errors))))))

(defn get-user-flair
  "Verify that user `uid` has the given user flair."
  [uid name]
  (let [rsp (send-request (-> graphql :query :sub-user-flair)
                          {:name name}
                          (utils/request-cookies uid))
        {:keys [data errors]} (:body rsp)]
    (is (= 200 (:status rsp)))
    (is (nil? errors))
    (get-in data [:sub_by_name :user_attributes :user_flair])))

(deftest test-user-flair-error-checking
  (let [{:keys [admin-uid mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)
        db (get-in *system* [:db :ds])]
    (testing "Users can't set user flair if not enabled for sub"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (jdbc/insert! db "sub_metadata" {:sid sid
                                     :key "user_can_flair_self"
                                     :value "1"})
    (testing "Users can't set freeform user flair if not enabled for sub"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (jdbc/insert! db "sub_metadata" {:sid sid
                                     :key "freeform_user_flairs"
                                     :value "1"})
    (testing "Anons can't set user flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "AnonFlair"})]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "uid is required"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :user_flair "Flair"}
                              (utils/request-cookies mod-uid))]
        (is (= 400 (:status rsp)))))
    (testing "sub must exist"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid "not a sid"
                               :uid user-uid
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not found" (get-in rsp [:body :errors 0 :message])))))
    (testing "Users can't set someone else's flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid mod-uid
                               :user_flair "Flair"})]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "Flair length limit enforced"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid admin-uid
                               :user_flair "FlairFlairFlairFlairFlairFlair"}
                              (utils/request-cookies admin-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Invalid argument: user_flair"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "Banned users can't flair"
      (jdbc/insert! db "sub_ban" {:uid user-uid
                                  :sid sid
                                  :created (util/sql-timestamp)
                                  :reason "test"
                                  :effective true
                                  :created_by_id mod-uid})
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "BanFlair"}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "Verify flair was not set by failed mutations"
      (is (nil? (get-user-flair user-uid sub-name))))))

(deftest test-user-freeform-flair
  (let [{:keys [admin-uid mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)
        db (get-in *system* [:db :ds])]
    (jdbc/insert! db "sub_metadata" {:sid sid
                                     :key "freeform_user_flairs"
                                     :value "1"})
    (testing "Users can set a freeform flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:change_user_flair "Flair"} data))
        (is (= "Flair" (get-user-flair user-uid sub-name)))))
    (testing "Mods can set freeform user flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "FlairByMod"}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:change_user_flair "FlairByMod"} data))
        (is (= "FlairByMod" (get-user-flair user-uid sub-name)))))
    (testing "Admins can set a freeform flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "FlairByAdmin"}
                              (utils/request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:change_user_flair "FlairByAdmin"} data))
        (is (= "FlairByAdmin" (get-user-flair user-uid sub-name)))))
    (testing "Users can delete their freeform flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair nil}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:change_user_flair nil} data))
        (is (nil? (get-user-flair user-uid sub-name)))))))

(deftest test-user-flair-choices-error-checking
  (let [{:keys [mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)]
    (testing "Unauthenticated users can't create flair"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid sid
                               :user_flair "Flair"})]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))

    (testing "Regular users can't create flair"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid sid
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))

    (testing "Sub must exist"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid "not a sid"
                               :user_flair "Flair"}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))

    (testing "Mods can't create empty flair"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid sid
                               :user_flair ""}
                              (utils/request-cookies mod-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Invalid argument: user_flair"
               (get-in rsp [:body :errors 0 :message])))))

    (testing "Failed attempts did not create flair")
    (let [rsp (send-request (-> graphql :query :sub-user-flair)
                            {:name sub-name}
                            (utils/request-cookies user-uid))
          {:keys [data errors]} (:body rsp)]
      (is (= 200 (:status rsp)))
      (is (nil? errors))
      (is (empty? (get-in data [:sub_by_name :user_flair_choices]))))

    (testing "A limit on the number of flairs is enforced"
      (loop [num 0]
        (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                                {:sid sid
                                 :user_flair (str num)}
                                (utils/request-cookies mod-uid))]
          (if (and (< num 200) (= 200 (:status rsp)))
            (recur (inc num))
            (do
              (is (< num 200))
              (is (= 400 (:status rsp)))
              (is (= "Too many flairs"
                     (get-in rsp [:body :errors 0 :message]))))))))))

(deftest test-user-flair-choices
  (let [{:keys [admin-uid mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)
        db (get-in *system* [:db :ds])]
    (jdbc/insert! db "sub_metadata" {:sid sid
                                     :key "user_can_flair_self"
                                     :value "1"})
    (testing "Mods can create a preset flair"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid sid
                               :user_flair "Preset Flair"}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:create_user_flair_choice
                {:sid sid
                 :user_flair_choices ["Preset Flair"]}}
               data))))
    (testing "Users can use a preset flair"
      (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                              {:sid sid
                               :uid user-uid
                               :user_flair "Preset Flair"}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:change_user_flair "Preset Flair"}
               data)))
      (testing "and verify it"
        (let [rsp (send-request (-> graphql :query :sub-user-flair)
                                {:name sub-name}
                                (utils/request-cookies user-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:sub_by_name
                  {:sid sid
                   :user_attributes {:user_flair "Preset Flair"}
                   :user_flair_choices ["Preset Flair"]}} data)))))
    (testing "Admins can create a flair"
      (let [rsp (send-request (-> graphql :mutation :create-user-flair-choice)
                              {:sid sid
                               :user_flair "ByAdmin"}
                              (utils/request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:create_user_flair_choice
                {:sid sid
                 :user_flair_choices #{"ByAdmin" "Preset Flair"}}}
               (update-in data [:create_user_flair_choice :user_flair_choices]
                          set))))
      (testing "and verify they created it"
        (let [rsp (send-request (-> graphql :query :sub-user-flair)
                                {:name sub-name}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:sub_by_name
                  {:sid sid
                   :user_attributes {:user_flair nil}
                   :user_flair_choices #{"Preset Flair" "ByAdmin"}}}
                 (update-in data [:sub_by_name :user_flair_choices] set)))))
      (testing "A mod can assign a preset to a user"
        (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                                {:sid sid
                                 :uid user-uid
                                 :user_flair "ByAdmin"}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:change_user_flair "ByAdmin"} data))))
      (testing "A mod can assign a freeform flair regardless of the setting"
        (let [rsp (send-request (-> graphql :mutation :change-user-flair)
                                {:sid sid
                                 :uid user-uid
                                 :user_flair "Freeform"}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:change_user_flair "Freeform"} data)))))))

(deftest test-post-type-config-auth
  (let [{:keys [sid sub-name user-uid]} (utils/mod-user-and-sub *system*)
        expected {:sub_by_name
	          {:sid sid
	           :post_type_config
	           (set
                    [{:post_type "LINK" :rules "" :mods_only false}
	             {:post_type "POLL" :rules "" :mods_only true}
	             {:post_type "TEXT" :rules "" :mods_only false}
	             {:post_type "UPLOAD" :rules "" :mods_only false}])}}]
    (testing "Anyone can query post types and rules"
      (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                              {:name sub-name})
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= expected
               (update-in data [:sub_by_name :post_type_config] set)))))
    (testing "Anons and users can't update post types"
      (doseq [uid [nil user-uid ]]
        (let [rsp (send-request (-> graphql :mutation
                                    :update-sub-post-type-config)
                                {:sid sid
                                 :post_type "POLL"
                                 :mods_only false
                                 :rules ""}
                                (utils/request-cookies uid))]
          (is (= 403 (:status rsp))))))
    (testing "Anons and users can't change post type rules"
      (doseq [uid [nil user-uid ]]
        (let [rsp (send-request (-> graphql :mutation
                                    :update-sub-post-type-config)
                                {:sid sid
                                 :post_type "POLL"
                                 :rules "New rules"}
                                (utils/request-cookies uid))]
          (is (= 403 (:status rsp))))))
    (testing "Post types and rules are unchanged after failed mutations"
      (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                              {:name sub-name}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= expected
               (update-in data [:sub_by_name :post_type_config] set)))))))

(defn make-post-config
  [sid types]
  (let [check (comp nil? types)]
    {:sub_by_name
     {:sid sid
      :post_type_config
      (set
       [{:post_type "LINK" :rules "" :mods_only (check "LINK")}
	{:post_type "POLL" :rules "" :mods_only (check "POLL")}
	{:post_type "TEXT" :rules "" :mods_only (check "TEXT")}
	{:post_type "UPLOAD" :rules "" :mods_only (check "UPLOAD")}])}}))

(deftest test-update-post-types
  (let [{:keys [sid sub-name mod-uid
                admin-uid]} (utils/mod-user-and-sub *system*)]
    (testing "Mods can update post types"
      (let [rsp (send-request (-> graphql :mutation
                                  :update-sub-post-type-config)
                              {:sid sid
                               :post_type "LINK"
                               :mods_only true}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors)))
      (testing "and query the changes"
        (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                                {:name sub-name})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= (make-post-config sid #{"TEXT" "UPLOAD"})
                 (update-in data [:sub_by_name :post_type_config] set))))))
    (testing "Admins can update post types"
      (let [rsp (send-request (-> graphql :mutation
                                  :update-sub-post-type-config)
                              {:sid sid
                               :post_type "POLL"
                               :mods_only false}
                              (utils/request-cookies admin-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors)))
      (testing "and query the changes"
        (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                                {:name sub-name}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= (make-post-config sid #{"POLL" "TEXT" "UPLOAD"})
                 (update-in data [:sub_by_name :post_type_config] set))))))))

(deftest test-update-post-type-config
  (let [{:keys [sid sub-name mod-uid
                admin-uid]} (utils/mod-user-and-sub *system*)]
    (testing "Mods can update post type rules"
      (let [rsp (send-request (-> graphql :mutation
                                  :update-sub-post-type-config)
                              {:sid sid
                               :post_type "TEXT"
                               :mods_only nil
                               :rules "some rules"}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors)))
      (testing "and query the changes"
        (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                                {:name sub-name})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:sub_by_name
	          {:sid sid
	           :post_type_config
	           #{{:post_type "UPLOAD" :rules "" :mods_only false}
	             {:post_type "LINK" :rules "" :mods_only false}
	             {:post_type "POLL" :rules "" :mods_only true}
	             {:post_type "TEXT" :rules "some rules" :mods_only false}}}}
                 (update-in data [:sub_by_name :post_type_config] set))))))
    (testing "Admins can update post types"
      (let [rsp (send-request (-> graphql :mutation
                                  :update-sub-post-type-config)
                              {:sid sid
                               :post_type "POLL"
                               :mods_only nil
                               :rules "more rules"}
                              (utils/request-cookies admin-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors)))
      (testing "and query the changes"
        (let [rsp (send-request (-> graphql :query :sub-with-post-types)
                                {:name sub-name}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:sub_by_name
	          {:sid sid
	           :post_type_config
	           #{{:post_type "UPLOAD" :rules "" :mods_only false}
	             {:post_type "LINK" :rules "" :mods_only false}
	             {:post_type "POLL" :rules "more rules" :mods_only true}
	             {:post_type "TEXT" :rules "some rules" :mods_only false}}}}
                 (update-in data [:sub_by_name :post_type_config] set))))))))

(deftest test-update-post-type-flairs-auth
  (let [{:keys [admin-uid mod-uid mod2-uid sid sid2 sub-name
                user-uid]} (utils/mod-user-and-sub *system*)
        f1 (create-a-post-flair sid "f1" mod-uid)]
    (testing "Anyone can query flairs permitted by post type"
      (doseq [uid [nil admin-uid mod-uid user-uid]]
        (let [rsp (send-request (-> graphql :query :sub-post-flairs)
                                {:name sub-name}
                                (utils/request-cookies uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (every? #(= (set (:post_types %))
                          #{"TEXT" "LINK" "POLL" "UPLOAD"})
                      (get-in data [:sub_by_name :post_flairs]))))))
    (testing "Anons and users can't update flairs for a post type"
      (doseq [uid [nil user-uid]]
        (let [rsp (send-request (-> graphql :mutation :update-post-flair)
                                {:sid sid
                                 :id (:id f1)
                                 :mods_only true
                                 :post_types ["TEXT"]}
                                (utils/request-cookies uid))]
          (is (= 403 (:status rsp))))))
    (testing "Mods can't change flairs not in their sub"
      (let [rsp (send-request (-> graphql :mutation :update-post-flair)
                              {:sid sid2
                               :id (:id f1)
                               :mods_only true
                               :post_types ["TEXT"]}
                              (utils/request-cookies mod2-uid))]
        (is (= 200 (:status rsp)))
        (is (nil? (get-in rsp [:body :data :update_post_flair])))))
    (testing "Allowed flairs are unchanged after failed mutations"
      (let [rsp (send-request (-> graphql :query :sub-post-flairs)
                              {:name sub-name})
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (let [flair (-> data
                        (get-in [:sub_by_name :post_flairs])
                        first)]
          (is (false? (:mods_only flair)))
          (is (= #{"TEXT" "LINK" "POLL" "UPLOAD"} (-> flair
                                                      :post_types
                                                      set))))))))

(deftest test-update-post-flairs
  (let [{:keys [sid sub-name mod-uid
                admin-uid]} (utils/mod-user-and-sub *system*)
        f1 (create-a-post-flair sid "f1" mod-uid)
        f2 (create-a-post-flair sid "f2" mod-uid)]
    (testing "Mods can update post flairs"
      (let [rsp (send-request (-> graphql :mutation :update-post-flair)
                              {:sid sid
                               :id (:id f1)
                               :post_types ["TEXT"]
                               :mods_only false}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (nil? (get-in rsp [:body :data :update_post_type_flairs]))))
      (testing "and query the changes"
        (let [rsp (send-request (-> graphql :query :sub-post-flairs)
                                {:name sub-name}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)
              flairs (->> (get-in data [:sub_by_name :post_flairs])
                          (map #(update % :post_types set))
                          set)
              expected #{(assoc f1 :post_types #{"TEXT"}) f2}]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= expected flairs))))
      (testing "Admins can update post type flairs"
        (let [rsp (send-request (-> graphql :mutation :update-post-flair)
                                {:sid sid
                                 :id (:id f2)
                                 :post_types ["LINK" "UPLOAD"]
                                 :mods_only true}
                                (utils/request-cookies admin-uid))
              {:keys [errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (get-in rsp [:body :data :update_post_type_flairs]))))
        (testing "and query the changes"
          (let [rsp (send-request (-> graphql :query :sub-post-flairs)
                                  {:name sub-name}
                                  (utils/request-cookies admin-uid))
                {:keys [data errors]} (:body rsp)
                flairs (->> (get-in data [:sub_by_name :post_flairs])
                            (map #(update % :post_types set))
                            set)
                expected #{(assoc f1 :post_types #{"TEXT"})
                           (assoc f2 :post_types #{"LINK" "UPLOAD"}
                                  :mods_only true)}]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= expected flairs))))))))
