;; gql/elements/comment/resolve_test.clj -- Testing comment functionality for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.resolve-test
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [clojure.walk :as walk]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [dotenv :refer [env]]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request] :as utils]
   [taoensso.carmine :as car]))

(defonce rng (new java.util.Random))
(def seed #_999 (rand-int 1000))

(def ^:dynamic ^:private *system* nil)

(defgraphql graphql
  "test-graphql/comment.graphql"
  "test-graphql/shared.graphql")

(defn test-content
  []
  (let [content (utils/mod-user-and-sub *system*)
        {:keys [sid sid2 user-uid]} content
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        rsp2 (send-request (-> graphql :mutation :create-test-post)
                           {:title "Test post"
                            :content "testify"
                            :sid sid2
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))]
    (assoc content
           :pid (get-in rsp [:body :data :create_test_post :pid])
           :pid2 (get-in rsp2 [:body :data :create_test_post :pid]))))

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries graphql)
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

;; Give each test a repeatable sequence of random numbers.
(use-fixtures :each
  (fn [test-fn]
    (.setSeed rng seed)
    (log/info {:seed seed} "Set random seed for test")
    (test-fn)))

(deftest test-create-comment-error-not-authenticated
  (let [{:keys [pid]} (test-content)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "comment"
                           :pid pid})]
    (is (= 403 (:status rsp)))
    (is (= (get-in rsp [:body :errors 0 :message]) "Not authorized"))))

(deftest test-create-comment-error-posting-disaabled
  (let [{:keys [pid user-uid mod-uid admin-uid]} (test-content)]
    (testing "When posting is disabled"
      (jdbc/execute! (:ds (:db *system*))
                     [(str "update site_metadata set value='0' "
                           "where key='site.enable_posting'")])
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb))
      (testing "mods and regular users cannot make comments"
        (doseq [uid [mod-uid user-uid]]
          (let [rsp (send-request (-> graphql :mutation :create-comment)
                                  {:content "comment"
                                   :pid pid}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
      (testing "admins can make comments"
        (let [rsp (send-request (-> graphql :mutation :create-comment)
                                {:content "comment"
                                 :pid pid}
                                (utils/request-cookies admin-uid))]
          (is (= 200 (:status rsp)))))
      (jdbc/execute! (:ds (:db *system*))
                     [(str "update site_metadata set value='1' "
                           "where key='site.enable_posting'")])
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))))

(deftest test-create-comment-error-on-missing-post
  (let [{:keys [user-uid]} (test-content)]
    (testing "Can't comment on a non-existent post"
      (let [rsp (send-request (-> graphql :mutation :create-comment)
                              {:content "comment"
                               :pid "clearly not a pid"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Invalid argument: pid"
               (get-in rsp [:body :errors 0 :message]))))
      (let [rsp (send-request (-> graphql :mutation :create-comment)
                              {:content "comment"
                               :pid "123456"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not found"
               (get-in rsp [:body :errors 0 :message])))))))

(deftest test-create-comment-error-on-deleted-post
  (let [{:keys [pid admin-uid mod-uid user-uid]} (test-content)]
    (testing "On deleted posts"
      (let [rsp (send-request (-> graphql :mutation :remove-post)
                              {:pid pid}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp))))
      (testing "users can't comment"
        (let [rsp (send-request (-> graphql :mutation :create-comment)
                                {:content "comment"
                                 :pid pid}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= (get-in rsp [:body :errors 0 :message]) "Post deleted"))))
      (testing "mods and admins can comment"
        (doseq [uid [mod-uid admin-uid]]
          (let [rsp (send-request (-> graphql :mutation :create-comment)
                                  {:content "comment"
                                   :pid pid}
                                  (utils/request-cookies uid))]
            (is (= 200 (:status rsp)))))))))

(deftest test-create-comment-error-on-archived-post
  (let [{:keys [pid admin-uid mod-uid user-uid]} (test-content)]
    (testing "No one can comment on archived posts"
      (jdbc/execute! (:ds (:db *system*))
                     [(str "update sub_post "
                           "set posted=posted-interval '1 year' "
                           "where pid=?")
                      (Integer. pid)])
      (doseq [uid [admin-uid mod-uid user-uid]]
        (let [rsp (send-request (-> graphql :mutation :create-comment)
                                {:content "comment"
                                 :pid pid}
                                (utils/request-cookies uid))]
          (is (= 400 (:status rsp)))
          (is (= (get-in rsp [:body :errors 0 :message]) "Post archived")))))))

(deftest test-create-comment-error-on-locked-post
  (let [{:keys [pid admin-uid mod-uid user-uid]} (test-content)]
    (testing "On locked posts"
      (jdbc/execute! (:ds (:db *system*))
                     [(str "insert into sub_post_metadata(key, value, pid) "
                           "values ('lock-comments', '1', ?)")
                      (Integer. pid)])
      (testing "users can't comment"
        (let [rsp (send-request (-> graphql :mutation :create-comment)
                                {:content "comment"
                                 :pid pid}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= (get-in rsp [:body :errors 0 :message]) "Post locked"))))
      (testing "mods and admins can comment"
        (doseq [uid [mod-uid admin-uid]]
          (let [rsp (send-request (-> graphql :mutation :create-comment)
                                  {:content "comment"
                                   :pid pid}
                                  (utils/request-cookies uid))]
            (is (= 200 (:status rsp)))))))))

(deftest test-create-comment-error-when-banned
  (let [{:keys [pid sid user-uid]} (test-content)]
    (testing "Banned users can't make comments"
      (jdbc/execute! (:ds (:db *system*))
                     [(str "insert into sub_ban(uid, sid, created, reason, "
                           "effective) "
                           "values (?, ?, now(), 'reason', true)")
                      user-uid sid])
      (let [rsp (send-request (-> graphql :mutation :create-comment)
                              {:content "comment"
                               :pid pid}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= (get-in rsp [:body :errors 0 :message]) "Not authorized"))))))

(deftest test-create-comment-error-on-missing-parent
  (let [{:keys [pid user-uid]} (test-content)]
    (testing "Can't comment on a non-existent parent comment"
      (doseq [cid ["123456" "clearly not a cid"]]
        (let [rsp (send-request (-> graphql :mutation :create-comment)
                                {:content "comment"
                                 :pid pid
                                 :parent_cid cid}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= (get-in rsp [:body :errors 0 :message]) "Not found")))))))

(deftest test-create-comment-error-mismatched-parent
  (let [{:keys [pid pid2 user-uid]} (test-content)]
    (testing "Parent comment and parent post must match"
      (let [rsp (send-request (-> graphql :mutation :create-comment)
                              {:content "comment"
                               :pid pid}
                              (utils/request-cookies user-uid))
            cid (get-in rsp [:body :data :create_comment :cid])
            _ (is (some? cid))
            rsp2 (send-request (-> graphql :mutation :create-comment)
                               {:content "comment"
                                :pid pid2
                                :parent_cid cid}
                               (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp2)))
        (is (= (get-in rsp2 [:body :errors 0 :message]) "Not found"))))))

(deftest test-new-comment-subscription
  (let [{:keys [sid user-uid]} (test-content)
        user-session (utils/make-session user-uid)
        rsp1 (send-request (-> graphql :mutation :create-test-post)
                           {:title "Test post"
                            :content "testing"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_test_post :pid])]
    (testing "inserting a comment streams it"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :new-comments
                                               utils/hash-query)
                                    :variables {:pid pid}}})
        (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                 {:content "new comment"
                                  :pid pid}
                                 (utils/request-cookies user-uid))
              _ (is (= 200 (:status rsp2)))
              cid (get-in rsp2 [:body :data :create_comment :cid])]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:new_comment
	                                    {:cid cid
	                                     :parent_cid nil
	                                     :content "new comment"}}}})
          (testing "and inserting a child comment streams it"
            (let [rsp3 (send-request (-> graphql :mutation :create-comment)
                                     {:content "child comment"
                                      :pid pid
                                      :parent_cid cid}
                                     (utils/request-cookies user-uid))
                  _ (is (= 200 (:status rsp2)))
                  child-cid (get-in rsp3 [:body :data :create_comment :cid])]
              (utils/expect-message {:type "data"
	                             :id id
	                             :payload
                                     {:data
	                              {:new_comment
	                               {:cid child-cid
	                                :parent_cid cid
	                                :content "child comment"}}}}))))))))

(deftest test-post-update-subscription
  (let [{:keys [sid user-uid]} (test-content)
        user-session (utils/make-session user-uid)
        rsp1 (send-request (-> graphql :mutation :create-test-post)
                           {:title "Test post"
                            :content "testing"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_test_post :pid])]
    (testing "inserting a comment streams the post comment count"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :post-update
                                               utils/hash-query)
                                    :variables {:pid pid}}})
        (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                 {:content "new comment"
                                  :pid pid}
                                 (utils/request-cookies user-uid))
              _ (is (= 200 (:status rsp2)))]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
                                           {:post_update
	                                    {:comment_count 1}}}}))))))

(deftest test-post-comment-notifications
  (let [{:keys [sid]} (test-content)
        prefix (str "user-" (swap! utils/name-counter inc) "-")
        uid1 (-> graphql :mutation :create-user
                 (send-request {:name (str prefix "one")})
                 (get-in [:body :data :register_user :uid]))
        uid2 (-> graphql :mutation :create-user
                 (send-request {:name (str prefix "two")})
                 (get-in [:body :data :register_user :uid]))
        u1-session (utils/make-session uid1)
        u2-session (utils/make-session uid2)
        rsp1 (send-request (-> graphql :mutation :create-test-post)
                           {:title "Test post"
                            :content "testing"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies uid1))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_test_post :pid])]
    (testing "commenting on a post notifies the author"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers u1-session))
        (utils/send-init {:token (utils/sign-csrf-token u1-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :notification-counts
                                               utils/hash-query)}})
        (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                 {:content "new comment"
                                  :pid pid}
                                 (utils/request-cookies uid2))
              _ (is (= 200 (:status rsp2)))]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:notification_counts
	                                    {:unread_notification_count 1}}}})
          ;; Once the notifications query is complete we should use it
          ;; here to make sure the notification says what it's
          ;; supposed to.
          )))
    (testing "replying to a comment notifies the comment author"
      (let [rsp (send-request (-> graphql :query :post-and-comments)
                              {:pid pid :sort_by "NEW"
                               :first 100 :level 2})
            _ (is (= 200 (:status rsp)))
            cid (get-in rsp [:body :data :post_by_pid :comment_tree
                             :comments 0 :cid])]
        (is (string? cid))
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers u2-session))
          (utils/send-init {:token (utils/sign-csrf-token u2-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql
                                                 :subscription
                                                 :notification-counts
                                                 utils/hash-query)}})
          (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                   {:content "comment reply"
                                    :parent_cid cid
                                    :pid pid}
                                   (utils/request-cookies uid1))
                _ (is (= 200 (:status rsp2)))]
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:notification_counts
	                                      {:unread_notification_count 1}}}})
            ;; Once the notifications query is complete we should use it
            ;; here to make sure the notification says what it's
            ;; supposed to.
            ))))))

(deftest test-comment-mention-notifications
  (testing "inserting a comment notifies a user who was mentioned"
    (let [{:keys [sid]} (test-content)
          prefix (str "user-" (swap! utils/name-counter inc) "-")
          name1 (str prefix "one")
          uid1 (-> graphql :mutation :create-user
                   (send-request {:name name1})
                   (get-in [:body :data :register_user :uid]))
          name2 (str prefix "two")
          uid2 (-> graphql :mutation :create-user
                   (send-request {:name name2})
                   (get-in [:body :data :register_user :uid]))
          u2-session (utils/make-session uid2)
          rsp1 (send-request (-> graphql :mutation :create-test-post)
                             {:title "Test post"
                              :content "testing mentions"
                              :sid sid
                              :nsfw false
                              :type :TEXT}
                             (utils/request-cookies uid1))
          _ (is (= 200 (:status rsp1)))
          pid (get-in rsp1 [:body :data :create_test_post :pid])]
      (testing "mentioning someone in a comment notifies them"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers u2-session))
          (utils/send-init {:token (utils/sign-csrf-token u2-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql
                                                 :subscription
                                                 :notification-counts
                                                 utils/hash-query)}})
          (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                   {:content (str "Mentioning /u/" name2)
                                    :pid pid}
                                   (utils/request-cookies uid1))
                _ (is (= 200 (:status rsp2)))]
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload {:data
	                                     {:notification_counts
	                                      {:unread_notification_count 1}}}})
            ;; Once the notifications query is complete we should use it
            ;; here to make sure the notification says what it's
            ;; supposed to.
            )))
      (testing "mentioning yourself in a comment does not notify you"
        (let [rsp2 (send-request (-> graphql :mutation :create-comment)
                                 {:content (str "Self mention /u/" name1
                                                " test")
                                  :pid pid}
                                 (utils/request-cookies uid1))
              _ (is (= 200 (:status rsp2)))
              rsp3 (send-request (-> graphql :query :self-notification-count)
                                 {}
                                 (utils/request-cookies uid1))]
          (is (= 200 (:status rsp3)))
          (is (= {:data
                  {:current_user
                   {:unread_notification_count 0}}} (:body rsp3)))
          ;; Once the notifications query is complete we should use it
          ;; here to make sure the notification says what it's
          ;; supposed to.
          )))))

#_(deftest test-comment-insertion-self-voting
    (testing "inserting a comment with self-voting enabled"
      (testing "gives the comment an upvote")
      (testing "increases the user's votes given count")
      (testing "notifies the user that their score increased")))

(defn make-comment
  "Insert a comment and return its cid."
  [{:keys [uid pid parent-cid level num]}]
  (let [rsp (send-request (-> graphql :mutation :create-comment)
                          {:content (str "Level: " level " Num: " num)
                           :pid pid
                           :parent_cid parent-cid}
                          (utils/request-cookies uid))
        _ (is (= 200 (:status rsp)))]
    (get-in rsp [:body :data :create_comment :cid])))

(defn add-comment-to-tree
  "Add a new comment somewhere in an existing tree of comments.
  Half of the time add it at the top level, the other half as
  a child of an existing comment."
  [{:keys [level tree ratio uid] :as args}]
  (if (or (empty? tree) (< (.nextFloat rng) ratio))
    (let [cid (make-comment args)]
      {:tree (conj tree {:cid cid :level level :children [] :uid uid
                         :status "ACTIVE"})
       :cid cid})
    (let [idx (int (* (count tree) (.nextFloat rng)))
          elem (get tree idx)
          {children :tree
           cid :cid} (add-comment-to-tree (assoc args
                                                 :parent-cid (:cid elem)
                                                 :tree (:children elem)
                                                 :level (inc level)))]
      {:tree (assoc-in tree [idx :children] children)
       :cid cid})))

(defn make-comment-tree
  "Make a comment tree containing `num` comments."
  [uid pid num ratio]
  (loop [n 0
         cids []
         tree []]
    (if (>= n num) {:cids cids
                    :tree tree}
        (let [result (add-comment-to-tree {:uid uid :pid pid
                                           :parent-cid nil
                                           :level 0 :num n :ratio ratio
                                           :tree tree})]
          (recur (inc n)
                 (conj cids (:cid result))
                 (:tree result))))))

(deftest test-comment-tree
  (let [{:keys [user-uid mod-uid sid]} (test-content)
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post for test-comment-tree"
                           :content "content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_test_post :pid])
        comment-count 100
        {:keys [cids tree]} (make-comment-tree user-uid pid comment-count 0.2)
        reverse-children (fn [com] (update com :children #(vec (reverse %))))
        reverse-tree (fn [tree]
                       (->> tree
                            (walk/postwalk #(if (map? %)
                                              (reverse-children %)
                                              %))
                            reverse))
        tree-newest-first (reverse-tree tree)
        parse-tree (fn [json]
                     (->> (cheshire/parse-string json)
                          walk/keywordize-keys
                          (walk/postwalk #(if (map? %)
                                            (dissoc % :time)
                                            %))))]
    (testing "full tree can be fetched"
      (let [rsp (send-request (-> graphql :query :post-and-comments)
                              {:pid pid :sort_by "NEW"
                               :first comment-count :level comment-count})
            _ (is (= 200 (:status rsp)))
            {:keys [data errors]} (:body rsp)
            {:keys [tree_json comments]} (get-in data [:post_by_pid
                                                       :comment_tree])
            comment-tree (parse-tree tree_json)]
        (is (nil? errors))
        (is (= tree-newest-first comment-tree))
        (is (= (set cids) (set (map :cid comments))))))
    (testing "comments can be fetched up to a level"
      (let [level 2
            rsp (send-request (-> graphql :query :post-and-comments)
                              {:pid pid :sort_by "NEW"
                               :first comment-count :level level})
            _ (is (= 200 (:status rsp)))
            {:keys [data errors]} (:body rsp)
            {:keys [tree_json comments]} (get-in data [:post_by_pid
                                                       :comment_tree])
            comment-tree (parse-tree tree_json)]
        (is (nil? errors))
        (is (= tree-newest-first comment-tree))
        (is (> (count cids) (count comments)))
        (is (empty? (->> comments
                         (map #(-> %
                                   :content
                                   (str/split #" ")
                                   second
                                   Integer/parseInt))
                         (filter #(> % level)))))))
    (testing "a limited number of comments can be fetched"
      (let [num 20
            level 2
            rsp (send-request (-> graphql :query :post-and-comments)
                              {:pid pid :sort_by "NEW"
                               :first num :level level})
            _ (is (= 200 (:status rsp)))
            {:keys [data errors]} (:body rsp)
            {:keys [tree_json comments]} (get-in data [:post_by_pid
                                                       :comment_tree])
            comment-tree (parse-tree tree_json)
            first-cid (:cid (first comment-tree))
            last-cid (:cid (last comment-tree))
            fetched-cids (map :cid comments)]
        (is (nil? errors))
        (is (= tree-newest-first comment-tree))
        (is (>= (count comments) num))
        (is (< (count comments) comment-count))
        ;; We should have info for the first comment,
        ;; but not the last comment.
        (is (some #(= first-cid %) fetched-cids))
        (is (not (some #(= last-cid %) fetched-cids)))))
    (testing "a list of comments can be fetched by cid"
      (let [first20 (take 20 cids)
            rsp (send-request (-> graphql :query :comments)
                              {:cids first20
                               :sid sid
                               :is_mod true}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= (set first20)
               (set (map :cid (:comments_by_cid data)))))))
    (testing "a single comment thread can be fetched"
      (let [cid20 (nth cids 20)
            rsp (send-request (-> graphql :query :post-and-comments)
                              {:pid pid :first 20 :level 3 :cid cid20})
            _ (is (= 200 (:status rsp)))
            {:keys [data errors]} (:body rsp)
            {:keys [tree_json comments]} (get-in data [:post_by_pid
                                                       :comment_tree])
            comment-tree (-> (cheshire/parse-string tree_json)
                             walk/keywordize-keys)]
        (is (nil? errors))
        (is (= 1 (count comment-tree)))
        (is ((set (map :cid comments)) cid20))
        (is (or (some #(= cid20 (:cid %))
                      comment-tree)
                (some #(= cid20 (:cid %))
                      (-> comment-tree first :children))))))))

(deftest test-update-content-unauthorized
  (let [{:keys [mod-uid user-uid admin-uid pid]} (test-content)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "Commentary"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        cid (get-in rsp [:body :data :create_comment :cid])]
    (testing "anonymous users"
      (testing "can't edit a comment"
        (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                {:cid cid :content "New content"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "mods and admins can't edit user's comments"
        (doseq [uid [mod-uid admin-uid]]
          (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                  {:cid cid :content "New content"}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))
        (let [rsp1 (send-request (-> graphql :query :get-single-comment)
                                 {:cid cid}
                                 (utils/request-cookies user-uid))
              comment (get-in rsp1 [:body :data :comment_by_cid])]
          (is (= {:cid cid
                  :content "Commentary"} comment)))))))

(deftest test-update-content-deleted-comment
  (let [{:keys [mod-uid user-uid pid]} (test-content)
        ds (get-in *system* [:db :ds])
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "deleted"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        cid-deleted (get-in rsp [:body :data :create_comment :cid])
        _ (jdbc/update! ds "sub_post_comment" {:status 1}
                        ["cid = ?" cid-deleted])]
    (testing "users can't edit deleted comments"
      (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                              {:cid cid-deleted :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Comment deleted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-comment)
                                 {:cid cid-deleted}
                                 (utils/request-cookies mod-uid))
              comment (get-in rsp1 [:body :data :comment_by_cid])]
          (is (= cid-deleted (:cid comment)))
          (is (= "deleted" (:content comment))))))))

(deftest test-update-content-archived-posts
  (let [{:keys [user-uid sid]} (test-content)
        ds (get-in *system* [:db :ds])
        days (-> (jdbc/query ds
                             ["select value from site_metadata where key= ?"
                              "site.archive_post_after"])
                 first
                 :value
                 parse-long)
        ms (* (inc days) 24 60 60 1000)
        rsp (send-request (-> graphql :mutation :create-test-post)
                          {:title "Test post"
                           :content "archived"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        pid (get-in rsp [:body :data :create_test_post :pid])
        rsp1 (send-request (-> graphql :mutation :create-comment)
                           {:content "archived"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        cid (get-in rsp1 [:body :data :create_comment :cid])]
    (jdbc/update! ds "sub_post" {:posted (-> (java.util.Date.)
                                             .getTime
                                             (- ms)
                                             java.sql.Timestamp.)}
                  ["pid = ?" (parse-long pid)])
    (testing "users can't edit comments on archived posts"
      (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                              {:cid cid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post archived"
               (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-comment)
                                 {:cid cid}
                                 (utils/request-cookies user-uid))
              comment (get-in rsp1 [:body :data :comment_by_cid])]
          (is (= {:cid cid :content "archived"} comment)))))))

(deftest test-update-post-content-saves-history
  (let [{:keys [mod-uid user-uid pid]} (test-content)
        ds (get-in *system* [:db :ds])
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "original"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        cid (get-in rsp [:body :data :create_comment :cid])]
    (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "1"})
    (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
              (car/flushdb))
    (testing "users can edit comment content"
      (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                              {:cid cid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (is (true? (get-in rsp [:body :data :update_comment_content])))
        (testing "but not see their own edit history"
          (let [rsp1 (send-request (-> graphql :query :get-comment-history)
                                   {:cid cid}
                                   (utils/request-cookies user-uid))
                _ (is (= 403 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                comment (:comment_by_cid data)]
            (is (= "Not authorized" (get-in errors [0 :message])))
            (is (empty? (:content_history comment)))))
        (testing "and mods can see the edit history"
          (let [rsp1 (send-request (-> graphql :query :get-comment-history)
                                   {:cid cid}
                                   (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                comment (:comment_by_cid data)]
            (is (nil? errors))
            (is (= {:cid cid
	            :content "changed"
                    :content_history [{:content "original"}]}
                   comment)))))
      (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "0"})
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))
    (testing "mods can't see edit history when config option is off"
      (let [rsp1 (send-request (-> graphql :query :get-comment-history)
                               {:cid cid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp1)))
            {:keys [data errors]} (:body rsp1)
            comment (:comment_by_cid data)]
        (is (nil? errors))
        (is (= {:cid cid
	        :content "changed"
                :content_history nil} comment))))))

(deftest test-update-comment-content-argument-checking
  (let [{:keys [user-uid pid]} (test-content)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "original"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        cid (get-in rsp [:body :data :create_comment :cid])]
    (testing "can't update a non-existent comment"
      (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                              {:cid "999999" :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not found"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "can't update comment content to"
      (testing "empty"
        (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                {:cid cid :content ""}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "just whitespace"
        (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                {:cid cid :content "       "}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too long"
        (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                {:cid cid :content
                                 (apply str (map (fn [_] "x") (range 16385)))}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "comment unchanged by failed attempts"
        (let [rsp1 (send-request (-> graphql :query :get-single-comment)
                                 {:cid cid})
              comment (get-in rsp1 [:body :data :comment_by_cid])]
          (is (= cid (:cid comment)))
          (is (= "original" (:content comment))))))))

(deftest test-mod-automatic-checkoff
  (let [{:keys [user-uid mod-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "checkoff test"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "mod comment"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        cid (get-in rsp [:body :data :create_comment :cid])]
    (testing "mod comments start out checked off"
      (let [rsp (send-request (-> graphql :query :get-comment-checkoff)
                              {:cid cid}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)
            time (get-in data [:comment_by_cid :checkoff :time])]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (some? time))
        (is (= {:comment_by_cid
	        {:cid cid
                 :checkoff
                 {:user {:uid mod-uid}
                  :time time}}} data))
        (testing "when edited the checkoff is not deleted"
          (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                  {:cid cid :content "edited mod comment"}
                                  (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp)))
                rsp2 (send-request (-> graphql :query :get-comment-checkoff)
                                   {:cid cid}
                                   (utils/request-cookies mod-uid))
                {:keys [data errors]} (:body rsp2)
                updated-time (get-in data [:comment_by_cid :checkoff :time])]
            (is (nil? errors))
            (is (some? updated-time))
            ;; Resolution is to the nearest second.
            (is (>= (parse-long updated-time) (parse-long time)))
            (is (= {:comment_by_cid
	            {:cid cid
                     :checkoff
                     {:user {:uid mod-uid}
                      :time updated-time}}} data))))))))

(deftest test-checkoff-authorization
  (let [{:keys [admin-uid user-uid mod-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "checkoff auth test"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "mod comment"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        cid1 (-> rsp :body :data :create_comment :cid)
        rsp2 (send-request (-> graphql :mutation :create-comment)
                           {:content "mod reply"
                            :parent_cid cid1
                            :pid pid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp2)))
        cid2 (-> rsp2 :body :data :create_comment :cid)
        rsp3 (send-request (-> graphql :mutation :create-comment)
                           {:content "user reply"
                            :parent_cid cid1
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp3)))
        cid3 (-> rsp3 :body :data :create_comment :cid)
        simplify-tree (fn [tree]
                        (->> tree
                             (walk/postwalk #(if (map? %)
                                               (dissoc % :time)
                                               %))
                             (s/transform [s/FIRST :children] set)))]
    (testing "when fetching comments and comment tree"
      (testing "mods and admins get checked and checkoff fields"
        (doseq [uid [admin-uid mod-uid]]
          (let [rsp (send-request (-> graphql :query
                                      :post-and-comment-checkoffs)
                                  {:pid pid :sort_by "NEW"
                                   :first 5 :level 2}
                                  (utils/request-cookies uid))
                _ (is (= 200 (:status rsp)))
                {:keys [data errors]} (:body rsp)
                _ (is (nil? errors))
                {:keys [tree_json
                        comments]} (-> data :post_by_pid :comment_tree)
                comment-tree (-> (cheshire/parse-string tree_json)
                                 walk/keywordize-keys)]
            (is (= [{:cid cid1
                     :checked true
                     :level 0
                     :status "ACTIVE"
                     :uid mod-uid
                     :children #{{:cid cid2
                                  :children []
                                  :checked true
                                  :uid mod-uid
                                  :level 1
                                  :status "ACTIVE"}
                                 {:cid cid3
                                  :children []
                                  :checked false
                                  :uid user-uid
                                  :level 1
                                  :status "ACTIVE"}}}]
                   (simplify-tree comment-tree)))
            (is (= #{{:cid cid1
                      :checkoff {:user {:uid mod-uid}}}
                     {:cid cid2
                      :checkoff {:user {:uid mod-uid}}}
                     {:cid cid3
                      :checkoff nil}}
                   (set comments))))))
      (testing "regular users don't get checked field"
        (testing "and get error querying checkoff")
        (let [rsp (send-request (-> graphql :query :post-and-comment-checkoffs)
                                {:pid pid :sort_by "NEW"
                                 :first 5 :level 2}
                                (utils/request-cookies user-uid))
              _ (is (= 403 (:status rsp)))
              {:keys [data errors]} (:body rsp)
              {:keys [tree_json
                      comments]} (-> data :post_by_pid :comment_tree)
              comment-tree (-> (cheshire/parse-string tree_json)
                               walk/keywordize-keys)]
          (is (= (get-in errors [0 :message]) "Not authorized"))
          (is (= [{:cid cid1
                   :level 0
                   :status "ACTIVE"
                   :uid mod-uid
                   :children #{{:cid cid2
                                :children []
                                :uid mod-uid
                                :level 1
                                :status "ACTIVE"}
                               {:cid cid3
                                :children []
                                :uid user-uid
                                :level 1
                                :status "ACTIVE"}}}]
                 (simplify-tree comment-tree)))
          (is (= #{{:cid cid1
                    :checkoff nil}
                   {:cid cid2
                    :checkoff nil}
                   {:cid cid3
                    :checkoff nil}}
                 (set comments)))))
      (testing "anons don't get checked field"
        (testing "and get error querying checkoff")
        (let [rsp (send-request (-> graphql :query :post-and-comment-checkoffs)
                                {:pid pid :sort_by "NEW"
                                 :first 5 :level 2})
              _ (is (= 403 (:status rsp)))
              {:keys [data errors]} (:body rsp)
              {:keys [tree_json
                      comments]} (-> data :post_by_pid :comment_tree)
              comment-tree (-> (cheshire/parse-string tree_json)
                               walk/keywordize-keys)]
          (is (= (get-in errors [0 :message]) "Not authorized"))
          (is (= [{:cid cid1
                   :level 0
                   :status "ACTIVE"
                   :uid mod-uid
                   :children #{{:cid cid2
                                :children []
                                :uid mod-uid
                                :level 1
                                :status "ACTIVE"}
                               {:cid cid3
                                :children []
                                :uid user-uid
                                :level 1
                                :status "ACTIVE"}}}]
                 (simplify-tree comment-tree)))
          (is (= #{{:cid cid1
                    :checkoff nil}
                   {:cid cid2
                    :checkoff nil}
                   {:cid cid3
                    :checkoff nil}}
                 (set comments))))))))

(deftest test-mod-checkoff
  (let [{:keys [user-uid mod-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "test checkoffs"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        mod-alt-name (str "test-" utils/name-counter "-mod-alt")
        mod-alt-uid (-> graphql :mutation :create-user
                        (send-request {:name mod-alt-name})
                        :body :data :register_user :uid)
        {:keys [status body]} (send-request (-> graphql :mutation
                                                :create-comment)
                                            {:content "comment"
                                             :parent_cid nil
                                             :pid pid}
                                            (utils/request-cookies user-uid))
        _ (is (= 200 status))
        cid (-> body :data :create_comment :cid)]
    (jdbc/insert! (-> *system* :db :ds) "sub_mod"
                  {:uid mod-alt-uid :sid sid :power_level 1 :invite false})
    (testing "regular user comments start out not checked off"
      (let [rsp (send-request (-> graphql :query :get-comment-checkoff)
                              {:cid cid}
                              (utils/request-cookies mod-alt-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:cid cid
                :checkoff nil}
               (-> data :comment_by_cid)))))
    (testing "a mod can check off a comment"
      (let [rsp (send-request (-> graphql :mutation :add-comment-checkoff)
                              {:cid cid :sid sid}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= mod-uid (-> data :checkoff_comment :user :uid)))
        (testing "and confirm they added it"
          (let [rsp2 (send-request (-> graphql :query :get-comment-checkoff)
                                   {:cid cid}
                                   (utils/request-cookies mod-uid))
                {:keys [data errors]} (:body rsp2)]
            (is (= 200 (:status rsp2)))
            (is (nil? errors))
            (is (= mod-uid (-> data :comment_by_cid :checkoff :user :uid)))))
        (testing "and a second mod can not remove that checkoff"
          (let [rsp2 (send-request (-> graphql :mutation
                                       :remove-comment-checkoff)
                                   {:cid cid :sid sid}
                                   (utils/request-cookies mod-alt-uid))
                {:keys [data errors]} (:body rsp2)]
            (is (= 200 (:status rsp2)))
            (is (nil? errors))
            (is (= mod-uid (-> data :un_checkoff_comment :user :uid))))
          (testing "verifying"
            (let [rsp3 (send-request (-> graphql :query :get-comment-checkoff)
                                     {:cid cid}
                                     (utils/request-cookies mod-uid))
                  {:keys [data errors]} (:body rsp3)]
              (is (= 200 (:status rsp3)))
              (is (nil? errors))
              (is (= mod-uid (-> data :comment_by_cid :checkoff :user :uid))))))
        (testing "and a second mod will get the first checkoff"
          (let [rsp2 (send-request (-> graphql :mutation :add-comment-checkoff)
                                   {:cid cid :sid sid}
                                   (utils/request-cookies mod-alt-uid))
                {:keys [data errors]} (:body rsp2)]
            (is (= 200 (:status rsp2)))
            (is (nil? errors))
            (is (= mod-uid (-> data :checkoff_comment :user :uid)))))
        (testing "and remove the checkoff"
          (let [rsp2 (send-request (-> graphql :mutation
                                       :remove-comment-checkoff)
                                   {:cid cid :sid sid}
                                   (utils/request-cookies mod-uid))
                {:keys [data errors]} (:body rsp2)]
            (is (= 200 (:status rsp2)))
            (is (nil? errors))
            (is (= {:un_checkoff_comment nil} data))
            (testing "and verify it was removed"
              (let [rsp3 (send-request (-> graphql :query :get-comment-checkoff)
                                       {:cid cid}
                                       (utils/request-cookies mod-uid))
                    {:keys [data errors]} (:body rsp3)]
                (is (= 200 (:status rsp3)))
                (is (nil? errors))
                (is (nil? (-> data :comment_by_cid :checkoff)))))))))))

(deftest test-checkoff-mutation-authorization
  (let [{:keys [admin-uid user-uid mod-uid sid mod2-uid sid2]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "mutation auth"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp1 (send-request (-> graphql :mutation :create-comment)
                           {:content "checked off"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        cid1 (-> rsp1 :body :data :create_comment :cid)
        rsp2 (send-request (-> graphql :mutation :create-comment)
                           {:content "not checked off"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp2)))
        cid2 (-> rsp2 :body :data :create_comment :cid)
        rsp3 (send-request (-> graphql :mutation :add-comment-checkoff)
                           {:cid cid1 :sid sid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp3)))]
    (testing "users, anons and admins"
      (doseq [uid [nil user-uid mod2-uid admin-uid]]
        (testing "can't check off a comment"
          (let [rsp (send-request (-> graphql :mutation :add-comment-checkoff)
                                  {:cid cid2 :sid sid}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
        (testing "can't remove a mod comment checkoff"
          (let [rsp (send-request (-> graphql :mutation
                                      :remove-comment-checkoff)
                                  {:cid cid1 :sid sid}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))
    (testing "mods of other subs"
      (testing "can't check off a comment not in their sub"
        (let [rsp (send-request (-> graphql :mutation :add-comment-checkoff)
                                {:cid cid2 :sid sid2}
                                (utils/request-cookies mod2-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Wrong sub" (get-in rsp [:body :errors 0 :message])))))
      (testing "can't remove a comment checkoff not in their sub"
        (let [rsp (send-request (-> graphql :mutation :remove-comment-checkoff)
                                {:cid cid1 :sid sid2}
                                (utils/request-cookies mod2-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Wrong sub"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-edit-checked-off-comment
  (let [{:keys [user-uid mod-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "testify"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp (send-request (-> graphql :mutation :create-comment)
                          {:content "original"
                           :parent_cid nil
                           :pid pid}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        cid (get-in rsp [:body :data :create_comment :cid])
        rsp2 (send-request (-> graphql :mutation :add-comment-checkoff)
                           {:cid cid :sid sid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp2)))]
    (testing "editing a comment removes the mod checkoff"
      (let [rsp3 (send-request (-> graphql :mutation :update-comment-content)
                               {:cid cid :content "changed"}
                               (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp3)))
            rsp4 (send-request (-> graphql :query :get-comment-checkoff)
                               {:cid cid}
                               (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp4)]
        (is (= 200 (:status rsp4)))
        (is (nil? errors))
        (is (nil? (-> data :comment_by_cid :checkoff)))))))

(deftest test-distinguish-authorization
  (let [{:keys [admin-uid user-uid mod-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "distinguish auth"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp1 (send-request (-> graphql :mutation :create-comment)
                           {:content "user comment"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        user-cid (-> rsp1 :body :data :create_comment :cid)
        rsp2 (send-request (-> graphql :mutation :create-comment)
                           {:content "mod comment"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp2)))
        mod-cid (-> rsp2 :body :data :create_comment :cid)
        rsp3 (send-request (-> graphql :mutation :create-comment)
                           {:content "admin comment "
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies admin-uid))
        _ (is (= 200 (:status rsp3)))
        admin-cid (-> rsp3 :body :data :create_comment :cid)]
    (testing "users and anons"
      (doseq [uid [nil user-uid]]
        (testing "can't distinguish a comment"
          (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                  {:cid user-cid :sid sid :distinguish :MOD}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))
    (testing "mods can't distinguish other's comments"
      (doseq [cid [user-cid admin-cid]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                {:cid cid :sid sid :distinguish :MOD}
                                (utils/request-cookies mod-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "admins can't distinguish other's comments"
      (doseq [cid [user-cid mod-cid]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                {:cid cid :sid sid :distinguish :ADMIN}
                                (utils/request-cookies admin-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "mods can't distinguish as admin"
      (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                              {:cid mod-cid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies mod-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "admins can't distinguish as mod"
      (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                              {:cid admin-cid :sid sid :distinguish :MOD}
                              (utils/request-cookies admin-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))))

(deftest test-distinguish
  (let [{:keys [admin-uid mod-uid user-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "distinguish"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp1 (send-request (-> graphql :mutation :create-comment)
                           {:content "mod comment"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp1)))
        mod-cid (-> rsp1 :body :data :create_comment :cid)
        rsp2 (send-request (-> graphql :mutation :create-comment)
                           {:content "admin comment "
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies admin-uid))
        _ (is (= 200 (:status rsp2)))
        admin-cid (-> rsp2 :body :data :create_comment :cid)]
    (testing "new comments are undistinguished"
      (doseq [cid [admin-cid mod-cid]]
        (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                {:cid cid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:cid cid :distinguish nil} (:comment_by_cid data))))))
    (testing "mods can distinguish as mod"
      (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                              {:cid mod-cid :sid sid :distinguish :MOD}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "MOD" (:distinguish_comment data)))
        (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                {:cid mod-cid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:cid mod-cid :distinguish "MOD"} (:comment_by_cid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                {:cid mod-cid :sid sid :distinguish nil}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_comment data)))
          (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                  {:cid mod-cid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:cid mod-cid :distinguish nil} (:comment_by_cid data)))))))
    (testing "admins can distinguish as admin"
      (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                              {:cid admin-cid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "ADMIN" (:distinguish_comment data)))
        (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                {:cid admin-cid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:cid admin-cid :distinguish "ADMIN"}
                 (:comment_by_cid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                {:cid admin-cid :sid sid :distinguish nil}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_comment data)))
          (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                  {:cid admin-cid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:cid admin-cid :distinguish nil}
                   (:comment_by_cid data)))))))
    (testing "mods who are also admins can distinguish as mod or admin"
      (utils/promote-user-to-admin! *system* mod-uid)
      (doseq [distinguish [:MOD :ADMIN]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-comment)
                                {:cid mod-cid :sid sid :distinguish distinguish}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= (name distinguish) (:distinguish_comment data)))
          (let [rsp (send-request (-> graphql :query :get-comment-distinguish)
                                  {:cid mod-cid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:cid mod-cid :distinguish (name distinguish)}
                   (:comment_by_cid data)))))))))

(deftest test-comment-update-subscription
  (let [{:keys [admin-uid mod-name mod-uid user-uid sid]} (test-content)
        pid (-> (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post"
                               :content "distinguish"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_test_post :pid)
        rsp1 (send-request (-> graphql :mutation :create-comment)
                           {:content "mod comment"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp1)))
        mod-cid (-> rsp1 :body :data :create_comment :cid)
        rsp2 (send-request (-> graphql :mutation :create-comment)
                           {:content "user comment"
                            :parent_cid nil
                            :pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp2)))
        user-cid (-> rsp2 :body :data :create_comment :cid)
        admin-session (utils/make-session admin-uid)
        mod-session (utils/make-session mod-uid)]
    (utils/with-websocket id (utils/open-websocket
                              (utils/ws-headers admin-session))

      (utils/send-init {:token (utils/sign-csrf-token admin-session)})
      (utils/expect-message {:type "connection_ack"})
      (utils/send-data {:id id
                        :type :start
                        :payload {:query (-> graphql
                                             :subscription
                                             :comment-updates
                                             utils/hash-query)
                                  :variables {:pid pid :is_mod true}}})
      (testing "distinguishing a comment streams it"
        (let [rsp2 (send-request (-> graphql :mutation :distinguish-comment)
                                 {:cid mod-cid :sid sid :distinguish :MOD}
                                 (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp2)))]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:comment_update
	                                    {:cid mod-cid
                                             :distinguish "MOD"
	                                     :content "mod comment"
                                             :status "ACTIVE"
                                             :content_history nil
                                             :checkoff
                                             {:user
                                              {:name mod-name}}}}}})))
      (testing "checking off a comment streams it"
        (let [rsp2 (send-request (-> graphql :mutation :add-comment-checkoff)
                                 {:cid user-cid :sid sid}
                                 (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp2)))]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:comment_update
	                                    {:cid user-cid
                                             :distinguish nil
	                                     :content "user comment"
                                             :status "ACTIVE"
                                             :content_history nil
                                             :checkoff
                                             {:user
                                              {:name mod-name}}}}}})))
      (testing "unchecking a comment streams it"
        (let [rsp2 (send-request (-> graphql :mutation :remove-comment-checkoff)
                                 {:cid user-cid :sid sid}
                                 (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp2)))]
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:comment_update
	                                    {:cid user-cid
                                             :distinguish nil
	                                     :content "user comment"
                                             :status "ACTIVE"
                                             :content_history nil
                                             :checkoff nil}}}}))))
    (testing "updating comment content streams it"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers mod-session))
        (utils/send-init {:token (utils/sign-csrf-token mod-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :comment-updates
                                               utils/hash-query)
                                    :variables {:pid pid :is_mod true}}})
        (let [rsp (send-request (-> graphql :mutation :update-comment-content)
                                {:cid user-cid :content "changed"}
                                (utils/request-cookies user-uid))]
          (is (= 200 (:status rsp)))
          (is (true? (get-in rsp [:body :data :update_comment_content])))
          (utils/expect-message
           {:type "data"
	    :id id
	    :payload {:data
	              {:comment_update
	               {:cid user-cid
                        :distinguish nil
	                :content "changed"
                        :status "ACTIVE"
                        :content_history [{:content "user comment"}]
                        :checkoff nil}}}}))))))
