;; gql/elements/post/resolve_test.clj -- Testing post functionality for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [dotenv :refer [env]]
   [java-time.api :as jt]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request] :as utils]
   [ovarit.app.gql.util :as util]
   [taoensso.carmine :as car]))

(def ^:private clock
  (doto (jt/mock-clock 1000 "UTC")))
(def ^:dynamic ^:private *system* nil)
(defgraphql graphql
  "test-graphql/post.graphql"
  "test-graphql/shared.graphql")

(defn system-map []
  (-> (utils/test-system-config-map)
      (assoc-in [:server :clock] clock)
      (utils/add-queries graphql)
      utils/disable-refresh-site-config
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-query-user-deleted-post
  (let [{:keys [admin-uid mod-uid mod2-uid
                user-name user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test deleted post"
                           :content "test post content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        instant (str (jt/with-clock clock
                       (-> (jt/instant) jt/to-millis-from-epoch)))
        _ (is (= 200 (:status rsp)))
        {:keys [pid]} (get-in rsp [:body :data :create_post])
        rsp2 (send-request (-> graphql :mutation :remove-post)
                           {:pid pid}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp2)))
        post {:pid pid
              :content "test post content"
	      :title "Test deleted post"
              :slug "test-deleted-post"
              :posted instant
	      :is_archived false
	      :type "TEXT"
	      :nsfw false
	      :edited nil
	      :author {:name user-name
                       :status "ACTIVE"}
	      :author_flair nil
	      :upvotes 1
	      :downvotes 0
	      :thumbnail nil
	      :comment_count 0
	      :link nil
	      :score 1
	      :flair nil
	      :distinguish nil
              :best_sort_enabled true
              :default_sort "BEST"
              :user_attributes {:vote nil}}
        ds (get-in *system* [:db :ds])]
    (testing "admin, mod can see deleted post title, user and content"
      (doseq [viewer [admin-uid mod-uid]]
        (let [rsp (send-request (-> graphql :query :get-single-post)
                                {:pid pid}
                                (utils/request-cookies viewer))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :slug nil
                                      :status "DELETED_BY_USER")}
                 data)))))
    (testing "author does not see content of self-deleted post"
      (let [rsp (send-request (-> graphql :query :get-single-post)
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (assoc post
                                    :content nil :slug nil
                                    :status "DELETED_BY_USER")} data))))
    (testing "another user does not see content or title of deleted post"
      (let [rsp (send-request (-> graphql :query :get-single-post)
                              {:pid pid}
                              (utils/request-cookies mod2-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (assoc post
                                    :content nil :title nil :slug nil
                                    :status "DELETED")}
               data))))
    (testing "anon does not see content or title of deleted post"
      (let [rsp (send-request (-> graphql :query :get-single-post-anon)
                              {:pid pid})
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (-> post
                                 (dissoc :user_attributes)
                                 (assoc :content nil :title nil :slug nil
                                        :status "DELETED"))} data))))
    (testing "when author is deleted and post is user-deleted"
      (jdbc/update! ds "public.user" {:status 10} ["uid = ?" user-uid])
      (testing "admin can see deleted user name"
        (let [rsp (send-request (-> graphql :query :get-single-post)
                                {:pid pid}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name user-name
                                               :status "DELETED"}
                                      :status "DELETED_BY_USER")} data))))
      (testing "mod can not see deleted user name"
        (let [rsp (send-request (-> graphql :query :get-single-post)
                                {:pid pid}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name nil
                                               :status "DELETED"}
                                      :status "DELETED_BY_USER")} data))))
      (testing "another user can not see deleted user name"
        (let [rsp (send-request (-> graphql :query :get-single-post)
                                {:pid pid}
                                (utils/request-cookies mod2-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name nil
                                               :status "DELETED"}
                                      :status "DELETED")} data))))
      (testing "anon does not see content or title of deleted post"
        (let [rsp (send-request (-> graphql :query :get-single-post-anon)
                                {:pid pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (-> post
                                   (dissoc :user_attributes)
                                   (assoc :content nil :title nil :slug nil
                                          :author {:name nil
                                                   :status "DELETED"}
                                          :status "DELETED"))} data)))))))

(deftest test-save-and-unsave-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for saving"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "a newly created post is not saved"
      (let [rsp (send-request (-> graphql :query :get-single-post-saved)
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (false? (get-in data [:post_by_pid :user_attributes :is_saved])))))
    (testing "a post can be saved"
      (let [rsp (send-request (-> graphql :mutation :save-post)
                              {:pid pid :save true}
                              (utils/request-cookies user-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (testing "and then is_saved will be set"
          (let [rsp (send-request (-> graphql :query :get-single-post-saved)
                                  {:pid pid}
                                  (utils/request-cookies user-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (true? (get-in data [:post_by_pid :user_attributes
                                     :is_saved])))))
        (testing "and unsaved"
          (let [rsp (send-request (-> graphql :mutation :save-post)
                                  {:pid pid :save false}
                                  (utils/request-cookies user-uid))
                {:keys [errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors)))
          (testing "and then is_saved will be unset"
            (let [rsp (send-request (-> graphql :query :get-single-post-saved)
                                    {:pid pid}
                                    (utils/request-cookies user-uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (false? (get-in data [:post_by_pid :user_attributes :is_saved]))))))))))

(defn new-post-flair-id
  "Create a post flair using a GraphQL mutation."
  [sid text mod-uid]
  (let [response (send-request (-> graphql :mutation :create-sub-post-flair)
                               {:sid sid :text text}
                               (utils/request-cookies mod-uid))
        {:keys [data]} (:body response)]
    (is (= 200 (:status response)))
    (get-in data [:create_sub_post_flair :id])))

(deftest test-set-post-flair-anonymous
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for flairs"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "flair" mod-uid)]
    (testing "anonymous users"
      (testing "can't set a post flair"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id flair-id})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "flair" (:flair post))))))
      (testing "can't delete a post flair"
        (let [rsp (send-request (-> graphql :mutation :remove-post-flair)
                                {:pid pid})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-set-post-flair-invalid-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for flairs"
                           :content "invalid"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "testflair" mod-uid)]
    (testing "users"
      (testing "can't set a flair of someone else's post"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "testflair" (:flair post))))))
      (testing "can't delete a flair from someone else's post"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp)))
              rsp1 (send-request (-> graphql :mutation :remove-post-flair)
                                 {:pid pid}
                                 (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp1)))
          (is (= "Not authorized" (get-in rsp1 [:body :errors 0 :message])))
          (let [rsp2 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp2 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "testflair" (:flair post))))))
      (testing "can't flair a post that doesn't exist"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid "11111" :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Not found"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-set-post-flair-deleted-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid :key "ucf" :value "1"})
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for flairs"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        rsp2 (send-request (-> graphql :mutation :remove-post)
                           {:pid pid-deleted}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp2)))
        flair-id (new-post-flair-id sid "testflair" mod-uid)]
    (testing "users"
      (testing "can't flair deleted posts"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid-deleted :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Bad Request" (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid-deleted}
                                   (utils/request-cookies user-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid-deleted (:pid post)))
            (is (nil? (:flair post)))))))
    (testing "mods can flair deleted posts"
      (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                              {:pid pid-deleted :flair_id flair-id}
                              (utils/request-cookies mod-uid))]
        (is (= 200 (:status rsp)))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid-deleted}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "testflair" (:flair post))))))))

(deftest test-set-post-flair-archived-posts
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid
                                           :key "ucf"
                                           :value "1"})
        days (-> (jdbc/query ds
                             ["select value from site_metadata where key= ?"
                              "site.archive_post_after"])
                 first :value parse-long)
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for flairs"
                            :content "archived"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "original" mod-uid)
        flair-id2 (new-post-flair-id sid "new" mod-uid)
        rsp2 (send-request (-> graphql :mutation :set-post-flair)
                           {:pid pid :flair_id flair-id}
                           (utils/request-cookies user-uid))]
    (is (= 200 (:status rsp2)))
    (jt/advance-clock! clock (jt/days (+ days 1)))
    (testing "users and mods"
      (testing "can't flair archived posts"
        (doseq [uid [user-uid admin-uid mod-uid]]
          (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                  {:pid pid :flair_id flair-id2}
                                  (utils/request-cookies uid))]
            (is (= 400 (:status rsp)))
            (is (= "Post archived"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                     {:pid pid}
                                     (utils/request-cookies uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "original" (:flair post)))))))
      (testing "can't remove flair from archived posts"
        (doseq [uid [user-uid admin-uid mod-uid]]
          (let [rsp (send-request (-> graphql :mutation :remove-post-flair)
                                  {:pid pid}
                                  (utils/request-cookies uid))]
            (is (= 400 (:status rsp)))
            (is (= "Post archived"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                     {:pid pid}
                                     (utils/request-cookies uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "original" (:flair post))))))))))

(deftest test-set-post-flair-permissions
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/delete! ds "sub_metadata" ["sid = ? and key in (?, ?)",
                                           sid, "ucf" "umf"])
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for flairs"
                            :content "user-must-flair"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "test" mod-uid)]
    (testing "users"
      (testing "can't flair a post unless sub permissions are set"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
      (jdbc/insert! ds "sub_metadata" {:sid sid :key "umf" :value "1"})
      (testing "can flair a post when user_must_flair is set"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 200 (:status rsp))))
        (testing "but can't remove it"
          (let [rsp (send-request (-> graphql :mutation :remove-post-flair)
                                  {:pid pid}
                                  (utils/request-cookies user-uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                     {:pid pid}
                                     (utils/request-cookies user-uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "test" (:flair post))))))
        (testing "and a mod can remove it"
          (let [rsp (send-request (-> graphql :mutation :remove-post-flair)
                                  {:pid pid}
                                  (utils/request-cookies mod-uid))]
            (is (= 200 (:status rsp)))
            (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                     {:pid pid}
                                     (utils/request-cookies user-uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (nil? (:flair post))))))))))

(deftest test-create-post-with-flair-permissions
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/delete! ds "sub_metadata" ["sid = ? and key in (?, ?)",
                                           sid, "ucf" "umf"])
        flair-id (new-post-flair-id sid "test" mod-uid)
        mod-flair-id (new-post-flair-id sid "modsonly" mod-uid)
        rsp (send-request (-> graphql :mutation :update-flair)
                          {:sid sid :id mod-flair-id
                           :mods_only true
                           :post_types ["TEXT"]}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))]
    (testing "users can't create a flaired post unless sub permissions are set"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "Flair permissions"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :flair_id flair-id
                               :type :TEXT}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Flair not permitted"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "mods can create a flaired post when sub permissions are not set"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "Flair permissions"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :flair_id flair-id
                               :type :TEXT}
                              (utils/request-cookies mod-uid))]
        (is (= 200 (:status rsp)))
        (is (string? (get-in rsp [:body :data :create_post :pid])))))
    (jdbc/insert! ds "sub_metadata" {:sid sid :key "umf" :value "1"})
    (testing "users must create flaired posts when user_must_flair is set"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "Flair permissions"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Flair required"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "mods can create an unflaired post when user_must_flair is set"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "Flair permissions"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies mod-uid))]
        (is (= 200 (:status rsp)))
        (is (string? (get-in rsp [:body :data :create_post :pid])))))
    (testing "users can't create a flaired post with a mods-only flair"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "mods only flair"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :flair_id mod-flair-id
                               :type :TEXT}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Flair not permitted"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "mods can create a flaired post with a mods-only flair"
      (let [rsp (send-request (-> graphql :mutation :create-post)
                              {:title "mods only flair"
                               :content "testing"
                               :sid sid
                               :nsfw false
                               :flair_id mod-flair-id
                               :type :TEXT}
                              (utils/request-cookies mod-uid))]
        (is (= 200 (:status rsp)))
        (is (string? (get-in rsp [:body :data :create_post :pid])))))
    (testing "if a flair is restricted by post type"
      (testing "users can't use it to create a restricted type of post"
        (let [rsp (send-request (-> graphql :mutation :create-post)
                                {:title "text only"
                                 :link "https://example.com"
                                 :sid sid
                                 :nsfw false
                                 :flair_id flair-id
                                 :type :LINK}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (nil? (:body rsp)))
          (is (= "Flair not permitted"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "mods can use it to create a restricted type of post"))))

(deftest test-set-post-flair-valid-flair
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid :key "ucf" :value "1"})
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for flairs"
                           :content "testing"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        mod-flair-id (new-post-flair-id sid "mods-only" mod-uid)
        poll-flair-id (new-post-flair-id sid "polls-only" mod-uid)]
    (testing "users and mods can't add a flair that does not exist"
      (doseq [uid [admin-uid mod-uid user-uid]]
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id "999"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Flair not found" (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid}
                                   (utils/request-cookies uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (nil? (:flair post)))))))
    (testing "users can't use a mods only flair"
      (let [rsp (send-request (-> graphql :mutation :update-flair)
                              {:sid sid :id mod-flair-id
                               :mods_only true
                               :post_types ["TEXT"]}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (= {:data {:update_sub_post_flair nil}} (:body rsp)))
            rsp (send-request (-> graphql :mutation :set-post-flair)
                              {:pid pid :flair_id mod-flair-id}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (nil? (:flair post)))))
      (testing "but mods can"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id mod-flair-id}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp)))
          (is (= {:data {:set_post_flair {:id mod-flair-id :text "mods-only"}}}
                 (:body rsp)))
          (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "mods-only" (:flair post)))))))
    (testing "users can't use a flair not permitted on the post type"
      (let [rsp (send-request (-> graphql :mutation :remove-post-flair)
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            rsp (send-request (-> graphql :mutation :update-flair)
                              {:sid sid :id poll-flair-id
                               :mods_only false
                               :post_types ["POLL"]}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (= {:data {:update_sub_post_flair nil}} (:body rsp)))
            rsp (send-request (-> graphql :mutation :set-post-flair)
                              {:pid pid :flair_id poll-flair-id}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Flair not permitted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (nil? (:flair post)))))
      (testing "but mods can"
        (let [rsp (send-request (-> graphql :mutation :set-post-flair)
                                {:pid pid :flair_id poll-flair-id}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp)))
          (is (= {:data {:set_post_flair {:id poll-flair-id :text "polls-only"}}}
                 (:body rsp)))
          (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "polls-only" (:flair post)))))))))

(deftest test-update-content-unauthorized
  (let [{:keys [mod-uid user-uid admin-uid
                sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "anonymous users"
      (testing "can't edit a post"
        (let [rsp (send-request (-> graphql :mutation :update-post-content)
                                {:pid pid :content "New content"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "New content" (:content post))))))
      (testing "mods and admins can't edit user's posts"
        (doseq [uid [mod-uid admin-uid]]
          (let [rsp (send-request (-> graphql :mutation :update-post-content)
                                  {:pid pid :content "New content"}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))
        (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "anon" (:content post))))))))

(deftest test-update-content-deleted-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update content"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        rsp2 (send-request (-> graphql :mutation :remove-post)
                           {:pid pid-deleted
                            :reason "test"}
                           (utils/request-cookies mod-uid))]
    (is (= 200 (:status rsp2)))
    (is (nil? (-> rsp2 :body :errors)))
    (testing "users can't edit deleted posts"
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid-deleted :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post deleted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "deleted" (:content post))))))))

(deftest test-update-content-archived-posts
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        days (-> (jdbc/query ds
                             ["select value from site_metadata where key= ?"
                              "site.archive_post_after"])
                 first :value parse-long)
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for update content"
                            :content "archived"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (jt/advance-clock! clock (jt/days (+ days 1)))
    (testing "users can't edit archived posts"
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post archived"
               (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "archived" (:content post))))))))

(deftest test-update-content-length-limits
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for update content"
                            :content "content"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (testing "post content must be above site minimum length"
      (jdbc/update! ds "site_metadata"
                    {:value "10"}
                    ["key = ?" "site.text_post.min_length"])
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid :content "123456789"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Content too short"
               (get-in rsp [:body :errors 0 :message]))))
      (jdbc/update! ds "site_metadata"
                    {:value "0"}
                    ["key = ?" "site.text_post.min_length"]))
    (testing "post content must be above sub minimum length"
      (jdbc/insert! ds "sub_metadata" {:sid sid
                                       :key "text_post_min_length"
                                       :value "15"})
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid :content "12345678901"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Content too short"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "post content must be below site maximum length"
      (jdbc/update! ds "site_metadata"
                    {:value "20"}
                    ["key = ?" "site.text_post.max_length"])
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid :content
                               "12345678901234567890123"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Content too long"
               (get-in rsp [:body :errors 0 :message]))))
      (jdbc/update! ds "site_metadata"
                    {:value "65535"}
                    ["key = ?" "site.text_post.max_length"]))))

(deftest test-update-post-content-saves-history
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update content"
                           :content "original"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "1"})
    (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
              (car/flushdb))
    (testing "users can edit post content"
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid pid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (is (true? (get-in rsp [:body :data :update_post_content])))
        (testing "but not see their own edit history"
          (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                _ (is (= 403 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (= "Not authorized" (get-in errors [0 :message])))
            (is (empty? (:content_history post)))))
        (testing "and mods can see the edit history"
          (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (nil? errors))
            (is (= {:pid pid
	            :content "changed"
	            :title "Test post for update content"
                    :slug "test-post-for-update-content"
	            :content_history [{:content "original"}]
	            :title_history nil} post)))))
      (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "0"})
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))
    (testing "mods can't see edit history when config option is off"
      (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp1)))
            {:keys [data errors]} (:body rsp1)
            post (:post_by_pid data)]
        (is (nil? errors))
        (is (= {:pid pid
	        :content "changed"
	        :title "Test post for update content"
                :slug "test-post-for-update-content"
	        :content_history nil
	        :title_history nil} post))))))

(deftest test-update-post-content-argument-checking
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update content args"
                           :content "content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "can't update a non-existent post"
      (let [rsp (send-request (-> graphql :mutation :update-post-content)
                              {:pid "999999" :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not found"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "can't update post content to"
      (testing "empty"
        (let [rsp (send-request (-> graphql :mutation :update-post-content)
                                {:pid pid :content ""}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "just whitespace"
        (let [rsp (send-request (-> graphql :mutation :update-post-content)
                                {:pid pid :content "       "}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too long"
        (let [rsp (send-request (-> graphql :mutation :update-post-content)
                                {:pid pid :content
                                 (apply str (map (fn [_] "x") (range 65536)))}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "post unchanged by failed attempts"
        (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "content" (:content post))))))))

(deftest test-update-title-unauthorized
  (let [{:keys [mod2-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "anonymous users"
      (testing "can't edit a post title"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title "New title"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "Test post" (:title post))))))
      (testing "other users can't edit user's titles"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title "New title"}
                                (utils/request-cookies mod2-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message]))))
        (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post" (:title post))))))))

(deftest test-update-title-deleted-post
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update title"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        rsp2 (send-request (-> graphql :mutation :remove-post)
                           {:pid pid-deleted
                            :reason "by admin"}
                           (utils/request-cookies admin-uid))]
    (is (= 200 (:status rsp2)))
    (testing "users can't edit deleted post titles"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid pid-deleted :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post deleted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "Test post for update title" (:title post))))))
    (testing "mods can edit deleted post titles"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid pid-deleted :title "changed"
                               :reason "rules"}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "changed" (:title post))))))))

(deftest test-update-title-time-limit
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        seconds (-> (jdbc/query ds
                                ["select value from site_metadata where key= ?"
                                 "site.title_edit_timeout"])
                    first :value parse-long)
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for update title"
                            :content "timeout"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (jt/advance-clock! clock (jt/seconds (inc seconds)))
    (testing "users can't edit titles on posts older than timeout"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid pid :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Time expired"
               (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post for update title" (:title post))))))
    (testing "mods can edit titles on posts older than timeout"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid pid :title "mod change"
                               :reason "moderation"}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (let [rsp1 (send-request (-> graphql :query :get-single-post)
                                 {:pid pid}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "mod change" (:title post))))))))

(deftest test-update-title-notifications
  (let [{:keys [mod-uid user-uid sid
                sub-name]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request (-> graphql :mutation :create-post)
                           {:title "Test post for update title"
                            :content "notifications"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        user-session (utils/make-session user-uid)]
    (testing "mod edit title"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql :subscription
                                               :notification-counts
                                               utils/hash-query)}})
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title "mod change"
                                 :reason "moderation"}
                                (utils/request-cookies mod-uid))
              {:keys [errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (testing "notifies user"
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:notification_counts
	                             {:unread_message_count 1
	                              :unread_notification_count 0}}}}))
          (testing "sends a USER_NOTIFICATION message"
            (let [rsp (send-request (-> graphql :query :direct-message-inbox) {}
                                    (utils/request-cookies user-uid))
                  _ (is (= 200 (:status rsp)))
                  {:keys [data errors]} (:body rsp)
                  msg (-> data :direct_message_inbox :edges first :node)
                  content (:content msg)]
              (is (nil? errors))
              (is (= {:mtype "USER_NOTIFICATION"
	              :unread false
                      :sender nil}
                     (dissoc msg :content)))
              (is (str/includes? content (str "/o/" sub-name)))
              (is (str/includes? content (str "/o/" sub-name "/" pid "/"
                                              "mod-change")))
              (is (str/includes? content "Reason: moderation")))))))))

(deftest test-update-post-title-saves-history
  (let [{:keys [mod-uid user-name user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        seconds (-> (jdbc/query ds
                                ["select value from site_metadata where key= ?"
                                 "site.title_edit_timeout"])
                    first :value parse-long)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update title"
                           :content "original"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (jt/advance-clock! clock (jt/seconds (dec seconds)))
    (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "1"})
    (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
              (car/flushdb))
    (testing "users can edit post title"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid pid :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (is (true? (get-in rsp [:body :data :update_post_title])))
        (testing "but not see their own edit history"
          (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                _ (is (= 403 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (= "Not authorized" (get-in errors [0 :message])))
            (is (empty? (:title_history post)))))
        (testing "and mods can see the edit history"
          (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (nil? errors))
            (is (= {:pid pid
	            :content "original"
	            :title "changed"
                    :slug "changed"
	            :title_history [{:content "Test post for update title"
                                     :user {:name user-name}}]
	            :content_history nil} post)))))
      (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "0"})
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))
    (testing "mods can't see edit history when config option is off"
      (let [rsp1 (send-request (-> graphql :query :get-single-post-history)
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp1)))
            {:keys [data errors]} (:body rsp1)
            post (:post_by_pid data)]
        (is (nil? errors))
        (is (= {:pid pid
	        :title "changed"
	        :content "original"
                :slug "changed"
	        :content_history nil
	        :title_history nil} post))))))

(deftest test-update-post-title-argument-checking
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post for update title args"
                           :content "args"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "can't change title of a non-existent post"
      (let [rsp (send-request (-> graphql :mutation :update-post-title)
                              {:pid "abcde" :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Invalid argument: pid"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "can't update post title to"
      (testing "empty"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title ""}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "just whitespace"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title "       "}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too short"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title "a"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too long"
        (let [rsp (send-request (-> graphql :mutation :update-post-title)
                                {:pid pid :title
                                 (apply str (map (fn [_] "x") (range 300)))}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "title unchanged by failed attempts"
        (let [rsp1 (send-request (-> graphql :query :get-single-post-anon)
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post for update title args" (:title post))))))))

(deftest test-query-post-reports
  (let [{:keys [user-uid mod-uid mod2-uid admin-uid
                sid]} (utils/mod-user-and-sub *system*)]
    (testing "create a post with a report"
      (let [rsp1 (send-request (-> graphql :mutation :create-post)
                               {:title "Test post"
                                :content "testify"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp1)))
            _ (is (nil? (get-in rsp1 [:body :errors])))
            pid (get-in rsp1 [:body :data :create_post :pid])
            _ (is (some? pid))
            rsp2 (send-request (-> graphql :mutation :create-test-post-report)
                               {:reason "test reason"
                                :send_to_admin false
                                :pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp2)))
            _ (is (nil? (get-in rsp2 [:body :errors])))
            rid (get-in rsp2 [:body :data :create_report :id])
            _ (is (some? rid))]
        (testing "and admins and mods can fetch the post with the report"
          (doseq [uid [mod-uid admin-uid]]
            (let [rsp (send-request (-> graphql :query
                                        :get-single-post-with-reports)
                                    {:pid pid}
                                    (utils/request-cookies uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (= {:post_by_pid {:pid pid
                                    :open_reports [{:id rid
                                                    :rtype "POST"}]}}
                     data)))))
        (testing "and authors, users and anons cannot fetch the report"
          (doseq [uid [nil user-uid mod2-uid]]
            (let [rsp (send-request (-> graphql :query
                                        :get-single-post-with-reports)
                                    {:pid pid}
                                    (utils/request-cookies uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 403 (:status rsp)))
              (is (= "Not authorized" (get-in errors [0 :message])))
              (is (empty? (get-in data [:post_by_pid :open_reports]))))))))))

(deftest test-post-viewed
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request (-> graphql :mutation :create-test-post-viewed)
                           {:title "Test post"
                            :content "testify"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies mod-uid))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (is (= 200 (:status rsp1)))
    (is (nil? (get-in rsp1 [:body :errors])))
    (is (some? pid))
    (is (= (get-in rsp1 [:body :data :create_post :posted])
           (get-in rsp1 [:body :data :create_post :user_attributes
                         :viewed])))
    (testing "new post is marked viewed for author"
      (let [rsp (send-request (-> graphql :query :get-single-post-viewed)
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (= (get-in data [:post_by_pid :posted])
               (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "new post is marked unviewed for other users"
      (let [rsp (send-request (-> graphql :query :get-single-post-viewed)
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (nil? (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "anons can't mark a post viewed"
      (let [rsp (send-request (-> graphql :mutation :update-post-viewed)
                              {:pid pid})]
        (is (= 403 (:status rsp)))))
    (testing "marking a viewed post viewed again updates the datetime"
      (jt/advance-clock! clock (jt/seconds 1))
      (let [rsp (send-request (-> graphql :mutation :update-post-viewed)
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            rsp1 (send-request (-> graphql :query :get-single-post-viewed)
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp1)]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (string? (get-in data [:post_by_pid :user_attributes :viewed])))
        (is (< (-> data :post_by_pid :posted parse-long)
               (-> data :post_by_pid :user_attributes :viewed parse-long)))))
    (testing "other users can mark a post viewed"
      (let [rsp (send-request (-> graphql :mutation :update-post-viewed)
                              {:pid pid}
                              (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp)))
            rsp1 (send-request (-> graphql :query :get-single-post-viewed)
                               {:pid pid}
                               (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp1)]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (string? (get-in data [:post_by_pid :user_attributes :viewed])))))))

(deftest test-distinguish-authorization
  (let [{:keys [admin-uid user-uid mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        user-pid (-> (send-request (-> graphql :mutation :create-post)
                                   {:title "user post"
                                    :content "test"
                                    :sid sid
                                    :nsfw false
                                    :type :TEXT}
                                   (utils/request-cookies user-uid))
                     :body :data :create_post :pid)
        mod-pid (-> (send-request (-> graphql :mutation :create-post)
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        admin-pid (-> (send-request (-> graphql :mutation :create-post)
                                    {:title "mod post"
                                     :content "test"
                                     :sid sid
                                     :nsfw false
                                     :type :TEXT}
                                    (utils/request-cookies admin-uid))
                      :body :data :create_post :pid)]
    (is (and user-pid mod-pid admin-pid))
    (testing "users and anons"
      (doseq [uid [nil user-uid]]
        (testing "can't distinguish a post"
          (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                  {:pid user-pid :sid sid :distinguish :MOD}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))
    (testing "mods can't distinguish other's posts"
      (doseq [pid [user-pid admin-pid]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                {:pid pid :sid sid :distinguish :MOD}
                                (utils/request-cookies mod-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "admins can't distinguish other's posts"
      (doseq [pid [user-pid mod-pid]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                {:pid pid :sid sid :distinguish :ADMIN}
                                (utils/request-cookies admin-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "mods can't distinguish as admin"
      (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                              {:pid mod-pid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies mod-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "admins can't distinguish as mod"
      (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                              {:pid admin-pid :sid sid :distinguish :MOD}
                              (utils/request-cookies admin-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))))

(deftest test-distinguish
  (let [{:keys [admin-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-pid (-> (send-request (-> graphql :mutation :create-post)
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        admin-pid (-> (send-request (-> graphql :mutation :create-post)
                                    {:title "mod post"
                                     :content "test"
                                     :sid sid
                                     :nsfw false
                                     :type :TEXT}
                                    (utils/request-cookies admin-uid))
                      :body :data :create_post :pid)]
    (is (and mod-pid admin-pid))
    (testing "new posts are undistinguished"
      (doseq [pid [admin-pid mod-pid]]
        (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                {:pid pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid pid :distinguish nil} (:post_by_pid data))))))
    (testing "mods can distinguish as mod"
      (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                              {:pid mod-pid :sid sid :distinguish :MOD}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "MOD" (:distinguish_post data)))
        (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                {:pid mod-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid mod-pid :distinguish "MOD"} (:post_by_pid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                {:pid mod-pid :sid sid :distinguish nil}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_post data)))
          (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :distinguish nil} (:post_by_pid data)))))))
    (testing "admins can distinguish as admin"
      (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                              {:pid admin-pid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "ADMIN" (:distinguish_post data)))
        (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                {:pid admin-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid admin-pid :distinguish "ADMIN"}
                 (:post_by_pid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                {:pid admin-pid :sid sid :distinguish nil}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_post data)))
          (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                  {:pid admin-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid admin-pid :distinguish nil}
                   (:post_by_pid data)))))))
    (testing "mods who are also admins can distinguish as mod or admin"
      (utils/promote-user-to-admin! *system* mod-uid)
      (doseq [distinguish [:MOD :ADMIN]]
        (let [rsp (send-request (-> graphql :mutation :distinguish-post)
                                {:pid mod-pid :sid sid :distinguish distinguish}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= (name distinguish) (:distinguish_post data)))
          (let [rsp (send-request (-> graphql :query :get-post-distinguish)
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :distinguish (name distinguish)}
                   (:post_by_pid data)))))))))

(deftest test-nsfw-authorization
  (let [{:keys [user-uid user-name mod2-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid (-> (send-request (-> graphql :mutation :create-post)
                              {:title "user post"
                               :content "nsfw"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_post :pid)
        extra-user-uid (-> graphql :mutation :create-user
                           (send-request {:name (str user-name "-extra")})
                           (get-in [:body :data :register_user :uid]))]
    (is (and pid extra-user-uid))
    (testing "anons, other users and mods of other subs"
      (doseq [uid [nil mod2-uid extra-user-uid]]
        (testing "can't change post nsfw"
          (let [rsp (send-request (-> graphql :mutation :set-post-nsfw)
                                  {:pid pid :nsfw true}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))))

(deftest test-set-nsfw
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        mod-pid (-> (send-request (-> graphql :mutation :create-post)
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        user-pid (-> (send-request (-> graphql :mutation :create-post)
                                   {:title "user post"
                                    :content "test"
                                    :sid sid
                                    :nsfw false
                                    :type :TEXT}
                                   (utils/request-cookies user-uid))
                     :body :data :create_post :pid)]
    (testing "a mod can change nsfw tag on mod and user posts"
      (doseq [pid [mod-pid user-pid]]
        (let [rsp (send-request (-> graphql :mutation :set-post-nsfw)
                                {:pid pid :nsfw true}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (:set_post_nsfw data))
          (let [rsp (send-request (-> graphql :query :get-post-nsfw)
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :nsfw true} (:post_by_pid data)))))))
    (testing "an admin can change nsfw tag on mod and user posts"
      (doseq [pid [mod-pid user-pid]]
        (let [rsp (send-request (-> graphql :mutation :set-post-nsfw)
                                {:pid pid :nsfw false}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (false? (:set_post_nsfw data)))
          (let [rsp (send-request (-> graphql :query :get-post-nsfw)
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :nsfw false} (:post_by_pid data)))))))
    (testing "a user can change nsfw tag on their post"
      (let [rsp (send-request (-> graphql :mutation :set-post-nsfw)
                              {:pid user-pid :nsfw true}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (:set_post_nsfw data))
        (let [rsp (send-request (-> graphql :query :get-post-nsfw)
                                {:pid user-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid user-pid :nsfw true} (:post_by_pid data))))))))

(deftest test-remove-post-authorization
  (let [{:keys [user-uid user-name mod2-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid (-> (send-request (-> graphql :mutation :create-post)
                              {:title "user post"
                               :content "whatever"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_post :pid)
        extra-user-uid (-> graphql :mutation :create-user
                           (send-request {:name (str user-name "-extra")})
                           (get-in [:body :data :register_user :uid]))]
    (is (and pid extra-user-uid))
    (testing "anons, other users and mods of other subs cannot remove a post"
      (doseq [uid [nil mod2-uid extra-user-uid]]
        (let [rsp (send-request (-> graphql :mutation :remove-post)
                                {:pid pid :reason "reason"}
                                (utils/request-cookies uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message]))))))
    (testing "a user can remove their own post"
      (let [rsp (send-request (-> graphql :mutation :remove-post)
                              {:pid pid}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp))))
      (testing "if it hasn't already been removed"
        (let [rsp (send-request (-> graphql :mutation :remove-post)
                                {:pid pid}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Post deleted"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-remove-post-reason-requirement
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid (-> (send-request (-> graphql :mutation :create-post)
                              {:title "user post"
                               :content "whatever"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_post :pid)]
    (is pid)
    (testing "mods and admins must supply a reason to remove a post"
      (doseq [uid [mod-uid admin-uid]]
        (doseq [{:keys [reason error]} [{:reason nil
                                         :error "Reason required"}
                                        {:reason ""
                                         :error "Reason required"}
                                        {:reason "              "
                                         :error "Reason required"}
                                        {:reason "xx"
                                         :error "Reason too short"}
                                        {:reason (util/rand-str 2000)
                                         :error "Reason too long"}]]
          (let [rsp (send-request (-> graphql :mutation :remove-post)
                                  {:pid pid :reason reason}
                                  (utils/request-cookies uid))]
            (is (= 400 (:status rsp)))
            (is (= error
                   (get-in rsp [:body :errors 0 :message])))))))))

(deftest test-remove-link-post-time-limit
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid1 (-> (send-request (-> graphql :mutation :create-test-post)
                               {:title "link post"
                                :link "https://example.com"
                                :sid sid
                                :nsfw false
                                :type :LINK}
                               (utils/request-cookies user-uid))
                 :body :data :create_test_post :pid)
        pid2 (-> (send-request (-> graphql :mutation :create-test-post)
                               {:title "link post too"
                                :link "https://example.org"
                                :sid sid
                                :nsfw false
                                :type :LINK}
                               (utils/request-cookies user-uid))
                 :body :data :create_test_post :pid)]
    (is pid1)
    (is pid2)
    (jt/advance-clock! clock (jt/days 1))  ; Default timeout is 30 minutes.
    (testing "the author can't remove a link post after the time limit"
      (let [rsp (send-request (-> graphql :mutation :remove-post)
                              {:pid pid1}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Time expired"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "mods and admins can remove link posts after the time limit"
      (doseq [{:keys [uid pid]} [{:uid admin-uid :pid pid1}
                                 {:uid mod-uid :pid pid2}]]
        (let [rsp (send-request (-> graphql :mutation :remove-post)
                                {:pid pid
                                 :reason "reason"}
                                (utils/request-cookies uid))]
          (is (= 200 (:status rsp)))
          (is (nil? (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-mod-remove-post-notifications
  (testing "users are notified when their post is removed"
    (let [{:keys [admin-uid mod-uid user-uid sid
                  sub-name]} (utils/mod-user-and-sub *system*)
          user-session (utils/make-session user-uid)
          pid1 (-> (send-request (-> graphql :mutation :create-test-post)
                                 {:title "link post"
                                  :link "https://example.com"
                                  :sid sid
                                  :nsfw false
                                  :type :LINK}
                                 (utils/request-cookies user-uid))
                   :body :data :create_test_post :pid)
          pid2 (-> (send-request (-> graphql :mutation :create-test-post)
                                 {:title "link post too"
                                  :link "https://example.org"
                                  :sid sid
                                  :nsfw false
                                  :type :LINK}
                                 (utils/request-cookies user-uid))
                   :body :data :create_test_post :pid)]
      (is (and pid1 pid2))
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql :subscription
                                               :notification-counts
                                               utils/hash-query)}})
        (testing "by a mod"
          (let [rsp (send-request (-> graphql :mutation :remove-post)
                                  {:pid pid1
                                   :reason "mod reason"}
                                  (utils/request-cookies mod-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:notification_counts
	                             {:unread_message_count 1
	                              :unread_notification_count 0}}}})))
        (testing "by an admin"
          (let [rsp (send-request (-> graphql :mutation :remove-post)
                                  {:pid pid2
                                   :reason "admin reason"}
                                  (utils/request-cookies admin-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:notification_counts
	                             {:unread_message_count 2
	                              :unread_notification_count 0}}}})))
        (testing "by USER_NOTIFICATION messages"
          (let [rsp (send-request (-> graphql :query :direct-message-inbox) {}
                                  (utils/request-cookies user-uid))
                _ (is (= 200 (:status rsp)))
                {:keys [data errors]} (:body rsp)
                [admin-msg mod-msg] (->> data :direct_message_inbox :edges
                                         (map :node))]
            (is (nil? errors))
            (is (= "USER_NOTIFICATION" (:mtype mod-msg)))
            (is (str/includes? (:content mod-msg)
                               (format "The moderators of /o/%s " sub-name)))
            (is (str/includes? (:content mod-msg) "Reason: mod reason"))
            (is (= "USER_NOTIFICATION" (:mtype admin-msg)))
            (is (str/includes? (:content admin-msg) "The site administrators "))
            (is (str/includes? (:content admin-msg)
                               "Reason: admin reason"))))))))

(deftest test-remove-post-updates
  (testing "when a post is removed"
    (let [{:keys [admin-uid mod-uid user-uid
                  sid]} (utils/mod-user-and-sub *system*)
          mod-session (utils/make-session mod-uid)
          user-session (utils/make-session user-uid)
          pid1 (-> (send-request (-> graphql :mutation :create-test-post)
                                 {:title "link post"
                                  :link "https://example.com"
                                  :sid sid
                                  :nsfw false
                                  :type :LINK}
                                 (utils/request-cookies user-uid))
                   :body :data :create_test_post :pid)
          pid2 (-> (send-request (-> graphql :mutation :create-test-post)
                                 {:title "link post too"
                                  :link "https://example.org"
                                  :sid sid
                                  :nsfw false
                                  :type :LINK}
                                 (utils/request-cookies user-uid))
                   :body :data :create_test_post :pid)
          ds (get-in *system* [:db :ds])]
      (is pid1 pid2)
      (jdbc/insert! ds "site_metadata" {:key "site.show_votes" :value "0"})
      (testing "a mod receives a full update"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers mod-session))
          (utils/send-init {:token (utils/sign-csrf-token mod-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql :subscription
                                                 :post-update
                                                 utils/hash-query)
                                      :variables {:pid (str pid1)}}})
          (let [rsp (send-request (-> graphql :mutation :remove-post)
                                  {:pid pid1
                                   :reason "admin reason"}
                                  (utils/request-cookies admin-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:post_update
	                             {:pid pid1
	                              :status "DELETED_BY_ADMIN"
	                              :score 1
	                              :upvotes 1
	                              :downvotes 0
	                              :comment_count 0
	                              :locked false}}}}))))
      (testing "a user receives a redacted update"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers user-session))
          (utils/send-init {:token (utils/sign-csrf-token user-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql :subscription
                                                 :post-update
                                                 utils/hash-query)
                                      :variables {:pid (str pid2)}}})
          (let [rsp (send-request (-> graphql :mutation :remove-post)
                                  {:pid pid2
                                   :reason "admin reason"}
                                  (utils/request-cookies admin-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:post_update
	                             {:pid pid2
	                              :status "DELETED_BY_MOD"
	                              :score 1
	                              :upvotes nil
	                              :downvotes nil
	                              :comment_count 0
	                              :locked false}}}}))))
      (jdbc/insert! ds "site_metadata" {:key "site.show_votes" :value "1"}))))

(deftest test-mod-remove-post-sublogs
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid1 (-> (send-request (-> graphql :mutation :create-post)
                               {:title "post 1"
                                :content "test content"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
                 :body :data :create_post :pid)
        pid2 (-> (send-request (-> graphql :mutation :create-post)
                               {:title "post 2"
                                :content "more test content"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
                 :body :data :create_post :pid)]
    (is (string? pid1))
    (is (string? pid2))
    (jt/advance-clock! clock (jt/hours 1))
    (testing "a sublog entry is created when"
      (testing "a mod removes a post"
        (let [rsp1 (send-request (-> graphql :mutation :remove-post)
                                 {:pid pid1
                                  :reason "mod reason"}
                                 (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp1)))
              rsp2 (send-request (-> graphql :query :sublog-entries)
                                 {:sid sid}
                                 (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp2)))
          (is (nil? (-> rsp2 :body :errors)))
          (is (= {:action "DELETE_POST"
	          :user {:uid mod-uid}
	          :desc "mod reason"
	          :link (str "/p/" pid1)
	          :target_user nil
                  :admin false}
                 (-> rsp2 :body :data :sublog_entries :edges first :node
                     (dissoc :time))))))
      (jt/advance-clock! clock (jt/hours 1))
      (testing "an admin removes a post"
        (let [rsp1 (send-request (-> graphql :mutation :remove-post)
                                 {:pid pid2
                                  :reason "admin reason"}
                                 (utils/request-cookies admin-uid))
              _               (is (= 200 (:status rsp1)))
              rsp2 (send-request (-> graphql :query :sublog-entries)
                                 {:sid sid}
                                 (utils/request-cookies admin-uid))]
          (is (= 200 (:status rsp2)))
          (is (nil? (-> rsp2 :body :errors)))
          (is (= {:action "DELETE_POST"
	          :user {:uid admin-uid}
	          :desc "admin reason"
	          :link (str "/p/" pid2)
	          :target_user nil
                  :admin true}
                 (-> rsp2 :body :data :sublog_entries :edges first :node
                     (dissoc :time)))))))))

(deftest test-remove-sticky-post
  (testing "a removed sticky post"
    (let [{:keys [mod-uid sid]} (utils/mod-user-and-sub *system*)
          pid (-> (send-request (-> graphql :mutation :create-post)
                                {:title "Sticky"
                                 :content "mod content"
                                 :sid sid
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies mod-uid))
                  :body :data :create_post :pid)
          rsp1 (-> (send-request (-> graphql :mutation :stick-post)
                                 {:pid pid
                                  :sid sid}
                                 (utils/request-cookies mod-uid)))
          _ (is (= 200 (:status rsp1)))
          _ (is (= {:pid pid :sticky true}
                   (-> (send-request (-> graphql :query :get-post-sticky)
                                     {:pid pid})
                       :body :data :post_by_pid)))
          _ (jt/advance-clock! clock (jt/hours 1))
          rsp2 (send-request (-> graphql :mutation :remove-post)
                             {:pid pid}
                             (utils/request-cookies mod-uid))
          _ (is (= 200 (:status rsp2)))]
      (testing "is no longer sticky"
        (is (= {:pid pid :sticky false}
               (-> (send-request (-> graphql :query :get-post-sticky)
                                 {:pid pid})
                   :body :data :post_by_pid))))
      (testing "has a sublog entry"
        (let [entry (-> (send-request (-> graphql :query :sublog-entries)
                                      {:sid sid}
                                      (utils/request-cookies mod-uid))
                        :body :data :sublog_entries :edges
                        first :node)]
          (is (= {:action "STICKY_DEL"
	          :user {:uid mod-uid}
	          :desc nil
	          :link (str "/p/" pid)
	          :target_user nil
	          :admin false}
                 (dissoc entry :time))))))))

(deftest test-remove-announcement-post
  (testing "a removed announcement post"
    (let [{:keys [admin-uid sid]} (utils/mod-user-and-sub *system*)
          pid (-> (send-request (-> graphql :mutation :create-post)
                                {:title "Announcement"
                                 :content "admin content"
                                 :sid sid
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies admin-uid))
                  :body :data :create_post :pid)
          rsp (-> (send-request (-> graphql :mutation :set-announcement-post)
                                {:pid pid}
                                (utils/request-cookies admin-uid)))
          _ (is (= 200 (:status rsp)))
          _ (is (= pid
                   (-> (send-request (-> graphql :query :announcement-post-id)
                                     {})
                       :body :data :announcement_post_id)))
          rsp2 (send-request (-> graphql :mutation :remove-post)
                             {:pid pid}
                             (utils/request-cookies admin-uid))
          _ (is (= 200 (:status rsp2)))]
      (testing "is no longer the announcement"
        (is (= {:announcement_post_id nil}
               (-> (send-request (-> graphql :query :announcement-post-id)
                                 {})
                   :body :data)))))))

(deftest test-create-upload-url-authorization
  (let [{:keys [user-uid]} (utils/mod-user-and-sub *system*)]
    (testing "anons can't create an upload url"
      (let [rsp (send-request (-> graphql :mutation :create-upload-url) {})]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "low-level users can't create an upload url"
      (let [rsp (send-request (-> graphql :mutation :create-upload-url)
                              {}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))))

(deftest test-create-post-authorization
  (testing "anons can't post")
  (testing "if banned in a sub"
    (testing "users cannot post")
    (testing "admins can still post"))
  (testing "if a sub is restricted"
    (testing "users cannot post")
    (testing "admins can still post")))

(deftest test-posting-limits
  (testing "posting limits are enforced"
    (let [{:keys [admin-uid mod-uid sid sid2
                  user-uid]} (utils/mod-user-and-sub *system*)
          ds (get-in *system* [:db :ds])
          sub-limit 5
          site-limit 8]
      (jt/advance-clock! clock (jt/days 2))
      (jdbc/update! ds "site_metadata"
                    {:value (str sub-limit)}
                    ["key = ?" "site.daily_sub_posting_limit"])
      (jdbc/update! ds "site_metadata"
                    {:value (str site-limit)}
                    ["key = ?" "site.daily_site_posting_limit"])
      (doseq [uid [admin-uid mod-uid user-uid]]
        (dorun (for [i (range sub-limit)]
                 (let [rsp (send-request (-> graphql :mutation :create-post)
                                         {:title (str "sub 1 test post #" i)
                                          :content "some text"
                                          :sid sid
                                          :nsfw false
                                          :type :TEXT}
                                         (utils/request-cookies uid))]
                   (is (= 200 (:status rsp)))
                   (is (string?
                        (get-in rsp [:body :data :create_post :pid]))))))
        (let [rsp (send-request (-> graphql :mutation :create-post)
                                {:title "one too many"
                                 :content "some text"
                                 :sid sid
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies uid))]
          (is (= 400 (:status rsp)))
          (is (= "Too many recent posts"
                 (get-in rsp [:body :errors 0 :message]))))
        (dorun (for [i (range (- site-limit sub-limit))]
                 (let [rsp (send-request (-> graphql :mutation :create-post)
                                         {:title (str "sub 2 test post #" i)
                                          :content "some text"
                                          :sid sid2
                                          :nsfw false
                                          :type :TEXT}
                                         (utils/request-cookies uid))]
                   (is (= 200 (:status rsp)))
                   (is (string?
                        (get-in rsp [:body :data :create_post :pid]))))))
        (let [rsp (send-request (-> graphql :mutation :create-post)
                                {:title "one too many"
                                 :content "some text"
                                 :sid sid2
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies uid))]
          (is (= 400 (:status rsp)))
          (is (= "Too many recent posts"
                 (get-in rsp [:body :errors 0 :message]))))
        (jt/advance-clock! clock (jt/days 1))
        (let [rsp (send-request (-> graphql :mutation :create-post)
                                {:title "the next day"
                                 :content "some text"
                                 :sid sid2
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies uid))]
          (is (= 200 (:status rsp)))
          (is (string? (get-in rsp [:body :data :create_post :pid]))))))))

(deftest test-sitewide-posting-disabled
  (let [{:keys [admin-uid mod-uid sid
                user-uid]} (utils/mod-user-and-sub *system*)]
    (jt/advance-clock! clock (jt/days 1))
    (testing "when posting is disabled sitewide"
      (jdbc/update! (:ds (:db *system*)) "site_metadata"
                    {:value (str "0")} ["key = ?" "site.enable_posting"])
      (testing "mods and users cannot post"
        (doseq [uid [mod-uid user-uid]]
          (let [rsp (send-request (-> graphql :mutation :create-post)
                                  {:title "test post"
                                   :link "https://example.com"
                                   :sid sid
                                   :nsfw false
                                   :type :LINK}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Posting disabled"
                   (get-in rsp [:body :errors 0 :message]))))))
      (testing "admins can still post"
        (let [rsp (send-request (-> graphql :mutation :create-post)
                                {:title "test text post"
                                 :content "admin says"
                                 :sid sid
                                 :nsfw false
                                 :type :TEXT}
                                (utils/request-cookies admin-uid))]
          (is (= 200 (:status rsp)))
          (is (string? (get-in rsp [:body :data :create_post :pid])))))
      (jdbc/update! (:ds (:db *system*)) "site_metadata"
                    {:value (str "1")} ["key = ?" "site.enable_posting"]))))

(deftest test-create-text-post
  (let [{:keys [user-name user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :create-post)
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        instant (str (jt/with-clock clock
                       (-> (jt/instant) jt/to-millis-from-epoch)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (is (= 200 (:status rsp)))
    (testing "anon user can query a post"
      (let [rsp (send-request (-> graphql :query :get-single-post-anon)
                              {:pid pid})
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid {:content "testify"
                              :posted instant
	                      :is_archived false
	                      :type "TEXT"
	                      :title "Test post"
                              :slug "test-post"
	                      :nsfw false
	                      :edited nil
	                      :author {:name user-name
                                       :status "ACTIVE"}
	                      :author_flair nil
	                      :pid pid
	                      :upvotes 1
	                      :downvotes 0
	                      :thumbnail nil
	                      :status "ACTIVE"
	                      :comment_count 0
	                      :link nil
	                      :score 1
	                      :flair nil
	                      :distinguish nil
                              :best_sort_enabled true
                              :default_sort "BEST"}}
               data))))
    (testing "author can query a post"
      (let [rsp (send-request (-> graphql :query :get-single-post)
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid {:content "testify"
                              :posted instant
	                      :is_archived false
	                      :type "TEXT"
	                      :title "Test post"
                              :slug "test-post"
	                      :nsfw false
	                      :edited nil
	                      :author {:name user-name
                                       :status "ACTIVE"}
	                      :author_flair nil
	                      :pid pid
	                      :upvotes 1
	                      :downvotes 0
	                      :thumbnail nil
	                      :status "ACTIVE"
	                      :comment_count 0
	                      :link nil
	                      :score 1
	                      :flair nil
	                      :distinguish nil
                              :best_sort_enabled true
                              :default_sort "BEST"
                              :user_attributes {:vote nil}}}
               data))))))

(deftest test-create-upload-post)

(deftest test-create-link-post)

(deftest test-create-poll-post
  ;; test subs start out disallowing polls
  (testing "users cannot create a post when disallowed in post type config (the default)")
  )
