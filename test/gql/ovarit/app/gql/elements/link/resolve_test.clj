;; gql/elements/link/resolve_test.clj -- Testing url metadata for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.link.resolve-test
  (:require
   [clj-http.fake :refer [with-global-fake-routes]]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [hiccup.page :refer [html5]]
   [ovarit.app.gql.test-utils :refer [defgraphql] :as utils]))

(def ^:dynamic ^:private *system* nil)
(defgraphql graphql "test-graphql/link.graphql")

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries graphql)
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-request-url-metadata-checks
  (let [{:keys [user-uid]} (utils/mod-user-and-sub *system*)
        user-session (utils/make-session user-uid)
        anon-session (utils/make-session nil)
        hashed-query (-> graphql :subscription :url-metadata-result
                         utils/hash-query)]
    (testing "Anons can't request URL metadata"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers anon-session))
        (utils/send-init {:token (utils/sign-csrf-token anon-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query hashed-query
                                    :variables {:url "https://example.com"}}})
        (let [{:keys [type payload]} (utils/<message!! 100)
              {:keys [data errors]} payload]
          (is (= type "data"))
          (is (= {:url_metadata nil} data))
          (is (= "Not authorized" (get-in errors [0 :message]))))))
    (testing "Requesting bad URLs results in an error"
      (doseq  [url ["not an url" "file://ftp.example.com"]]
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers user-session))
          (utils/send-init {:token (utils/sign-csrf-token user-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query hashed-query
                                      :variables {:url url}}})
          (let [{:keys [type payload]} (utils/<message!! 100)
                {:keys [data errors]} payload]
            (is (= type "data"))
            (is (= {:url_metadata nil} data))
            (is (= "Invalid URL" (get-in errors [0 :message])))))))))

(deftest test-request-url-metadata
  (let [{:keys [user-uid]} (utils/mod-user-and-sub *system*)
        user-session (utils/make-session user-uid)
        url "https://example.com/hello-world"]
    (with-global-fake-routes
        {url
         (fn [_request]
           {:status 200
            :headers {"Content-Type" "text/html"}
            :body (html5 [:head
                          [:title "Hello world"]
                          [:meta {:property "og:description"
                                  :content "Greetings"}]]
                    [:body])})}
      (testing "Users can request URL metadata"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers user-session))
          (utils/send-init {:token (utils/sign-csrf-token user-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql :subscription
                                                 :url-metadata-result
                                                 utils/hash-query)
                                      :variables {:url url}}})
          (let [{:keys [type payload]} (utils/<message!! 200)
                {:keys [data errors]} payload]
            (is (= type "data"))
            (is (= {:url_metadata {:title "Hello world"
                                   :description "Greetings"}} data))
            (is (nil? errors))))))))
