;; gql/elements/message/resolve_test.clj -- Testing messaging functionality for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.message.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [java-time.api :as jt]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request] :as utils]
   [ovarit.app.gql.util :as util]))

(def ^:private clock (jt/mock-clock 0 "UTC"))
(def ^:dynamic ^:private *system* nil)
(defgraphql graphql
  "test-graphql/message.graphql"
  "test-graphql/shared.graphql")

(defn system-map []
  (-> (utils/test-system-config-map #{:webserver :taskrunner})
      (assoc-in [:server :clock] clock)
      (assoc-in [:job-queue :clock] clock)
      (utils/add-queries graphql)
      utils/test-system-map
      utils/disable-refresh-site-config))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system*
              (component/start-system (utils/test-system (system-map)))]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-create-modmail
  (let [{:keys [user-uid user-name mod-uid mod-name
                sid]} (utils/mod-user-and-sub *system*)
        user-session (utils/make-session user-uid)]
    (testing "create new modmail"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :notification-counts
                                               utils/hash-query)}})
        (let [response (send-request (-> graphql :mutation :create-new-modmail)
                                     {:subject "Welcome"
                                      :content "Modmail!"
                                      :sid sid
                                      :username user-name
                                      :show_mod_username true}
                                     (utils/request-cookies mod-uid))]
          (is (= 200 (:status response)))
          (is (= {:data
	          {:create_modmail_message
	           {:content "Modmail!"
                    :mtype "MOD_TO_USER_AS_USER"
                    :sender {:name mod-name}
                    :receiver {:name user-name}}}}
                 (-> (:body response)
                     (update-in [:data :create_modmail_message]
                                dissoc :thread_id))))
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 1
	                                     :unread_notification_count 0
	                                     :sub_notifications []}}}}))))
    (testing "with non-existent sub"
      (let [response
            (send-request (-> graphql :mutation :create-new-modmail)
                          {:subject "Welcome"
                           :content "Modmail!"
                           :sid "clearly a bogus sid"
                           :username user-name
                           :show_mod_username true}
                          (utils/request-cookies mod-uid))]
        (is (= 403 (:status response)))
        (is (nil? (get-in response [:body :data :create_modmail_message])))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))))

(deftest test-create-modmail-with-report
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)]
    (testing "create a modmail thread with a linked report"
      (let [rsp1 (send-request (-> graphql :mutation :create-test-post)
                               {:title "Test post"
                                :content "testify"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp1)))
            _ (is (nil? (get-in rsp1 [:body :errors])))
            pid (get-in rsp1 [:body :data :create_test_post :pid])
            _ (is (some? pid))
            rsp2 (send-request (-> graphql :mutation :create-test-post-report)
                               {:reason "test reason"
                                :send_to_admin false
                                :pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp2)))
            _ (is (nil? (get-in rsp2 [:body :errors])))
            rid (get-in rsp2 [:body :data :create_report :id])
            _ (is (some? rid))
            rsp3 (send-request (-> graphql :mutation
                                   :create-new-modmail-with-report)
                               {:subject "Your report"
                                :content "feedback"
                                :sid sid
                                :username user-name
                                :show_mod_username false
                                :report_type "POST"
                                :report_id rid}
                               (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp3)
            mtid (get-in data [:create_modmail_message :thread_id])]
        (is (= 200 (:status rsp3)))
        (is (nil? errors))
        (is (= "MOD_TO_USER_AS_MOD"
               (get-in rsp3 [:body :data :create_modmail_message :mtype])))
        (testing "and fetch it with the report"
          (let [rsp (send-request (-> graphql :query
                                      :modmail-thread-by-id-with-linkage)
                                  {:thread_id mtid}
                                  (utils/request-cookies mod-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:message_thread_by_id
                    {:log
                     [{:action "RELATED_REPORT"
                       :report_id rid
                       :rtype "POST"}]}}
                   data))))))))

(deftest test-create-linked-modmail
  (let [{:keys [user-uid user-name mod-uid sub-name
                sid]} (utils/mod-user-and-sub *system*)]
    (testing "create a modmail thread linked to another"
      (jdbc/insert! (:ds (:db *system*)) "site_metadata"
                    {:key "site.admin_sub" :value sub-name})
      (jdbc/update! (:ds (:db *system*)) "site_metadata"
                    {:value (str "1")} ["key = ?" "site.downvotes.count"])
      (let [rsp (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post 1"
                               :content "testify"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (nil? (get-in rsp [:body :errors])))
            pid1 (get-in rsp [:body :data :create_test_post :pid])
            rsp (send-request (-> graphql :mutation :create-test-post)
                              {:title "Test post 2"
                               :content "testify"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (nil? (get-in rsp [:body :errors])))
            pid2 (get-in rsp [:body :data :create_test_post :pid])]
        (is (some? pid1))
        (is (some? pid2))
        ;; Send an upvote first to beat the negative score check.
        (send-request (-> graphql :mutation :vote-post)
                      {:pid pid1 :vote "UP"}
                      (utils/request-cookies user-uid))
        (send-request (-> graphql :mutation :vote-post)
                      {:pid pid2 :vote "DOWN"}
                      (utils/request-cookies user-uid))
        ;; Allow time for the downvoting check to run.
        (Thread/sleep 150)
        (let [rsp (send-request (-> graphql :query :modmail-threads)
                                {:first 1
                                 :sids [sid]
                                 :category "MOD_NOTIFICATIONS"
                                 :unread_only false}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)
              _ (is (= 200 (:status rsp)))
              _ (is (nil? errors))
              source-thread-id (get-in data
                                       [:modmail_threads :edges 0 :node :id])]
          (is (string? source-thread-id))
          (let [rsp (send-request (-> graphql :mutation :create-linked-modmail)
                                  {:subject "linked message"
                                   :content "content"
                                   :sid sid
                                   :username user-name
                                   :show_mod_username false
                                   :reference_thread_id source-thread-id}
                                  (utils/request-cookies mod-uid))

                {:keys [data errors]} (:body rsp)
                _ (is (= 200 (:status rsp)))
                _ (is (nil? errors))
                target-thread-id (-> data :create_modmail_message :thread_id)]
            (testing "and fetch it with the linked thread id"
              (let [rsp (send-request (-> graphql :query
                                          :modmail-thread-by-id-with-linkage)
                                      {:thread_id target-thread-id}
                                      (utils/request-cookies mod-uid))
                    {:keys [data errors]} (:body rsp)]
                (is (= 200 (:status rsp)))
                (is (nil? errors))
                (is (= {:message_thread_by_id
                        {:log
                         [{:action "RELATED_THREAD"
                           :related_thread_id source-thread-id
                           :reference_type "NOTIFICATION"}]}}
                       data))))))))
    (jdbc/delete! (:ds (:db *system*)) "site_metadata"
                  ["key = ?" "site.admin_sub"])))

(deftest test-query-all-modmails
  (testing "Retrieve modmails:"
    (let [{:keys [mod-uid user-uid user-name
                  sid]} (utils/mod-user-and-sub *system*)
          how-many 10  ; Use an even number.
          make-subject #(format "message %d" %)
          create-modmail #(send-request (-> graphql :mutation
                                            :create-new-modmail)
                                        {:subject (make-subject %)
                                         :content "Modmail!"
                                         :sid sid
                                         :username user-name
                                         :show_mod_username true}
                                        (utils/request-cookies mod-uid))
          modmails (map create-modmail (range how-many))
          statuses (map :status modmails)
          errors (map #(get-in % [:body :errors]) modmails)]
      (testing "first create some"
        (is (= (repeat how-many 200) statuses))
        (is (= (repeat how-many nil) errors)))

      ;; The recipient isn't a mod so can't use the modmails query.
      (testing "try query as non-mod")
      (let [response (send-request (-> graphql :query :modmail-threads)
                                   {:first 10
                                    :sids [sid]
                                    :category "ALL"
                                    :unread_only false}
                                   (utils/request-cookies user-uid))]
        (is (= (:status response) 403))
        (is (= (get-in response [:body :errors 0 :message]) "Not authorized")))

      ;; Try a mod query to get all the threads.
      (testing "query requesting more than available"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first (* 2 how-many)
                                      :sids [sid]
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              page-info (get-in data [:modmail_threads :pageInfo])]
          (is (= (:status response) 200))
          (is (not (:hasNextPage page-info)))
          (is (= (map #(make-subject %) (range (dec how-many) -1 -1))
                 (map #(get-in % [:node :subject]) edges)))))

      ;; Try to get all the threads using pagination.
      (testing "query using pagination -"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first (/ how-many 2)
                                      :sids [sid]
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              page-info (get-in data [:modmail_threads :pageInfo])]
          (testing "getting the first half"
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (:hasNextPage page-info))
            (is (= (/ how-many 2) (count edges)))
            (is (:endCursor page-info))
            (is (= (:cursor (last edges)) (:endCursor page-info)))
            (is (= (map #(make-subject %)
                        (range (dec how-many) (dec (/ how-many 2)) -1))
                   (map #(get-in % [:node :subject]) edges))))
          (let [response (send-request
                          (-> graphql :query :modmail-threads)
                          {:first (/ how-many 2)
                           :after (:endCursor page-info)
                           :sids [sid]
                           :category "ALL"
                           :unread_only false}
                          (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)
                edges (get-in data [:modmail_threads :edges])
                page-info (get-in data [:modmail_threads :pageInfo])]
            (testing "getting the second half"
              (is (= 200 (:status response)))
              (is (nil? errors))
              (is (not (:hasNextPage page-info)))
              (is (= (/ how-many 2) (count edges)))
              (is (= (:cursor (last edges)) (:endCursor page-info)))
              (is (= (map #(make-subject %)
                          (range (dec (/ how-many 2)) -1 -1))
                     (map #(get-in % [:node :subject]) edges))))
            (let [response (send-request
                            (-> graphql :query :modmail-threads)
                            {:first how-many
                             :after (:endCursor page-info)
                             :sids [sid]
                             :category "ALL"
                             :unread_only false}
                            (utils/request-cookies mod-uid))]
              (testing "asking for more when at the end"
                (is (= 200 (:status response)))
                (is (= {:data {:modmail_threads nil}}
                       (:body response)))))))))))

(deftest test-modmail-query-with-empty-sid-list
  (testing "Empty sub list defaults to all the subs"
    (let [{:keys [mod-uid user-uid user-name
                  sid]} (utils/mod-user-and-sub *system*)
          mm (send-request (-> graphql :mutation :create-new-modmail)
                           {:subject "query-empty"
                            :content "Modmail!"
                            :username user-name
                            :sid sid
                            :show_mod_username false}
                           (utils/request-cookies mod-uid))]
      (is (= 200 (:status mm)))
      (testing "- query by mod"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 100
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              edges (get-in response [:body :data :modmail_threads
                                      :edges])]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (some #(= (get-in % [:node :subject]) "query-empty") edges))))
      (testing "- query by non-mod user"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies user-uid))]
          (is (= 403 (:status response)))
          (is (= "Not authorized"
                 (get-in response [:body :errors 0 :message])))
          (is (= {:modmail_threads nil}
                 (get-in response [:body :data]))))))))

(deftest test-contact-mods
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)]
    (testing "create new message to mods"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers mod-session))
        (utils/send-init {:token (utils/sign-csrf-token mod-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (-> graphql
                                               :subscription
                                               :notification-counts
                                               utils/hash-query)}})
        (let [response (send-request (-> graphql :mutation :contact-mods)
                                     {:subject "Random complaint"
                                      :content "Do better!"
                                      :sid sid}
                                     (utils/request-cookies user-uid))]
          (is (= 200 (:status response)))
          (is (= {:data
	          {:create_message_to_mods
	           {:content "Do better!"
                    :mtype "USER_TO_MODS"
                    :sender {:name user-name}
                    :receiver nil}}}
                 (:body response)))
          (utils/expect-message
           {:type "data"
            :id id
            :payload {:data
	              {:notification_counts
	               {:unread_message_count 0
	                :unread_notification_count 0
	                :sub_notifications
	                [{:sid sid
	                  :open_report_count 0
	                  :unread_modmail_count 1
                          :new_unread_modmail_count 1
	                  :in_progress_unread_modmail_count 0
	                  :all_unread_modmail_count 1
	                  :discussion_unread_modmail_count 0
	                  :notification_unread_modmail_count 0}]}}}})))
      (testing "with non-existent sub"
        (let [response
              (send-request (-> graphql :mutation :contact-mods)
                            {:subject "Hello"
                             :content "Anybody there?"
                             :sid "clearly a bogus sid"}
                            (utils/request-cookies user-uid))]
          (is (= 400 (:status response)))
          (is (nil? (get-in response [:body :data :create_message_to_mods])))
          (is (= "Sub does not exist"
                 (get-in response [:body :errors 0 :message]))))))))

(deftest test-modmail-stats
  (testing "modmail stats for mod dashboard")
  (let [{:keys [mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)]
    (send-request (-> graphql :mutation :contact-mods)
                  {:subject "Random complaint"
                   :content "Do better!"
                   :sid sid}
                  (utils/request-cookies user-uid))
    (testing "- non-mods can't query"
      (let [response (send-request (-> graphql :query :new-modmail-count)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            {:keys [data errors]} (:body response)]
        (is (= 403 (:status response)))
        (is (= "Not authorized" (get-in errors [0 :message])))
        ;; Lacinia fills in a 0 here.
        (is (not= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (not= 1 (get-in data [:sub_by_name :new_modmail_count])))
        (is (not= 1 (get-in data [:sub_by_name :unread_modmail_count])))))
    (testing "- mods get results"
      (let [response (send-request (-> graphql :query :new-modmail-count)
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body response)]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :new_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :unread_modmail_count])))))
    (testing "- admins get results"
      (utils/promote-user-to-admin! *system* user-uid)
      (let [response (send-request (-> graphql :query
                                       :new-modmail-count-for-admins)
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            {:keys [data errors]} (:body response)]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :new_modmail_count])))))))

(deftest test-modmail-categories
  (let [{:keys [mod-uid user-uid user-name
                sid]} (utils/mod-user-and-sub *system*)
        user2mod (send-request (-> graphql :mutation :contact-mods)
                               {:subject "Random complaint"
                                :content "Do better!"
                                :sid sid}
                               (utils/request-cookies user-uid))
        mod2user (send-request (-> graphql :mutation :create-new-modmail)
                               {:subject "Welcome"
                                :content "Modmail!"
                                :sid sid
                                :username user-name
                                :show_mod_username false}
                               (utils/request-cookies mod-uid))
        mod2mod (send-request (-> graphql :mutation :create-new-modmail)
                              {:subject "Talk"
                               :content "Discussion!"
                               :sid sid}
                              (utils/request-cookies mod-uid))]
    (is (= 200 (:status user2mod)))
    (is (= 200 (:status mod2user)))
    (is (= 200 (:status mod2mod)))
    (testing "Querying all modmails doesn't fetch mod discussions"
      (let [response (send-request (-> graphql :query :modmail-threads)
                                   {:first 10
                                    :sids []
                                    :category "ALL"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Random complaint" "Welcome"} subjects))))
    (testing "Querying new modmails gets ones from users without replies."
      (let [response (send-request (-> graphql :query :modmail-threads)
                                   {:first 10
                                    :sids []
                                    :category "NEW"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Random complaint"} subjects))))
    (testing "Querying in progress modmails gets ones sent by mods and
              ones sent by users with no replies."
      (let [response (send-request (-> graphql :query :modmail-threads)
                                   {:first 10
                                    :sids []
                                    :category "IN_PROGRESS"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Welcome"} subjects))))
    (testing "Querying mod discussion gets only discussion messages."
      (let [response (send-request (-> graphql :query :modmail-threads)
                                   {:first 10
                                    :sids []
                                    :category "MOD_DISCUSSIONS"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Talk"} subjects))))))

(deftest test-mark-read-and-unread
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)
        response (send-request (-> graphql :mutation :contact-mods-mid)
                               {:subject "Random complaint"
                                :content "Do better!"
                                :sid sid}
                               (utils/request-cookies user-uid))
        {:keys [data errors]} (:body response)]
    (is (= 200 (:status response)))
    (is (nil? errors))
    (let [mid (get-in data [:create_message_to_mods :mid])]
      (testing "a message starts out unread for the receiver"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only true}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is unread)))

      (testing "the receiver can mark a message read"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers mod-session))
          (utils/send-init {:token (utils/sign-csrf-token mod-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql
                                                 :subscription
                                                 :notification-counts
                                                 utils/hash-query)}})
          (let [response (send-request (-> graphql :mutation :update-unread)
                                       {:mid mid
                                        :unread false}
                                       (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)]
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (= mid (:update_message_unread data))))
          (utils/expect-message
           {:type "data"
            :id id
            :payload {:data
	              {:notification_counts
	               {:unread_message_count 0,
	                :unread_notification_count 0,
	                :sub_notifications
	                [{:sid sid
	                  :open_report_count 0
	                  :unread_modmail_count 0
                          :new_unread_modmail_count 0
	                  :in_progress_unread_modmail_count 0
	                  :all_unread_modmail_count 0
	                  :discussion_unread_modmail_count 0
	                  :notification_unread_modmail_count 0}]}}}})))
      (testing "the read message flag is set correctly"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is (not unread))))

      (testing "a query for unread messages doesn't fetch read ones"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only true}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= {:modmail_threads nil} data))))

      (testing "the receiver can mark a message unread"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers mod-session))
          (utils/send-init {:token (utils/sign-csrf-token mod-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (-> graphql
                                                 :subscription
                                                 :notification-counts
                                                 utils/hash-query)}})
          (let [response (send-request (-> graphql :mutation :update-unread)
                                       {:mid mid
                                        :unread true}
                                       (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)]
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (= mid (:update_message_unread data))))
          (utils/expect-message
           {:type "data"
            :id id
            :payload {:data
	              {:notification_counts
	               {:unread_message_count 0,
	                :unread_notification_count 0,
	                :sub_notifications
	                [{:sid sid
	                  :open_report_count 0
	                  :unread_modmail_count 1
                          :new_unread_modmail_count 1
	                  :in_progress_unread_modmail_count 0
	                  :all_unread_modmail_count 1
	                  :discussion_unread_modmail_count 0
	                  :notification_unread_modmail_count 0}]}}}})))

      (testing "a message marked unread is unread"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is unread))))))

(deftest test-queries-on-unarchived-messages
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        r1 (send-request (-> graphql :mutation :contact-mods-mid-thread)
                         {:subject "Random complaint"
                          :content "Do better!"
                          :sid sid}
                         (utils/request-cookies user-uid))
        r2 (send-request (-> graphql :mutation :create-new-modmail-mid-thread)
                         {:subject "Welcome"
                          :content "Modmail!"
                          :sid sid
                          :username user-name
                          :show_mod_username true}
                         (utils/request-cookies mod-uid))
        msg1 (get-in r1 [:body :data :create_message_to_mods])
        msg2 (get-in r2 [:body :data :create_modmail_message])]
    (is (= 200 (:status r1)))
    (is (= 200 (:status r2)))
    (is (nil? (get-in r1 [:body :errors])))
    (is (nil? (get-in r2 [:body :errors])))

    (testing "messages start out in the inbox"
      (let [r1 (send-request (-> graphql :query :message-thread-by-id-mailbox)
                             {:thread_id (:thread_id msg1)}
                             (utils/request-cookies mod-uid))
            thread1 (get-in r1 [:body :data :message_thread_by_id])
            r2 (send-request (-> graphql :query :message-thread-by-id-mailbox)
                             {:thread_id (:thread_id msg2)}
                             (utils/request-cookies mod-uid))
            thread2 (get-in r2 [:body :data :message_thread_by_id])]
        (is (= 200 (:status r1)))
        (is (nil? (get-in r1 [:body :errors])))
        (is (= "INBOX" (:mailbox thread1)))
        (is (= 200 (:status r2)))
        (is (nil? (get-in r2 [:body :errors])))
        (is (= "INBOX" (:mailbox thread2)))))

    (testing "new messages start out unarchived"
      (testing "- query for all messages finds them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 2 (count edges)))
          (is (= "Welcome" (get-in edges [0 :node :subject])))
          (is (= "Random complaint" (get-in edges [1 :node :subject])))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg2)
                  :content "Modmail!"
                  :mtype "MOD_TO_USER_AS_USER"
                  :unread false}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [1 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [1 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for new messages finds them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 1 (count edges)))
          (is (= "Random complaint" (get-in edges [0 :node :subject])))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for archived messages does not find them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ARCHIVED"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data))))))))

(deftest test-queries-on-archived-messages
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        r1 (send-request (-> graphql :mutation :contact-mods-mid-thread)
                         {:subject "Random complaint"
                          :content "Do better!"
                          :sid sid}
                         (utils/request-cookies user-uid))
        r2 (send-request (-> graphql :mutation :create-new-modmail-mid-thread)
                         {:subject "Welcome"
                          :content "Modmail!"
                          :sid sid
                          :username user-name
                          :show_mod_username true}
                         (utils/request-cookies mod-uid))
        msg1 (get-in r1 [:body :data :create_message_to_mods])
        msg2 (get-in r2 [:body :data :create_modmail_message])]
    (is (= 200 (:status r1)))
    (is (= 200 (:status r2)))
    (is (nil? (get-in r1 [:body :errors])))
    (is (nil? (get-in r2 [:body :errors])))

    (testing "messages can be archived"
      (let [r1 (send-request (-> graphql :mutation :update-modmail-mailbox)
                             {:thread_id (:thread_id msg1)
                              :mailbox "ARCHIVED"}
                             (utils/request-cookies mod-uid))
            r2 (send-request (-> graphql :mutation :update-modmail-mailbox)
                             {:thread_id (:thread_id msg2)
                              :mailbox "ARCHIVED"}
                             (utils/request-cookies mod-uid))]
        (is (= 200 (:status r1)))
        (is (= 200 (:status r2)))
        (is (nil? (get-in r1 [:body :errors])))
        (is (nil? (get-in r2 [:body :errors]))))
      (testing "- query for archived messages finds them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ARCHIVED"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 2 (count edges)))
          (is (= "Welcome" (get-in edges [0 :node :subject])))
          (is (= sid (get-in edges [0 :node :sub :sid])))
          (is (= {:mid (:mid msg2)
                  :content "Modmail!"
                  :mtype "MOD_TO_USER_AS_USER"
	          :unread false}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [1 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [1 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for new messages does not find them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data)))))
      (testing "- query for all messages does not find them"
        (let [response (send-request (-> graphql :query :modmail-threads)
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data))))))))

(deftest test-modmail-thread-query
  (let [{:keys [user-uid user2-uid user-name mod-uid mod-name
                sid sub-name]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request (-> graphql :mutation :contact-mods-mid-thread)
                           {:subject "Random complaint"
                            :content "Do better!"
                            :sid sid}
                           (utils/request-cookies user-uid))
        msg1 (get-in rsp1 [:body :data :create_message_to_mods])
        mid1 (:mid msg1)
        rsp2 (send-request (-> graphql :mutation :create-modmail-reply)
                           {:thread_id (:thread_id msg1)
                            :content "Here's your answer!"
                            :send_to_user true
                            :show_mod_username true}
                           (utils/request-cookies mod-uid))
        msg2 (get-in rsp2 [:body :data :create_modmail_reply])
        mid2 (:mid msg2)]
    (is (= 200 (:status rsp1)))
    (is (= 200 (:status rsp2)))
    (is (nil? (get-in rsp1 [:body :errors])))
    (is (map? (:body rsp2)))
    (is (nil? (get-in rsp2 [:body :errors])))
    (is (= {:content "Here's your answer!"
            :mid mid2
            :mtype "MOD_TO_USER_AS_USER"
            :receiver {:name user-name}
            :sender {:name mod-name}}
           msg2))

    (testing "modmail_thread query"
      (let [rsp1 (send-request (-> graphql :query :modmail-thread)
                               {:first 10
                                :thread_id (:thread_id msg1)}
                               (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body rsp1)
            edges (get-in data [:modmail_thread :edges])]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= 2 (count edges)))
        (is (= {:mid mid2
                :content "Here's your answer!"
                :unread false}
               (get-in edges [0 :node])))
        (is (= {:mid mid1 :content "Do better!"
                :unread true}
               (get-in edges [1 :node])))))
    (testing "modmail_thread_by_id query"
      (testing "works for mod"
        (let [rsp (send-request (-> graphql :query :modmail-thread-by-id)
                                {:thread_id (:thread_id msg1)}
                                (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:message_thread_by_id
	          {:id (:thread_id msg1)
	           :sub {:name sub-name}
	           :subject "Random complaint"
	           :first_message {:content "Do better!"}}}
                 data))))
      (testing "fails for user other than sender"
        (let [rsp (send-request (-> graphql :query :modmail-thread-by-id)
                                {:thread_id (:thread_id msg1)}
                                (utils/request-cookies user2-uid))
              {:keys [data]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= {:message_thread_by_id nil} data))))
      (testing "fails when unauthenticated"
        (let [rsp (send-request (-> graphql :query :modmail-thread-by-id)
                                {:thread_id (:thread_id msg1)})
              {:keys [data]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= data {:message_thread_by_id nil})))))))

(deftest test-modmail-mark-thread-unread
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)]
    (utils/with-websocket id (utils/open-websocket
                              (utils/ws-headers mod-session))
      (utils/send-init {:token (utils/sign-csrf-token mod-session)})
      (utils/expect-message {:type "connection_ack"})
      (utils/send-data {:id id
                        :type :start
                        :payload {:query (-> graphql
                                             :subscription
                                             :notification-counts
                                             utils/hash-query)}})
      (testing "update_thread_unread mutation"
        (let [rsp1 (send-request (-> graphql :mutation :contact-mods-mid-thread)
                                 {:subject "Random complaint"
                                  :content "Do better!"
                                  :sid sid}
                                 (utils/request-cookies user-uid))
              msg1 (get-in rsp1 [:body :data :create_message_to_mods])
              mtid (:thread_id msg1)
              rsp2 (send-request (-> graphql :mutation :create-modmail-reply)
                                 {:thread_id mtid
                                  :content "Here's your answer!"
                                  :send_to_user true
                                  :show_mod_username false}
                                 (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp1)))
          (is (nil? (get-in rsp1 [:body :errors])))
          (is (= 200 (:status rsp2)))
          (is (nil? (get-in rsp2 [:body :errors])))
          (utils/expect-message
           {:type "data"
	    :id id
	    :payload {:data
	              {:notification_counts
	               {:unread_message_count 0
	                :unread_notification_count 0
	                :sub_notifications
                        [{:sid sid
                          :open_report_count 0
                          :unread_modmail_count 1
                          :new_unread_modmail_count 1
	                  :in_progress_unread_modmail_count 0
	                  :all_unread_modmail_count 1
	                  :discussion_unread_modmail_count 0
	                  :notification_unread_modmail_count 0}]}}}})
          ;; unread_modmail_count goes to zero on the second message because
          ;; it's really counting threads where the newest message is unread,
          ;; and since the mod sent the newest message, it's not unread.
          (utils/expect-message
           {:type "data"
	    :id id
	    :payload {:data
	              {:notification_counts
	               {:unread_message_count 0
	                :unread_notification_count 0
	                :sub_notifications
                        [{:sid sid
                          :open_report_count 0
                          :unread_modmail_count 0
                          :new_unread_modmail_count 0
	                  :in_progress_unread_modmail_count 0
	                  :all_unread_modmail_count 0
	                  :discussion_unread_modmail_count 0
	                  :notification_unread_modmail_count 0}]}}}})
          (testing "marks a thread unread"
            (let [rsp1 (send-request (-> graphql :mutation :update-thread-unread)
                                     {:thread_id mtid
                                      :unread true}
                                     (utils/request-cookies mod-uid))
                  errors (get-in rsp1 [:body :errors])]
              (is (= 200 (:status rsp1)))
              (is (= mtid (get-in rsp1 [:body :data :update_thread_unread])))
              (is (nil? errors))
              (utils/expect-message
               {:type "data"
                :id id
                :payload {:data
	                  {:notification_counts
	                   {:unread_message_count 0
	                    :unread_notification_count 0
	                    :sub_notifications
                            [{:sid sid
                              :open_report_count 0
                              :unread_modmail_count 1
                              :new_unread_modmail_count 0
	                      :in_progress_unread_modmail_count 1
	                      :all_unread_modmail_count 1
	                      :discussion_unread_modmail_count 0
	                      :notification_unread_modmail_count 0}]}}}})
              (let [rsp2 (send-request (-> graphql :query :modmail-thread)
                                       {:first 10
                                        :thread_id (:thread_id msg1)}
                                       (utils/request-cookies mod-uid))
                    {:keys [errors data]} (:body rsp2)
                    edges (get-in data [:modmail_thread :edges])]
                (is (= 200 (:status rsp2)))
                (is (nil? errors))
                (is (= 2 (count edges)))
                (is (= [true true] (map #(get-in % [:node :unread]) edges))))))
          (testing "marks a thread read"
            (let [rsp1 (send-request (-> graphql :mutation :update-thread-unread)
                                     {:thread_id mtid
                                      :unread false}
                                     (utils/request-cookies mod-uid))
                  errors (get-in rsp1 [:body :errors])]
              (is (= 200 (:status rsp1)))
              (is (nil? errors))
              (let [rsp2 (send-request (-> graphql :query :modmail-thread)
                                       {:first 10
                                        :thread_id mtid}
                                       (utils/request-cookies mod-uid))
                    {:keys [errors data]} (:body rsp2)
                    edges (get-in data [:modmail_thread :edges])]
                (is (= 200 (:status rsp2)))
                (is (nil? errors))
                (is (= 2 (count edges)))
                (is (= [false false] (map #(get-in % [:node :unread]) edges)))
                (utils/expect-message
                 {:type "data"
                  :id id
                  :payload
                  {:data
	           {:notification_counts
	            {:unread_message_count 0
	             :unread_notification_count 0
	             :sub_notifications
                     [{:sid sid
                       :open_report_count 0
                       :unread_modmail_count 0
                       :new_unread_modmail_count 0
	               :in_progress_unread_modmail_count 0
	               :all_unread_modmail_count 0
	               :discussion_unread_modmail_count 0
	               :notification_unread_modmail_count 0}]}}}})))))))))

(deftest test-create-private-message
  (testing "send new private message"
    (let [{:keys [mod-uid mod-name
                  user-uid user-name]} (utils/mod-user-and-sub *system*)
          response (send-request (-> graphql :mutation :create-new-message)
                                 {:subject "Hello"
                                  :content "Just between us!"
                                  :username user-name}
                                 (utils/request-cookies mod-uid))
          data (get-in response [:body :data])
          errors (get-in response [:body :errors])
          mid (get-in data [:create_private_message :mid])]
      (is (= 200 (:status response)))
      (is (nil? errors))
      (is (= {:create_private_message
	      {:content "Just between us!"
               :mtype "USER_TO_USER"
               :sender {:name mod-name}
               :receiver {:name user-name}}}
             (update data :create_private_message dissoc :mid)))
      (testing "recipient can read message"
        (let [response (send-request (-> graphql :query :get-message)
                                     {:mid mid}
                                     (utils/request-cookies user-uid))
              data (get-in response [:body :data])
              errors (get-in response [:body :errors])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= {:message_by_mid
	          {:content "Just between us!"
                   :mtype "USER_TO_USER"
                   :sender {:uid mod-uid}
                   :receiver {:uid user-uid}}}
                 data)))))))

(deftest test-block-direct-messages-abuse-notification
  (testing "when a user blocks all direct messages"
    (let [{:keys [user-name user-uid mod-uid mod2-name sid
                  sub-name]} (utils/mod-user-and-sub *system*)
          mod-session (utils/make-session mod-uid)]
      (jdbc/insert! (-> *system* :db :ds) "site_metadata" {:key "site.admin_sub"
                                                           :value sub-name})
      (let [rsp (send-request (-> graphql :mutation :update-user-preferences)
                              {:preferences {:nsfw true
                                             :nsfw_blur true
                                             :lab_rat true
                                             :enable_collapse_bar true
                                             :block_dms true
                                             :language "en"}}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (jt/advance-clock! clock (jt/plus (jt/days 1))))
      (testing "then unblocks direct messages and sends a message"
        (let [rsp (send-request (-> graphql :mutation :update-user-preferences)
                                {:preferences {:nsfw true
                                               :nsfw_blur true
                                               :lab_rat true
                                               :enable_collapse_bar true
                                               :block_dms false
                                               :language "en"}}
                                (utils/request-cookies user-uid))
              _ (is (= 200 (:status rsp)))
              blocking-disabled (jt/with-clock clock
                                  (jt/format "yyyy-MM-dd HH:mm:ss"
                                             (jt/zoned-date-time)))
              _ (jt/advance-clock! clock (jt/plus (jt/days 1)))
              rsp2 (send-request (-> graphql :mutation :create-new-message)
                                 {:subject "Hello"
                                  :content "Just between us!"
                                  :username mod2-name}
                                 (utils/request-cookies user-uid))
              _ (is (= 200 (:status rsp2)))]
          (testing "then reblocks direct messages, admins are notified"
            (utils/with-websocket id (utils/open-websocket
                                      (utils/ws-headers mod-session))
              (utils/send-init {:token (utils/sign-csrf-token mod-session)})
              (utils/expect-message {:type "connection_ack"})
              (utils/send-data {:id id
                                :type :start
                                :payload {:query (-> graphql
                                                     :subscription
                                                     :notification-counts
                                                     utils/hash-query)}})
              (let [_ (jt/advance-clock! clock (jt/plus (jt/days 1)))
                    rsp (send-request (-> graphql :mutation
                                          :update-user-preferences)
                                      {:preferences {:nsfw true
                                                     :nsfw_blur true
                                                     :lab_rat true
                                                     :enable_collapse_bar true
                                                     :block_dms true
                                                     :language "en"}}
                                      (utils/request-cookies user-uid))]
                (is (= 200 (:status rsp)))
                (is (= {:notification_counts
	                {:unread_message_count 0
	                 :unread_notification_count 0
	                 :sub_notifications
	                 [{:sid sid
	                   :open_report_count 0
	                   :unread_modmail_count 1
	                   :new_unread_modmail_count 0
	                   :in_progress_unread_modmail_count 0
	                   :all_unread_modmail_count 0
	                   :discussion_unread_modmail_count 0
	                   :notification_unread_modmail_count 1}]}}
                       (-> (utils/<message!! 200) :payload :data)))
                (let [rsp (send-request (-> graphql :query
                                            :modmail-notifications)
                                        {:first 10
                                         :sids [sid]}
                                        (utils/request-cookies mod-uid))
                      {:keys [body status]} rsp
                      {:keys [data errors]} body
                      _ (is (= 200 status))
                      _ (is (nil? errors))
                      mtid (get-in data [:modmail_threads :edges 0 :node :id])]
                  (is (= data
                         {:modmail_threads
	                  {:edges
	                   [{:node
	                     {:id mtid
                              :subject "Possible direct message blocking abuse"
	                      :sub {:sid sid}
	                      :reply_count 0
	                      :first_message
	                      {:content
                               (-> (format
                                    "[%s](/u/%s) previously had all direct
                                     messages blocked, disabled blocking at
                                     %s (GMT), sent 1 messages in the
                                     interim, and just re-enabled blocking."
                                    user-name user-name blocking-disabled)
                                   util/condense-whitespace)
                               :mtype "MOD_NOTIFICATION"
	                       :unread true}
	                      :mailbox "INBOX"}}]
	                   :pageInfo {:hasNextPage false}}}))))))))
      (jdbc/delete! (:ds (:db *system*)) "site_metadata"
                    ["key = ?" "site.admin_sub"]))))
