;; gql/elements/user/resolve_tests.clj -- Testing user functionality for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [java-time.api :as jt]
   [ovarit.app.gql.test-utils :refer [defgraphql
                                      send-request
                                      test-system
                                      test-system-config-map
                                      test-system-map
                                      add-queries
                                      ban-user! delete-user!
                                      request-cookies] :as utils]
   [ovarit.app.gql.util :as util]))

(def ^:dynamic ^:private *system* nil)
(def ^:dynamic ^:private *fixtures* nil)
(def ^:private clock (doto (jt/mock-clock 1000 "UTC")))
(defgraphql graphql
  "test-graphql/user.graphql"
  "test-graphql/shared.graphql")

(defn system-map []
  (-> (test-system-config-map)
      (assoc-in [:server :clock] clock)
      (add-queries graphql)
      test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (component/start-system (test-system (system-map)))]
      (try
        (binding [*fixtures* (utils/mod-user-and-sub *system*)]
          (test-fn))
        (finally
          (component/stop-system *system*))))))

(deftest test-create-user
  (testing "Create user returns a uid"
    (is (= 36 (count (:user-uid *fixtures*))))))

(deftest test-query-get-subscriptions
  (testing "the user can query their subscribed subs"
    (let [{:keys [mod-uid mod-name sub-name sid]} *fixtures*
          response (send-request (-> graphql :query :get-subscriptions)
                                 {:name mod-name}
                                 (request-cookies mod-uid))]
      (is (= 200 (:status response)))
      (is (= {:data
              {:user_by_name
               {:subscriptions [{:sid sid
                                 :name sub-name
                                 :status "SUBSCRIBED"}]}}}
             (:body response)))))
  (testing "anons can't query a user's subs"
    (let [{:keys [mod-name]} *fixtures*
          response (send-request (-> graphql :query :get-subscriptions)
                                 {:name mod-name})]
      (is (= 403 (:status response)))
      (is (= "Not authorized" (get-in response [:body :errors 0 :message])))
      (is (=  {:user_by_name {:subscriptions []}}
              (get-in response [:body :data])))))
  (testing "users can't query another user's subs"
    (let [{:keys [user-uid mod-name]} *fixtures*
          response (send-request (-> graphql :query :get-subscriptions)
                                 {:name mod-name}
                                 (request-cookies user-uid))]
      (is (= 403 (:status response)))
      (is (= "Not authorized" (get-in response [:body :errors 0 :message])))
      (is (=  {:user_by_name {:subscriptions []}}
              (get-in response [:body :data]))))))

(deftest test-query-user-by-name-with-metadata
  (let [{:keys [user-uid user-name]} *fixtures*
        response (send-request (-> graphql :query :user-metadata)
                               {:name user-name}
                               (request-cookies user-uid))]
    (is (= 200 (:status response)))
    (is (= {:data
            {:user_by_name {:uid user-uid
                            :name user-name
                            :language nil
                            :score 0
                            :given 0
                            :status "ACTIVE"
                            :attributes {:can_admin false
                                         :lab_rat false
                                         :nsfw false
                                         :nsfw_blur false
                                         :no_chat false
                                         :enable_collapse_bar true
                                         :block_dms false}}}}
           (:body response)))))

(deftest test-query-metadata-and-subscriptions-refused
  (testing "Anonymous and other users can't get metadata and subscriptions"
    (let [uid (:user-uid *fixtures*)
          user2-uid (-> (-> graphql :mutation :create-user)
                        (send-request {:name "user2"})
                        (get-in [:body :data :register_user :uid]))
          expected-errors {"subscriptions" "Not authorized"
	                   "lab_rat" "Not authorized"
	                   "nsfw" "Not authorized"
	                   "no_chat" "Not authorized"
	                   "enable_collapse_bar" "Not authorized"}]
      (testing "- anonymous users"
        (let [response (send-request (-> graphql :query
                                         :get-meta-and-subscriptions)
                                     {:uid uid})
              result (-> (:body response)
                         (update-in [:errors 0] dissoc :locations))
              {:keys [data errors]} result
              errors-by-path (zipmap (map #(last (:path %)) errors)
                                     (map :message errors))]
          (is (= 403 (:status response)))
          (is (= {:user_by_uid {:attributes nil
                                :subscriptions []}}
                 data))
          (is (= expected-errors errors-by-path))))
      (testing "- different user"
        (let [response (send-request (-> graphql :query
                                         :get-meta-and-subscriptions)
                                     {:uid uid}
                                     (request-cookies user2-uid))
              result (-> (:body response)
                         (update-in [:errors 0] dissoc :locations))
              {:keys [data errors]} result
              errors-by-path (zipmap (map #(last (:path %)) errors)
                                     (map :message errors))]
          (is (= 403 (:status response)))
          (is (= {:user_by_uid {:attributes nil
                                :subscriptions []}}
                 data))
          (is (= expected-errors errors-by-path))))
      (ban-user! *system* user2-uid)
      (testing "- banned user"
        (let [response (send-request (-> graphql :query
                                         :get-meta-and-subscriptions)
                                     {:uid uid}
                                     (request-cookies user2-uid))
              result (-> (:body response)
                         (update-in [:errors 0] dissoc :locations))
              {:keys [data errors]} result
              errors-by-path (zipmap (map #(last (:path %)) errors)
                                     (map :message errors))]
          (is (= 403 (:status response)))
          (is (= {:user_by_uid {:attributes nil
                                :subscriptions []}}
                 data))
          (is (= expected-errors errors-by-path))))
      (delete-user! *system* user2-uid)
      (testing "- deleted user"
        (let [response (send-request (-> graphql :query
                                         :get-meta-and-subscriptions)
                                     {:uid uid}
                                     (request-cookies user2-uid))
              result (-> (:body response)
                         (update-in [:errors 0] dissoc :locations))
              {:keys [data errors]} result
              errors-by-path (zipmap (map #(last (:path %)) errors)
                                     (map :message errors))]
          (is (= 403 (:status response)))
          (is (= {:user_by_uid {:attributes nil
                                :subscriptions []}}
                 data))
          (is (= expected-errors errors-by-path)))))))

(deftest test-query-current-user
  (testing "Can get current user with a cookie"
    (let [{:keys [mod-uid mod-name sub-name]} *fixtures*
          response (send-request (-> graphql :query :current-user) nil
                                 (request-cookies mod-uid))]
      (is (= 200 (:status response)))
      (is (= {:data {:current_user {:name mod-name
                                    :uid mod-uid
                                    :language nil
                                    :attributes {:can_admin false}
                                    :subscriptions [{:name sub-name}]
                                    :unread_message_count 0
                                    :unread_notification_count 0}}}
             (:body response))))))

(deftest test-query-moderated-subs
  (testing "A query can retrieve subs owned by a user"
    (let [{:keys [mod-uid mod-name sub-name sid]} *fixtures*
          response (send-request (-> graphql :query :moderated-subs)
                                 {:name mod-name})]
      (is (= 200 (:status response)))
      (is (= {:data
	      {:user_by_name
	       {:uid mod-uid
	        :subs_moderated [{:sub {:name sub-name
                                        :sid sid}
                                  :moderation_level "OWNER"}]}}}
             (:body response))))))

(deftest test-subscribe-and-unsubscribe
  (let [{:keys [user-uid mod-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)
        send-sub-query (fn [uid]
                         (let [rsp (-> (-> graphql :query :subscriptions)
                                       (send-request {:sub sub-name
                                                      :uid uid}
                                                     (request-cookies uid)))]
                           (is (= 200 (:status rsp)))
                           (is (nil? (get-in rsp [:body :errors])))
                           (get-in rsp [:body :data])))
        user-session (utils/make-session user-uid)]
    (utils/with-websocket id (utils/open-websocket
                              (utils/ws-headers user-session))
      (utils/send-init {:token (utils/sign-csrf-token user-session)})
      (utils/expect-message {:type "connection_ack"})
      (utils/send-data {:id id
                        :type :start
                        :payload {:query (-> graphql :subscription
                                             :subscription-update
                                             utils/hash-query)}})
      (testing "the creator of a sub starts out subscribed"
        (is (= {:user_by_uid {:subscriptions [{:sid sid
                                               :name sub-name
                                               :status "SUBSCRIBED"
                                               :order nil}]}
                :sub_by_name {:subscriber_count 1}}
               (send-sub-query mod-uid))))
      (testing "a new user starts out unsubscribed to anything"
        (is (= {:user_by_uid {:subscriptions []}
                ;; The mod who created the sub is the first subscriber.
                :sub_by_name {:subscriber_count 1}}
               (send-sub-query user-uid))))
      (testing "a user can subscribe to a sub"
        (let [rsp (-> (-> graphql :mutation :change-subscription)
                      (send-request {:sid sid
                                     :change :SUBSCRIBE}
                                    (request-cookies user-uid)))
              {:keys [data errors]} (:body rsp)
              subscribed-result {:sid sid
                                 :name sub-name
                                 :status "SUBSCRIBED"
                                 :order nil}
              unsubscribed-result {:sid sid
                                   :name sub-name
                                   :status nil
                                   :order nil}
              blocked-result {:sid sid
                              :name sub-name
                              :status "BLOCKED"
                              :order nil}
              ws-result (fn [res] {:type "data" :id id
                                   :payload {:data
                                             {:subscription_update res}}})]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:change_subscription_status subscribed-result} data))
          (utils/expect-message (ws-result subscribed-result))
          (testing "and be included in the sub's subscriber count"
            (is (= {:user_by_uid {:subscriptions [subscribed-result]}
                    :sub_by_name {:subscriber_count 2}}
                   (send-sub-query user-uid))))
          (testing "and subscribing again does nothing"
            (let [rsp (-> (-> graphql :mutation :change-subscription)
                          (send-request {:sid sid
                                         :change "SUBSCRIBE"}
                                        (request-cookies user-uid)))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (= {:change_subscription_status subscribed-result} data))
              (utils/expect-message (ws-result subscribed-result)))
            (is (= {:user_by_uid {:subscriptions [subscribed-result]}
                    :sub_by_name {:subscriber_count 2}}
                   (send-sub-query user-uid)))
            (testing "and unblocking does nothing"
              (let [rsp (-> (-> graphql :mutation :change-subscription)
                            (send-request {:sid sid
                                           :change "SUBSCRIBE"}
                                          (request-cookies user-uid)))
                    {:keys [data errors]} (:body rsp)]
                (is (= 200 (:status rsp)))
                (is (nil? errors))
                (is (= {:change_subscription_status subscribed-result} data))
                (utils/expect-message (ws-result subscribed-result)))
              (is (= {:user_by_uid {:subscriptions [subscribed-result]}
                      :sub_by_name {:subscriber_count 2}}
                     (send-sub-query user-uid)))))
          (testing "and unsubscribe from it"
            (let [rsp (-> (-> graphql :mutation :change-subscription)
                          (send-request {:sid sid
                                         :change "UNSUBSCRIBE"}
                                        (request-cookies user-uid)))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (= {:change_subscription_status unsubscribed-result} data))
              (utils/expect-message (ws-result unsubscribed-result)))
            (testing "which removes them from the sub's subscriber count"
              (is (= {:user_by_uid {:subscriptions []}
                      :sub_by_name {:subscriber_count 1}}
                     (send-sub-query user-uid))))
            (testing "subscribe then block"
              (let [rsp (-> (-> graphql :mutation :change-subscription)
                            (send-request {:sid sid
                                           :change :SUBSCRIBE}
                                          (request-cookies user-uid)))
                    {:keys [data errors]} (:body rsp)]
                (is (= 200 (:status rsp)))
                (is (nil? errors))
                (is (= {:change_subscription_status subscribed-result} data))
                (utils/expect-message (ws-result subscribed-result)))
              (let [rsp (-> (-> graphql :mutation :change-subscription)
                            (send-request {:sid sid
                                           :change "BLOCK"}
                                          (request-cookies user-uid)))
                    {:keys [data errors]} (:body rsp)]
                (is (= 200 (:status rsp)))
                (is (nil? errors))
                (is (= {:change_subscription_status blocked-result} data))
                (utils/expect-message (ws-result blocked-result)))
              (testing "results in a blocked subscription"
                (is (= {:user_by_uid {:subscriptions [blocked-result]}
                        :sub_by_name {:subscriber_count 1}}
                       (send-sub-query user-uid)))))))))))

(deftest test-anons-cant-subscribe
  (let [{:keys [sid]} *fixtures*
        rsp1 (-> (-> graphql :mutation :change-subscription)
                 (send-request {:sid sid
                                :change :SUBSCRIBE}))]
    (is (= 403 (:status rsp1)))
    (is (= "Not authorized" (get-in rsp1 [:body :errors 0 :message])))))

(deftest test-username-history-visibility
  (let [ds (get-in *system* [:db-admin :ds])
        {:keys [user-name user-uid mod-uid
                admin-uid]} (utils/mod-user-and-sub *system*)
        name1 (str user-name "-1")
        name2 (str user-name "-2")
        two-days-ago (-> (jt/minus (jt/zoned-date-time)
                                   (jt/days 2))
                         jt/sql-timestamp)]
    (jdbc/update! ds "site_metadata" {:value 1}
                  ["key = ?" "site.username_change.display_days"])
    (jdbc/insert! ds "username_history" {:name user-name
                                         :uid user-uid
                                         :required_id nil
                                         :changed two-days-ago})
    (jdbc/insert! ds "username_history" {:name name1
                                         :uid user-uid
                                         :required_id nil
                                         :changed (util/sql-timestamp)})
    (jdbc/update! ds "public.user" {:name name2} ["uid = ?" user-uid])
    (testing "admins, mods and self can see username history"
      (doseq [uid [admin-uid mod-uid user-uid]]
        (let [is-admin? (= admin-uid uid)
              rsp (send-request (-> graphql :query :get-username-history)
                                {:uid user-uid
                                 :is_admin is-admin?}
                                (request-cookies uid))
              {:keys [data errors]} (:body rsp)
              names-and-required [{:name name1 :required_by_admin false}
                                  {:name user-name :required_by_admin false}]
              names [{:name name1} {:name user-name}]]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:user_by_uid {:name name2
                                :username_history (if is-admin?
                                                    names-and-required
                                                    names)}}
                 (update-in data [:user_by_uid :username_history]
                            (fn [h] (map #(dissoc % :changed) h)))))
          (when is-admin?
            (let [hist (get-in data [:user_by_uid :username_history])]
              (is (> (parse-long (:changed (nth hist 0)))
                     (parse-long (:changed (nth hist 1)))))))
          (is (nil? errors)))))
    (testing "users can see other user's username history for configured time"
      (let [user2-uid (-> (-> graphql :mutation :create-user)
                          (send-request {:name (str user-name "-alt")})
                          (get-in [:body :data :register_user :uid]))
            rsp (send-request (-> graphql :query :get-username-history)
                              {:uid user-uid
                               :is_admin false}
                              (request-cookies user2-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:user_by_uid {:name name2
                              :username_history [{:name name1}]}}
               (update-in data [:user_by_uid :username_history]
                          (fn [h] (map #(dissoc % :changed) h)))))
        (is (nil? errors))))
    (testing "anons cannot see username history"
      (let [rsp (send-request (-> graphql :query :get-username-history)
                              {:uid user-uid
                               :is_admin false})
            {:keys [data errors]} (:body rsp)]
        (is (= 403 (:status rsp)))
        (is (empty? (get-in data [:user_by_uid :username_history])))
        (is (= "Not authorized" (get-in errors [0 :message])))))))

(deftest test-update-preferences-anons
  (testing "anons can't update preferences"
    (let [rsp (send-request (-> graphql :mutation :update-user-preferences)
                            {:preferences {:nsfw false
                                           :nsfw_blur false
                                           :lab_rat false
                                           :enable_collapse_bar false
                                           :block_dms false
                                           :language nil}})]
      (is (= 403 (:status rsp)))
      (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))

(deftest test-update-preferences-language
  (testing "language value must be valid"
    (let [{:keys [user-uid]} (utils/mod-user-and-sub *system*)
          rsp (send-request (-> graphql :mutation :update-user-preferences)
                            {:preferences {:nsfw false
                                           :nsfw_blur false
                                           :lab_rat false
                                           :enable_collapse_bar false
                                           :block_dms false
                                           :language "bogus"}}
                            (request-cookies user-uid))]
      (is (= 400 (:status rsp)))
      (is (= "Language not supported"
             (get-in rsp [:body :errors 0 :message]))))))

(deftest test-update-preferences
  (let [{:keys [user-name user-uid]} (utils/mod-user-and-sub *system*)]
    (testing "users can update their preferences"
      (let [rsp (send-request (-> graphql :mutation :update-user-preferences)
                              {:preferences {:nsfw true
                                             :nsfw_blur true
                                             :lab_rat true
                                             :enable_collapse_bar true
                                             :block_dms true
                                             :language "en"}}
                              (request-cookies user-uid))
            _ (is (= 200 (:status rsp)))
            rsp2 (send-request (-> graphql :query :user-metadata)
                               {:name user-name}
                               (request-cookies user-uid))
            {:keys [data errors]} (:body rsp2)]
        (is (= 200 (:status rsp2)))
        (is (nil? errors))
        (testing "and verify that they are changed"
          (is (= {:user_by_name {:uid user-uid
	                         :name user-name
	                         :language "en"
	                         :score 0
	                         :given 0
	                         :status "ACTIVE"
	                         :attributes
	                         {:can_admin false
	                          :lab_rat true
	                          :nsfw true
	                          :nsfw_blur true
	                          :no_chat false
	                          :enable_collapse_bar true
	                          :block_dms true}}}
                 data))))
      (testing "more than once"
        (let [rsp (send-request (-> graphql :mutation :update-user-preferences)
                                {:preferences {:nsfw false
                                               :nsfw_blur false
                                               :lab_rat false
                                               :enable_collapse_bar false
                                               :block_dms false
                                               :language nil}}
                                (request-cookies user-uid))
              _ (is (= 200 (:status rsp)))
              rsp2 (send-request (-> graphql :query :user-metadata)
                                 {:name user-name}
                                 (request-cookies user-uid))
              {:keys [data errors]} (:body rsp2)]
          (is (= 200 (:status rsp2)))
          (is (nil? errors))
          (testing "and verify that they are changed"
            (is (= {:user_by_name {:uid user-uid
	                           :name user-name
	                           :language nil
	                           :score 0
	                           :given 0
	                           :status "ACTIVE"
	                           :attributes
	                           {:can_admin false
	                            :lab_rat false
	                            :nsfw false
	                            :nsfw_blur false
	                            :no_chat false
	                            :enable_collapse_bar false
	                            :block_dms false}}}
                   data))))))))

(deftest test-update-preferences-admins
  (testing "admins can't block direct messages"
    (let [{:keys [admin-uid]} (utils/mod-user-and-sub *system*)
          rsp (send-request (-> graphql :mutation :update-user-preferences)
                            {:preferences {:nsfw false
                                           :nsfw_blur false
                                           :lab_rat false
                                           :enable_collapse_bar false
                                           :block_dms true
                                           :language nil}}
                            (request-cookies admin-uid))]
      (is (= 400 (:status rsp)))
      (is (= "Bad Request" (get-in rsp [:body :errors 0 :message]))))))

(deftest test-require-name-change-auth
  (let [{:keys [admin-name admin-uid mod-uid mod2-name mod2-uid
                user-uid]} (utils/mod-user-and-sub *system*)
        rsp (send-request (-> graphql :mutation :require-name-change)
                          {:uid mod2-uid
                           :message "name change"}
                          (request-cookies admin-uid))]
    (is (= 200 (:status rsp)))
    (testing "mods, users, self and anons can't query required name change"
      (doseq [uid [mod-uid mod2-uid user-uid nil]]
        (let [rsp (send-request (-> graphql :query :required-name-change)
                                {:name mod2-name}
                                (request-cookies uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= {:required_name_change nil} data))
          (is (= "Not authorized" (get-in errors [0 :message]))))))
    (testing "mods, users, self and anons can't require a name change"
      (doseq [uid [mod-uid user-uid nil]]
        (let [rsp (send-request (-> graphql :mutation :require-name-change)
                                {:uid mod2-uid
                                 :message "name change"}
                                (request-cookies uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= {:require_name_change nil} data))
          (is (= "Not authorized" (get-in errors [0 :message]))))))
    (testing "mods, users, self and anons can't remove a required name change"
      (doseq [uid [mod-uid user-uid nil]]
        (let [rsp (send-request (-> graphql :mutation :cancel-name-change)
                                {:uid mod2-uid}
                                (request-cookies uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= {:cancel_required_name_change nil} data))
          (is (= "Not authorized" (get-in errors [0 :message]))))))
    (testing "required name change unchanged by unauthorized attempts"
      (let [rsp (send-request (-> graphql :query :required-name-change)
                              {:name mod2-name}
                              (request-cookies admin-uid))]
        (is (= 200 (:status rsp)))
        (is (nil? (-> rsp :body :errors)))
        (is (=  {:message "name change"
                 :admin {:name admin-name}}
                (-> rsp :body :data :required_name_change
                    (dissoc :required_at))))))))

(deftest test-require-change-username
  (let [{:keys [admin-name admin-uid user-name
                user-uid]} (utils/mod-user-and-sub *system*)
        now (jt/with-clock clock
              (.getTime (jt/sql-timestamp)))]
    (testing "initially username change is not required")
    (let [rsp (send-request (-> graphql :query :required-name-change)
                            {:name user-name}
                            (request-cookies admin-uid))]
      (is (= 200 (:status rsp)))
      (is (nil? (-> rsp :body :errors)))
      (is (= {:required_name_change nil} (-> rsp :body :data))))
    (testing "admins can set change username"
      (jt/advance-clock! clock (jt/seconds 1))
      (let [rsp (send-request (-> graphql :mutation :require-name-change)
                              {:uid user-uid
                               :message "asking nicely"}
                              (request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)
            datetime (-> data :require_name_change :required_at)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:require_name_change {:message "asking nicely"
                                      :admin {:name admin-name}}}
               (update data :require_name_change dissoc :required_at)))
        (is (> (parse-long datetime) now))
        (testing "and query it"
          (let [rsp (send-request (-> graphql :query :required-name-change)
                                  {:name user-name}
                                  (request-cookies admin-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (is (= {:required_name_change {:message "asking nicely"
                                           :admin {:name admin-name}
                                           :required_at datetime}}
                   (-> rsp :body :data)))))
        (testing "creating a sitelog entry"
          (let [rsp (send-request (-> graphql :query :sitelog-entries)
                                  {}
                                  (request-cookies admin-uid))
                latest-entry (->> rsp :body :data :sitelog_entries
                                  :edges
                                  (map :node)
                                  first)]
            (is (= {:action "REQUIRE_NAME_CHANGE"
	            :user {:uid admin-uid}
	            :desc user-name
	            :link ""
	            :target_user {:uid user-uid}}
                   (dissoc latest-entry :time)))))
        (testing "and a second set returns the original"
          (let [rsp (send-request (-> graphql :mutation :require-name-change)
                                  {:uid user-uid
                                   :message "not so nicely"}
                                  (request-cookies admin-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:require_name_change {:message "asking nicely"
                                          :admin {:name admin-name}
                                          :required_at datetime}}
                   data)))))
      (testing "and cancel it"
        (jt/advance-clock! clock (jt/seconds 1))
        (let [rsp (send-request (-> graphql :mutation :cancel-name-change)
                                {:uid user-uid}
                                (request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:cancel_required_name_change "void"} data)))
        (testing "creating a sitelog entry"
          (let [rsp (send-request (-> graphql :query :sitelog-entries)
                                  {}
                                  (request-cookies admin-uid))
                latest-entry (->> rsp :body :data :sitelog_entries
                                  :edges
                                  (map :node)
                                  first)]
            (is (= {:action "CANCEL_REQUIRE_NAME_CHANGE"
	            :user {:uid admin-uid}
	            :desc ""
	            :link ""
	            :target_user {:uid user-uid}}
                   (dissoc latest-entry :time)))))
        (testing "and verify it was cancelled"
          (let [rsp (send-request (-> graphql :query :required-name-change)
                                  {:name user-name}
                                  (request-cookies admin-uid))]
            (is (= 200 (:status rsp)))
            (is (nil? (-> rsp :body :errors)))
            (is (= {:required_name_change nil}
                   (-> rsp :body :data)))))))))
