;; gql/e2e/donate_test.cljs -- Donation integration testing for throat
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.e2e.donate-test
  (:require
   [clojure.test :refer [use-fixtures deftest is testing]]
   [dotenv]
   [etaoin.api :refer
    [chrome click exists? fill fill-active has-text?
     get-element-text-el get-url go query-all screenshot wait-exists
     wait-visible with-chrome with-chrome-headless with-postmortem
     with-wait-timeout] :as api]
   [etaoin.keys :as keys]))

(def ^:dynamic *driver*)

(def test-url (or (dotenv/env "TEST_URL") "http://localhost:5000"))
(defn url
  [path]
  (str test-url path))

(def postmortem-dir (or (dotenv/env "POSTMORTEM") "postmortem"))
(def pm-opt {:dir postmortem-dir
             :date-format "yyyyMMdd-HHmmss"})

(def test-user (or (dotenv/env "TEST_USER") "e2etest"))
(def test-password (or (dotenv/env "TEST_PASSWORD") "password"))

(defn envvar-true?
  [envvar]
  (->
   (dotenv/env envvar)
   #{"true" "True" "1"}
   some?))

(def test-in-docker? (envvar-true? "TEST_IN_DOCKER"))
(def test-headless? (envvar-true? "TEST_HEADLESS"))

(def user-agent-string
  "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36")

(defn fixture-driver
  "Execute a test with a chrome driver bound to `*driver*`."
  [f]
  (if (or test-in-docker? test-headless?)
    (let [base-args ["--user-agent" user-agent-string
                     "--window-size=1920,1080"
                     "--start-maximized"]
          args (if test-in-docker?
                 (conj base-args "--no-sandbox")
                 base-args)]
      (with-chrome-headless {:args args} driver
        (binding [*driver* driver]
          (f))))
    (with-chrome {} driver
      (binding [*driver* driver]
        (f)))))

;; Start and stop driver for each test.
(use-fixtures
  :each
  fixture-driver)

(defn login
  "Starting from the logged-out state, log in a user."
  [driver username password]
  (go driver (url "/"))
  (wait-exists driver {:id "th-uinfo"})
  (when (has-text? driver "Log out")
    (click driver {:tag :button :fn/text "Log out"}))
  (go driver (url "/"))
  (wait-exists driver {:fn/text "Log in"})
  (doto driver
    (go (url "/login"))
    (wait-visible {:id "username"})
    (fill {:id "username"} username)
    (fill {:id "password"} password)
    (click {:type "submit"})))

(defn login-via-donate
  "Starting from the logged-out state, log in a user via the donate page."
  [driver username password]
  (go driver (url "/donate"))
  (wait-exists driver {:id "th-uinfo"})
  (when (has-text? driver "Log out")
    (click driver {:tag :button :fn/text "Log out"}))
  (go driver (url "/donate"))
  (wait-exists driver {:fn/text "Log in"})
  (doto driver
    (fill {:id "username"} username)
    (fill {:id "password"} password)
    (click {:type "submit"})))

(defn cancel-existing-subscription
  "If the test user already has a subscription, perhaps from a
  previous failed test, then cancel it."
  [driver]
  (testing "cancel existing donation subscription"
    (when (exists? driver {:fn/text "Your donation subscription"})
      (doto driver
        (click {:fn/text "Cancel Donation Subscription"})
        (wait-visible {:fn/text "Are you sure?"})
        (click {:fn/has-text "Yes"})
        (wait-visible {:fn/has-text
                       "Your subscription has been cancelled"})
        (go (url "/donate"))
        (wait-visible {:tag :div :fn/text "Custom"})))))

(comment
  (def driver (chrome))
  (def *driver* (chrome))
  (def driver *driver*)
  (login *driver* "user1" "password")
  (go *driver* (url "/donate"))
  (go *driver* "http://localhost:5000/donate")
  (go *driver* "http://localhost:5000/")
  (get-url *driver*)
  (screenshot *driver* "page.png"))

#_(deftest ^:e2e test-one-time-donation
    (with-postmortem *driver* pm-opt
      (with-wait-timeout 3
        (testing "make a one-time donation"
          (doto *driver*
            (login test-user test-password)
            (go (url "/donate"))
            (wait-visible {:tag :div :fn/text "Custom"})
            (cancel-existing-subscription))
          (is (has-text? *driver* {:tag :h1} "Donate"))
          (doto *driver*
            (click {:tag :div :fn/text "Custom"})
            (wait-exists {:id "donate-custom-amount"})
            (fill {:id "donate-custom-amount"} "10")
            (click {:id "donate-type-one-time"})
            (click {:id "continue"})
            (wait-exists {:tag :span :fn/has-text "Pay"} {:timeout 10}))
          (is (exists? *driver* {:tag :div
                                 :fn/has-text "One Time Payment to Ovarit"}))
          (is (exists? *driver* {:fn/has-text (str test-user "@example.com")}))
          ;; If Stripe has remembered card information it will show a collapsed
          ;; form.  Expand it.
          (when (exists? *driver* {:fn/text "Change"})
            (click *driver* {:fn/text "Change"})
            (wait-visible *driver* {:name "cardNumber"}))
          (doto *driver*
            (fill {:name "cardNumber"} "4242 4242 4242 4242")
            (fill {:name "cardExpiry"} "04/24")
            (fill {:name "cardCvc"} "222")
            ;; Billing name is the only field where fill doesn't replace
            ;; the previous content.
            (click {:name "billingName"})
            (fill-active keys/home (keys/with-shift keys/end) keys/delete)
            (fill {:name "billingName"} "Testy Tester")
            (fill {:name "billingPostalCode"} "42424")
            (click {:tag :button :type "submit"})
            (wait-exists {:fn/text "Donation Complete"} {:timeout 10}))
          (is (exists? *driver* {:tag :p :fn/text "You donated $10 today."}))))))

#_(deftest ^:e2e test-subscription-donation
    (with-postmortem *driver* pm-opt
      (with-wait-timeout 3
        (doto *driver*
          (login-via-donate test-user test-password)
          (go (url "/donate"))
          (wait-visible {:tag :div :fn/text "Custom"})
          (cancel-existing-subscription))
        (testing "create a new donation subscription"
          (let [values (query-all *driver* {:tag :div :fn/has-text "$"})
                sub-amount (get-element-text-el *driver* (last values))
                one-time-amount (get-element-text-el *driver* (first values))]
            (doto *driver*
              (click {:fn/text sub-amount})
              (click {:id "donate-type-recurring"})
              (click {:id "continue"})
              (wait-exists {:tag :span :fn/has-text "Subscribe"} {:timeout 10}))
            (is (exists? *driver* {:fn/has-text "Subscribe to Ovarit Donation"}))
            (is (exists? *driver* {:fn/has-text (str sub-amount ".00")}))
            (is (exists? *driver* {:fn/has-text (str test-user "@example.com")}))
            ;; If Stripe has remembered card information it will show a collapsed
            ;; form.  Expand it.
            (when (exists? *driver* {:fn/text "Change"})
              (click *driver* {:fn/text "Change"})
              (wait-visible *driver* {:name "cardNumber"}))
            (doto *driver*
              (fill {:name "cardNumber"} "4242 4242 4242 4242")
              (fill {:name "cardExpiry"} "04/24")
              (fill {:name "cardCvc"} "222")
              ;; Billing name is the only field where fill doesn't replace
              ;; the previous content.
              (click {:name "billingName"})
              (fill-active keys/home (keys/with-shift keys/end) keys/delete)
              (fill {:name "billingName"} "Testy Tester")
              (fill {:name "billingPostalCode"} "42424")
              (click {:tag :button :type "submit"})
              (wait-exists {:fn/text "Donation Complete"} {:timeout 10}))
            (is (exists? *driver* {:tag :p :fn/has-text
                                   (str "You are subscribed to donate " sub-amount
                                        " each month")}))
            (is (exists? *driver* {:tag :a :fn/link "/donate"}))
            (testing "and verify it shows on the donation page"
              (doto *driver*
                (go (url "/donate"))
                (wait-visible {:tag :div :fn/text "Custom"}))
              (is (exists? *driver* {:tag :p :fn/has-text
                                     (str "You are currently subscribed to donate "
                                          sub-amount " per month")})))
            (testing "and make an additional one-time donation"
              (is (exists? *driver* {:fn/has-text
                                     "Make an additional one-time donation"}))
              (doto *driver*
                (click {:fn/text one-time-amount})
                (click {:id "continue"})
                (wait-exists {:tag :span :fn/has-text "Pay"} {:timeout 10}))
              ;; Stripe remembers the credit card information and is showing
              ;; the collapsed version of its form.
              (is (exists? *driver* {:fn/has-text "4242"}))
              (doto *driver*
                (click {:tag :button :type "submit"})
                (wait-exists {:fn/text "Donation Complete"} {:timeout 10}))
              (is (exists? *driver* {:tag :p :fn/text (str "You donated "
                                                           one-time-amount
                                                           " today.")})))
            (testing "and cancel it"
              (doto *driver*
                (go (url "/donate"))
                (wait-visible {:tag :div :fn/text "Custom"})
                (click {:fn/text "Cancel Donation Subscription"})
                (wait-visible {:fn/text "Are you sure?"})
                ;; Clicking No should go back to the setup donation page.
                (click {:fn/has-text "No"})
                (wait-visible {:tag :div :fn/text "Custom"})
                (click {:fn/text "Cancel Donation Subscription"})
                (wait-visible {:fn/text "Are you sure?"})
                (click {:fn/has-text "Yes"})
                (wait-visible {:fn/has-text
                               "Your subscription has been cancelled"}))
              (is (exists? *driver* {:tag :a :fn/link "/donate"}))
              (testing "and see that they no longer have a subscription"
                (doto *driver*
                  (go (url "/donate"))
                  (wait-visible {:tag :div :fn/text "Custom"}))
                (is (not (exists? *driver* {:fn/has-text
                                            "You are currently subscribed"})))
                (is (exists? *driver* {:id "donate-type-recurring"})))))))))
