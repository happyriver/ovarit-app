;; fe/errors.cljs -- Test errors for ovarit-app
;; Copyright (C) 2020-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.errors-test
  (:require
   [cljs.test :refer-macros [deftest is testing]]
   [day8.re-frame.test :as rf-test]
   [ovarit.app.fe.db :as db]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.test-utils :as test-utils]
   [re-frame.core :refer [dispatch subscribe reg-event-db]]))

(def error-502
  [{:message "The HTTP call failed."
    :extensions {:status 502}}])

(def example-errors
  "An error returned by re-graph."
  [{:message "Sender and recipient are the same"
    :locations [{:line 2, :column 3}]
    :path ["create_modmail_message"]
    :extensions {:arguments {:subject "$subject", :content "$content",
                             :sid "$sid", :username "$username",
                             :show_mod_username "$show_mod_username"}}}])

(defn test-fixtures
  []
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name)} "Starting test")
  (reg-event-db
   ;; Set up a test event to hand off errors.
   ::receive-errors
   (fn [db [_ errors]]
     (errors/assoc-errors db {:event ::receive-errors
                              :errors errors}))))

(deftest test-error-processing
  (rf-test/run-test-sync
   (test-fixtures)
   (testing "Processing errors returned from graphql"
     (dispatch [::db/initialize-db])
     (test-utils/mute-error-logging true)
     (dispatch [::receive-errors error-502])
     (test-utils/mute-error-logging false)
     (let [errors @(subscribe [::errors/errors])
           msgs (map :msg errors)]
       (is (= msgs ["Could not contact server"]))))))
