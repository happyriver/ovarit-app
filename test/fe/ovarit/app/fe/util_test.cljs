;; fe/util-test.cljs -- Testing of utility functions for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.util-test
  (:require
   [cljs.test :refer-macros [deftest is testing]]
   [day8.re-frame.test :as rf-test]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.test-utils :as test-utils]
   [ovarit.app.fe.util :as util]
   [re-frame.core :refer [dispatch]]))

(defn startup []
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name)} "Starting test"))

(deftest test-markdown-mentions
  (startup)
  (rf-test/run-test-sync
   (dispatch [::util/start-markdown-renderer {:sub-prefix "s"}])
   (testing "When converting markdown to html"
     (testing "user mentions are converted to links"
       (is (= (util/markdown-to-html "Say hi to @foo")
              "<p>Say hi to <a href=\"/u/foo\">@foo</a></p>\n"))
       (is (= (util/markdown-to-html "Say hi to /u/foo")
              "<p>Say hi to <a href=\"/u/foo\">/u/foo</a></p>\n")))
     (testing "sub mentions are converted to links"
       (is (= (util/markdown-to-html "Come say hello in /s/foo")
              "<p>Come say hello in <a href=\"/s/foo\">/s/foo</a></p>\n")))
     (testing "mentions in code blocks are ignored"
       (is (= (util/markdown-to-html "```Some code about /u/foo```")
              "<p><code>Some code about /u/foo</code></p>\n"))))))

(deftest test-markdown-linkification
  (startup)
  (rf-test/run-test-sync
   (dispatch [::util/start-markdown-renderer {:sub-prefix "s"}])
   (testing "Non-relative markdown links have rel and target"
     (is (= (util/markdown-to-html "Eat [Spam](https://spam.com) for lunch")
            (str "<p>Eat <a href=\"https://spam.com\" "
                 "rel=\"noopener nofollow ugc\" target=\"_blank\">"
                 "Spam</a> for lunch</p>\n"))))))

(deftest test-markdown-spoilers
  (startup)
  (rf-test/run-test-sync
   (dispatch [::util/start-markdown-renderer {:sub-prefix "s"}])
   (testing "When converting markdown to html"
     (testing "spoiler tags are converted"
       (is (= (util/markdown-to-html "This is a >!spoiler!<!")
              (str "<p>This is a <spoiler>spoiler</spoiler>!</p>\n")))))))
