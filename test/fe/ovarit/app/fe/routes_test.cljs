;; fe/routes-test.cljs -- Testing routes for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.routes-test
  (:require [cljs.test :refer-macros [deftest is testing]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes.util :refer [parse-url]]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.util :as util]
            [ovarit.app.fe.views.util :refer [url-for]]
            [re-frame.core :as re-frame :refer [dispatch]]
            [re-graph.core :as re-graph]))

(spec/check-asserts true)
(def seed #_999 (rand-int 1000))
(def size 5)

(def index-query-result
  "Startup query with sub-prefix 'o'."
  (-> ::graphql-spec/query.index-query
      spec/gen
      (as-> $
          (gen/fmap
           (fn [result]
             (-> result
                 (assoc-in [:site_configuration :sub_prefix] "o")
                 (assoc-in [:site_configuration :enable_totp] false)))
           $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/query.index-query $))
      util/kebab-case-keys))

(defn register-test-graphql-queries
  "Define the queries to return the generated data above."
  []
  (test-utils/setup-regraph-subscribe-logging)
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result})]]}))))

(defn startup
  "Start up the app with the test data."
  []
  (test-utils/init-logging ::test)
  (log/info {:seed seed
             :test (test-utils/test-name)} "Starting test")
  (register-test-graphql-queries)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))
  (dispatch [::db/initialize-db])
  (dispatch [::settings/load-index-query]))

(deftest test-url-for
  (rf-test/run-test-sync
   (startup)
   (testing "url-for creates expected urls"
     (is (= "/mod/mail/compose" (url-for :modmail/compose)))
     (is (= "/mod/mail/all" (url-for :modmail/mailbox :mailbox "all")))
     (is (= "/mod/mail/new/99" (url-for :modmail/thread
                                        :mailbox "new" :msg 99)))
     (is (= "/mod/admin" (url-for :mod/admin)))
     (is (= "/o/circle/contact_mods" (url-for :sub/contact-mods
                                              :sub "circle")))
     (is (= "/donate" (url-for :donate/setup)))
     (is (= "/donate/cancel" (url-for :donate/cancel)))
     (is (= "/donate/funding_progress" (url-for :donate/funding-progress)))
     (is (= "/donate/success" (url-for :donate/success)))
     (is (= "/admin/stats" (url-for :admin/stats)))
     (is (= "/admin/banned_user_names" (url-for :admin/banned-user-names))))))

(deftest test-parse-url
  (rf-test/run-test-sync
   (startup)
   (re-frame/reg-event-db ::test-parse-url
     (fn [db _]
       (let [routes (:routes db)]
         (is (= {:handler :modmail/compose
                 :url-params {}
                 :fragment ""} (parse-url routes "/mod/mail/compose")))
         (is (= {:handler :modmail/mailbox
                 :route-params {:mailbox "all"}
                 :url-params {}
                 :fragment ""} (parse-url routes "/mod/mail/all")))
         (is (= {:handler :modmail/thread
                 :route-params {:mailbox "new"
                                :msg "99"}
                 :url-params {}
                 :fragment ""} (parse-url routes "/mod/mail/new/99")))
         (is (= {:handler :mod/admin
                 :url-params {}
                 :fragment ""} (parse-url routes "/mod/admin")))
         (is (= {:handler :sub/contact-mods
                 :route-params {:sub "circle"}
                 :url-params {}
                 :fragment ""} (parse-url routes "/o/circle/contact_mods")))
         (is (= {:handler :donate/setup
                 :url-params {}
                 :fragment ""} (parse-url routes "/donate")))
         (is (= {:handler :donate/cancel
                 :url-params {}
                 :fragment ""} (parse-url routes "/donate/cancel")))
         (is (= {:handler :donate/funding-progress
                 :url-params {}
                 :fragment ""} (parse-url routes "/donate/funding_progress")))
         (is (= {:handler :donate/success
                 :url-params {}
                 :fragment ""} (parse-url routes "/donate/success")))
         (is (= {:handler :admin/stats
                 :url-params {}
                 :fragment ""} (parse-url routes "/admin/stats")))
         (is (= {:handler :admin/banned-user-names
                 :url-params {}
                 :fragment ""} (parse-url routes "/admin/banned_user_names")))
         (is (= {:handler :sub/view-single-post
                 :route-params {:pid "110" :sub "circle" :slug "a_post"}
                 :url-params {}
                 :fragment ""} (parse-url routes "/o/circle/110/a_post")))
         (is (= {:handler :sub/view-direct-link
                 :route-params {:pid "110" :sub "circle" :slug "a_post" :cid "xyzzy"}
                 :url-params {}
                 :fragment "comment-xyzzy"}
                (parse-url routes "/o/circle/110/a_post/xyzzy#comment-xyzzy")))
         db)))
   (testing "parse-url finds expected handlers"
     (dispatch [::test-parse-url]))))
