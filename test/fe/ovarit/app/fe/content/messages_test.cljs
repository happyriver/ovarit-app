;; fe/content/messages_test.cljs -- Testing modmail and messaging for ovarit-app
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.messages-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.modmail :as modmail]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes :as routes]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.forms.compose-message :as compose-message]
            [ovarit.app.fe.ui.forms.core :as forms]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.util :as util]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]))

(spec/check-asserts true)
(def seed #_999 (rand-int 1000))
(def size 5)

(def get-all-subs-result
  "A non-empty generated list of subs."
  (-> ::graphql-spec/query.get-all-subs
      (spec/and (fn [data]
                  (seq (get-in data [:all_subs :edges]))))
      spec/gen
      (as-> $ (gen/fmap
               #(assoc-in % [:all_subs :pageInfo :hasNextPage] false) $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/query.get-all-subs $))))

(def sub
  "The first sub in `get-all-subs`."
  (-> get-all-subs-result
      :all_subs :edges first :node))

(def index-query-result
  "Startup query with a user who is a moderator of `sub-name`."
  (-> ::graphql-spec/query.index-query
      (spec/and #(map? (:current_user %)))
      spec/gen
      (as-> $
          (gen/fmap
           (fn [result]
             (-> result
                 (update-in [:current_user :attributes] assoc
                            :can_admin false)
                 (assoc-in [:current_user :subs_moderated]
                           [{:sub (-> sub
                                      (select-keys [:sid :name])
                                      (assoc :open_report_count 0
                                             :unread_modmail_count 0))
                             :moderation_level "MODERATOR"}])))
           $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/query.index-query $))
      util/kebab-case-keys))

(defn- fixup-modmail-thread-by-id-result
  [result]
  (let [timed-edges (map-indexed
                     (fn [num edge]
                       (assoc-in edge [:node :time]
                                 (str (- 1699804535142 num))))
                     (-> result :modmail_thread :edges))]
    (-> result
        (assoc-in [:modmail_thread :edges] timed-edges)
        (update :message_thread_by_id assoc
                :sub (select-keys sub [:sid :name])
                :mailbox "INBOX")
        (assoc-in [:message_thread_by_id :first_message]
                  (-> timed-edges
                      last
                      :node)))))

(def get-modmail-thread-by-id-result
  "A generated message thread and some messages."
  (-> ::graphql-spec/query.get-modmail-thread-by-id
      (spec/and #(> (count (get-in % [:modmail_thread :edges])) 1))
      (spec/and #(empty? (filter (fn [node]
                                   (#{"USER_TO_USER" "MOD_NOTIFICATION"}
                                    (get-in node [:node :mtype])))
                                 (get-in % [:modmail_thread :edges]))))
      spec/gen
      (->> (gen/fmap fixup-modmail-thread-by-id-result))
      (gen/generate size seed)
      (->> (spec/assert ::graphql-spec/query.get-modmail-thread-by-id))))

(def get-modmail-category-result
  "The generated message thread and messages in modmail category form."
  (let [thread (:message_thread_by_id get-modmail-thread-by-id-result)
        messages (:modmail_thread get-modmail-thread-by-id-result)]
    (->>
     {:modmail_threads
      {:edges [{:node (-> (select-keys thread [:id :sub :subject :mailbox
                                               :reply_count :first_message])
                          (assoc :latest_message
                                 (-> messages
                                     :edges
                                     first
                                     :node)))}]
       :pageInfo {:hasNextPage false :endCursor "x"}}}
     (spec/assert ::graphql-spec/query.get-modmail-category))))

(def thread-id
  "The id of the generated thread."
  (get-in get-modmail-thread-by-id-result [:message_thread_by_id :id]))

(def modmail-reply
  "A reply to a modmail message."
  (-> ::graphql-spec/mutation.create-modmail-reply
      (spec/and #(= (get-in % [:create_modmail_reply :mtype])
                    "MOD_TO_USER_AS_MOD"))
      spec/gen
      (as-> $
          ;; Make this newer than everything else.
          (gen/fmap (fn [result]
                      (assoc-in result [:create_modmail_reply :time]
                                (str (+ 1699804535142 100000))))
                    $))
      gen/generate))

(defn get-modmail-thread-by-id
  "Emulate the get_modmail_thread_by_id query."
  [{:keys [thread_id]}]
  (testing "requests the thread"
    (is (= thread-id thread_id))
    get-modmail-thread-by-id-result))

(defn register-test-graphql-queries
  "Define the queries to return the generated data above."
  []
  (test-utils/setup-regraph-subscribe-logging)
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query variables callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       "get_all_subs" get-all-subs-result
                       "get_modmail_thread_by_id" (get-modmail-thread-by-id
                                                   variables)
                       "get_modmail_category" get-modmail-category-result
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result})]]}))))

(defn startup
  "Start up the app with the test data."
  []
  (test-utils/init-logging ::test)
  (log/info {:seed seed
             :test (test-utils/test-name)} "Starting test")
  (register-test-graphql-queries)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))
  (dispatch [::db/initialize-db])
  (dispatch [::settings/load-index-query]))

(deftest test-modmail-reply
  (rf-test/run-test-sync
   (startup)
   (testing "modmail thread view"
     (let [compose-form ::compose-message/compose-form
           content (get-in modmail-reply [:create_modmail_reply :content])
           modmail-replies (-> get-modmail-thread-by-id-result
                               (get-in [:modmail_thread :edges])
                               butlast
                               reverse
                               (as-> $ (map #(get-in % [:node :content]) $)))
           thread-sub (subscribe [::modmail/modmail-thread])]
       (dispatch [::routes/show-modmail {:msg thread-id
                                         :mailbox "all"
                                         :sub (:name sub)}])
       (testing "reply"
         (is (= modmail-replies (->> @thread-sub
                                     (map :content)
                                     (remove nil?))))
         (dispatch [::forms/edit compose-form :content content])
         (is (= :READY @(subscribe [::forms/form-state compose-form])))

         (testing "adds the message to the thread"
           (re-frame/reg-event-fx ::re-graph/mutate
             (fn [_ [_ {:keys [query variables callback]}]]
               (testing "expected create_modmail_reply mutation"
                 (is (= {:thread_id thread-id
                         :content content
                         :send_to_user false} variables))
                 (let [mutation (test-utils/query-name-for-test query)
                       result modmail-reply]
                   (is (= mutation "create_modmail_reply"))
                   {:fx [[:dispatch (update callback 1
                                            assoc :response {:data result})]]}))))
           (dispatch [::forms/action compose-form :send])
           (is (= (concat modmail-replies [content])
                  (->> @thread-sub
                       (map :content)
                       (remove nil?))))))))))

(deftest test-modmail-unread-change
  (rf-test/run-test-sync
   (let [mutation-vars (atom nil)]
     (startup)

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (testing "expected update_message_unread mutation"
           (is (= "update_message_unread" (test-utils/query-name-for-test query)))
           (reset! mutation-vars variables)
           {:fx [[:dispatch (update callback 1
                                    assoc :response
                                    {:data {:update_message_unread
                                            (:mid variables)}})]]})))
     (let [messages (-> get-modmail-thread-by-id-result
                        (get-in [:modmail_thread :edges]))
           replies (-> messages
                       butlast
                       reverse
                       (as-> $ (map :node $)))
           first-message (-> messages
                             last
                             :node)]
       (testing "in modmail thread view"
         (dispatch [::routes/show-modmail {:msg thread-id
                                           :mailbox "all"
                                           :sub (:name sub)}])
         (testing "mark unread changes first message unread value"
           (let [{:keys [mid unread]} first-message
                 parent-sub (subscribe [::modmail/modmail-thread-parent])]
             (is (= unread
                    (:unread? @parent-sub)))
             (dispatch [::modmail/set-unread mid (not unread)])
             (is (= {:mid mid :unread (not unread)} @mutation-vars))
             (testing "and updates it in db"
               (is (= (not unread)
                      (:unread? @parent-sub))))))

         (testing "mark unread changes reply unread value"
           (reset! mutation-vars nil)
           (let [{:keys [mid unread]} (last replies)
                 thread-sub (subscribe [::modmail/modmail-thread])]
             (is (= unread
                    (:unread? (last @thread-sub))))
             (dispatch [::modmail/set-unread mid (not unread)])
             (is (= {:mid mid :unread (not unread)} @mutation-vars))
             (testing "and updates it in db"
               (is (= (not unread)
                      (:unread? (last @thread-sub))))))))

       (testing "in modmail mailbox view"
         (reset! mutation-vars nil)
         (dispatch [::routes/show-modmail {:mailbox "all"
                                           :sub (:name sub)}])
         (testing "mark unread changes unread value"
           (reset! mutation-vars nil)
           (let [thread-sub (subscribe [::modmail/modmail-mailbox-threads])
                 {:keys [mid unread?]} (-> @thread-sub
                                           first)]
             (is (= mid (:mid (last replies))))
             (is (= unread? (:unread (last replies))))
             (dispatch [::modmail/set-unread mid (not unread?)])
             (is (= {:mid mid :unread (not unread?)} @mutation-vars))
             (testing "and updates it in db"
               (is (= (not unread?) (-> @thread-sub
                                        first
                                        :unread?)))))))))))

(deftest test-modmail-thread-unread-change
  (testing "marking a thread read or unread"
    (rf-test/run-test-sync
     (let [mutation-vars (atom nil)]
       (startup)
       (re-frame/reg-event-fx ::re-graph/mutate
         (fn [_ [_ {:keys [query variables callback]}]]
           (testing "expected update_thread_unread mutation"
             (is (= "update_thread_unread"
                    (test-utils/query-name-for-test query)))
             (reset! mutation-vars variables)
             {:fx [[:dispatch (conj callback {:data (:mid variables)})]]})))
       (testing "in modmail thread view"
         (dispatch [::routes/show-modmail {:msg thread-id
                                           :mailbox "all"
                                           :sub (:name sub)}])
         (testing "updates all messages in the thread"
           (let [info-sub (subscribe [::modmail/modmail-thread-info])
                 parent-sub (subscribe [::modmail/modmail-thread-parent])
                 thread-sub (subscribe [::modmail/modmail-thread])]
             (is (= thread-id (:thread-id @info-sub)))
             (dispatch [::modmail/set-thread-unread thread-id true])
             (is (= {:thread_id thread-id :unread true} @mutation-vars))
             (testing "and updates it in db (unread)"
               (is (true? (:unread? @parent-sub)))
               (is @(subscribe [::modmail/modmail-thread-any-unread]))
               (is (->> @thread-sub
                        (filter :content)
                        (every? #(true? (:unread? %))))))
             (dispatch [::modmail/set-thread-unread thread-id false])
             (is (= {:thread_id thread-id :unread false} @mutation-vars))
             (testing "and updates it in db (read)"
               (is (false? (:unread? @parent-sub)))
               (is (not @(subscribe [::modmail/modmail-thread-any-unread])))
               (is (->> @thread-sub
                        (filter :content)
                        (every? #(false? (:unread? %)))))))))))))

(deftest test-modmail-archive-thread
  (rf-test/run-test-sync
   (let [mutation-vars (atom nil)]
     (startup)

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (testing "expected update_modmail_mailbox mutation"
           (is (= "update_modmail_mailbox"
                  (test-utils/query-name-for-test query)))
           (reset! mutation-vars variables)
           {:fx [[:dispatch (update callback 1 assoc :response
                                    {:data {:update_modmail_mailbox
                                            {:id
                                             (:thread_id variables)}}})]]})))
     (testing "in modmail thread view"
       (dispatch [::routes/show-modmail {:msg thread-id
                                         :mailbox "all"
                                         :sub (:name sub)}])
       (testing "change mailbox changes thread mailbox"
         (let [thread-info (subscribe [::modmail/modmail-thread-info])]
           (is (= :INBOX (:mailbox @thread-info)))
           (is (= thread-id (:thread-id @thread-info)))
           (dispatch [::modmail/set-archived thread-id true])
           (is (= {:thread_id thread-id :mailbox :ARCHIVED} @mutation-vars))
           (testing "and updates it in db"
             (is (= :ARCHIVED (:mailbox @thread-info)))))))

     (testing "in modmail mailbox view"
       (reset! mutation-vars nil)
       (dispatch [::routes/show-modmail {:mailbox "all" :sub (:name sub)}])
       (testing "change mailbox changes thread mailbox"
         (reset! mutation-vars nil)
         (let [thread-sub (subscribe [::modmail/modmail-mailbox-threads])
               thread-info (first @thread-sub)]
           (is (= thread-id (:thread-id thread-info)))
           (is (= :INBOX (:mailbox thread-info)))
           (dispatch [::modmail/set-archived thread-id true])
           (is (= {:thread_id thread-id :mailbox :ARCHIVED} @mutation-vars))
           (testing "and updates it in db"
             (is (= :ARCHIVED (:mailbox (first @thread-sub)))))))))))
