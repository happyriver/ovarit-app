;; fe/content/auth_test.cljs -- Testing login-related forms for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.auth-test
  (:require [camel-snake-kebab.core :as csk]
            [camel-snake-kebab.extras :as cske]
            [cljs.test :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes :as routes]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.forms.auth :as-alias auth-forms]
            [ovarit.app.fe.ui.forms.core :as forms]
            [ovarit.app.fe.ui.forms.login :as login-form]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.util :as util]
            [ovarit.app.fe.views.util :refer [url-for]]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]))

(spec/check-asserts true)
(def seed #_999 (rand-int 1000))
(def size 5)

(defn generate-index-query-result
  "Startup query with an authenticated user."
  []
  (try
    (-> ::graphql-spec/query.index-query
        (spec/and #(map? (:current_user %))
                  #(string? (get-in % [:current_user :name])))
        spec/gen
        (as-> $ (gen/fmap
                 #(cond-> %
                    true (assoc-in [:site_configuration :enable_totp] false)) $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.index-query $))
        util/kebab-case-keys)
    (catch js/Error e
      (log/error {:exception e} "Error generating index query result"))))

(defn register-test-graphql-queries
  "Define the queries to return the generated data."
  [{:keys [registration-settings]}]
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       "registration_settings" registration-settings
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result
                                                  :errors nil})]]})))
  (re-frame/reg-event-fx ::re-graph/subscribe
    (fn [_ _] {}))
  (re-frame/reg-event-fx ::re-graph/unsubscribe
    (fn [_ _] {})))

(def routed-to-url (atom ""))

(defn startup
  "Start up the app with the test data."
  [{:keys [index-query-result] :as test-data}]
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name) :seed seed} "Starting test")
  (register-test-graphql-queries test-data)

  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))

  ;; Prevent the app from redirecting away from the test page.
  (re-frame/reg-fx ::routes/redirect
    (fn [url]
      (reset! routed-to-url url)))
  (re-frame/reg-fx ::routes/set-url
    (fn [url]
      (reset! routed-to-url url)))

  ;; Set up initial app state
  (re-frame/reg-event-fx ::routes
    (fn [{:keys [db]} _]
      (routes/app-routes (:routes db))
      nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))

  (dispatch [::db/initialize-db])
  (dispatch [::routes])
  (dispatch [::settings/load-index-query]))

(deftest test-registration
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         settings {:current_user nil
                   :site_configuration {:registration_enabled true
                                        :password_length 12
                                        :password_complexity 3
                                        :username_max_length 32}
                   :invite_code_settings {:required true}}
         test-data {:index-query-result index-query
                    :registration-settings settings}
         registration-form ::auth-forms/registration
         reg {:invite-code "test-invite"
              :email "test@test.test"
              :username "tester"
              :password "password123#"}
         {:keys [invite-code email username password]} reg
         resp {:code "delete_code"
               :uid "uid"}
         ajax-args (atom nil)]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload success-event] :as state}]
         (reset! ajax-args payload)
         (is (= "/ovarit_auth/create" url))
         (re-frame/dispatch (conj success-event (assoc state :response resp)))))

     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-registration])
     (dispatch [::forms/edit registration-form :invite-code invite-code])
     (dispatch [::forms/edit registration-form :email email])
     (dispatch [::forms/edit registration-form :username username])
     (dispatch [::forms/edit registration-form :password password])
     (dispatch [::forms/edit registration-form :confirm-password password])
     (dispatch [::forms/edit registration-form :accept-tos? true])
     (dispatch [::forms/action registration-form :send])
     (is (= (cske/transform-keys csk/->snake_case reg)
            @ajax-args))
     (is (= :SHOW-SUCCESS
            @(subscribe [::forms/form-state registration-form])))
     (is (= (url-for :auth/verify-registration
                     :query-args {:delete_code (:code resp)
                                  :uid (:uid resp)})
            @routed-to-url)))))

(deftest test-registration-failure
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         settings {:current_user nil
                   :site_configuration {:registration_enabled true
                                        :password_length 12
                                        :password_complexity 3
                                        :username_max_length 32}
                   :invite_code_settings {:required false}}
         test-data {:index-query-result index-query
                    :registration-settings settings}
         registration-form ::auth-forms/registration
         reg {:invite-code ""
              :email "test@test.test"
              :username "tester"
              :password "password123#"}
         {:keys [email username password]} reg
         ajax-args (atom nil)]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload error-event] :as state}]
         (is (= "/ovarit_auth/create" url))
         (reset! ajax-args payload)
         (re-frame/dispatch (conj error-event
                                  (assoc state :error-response
                                         {:status 400
                                          :status-text "Bad Request"
                                          :response
                                          {:reason
                                           "Username already in use"}})))))
     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-registration])
     (dispatch [::forms/edit registration-form :email email])
     (dispatch [::forms/edit registration-form :username username])
     (dispatch [::forms/edit registration-form :password password])
     (dispatch [::forms/edit registration-form :confirm-password password])
     (dispatch [::forms/edit registration-form :accept-tos? true])
     (test-utils/mute-error-logging true)
     (dispatch [::forms/action registration-form :send])
     (test-utils/mute-error-logging false)
     (is (= (cske/transform-keys csk/->snake_case reg) @ajax-args))
     (is (= :SHOW-FAILURE
            @(subscribe [::forms/form-state registration-form])))
     (is (nil? @routed-to-url))
     (is (= "Username already in use"
            (first @(subscribe [::forms/send-errors registration-form])))))))

(deftest test-login
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         test-data {:index-query-result index-query}
         login-form ::login-form/login
         login {:username "tester"
                :password "password123#"
                :remember_me false}
         {:keys [username password]} login
         remember-me? (:remember_me login)
         resp {:status 200 :status-text "OK"}
         ajax-args (atom nil)]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload success-event] :as state}]
         (is (= "/ovarit_auth/authenticate" url))
         (reset! ajax-args payload)
         (re-frame/dispatch (conj success-event (assoc state :response resp)))))

     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-login])
     (dispatch [::forms/edit login-form :username username])
     (dispatch [::forms/edit login-form :password password])
     (dispatch [::forms/edit login-form :remember-me? remember-me?])
     (dispatch [::forms/action login-form :send])
     (is (= (cske/transform-keys csk/->snake_case login) @ajax-args))
     (is (= (url-for :home/index) @routed-to-url)))))

(deftest test-login-failure
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         test-data {:index-query-result index-query}
         login-form ::login-form/login
         login {:username "tester"
                :password "password123#"
                :remember_me true}
         {:keys [username password]} login
         remember-me? (:remember_me login)
         ajax-args (atom nil)]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload error-event] :as state}]
         (is (= "/ovarit_auth/authenticate" url))
         (reset! ajax-args payload)
         (re-frame/dispatch (conj error-event
                                  (assoc state :error-response
                                         {:status 401
                                          :status-text "Unauthorized"
                                          :response
                                          {:reason "Incorrect password"}})))))
     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-login])
     (dispatch [::forms/edit login-form :username username])
     (dispatch [::forms/edit login-form :password password])
     (dispatch [::forms/edit login-form :remember-me? remember-me?])
     (test-utils/mute-error-logging true)
     (dispatch [::forms/action login-form :send])
     (test-utils/mute-error-logging false)
     (is (= (cske/transform-keys csk/->snake_case login) @ajax-args))
     (is (= :SHOW-FAILURE
            @(subscribe [::forms/form-state login-form])))
     (is (nil? @routed-to-url))
     (is (= "Incorrect password"
            (first @(subscribe [::forms/send-errors login-form])))))))

(deftest test-resend
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         test-data {:index-query-result index-query}
         resend-form ::auth-forms/username
         username "tester"
         ajax-args (atom nil)
         resp {:status 200 :status-text "OK"}]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload success-event] :as state}]
         (reset! ajax-args payload)
         (is (= "/ovarit_auth/create/resend" url))
         (re-frame/dispatch (conj success-event (assoc state :response resp)))))

     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-resend-verification-email])
     (dispatch [::forms/edit resend-form :username username])
     (dispatch [::forms/action resend-form :resend])
     (is (= {:username username} @ajax-args))
     (is (= :SHOW-SUCCESS @(subscribe [::forms/form-state resend-form]))))))

(deftest test-reset
  (rf-test/run-test-sync
   (let [index-query (-> (generate-index-query-result)
                         (assoc :current-user nil))
         test-data {:index-query-result index-query}
         reset-form ::auth-forms/start-reset
         username "tester"
         email "tester@example.org"
         resp {:status 200 :status-text "OK"}
         ajax-args (atom nil)]

     (re-frame/reg-fx ::util/ajax-post
       (fn [{:keys [url payload success-event] :as state}]
         (is (= "/ovarit_auth/reset" url))
         (reset! ajax-args payload)
         (re-frame/dispatch (conj success-event (assoc state :response resp)))))

     (reset! routed-to-url nil)
     (startup test-data)
     (dispatch [::routes/show-start-reset-password])
     (dispatch [::forms/edit reset-form :username username])
     (dispatch [::forms/edit reset-form :email email])
     (dispatch [::forms/action reset-form :reset])
     (is (= {:username username
             :email email} @ajax-args))
     (is (= :SHOW-SUCCESS @(subscribe [::forms/form-state reset-form]))))))
