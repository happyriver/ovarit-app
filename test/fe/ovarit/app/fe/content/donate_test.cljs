;; fe/content/donate-test.cljs -- Testing of donation content for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.donate-test
  (:require [cljs.test :as t :refer-macros [deftest testing is]]
            [ovarit.app.fe.content.donate :as donate]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.test-utils :as test-utils]))

(deftest test-parse-custom-amount
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name)} "Starting test")
  (testing "rejection of invalid amounts"
    (let [fails ["0" "$0" "$0.00" "abc" "$abc" "$abc.xyz" "$$3.90" "3.5.4"
                 "04" "08.50" "7.0" "68." "7.03" "36.99"]
          fails-map (into {} (map (fn [x] [x 0]) fails))]
      (is (= fails-map (into {} (map (fn [x] [x (donate/parse-amount x)])
                                     fails))))))
  (testing "parsing of valid amounts"
    (let [successes {"1" 1 "$10" 10 "7.00" 7 "$36.00" 36}]
      (is (= successes (into {} (map (fn [[x _]] [x (donate/parse-amount x)])
                                     successes)))))))
