;; fe/content/comment_test.cljs -- Testing comments for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.comment-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.comment :as comment]
            [ovarit.app.fe.content.comment.moderation :as comment-mod]
            [ovarit.app.fe.content.donate :as donate]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes :as routes]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.forms.compose-comment :as comment-form]
            [ovarit.app.fe.ui.forms.core :as forms]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.util :as util]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]))

(spec/check-asserts true)

(def seed #_999 (rand-int 1000))
(def size 5)

(def failure-response {:errors
                       {:message "The HTTP call failed."
                        :extensions {:status 502}}})

(defn make-comment-tree
  "Make a JSON comment tree object that matches the comments in the list."
  [{:keys [comments]} post-result]
  (let [comment-tree (map (fn [{:keys [cid]}]
                            {:cid cid
                             :children []
                             :level 0
                             :time 1699804535142
                             :uid "user"
                             :status "ACTIVE"}) comments)
        pid (get-in post-result [:post_by_pid :pid])]
    {:tree_json (->> comment-tree
                     clj->js
                     js/JSON.stringify)
     :comments (map #(-> %
                         ;; Set all the comment authors to active
                         ;; status.  This gets rid of PROBATION which
                         ;; is a possible value for user status, but
                         ;; not for authors.
                         (assoc-in [:author :status] "ACTIVE")
                         (assoc-in [:parent_post :pid] pid)) comments)}))

(defn create-post-result
  []
  (try
    (-> ::graphql-spec/query.get-single-post
        spec/gen
        (as-> $ (gen/fmap
                 #(-> %
                      (assoc-in [:post_by_pid :status] "ACTIVE")
                      (assoc-in [:post_by_pid :slug] "_")
                      (assoc-in [:post_by_pid :locked] false)
                      (assoc-in [:post_by_pid :is_archived] false)
                      (assoc-in [:post_by_pid :author :status] "ACTIVE")
                      (assoc-in [:post_by_pid :sub :restricted] false))
                 $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.get-single-post $)))
    (catch js/Error e
      (log/error {:exception e} "Error generating post result"))))

(defn create-comment-tree-result
  [post]
  (try
    (-> ::graphql-spec/query.get-comment-tree
        (spec/and #(seq (get-in % [:post_by_pid :comment_tree :comments])))
        spec/gen
        (as-> $(gen/fmap
                #(-> %
                     (update-in [:post_by_pid :comment_tree] make-comment-tree
                                post)
                     (assoc-in [:post_by_pid :default_sort]
                               (get-in post [:post_by_pid :default_sort])))
                $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.get-comment-tree $)))
    (catch js/Error e
      (log/error {:exception e} "Error generating comment tree result"))))

(defn create-index-query-result
  "Startup query with an authenticated user."
  []
  (try
    (-> ::graphql-spec/query.index-query
        (spec/and #(map? (:current_user %)))
        (spec/and #(string? (get-in % [:current_user :name])))
        spec/gen
        (as-> $ (gen/fmap
                 #(assoc-in % [:site_configuration :enable_totp] false) $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.index-query $))
        util/kebab-case-keys)
    (catch js/Error e
      (log/error {:exception e} "Error generating index query result"))))

(defn create-comment-result
  "Generate a create-comment mutation result."
  []
  (-> ::graphql-spec/mutation.create-comment
      spec/gen
      (as-> $ (gen/fmap
               #(-> %
                    (assoc-in [:create_comment :status] "ACTIVE")
                    (assoc-in [:create_comment :author :status] "ACTIVE"))
               $))
      (gen/generate size seed)
      (as-> $ (spec/assert ::graphql-spec/mutation.create-comment $))))

(defn create-content-block-result
  "Response for the content block query."
  []
  {:current_user {:content_blocks []}})

(defn register-test-graphql-queries
  "Define the queries to return the generated data above."
  [{:keys [post-result comment-tree-result content-block-result]}]
  (test-utils/setup-regraph-subscribe-logging)
  (log/debug {:comment-tree-result comment-tree-result} "register")
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       "get_single_post" post-result
                       "get_comment_tree" comment-tree-result
                       "content_blocks" content-block-result
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result
                                                  :errors nil})]]}))))

(defn startup-data
  []
  (let [post (create-post-result)
        comment-tree (create-comment-tree-result post)]
    {:post-result post
     :comment-tree-result comment-tree
     :content-block-result (create-content-block-result)
     :index-query-result (create-index-query-result)}))

(defn startup
  "Start up the app with the test data."
  [{:keys [index-query-result] :as data}]
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name) :seed seed} "Starting test")
  (register-test-graphql-queries data)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  ;; Stop some things that spam the logs.
  (re-frame/reg-event-fx ::comment/start-scroll-watcher (constantly nil))
  (re-frame/reg-event-fx ::comment/start-resize-watcher (constantly nil))
  (re-frame/reg-event-fx ::donate/subscribe-funding-progress (constantly nil))
  (re-frame/reg-event-fx ::routes
    (fn [{:keys [db]} _]
      (routes/app-routes (:routes db))
      nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))
  (dispatch [::db/initialize-db])
  (dispatch [::routes])
  (dispatch [::settings/load-index-query]))

(deftest test-top-level-comment-reply
  (rf-test/run-test-sync
   (let [data (startup-data)
         post (get-in data [:post-result :post_by_pid])
         form (comment-form/form-id :reply nil)
         cid "CID"
         mutation-vars (atom nil)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (log/debug {:variables variables} "mutation")
         (let [gql-query (test-utils/query-name-for-test query)
               {:keys [content]} variables
               result (-> (create-comment-result)
                          (assoc-in [:create_comment :cid] cid)
                          (assoc-in [:create_comment :content] content))]
           (case gql-query
             "update_post_viewed" nil

             "create_comment"
             (do (reset! mutation-vars variables)
                 {:fx [[:dispatch (update callback 1
                                          assoc :response {:data result
                                                           :errors nil})]]})
             (is (= gql-query false) "unexpected query")))))

     (startup data)
     (testing "a comment can be created"
       (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                             :pid (:pid post)
                                             :slug "_"}])
       (dispatch [::forms/edit form :content "Test reply"])
       (dispatch [::forms/action form :send])
       (is (= {:pid (:pid post)
               :content "Test reply"
               :parent_cid nil} @mutation-vars))
       (testing "and shows up first in the comment list"
         (is (= cid (-> @(subscribe [::comment/child-comments nil])
                        first
                        :cid)))
         (is (= "Test reply" @(subscribe [::comment/content cid]))))))))

(deftest test-distinguish-comment-mod
  (rf-test/run-test-sync
   (let [test-data (startup-data)
         {:keys [index-query-result post-result]} test-data
         {:keys [pid sub]} (:post_by_pid post-result)
         {sid :sid sub-name :name} sub
         user (-> (:current-user index-query-result)
                  (select-keys [:uid :name])
                  (assoc :status "ACTIVE"))
         data (-> test-data
                  (update-in [:index-query-result :current-user :subs-moderated]
                             conj {:sub {:sid sid :name sub-name
                                         :open-report-count 0
                                         :unread-modmail-count 0}
                                   :moderation-level "MODERATOR"})
                  (update-in [:comment-tree-result :post_by_pid :comment_tree
                              :comments] vec)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :author] user)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :distinguish] nil))
         cid (get-in data [:comment-tree-result :post_by_pid
                           :comment_tree :comments 0 :cid])
         mutation-vars (atom nil)
         fail (atom false)]

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (log/debug {:query query :variables variables} "mutation")
         (let [gql-query (test-utils/query-name-for-test query)]
           (case gql-query
             "update_post_viewed" nil

             "distinguish_comment"
             (do (reset! mutation-vars variables)
                 (let [distinguish (when (:distinguish variables)
                                     (name (:distinguish variables)))
                       response (if @fail
                                  failure-response
                                  {:data {:distinguish_comment distinguish}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup data)
     (testing "a comment"
       (dispatch [::routes/show-single-post {:sub sub-name :pid pid :slug "_"}])
       (testing "can be distinguished by a mod author"
         (is @(subscribe [::comment/can-distinguish? cid]))
         (dispatch [::comment-mod/edit-distinguish cid])
         (is (= {:cid cid :sid sid :distinguish :MOD} @mutation-vars))
         (is (= :MOD (:distinguish @(subscribe [::comment/details cid])))))
       (testing "has the same distinguish state after an error"
         (reset! fail true)
         (test-utils/mute-error-logging true)
         (dispatch [::comment-mod/edit-distinguish cid])
         (is (= {:cid cid :sid sid :distinguish nil} @mutation-vars))
         (is (= :MOD (:distinguish @(subscribe [::comment/details cid]))))
         (reset! fail false)
         (test-utils/mute-error-logging false))
       (testing "can be un-distinguished by a mod author"
         (dispatch [::comment-mod/edit-distinguish cid])
         (is (= {:cid cid :sid sid :distinguish nil} @mutation-vars))
         (is (nil? (:distinguish @(subscribe [::comment/details cid])))))))))

(deftest test-checkoff-comment
  (rf-test/run-test-sync
   (let [test-data (startup-data)
         {:keys [index-query-result post-result]} test-data
         {:keys [pid sub]} (:post_by_pid post-result)
         {:keys [sid name]} sub
         username (get-in index-query-result [:current-user :name])
         data (-> test-data
                  (update-in [:index-query-result :current-user :subs-moderated]
                             conj {:sub {:sid sid :name name
                                         :open-report-count 0
                                         :unread-modmail-count 0}
                                   :moderation-level "MODERATOR"})
                  (update-in [:comment-tree-result :post_by_pid :comment_tree
                              :comments] vec)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :checkoff] nil)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :status] "ACTIVE"))
         cid (get-in data [:comment-tree-result :post_by_pid
                           :comment_tree :comments 0 :cid])
         time (.toISOString (js/Date.))
         checkoff {:user {:name username}
                   :time time}
         mutation-vars (atom nil)
         fail (atom false)]

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (log/debug {:query query :variables variables} "mutation")
         (let [gql-query (test-utils/query-name-for-test query)]
           (case gql-query
             "update_post_viewed" nil

             "checkoff_comment"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:checkoff_comment checkoff}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             "un_checkoff_comment"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:un_checkoff_comment nil}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup data)
     (testing "a comment"
       (dispatch [::routes/show-single-post {:sub name :pid pid :slug "_"}])
       (testing "can be checked off by a mod"
         (is @(subscribe [::comment/can-check-off? cid]))
         (dispatch [::comment-mod/check-off cid])
         (is (= {:cid cid :sid sid} @mutation-vars))
         (is (= {:user {:name username} :time time}
                @(subscribe [::comment/checkoff cid]))))
       (testing "has the same checkoff state after an error unchecking"
         (is @(subscribe [::comment/can-uncheck? cid]))
         (reset! fail true)
         (reset! mutation-vars nil)
         (test-utils/mute-error-logging true)
         (dispatch [::comment-mod/uncheck cid])
         (test-utils/mute-error-logging false)
         (is (= {:cid cid :sid sid} @mutation-vars))
         (is (= {:user {:name username} :time time}
                @(subscribe [::comment/checkoff cid])))
         (reset! fail false))
       (testing "can be un-checked by a mod"
         (reset! mutation-vars nil)
         (is @(subscribe [::comment/can-uncheck? cid]))
         (dispatch [::comment-mod/uncheck cid])
         (is (= {:cid cid :sid sid} @mutation-vars))
         (is (nil? @(subscribe [::comment/checkoff cid]))))))))

(deftest test-update-comment-content
  (rf-test/run-test-sync
   (let [test-data (startup-data)
         {:keys [index-query-result post-result]} test-data
         {:keys [pid sub]} (:post_by_pid post-result)
         {:keys [name]} sub
         user (-> (:current-user index-query-result)
                  (select-keys [:uid :name])
                  (assoc :status "ACTIVE"))
         data (-> test-data
                  (update-in [:comment-tree-result :post_by_pid :comment_tree
                              :comments] vec)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :author] user)
                  (assoc-in [:comment-tree-result :post_by_pid :comment_tree
                             :comments 0 :status] "ACTIVE")
                  (update-in [:comment-tree-result :post_by_pid :comment_tree
                              :comments 0 :content] #(or % "test")))
         {:keys [cid content]} (get-in data [:comment-tree-result :post_by_pid
                                             :comment_tree :comments 0])
         form (comment-form/form-id :edit cid)
         mutation-vars (atom nil)]

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (log/debug {:query query :variables variables} "mutation")
         (let [gql-query (test-utils/query-name-for-test query)]
           (case gql-query
             "update_post_viewed" nil

             "update_comment_content"
             (do (reset! mutation-vars variables)
                 (let [response {:data {:update_comment_content true}
                                 :errors nil}]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup data)
     (testing "a comment"
       (dispatch [::routes/show-single-post {:sub name :pid pid :slug "_"}])
       (testing "can be edited by its author"
         (is @(subscribe [::comment/can-edit? cid]))
         (dispatch [::comment/toggle-editor cid])
         (is (= content @(subscribe [::forms/field form :content])))
         (dispatch [::forms/edit form :content "Test edit"])
         (dispatch [::forms/action form :send])
         (is (= {:cid cid :content "Test edit"} @mutation-vars))
         (is (= "Test edit" (-> @(subscribe [::comment/details cid])
                                :content))))))))
