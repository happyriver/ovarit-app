;; fe/content/moderation-test.cljs -- Testing of moderation content for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.moderation-test
  (:require [cljs.test :as t :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.moderation :as moderation]
            [ovarit.app.fe.content.subs :as subs]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes :as routes]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.forms.core :as forms]
            [ovarit.app.fe.ui.forms.moderation :as mod-forms]
            [ovarit.app.fe.util :as util]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.content.settings :as settings]))

(spec/check-asserts true)

(def seed #_999 (rand-int 1000))
(def size 5)

(def subinfo
  {:all_subs {:edges [{:node {:sid "9bcb7892-1441-4dd8-bf3b-023f339cba54"
                              :name "TestSub"
                              :creation "1639631483177"
                              :subscriber_count 1
                              :post_count 0}}
                      {:node {:sid "6cb37694-e904-4614-a18a-80d9e13de623"
                              :name "TestSub2"
                              :creation "1639631483158"
                              :subscriber_count 1
                              :post_count 0}}]
              :pageInfo {:hasNextPage false
                         :endCursor "TestSub2"}}})

(def subs-moderated
  [{:sub {:name "TestSub"
          :sid "9bcb7892-1441-4dd8-bf3b-023f339cba54"
          :open_report_count 0
          :unread_modmail_count 0
          :new_unread_modmail_count 0
          :in_progress_unread_modmail_count 0
          :all_unread_modmail_count 0
          :discussion_unread_modmail_count 0
          :notification_unread_modmail_count 0}
    :moderation-level "OWNER"}
   {:sub {:name "TestSub2"
          :sid "6cb37694-e904-4614-a18a-80d9e13de623"
          :open_report_count 0
          :unread_modmail_count 0
          :new_unread_modmail_count 0
          :in_progress_unread_modmail_count 0
          :all_unread_modmail_count 0
          :discussion_unread_modmail_count 0
          :notification_unread_modmail_count 0}
    :moderation-level "MODERATOR"}])

(def sids-by-name
  {"TestSub" "9bcb7892-1441-4dd8-bf3b-023f339cba54"
   "TestSub2" "6cb37694-e904-4614-a18a-80d9e13de623"})

(def subflairs
  {"6cb37694-e904-4614-a18a-80d9e13de623" [{:id "a1"
                                            :text "Flair A"
                                            :mods_only false
                                            :post_types ["LINK"]}
                                           {:id "b2"
                                            :text "Flair B"
                                            :mods_only false
                                            :post_types ["TEXT"]}]})

(def subrules
  {"9bcb7892-1441-4dd8-bf3b-023f339cba54" [{:id "a"
                                            :text "Rule a"}
                                           {:id "b"
                                            :text "Rule b"}]})

(defn create-index-query-result
  "Startup query with an authenticated user who moderates the subs."
  []
  (try
    (-> ::graphql-spec/query.index-query
        (spec/and #(map? (:current_user %)))
        (spec/and #(string? (get-in % [:current_user :name])))
        spec/gen
        (as-> $ (gen/fmap
                 #(assoc-in % [:site_configuration :enable_totp] false) $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.index-query $))
        util/kebab-case-keys
        (assoc :subs_moderated subs-moderated))
    (catch js/Error e
      (log/error {:exception e} "Error generating index query result"))))

(defn startup
  "Start up the app with the test data."
  []
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name) :seed seed} "Starting test")
  (test-utils/setup-regraph-subscribe-logging)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  (re-frame/reg-event-fx ::routes
    (fn [{:keys [db]} _]
      (routes/app-routes (:routes db))
      nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data (create-index-query-result)})))
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query variables callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            sid (get sids-by-name (:name variables))
            result (testing "expected query"
                     (cond
                       (= gql-query "get_all_subs") subinfo
                       (= gql-query "sub_post_flairs") {:sub_by_name
                                                        {:sid sid
                                                         :post_flairs
                                                         (get subflairs sid)}}
                       (= gql-query "sub_rules") {:sub_by_name
                                                  {:sid sid
                                                   :rules (get subrules sid)}}
                       :else (is false)))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result
                                                  :errors nil})]]})))

  (re-frame/dispatch [::db/initialize-db])
  (dispatch [::routes])
  (dispatch [::settings/load-index-query]))

(deftest test-create-sub-post-flair
  (rf-test/run-test-sync
   (startup)
   (testing "edit sub flairs"
     (let [create-sub-post-flair-form ::mod-forms/create-post-flair]
       (dispatch [::routes/show-mod-edit-post-flairs {:sub "TestSub2"}])
       (is (= "TestSub2" (:sub @(subscribe [:view/options]))))
       (is (= ["Flair A" "Flair B"]
              (map :text @(subscribe [::subs/post-flairs]))))

       (testing "can add a new flair"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables callback]}]]
             (let [gql-query (test-utils/query-name-for-test query)
                   text (:text variables)]
               (is (= gql-query "create_sub_post_flair"))
               {:fx [[:dispatch (update callback 1
                                        assoc :response
                                        {:data
                                         {:create_sub_post_flair
                                          {:id "c3"
                                           :text text
                                           :mods_only false
                                           :post_types ["POLL"]}}})]]})))
         (dispatch [::forms/edit create-sub-post-flair-form :flair "New flair"])
         (dispatch [::forms/action create-sub-post-flair-form :create])
         (is (= ["Flair A" "Flair B" "New flair"]
                (map :text @(subscribe [::subs/post-flairs])))))

       (testing "shows reordered flairs while waiting for server"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables]}]]
             (let [{:keys [sid ids]} variables
                   gql-query (test-utils/query-name-for-test query)]
               (is (= gql-query "reorder_sub_post_flairs"))
               (is (= (get sids-by-name "TestSub2") sid))
               (is (= 3 (count ids)))
               ;; do not dispatch query to see state before server response
               nil)))
         (dispatch [::moderation/reorder-post-flairs 0 2])
         (is (= ["Flair B" "New flair" "Flair A"]
                (map :text @(subscribe [::subs/post-flairs])))))

       (testing "replaces flairs on server reorder response"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables callback]}]]
             (let [{:keys [sid ids]} variables
                   gql-query (test-utils/query-name-for-test query)]
               (is (= gql-query "reorder_sub_post_flairs"))
               (is (= (get sids-by-name "TestSub2") sid))
               (is (= 3 (count ids)))
               {:fx [[:dispatch (update callback 1
                                        assoc :response
                                        {:data
                                         {:reorder_sub_post_flairs
                                          (get subflairs sid)}})]]})))
         (dispatch [::moderation/reorder-post-flairs 2 0])
         (is (= ["Flair A" "Flair B"]
                (map :text @(subscribe [::subs/post-flairs])))))))))

(deftest test-delete-sub-post-flair
  (rf-test/run-test-sync
   (startup)
   (testing "delete a sub flair"
     (dispatch [::routes/show-mod-edit-post-flairs {:sub "TestSub2"}])
     (is (= "TestSub2" (:sub @(subscribe [:view/options]))))
     (is (= ["Flair A" "Flair B"] (map :text @(subscribe [::subs/post-flairs]))))

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (is (= gql-query "delete_sub_post_flair"))
           nil)))

     (dispatch [::moderation/show-post-flair-options "b2"])
     (dispatch [::forms/action (mod-forms/edit-post-flair-form-id "b2") :delete])
     (is (= ["Flair A"] (map :text @(subscribe [::subs/post-flairs])))))))

(deftest test-update-sub-post-flair
  (rf-test/run-test-sync
   (startup)
   (let [b2-form (mod-forms/edit-post-flair-form-id "b2")
         mutation-vars (atom nil)]
     (testing "update a sub flair"
       (dispatch [::routes/show-mod-edit-post-flairs {:sub "TestSub2"}])
       (is (= "TestSub2" (:sub @(subscribe [:view/options]))))
       (is (= ["Flair A" "Flair B"] (map :text @(subscribe [::subs/post-flairs]))))

       (re-frame/reg-event-fx ::re-graph/mutate
         (fn [_ [_ {:keys [query variables]}]]
           (let [gql-query (test-utils/query-name-for-test query)]
             (is (= gql-query "update_sub_post_flair"))
             (reset! mutation-vars variables)
             nil)))

       (dispatch [::moderation/show-post-flair-options "b2"])
       (dispatch [::forms/edit b2-form :mods-only true])
       (dispatch [::forms/action b2-form :save])
       (is (= {:id "b2"
               :sid (-> subflairs keys first)
               :mods_only true
               :post_types [:TEXT]}
              @mutation-vars))))))

(deftest test-create-sub-rule
  (rf-test/run-test-sync
   (startup)
   (testing "edit sub rules"
     (let [create-sub-rule-form ::mod-forms/create-sub-rule]
       (dispatch [::routes/show-mod-edit-sub-rules {:sub "TestSub"}])
       (is (= "TestSub" (:sub @(subscribe [:view/options]))))
       (is (= ["Rule a" "Rule b"]
              (map :text @(subscribe [::subs/rules]))))

       (testing "can add a new rule"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables callback]}]]
             (let [gql-query (test-utils/query-name-for-test query)]
               (is (= gql-query "create_sub_rule"))
               {:fx [[:dispatch (update callback 1
                                        assoc :response
                                        {:data
                                         {:create_sub_rule
                                          {:id "c"
                                           :text (:text variables)}}})]]})))
         (dispatch [::forms/edit create-sub-rule-form :rule "New rule"])
         (dispatch [::forms/action create-sub-rule-form :create])
         (is (= ["Rule a" "Rule b" "New rule"]
                (map :text @(subscribe [::subs/rules])))))

       (testing "shows reordered rules while waiting for server"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables]}]]
             (let [{:keys [sid ids]} variables
                   gql-query (test-utils/query-name-for-test query)]
               (is (= gql-query "reorder_sub_rules"))
               (is (= (get sids-by-name "TestSub") sid))
               (is (= 3 (count ids)))
               ;; do not dispatch query to see state before server response
               nil)))
         (dispatch [::moderation/reorder-rules 2 0])
         (is (= ["New rule" "Rule a" "Rule b"]
                (map :text @(subscribe [::subs/rules])))))

       (testing "replaces rules on server reorder response"
         (re-frame/reg-event-fx ::re-graph/mutate
           (fn [_ [_ {:keys [query variables callback]}]]
             (let [{:keys [sid ids]} variables
                   gql-query (test-utils/query-name-for-test query)]
               (is (= gql-query "reorder_sub_rules"))
               (is (= (get sids-by-name "TestSub") sid))
               (is (= 3 (count ids)))
               {:fx [[:dispatch (update callback 1
                                        assoc :response
                                        {:data
                                         {:reorder_sub_rules
                                          (get subrules sid)}})]]})))
         (dispatch [::moderation/reorder-rules 2 0])
         (is (= ["Rule a" "Rule b"]
                (map :text @(subscribe [::subs/rules])))))))))

(deftest test-delete-sub-rule
  (rf-test/run-test-sync
   (startup)
   (testing "delete a sub rule"
     (dispatch [::routes/show-mod-edit-sub-rules {:sub "TestSub"}])
     (is (= "TestSub" (:sub @(subscribe [:view/options]))))
     (is (= ["a" "b"] (map :id @(subscribe [::subs/rules]))))

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (is (= gql-query "delete_sub_rule"))
           nil)))

     (dispatch [::moderation/delete-sub-rule "a"])
     (is (= ["Rule b"] (map :text @(subscribe [::subs/rules])))))))
