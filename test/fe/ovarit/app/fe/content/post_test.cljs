;; fe/content/post_test.cljs -- Testing single post events for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.post-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [clojure.spec.alpha :as spec]
            [clojure.test.check.generators :as gen]
            [day8.re-frame.test :as rf-test]
            [ovarit.app.fe.content.comment :as comment]
            [ovarit.app.fe.content.donate :as donate]
            [ovarit.app.fe.content.graphql-spec :as graphql-spec]
            [ovarit.app.fe.content.post :as post]
            [ovarit.app.fe.content.post.moderation :as post-mod]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.db :as db]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes :as routes]
            [ovarit.app.fe.test-utils :as test-utils]
            [ovarit.app.fe.ui.forms.core :as forms]
            [ovarit.app.fe.ui.forms.post :as post-forms]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.util :as util]
            [re-frame.core :as re-frame :refer [subscribe dispatch]]
            [re-graph.core :as re-graph]))

(spec/check-asserts true)

(def seed #_999 (rand-int 1000))
(def size 5)
(def failure-response {:errors
                       {:message "The HTTP call failed."
                        :extensions {:status 502}}})

(defn fix-comments
  "Set all the comment authors to active status.
  This gets rid of PROBATION which is a possible value
  for user status, but not for authors."
  [comments]
  (map #(assoc-in % [:author :status] "ACTIVE") comments))

(defn empty-comment-tree
  "Make an empty comment tree response."
  []
  {:tree_json (->> []
                   clj->js
                   js/JSON.stringify)
   :comments []})

(defn generate-post-result
  [editable?]
  (let [spec-editable (fn [spec]
                        (if editable?
                          (spec/and spec #(#{"POLL" "TEXT"}
                                           (get-in % [:post_by_pid :type])))
                          spec))]
    (try
      (-> ::graphql-spec/query.get-single-post
          (spec/and #(map? (get-in % [:post_by_pid :user_attributes])))
          (spec/and #(string? (get-in % [:post_by_pid :author :name])))
          (spec/and #(string? (get-in % [:post_by_pid :title])))
          spec-editable
          spec/gen
          (as-> $ (gen/fmap
                   #(-> %
                        (assoc-in [:post_by_pid :status] "ACTIVE")
                        (assoc-in [:post_by_pid :slug] "_")
                        (assoc-in [:post_by_pid :locked] false)
                        (assoc-in [:post_by_pid :is_archived] false)
                        (assoc-in [:post_by_pid :author :status] "ACTIVE")
                        (assoc-in [:post_by_pid :sub :restricted] false))
                   $))
          (gen/generate size seed)
          (as-> $ (spec/assert ::graphql-spec/query.get-single-post $)))
      (catch js/Error e
        (log/error {:exception e} "Error generating post result")))))

(defn generate-comment-tree-result [default-sort]
  {:post_by_pid {:default_sort default-sort
                 :comment_tree (empty-comment-tree)}})

(defn generate-index-query-result
  "Startup query with an authenticated user."
  [{:keys [moderates user]}]
  (try
    (-> ::graphql-spec/query.index-query
        (spec/and #(map? (:current_user %))
                  #(string? (get-in % [:current_user :name])))
        spec/gen
        (as-> $ (gen/fmap
                 #(cond-> %
                    true (assoc-in [:site_configuration :enable_totp] false)
                    moderates (assoc-in [:current_user :subs_moderated]
                                        [{:sub (assoc moderates
                                                      :open_report_count 0
                                                      :unread_modmail_count 0)
                                          :moderation_level "MODERATOR"}])
                    user (update :current_user merge user)) $))
        (gen/generate size seed)
        (as-> $ (spec/assert ::graphql-spec/query.index-query $))
        util/kebab-case-keys)
    (catch js/Error e
      (log/error {:exception e} "Error generating index query result"))))

(def content-block-result
  "Response for the content block query."
  {:current_user {:content_blocks []}})

(defn startup-data
  ([] (startup-data {}))
  ([{:keys [author? mod? editable-content?]}]
   (let [post-result (generate-post-result editable-content?)
         author (when author?
                  (-> post-result
                      (get-in [:post_by_pid :author])))
         sub (when mod?
               (-> post-result
                   (get-in [:post_by_pid :sub])
                   (select-keys [:sid :name])))
         default-sort (get-in post-result [:post_by_pid :default_sort])
         comment-tree-result (generate-comment-tree-result default-sort)
         index-query-result (generate-index-query-result {:moderates sub
                                                          :user author})]
     {:post-result post-result
      :comment-tree-result comment-tree-result
      :index-query-result index-query-result
      :content-block-result content-block-result})))

(defn register-test-graphql-queries
  "Define the queries to return the generated data."
  [{:keys [post-result comment-tree-result content-block-result]}]
  (test-utils/setup-regraph-subscribe-logging)
  (re-frame/reg-event-fx ::re-graph/query
    (fn [_ [_ {:keys [query callback]}]]
      (let [gql-query (test-utils/query-name-for-test query)
            result (testing "expected query"
                     (case gql-query
                       "get_single_post" post-result
                       "get_comment_tree" comment-tree-result
                       "content_blocks" content-block-result
                       (is (= gql-query false))))]
        {:fx [[:dispatch (update callback 1
                                 assoc :response {:data result
                                                  :errors nil})]]})))
  (re-frame/reg-event-fx ::re-graph/subscribe
    (fn [_ _] {}))
  (re-frame/reg-event-fx ::re-graph/unsubscribe
    (fn [_ _] {})))

(defn startup
  "Start up the app with the test data."
  [{:keys [index-query-result] :as test-data}]
  (test-utils/init-logging ::test)
  (log/info {:test (test-utils/test-name) :seed seed} "Starting test")
  (register-test-graphql-queries test-data)
  ;; Prevent the app from clobbering the test tab title.
  (re-frame/reg-fx ::page-title/set (fn [_] nil))
  ;; Stop some things that spam the logs.
  (re-frame/reg-event-fx ::comment/start-scroll-watcher (constantly nil))
  (re-frame/reg-event-fx ::comment/start-resize-watcher (constantly nil))
  (re-frame/reg-event-fx ::donate/subscribe-funding-progress (constantly nil))
  (re-frame/reg-event-fx ::routes
    (fn [{:keys [db]} _]
      (routes/app-routes (:routes db))
      nil))
  (re-frame/reg-cofx ::settings/index-query-results
    (fn [cofx _]
      (assoc cofx :query-results {:data index-query-result})))
  (dispatch [::db/initialize-db])
  (dispatch [::routes])
  (dispatch [::settings/load-index-query]))

(deftest test-save-post
  (rf-test/run-test-sync
   (let [{:keys [post-result] :as test-data} (startup-data)
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "save_post"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:save_post (:save variables)}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false))))))

     (startup test-data)
     (testing "a post"
       (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                             :pid (:pid post)
                                             :slug "_"}])
       (let [saved? @(subscribe [::post/single-post-saved?])]
         (testing "can be saved or unsaved"
           (dispatch [::post/toggle-post-saved])
           (is (= (not saved?) (:save @mutation-vars)))
           (is (= (not saved?) @(subscribe [::post/single-post-saved?]))))
         (testing "has the same save state after an error"
           (reset! fail true)
           (test-utils/mute-error-logging true)
           (dispatch [::post/toggle-post-saved])
           (test-utils/mute-error-logging false)
           (is (= saved? (:save @mutation-vars)))
           (is (= (not saved?) @(subscribe [::post/single-post-saved?])))))))))

(deftest test-change-post-flair
  (rf-test/run-test-sync
   (let [{:keys [post-result] :as test-data} (startup-data)
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         num (atom 0)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "set_post_flair"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:set_post_flair
                                          {:text (str "new-flair-" @num)}}
                                   :errors nil})]
                   (swap! num inc)
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             "remove_post_flair"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:remove_post_flair
                                          (:pid variables)}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (testing "a post"
       (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                             :pid (:pid post)
                                             :slug "_"}])
       (testing "can be flaired"
         (dispatch [::post/edit-single-post-flair])
         (dispatch [::forms/action ::post-forms/select-flair :change
                    "flair-id"])
         (is (= {:pid (:pid post) :flair_id "flair-id"} @mutation-vars))
         (is (= "new-flair-0" (:flair @(subscribe [::post/single-post]))))
         (is (not @(subscribe [::post/show-single-post-flair-modal?]))))
       (testing "flair is unchanged on error when"
         (reset! fail true)
         (dispatch [::post/edit-single-post-flair])
         (testing "setting flair"
           (test-utils/mute-error-logging true)
           (dispatch [::forms/action ::post-forms/select-flair
                      :change "flair-id"])
           (test-utils/mute-error-logging false)
           (is (= {:pid (:pid post) :flair_id "flair-id"} @mutation-vars))
           (is (= "new-flair-0" (:flair @(subscribe [::post/single-post]))))
           (is @(subscribe [::post/show-single-post-flair-modal?]))
           (dispatch [::post/edit-single-post-flair]))
         (testing "removing flair"
           (dispatch [::post/edit-single-post-flair])
           (test-utils/mute-error-logging true)
           (dispatch [::forms/action ::post-forms/select-flair :remove])
           (test-utils/mute-error-logging false)
           (is (= {:pid (:pid post)} @mutation-vars))
           (is (= "new-flair-0" (:flair @(subscribe [::post/single-post])))))
         (dispatch [::post/edit-single-post-flair])
         (reset! fail false))
       (testing "flair can be removed"
         (dispatch [::post/edit-single-post-flair])
         (dispatch [::forms/action ::post-forms/select-flair :remove])
         (is (= {:pid (:pid post)} @mutation-vars))
         (is (nil? (:flair @(subscribe [::post/single-post]))))
         (is (not @(subscribe [::post/show-single-post-flair-modal?]))))))))

(deftest test-edit-post-content
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true
                                      :editable-content? true})
                       (assoc-in [:post-result :post_by_pid :content]
                                 "post content"))
         {:keys [post-result]} test-data
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "update_post_content"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:update_post_content true}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                           :pid (:pid post)
                                           :slug "_"}])
     (is @(subscribe [::post/can-edit-post-content?]))
     (testing "author can edit post content"
       (dispatch [::post/toggle-edit-post-content])
       (dispatch [::forms/edit ::post-forms/edit-content :content "New content"])
       (dispatch [::forms/action ::post-forms/edit-content :send])
       (is (= "New content" @(subscribe [::post/post-content])))
       (is (false? @(subscribe [::post/show-post-content-editor?]))))
     (testing "post content is unchanged on error"
       (reset! fail true)
       (dispatch [::post/toggle-edit-post-content])
       (dispatch [::forms/edit ::post-forms/edit-content :content "different"])
       (test-utils/mute-error-logging true)
       (dispatch [::forms/action ::post-forms/edit-content :send])
       (test-utils/mute-error-logging false)
       (is (= "New content" @(subscribe [::post/post-content])))
       (is @(subscribe [::post/show-post-content-editor?]))))))

(deftest test-edit-post-content-mod
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true
                                      :mod? true
                                      :editable-content? true})
                       (assoc-in [:post-result :post_by_pid :content]
                                 "post content"))
         {:keys [post-result]} test-data
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "update_post_content"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:update_post_content true}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                           :pid (:pid post)
                                           :slug "_"}])
     (is @(subscribe [::post/can-edit-post-content?]))
     (testing "mod can edit post content"
       (is @(subscribe [::post/can-edit-post-content?]))
       (dispatch [::post/toggle-edit-post-content])
       (dispatch [::forms/edit ::post-forms/edit-content :content "New content"])
       (dispatch [::forms/action ::post-forms/edit-content :send])
       (is (= {:pid (:pid post)
               :content "New content"} @mutation-vars))
       (is (= "New content" @(subscribe [::post/post-content])))
       (is (false? @(subscribe [::post/show-post-content-editor?])))
       (testing "and see the post content history updated"
         (dispatch [::post/post-content-see-older-version])
         (is (= (:content post) @(subscribe [::post/post-content])))
         (dispatch [::post/post-content-see-newer-version])
         (is (= "New content" @(subscribe [::post/post-content]))))))))

(deftest test-edit-post-title
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true})
                       (assoc-in [:post-result :post_by_pid :posted]
                                 (str (.getTime (js/Date.)))))
         {:keys [post-result]} test-data
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "update_post_title"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:update_post_title true}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                           :pid (:pid post)
                                           :slug "_"}])
     (is @(subscribe [::post/can-edit-post-title?]))
     (testing "author can edit post title"
       (dispatch [::post/toggle-edit-post-title])
       (dispatch [::forms/edit ::post-forms/edit-title :title "New title"])
       (dispatch [::forms/action ::post-forms/edit-title :send])
       (is (= {:pid (:pid post) :title "New title" :reason ""} @mutation-vars))
       (is (= "New title" @(subscribe [::post/post-title])))
       (is (false? @(subscribe [::post/show-post-title-editor?]))))
     (testing "post title is unchanged on error"
       (reset! fail true)
       (dispatch [::post/toggle-edit-post-title])
       (dispatch [::forms/edit ::post-forms/edit-title :title "different"])
       (test-utils/mute-error-logging true)
       (dispatch [::forms/action ::post-forms/edit-title :send])
       (test-utils/mute-error-logging false)
       (is (= {:pid (:pid post) :title "different" :reason ""} @mutation-vars))
       (is (= "New title" @(subscribe [::post/post-title])))
       (is @(subscribe [::post/show-post-title-editor?]))))))

(deftest test-edit-post-title-mod
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true
                                      :mod? true})
                       (assoc-in [:post-result :post_by_pid :posted]
                                 (str (.getTime (js/Date.)))))
         {:keys [post-result]} test-data
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "update_post_title"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:update_post_title true}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                           :pid (:pid post)
                                           :slug "_"}])
     (is @(subscribe [::post/can-edit-post-title?]))
     (testing "author can edit post title"
       (dispatch [::post/toggle-edit-post-title])
       (dispatch [::forms/edit ::post-forms/edit-title :title "New title"])
       (dispatch [::forms/action ::post-forms/edit-title :send])
       (is (= {:pid (:pid post) :title "New title" :reason ""} @mutation-vars))
       (is (= "New title" @(subscribe [::post/post-title])))
       (is (false? @(subscribe [::post/show-post-title-editor?])))
       (testing "and see the post title history updated"
         (dispatch [::post/post-title-see-older-version])
         (is (= (:title post) @(subscribe [::post/post-title])))
         (dispatch [::post/post-title-see-newer-version])
         (is (= "New title" @(subscribe [::post/post-title]))))))))

(deftest test-set-post-nsfw
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true})
                       (assoc-in [:post-result :post_by_pid :sub :nsfw] false))
         {:keys [post-result]} test-data
         post (:post_by_pid post-result)
         mutation-vars (atom nil)
         fail (atom false)]
     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "set_post_nsfw"
             (do (reset! mutation-vars variables)
                 (let [response (if @fail
                                  failure-response
                                  {:data {:set_post_nsfw (:nsfw variables)}
                                   :errors nil})]
                   {:fx [[:dispatch (update callback 1
                                            assoc :response response)]]}))

             (is (= gql-query false))))))

     (startup test-data)
     (testing "a post"
       (dispatch [::routes/show-single-post {:sub (get-in post [:sub :name])
                                             :pid (:pid post)
                                             :slug "_"}])
       (let [nsfw? (:nsfw @(subscribe [::post/single-post]))]
         (testing "can have its nsfw toggled"
           (is @(subscribe [::post/can-tag-post-nsfw?]))
           (dispatch [::post/toggle-post-nsfw])
           (is (= {:pid (:pid post) :nsfw (not nsfw?)} @mutation-vars))
           (is (= (not nsfw?) (:nsfw @(subscribe [::post/single-post])))))
         (testing "has the same nsfw state after an error"
           (reset! fail true)
           (test-utils/mute-error-logging true)
           (dispatch [::post/toggle-post-nsfw])
           (test-utils/mute-error-logging false)
           (is (= {:pid (:pid post) :nsfw nsfw?} @mutation-vars))
           (is (= (not nsfw?) (:nsfw @(subscribe [::post/single-post]))))))))))

(deftest test-distinguish-post-mod
  (rf-test/run-test-sync
   (let [test-data (-> (startup-data {:author? true
                                      :mod? true})
                       (assoc-in [:index-query-result :current-user :attributes
                                  :can-admin] false)
                       (assoc-in [:post-result :post_by_pid :distinguish] nil))
         {:keys [post-result]} test-data
         {:keys [pid sub]} (:post_by_pid post-result)
         {sid :sid sub-name :name} sub
         mutation-vars (atom nil)
         fail (atom false)]

     (re-frame/reg-event-fx ::re-graph/mutate
       (fn [_ [_ {:keys [query variables callback]}]]
         (let [gql-query (test-utils/query-name-for-test query)]
           (log/debug {:query gql-query :variables variables} "mutation")
           (case gql-query
             "update_post_viewed" nil

             "distinguish_post"
             (let [distinguish (when (:distinguish variables)
                                 (name (:distinguish variables)))]
               (reset! mutation-vars variables)
               (let [response (if @fail
                                failure-response
                                {:data {:distinguish_post distinguish}
                                 :errors nil})]
                 {:fx [[:dispatch (update callback 1
                                          assoc :response response)]]}))

             (is (= gql-query false) "unexpected mutation")))))

     (startup test-data)
     (testing "a post"
       (dispatch [::routes/show-single-post {:sub sub-name :pid pid :slug "_"}])
       (testing "can be distinguished by a mod author"
         (is @(subscribe [::post/can-distinguish-post-author-oneshot?]))
         (dispatch [::post-mod/edit-post-author-distinguish])
         (is (= {:pid pid :sid sid :distinguish :MOD} @mutation-vars))
         (is (= :MOD (:distinguish @(subscribe [::post/single-post])))))
       (testing "has the same distinguish state after an error"
         (reset! fail true)
         (test-utils/mute-error-logging true)
         (dispatch [::post-mod/edit-post-author-distinguish])
         (test-utils/mute-error-logging false)
         (is (= {:pid pid :sid sid :distinguish nil} @mutation-vars))
         (is (= :MOD (:distinguish @(subscribe [::post/single-post]))))
         (reset! fail false))
       (testing "can be un-distinguished by a mod author"
         (dispatch [::post-mod/edit-post-author-distinguish])
         (is (= {:pid pid :sid sid :distinguish nil} @mutation-vars))
         (is (nil? (:distinguish @(subscribe [::post/single-post])))))))))
