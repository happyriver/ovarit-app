# Moderator mail

Moderator mail is a shared messaging system for users to communicate with circle moderators and 
for circle moderators to communicate with each other and to respond to requests from their users.

Moderator mail is automatically enabled for every circle.

## Accessing moderator mail

To access moderator mail, first select your shield icon from the top
menu to see the moderator dashboard.  For each circle you moderate
there will be a box of statistics and you can click on either of the
modmail links in that box to go to modmail for that circle.  To see
all the messages for all the circles you moderate, use the **All Mod
Mail** button on the sidebar (which is in the hamburger menu on
mobile).

The number on your shield item is the sum total of your open reports
and your unread modmails for all the circles you moderate.

## Using Moderator mail

### Types of messages and the sidebar

The top thing on the sidebar is the **Compose a Message** button.  This
can be used for messaging one of your users or for starting a discussion with 
the other moderators of a circle.

Below the compose button, the sidebar has several buttons to let you
look at different categories of conversations with users.  Let's call
these "mailboxes".  When you use one of those buttons to bring up a
list of conversations, you will see the most recent message in each
conversation.

- **All** will show you all unarchived conversations involving users.
- **New** will show you all unarchived conversations from users to which
  none of your fellow mods have replied yet.
- **In progress** will show you all unarchived conversations from users
  that have replies, in addition to conversations from mods to users with
  their replies, if any.
- **Archived** will show you conversations which have been archived by
  you or one of your fellow mods.
- **Mod discussions** will show you conversations between you and your fellow
  mods.

### Looking at conversations

When you choose a mailbox, and see the list of conversations, each
message in the list is the most recent one from that conversation.  If
a message has an orange left border, that means it is unread.  You can
mark it read using the checkmark on the right.  Next to the checkmark
is the filebox icon, which will move the conversation to the
**Archived** mailbox, unless you are looking at it in the **Archived**
mailbox, in which case it will make it un-archived.  Past the filebox
is a curvy arrow, which will show you all the messages in a single
conversation.

When you click the curvy arrow, you will see the subject of the
conversation and the original message that started it at the top of
the page, followed by replies in the order of oldest to newest.  If
there are more than 25 replies to a message, only the newest replies
will be shown and you will get a link to load older ones below the
original message.

At the bottom of the single-conversation page is a reply box where you
can write a reply and choose whether or not it is a reply that the
user will be able to see, or a note for your fellow mods that the user
will not be able to see.

## Things that aren't done yet

### Known issues
- Notification counts (those numbers on the envelope and shield icon) for mods
- Push notification count updates for users
- Push updates for mods (if a user sends you something you won't see
  it until you reload the page or change pages)
  
### Bugs

- This is brand new so I'm sure we have lots!  Please report them on
  /o/Bugs or on Discord.
