# Place a file like this named ".env" in the current directory,
# or set these variables in the environment.

# For development; do not set this in production.
DEBUG=true

# Optional.  The defaults are localhost and 8888.
SERVER_HOST=localhost
SERVER_PORT=8888

SITE_SUB_PREFIX=o

# Optional; Links to place at the bottom of the page, in JSON format.
# SITE_FOOTER_LINKS='{"ToS": "/wiki/tos", "Privacy": "/wiki/privacy", "Canary": "/wiki/canary", "Donate": "/donate", "Bugs": "/wiki/bugs"}'

# Optional; a file containing motto strings to show under the logo.
# SITE_MOTTOS=/path/to/mottos.txt

# Optional; will be shown at the bottom of every page.
SITE_LAST_WORD="A little bit of wisdom to show at the bottom of every page."

# This is optional; if not supplied the default Throat logo will be used.
# SITE_LOGO=/path/to/logo.svg

# Optional image to supply in OpenGraph metadata for pages that
# don't have a thumbnail.
# SITE_METADATA_IMAGE=https://example.com/static/logo.jpg
# SITE_METADATA_IMAGE_WIDTH=400
# SITE_METADATA_IMAGE_HEIGHT=300

# Enforces 2FA for administrators. This will remove _most_ privileges
# from administrators until they introduce a timed one-time password.
SITE_ENABLE_TOTP=false

DATABASE_HOST=localhost
DATABASE_PORT=5432
DATABASE_NAME=throat
DATABASE_USER=throat
DATABASE_PASSWORD=pass1234

TEST_DATABASE_HOST=localhost
TEST_DATABASE_PORT=5432
TEST_DATABASE_NAME=throat_test
TEST_DATABASE_USER=throat
TEST_DATABASE_PASSWORD=pass1234

# URL of the Redis server used by the Throat server.
REDIS_URL=redis://127.0.0.1:6379

# URL of the Redis database used by the tests.
TEST_REDIS_URL=redis://127.0.0.1:6379/1

# Thread pool size. Set the size of the fixed pool of threads
# that works on SQL queries.  This size should be smaller than
# the number of database connections available, by some margin,
# since other thread pools may also make database queries, for
# example those running background tasks.
SERVER_QUERY_POOL_SIZE=16

# The clojure.async pool is used to respond to subscription requests.
# Its default size is 8 threads.  Changing its size in production can
# be done on the Java command line like this:
# -Dclojure.core.async.pool-size=16

# The number of simultaneous requests to allow the thread pool to
# process.  How this should relate to the number of threads is a
# really good question.
SERVER_PROCESS_REQUESTS=16

# The number of requests allowed to wait for processing.  If new
# requests arrive while this many are waiting they will receive a 503
# response.
SERVER_QUEUE_REQUESTS=640

# If a request has waited for a thread for longer than this amount of
# time (in milliseconds), it will get a 503 response.
SERVER_REQUEST_TIMEOUT=2500

# Prefix path for the front end asset files.  Anything in this
# directory will be served if requested.  When building index.html,
# the server will look in the 'fe' subdirectory at this location,
# choose the newest subdirectory found there, and use that as the
# prefix path for the javascript and css files.
ASSET_PATH=/path/to/front/end/assets

# Number of trusted proxies which set the X-Forwarded-For header
# ahead of the application.  If you run the application behind
# nginx or a load balancer, this should be set to 1 or more.
SITE_TRUSTED_PROXY_COUNT=1

# Domain name at which the app is being served.  This must be
# configured for websocket support (notifications) on
# Safari and Apple WebKit.
# SITE_SERVER_NAME=example.com

# Uncomment to disable rate limiting by IP (useful for development,
# but you should leave it on for production).
# RATELIMIT_ENABLE=False

# This should be the same secret key used by the Python Throat server.
# Use something long and random in production.
SECRET_KEY="PUT SOMETHING HERE"

# A time in seconds, before session cookies expire.  This should
# be set to the same value as the Python server (Flask default is
# 31 days.
PERMANENT_SESSION_LIFETIME=2678400

# Extra headers from the request to include in the logs.
# Should be a list, in JSON format.
LOG_REQUEST_HEADERS='["user-agent"]'

# The IP and time of every request are entered in the database.  These
# rows are periodically deleted and summarized into the daily_uniques
# table.
#
# How often this is done, in ms:
VISITOR_COUNT_UPDATE_TASK_INTERVAL=60000
# How many rows to delete per SQL query:
VISITOR_COUNT_UPDATE_TASK_BATCH_SIZE=10000
# If the batch size is less than the number of rows that need
# processing, the query will be repeated immediately, until up to this
# many rows have been processed:
VISITOR_COUNT_UPDATE_TASK_BATCH_LIMIT=100000

# Periodic removal of old comment views.  Old comment views are removed
# from posts older than site.archive_post_after.
REMOVE_OLD_COMMENT_VIEWS_TASK_INTERVAL=60000
REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_SIZE=10000
REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_LIMIT=100000

# Asynchronous removal of votes from users flagged by admins.
ADMIN_REMOVE_VOTES_TASK_INTERVAL=100
ADMIN_REMOVE_VOTES_TASK_BATCH_SIZE=120
ADMIN_REMOVE_VOTES_TASK_STATUS_TIMEOUT=180

# Preset amounts for /donate, in dollars.
SITE_DONATE_PRESETS='[5, 10, 20]'
SITE_DONATE_MINIMUM=1

# Periodically look for customer update logs entered into the
# database by the Python server, and pass them along to the
# ovarit-subscription server.
SUBSCRIPTION_UPDATE_TASK_INTERVAL=60000

# Address of the ovarit-subscription server.  Include the
# protocol but no trailing slash.
OVARIT_SUBSCRIPTION_HOST="http://ovarit-subs:5003"

# Query the /progress endpoint of the ovarit-subcription server at
# intervals for distribution to client browsers via websocket.
FUNDING_PROGRESS_UPDATE_TASK_INTERVAL=300000

# Address of the ovarit image service. Include the
# protocol but no trailing slash.  This is used by the
# graphql server to contact the image service.
IMAGE_SERVICE_URL="http://localhost:8898"

# URL or relative path at which thumbnails are served. Omit the
# trailing slash.
STORAGE_THUMBNAILS_URL="http://localhost:5000/files"

# Configuration for usefathom.com
# FATHOM_DATA_SITE=
# FATHOM_GOAL=

# Configuration for a daily restart.
# If set, at this hour in the server's timezone, on the day following
# startup, the server will answer /health with 503s for 10 minutes.
# SERVER_STOP_HOUR=7

# List of supported languages, in JSON format.  Defaults to ["en"].
APP_LANGUAGES='["en"]'

################ Variables needed by --image-service option ################

# If the --image-service option is enabled, bind its server to this
# address. If not supplied, use the defaults of localhost and 8898.
IMAGE_SERVER_HOST=localhost
IMAGE_SERVER_PORT=8898

# One of: LOCAL, us-west, us-west-2, us-west-oregon, eu-west,
# ap-southeast, ap-northeast, etc.
STORAGE_PROVIDER=LOCAL

# AWS credentials if provider is S3.  These are optional.
# If not supplied, DefaultAWSCredentialsProviderChain will
# be used to get credentials.
# AWS_ACCESS_KEY_ID=
# AWS_SECRET_ACCESS_KEY=

# Bucket for durable, public items, if using S3.
# Credentials should allow read and write access.
#
# If using local storage, directory where served objects are stored.
# The app must have read and write access to this directory.
# IMAGE_CONTAINER_PUBLIC should be served at STORAGE_THUMBNAILS_URL
# (in development this is done by the Python server).
IMAGE_CONTAINER_PUBLIC="./files/public"

# Bucket for images fetched for thumbnailing, metadata stripping and
# other work in progress, if using S3.  This can be set up to delete
# older objects automatically.  This bucket should not be public.
# Credentials should allow read and write access.
#
# If using local storage, directory where temporary objects are stored.
# The app must have read and write access to this directory.
IMAGE_CONTAINER_TEMPORARY="./files/tmp"
