;; gql/schema.clj -- GraphQL schema for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.schema
  "Provide a compiled GraphQL schema with linked resolvers."
  (:require
   [cambium.core :as log]
   [camel-snake-kebab.core :as csk]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.set :as set]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [com.walmartlabs.lacinia :as lacinia]
   [com.walmartlabs.lacinia.executor :as executor]
   [com.walmartlabs.lacinia.parser :as parser]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [com.walmartlabs.lacinia.schema :as schema]
   [com.walmartlabs.lacinia.select-utils :as select-utils]
   [com.walmartlabs.lacinia.selection :as selection]
   [com.walmartlabs.lacinia.util :as util]
   [ovarit.app.gql.elements.comment.resolve :as comment]
   [ovarit.app.gql.elements.config.resolve :as config]
   [ovarit.app.gql.elements.link.resolve :as link]
   [ovarit.app.gql.elements.message.resolve :as message]
   [ovarit.app.gql.elements.notification-counts.resolve :as notification-counts]
   [ovarit.app.gql.elements.post.resolve :as post]
   [ovarit.app.gql.elements.site.resolve :as site]
   [ovarit.app.gql.elements.sub.resolve :as sub]
   [ovarit.app.gql.elements.user.resolve :as user]
   [ovarit.app.gql.elements.vote.resolve :as vote]
   [ovarit.app.gql.protocols.current-user :refer [is-admin? can-admin?
                                                  get-uid get-name
                                                  moderation-level
                                                  subs-moderated]]
   [ovarit.app.gql.protocols.ratelimit :as ratelimit]
   [ovarit.app.gql.protocols.schema :as protocol :refer [find-in-auth-info]]
   [ovarit.app.gql.resolver-helpers.core :as resolver]
   [ovarit.app.gql.resolver-helpers.stream :as stream]
   [ovarit.app.gql.system.server.flask-compat :as compat]
   [ovarit.app.gql.util :refer [expound-str
                                kebab-case-keys
                                ->snake-case-qualified]]
   [promesa.core :as prom]))

(def resolve-error-403
  (resolve/with-error nil {:message "Not authorized" :status 403}))

(def resolve-list-error-403
  (resolve/with-error [] {:message "Not authorized" :status 403}))

(def resolve-error-500
  (resolve/with-error nil {:message "Internal server error" :status 500}))

;;; Rate limiting at the resolver level

(def resolver-limits
  "Rate limiting keys to use for resolver functions.
  If a resolver function is not listed here, no rate limit will be applied
  (other than those in server.clj)."
  {:mutation/create-modmail-message :send-message
   :mutation/create-modmail-reply   :send-message
   :mutation/create-message-to-mods :send-message
   :mutation/create-private-message :send-message
   :mutation/create-comment         :posting
   :mutation/create-upload-url      :posting
   :mutation/update-comment-content :posting
   :mutation/update-post-title      :posting
   :mutation/update-post-content    :posting})
;; TODO rate limit for request url metadata

;;; Directives

(def directive-map
  "Map of unqualified keys in schema to qualified implementation keys."
  {:info-setter
   {:arguments                  ::arguments
    :containing_field_reference ::containing-field-reference
    :fetch_message              ::message/fetch-message
    :fetch_message_thread       ::message/fetch-message-thread
    :resolution                 ::resolution}

   :role-setter
   {:mod_by_sid                            ::mod-by-sid
    :mod_by_sids_or_empty                  ::mod-by-sids-or-empty
    :self_by_name                          ::self-by-name
    :self_by_uid                           ::self-by-uid
    :self_if_authed                        ::self-if-authed
    :sender                                ::message/sender
    :sender_and_receiver_by_message        ::message/sender-and-receiver
    :sender_and_receiver_by_message_thread ::message/thread-sender-and-receiver
    :report_author                         ::post/report-author}

   :redaction
   {:content_of_deleted_comment ::comment/deleted-content
    :content_of_deleted_post    ::post/deleted-content
    :deletion_reason            ::post/deletion-reason
    :name_of_deleted_user       ::user/name-of-deleted-user
    :vote_breakdown             ::post/vote-breakdown}

   :check-argument
   {:nat_int                  ::nat-int?
    :pos_int                  ::pos-int?
    :pos_int_nullable         ::pos-int-nullable?
    :non_empty_string         ::non-empty-string?
    :timestamp                ::timestamp?
    :site_username_ban_string ::site/username-ban-string?
    :string_max_length        ::string-max-length?
    :string_length_minmax     ::string-length-minmax?}})

;;; Resolver Interceptors

(defn- rate-limited?
  "Determine whether a schema entry is rate-limited."
  [ratelimiter schema-key remote-addr]
  (let [rate-limit-type (get resolver-limits schema-key)]
    (and rate-limit-type
         (ratelimit/enabled? ratelimiter)
         (not (ratelimit/check! ratelimiter rate-limit-type remote-addr)))))

(def rate-limit-interceptor
  "Apply rate limiting to a schema resolver."
  {:name :rate-limit
   :enter
   (fn [{:keys [request ratelimiter schema-key] :as context} _ _]
     (let [{:keys [real-remote-addr]} request]
       (cond-> context
         (rate-limited? ratelimiter schema-key real-remote-addr)
         (assoc ::error-resolution
                (resolve/with-error nil {:message "Too many queries"
                                         :status 429})))))})

(defmethod protocol/check-argument ::nat-int?
  [_ _ arg]
  (nat-int? arg))

(defmethod protocol/check-argument ::pos-int?
  [_ _ arg]
  (pos-int? arg))

(defmethod protocol/check-argument ::pos-int-nullable?
  [_ _ arg]
  (or (nil? arg) (pos-int? arg)))

(defmethod protocol/check-argument ::non-empty-string?
  [_ _ arg]
  (not= arg ""))

(defmethod protocol/check-argument ::timestamp?
  [_ _ arg]
  (try
    (pos-int? (Long/parseLong arg))
    (catch java.lang.NumberFormatException _
      false)))

(defmethod protocol/check-argument ::string-max-length?
  [{:keys [args]} _ arg]
  (<= (count arg) (:chars args)))

(defmethod protocol/check-argument ::string-length-minmax?
  [{:keys [args]} _ arg]
  (<= (:min_chars args) (count arg) (:max_chars args)))

(defn- valid-argument?
  "Determine if the argument specs apply.
  Allow everything to be nil, because that should be enforced with
  the non-null type modifier in the schema."
  [context arg arg-directives]
  (every? #(or (nil? arg) (protocol/check-argument % context arg))
          arg-directives))

(defn argument-check-interceptor
  "Ensure resolver arguments meet their specs."
  [directives]
  (let [arg-checks (->> directives
                        :argument
                        (s/transform [s/MAP-VALS] :check-argument))]
    (when (some #(-> % val seq) arg-checks)
      {:name :argument-check
       :enter
       (fn [context args _]
         (let [valid-arg? #(valid-argument? context
                                            ((csk/->kebab-case %) args)
                                            (% arg-checks))
               failed-checks (->> (keys arg-checks)
                                  (map #(when-not (valid-arg? %) %))
                                  (remove nil?))]
           (log/trace {:arg-checks arg-checks
                       :check-results (doall failed-checks)}
                      "argument-check-interceptor")
           (if (empty? failed-checks)
             context
             (do
               (log/trace {:failed-checks failed-checks
                           :args args
                           :arg-directives arg-checks}
                          "argument failed check")
               (assoc context ::error-resolution
                      (resolve/with-error nil
                        {:message (str "Invalid argument: "
                                       (->> failed-checks
                                            (map name)
                                            (str/join " ")))
                         :status 400}))))))})))

(defn argument-modification-interceptor
  "Apply directives that modify the resolver arguments."
  [directives]
  (let [args-to-trim (->> directives
                          :argument
                          (map (fn [[arg {:keys [trim-string?]}]]
                                 (when trim-string?
                                   arg)))
                          (remove nil?))
        trim-arg (fn [args kw]
                   (update args kw #(when % (str/trim %))))]
    (when (seq args-to-trim)
      {:name :argument-modification
       :enter
       (fn [context args _]
         (log/trace {:schema-key (:schema-key context)
                     :args-to-trim args-to-trim
                     :args-before args
                     :args-after (reduce trim-arg args args-to-trim)})
         (assoc context ::updated-args
                (reduce trim-arg args args-to-trim)))})))

(def user-role-interceptor
  "Figure out which roles are active, and add them to the context."
  {:name :user-role
   :enter
   (fn [{:keys [current-user] :as context} _ _]
     (update context :roles set/union
             (->> [(when (is-admin? current-user) :IS_ADMIN)
                   (when (can-admin? current-user) :CAN_ADMIN)
                   (when (get-uid current-user) :USER)
                   :EVERYONE]
                  (remove nil?)
                  set)))})

(defn- mod-roles
  "Get the moderation roles if any for the current-user in a sub."
  [current-user sid]
  (get {:OWNER     #{:OWNER_MOD :LEAD_MOD :MOD}
        :MODERATOR #{:LEAD_MOD :MOD}
        :JANITOR   #{:MOD}}
       (moderation-level current-user sid)))

(def all-mod-roles #{:OWNER_MOD :LEAD_MOD :MOD})

(defn- authorized?
  "Given user roles and field roles, determine if the field is visible."
  [required-roles roles]
  (-> (set/intersection required-roles roles) seq some?))

(defn- list-type?
  "Determine if the selected field represents a list."
  [context]
  (= :list (some-> (executor/selection context)
                   selection/field
                   selection/kind
                   selection/kind-type)))

(defn- qualified-field-name
  "Return the qualified name of a lacinia FieldSelection."
  [sel]
  (-> sel
      selection/field
      selection/qualified-name))

(defmethod protocol/update-auth-info ::containing-field-reference
  [_ context _ resolved]
  (assoc-in context [:auth-info :reference] resolved))

(defmethod protocol/update-auth-info ::arguments
  [_ context args _]
  (assoc-in context [:auth-info :arguments] args))

(defmethod protocol/update-auth-info ::resolution
  [_ context _ _]
  (assoc-in context [:auth-info :grant-all] {}))

(defn- assoc-auth-info [context based-on args resolved]
  (if based-on
    (protocol/update-auth-info based-on context args resolved)
    context))

;; Set the moderator roles based on the sid in the auth info.
(defmethod protocol/set-roles ::mod-by-sid
  [_ {:keys [auth-info current-user] :as context}]
  (if-not (get-uid current-user)
    context
    (let [sid (find-in-auth-info auth-info :sid)]
      (update context :roles set/union (mod-roles current-user sid)))))

;; Set the :MOD role based on the sids in auth-info.  If there are no
;; sids in the list, give the :MOD role to users who moderate any sub.
(defmethod protocol/set-roles ::mod-by-sids-or-empty
  [_ {:keys [auth-info current-user] :as context}]
  (let [sids (find-in-auth-info auth-info :sids)]
    (if (or (nil? (get-uid current-user))
            (empty? (subs-moderated current-user)))
      context
      (if (empty? sids)
        (update context :roles set/union #{:MOD})
        (let [sid-set (set sids)
              sids-modded-set (set (map :sid (subs-moderated current-user)))]
          (if (= sid-set (set/intersection sid-set sids-modded-set))
            (update context :roles set/union #{:MOD})
            context))))))

;; Set the :SELF role based on whether the name matches the current user's name.
(defmethod protocol/set-roles ::self-by-name
  [_ {:keys [auth-info current-user] :as context}]
  (let [name (find-in-auth-info auth-info :name)]
    (update context :roles set/union
            (when (and name (= name (get-name current-user))) #{:SELF}))))

;; Set the :SELF role based on whether the uid matches the current user's uid.
(defmethod protocol/set-roles ::self-by-uid
  [_ {:keys [auth-info current-user] :as context}]
  (let [uid (find-in-auth-info auth-info :uid)]
    (update context :roles set/union
            (when (and uid (= uid (get-uid current-user))) #{:SELF}))))

;; Set the :SELF role if the user is authenticated.
(defmethod protocol/set-roles ::self-if-authed
  [_ {:keys [current-user] :as context}]
  (update context :roles set/union
          (when (get-uid current-user) #{:SELF})))

(defn- run-role-setters
  "Apply a sequence of role setters to the context.
  Use each one's :based_on argument to set up :auth-info in the context
  (which accumulates changes from all role setters), then run it."
  [context role-setters args resolved]
  (reduce (fn [ctx {:keys [based_on] :as role-setter}]
            (->> (assoc-auth-info ctx based_on args resolved)
                 (protocol/set-roles role-setter)))
          context role-setters))

(defn- run-early-role-setters
  "Run the methods for the set_role directives on the field.
  If a directive has :resolution for its based_on argument,
  instead of running it now, wait until after the resolver has run."
  [{:keys [::directives] :as context} args resolved]
  (let [role-setters (get-in directives [:field :role-setters])
        role-setters-not-based-on-resolution
        (filter (fn [{:keys [based_on]}]
                  (not= based_on :resolution)) role-setters)]
    (run-role-setters context role-setters-not-based-on-resolution
                      args resolved)))

(def permitted-field-interceptor
  "Check permissions for the resolver."
  {:name :permitted-field
   :enter
   (fn [context args resolved]
     (let [ctx (run-early-role-setters context args resolved)
           {:keys [roles ::directives]} ctx
           required-roles (get-in directives [:field :required-roles])]
       (log/trace {:roles roles
                   :required-roles required-roles
                   :field (qualified-field-name (executor/selection context))
                   :authorized (authorized? required-roles roles)}
                  "permitted-field-interceptor")
       (if (authorized? required-roles roles)
         ctx
         (assoc ctx ::error-resolution (if (list-type? context)
                                         resolve-list-error-403
                                         resolve-error-403)))))})

(defmulti apply-directive (fn [directive _]
                            (selection/directive-type directive)))

(defmethod apply-directive :require_roles
  [directive effects]
  (->> (selection/arguments directive)
       :roles
       set
       (update effects :required-roles set/union)))

(defmethod apply-directive :set_role
  [directive effects]
  (update effects :role-setters conj (selection/arguments directive)))

(defmethod apply-directive :paginate
  [_ effects]
  (assoc effects :paginated-field? true))

(defmethod apply-directive :redact
  [directive effects]
  (update effects :redact concat (-> (selection/arguments directive)
                                     :content)))

(defmethod apply-directive :check_argument
  [directive effects]
  (let [specs (->> (selection/arguments directive)
                   :specs
                   (map (fn [spec] {:spec spec})))]
    (update effects :check-argument concat specs)))

(defmethod apply-directive :string_max_length
  [directive effects]
  (let [args (selection/arguments directive)]
    (update effects :check-argument concat [{:spec :string_max_length
                                             :args args}])))

(defmethod apply-directive :string_length_minmax
  [directive effects]
  (let [args (selection/arguments directive)]
    (update effects :check-argument concat [{:spec :string_length_minmax
                                             :args args}])))

(defmethod apply-directive :trim_string
  [_ effects]
  (assoc effects :trim-string? true))

(defn- run-directives
  [directives]
  (let [directive-vals (->> directives
                            vals
                            (apply concat))]
    (reduce (fn [effects directive]
              (apply-directive directive effects))
            {} directive-vals)))

(comment
  (map #(-> %
            selection/field
            selection/qualified-name) (-> sel selection/selections))

  ;; Get the fields selected in a paginated request
  (->> sel
       selection/selections
       (some #(when (= :edges (-> % selection/field selection/field-name))
                %))
       selection/selections
       (some #(when (= :node (-> % selection/field selection/field-name))
                %))
       selection/selections
       (map #(-> %
                 selection/field
                 selection/qualified-name))))

(defn- update-simple-field
  [context update-fn]
  (update context ::resolution update-fn))

(defn update-list-field
  [context update-fn]
  (update context ::resolution #(map update-fn %)))

(defn- update-paginated-field
  [{:keys [::resolution] :as context} update-fn]
  (if resolution
    (update-in context [::resolution :edges]
               #(map (fn [edge] (update edge :node update-fn)) %))
    context))

(defn update-resolution
  "Apply a function to a resolution's data."
  [{:keys [::directives] :as context} update-fn]
  (let [paginated-field? (get-in directives [:field :paginated-field])]
    (cond
      paginated-field?     (update-paginated-field context update-fn)
      (list-type? context) (update-list-field context update-fn)
      :else                (update-simple-field context update-fn))))

(defn redaction-interceptor
  "Based on the user's roles, remove content from the resolution."
  [directives]
  (let [paginated-field? (get-in directives [:field :paginated-field])
        redact (get-in directives [:type :redact])]
    (when (seq redact)
      {:name :redaction
       :leave
       (fn [{:keys [roles] :as context} _ _]
         (let [redact-fn (fn [res to-redact]
                           (protocol/redact-content to-redact context
                                                    res roles))
               run-all-fn #(reduce redact-fn % redact)]
           (log/trace {:paginated-field? paginated-field?
                       :list-type (list-type? context)
                       :schema-key (:schema-key context)
                       :redact redact
                       :roles roles
                       :resolution (::resolution context)
                       :redacted (::resolution (update-resolution context run-all-fn))}
                      "redaction-interceptor")
           (update-resolution context run-all-fn)))})))

(defn- run-enter-functions
  "Run the enter functions for the interceptors.
  Chain the context through each function and return the final
  context.  If a function assigns something to ::error-resolution,
  stop and return the context."
  [{:keys [::interceptors] :as context} args resolved]
  (loop [remaining interceptors
         ctx context
         args' args]
    (if (or (empty? remaining) (::error-resolution ctx))
      ctx
      (let [func (:enter (first remaining))
            updated-context (if (nil? func)
                              ctx
                              (func ctx args' resolved))
            _ (when-not (nil? func)
                (log/trace {:schema-key (:schema-key context)
                            :name (:name (first remaining))
                            :updated-args (::updated-args updated-context)
                            :err (::error-resolution updated-context)}
                           "Ran :enter"))]
        (recur (rest remaining)
               updated-context
               (or (::updated-args updated-context) args'))))))

(defn- run-leave-functions
  "Run the leave functions for the interceptors.
  Chain the context through each function and return the final
  context."
  [{:keys [::interceptors] :as context} args resolved]
  (loop [remaining (reverse interceptors)
         ctx context]
    (let [func (:leave (first remaining))]
      (if (empty? remaining)
        ctx
        (recur (rest remaining)
               (if (nil? func)
                 ctx
                 (let [result (func ctx args resolved)]
                   (log/trace {:schema-key (:schema-key context)
                               :name (:name (first remaining))
                               :resolution (::resolution result)}
                              "Ran :leave")
                   result)))))))

(defn- run-or-promise-leave-functions
  "Run the leave functions for the interceptors.
  If the resolution is a promise, set up the functions to run when
  the promise is completed."
  [{:keys [::resolution] :as context} args resolved]
  (if (prom/promise? resolution)
    (let [lacinia-resolution (resolve/resolve-promise)
          logging-context (get-in context [:request :logging-context])]
      (prom/then
       (prom/catch resolution prom/resolved)
       #(do
          (log/with-logging-context logging-context
            (log/trace {:res %
                        :is-wrapped? (select-utils/is-wrapped-value? %)}
                       "got a promise completion"))
          (resolve/deliver! lacinia-resolution
                            ;; A wrapped value here means it was made
                            ;; by resolve/with-error.
                            (if (select-utils/is-wrapped-value? %)
                              %
                              (log/with-logging-context logging-context
                                (-> (assoc context ::resolution %)
                                    (run-leave-functions args resolved)
                                    ::resolution))))))
      (assoc context ::resolution lacinia-resolution))
    (try
      (run-leave-functions context args resolved)
      (catch Exception e
        (log/error e "error running interceptor leave fns")))))

(defn inner-resolver-wrapper
  "Run the leave functions for the interceptors on a resolver result.
  This is suitable for use in resolve/wrap-resolver-result."
  [context args resolved resolver-result]
  (log/trace {:res resolver-result
              :schema-key (:schema-key context)} "resolver result")
  (-> (assoc context ::resolution resolver-result)
      (run-or-promise-leave-functions args resolved)
      ::resolution))

(defn- interceptor-list
  "Create the list of interceptors for a resolver."
  [directives]
  (->> [rate-limit-interceptor
        (argument-modification-interceptor directives)
        (argument-check-interceptor directives)
        user-role-interceptor
        (redaction-interceptor directives)
        permitted-field-interceptor]
       (remove nil?)))

(defn- field-directives
  "Parse all the directives on a field."
  [fdef]
  (let [field-directives (-> fdef
                             selection/directives
                             run-directives)
        kind (-> fdef selection/kind)
        kind-type (-> kind selection/kind-type)
        type-directives (some-> (if (= kind-type :root)
                                  kind
                                  (selection/of-kind kind))
                                ;; This will be nil for scalar types.
                                selection/of-type
                                selection/directives
                                run-directives)
        argument-defs (-> fdef
                          selection/argument-defs)
        argument-directives (s/transform [s/MAP-VALS] #(-> %
                                                           selection/directives
                                                           run-directives)
                                         argument-defs)]
    {:field field-directives
     :type type-directives
     :argument argument-directives}))

(defn apply-field-directives
  "Wrap a resolver function with parsed directives."
  [fdef resolver]
  (let [directives (field-directives fdef)
        interceptors (interceptor-list directives)]
    (log/trace {:qualified-name (selection/qualified-name fdef)
                :directives directives}
               "directive introspection results")
    (fn [context args resolved]
      (resolver (-> context
                    (assoc ::directives directives
                           ::interceptors interceptors))
                args resolved))))

(defn- setup-context
  "Set the context up with and for the resolver interceptors."
  [{:keys [::interceptors] :as context} schema-key]
  (log/trace {:schema-key schema-key
              :interceptors (map :name interceptors)}
             "set up context for resolver")
  (-> context
      (assoc :schema-key schema-key
             ::interceptors interceptors)))

(defn wrap-resolver
  "Wrap a resolver function with rate limiting, exception handling,
  authorization and argument checking.  Operate on an entry from the
  resolver map, so take and return a two item vector containing the
  schema key (used for looking up rate limiters) and resolver
  function."
  [[schema-key resolver]]
  (let [resolve (fn [context args resolved]
                  (resolver/run-resolver resolver context args resolved))
        wrapped-resolver (resolve/wrap-resolver-result
                          resolve inner-resolver-wrapper)]
    [schema-key
     ^{:tag resolve/ResolverResult}
     (fn [context args resolved]
       (let [logging-context (get-in context [:request :logging-context])]
         (log/with-logging-context logging-context
           (if (nil? (::interceptors context))
             ;; Don't allow a resolver to run that hasn't been through
             ;; apply-field-directives.
             (do (log/error {:schema-key schema-key}
                            "Field without directives")
                 resolve-error-500)
             (try
               (let [args (kebab-case-keys args)
                     inbound (-> context
                                 (setup-context schema-key)
                                 (run-enter-functions args resolved))
                     err (::error-resolution inbound)
                     args' (-> (or (::updated-args inbound) args))]
                 (log/trace {:err err :schema-key schema-key
                             :resolution (::resolution inbound)}
                            "inbound complete")
                 (or err
                     (let [res (wrapped-resolver inbound args' resolved)]
                       (log/trace {:err err
                                   :schema-key schema-key
                                   :inbound (::resolution inbound)
                                   :res res} "after leave functions")
                       res)))
               (catch Exception e
                 (log/error {:expound (expound-str (ex-data e) context)
                             :msg (ex-message e)} e "Error in resolver")
                 resolve-error-500))))))]))

(comment
  #_(require '[user :as u])
  (u/q "query {user_by_name(name: \"cameronlindsay\") {name uid unread_message_count} }"
       {} "cameronlindsay")
  (u/q "query {user_by_uid(uid: \"20369e6b-afd6-4d8b-b3b5-56bc5096ba61\")
         {name uid status} }" {} )
  (u/q "query {all_subs (first: 3) {edges { node { sid name}}}}" {})
  (u/q "query {post_by_pid (pid: \"12524\") { author { name status }}}" {} "cameronlindsay")
  (u/q "query {user_by_name (name: \"kristie94\") { uid status}}" {})
  (u/q "query {invite_codes (first: 1, search:\"type_out_step\")
                {edges { node { code used_by { name email } }}}}" {} "cameronlindsay")
  (def sel (first @u/taps)) ;; (executor/selection context)

  (def schema (first @u/taps))
  (selection/selection-kind sel)

  ;; Getting the name of a field
  (-> (selection/selections sel)
      first
      selection/field
      selection/field-name)

  ;; Get map of directives
  (-> (selection/selections sel)
      first
      selection/field
      selection/directives
      :auth
      first
      selection/arguments
      )

  ;; Get directives on resolver arguments
  (-> sel
      selection/field
      selection/argument-defs
      vals
      last
      selection/directives
      )

  ;; Determine if selection is a :list or :root
  (-> sel selection/field selection/kind selection/kind-type)

  ;; Determine what a :root selection is
  (-> sel selection/field selection/kind selection/of-type)

  ;; Get directives of a :root selection
  (-> sel selection/field selection/kind selection/of-type selection/directives)

  ;; Determine what it's a list of
  (-> sel selection/field selection/kind selection/of-kind selection/of-type)

  (-> sel selection/field))

;;; Resolver factories

(defn to-java-time
  "Generate a resolver to convert SQL timestamps to milliseconds since 1970."
  [key]
  (fn [_ _ resolved]
    (let [val (key resolved)]
      (if (or (nil? val) (select-utils/is-wrapped-value? val))
        val
        (try
          (str (.getTime val))
          (catch Exception e
            (log/error {:val val} e "error in time conversion")
            resolve-error-500))))))

(comment
  (require '[java-time.api :as jt])
  ;; ISO 8601 formatting with java-time
  (jt/format :iso-offset-date-time
             (jt/zoned-date-time (jt/sql-timestamp) "UTC"))
  (jt/format :iso-offset-date-time (jt/zoned-date-time))
  (-> "2024-01-01T10:00:01.567Z"
      jt/zoned-date-time
      jt/sql-timestamp))

;; Resolvers for the schema

(defn resolver-map
  []
  (->> {:Comment/author                     user/user-by-reference
        :Comment/checkoff                   comment/checkoff-by-reference
        :Comment/content-history            comment/content-history-by-reference
        :Comment/post                       post/post-by-reference
        :Comment/user-attributes            comment/user-attributes-by-reference
        :CommentCheckoff/user               user/user-by-reference
        :CommentTree/comments               comment/comment-list-by-reference
        :CommentVote/comment                comment/comment-by-reference
        :InviteCode/created-by              user/user-by-reference
        :InviteCode/used-by                 user/user-list-by-reference
        :Message/receiver                   user/user-by-reference
        :Message/sender                     (user/user-by-reference-factory
                                             :sender-uid)
        :MessageThread/first-message        message/first-message-by-reference
        :MessageThread/latest-message       message/latest-message-by-reference
        :MessageThread/report               message/report-by-reference
        :MessageThread/log                  message/log-by-reference
        :MessageThread/sub                  sub/sub-by-reference
        :Post/author                        user/user-by-reference
        :Post/comment-tree                  comment/comment-tree-by-reference
        :Post/content-history               post/content-history-by-reference
        :Post/open-reports                  post/open-reports-by-reference
        :Post/poll-options                  post/poll-options-by-reference
        :Post/sub                           sub/sub-by-reference
        :Post/title-history                 post/title-history-by-reference
        :Post/user-attributes               post/user-attributes-by-reference
        :PostReport/post                    post/post-by-reference
        :PostVote/post                      post/post-by-reference
        :RequiredNameChange/admin           (user/user-by-reference-factory
                                             :admin-uid)
        :SiteConfiguration/footer           config/footer-link-list-by-reference
        :SiteConfiguration/software         config/software-list-by-reference
        :Sub/creator                        (user/user-by-reference-factory
                                             :creator-uid)
        :Sub/moderators                     sub/moderators-by-reference
        :Sub/post-flairs                    sub/flairs-by-reference
        :Sub/post-type-config               sub/post-type-config-by-reference
        :Sub/posts                          post/posts-by-reference
        :Sub/rules                          sub/rules-by-reference
        :Sub/user-attributes                sub/user-attributes-by-reference
        :SubMessageLogHighlightChange/user  user/user-by-reference
        :SubMessageLogMailboxChange/user    user/user-by-reference
        :SubMessageLogNotification/user     user/user-by-reference
        :SubMessageLogRelatedReport/user    user/user-by-reference
        :SubMessageLogRelatedThread/user    user/user-by-reference
        :SubModeration/sub                  sub/sub-by-reference
        :SubModerator/mod                   user/user-by-reference
        :TitleHistory/user                  user/user-by-reference
        :User/attributes                    user/attributes-by-reference
        :User/content-blocks                user/content-blocks-by-reference
        :User/subs-moderated                user/moderates-by-reference
        :User/subscriptions                 user/subscriptions-by-reference
        :User/username-history              user/username-history-by-reference
        :UserContentBlock/user              user/user-by-reference

        :queries/comment-by-cid             comment/comment-by-cid
        :queries/comments-by-cid            comment/comments-by-cids

        :queries/site-configuration         config/site-configuration

        :queries/message-by-mid             message/message-by-mid
        :queries/message-thread-by-id       message/modmail-message-thread-by-id
        :queries/modmail-notification-thread message/modmail-notification-thread
        :queries/modmail-thread             message/modmail-thread
        :queries/modmail-threads            message/modmail-threads

        :queries/all-posts                  post/all-posts
        :queries/default-posts              post/default-posts
        :queries/post-by-pid                post/post-by-pid

        :queries/announcement-post-id       site/current-announcement
        :queries/banned-username-strings    site/banned-username-strings
        :queries/funding-progress           site/current-funding-progress
        :queries/invite-codes               site/invite-codes
        :queries/invite-code-settings       site/invite-code-settings
        :queries/site-stats                 site/stats
        :queries/site-visitor-counts        site/visitor-counts

        :queries/all-subs                   sub/all-subs
        :queries/default-subs               sub/default-subscriptions
        :queries/sub-by-name                sub/sub-by-name

        :queries/current-user               user/current-user
        :queries/required-name-change       user/required-name-change
        :queries/user-by-name               user/user-by-name
        :queries/user-by-uid                user/user-by-uid

        :queries/votes                      vote/votes

        :mutations/checkoff-comment         comment/check-off-comment
        :mutations/create-comment           comment/create-comment
        :mutations/distinguish-comment      comment/distinguish
        :mutations/un-checkoff-comment      comment/un-check-comment
        :mutations/update-comment-content   comment/update-comment-content

        :mutations/create-message-to-mods   message/create-message-to-mods
        :mutations/create-modmail-message   message/create-modmail
        :mutations/create-modmail-reply     message/create-modmail-reply
        :mutations/update-message-unread    message/update-message-unread
        :mutations/update-modmail-mailbox   message/update-modmail-mailbox
        :mutations/update-thread-unread     message/update-thread-unread

        :mutations/create-post              post/create-post
        :mutations/remove-post              post/remove-post
        :mutations/create-upload-url        post/create-upload-url
        :mutations/distinguish-post         post/distinguish
        :mutations/update-post-viewed       post/update-viewed
        :mutations/save-post                post/save-post
        :mutations/set-post-nsfw            post/set-nsfw
        :mutations/set-post-flair           post/set-post-flair
        :mutations/remove-post-flair        post/remove-post-flair
        :mutations/update-post-content      post/update-post-content
        :mutations/update-post-title        post/update-post-title

        :mutations/ban-string-in-usernames  site/ban-string-in-usernames
        :mutations/expire-invite-codes      site/expire-invite-codes
        :mutations/generate-invite-code     site/generate-invite-code
        :mutations/set-invite-code-settings site/set-invite-code-settings
        :mutations/unban-string-in-usernames site/unban-string-in-usernames

        :mutations/change-user-flair        sub/change-user-flair
        :mutations/create-user-flair-choice sub/create-user-flair-choice
        :mutations/create-sub-post-flair    sub/create-post-flair
        :mutations/create-sub-rule          sub/create-rule
        :mutations/delete-sub-post-flair    sub/delete-post-flair
        :mutations/delete-sub-rule          sub/delete-rule
        :mutations/reorder-sub-post-flairs  sub/reorder-post-flairs
        :mutations/reorder-sub-rules        sub/reorder-rules
        :mutations/update-sub-post-flair    sub/update-post-flair
        :mutations/update-sub-post-type-config sub/update-post-type-config

        :mutations/change-subscription-status user/change-subscription-status
        :mutations/update-user-preferences  user/update-preferences
        :mutations/require-name-change      user/require-name-change
        :mutations/cancel-required-name-change user/cancel-required-name-change

        :mutations/cast-comment-vote        vote/cast-comment-vote
        :mutations/cast-post-vote           vote/cast-post-vote
        :mutations/start-remove-votes       vote/start-remove-votes
        :mutations/stop-remove-votes        vote/stop-remove-votes}
       (s/transform [s/ALL] wrap-resolver)
       (s/transform [s/MAP-KEYS] ->snake-case-qualified)))

(defn time-resolver-map
  []
  (->> {:Comment/edited                     (to-java-time :lastedit)
        :Comment/time                       (to-java-time :time)
        :CommentCheckoff/time               (to-java-time :time)
        :CommentVote/datetime               (to-java-time :datetime)
        :ContentHistory/time                (to-java-time :time)
        :InviteCode/created-on              (to-java-time :created)
        :InviteCode/expires                 (to-java-time :expires)
        :Message/time                       (to-java-time :time)
        :Post/edited                        (to-java-time :edited)
        :Post/title-edited                  (to-java-time :title-edited)
        :Post/posted                        (to-java-time :posted)
        :PostUserAttributes/viewed          (to-java-time :viewed)
        :PostVote/datetime                  (to-java-time :datetime)
        :RequiredNameChange/required-at     (to-java-time :required-at)
        :Sub/creation                       (to-java-time :creation)
        :SubMessageLogHighlightChange/time  (to-java-time :time)
        :SubMessageLogMailboxChange/time    (to-java-time :time)
        :SubMessageLogNotification/time     (to-java-time :time)
        :SubMessageLogRelatedReport/time    (to-java-time :time)
        :SubMessageLogRelatedThread/time    (to-java-time :time)
        :User/joindate                      (to-java-time :joindate)
        :UserNameHistory/changed            (to-java-time :changed)}
       (s/transform [s/MAP-KEYS] ->snake-case-qualified)))

(defn pagination-resolver-map
  []
  (let [paginated-objects [:Comment
                           :InviteCode
                           :Message
                           :MessageThread
                           :Post
                           :SiteLogEntry
                           :Sub
                           :SubLogEntry
                           :Vote]
        namespaced-resolver (fn [obj suffix field]
                              [(keyword (str (name obj) suffix) (name field))
                               (schema/default-field-resolver field)])
        fields (fn [obj]
                 [(namespaced-resolver obj "Connection" :edges)
                  (namespaced-resolver obj "Connection" :pageInfo)
                  (namespaced-resolver obj "Edge" :node)
                  (namespaced-resolver obj "Edge" :cursor)])
        page-info [(namespaced-resolver :PageInfo "" :hasPreviousPage)
                   (namespaced-resolver :PageInfo "" :hasNextPage)
                   (namespaced-resolver :PageInfo "" :startCursor)
                   (namespaced-resolver :PageInfo "" :endCursor)]]
    (merge (->> (map fields paginated-objects)
                (apply concat)
                (into {}))
           (into {} page-info))))

(defn default-resolver
  "The default for the :default-field-resolver option.
  Uses the field name as the key into the resolved value."
  [field-name]
  (log/trace {:field-name field-name} "using default resolver")
  (let [kebab-case-field-name (csk/->kebab-case field-name)
        resolve (fn [context _ v]
                  (log/trace {:field field-name
                              :interceptors (->> context
                                                 ::interceptors
                                                 (map :name)
                                                 vec)
                              :resolved-value v}
                             "default resolver")
                  (resolve/resolve-as (get v kebab-case-field-name)))
        [_ func] (wrap-resolver [(keyword "Default" (name field-name)) resolve])]
    func))

;; Subscriptions

(defn make-csrf-token-check-interceptor
  "Apply a CSRF token check to a schema resolver."
  [secret-key session-lifetime]
  {:name :csrf-token
   :enter
   (fn [{:keys [request] :as context} _ _]
     (let [token (get-in context [::lacinia/connection-params :token])
           decoded-token (when token
                           (compat/decode-csrf-token
                            token secret-key session-lifetime))
           csrf-token (get-in request compat/csrf-token-path)]
       (cond-> context
         (or (nil? decoded-token) (not= decoded-token csrf-token))
         (assoc ::error-resolution resolve-error-403))))})

(defn subscription-interceptor-list
  [directives]
  [rate-limit-interceptor
   user-role-interceptor
   (redaction-interceptor directives)
   permitted-field-interceptor])

(defn- setup-streamer-context
  [{:keys [::interceptors] :as context} schema-key csrf-interceptor]
  (log/trace {:schema-key schema-key}
             "set up context for streamer")
  (assoc context
         :schema-key schema-key
         ::interceptors (cons csrf-interceptor interceptors)))

(defn wrap-stream-callback
  [callback-fn {:keys [request] :as context} args]
  (fn [value]
    (log/with-logging-context (:logging-context request)
      (log/trace {:schema-key (:schema-key context)
                  :directives (::directives context)
                  :interceptors (map :name (::interceptors context))
                  :roles (:roles context)
                  :value value} "processing streamer value")
      (if (or (nil? value)
              (resolve/is-resolver-result? value))
        (callback-fn value)
        (let [result (-> (assoc context ::resolution value)
                         (run-or-promise-leave-functions args nil))]
          (log/trace {:schema-key (:schema-key context)
                      :value (::resolution result)}
                     "streamer value after leave interceptors")
          (callback-fn (::resolution result)))))))

(defn apply-subscription-field-directives
  "Wrap a streamer function with parsed directives."
  [fdef streamer]
  (let [directives (field-directives fdef)
        interceptors (subscription-interceptor-list directives)]
    (log/trace {:qualified-name (selection/qualified-name fdef)
                :directives directives
                :interceptors (map :name interceptors)}
               "subscription directive introspection results")
    (fn [context args source-stream-callback]
      (streamer (-> context
                    (assoc ::directives directives
                           ::interceptors interceptors))
                args source-stream-callback))))

(defn wrap-streamer
  "Wrap a streamer function with a CSRF token check, rate limiting,
  exception handling and argument checking.  Operate on an entry from
  the streamer map, so it takes and returns a two item vector
  containing the schema key (used for looking up rate limiters) and
  streamer function.

  Streamer functions run on the Clojure async pool so they should
  avoid doing anything that might block their thread."
  [[schema-key streamer] secret-key session-lifetime]
  (let [csrf-interceptor (make-csrf-token-check-interceptor
                          secret-key session-lifetime)]
    [schema-key
     (fn [{:keys [request] :as context} args source-stream-callback]
       (log/with-logging-context (:logging-context request)
         (if (nil? (::interceptors context))
           (do (log/error {:schema-key schema-key}
                          "Subscription without directives")
               (stream/stream-resolution resolve-error-500
                                         source-stream-callback))
           (try
             (let [args' (kebab-case-keys args)
                   inbound (-> context
                               (setup-streamer-context schema-key
                                                       csrf-interceptor)
                               (run-enter-functions args' nil))
                   callback (wrap-stream-callback source-stream-callback
                                                  inbound args')
                   err (::error-resolution inbound)]
               (log/trace {:err err :schema-key schema-key}
                          "streamer inbound complete")
               (if err
                 (stream/stream-resolution err source-stream-callback)
                 (resolver/run-streamer streamer context args callback)))
             (catch Exception e
               (log/error e "Error in streamer")
               (stream/stream-resolution resolve-error-500
                                         source-stream-callback))))))]))

(defn- streamer-map
  [{:keys [secret-key session-lifetime]}]
  (let [wrapper #(wrap-streamer % secret-key session-lifetime)]
    (s/transform
     [s/ALL] wrapper
     {:Post/stream-post-updates
      post/stream-post-updates

      :NotificationCounts/update-notification-counts
      notification-counts/stream-notification-updates

      :Post/stream-announcement-post-id
      post/stream-announcement-post-id

      :Site/stream-funding-progress
      site/stream-funding-progress

      :User/stream-subscription-updates
      user/stream-subscription-updates

      :Comment/stream-new-comments
      comment/stream-new-comments

      :Comment/stream-comment-updates
      comment/stream-comment-updates

      :URLMetadata/url-metadata-result
      link/stream-url-metadata

      :Vote/removal-update
      vote/stream-vote-removal-updates

      :UserScore/update
      user/stream-score-updates})))

;; Custom scalars

(def scalar-map
  {:scalars
   {:Void
    {:parse (constantly "void")
     :serialize (constantly "void")}}})

;; Schema

(defn- modify-schema
  "Add development and test queries to schema."
  [base-schema {:keys [schema resolvers streamers]}]
  (let [resolvers (s/transform [s/MAP-KEYS] ->snake-case-qualified resolvers)]
    (-> base-schema
        (as-> $ (merge-with merge $ schema))
        (util/inject-resolvers resolvers)
        (util/attach-streamers streamers))))

(defn- read-schema-files
  []
  (let [schema-files ["schema/message-schema.edn"
                      "schema/ovarit-schema.edn"
                      "schema/post-schema.edn"
                      "schema/user-schema.edn"]
        partial-schemas (map #(-> (io/resource %)
                                  slurp
                                  edn/read-string)
                             schema-files)]
    (apply merge-with merge partial-schemas)))

(defn- log-schema-stats [schema]
  (log/debug {:stats (s/transform [s/MAP-VALS] count schema)}
             "schema stats"))

(defn- assemble-schema
  "Read the schema files and attach the resolvers and streamers."
  ([] (assemble-schema {}))
  ([{:keys [extras] :as schema-config}]
   (try
     (-> (read-schema-files)
         (as-> $ (merge-with merge $ scalar-map))
         (util/inject-resolvers (resolver-map))
         (util/inject-resolvers (time-resolver-map))
         (util/inject-resolvers (pagination-resolver-map))
         (util/attach-streamers (streamer-map schema-config))
         (modify-schema extras)
         (doto log-schema-stats))
     (catch Exception e
       (log/error {:ex-data (ex-data e)} e "Error reading schema files")))))

(defn- load-schema
  "Return either a compiled schema, or a function which returns one.
  Returning a function allows schema changes to take immediate effect
  in development."
  [{:keys [env] :as schema-config}]
  (let [options {:enable-introspection? false #_(= env :dev)
                 :disable-checks? (not (#{:dev :test} env))
                 :disable-java-objects? true
                 :default-field-resolver default-resolver
                 :apply-field-directives apply-field-directives
                 :apply-subscription-field-directives
                 apply-subscription-field-directives}
        load-fn (fn []
                  (try
                    (-> (assemble-schema schema-config)
                        (schema/compile options))
                    (catch Exception e
                      (log/error {:exception-message (.getMessage e)}
                                 "Error compiling schema"))))]
    (if (= env :dev)
      load-fn
      (load-fn))))

(defn- prepare-query
  "Attach the parsed version of the query to an entry from the known
  queries table, and return a list with the hash and the modified
  entry.  Return nil, and log a warning, if the query fails to parse."
  [compiled-schema {:keys [hash query] :as entry}]
  (log/debug {:hash hash :query query} "preparing query")
  (try
    [hash (assoc entry :parsed
                 (parser/parse-query compiled-schema query))]
    (catch Exception e
      (log/warn {:hash hash :query query :exception-message (.getMessage e)}
                "Failed to parse query")
      nil)))

(defn- prepare-queries
  "Return a map from hashes to queries, based on the supplied map of
  known queries. Attach a parsed query.  Discard queries that fail to
  parse, and log a warning."
  [env schema query-hashes]
  (let [compiled-schema (if (= env :dev) (schema) schema)]
    (->> query-hashes
         (map #(prepare-query compiled-schema %))
         (into {}))))

(defn- directive-map-merge
  "Merge in additional keys to `directive-map`."
  [extras]
  (->> directive-map
       (map (fn [[enum mapping]]
              [enum (merge mapping (enum extras))]))
       (into {})))

(defrecord SchemaProvider [env extras query-hashes secret-key session-lifetime]
  component/Lifecycle

  (start [this]
    (let [compiled-schema (load-schema this)]
      (log/info {:count (count query-hashes)} "Parsing known queries")
      (assoc this
             :schema compiled-schema
             :parsed-queries (prepare-queries env compiled-schema
                                              query-hashes)
             :directive-map (directive-map-merge (:directive-map extras)))))

  (stop [this]
    (assoc this :schema nil)))

(extend-protocol protocol/Schema
  SchemaProvider
  (parse [{:keys [env schema parsed-queries]} graphql-query]
    (let [[_ hash] (str/split graphql-query #" ")
          query (get parsed-queries hash)]
      (if (= env :dev)
        (do
          (when-not query
            (log/warn {:query graphql-query} "Unknown query"))
          (if query
            (assoc query :parsed (parser/parse-query (schema) (:query query)))
            {:key :unknown
             :parsed (parser/parse-query (schema) graphql-query)}))
        (or query
            (throw (ex-info "Unknown query" {:status 400
                                             :hash-or-key hash}))))))

  (directive-method-key [{:keys [directive-map]} enum-type kw]
    (let [result (get-in directive-map [enum-type kw])]
      (if (nil? result)
        (log/error {:key kw} "Unknown schema directive")
        result))))
