;; gql/protocols/visitor-counter.clj -- Visitor counter protocol declaration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.visitor-counter)

(defprotocol VisitorCounter
  (add! [this ip] [this ip timestamp]
    "Record a visit from `ip` at `timestamp`.")
  (visitor-count [this]
    "Return the number of visitors that have been added to the database.
     May be less than the total passed to `add!` due to buffering or errors.
     This is provided for use by the tests.")
  (visitors [this since until by]
    "Get visitor statistics."))
