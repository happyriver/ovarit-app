;; gql/protocols/storage.clj -- File storage protocol for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.storage
  (:require [clojure.spec.alpha :as spec]))

;; Protocol to store files locally or remotely.

(spec/def ::filename string?)
(spec/def ::container string?)
(spec/def ::location (spec/keys :req-un [::filename
                                         ::container]))

(defprotocol Storage
  (write-file
    [this location stream] [this location stream metadata]
    "Write an input stream to storage.")
  (input-stream
    [this location]
    "Return an open input stream for the content.
     Should be used inside with-open to ensure that it is closed.")
  (rename-file
    [this old-location new-location-name]
    "Rename or move a file.")
  (signed-url
    [this location]
    "Generate a URL for uploads.")
  (exists?
    [this location]
    "Return whether a stored object exists at this location."))
