;; gal/protocols/tasks.clj -- Task protocol declaration for ovarit-app
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.tasks)

(defprotocol TaskList
  (state [this name]
    "Return the state atom for the task with task-name `name`.")

  (testrun [this name]
    "Run the action function associated with task-name `name`.
     Use in conjunction with the test-mode? flag in the task
     default options, which stops automatic scheduling."))
