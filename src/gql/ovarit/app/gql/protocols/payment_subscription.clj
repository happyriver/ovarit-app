;; gql/protocols/payment-subscription.clj -- Current user for ovarit-app.
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.payment-subscription)

(defprotocol PaymentSubscription
  (update-email [this user new_email]
    "Notify the subscription server of a change in a user's email address.
     Return :success, :failure or :retry depending on the outcome of
     the request.  `user` should be a map containing the keys :uid
     and :resets.")
  (cancel [this user]
    "Notify the subscription server of a cancelled subscription.
     Return :success, :failure or :retry depending on the outcome of
     the request.  `user` should be a map containing the keys :uid
     and :resets.")
  (stats [this user]
    "Fetch the sum of monthly subscriptions and donations, in dollars.
     `user` should be a map containing the keys :uid and :resets."))
