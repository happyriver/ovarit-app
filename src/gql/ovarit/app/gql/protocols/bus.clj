;; gql/protocols/bus.clj -- Message bus protocol declaration for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.bus)

(defprotocol Bus
  "Protocol for interchange of messages by topic and id.

  Delivers messages both within the instance and between instances.

  Differs from the Notify protocol in that it does not exchange
  messages with Python Throat.  Differs from the pubsub protocol in
  that the implementation is much more light weight.  The pubsub
  implementation uses one thread per subscription, and this one uses
  one thread per topic subscribed to."
  (subscribe [this topic id handler]
    "Subscribe to topic notifications by id.
     `topic` should be a keyword, and `id` must be a string.")
  (publish [this topic id payload]
    "Send `payload` to all subscribers to `topic` and `id`.")
  (close-subscription [this subscription]
    "Close a subscription."))
