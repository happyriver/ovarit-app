;; gql/protocols/image_manager.clj -- Uploaded image protocol for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.image-manager)

;; Protocol for communication with image processing service.

(defprotocol ImageManager
  (create-upload-url
    [this]
    "Create an URL to which a file may be uploaded.")

  (check-uploaded-file
    [this upload-url mime-types]
    "On completion of an upload, check the content uploaded.
     Return a filename to access the content.")

  (identify-upload [this upload-url]
    "Determine the content type of an uploaded file.
     Rename it with the correct extension and return a map containing
     the new name and mime type, or nil if there was an error.")

  (fetch-external-content
    [this external-url]
    "Fetch external content.
     Return a filename for the content fetched.")

  (create-thumbnail
    [this filename]
    "Create a thumbnail for an uploaded file.
     Return a filename for the thumbnail.")

  (remove-metadata
    [this filename]
    "Remove metadata from an image file.
     Return a new filename.")

  (make-public
    [this filename]
    "Copy a file from temporary to permanent storage.
     Return a new filename."))
