;; gql/protocols/cache.clj -- Cache protocol declaration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.cache)

(defprotocol Cache
  (lookup [this cache-key seconds action]
    "Look up a previous result for 'key' in the cache.  If it exists,
  return it, otherwise run 'action-fn' and cache and return its
  result.  'seconds' is how long to cache the result for.")

  (cache-get [this cache-key]
    "Get a value.")

  (cache-set [this cache-key value] [this cache-key value timeout]
    "Set a value, with an expiration of 5 minutes if not given.")

  (cache-del [this cache-key]
    "Remove a value from the cache."))
