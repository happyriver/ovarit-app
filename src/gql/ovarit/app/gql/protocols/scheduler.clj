;; gql/protocols/scheduler.clj -- Scheduler protocol declaration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.scheduler)

;; Protocol to schedule tasks. These tasks run on a threadpool in the
;; current process.

(defprotocol Scheduler
  (after [this interval fn]
    "Schedule `fn` to run after `interval` (ms). Return a function which
     can be called to stop it.")
  (every [this interval fn] [this interval fn options]
    "Schedule fn to run at intervals (ms). Return a function which can
     be called to stop it.  `options` if present is a map which may
     contain the keys :initial-delay and :desc.")
  (interspaced [this interval fn] [this interval fn options]
    "Schedule fn to be run periodically, with interval (ms) pauses in
     between invocations.  Return a function which can be called to
     stop it.  `options` if present is a map which may contain the
     keys :initial-delay and :desc."))
