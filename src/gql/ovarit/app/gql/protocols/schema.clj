;; gql/protocols/schema.clj -- Schema protocol declaration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.schema)

(defprotocol Schema
  (parse [this graphql-query]
    "Return a parsed query ready for execution.")
  (directive-method-key [this directive-enum schema-directive-key]
    "Return a map from schema directive keys to method keys."))

(comment (do (ns-unmap *ns* 'update-auth-info)
             (ns-unmap *ns* 'set-roles)
             (ns-unmap *ns* 'redact-content)
             (ns-unmap *ns* 'check-argument)))

(defmulti update-auth-info
  "Add supporting info for authorization decisions to the context."
  (fn [source {:keys [schema]} _args _resolved]
    (directive-method-key schema :info-setter source)))

(defmulti set-roles
  "Set appropriate authorization roles in the context."
  (fn [{:keys [setter]} {:keys [schema]}]
    (directive-method-key schema :role-setter setter)))

(defmulti redact-content
  "Remove content from a resolved object that the user isn't authorized to see."
  (fn [content-type {:keys [schema]} _resolution _roles]
    (directive-method-key schema :redaction content-type)))

;; Currently all the interface/update-auth-info methods attach maps to
;; the :auth-info key in the context, and when a role-setter is
;; looking for the value associated with a key we merge all the maps
;; together to see if the key is in one of them.  Probably ought to
;; find a smarter way to do that, or add a way to prioritize, or at
;; least complain if the key appears in more than one map.
(defn find-in-auth-info
  [auth-info keyw]
  (->> (vals auth-info)
       (apply merge)
       keyw))

(defmulti check-argument
  "Return true if an argument meets a specification."
  (fn [{:keys [spec]} {:keys [schema]} _arg]
    (directive-method-key schema :check-argument spec)))
