;; gql/protocols/current-user.clj -- Current user for ovarit-app.
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.current-user)

(defprotocol CurrentUser
  (get-uid [this]
    "Return the logged-in user's uid or nil if no login session.")
  (get-name [this]
    "Return the logged-in user's name or nil if no login session.")
  (user-if-realized [this]
    "Return the result of the sql query for the current user.
     If it hasn't yet been done, don't do it and return nil.")
  (user [this]
    "Return the result of the sql query for the current user.")
  (subs-moderated [this]
    "Return sub_mod records for the subs moderated by the current user.")
  (moderates? [this sid]
    "Return whether the user moderates a sub, by sid.")
  (moderates-by-name? [this sub]
    "Return whether the user moderates a sub, by name.")
  (moderation-level [this sid]
    "Return the moderation level the user has on a sub.")
  (can-admin? [this]
    "Return whether the user is an admin.")
  (is-admin? [this]
    "Return whether the user is an admin with admin capabilities enabled.")
  (totp-expiration [this]
    "Return the time when admin TOTP will expire, in milliseconds since 1/1/70."))
