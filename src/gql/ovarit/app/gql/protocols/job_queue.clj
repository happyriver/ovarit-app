;; gql/protocols/job_queue.clj -- Job queue protocol declaration for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.job-queue)

(defprotocol JobQueue
  (enqueue! [this db-tx job-type payload]
    "Add a job to the queue."))

(comment
  (ns-unmap *ns* 'handle-job!))

(defmulti handle-job!
  "Process a job."
  (fn [_job-queue job-type _payload] job-type))
