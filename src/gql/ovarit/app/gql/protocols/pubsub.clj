;; gql/protocols/pubsub.clj -- Publish-subscribe protocol declaration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.protocols.pubsub)

;; The Redis implementation of this uses one thread per subscription,
;; so use sparingly.
(defprotocol PubSub
  "Protocol for message publish and subscribe."
  (subscribe [this chan handler] [this chan handler options]
    "Subscribe to a channel identified by `chan`.
     `handler` will be called with one argument, the deserialized payload,
     when a message arrives.  Deserialization is controlled by the :format
     option in `options`, possible values are :edn (the default) and :json.")
  (psubscribe [this pattern handler] [this pattern handler options]
    "Subscribe to a channel identified by `pattern`.
     See Redis docs for what to use for `pattern`.  `handler` will be
     called with two arguments when a message arrives: the channel the
     message was sent on and the deserialized payload.
     Deserialization is controlled by the :format option in `options`,
     possible values are :edn (the default) and :json.")
  (publish [this chan value] [this chan value options]
    "Publish a mesage on a channel identified by `chan`.
     Serialize it according to the :format option in `options`,
     possible values are :edn (the default) and :json.")
  (close-subscription [this subscription]
    "Close a subscription started by `subscribe` or `psubscribe`."))
