;; gql/system.clj -- Integration of subsystems for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system
  (:require
   [cambium.codec :as codec]
   [cambium.core :as log]
   [cambium.logback.json.flat-layout :as flat]
   [camel-snake-kebab.core :as csk]
   [cheshire.core :as cheshire]
   [clojure.core.reducers :as r]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.set :as set]
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [dotenv]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.config.tasks :as config]
   [ovarit.app.gql.elements.post.tasks :as post]
   [ovarit.app.gql.elements.site.tasks :as site]
   [ovarit.app.gql.elements.user.tasks :as user]
   [ovarit.app.gql.elements.vote.tasks :as vote]
   [ovarit.app.gql.protocols.asset-path :as asset-path]
   [ovarit.app.gql.schema :as schema]
   [ovarit.app.gql.system.bus :as bus]
   [ovarit.app.gql.system.cache.redis :as redis-cache]
   [ovarit.app.gql.system.db :as db]
   [ovarit.app.gql.system.image-manager :as image-manager]
   [ovarit.app.gql.system.job-queue :as job-queue]
   [ovarit.app.gql.system.notify.throat-py :as throat-py-notify]
   [ovarit.app.gql.system.payment-subscription.ovarit :as payment-subscription]
   [ovarit.app.gql.system.pubsub.redis :as redis-pubsub]
   [ovarit.app.gql.system.ratelimit.token-bucket :as ratelimit-implementation]
   [ovarit.app.gql.system.runtime :as runtime]
   [ovarit.app.gql.system.scheduler :as scheduler]
   [ovarit.app.gql.system.server :as server]
   [ovarit.app.gql.system.storage.local :as local-storage]
   [ovarit.app.gql.system.storage.s3 :as s3-storage]
   [ovarit.app.gql.system.tasks :as tasks]
   [ovarit.app.gql.system.visitor-counter.pg-hyperloglog :as visitor-counter]
   [ovarit.app.gql.util :as util])
  (:gen-class))

;; Server configuration

(def session-lifetime
  (or (util/env-integer :PERMANENT_SESSION_LIFETIME)
      (* 31 24 60 60)))

(def headers-to-log
  (let [value (dotenv/env :LOG_REQUEST_HEADERS)
        headers (when value
                  (try
                    (cheshire/parse-string value)
                    (catch Exception e
                      (log/error {:exception-message (.getMessage e)}
                                 "JSON parse error in LOG_REQUEST_HEADERS")
                      nil)))]
    (map str/lower-case headers)))

(defn query-hashes
  "Load the query hash table and parse its dates into epoch milliseconds."
  []
  (let [to-ms (fn [datestr]
                (-> (jt/local-date "yyyy-MM-dd" datestr)
                    (jt/instant (jt/zone-offset 0))
                    jt/to-millis-from-epoch))]
    (->> (io/resource "query-hashes.edn")
         slurp
         edn/read-string
         (map (fn [entry]
                (update entry :date to-ms))))))

;; Look in the assets directory for the newest subdirectory of /fe.  This
;; allows you to replace the front end with a new version by copying it
;; into a new directory with a new (cachebusting) name.
(defrecord NewestSubdirectory [root-path]
  asset-path/AssetPath
  (asset-path [_ path]
    (let [dirs (when-not (nil? root-path)
                 (let [fe-dir (io/file root-path)
                       files (when (.isDirectory fe-dir)
                               (.listFiles fe-dir))]
                   (filter (fn [f] (and (.isDirectory f)
                                        (not= "js" (.getName f)))) files)))]
      (if (seq dirs)
        (let [dirs-with-times (zipmap dirs
                                      (map (fn [f] (.lastModified f)) dirs))
              newest-dir (key (apply max-key val dirs-with-times))
              basename (.getName newest-dir)]
          (str "/fe/" basename path))
        (str "/fe" path)))))

(defrecord DevAssets []
  asset-path/AssetPath
  (asset-path [_ path]
    path))

(defn- manifest
  "Read the manifest file produced by shadow.cljs.
  Return a map from module keyword to module filename."
  [env path]
  (let [filename (if (= :dev env)
                   "resources/public/js/compiled/manifest.edn"
                   (str path "/js/compiled/manifest.edn"))]
    (->> (io/file filename)
         slurp
         edn/read-string
         (map (juxt :name :output-name))
         (into {}))))

(defn get-server-config [{:keys [env server]}]
  (or
   server
   (let [configured-path (dotenv/env :ASSET_PATH)
         path (if (nil? configured-path)
                ""
                ;; Make the static asset path configuration more human-friendly
                ;; by doing the right thing whether or not they put a slash
                ;; on the end.
                (if (str/ends-with? configured-path "/")
                  (apply str (butlast configured-path))
                  configured-path))
         static-assets (if (= env :dev)
                         (DevAssets.)
                         (NewestSubdirectory. path))
         service-path (asset-path/asset-path static-assets "")
         secret-key (dotenv/env :SECRET_KEY)
         admin-totp (dotenv/env :SITE_ENABLE_TOTP)]
     ;; Put a warning in the logs if no files are present to serve.
     (when (= service-path "/fe")
       (log/info {:ASSET_PATH path}
                 "No subdirectories found in ASSET_PATH"))
     (when (nil? secret-key)
       (log/error "SECRET_KEY not set"))
     {:host (or
             (dotenv/env :SERVER_HOST)
             "localhost")
      :port (or
             (util/env-integer :SERVER_PORT)
             8888)
      :assets static-assets
      :manifest (manifest env path)
      :env env
      :redis-uri (dotenv/env :REDIS_URL)
      :request-timeout (or (util/env-integer :SERVER_REQUEST_TIMEOUT) 2500)
      :process-queue-size (or (util/env-integer :SERVER_PROCESS_REQUESTS) 16)
      :waiting-queue-size (or (util/env-integer :SERVER_QUEUE_REQUESTS) 640)
      :trusted-proxy-count (or (util/env-integer :SITE_TRUSTED_PROXY_COUNT) 0)
      :headers-to-log headers-to-log
      :secret-key secret-key
      :session-lifetime session-lifetime
      :stop-hour (dotenv/env :SERVER_STOP_HOUR)
      :site-name (dotenv/env :SITE_SERVER_NAME)
      :sub-prefix (or (dotenv/env :SITE_SUB_PREFIX) "s")
      :pool-size (or (util/env-integer :SERVER_QUERY_POOL_SIZE) 16)
      :admin-totp? (#{"True" "true" "1"} admin-totp)
      :query-hashes (query-hashes)
      :clock (jt/system-clock "UTC")})))

(defn webserver-component
  [config]
  (component/using
   (server/map->Server (get-server-config config))
   [:bus :cache :db #_:image-manager :job-queue :notify :pubsub :ratelimiter
    :schema-provider :scheduler :taskrunner :visitor-counter]))

(defn health-check-server-component
  [config]
  (component/using
   (server/map->HealthCheckServer (get-server-config config))
   [:cache]))

(defn get-storage-config
  [{:keys [env storage]}]
  (or storage
      (let [provider (-> (dotenv/env :STORAGE_PROVIDER)
                         str/lower-case)
            access-key (dotenv/env :AWS_ACCESS_KEY_ID)
            secret-key (dotenv/env :AWS_SECRET_ACCESS_KEY)
            local? (= "local" provider)]
        {:local? local?
         :credentials (cond
                        local? nil

                        (and access-key secret-key)
                        {:access-key access-key
                         :secret-key secret-key
                         :endpoint provider}

                        :else
                        {:endpoint provider})
         :env env})))

(defn storage-component [config]
  (let [config (get-storage-config config)]
    (if (:local? config)
      (local-storage/map->LocalStorage config)
      (s3-storage/map->S3Storage config))))

;; File uploads.
;;
;; The `:image-service` component can be run alongside the graphql
;; server or in its own separate process.  Either way, the graphql
;; server uses the `:image-manager` component to make requests to the
;; image service to accomplish file-related tasks.

(defn get-image-service-config [{:keys [env image-service]}]
  (or
   image-service
   {:host (or
           (dotenv/env :IMAGE_SERVER_HOST)
           "localhost")
    :port (or
           (util/env-integer :IMAGE_SERVER_PORT)
           8898)
    :service-url (dotenv/env :IMAGE_SERVICE_URL)
    :site-name (dotenv/env :SITE_SERVER_NAME)
    :containers {:public (dotenv/env :IMAGE_CONTAINER_PUBLIC)
                 :temporary (dotenv/env :IMAGE_CONTAINER_TEMPORARY)}
    :env env}))

(defn image-service-component
  [config]
  (component/using
   (server/map->ImageService (get-image-service-config config))
   [:storage]))

(defn get-image-manager-config
  [{:keys [image env]}]
  (or image
      {:image-service-url (dotenv/env :IMAGE_SERVICE_URL)
       :env env}))

(defn image-manager-component
  [config]
  (component/using
   (image-manager/map->ImageManager (get-image-manager-config config))
   [:cache]))

;; Schema configuration

(defn get-schema-provider-config [{:keys [env schema-provider]}]
  (or schema-provider
      {:env env
       :secret-key (dotenv/env :SECRET_KEY)
       :session-lifetime session-lifetime
       :query-hashes (query-hashes)}))

(defn schema-provider-component [config]
  (schema/map->SchemaProvider (get-schema-provider-config config)))

(defn get-cache-config [{:keys [cache]}]
  (or cache
      {:uri (dotenv/env :REDIS_URL)}))

(defn cache-component [config]
  (redis-cache/map->RedisCache (get-cache-config config)))

(defn get-job-queue-config [{:keys [env job-queue]}]
  (or job-queue
      {:env env
       :clock (jt/system-clock "UTC")
       :redis-uri (dotenv/env :REDIS_URL)
       :tr format}))

(defn job-queue-component [config]
  (component/using
   (job-queue/map->DbJobQueue (get-job-queue-config config))
   [:db :cache :notify :pubsub]))

(defn get-notify-config [{:keys [env notify]}]
  (or notify {:env env
              :prefixes {:outgoing "/send"
                         :incoming "/snt"}}))

(defn notify-component [config]
  (component/using (throat-py-notify/map->ThroatNotify
                    (get-notify-config config))
                   [:pubsub]))

(defn get-pubsub-config [{:keys [pubsub]}]
  (or pubsub
      {:uri (dotenv/env :REDIS_URL)}))

(defn pubsub-component [config]
  (redis-pubsub/map->RedisPubSub (get-pubsub-config config)))

(defn get-bus-config [{:keys [bus]}]
  (or bus
      {:prefix "mbus"}))

(defn bus-component [config]
  (component/using (bus/map->MessageBus (get-bus-config config))
                   [:pubsub]))

(defn get-ratelimiter-config [{:keys [ratelimiter]}]
  (or ratelimiter
      (let [value (dotenv/env :RATELIMIT_ENABLE)
            explicitly-disabled (or (= value "False") (= value "false"))]
        {:enabled? (not explicitly-disabled)
         :uri (dotenv/env :REDIS_URL)})))

(defn ratelimiter-component [config]
  (ratelimit-implementation/map->RateLimiter (get-ratelimiter-config config)))

(defn get-visitor-counter-config [{:keys [visitor-counter]}]
  (or visitor-counter {}))

(defn visitor-counter-component [config]
  (component/using (visitor-counter/map->VisitorCounter
                    (get-visitor-counter-config config))
                   [:db]))

(defn get-db-config [{:keys [db]}]
  (or db
      {:host (dotenv/env :DATABASE_HOST)
       :port (dotenv/env :DATABASE_PORT)
       :name (dotenv/env :DATABASE_NAME)
       :user (dotenv/env :DATABASE_USER)
       :password (dotenv/env :DATABASE_PASSWORD)}))

(defn db-component [config]
  (db/map->Db (get-db-config config)))

(defn get-scheduler-config [{:keys [scheduler]}]
  (or scheduler {}))

(defn scheduler-component [config]
  (scheduler/map->Scheduler (get-scheduler-config config)))

(defn- task-envvars
  "Return the environment variables that configure tasks (the ones that
  include the substring '_TASK_') as a map."
  []
  (->> (dotenv/env)
       (filter #(str/includes? (key %) "_TASK_"))
       (map (fn [[k v]] [(keyword k) v]))
       (into {})))

(defn- extract-task-options
  "From the map of all task options (derived from environment
  variables), get the ones relevant to the current task into a map.
  Supply the default if a value is not given, and parse integers
  if an integer value is expected."
  [taskname defaults envvars]
  (let [prefix (-> taskname
                   name
                   csk/->snake_case
                   str/upper-case
                   (str "_TASK_"))
        remove-prefix #(-> (name %)
                           (subs (count prefix))
                           str/lower-case
                           csk/->kebab-case
                           keyword)
        prefix-mismatch? #(not (str/starts-with? (name %) prefix))
        task-envvars (->> envvars
                          (s/setval [s/MAP-KEYS prefix-mismatch?] s/NONE)
                          (s/transform [s/MAP-KEYS] remove-prefix))
        parse-int (fn [k v vdef]
                    (try
                      (Integer/parseInt v)
                      (catch java.lang.NumberFormatException _
                        (log/error {:task taskname :option k}
                                   "Non-numeric value in environment variable")
                        vdef)))
        option-or-default (fn [[k vdef]]
                            (let [option (k task-envvars)]
                              [k (if-not option
                                   vdef
                                   (if (integer? vdef)
                                     (parse-int k option vdef)
                                     option))]))]
    (s/transform [s/ALL] option-or-default defaults)))

(comment
  (def m {:REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_LIMIT "100000",
          :RANDOM 32432,
          :REMOVE_OLD_COMMENT_VIEWS_TASK_INTERVALS "600000",
          :REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_SIZE "10000"})
  #_(extract-task-options :remove-old-comment-views (:defaults (first tasks)) m))

(defn- add-task-options
  "Add options derived from environment variables to a task description."
  [{:keys [task-name defaults] :as task} envvars]
  (assoc task :options
         (extract-task-options task-name defaults envvars)))

(def taskrunner-tasks
  "Tasks to run if the system includes the :taskrunner role."
  (let [host (or (dotenv/env :OVARIT_SUBSCRIPTION_HOST) "")
        payment-subscription (payment-subscription/map->OvaritSubscription
                              {:secret-key (dotenv/env :SECRET_KEY)
                               :host host})]
    (map #(assoc % :single-instance? true)
         [{:task-name :remove-old-comment-views
           :task-fn post/remove-old-comment-views
           :defaults {:interval 60000
                      :batch-size 10000
                      :batch-limit 100000
                      :initial-delay 60000}
           :description "Remove old comment views"}

          {:task-name :visitor-count-update
           :task-fn visitor-counter/update-uniques
           :defaults {:interval 5000
                      :batch-size 10000
                      :batch-limit 100000
                      :initial-delay 90000}
           :description "Update visitor counts"}

          {:task-name :subscription-update
           :task-fn user/update-customers
           :defaults {:interval 60000
                      :payment-subscription payment-subscription
                      :initial-delay 30000}
           :description "Update payment subscriptions"}

          {:task-name :name-change-notification
           :task-fn user/notify-required-name-changes
           :defaults {:interval 5000
                      :initial-delay 50000}
           :description "Notify admins of completed required name changes"}

          {:task-name :refresh-site-config
           :task-fn config/update-site-config
           :defaults {:interval 30000  ; in ms
                      :cache-ex 40     ; in s
                      :initial-delay 0}}

          {:task-name :admin-remove-votes
           :task-fn vote/remove-votes-on-admin-request
           :description "Remove votes from users designated by admin"
           :interspaced? true
           :defaults {:interval 100  ; ms
                      :initial-delay 20000  ; ms
                      ;; This timeout, which is an expiration time
                      ;; used for Redis keys, should be a number of
                      ;; seconds longer than the daily restart, which
                      ;; takes about 20s in prod, plus the initial
                      ;; delay above.
                      :status-timeout 180  ; seconds
                      ;; Number of times to call the vote removal queries
                      ;; before releasing the thread.
                      :batch-size 120}}])))

(def webserver-tasks
  "Tasks to run in each instance with the :webserver role."
  (let [host (or (dotenv/env :OVARIT_SUBSCRIPTION_HOST) "")
        payment-subscription (payment-subscription/map->OvaritSubscription
                              {:secret-key (dotenv/env :SECRET_KEY)
                               :host host})]
    (map #(assoc % :single-instance? false)
         [{:task-name :funding-progress-update
           :task-fn site/update-funding-progress
           :defaults {:interval 300000
                      :initial-state 0
                      :payment-subscription payment-subscription}
           :description "Update funding progress"}])))

(defn get-tasks-config
  "Add options extracted from environment variables to a list of tasks."
  [roles {:keys [env tasks]}]
  (or tasks
      {:env env
       :uri (dotenv/env :REDIS_URL)
       :parent "system"
       :taskrunner? (some? (roles :taskrunner))
       :tasks
       (let [envvars (task-envvars)]
         (map #(add-task-options % envvars)
              (concat (when (roles :webserver)
                        webserver-tasks)
                      (when (roles :taskrunner)
                        taskrunner-tasks))))}))

(defn tasks-component [roles config]
  (component/using
   (tasks/map->TaskRunner (get-tasks-config roles config))
   [:db :bus :cache :notify :scheduler]))

(defn- create-component
  [ctype roles config]
  (case ctype
    :cache           (cache-component config)
    :db              (db-component config)
    :bus             (bus-component config)
    :pubsub          (pubsub-component config)
    :job-queue       (job-queue-component config)
    :notify          (notify-component config)
    :ratelimiter     (ratelimiter-component config)
    :scheduler       (scheduler-component config)
    :schema-provider (schema-provider-component config)
    :storage         (storage-component config)
    :visitor-counter (visitor-counter-component config)
    :taskrunner      (tasks-component roles config)
    :server          (webserver-component config)
    :image-manager   (image-manager-component config)
    :image-service   (image-service-component config)))

(defn new-system-map
  "Build a map describing all the parts of the system."
  [roles config]
  (let [
        ;; List all the transitive dependencies for each variation on
        ;; service.
        by-role {:webserver     #{:bus :cache :db :pubsub :job-queue :notify
                                  :ratelimiter :scheduler :schema-provider
                                  :taskrunner #_:image-manager :visitor-counter
                                  :server}
                 :taskrunner    #{:bus :cache :db :notify :pubsub :scheduler
                                  :taskrunner}
                 :image-service #{:storage :image-service}}
        components (->> (map by-role roles)
                        (apply set/union)
                        (map (fn [ctype]
                               [ctype (create-component ctype roles config)]))
                        (into {}))]
    (log/debug {:roles roles
                :components (keys components)} "System map components")
    ;; If the configuration doesn't include a web server, add
    ;; one that just answers /health.
    (if (or (:server components) (:image-service components))
      components
      (assoc components :health-check-server
             (health-check-server-component config)))))

(defn new-system
  "Collect all the moving parts."
  [roles {:keys [env] :as config-map}]
  (let [system-map (new-system-map roles config-map)]
    (log/info {:env env} "Creating system")
    (apply component/system-map (into [] (r/flatten system-map)))))

(spec/def ::arg #{"--webserver" "--taskrunner" "--image-service"})
(spec/def ::args (spec/+ ::arg))

(defn parse-args
  "Return the arguments, keywordized, as a set."
  [args]
  (let [parsed-args (spec/conform ::args args)]
    (if (= ::spec/invalid parsed-args)
      (do (println "Unrecognized command line arguments")
          (spec/explain ::args args))
      (->> args
           (map #(get {"--webserver"      :webserver
                       "--taskrunner"     :taskrunner
                       "--image-service"  :image-service} %))
           set))))

(comment
  (parse-args ["--webserver"])
  (parse-args ["--webserver" "--taskrunner"])
  (parse-args ["--help"])
  (parse-args []))

(defn -main [& args]
  ;; Configure logging back end.
  (flat/set-decoder! codec/destringify-val)
  (let [env (if (dotenv/env :DEBUG) :dev :prod)
        roles (parse-args args)
        system (new-system (parse-args args) {:env env})]
    (runtime/set-default-uncaught-exception-handler!
     (fn [_thread e]
       (log/error e "Uncaught exception, system exiting.")
       (System/exit 1)))
    (runtime/add-shutdown-hook!
     ::stop-system #(do
                      (log/info "System exiting, running shutdown hooks")
                      (component/stop system)))
    ;; Shut down threads used by Proletarian job queue.
    (runtime/add-shutdown-hook!
     ::stop-agents #(shutdown-agents))
    (when (seq roles)
      (component/start-system system))))
