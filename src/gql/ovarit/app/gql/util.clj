;; gql/util.clj -- Utilities for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.util
  (:require
   [cambium.core :as log]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [cheshire.core :as cheshire]
   [clj-uuid :as uuid]
   [clojure.java.jdbc :as jdbc]
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [clojure.walk :as walk]
   [com.rpl.specter :as s]
   [dotenv]
   [java-time.api :as jt]
   [pandect.algo.sha256 :refer [sha256]])
  (:import java.util.Base64)
  (:import [java.util.regex Pattern])
  (:import [com.github.slugify Slugify]))

(defn- remove-padding
  "Remove the trailing ='s created by Base64 encoding."
  [b64]
  (-> b64
      (str/split #"=")
      (nth 0)))

(defn encode [to-encode]
  (remove-padding (.encodeToString (Base64/getUrlEncoder)
                                   (.getBytes to-encode))))

(defn decode [to-decode]
  (String. (.decode (Base64/getUrlDecoder) to-decode)))

(defn uuid4
  "Return a randomly generated UUID.
  Equivalent to Python's 'uuid4'."
  []
  (uuid/to-string (uuid/v4)))

(defn sql-timestamp
  "Return a SQL timestamp for the present moment."
  []
  (-> (java.util.Date.)
      .getTime
      java.sql.Timestamp.))

(defn sql-now
  "Return a SQL timestamp using the server clock."
  [clock]
  (jt/with-clock clock
    (jt/sql-timestamp)))

(defn sql-timestamp-from-epoch
  "Convert milliseconds since the UNIX epoch into a timestamp."
  [epoch]
  (some-> epoch
          .longValue
          (* 1000)
          java.sql.Timestamp.))

(defn assoc-all
  "Assoc the same value to a list of keys."
  [m ks value]
  (reduce #(assoc %1 %2 value) m ks))

(defn rand-str
  "Make a string of random letters."
  [len]
  (apply str (take len (repeatedly #(char (+ (rand 26) 97))))))

(defn short-hash
  "Make an abbreviated sha256 hash from a string"
  [s]
  (-> s sha256 (subs 0 10)))

(defn env-integer
  "Return an environment variable's value as an integer, or nil."
  [envvar]
  (when-let [value (dotenv/env envvar)]
    (try
      (Integer/parseInt value)
      (catch java.lang.NumberFormatException _
        (log/error {:name envvar}
                   "Non-numeric value in environment variable")
        nil))))

(defn id-from-string
  "Return a string parsed as an integer, or nil."
  [id-str]
  (if (= (type id-str) java.lang.String)
    (try
      (Integer/parseInt id-str)
      (catch NumberFormatException _ nil))
    id-str))

(defn convert-fields-to-boolean
  "Convert the named fields to booleans.
  Use on fields represented in the database as '0' and '1'."
  [map fields]
  (reduce (fn [map field]
            (assoc map field (= "1" (field map))))
          map fields))

(defn kebab-case-keys
  "Translate the keys in m from snake_case to kebab-case."
  [data]
  (cske/transform-keys csk/->kebab-case data))

(defn snake-case-keys
  "Translate the keys in m from kebab-case to snake_case."
  [data]
  (cske/transform-keys csk/->snake_case data))

(defn ->snake-case-qualified
  "Transform a qualified keyword to snake case."
  [k]
  (keyword (namespace k) (csk/->snake_case (name k))))

(defn valid-protocol?
  "Determine whether an URL is parseable and uses a http protocol."
  [url]
  (try
    (-> (java.net.URL. url)
        .getProtocol
        #{"http" "https"}
        some?)
    (catch java.net.MalformedURLException _
      false)))

(def mention-regex
  "The regular expression to find user mentions in Markdown."
  (let [lookbehind "(?<=^|(?<=[^a-zA-Z0-9-_./]))"
        mention "((@|/u/)([A-Za-z0-9-_]+))"
        mention-in-md-link (str "\\[.*?"
                                "(" lookbehind mention ")"
                                ".*?\\]"
                                "\\(.*?\\)")
        standalone-mention (str "((?:"
                                "(\\[.+?\\]\\(.+?\\))"
                                "|"
                                lookbehind mention
                                "))")]
    (-> (str mention-in-md-link "|" standalone-mention)
        (Pattern/compile (+ Pattern/MULTILINE Pattern/DOTALL)))))

(comment
  (re-seq mention-regex "Mention [the user @foo](blah)")
  (re-seq mention-regex "Mention [the user
 /u/foo](blah)")
  (re-seq mention-regex "[link](link link) /u/foo @blah")
  (re-seq mention-regex "user@example.com")
  (re-seq mention-regex "glu/stick www.example.com/u/foo"))

(defn mentions
  "Find the user mentions in a Markdown string.
  Return a set of lowercased user names."
  [markdown]
  (->> markdown
       (re-seq mention-regex)
       (map #(remove nil? %))
       (filter #(> (count %) 2))
       (filter #(#{"/u/" "@"} (first (take-last 2 %))))
       (map last)
       (map str/lower-case)
       set))

(defn expound-str
  "Convert exception data into a string if it contains a spec failure.
  Check the env so just in case instrumentation gets turned on in
  production, stop it from logging any sensitive data that might be
  in the arguments."
  [data {:keys [env]}]
  (when (and env (#{:dev :test} env) (:clojure.spec.alpha/failure data))
    (-> data
        spec/explain-out
        with-out-str)))

(defn find-newest-query-version
  "Find the newest version of a query in the query hashes."
  [query-hashes kw]
  (->> query-hashes
       (s/select [s/ALL #(= kw (:key %))])
       (sort-by :date)
       last))

(defn truncate
  "Shorten a string to at most `len` characters."
  [s len]
  (subs s 0 (min len (count s))))

(defn remove-trailing-hyphen
  "If `s` ends with a trailing hyphen, remove it."
  [s]
  (if (= (last s) \-)
    (apply str (butlast s))
    s))

(defn condense-whitespace
  "Replace repeated whitespace with single spaces."
  [s]
  (->> (str/split s #"\s+")
       (str/join " ")))

(defn slugify
  "Create a URL-friendly slug from `s`, at most 80 characters long."
  [s]
  (let [slug (-> (.build (Slugify/builder))
                 (.slugify s)
                 (truncate 80)
                 (str/replace #"_" "-")
                 remove-trailing-hyphen)]
    (if (empty? slug) "_" slug)))

(comment
  (map mentions ["Mention [the user @foo](blah)"
                 "Mention [the user
 /u/foo](blah)"
                 "Mention [/u/bxx] and (@fyy)"
                 "[link](link link) /u/foo @blah@blah"
                 "user@example.com gl/u/stick"
                 "/u/user and @user"]))

;; Automatically convert between Clojure vectors and SQL arrays by
;; extending two protocols.
;; ref: https://stackoverflow.com/a/25786990/1251467

;; to SQL
(extend-protocol clojure.java.jdbc/ISQLParameter clojure.lang.IPersistentVector
                 (set-parameter [v ^java.sql.PreparedStatement stmt ^long i]
                   (let [conn (.getConnection stmt)
                         meta (.getParameterMetaData stmt)
                         type-name (.getParameterTypeName meta i)]
                     (if-let [elem-type (when (= (first type-name) \_) (apply str (rest type-name)))]
                       (.setObject stmt i (.createArrayOf conn elem-type (to-array v)))
                       (.setObject stmt i v)))))
;; from SQL
(extend-protocol clojure.java.jdbc/IResultSetReadColumn
  java.sql.Array
  (result-set-read-column [val _ _]
    (into [] (.getArray val)))

  ;; Turn PG json values into clojure maps with keywordized keys.
  org.postgresql.util.PGobject
  (result-set-read-column [val _ _]
    (->> (.getValue val)
         cheshire/parse-string
         walk/keywordize-keys)))
