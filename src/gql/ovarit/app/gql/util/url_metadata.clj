;; util/url_metadata.clj -- HTTP metadata for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.util.url-metadata
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [clojure.java.io]
   [clj-http.client :as client])
  (:import
   [org.ccil.cowan.tagsoup Parser]
   [java.io BufferedInputStream]
   [javax.xml.stream XMLStreamConstants]
   [javax.xml.stream.events XMLEvent]
   [javanet.staxutils ContentHandlerToXMLEventWriter XMLEventPipe]
   [org.xml.sax InputSource]))

(defn- content-type-header-value
  "Get the value of the content type header.
  If the site sends multiple content type headers, join
  the values together with semicolons."
  [resp]
  (let [content-type (get (:headers resp) "Content-Type")]
    ;; Some sites send multiple content-type headers and
    ;; then the get returns a sequence of strings.
    (if (string? (first content-type))
      (str/join "; " content-type)
      content-type)))

(defn- charset-from-headers
  "Try to determine a character set from the HTTP headers."
  [url resp]
  (let [content-type (content-type-header-value resp)
        chunks (-> content-type
                   (str/split #";"))
        charset-elem (->> chunks
                          (filter #(str/starts-with? (str/trim %) "charset="))
                          first)
        charset (when charset-elem
                  (let [splits (str/split charset-elem #"=")]
                    (when (> (count splits) 1)
                      (nth splits 1))))]
    (when charset
      (log/debug {:url url :charset charset} "Found charset in headers"))
    charset))

(defn- charset-from-html
  "Attempt to find an encoding declaration at the start of some HTML.
  `stream` should be a BufferedInputStream."
  [url stream]
  (let [size 1024  ;; per HTML spec
        arr (make-array Byte/TYPE size)]
    (.mark stream size)
    (loop [offset 0]
      (let [num (.read stream arr offset (- size offset))]
        (if (or (= num -1) (= (+ offset num) size))
          arr
          (recur (+ offset num)))))
    (.reset stream)
    (let [charset (java.nio.charset.Charset/forName "ISO-8859-1")  ; Latin-1
          s (String. arr charset)
          charset (or
                   (second (re-find #"charset=\"(.+?)\"" s))
                   (second (re-find #"encoding=\"(.+?)\"" s)))]
      (when charset
        (log/debug {:url url :charset charset} "Found charset in html"))
      charset)))

(defn- make-source
  "Set up the input source for the tagsoup parser."
  [url stream charset]
  (let [stream (-> stream BufferedInputStream.)
        source (InputSource. stream)]
    (.setEncoding source (or charset
                             (charset-from-html url stream)
                             "utf-8"))
    source))

(defn- stream-parse-html
  "Parse HTML in a streaming fashion.
  Pass an InputSource.  Returns a sequence of elements as clojure
  maps.  Parsing happens on a background thread, from which XML events
  are reported to the calling thread via a XMLEventPipe."
  [source]
  (let [pipe (XMLEventPipe.)
        ereader (.getReadEnd pipe)
        ewriter (.getWriteEnd pipe)
        parser (Parser.)]
    (.setContentHandler parser (ContentHandlerToXMLEventWriter. ewriter))
    (let [task (future
                 (try
                   (.parse parser source)
                   (finally
                     (.close ewriter))))]
      {:cancel #(future-cancel task)
       :parsed-html (iterator-seq ereader)})))

(defn- xml-name
  "Returns the local part of the name of a given XML entity as a keyword."
  [x]
  (keyword (.getLocalPart (.getName x))))

(defn- trunc
  "There's no limit to how big an attribute value can be in the HTML spec.
  Enforce one that should be large enough for reasonable metadata."
  ([val] (trunc val 4096))
  ([val lim]
   (subs val 0 (min lim (count val)))))

(defn- extract
  "Extract the content of a javax.xml.stream.events.XMLEvent.
  Return a clojure map containing event type, name, attributes,
  content."
  [^XMLEvent ev]
  (condp = (.getEventType ev)

    XMLStreamConstants/START_ELEMENT
    {:type :start-element
     :name (xml-name ev)
     :attrs (into {} (map (fn [attr]
                            [(xml-name attr) (trunc (.getValue attr))])
                          (iterator-seq (.getAttributes ev))))
     :content nil}

    XMLStreamConstants/END_ELEMENT
    {:type :end-element
     :name (xml-name ev)
     :attrs nil
     :content nil}

    XMLStreamConstants/CHARACTERS
    {:type :characters
     :name nil
     :attrs nil
     :content (.getData ev)}

    {:type :other
     :val ev}))

(def twitter-metadata-tags #{"twitter:title"
                             "twitter:description"
                             "twitter:image"})

(defn- update-twitter-data [data {:keys [type name attrs]}]
  (if (and (= type :start-element)
           (= name :meta)
           (seq (:name attrs))
           (or (:content attrs) (:value attrs))
           (twitter-metadata-tags (:name attrs)))
    (assoc-in data [:parsed (:name attrs)]
              (or (:content attrs) (:value attrs)))
    data))

(def opengraph-metadata-tags #{"og:title"
                               "og:description"
                               "og:site_name"
                               "og:url"})

(defn- update-opengraph-data [data {:keys [type name attrs]}]
  (if (and (= type :start-element)
           (= name :meta)
           (seq (:content attrs))
           (opengraph-metadata-tags (:property attrs)))
    (assoc-in data [:parsed (:property attrs)] (:content attrs))
    data))

(defn- complete-opengraph-image [{:keys [state] :as data}]
  (if (nil? state)
    data
    (-> data
        (update-in [:parsed "og:image"] concat [state])
        (dissoc :state))))

(defn- update-opengraph-images
  [data {:keys [type name attrs]}]
  (cond
    (and (= type :start-element)
         (= name :meta)
         (seq (:content attrs))
         (string? (:property attrs))
         (str/starts-with? (:property attrs) "og:image"))
    (cond-> data
      ;; A new og:image tag ends collection of properties of the
      ;; previous image, if any, and starts a new one.
      (= "og:image" (:property attrs))
      (-> complete-opengraph-image
          (assoc :state {"og:image" (:content attrs)}))

      ;; Add a property to the current image.
      (and (:state data) (not= "og:image" (:property attrs)))
      (update :state assoc (:property attrs) (:content attrs)))

    ;; The end of the head element ends collection of image
    ;; properties, if any.
    (and (= type :end-element)
         (= name :head)
         (:state data))
    (complete-opengraph-image data)

    :else data))

(defn- og-image-href [props]
  (or (get props "og:image:secure_url")
      (get props "og:image:url")
      (get props "og:image")))

(defn- acceptable-image-href? [href]
  (let [href (-> href
                 (str/split #"\?")
                 first)]
    (and (or (str/starts-with? href "/")
             (str/starts-with? href "http://")
             (str/starts-with? href "https://"))
         (not (str/ends-with? href "svg"))
         (not (str/ends-with? href "SVG")))))

(defn- acceptable-og-image? [props]
  (let [href (og-image-href props)
        type (get props "og:image:type")]
    (and (acceptable-image-href? href)
         (not= type "image/svg+xml"))))

(defn- combine-images
  "Combine the images found in opengraph and twitter metadata.
  Filter out unacceptable ones, obviously broken links and SVG's.  Use
  only the first (preferred by Facebook) and the last (preferred by
  Twitter) opengraph images, and add the twitter image to the list if
  it differs from the opengraph ones."
  [{og-images "og:image" twitter-image "twitter:image"}]
  (let [og-images (->> og-images
                       (filter acceptable-og-image?)
                       (map og-image-href))
        first-og-image (first og-images)
        last-og-image (last og-images)
        twitter-image (when (and twitter-image
                                 (acceptable-image-href? twitter-image))
                        twitter-image)]
    (->> [first-og-image
          (when (not= first-og-image last-og-image)
            last-og-image)
          (when (not ((set [first-og-image last-og-image]) twitter-image))
            twitter-image)]
         (remove nil?))))

(defn- update-oembed-data [data {:keys [type name attrs]}]
  (if (and (= type :start-element)
           (= name :link)
           (= (:rel attrs) "alternate")
           (#{"application/json+oembed" "text/xml+oembed"} (:type attrs)))
    (assoc-in data [:parsed (:type attrs)] (:href attrs))
    data))

(defn- update-html-description
  [data {:keys [type name attrs]}]
  (if (and (= type :start-element)
           (= name :meta)
           (= (:name attrs) "description"))
    (assoc-in data [:parsed "description"] (:content attrs))
    data))

(defn- update-html-title
  [data {:keys [type name content]}]
  (cond
    (and (= type :start-element) (= name :title))
    (assoc data :state {:reading true :content ""})

    (and (= type :characters) (get-in data [:state :reading])
         (string? content))
    (update-in data [:state :content] #(-> % (str content) trunc))

    (and (= type :end-element) (= name :title))
    (-> data
        (assoc-in [:state :reading] false)
        (assoc-in [:parsed "title"]
                  (-> (get-in data [:state :content])
                      str/trim)))
    :else data))

(defn- update-links
  [data {:keys [type name attrs]}]
  (if (and (= type :start-element) (= name :link) (seq (:href attrs)))
    (cond
      (= (:rel attrs) "canonical")
      (assoc-in data [:parsed "canonical-url"] (:href attrs))

      (and (= (:rel attrs) "icon") (string? (:href attrs)))
      (update-in data [:parsed "icons"] conj
                 (select-keys attrs [:href :sizes :type]))

      :else data)
    data))

(defn- acceptable-icon? [{:keys [href type]}]
  (and (acceptable-image-href? href)
       (not= type "image/svg+xml")))

(defn- icon-size-reverse [{:keys [sizes]}]
  (if (seq sizes)
    (let [[x y] (str/split sizes #"x")]
      (if (and x y)
        (* (or (parse-long x) 0) (or (parse-long y) 0) -1)
        0))
    0))

(defn- prioritize-icons
  "Return three icons from the list, ordered from larger to smaller."
  [{icons "icons"}]
  (->> icons
       (filter acceptable-icon?)
       (sort-by icon-size-reverse)
       (take 3)
       (map :href)))

(defn- complete-url
  "If `href` doesn't have a protocol and host, add one to match `url`'s."
  [href url]
  (if (str/starts-with? href "http")
    href
    (let [parsed (java.net.URL. url)]
      (str (.getProtocol parsed) "://" (.getAuthority parsed) href))))

;; It would be nice to put a limit on this, after 10000 elements give
;; up. (youtube header has 1064 element events)
;;
;; Except stopping the loop early such as by looking for (and (=
;; type :end-element) (= name :head)) causes the thread to get stuck
;; and never be closed.
;;
;; Apparently the way to do it is to write my own ContentHandler (see
;; the one in clojure.xml for an example) and have it throw a
;; SAXException when it gets to the end of the head or whatever limit
;; it has
;;
;; The tagsoup parser has a setContentHandler function.
;;
;; another idea: https://gist.github.com/raek/2378475

(defn- extract-body-metadata
  [url resp]
  (let [collectors {:twitter update-twitter-data
                    :opengraph update-opengraph-data
                    :opengraph-image update-opengraph-images
                    :oembed update-oembed-data
                    :description update-html-description
                    :links update-links
                    :title update-html-title}]
    (with-open [reader (:body resp)]
      (let [charset (charset-from-headers url resp)
            source (make-source url reader charset)
            {:keys [parsed-html]} (stream-parse-html source)
            metadata (loop [html (map extract parsed-html)
                            result {}
                            reading-head? true
                            num 0]
                       (let [{:keys [type name] :as elem} (first html)]
                         (log/trace {:elem elem
                                     :result result
                                     :reading-head? reading-head?}
                                    "Extracting body metadata")
                         (if (nil? elem)
                           result
                           (recur (rest html)
                                  (if reading-head?
                                    (reduce (fn [data [kw func]]
                                              (update data kw func elem))
                                            result collectors)
                                    result)
                                  (and reading-head?
                                       (not (and (= type :end-element)
                                                 (= name :head))))
                                  (inc num)))))]
        (->> metadata
             (map #(:parsed (val %)))
             (apply merge))))))

(defn- extract-header-metadata [_url {:keys [links]}]
  (when (and (:canonical links)
             (get-in links [:canonical :href]))
    {"header-canonical-url" (get-in links [:canonical :href])}))

(defn- metadata-request [url]
  ;; this has an async interface for slow urls
  ;; archive.org probably a good one to try it out on
  (let [timeout-ms 10000
        max-content-length (* 1024 1024)
        ;; The Yahoo user agent is necessary to get responses
        ;; from Twitter.
        headers {"User-Agent" "Yahoo! Slurp/Site Explorer"
                 "Range" (str "bytes=0-" (dec max-content-length))}]
    (client/get url {:as :stream
                     :headers headers
                     ;; Don't try to decode cookies because Apache
                     ;; Client will throw exceptions if
                     ;; megacorporations break the rules.
                     :decode-cookies false
                     :socket-timeout timeout-ms
                     :conn-timeout timeout-ms})))

(defn- get-raw-metadata [url]
  (let [resp (metadata-request url)
        content-type (some-> (content-type-header-value resp)
                             (str/split #";")
                             first)
        meta {"content-type" content-type}]
    (if (#{"application/xhtml+xml" "text/html"} content-type)
      (let [header-metadata (extract-header-metadata url resp)
            body-metadata (extract-body-metadata url resp)]
        (merge meta header-metadata body-metadata))
      meta)))

(defn get-metadata
  [url]
  (try
    (let [meta (get-raw-metadata url)
          images (->> (combine-images meta)
                      (map #(complete-url % url)))
          icons (->> (prioritize-icons meta)
                     (map #(complete-url % url)))]
      (log/info {:url url
                 :metadata meta} "Fetched URL metadata")
      {:title (or (get meta "title")
                  (get meta "og:title")
                  (get meta "twitter:title"))
       :content-type (get meta "content-type")
       :description (or (get meta "description")
                        (get meta "og:description")
                        (get meta "twitter:description"))
       :oembed-xml (get meta "text/xml+oembed")
       :oembed-json (get meta "application/json+oembed")
       :url (or (get meta "og:url")
                (get meta "header-canonical-url")
                (get meta "canonical-url"))
       :site-name (get meta "og:site_name")
       :images images
       :icons icons})
    (catch clojure.lang.ExceptionInfo e
      ;; An exception thrown by clj-http.
      (log/info {:url url :error (ex-message e)
                 :info (-> (ex-data e)
                           (dissoc :body :http-client))}
                "Error fetching URL metadata")
      {:error (ex-message e)})
    (catch Exception e
      ;; Log unexpected exceptions at a higher level than expected ones.
      (let [exception-name (-> e class .getCanonicalName)]
        (if (#{"java.io.EOFException"
               "java.net.ConnectException"
               "java.net.SocketTimeoutException"
               "java.net.UnknownHostException"
               "java.net.URISyntaxException"
               "java.security.cert.CertificateExpiredException"
               "javax.net.ssl.SSLHandshakeException"
               "javax.net.ssl.SSLPeerUnverifiedException"
               "org.apache.http.client.CircularRedirectException"
               "org.apache.http.NoHttpResponseException"
               "sun.security.provider.certpath.SunCertPathBuilderException"}
             exception-name)
          (log/info {:url url :error (.toString e)}
                    "Error fetching URL metadata")
          (log/error {:url url} e
                     "Unexpected error fetching URL metadata"))
        {:error exception-name}))))
