;; elements/constants.clj -- Constants used in SQL queries
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.constants
  (:require
   [cambium.core :as log]
   [clojure.set :refer [map-invert]]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [ovarit.app.gql.util :as util]))

(defn- remove-namespace [keyw]
  (keyword (name keyw)))

(defn- remove-key-namespaces [kvmap]
  (s/transform [s/MAP-KEYS] remove-namespace kvmap))

(def misc-constants {:MIN_INTEGER -2147483647})

(def comment-distinguishes {:comment/NORMAL 0
                            :comment/MOD 1
                            :comment/ADMIN 2})
(def comment-distinguish-map (remove-key-namespaces comment-distinguishes))
(def reverse-comment-distinguish-map (map-invert comment-distinguish-map))

(def comment-statuses {:comment/ACTIVE        0
                       :comment/USER_REMOVED  1
                       :comment/MOD_REMOVED   2
                       :comment/ADMIN_REMOVED 3
                       :comment/ADMIN_DELETED 4})
(def comment-status-map (remove-key-namespaces comment-statuses))
(def reverse-comment-status-map (map-invert comment-status-map))
(def comment-constants (merge comment-distinguishes
                              comment-statuses))

(def message-types {:message/USER_TO_USER        100
                    :message/USER_TO_MODS        101
                    :message/MOD_TO_USER_AS_USER 102
                    :message/MOD_TO_USER_AS_MOD  103
                    :message/MOD_DISCUSSION      104
                    :message/USER_NOTIFICATION   105
                    :message/MOD_NOTIFICATION    106})

(def message-type-map (remove-key-namespaces message-types))
(def reverse-message-type-map (map-invert message-type-map))

(def message-mailboxes {:message/INBOX    200
                        :message/SENT     201
                        :message/SAVED    202
                        :message/ARCHIVED 203
                        :message/TRASH    204
                        :message/DELETED  205
                        :message/PENDING  206})

(def message-mailbox-map (remove-key-namespaces message-mailboxes))
(def reverse-message-mailbox-map (map-invert message-mailbox-map))

(def message-log-actions {:message/CHANGE_MAILBOX        1
                          :message/HIGHLIGHT             2
                          :message/REF_POST_REPORT       3
                          :message/REF_COMMENT_REPORT    4
                          :message/DOWNVOTE_NOTIFICATION 5
                          :message/REF_NOTIFICATION      6
                          :message/REF_NEW_THREAD        7
                          :message/DM_BLOCKING_ABUSE     8
                          :message/REQUIRED_NAME_CHANGE  9})

(def message-log-action-map (remove-key-namespaces message-log-actions))
(def reverse-message-log-action-map (map-invert message-log-action-map))

(def message-constants (merge message-types
                              message-mailboxes
                              message-log-actions))

(def moderation-levels {:moderation-level/OWNER 0
                        :moderation-level/MODERATOR 1
                        :moderation-level/JANITOR 2})
(def moderation-level-map (remove-key-namespaces moderation-levels))
(def reverse-moderation-level-map (map-invert moderation-level-map))

(def moderation-level-constants moderation-levels)

(def post-distinguishes {:post-distinguish/NORMAL 0
                         :post-distinguish/MOD 1
                         :post-distinguish/ADMIN 2})
(def post-distinguish-map (remove-key-namespaces post-distinguishes))
(def reverse-post-distinguish-map (map-invert post-distinguish-map))

(def post-deleteds {:post/NOT_DELETED 0
                    :post/USER_REMOVED  1
                    :post/MOD_REMOVED   2
                    :post/ADMIN_REMOVED 3
                    :post/ADMIN_DELETED 4})
(def post-deleted-map (remove-key-namespaces post-deleteds))
(def reverse-post-deleted-map (map-invert post-deleted-map))

(def post-types {:post-type/TEXT 0
                 :post-type/LINK 1
                 :post-type/UPLOAD 2
                 :post-type/POLL 3})
(def post-type-map (remove-key-namespaces post-types))
(def reverse-post-type-map (map-invert post-type-map))

(def post-constants (merge post-distinguishes
                           post-types))

(def report-log-actions {:report-log/CLOSE                      55
                         :report-log/REOPEN                     56
                         :report-log/CLOSE_RELATED              57
                         :report-log/SUB_UNDELETE_POST          58
                         :report-log/SUB_UNDELETE_COMMENT       59
                         :report-log/POST_DELETED               60
                         :report-log/POST_UNDELETED             61
                         :report-log/COMMENT_DELETED            62
                         :report-log/COMMENT_UNDELETED          63
                         :report-log/USER_SITE_BANNED           64
                         :report-log/USER_SUB_BANNED            65
                         :report-log/USER_SITE_UNBANNED         66
                         :report-log/USER_SUB_UNBANNED          67
                         :report-log/NOTE                       68
                         :report-log/MODMAIL_TO_REPORTED_USER   1002
                         :report-log/MODMAIL_TO_REPORTER        1003
                         :report-log/EDIT_POST_TITLE            1004
                         :report-log/EDIT_POST_TYPE_SETTINGS    1005})

(def report-log-action-map (remove-key-namespaces report-log-actions))
(def reverse-report-log-action-map (map-invert report-log-action-map))
(def report-log-constants report-log-actions)

(def site-log-actions {:site-log/USER_BAN                   19
                       :site-log/ANNOUNCEMENT               41
                       :site-log/DOMAIN_BAN                 42
                       :site-log/DOMAIN_UNBAN               43
                       :site-log/UNANNOUNCE                 44
                       :site-log/USER_UNBAN                 54
                       :site-log/EMAIL_DOMAIN_BAN           69
                       :site-log/EMAIL_DOMAIN_UNBAN         70
                       :site-log/ADMIN_CONFIG_CHANGE        75
                       :site-log/BAN_USERNAME_STRING        1000
                       :site-log/UNBAN_USERNAME_STRING      1001
                       :site-log/REQUIRE_NAME_CHANGE        1006
                       :site-log/CANCEL_REQUIRE_NAME_CHANGE 1007

                       ;; Obsolete; use ADMIN_CONFIG_CHANGE instead of
                       ;; these in the future.
                       :site-log/DISABLE_POSTING            45
                       :site-log/ENABLE_POSTING             46
                       :site-log/ENABLE_INVITE              47
                       :site-log/DISABLE_INVITE             48
                       :site-log/DISABLE_REGISTRATION       49
                       :site-log/ENABLE_REGISTRATION        50
                       :site-log/DISABLE_CAPTCHAS           71
                       :site-log/ENABLE_CAPTCHAS            72})
(def site-log-action-map (remove-key-namespaces site-log-actions))
(def reverse-site-log-action-map (map-invert site-log-action-map))
(def site-log-constants site-log-actions)

(def sub-log-actions {:sub-log/CREATE           20
                      :sub-log/SETTINGS         21
                      :sub-log/BAN              22
                      :sub-log/UNBAN            23
                      :sub-log/MOD_INVITE       24
                      :sub-log/MOD_ACCEPT       25
                      :sub-log/MOD_REMOVE       26
                      :sub-log/MOD_INV_CANCEL   27
                      :sub-log/MOD_INV_REJECT   28
                      :sub-log/CSS_CHANGE       29
                      :sub-log/TRANSFER         30
                      :sub-log/STICKY_ADD       50
                      :sub-log/STICKY_DEL       51
                      :sub-log/DELETE_POST      52
                      :sub-log/DELETE_COMMENT   53
                      :sub-log/UNDELETE_POST    58
                      :sub-log/UNDELETE_COMMENT 59})

(def sub-log-action-map (remove-key-namespaces sub-log-actions))
(def reverse-sub-log-action-map (map-invert sub-log-action-map))
(def sub-log-constants sub-log-actions)

(def subscription-statuses {:subscription-status/SUBSCRIBED 1
                            :subscription-status/BLOCKED    2})
(def subscription-status-map (remove-key-namespaces subscription-statuses))
(def reverse-subscription-status-map (map-invert subscription-status-map))
(def subscription-constants subscription-statuses)

(def user-content-block-types {:user-content-block/HIDE 0
                               :user-content-block/BLUR 1})
(def user-content-block-map (remove-key-namespaces user-content-block-types))
(def reverse-user-content-block-map (map-invert user-content-block-map))

(def user-statuses {:user-status/ACTIVE    0
                    :user-status/PROBATION 1
                    :user-status/BANNED    5
                    :user-status/DELETED   10})
(def user-status-map (remove-key-namespaces user-statuses))
(def reverse-user-status-map (map-invert user-status-map))

(def user-constants (merge user-content-block-types
                           user-statuses))

(def vote-values {:vote/UP 1
                  :vote/DOWN 0})
(def vote-value-map (remove-key-namespaces vote-values))
(def reverse-vote-value-map (map-invert vote-value-map))
(def vote-constants vote-values)

(def constants (merge comment-constants
                      message-constants
                      misc-constants
                      moderation-level-constants
                      post-constants
                      report-log-constants
                      site-log-constants
                      sub-log-constants
                      subscription-constants
                      user-constants
                      vote-constants))

(defn trim-snip
  "Remove the SQL from a query parameter if it is a SQL snippet vector."
  [[k v]]
  (if (str/ends-with? k "-snip")
    [k (when v (vec (rest v)))]
    [k v]))

(defn sql-log-context
  "Create the logging context map for a hugsql db function call"
  [sym params ms]
  (let [args (->> params
                  (remove (comp nil? second))
                  (map trim-snip)
                  (into {}))]
    {:sql {:name sym :args args :ms ms}}))

(defmacro wrap-db-fns
  "Wrap all the named HugSQL functions.
  Add the above constants to their parameter maps, and do some better
  logging than jdbc.sqlinfo does."
  [& names]
  `(do ~@(map
          (fn [fnname]
            `(let [funcval# ~fnname]
               (intern *ns* (quote ~fnname)
                       (fn [db# & args#]
                         (let [params# (first args#)
                               now# (-> (java.util.Date.) .getTime)
                               result# (funcval# db# (merge ~constants params#))
                               ms# (- (-> (java.util.Date.) .getTime) now#)]
                           (log/info
                            (sql-log-context (quote ~fnname) params# ms#)
                            (str "SQL query (executed in " ms# " ms)"))
                           (util/kebab-case-keys result#))))))
          names)))

(defmacro wrap-db-fn
  "Wrap the named HugSQL function.
  Add the above constants to its parameter map, and do some better
  logging than jdbc.sqlinfo does."
  [fnname]
  `(let [funcval# ~fnname]
     (intern *ns* (quote ~fnname)
             (fn [db# & args#]
               (let [params# (first args#)
                     now# (-> (java.util.Date.) .getTime)
                     result# (funcval# db# (merge ~constants params#))
                     ms# (- (-> (java.util.Date.) .getTime) now#)]
                 (log/info
                  (sql-log-context (quote ~fnname) params# ms#)
                  (str "SQL query (executed in " ms# " ms)"))
                 (util/kebab-case-keys result#))))))

(defmacro wrap-snip-fns
  "Wrap all the named snip functions.
  Add the above constants to their parameter maps, if they take one."
  [& names]
  `(do ~@(map (fn [fnname]
                `(let [funcval# ~fnname]
                   (intern *ns* (quote ~fnname)
                           (fn
                             ([] (funcval#))
                             ([params#]
                              (funcval# (merge ~constants params#)))))))
              names)))

(defmacro wrap-snip-fn
  "Wrap the named snip function.
  Add the above constants to its parameter map, if it takes one."
  [fnname]
  `(let [funcval# ~fnname]
     (intern *ns* (quote ~fnname)
             (fn
               ([] (funcval#))
               ([params#]
                (funcval# (merge ~constants params#)))))))
