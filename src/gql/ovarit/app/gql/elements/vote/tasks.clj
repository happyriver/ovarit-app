;; gql/elements/vote/tasks.clj -- Task definitions for votes
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.vote.tasks
  (:require
   [cambium.core :as log]
   [ovarit.app.gql.elements.config.core :as config]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.message.tasks :as message-tasks]
   [ovarit.app.gql.elements.post.query :as post-query]
   [ovarit.app.gql.elements.sub.query :as sub-query]
   [ovarit.app.gql.elements.user.query :as user-query]
   [ovarit.app.gql.elements.vote.query :as query]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.util :as util]
   [statecharts.core :as fsm]
   [taoensso.carmine :as car]))

;;; Admin remove votes command

(def in-progress-set
  (str (symbol ::remove-in-progress)))

(defn- user-progress-key
  [uid]
  (str (symbol ::progress) ":" uid))

(defn- vote-type-match [typ]
  (fn [_ event]
    (= typ (:vote-type event))))

(defn- vote-type-states [typ]
  {:initial :removing
   :states {:removing {:on {:success [{:target :removing
                                       :guard (vote-type-match typ)}]
                            :failure [{:target :retry
                                       :guard (vote-type-match typ)}]}}
            :retry {:on {:success [{:target :removing
                                    :guard (vote-type-match typ)}]
                         :failure [{:target :done
                                    :guard (vote-type-match typ)}]}}
            :done {}}})

(def vote-removal-machine
  "A statechart to hold the progress of vote deletion for a user."
  {:id ::vote-removal
   :type :parallel
   :regions
   {:post (vote-type-states :post)
    :comment (vote-type-states :comment)}})

(defn start-vote-removal
  [{:keys [conn site-config]} uid total]
  (let [status-timeout (or (:status-timeout (site-config)) 180)
        fsm (fsm/initialize vote-removal-machine
                            {:context {:total total
                                       :remaining total}})]
    (car/wcar conn
              (car/sadd in-progress-set uid)
              (car/set (user-progress-key uid) fsm :EX status-timeout)))
  (log/info {:uid uid :total total}
            "Started removal of user votes"))

(defn stop-vote-removal
  ([ctx uid]
   (stop-vote-removal ctx uid false))
  ([{:keys [conn]} uid finished?]
   (car/wcar conn (car/srem in-progress-set uid))
   (log/info {:uid uid} (if finished?
                          "Finished vote removal"
                          "Stopping vote removal"))))

(defn get-vote-status
  "Get the progress of a vote removal operation."
  [conn uid]
  (let [[ismember val] (car/wcar conn
                                 (car/sismember in-progress-set uid)
                                 (car/get (user-progress-key uid)))
        in-progress? (= ismember 1)
        {:keys [remaining total]} (when in-progress? val)]
    (when in-progress?
      (log/info {:uid uid :val val :remaining remaining :total total}
                "get-vote-status"))
    {:remaining remaining
     :total total}))

(defn- admin-delete-vote
  "Remove a vote, and adjust scores in the database.
  Return true on success or false on failure."
  [{:keys [db]} typ uid]
  (try
    (let [result (count (query/admin-delete-vote db typ {:uid uid}))]
      (pos? result))
    (catch Exception e
      (log/error {:uid uid :typ typ} e "Error removing vote")
      false)))

(defn- remove-one-user-vote
  [{:keys [conn options] :as context} uid]
  (let [{:keys [status-timeout]} options
        context (assoc-in context [:db :counter] (atom 0))
        state (car/wcar conn (car/get (user-progress-key uid)))
        event (cond
                (not (= :done (get-in state [:_state :post])))
                (if (admin-delete-vote context :post uid)
                  {:type :success :vote-type :post}
                  {:type :failure :vote-type :post})

                (admin-delete-vote context :comment uid)
                {:type :success :vote-type :comment}

                :else
                {:type :failure :vote-type :comment})
        new-state (cond-> (fsm/transition vote-removal-machine state
                                          event
                                          {:ignore-unknown-event? true})
                    (= (:type event) :success)
                    (update :remaining dec))]
    (log/info {:new-state new-state :uid uid :event event}
              "Vote removal attempt")
    (if (or (zero? (:remaining new-state))
            (and (= :done (get-in state [:_state :post]))
                 (= :done (get-in state [:_state :comment]))))
      (stop-vote-removal context uid (zero? (:remaining new-state)))
      (car/wcar conn (car/set (user-progress-key uid) new-state
                              :EX status-timeout)))))

(defn remove-votes-on-admin-request
  "Remove votes from users flagged for vote removal.
  Makes approximately `batch-size` attempts to remove votes,
  if there are votes to remove."
  [{:keys [conn options] :as context}]
  (let [{:keys [batch-size]} options]
    (loop [num 0]
      (let [uids (car/wcar conn (car/smembers in-progress-set))]
        (when (and (< num batch-size) (seq uids))
          (doseq [uid uids]
            (remove-one-user-vote context uid))
          (recur (+ num (count uids))))))))

;;; Check downvoters for abusive behavior

(defn- silence-seconds
  [site-config]
  (* 86400 (:downvotes-silence (site-config))))

(defn- sitewide-key [uid]
  (str (symbol ::sitewide) ":" uid))

(defn- check-sitewide-downvotes
  [{:keys [clock conn db tr site-config] :as context} {:keys [uid username]}]
  (let [{num :downvotes-count days :downvotes-days} (site-config)
        site-wide-sent? (car/wcar conn (car/get (sitewide-key uid)))]
    (when-not site-wide-sent?
      (let [now (util/sql-now clock)
            votes (-> db
                      (query/sitewide-downvote-count {:uid uid
                                                      :days days
                                                      :now now})
                      :count)]
        (log/debug {:uid uid :votes votes :num num}
                   "Site-wide downvote notification check")
        (when (>= votes num)
          (log/info {:uid uid} "Site-wide downvote notification triggered")
          (message-tasks/notify-admins
           context
           {:uid uid
            :action :DOWNVOTE_NOTIFICATION
            :subject (tr "Excessive downvoting by %s" username)
            :content (-> (tr "[%s](/u/%s) exceeded site-wide downvoting
                              limits." username username)
                         util/condense-whitespace)})
          (car/wcar conn (car/set (sitewide-key uid) true
                                  :EX (silence-seconds site-config))))))))

(defn- personal-key [uid target]
  (str (symbol ::personal) ":" uid ":" target))

(defn- check-personal-downvotes
  [{:keys [clock conn db tr site-config] :as context}
   {:keys [uid username target]}]
  (let [personal-sent? (car/wcar conn (car/get (personal-key uid target)))
        {num :downvotes-personal-count
         days :downvotes-personal-days} (site-config)]
    (when-not personal-sent?
      (let [now (util/sql-now clock)
            votes (->> (query/per-user-vote-history db {:voter uid
                                                        :target target
                                                        :num num
                                                        :days days
                                                        :now now})
                       (map :positive)
                       (filter #{(:DOWN constants/vote-value-map)})
                       count)]
        (log/debug {:uid uid :target target :votes votes :num num}
                   "Personal downvote notification check")
        (when (>= votes num)
          (log/info {:uid uid :target target}
                    "Personal downvote notification triggered")
          (let [target-name (-> db
                                (user-query/list-users-by-uid
                                 {:uids [target]})
                                first
                                :name)
                content (-> (tr "[%s](/u/%s) exceeded limits by
                                 consecutively downvoting [%s](/u/%s)."
                                username username target-name target-name)
                            util/condense-whitespace)]
            (message-tasks/notify-admins
             context {:uid uid
                      :action :DOWNVOTE_NOTIFICATION
                      :subject (tr "Excessive downvoting by %s" username)
                      :content content}))
          (car/wcar conn (car/set (personal-key uid target) true
                                  :EX (silence-seconds site-config))))))))

(defn- thread-key [uid pid]
  (str (symbol ::thread) ":" uid ":" pid))

(defn- check-thread-downvotes
  [{:keys [conn db tr site-config] :as context} {:keys [uid username sid pid]}]
  (let [thread-sent? (car/wcar conn (car/get (thread-key uid pid)))
        {percent :downvotes-thread-percent
         num     :downvotes-thread-comments} (site-config)]
    (when-not thread-sent?
      (let [thread-percent (-> db
                               (query/thread-downvote-percent {:pid pid
                                                               :uid uid
                                                               :num num})
                               :percent)]
        (log/debug {:uid uid :pid pid
                    :thread-percent thread-percent :percent percent}
                   "Thread downvote notification check")
        (when (>= thread-percent percent)
          (log/info {:uid uid :pid pid}
                    "Thread downvote notification triggered")
          (let [sub-name (-> (sub-query/select-subs-by-sid db {:uid uid
                                                               :sids [sid]})
                             first
                             :name)
                post-title (-> (post-query/select-posts-by-pid db
                                                               {:pids [pid]
                                                                :info? true})
                               first
                               :title)
                content (-> (tr "[%s](/u/%s) exceeded limits for downvoting
                                 comments on [%s](/o/%s/%s) in [%s](/o/%s)."
                                username username post-title
                                sub-name pid sub-name sub-name)
                            util/condense-whitespace)]
            (message-tasks/notify-admins
             context {:uid uid
                      :action :DOWNVOTE_NOTIFICATION
                      :subject (tr "Excessive downvoting by %s" username)
                      :content content}))
          (car/wcar conn (car/set (thread-key uid pid) true
                                  :EX (silence-seconds site-config))))))))

(defmethod job-queue/handle-job! ::check-downvote-limits
  [job-queue _job-type {:keys [comment-vote?] :as payload}]
  (let [site-config (config/site-config (select-keys job-queue
                                                     [:db :cache :env]))
        context (-> job-queue
                    (select-keys [:clock :conn :db :tr :notify])
                    (assoc :site-config (constantly site-config)))]
    (check-sitewide-downvotes context payload)
    (check-personal-downvotes context payload)
    (when comment-vote?
      (check-thread-downvotes context payload))))

(comment
  (require '[user :as u])
  (def conn (-> u/system :pubsub :conn))

  (car/wcar conn (car/smembers in-progress-set))

  )
