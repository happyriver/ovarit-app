;; gql/elements/post/query.clj -- SQL queries for votes
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.vote.query
  (:require
   [clojure.spec.alpha :as spec]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.comment.spec :as comment-spec]
   [ovarit.app.gql.elements.constants :as constants :refer [wrap-db-fn
                                                            wrap-snip-fn]]
   [ovarit.app.gql.elements.post.spec :as post-spec]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec])
  (:require
   [ovarit.app.gql.elements.vote.spec :as-alias vote-spec]
   [ovarit.app.gql.elements.vote.spec.count-votes :as-alias count-votes]
   [ovarit.app.gql.elements.vote.spec.downvote-count :as-alias downvote-count]
   [ovarit.app.gql.elements.vote.spec.select-votes :as-alias select-votes]))

(spec/def ::vote-spec/xid int?)
(spec/def ::vote-spec/positive (set (vals constants/vote-value-map)))

(hugsql/def-db-fns "sql/votes.sql")

(wrap-snip-fn forward-datetime-snip)
(db-spec/fdef forward-datetime-snip
  :args (spec/cat :args (spec/keys :req-un [::select-votes/cursor]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn reverse-datetime-snip)
(db-spec/fdef reverse-datetime-snip
  :args (spec/cat :args (spec/keys :req-un [::select-votes/cursor]))
  :ret ::db-spec/sqlvec)

(spec/def ::content-table-name #{"sub_post" "sub_post_comment"})
(spec/def ::vote-table-name #{"sub_post_vote" "sub_post_comment_vote"})
(wrap-snip-fn update-content-scores-snip)
(db-spec/fdef update-content-scores-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid
                                            ::content-table-name]))
  :ret ::db-spec/sqlvec)

(spec/def ::comment-score int?)
(spec/def ::update-scores-snip ::db-spec/sqlvec)
(wrap-db-fn _delete-vote)
(db-spec/fdef _delete-vote
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::content-id
                                            ::user-spec/uid
                                            ::content-table-name
                                            ::vote-table-name
                                            ::primary-key
                                            ::update-scores-snip]))
  :ret (spec/coll-of (spec/keys :req-un [::user-spec/uid
                                         ::user-spec/score
                                         ::user-spec/given
                                         ::user-spec/upvotes-given
                                         ::user-spec/downvotes-given
                                         ::content-score])))

(wrap-db-fn _admin-delete-vote)
(db-spec/fdef _admin-delete-vote
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::vote-table-name
                                            ::primary-key
                                            ::update-scores-snip]))
  :ret (spec/coll-of (spec/keys :req-un [::vote-spec/xid])))

(spec/def ::count-votes/votes int?)
(wrap-db-fn count-votes)
(db-spec/fdef count-votes
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid]))
  :ret (spec/keys :req-un [::count-votes/votes]))

(spec/def ::select-votes/cursor
  (spec/nilable ::element-spec/timestamp))
(spec/def ::select-votes/and-datetime-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-votes/cid (spec/nilable ::comment-spec/cid))
(spec/def ::select-votes/limit int?)
(spec/def ::select-votes/pid (spec/nilable ::post-spec/pid))
(spec/def ::select-votes/positive #{0 1})
(spec/def ::select-votes/types
  (spec/and seq
            (spec/coll-of #{"POST" "COMMENT"})))
(spec/def ::select-votes/datetime ::element-spec/timestamp)
(wrap-db-fn _select-votes)
(db-spec/fdef _select-votes
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/name
                                            ::select-votes/types
                                            ::select-votes/cursor
                                            ::select-votes/limit
                                            ::select-votes/and-datetime-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [::select-votes/pid
                            ::select-votes/cid
                            ::select-votes/positive
                            ::select-votes/datetime])))

(wrap-db-fn select-votes-given-by-name)
(db-spec/fdef select-votes-given-by-name
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [:user-spec/name]))
  :ret (spec/nilable
        (spec/keys :req-un [::user-spec/uid
                            ::user-spec/upvotes-given
                            ::user-spec/downvotes-given])))

(spec/def ::downvote-count/count int?)
(spec/def ::downvote-count/days pos-int?)
(spec/def ::downvote-count/now ::element-spec/timestamp)
(spec/def ::downvote-count/num pos-int?)
(spec/def ::downvote-count/percent nat-int?)
(spec/def ::downvote-count/target ::user-spec/uid)
(spec/def ::downvote-count/voter ::user-spec/uid)
(wrap-db-fn sitewide-downvote-count)
(db-spec/fdef sitewide-downvote-count
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::downvote-count/days
                                            ::downvote-count/now]))
  :ret (spec/keys :req-un [::downvote-count/count]))

(wrap-db-fn per-user-vote-history)
(db-spec/fdef per-user-vote-history
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::downvote-count/voter
                                            ::downvote-count/target
                                            ::downvote-count/days
                                            ::downvote-count/now
                                            ::downvote-count/num]))
  :ret (spec/coll-of
        (spec/keys :req-un [::vote-spec/positive])))

(wrap-db-fn thread-downvote-percent)
(db-spec/fdef thread-downvote-percent
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::post-spec/pid
                                            ::user-spec/uid
                                            ::downvote-count/num]))
  :ret (spec/keys :req-un [::downvote-count/percent]))

(spec/def ::content-score int?)
(spec/def ::content-id (spec/or :post    ::post-spec/pid
                                :comment ::comment-spec/cid))
(spec/def ::primary-key #{"cid" "pid"})
(wrap-db-fn _upsert-vote)
(db-spec/fdef _upsert-vote
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::content-id
                                            ::user-spec/uid
                                            ::vote-spec/positive
                                            ::element-spec/timestamp
                                            ::content-table-name
                                            ::vote-table-name
                                            ::primary-key
                                            ::update-scores-snip]))
  :ret (spec/coll-of (spec/keys :req-un [::user-spec/uid
                                         ::user-spec/score
                                         ::user-spec/given
                                         ::user-spec/upvotes-given
                                         ::user-spec/downvotes-given
                                         ::content-score
                                         ::comment-spec/upvotes
                                         ::comment-spec/downvotes])))

(defn select-votes
  [db {:keys [cursor forward?] :as args}]
  (let [and-datetime-snip (when cursor
                            (if forward?
                              (forward-datetime-snip {:cursor cursor})
                              (reverse-datetime-snip {:cursor cursor})))]
    (_select-votes db (assoc args :and-datetime-snip and-datetime-snip))))

(defn- assoc-update-scores-snip
  [args]
  (assoc args :update-scores-snip (update-content-scores-snip args)))

(defn- prepare-comment-vote-args
  [args]
  (-> args
      (assoc :content-table-name "sub_post_comment"
             :vote-table-name "sub_post_comment_vote"
             :primary-key "cid")
      assoc-update-scores-snip))

(defn- prepare-post-vote-args
  [args]
  (-> args
      (assoc :content-table-name "sub_post"
             :vote-table-name "sub_post_vote"
             :primary-key "pid")
      assoc-update-scores-snip))

(defn upsert-vote
  [db typ args]
  (let [prep-args (if (= :post typ)
                    prepare-post-vote-args
                    prepare-comment-vote-args)]
    (_upsert-vote db (prep-args args))))

(defn delete-vote
  [db typ args]
  (let [prep-args (if (= :post typ)
                    prepare-post-vote-args
                    prepare-comment-vote-args)]
    (_delete-vote db (prep-args args))))

(defn admin-delete-vote
  [db typ args]
  (let [prep-args (if (= :post typ)
                    prepare-post-vote-args
                    prepare-comment-vote-args)]
    (_admin-delete-vote db (prep-args args))))

(comment
  (require 'user)
  (def db (:db user/system))

  (upsert-comment-vote db {:uid "4a40732e-d4e3-4434-9417-faee30ad8695"
                           :cid "18b5a080-3813-4e7e-9740-3e58e9ebc867"
                           :positive 1
                           :timestamp (util/sql-timestamp)})
  (delete-comment-vote db {:uid "4a40732e-d4e3-4434-9417-faee30ad8695"
                           :cid "18b5a080-3813-4e7e-9740-3e58e9ebc867"})
  )
