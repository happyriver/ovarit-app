;; gql/elements/vote/prepare.clj -- Prepare vote arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.vote.prepare
  (:require
   [ovarit.app.gql.elements.vote.query :as query]
   [ovarit.app.gql.protocols.current-user :refer [user]]
   [ovarit.app.gql.resolver-helpers.core :as args :refer [assoc-in*
                                                          get-in*
                                                          fail-verification]]))

(defn prevent-negative-votes-given [vote-path]
  {:name ::prevent-negative-votes-given
   :func
   (fn [{:keys [current-user]} args]
     (let [vote (get-in* args vote-path)]
       (if (or (not= :DOWN vote)
               (pos? (:given (user current-user))))
         args
         (fail-verification args "Too many downvotes"))))})

(defn user-votes-given-by-name
  "Get the uid and vote counts for a user by name."
  [username-path user-path]
  {:name ::user-votes-given-by-name
   :func
   (fn [{:keys [db]} args]
     (let [username (get-in* args username-path)
           user (when username
                  (query/select-votes-given-by-name db {:name username}))]
       (assoc-in* args user-path user)))})
