;; gql/elements/vote/resolve.clj -- Resolve votes for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.vote.resolve
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [close!]]
   [clojure.java.jdbc :as jdbc]
   [clojure.set :as set]
   [com.walmartlabs.lacinia.schema :as lacinia-schema]
   [ovarit.app.gql.elements.comment.prepare :as comment]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.post.prepare :as post]
   [ovarit.app.gql.elements.site.prepare :as site]
   [ovarit.app.gql.elements.sub.prepare :as sub]
   [ovarit.app.gql.elements.user.prepare :as user-prepare]
   [ovarit.app.gql.elements.vote.prepare :as prepare]
   [ovarit.app.gql.elements.vote.query :as query]
   [ovarit.app.gql.elements.vote.tasks :as tasks]
   [ovarit.app.gql.protocols.current-user :refer [get-name get-uid]]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.protocols.notify :as notify]
   [ovarit.app.gql.resolver-helpers.core :as args]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.resolver-helpers.pagination :as pag]
   [ovarit.app.gql.resolver-helpers.stream :as stream]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]
   [taoensso.carmine :as car]))

(defn fixup-vote
  [{:keys [positive] :as vote}]
  (-> vote
      (assoc :direction (get constants/reverse-vote-value-map positive))
      (dissoc :positive)))

(defn- update-triggers
  [context votes page-size]
  ;; We requested an extra item in order to find out if there are more
  ;; available, but don't use that extra item when counting posts
  ;; and comments for the purpose of setting triggers.
  (let [requested-votes (take page-size votes)
        vote-count (count requested-votes)
        comment-vote-count (->> requested-votes
                                (filter :cid)
                                count)
        post-vote-count (- vote-count comment-vote-count)]
    (-> (prom/promise votes)
        (data/update-trigger! context :PostVote/post :post-bucket
                              (data/raise-threshold-fn post-vote-count))
        (data/update-trigger! context :Post/sub :sub-bucket
                              (data/raise-threshold-fn vote-count))
        (data/update-trigger! context :Post/author :user-bucket
                              (data/raise-threshold-fn post-vote-count))
        (data/update-trigger! context :Comment/post :post-bucket
                              (data/raise-threshold-fn comment-vote-count))
        (data/update-trigger! context :Comment/author :user-bucket
                              (data/raise-threshold-fn comment-vote-count))
        (data/update-trigger! context :CommentVote/comment :comment-bucket
                              (data/raise-threshold-fn comment-vote-count)))))

(def votes
  "Resolve the admin query for votes with pagination."
  {:name ::votes
   :prepare [(pag/verify-pagination-not-nested)
             (pag/prep-args 100)]
   :resolve
   (fn [{:keys [db] :as context}
        {:keys [page-size cursor forward? types] :as args} _]
     (let [select-args {:limit (inc page-size)
                        :cursor (when cursor
                                  (pag/timestamp-from-cursor cursor))
                        :forward? forward?
                        :name (:name args)
                        :types (map name types)}
           vote-data (query/select-votes db select-args)
           assoc-cursor #(assoc % :cursor (pag/make-cursor (:datetime %)))
           tag #(lacinia-schema/tag-with-type % (if (some? (:pid %))
                                                  :PostVote
                                                  :CommentVote))
           votes (map (comp fixup-vote assoc-cursor tag) vote-data)
           pagination-depth (list [:Vote page-size])]
       (data/with-superlifter context
         (-> (update-triggers context votes page-size)
             (prom/then
              #(pag/resolve-pagination-v2 % args pagination-depth
                                          {:triggers-set? true}))))))})

;;; Notifications following votes

(defn- publish-user-score-update
  [{:keys [notify] :as context} _ _ {:keys [::author-update]}]
  (log/debug {:update author-update} "notification after vote")
  (notify/publish notify :user-score context author-update))

;; Defer downvoting checks to a job queue that processes them one at
;; a time, so as to avoid concurrency problems.
(defn- send-downvote-notifications
  [{:keys [current-user db job-queue site-config]} {:keys [vote content]} _  _]
  (log/debug {:vote vote :admin-sub (:admin-sub (site-config))}
             "downvote notifications")
  (when (and (= :DOWN vote) (seq (:admin-sub (site-config))))
    (jdbc/with-db-transaction [dbx (:ds db)]
      (job-queue/enqueue! job-queue dbx ::tasks/check-downvote-limits
                          {:uid (get-uid current-user)
                           :username (get-name current-user)
                           :target (:uid content)
                           :pid (:pid content)
                           :sid (:sid content)
                           :comment-vote? (boolean (:cid content))}))))

;;; Mutations

(defn- resolve-cast-vote
  [{:keys [db current-user clock]} typ {:keys [content content-id vote]}]
  (let [result
        (if-let [numeric-vote (constants/vote-value-map vote)]


          (query/upsert-vote db typ {:content-id content-id
                                     :uid (get-uid current-user)
                                     :positive numeric-vote
                                     :timestamp (util/sql-now clock)})
          (query/delete-vote db typ {:content-id content-id
                                     :uid (get-uid current-user)}))
        {:keys [content-score upvotes downvotes]} (first result)]
    (log/info {:typ typ :content-id content-id :vote vote :result result}
              (if vote "Cast vote" "Removed vote"))
    {:score content-score
     :upvotes upvotes
     :downvotes downvotes
     :sid (:sid content)
     ::author-update {:uid (:uid content)
                      :score (->> result
                                  (filter #(= (:uid content) (:uid %)))
                                  first
                                  :score)}}))

(def cast-comment-vote
  "Resolve the cast comment vote mutation."
  {:name ::cast-comment-vote
   :prepare [(prepare/prevent-negative-votes-given :vote)
             (comment/query-comment :cid :content {})
             (args/verify-some [:content :cid] "Not found")
             (args/verify-fn [:content :status] #(= :ACTIVE %)
                             "Comment deleted")
             (site/prevent-self-vote [:content :uid] :comment)
             (sub/query-sub [:content :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
             (post/query-post [:content :pid] :post {})
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [context args  _]
     (resolve-cast-vote context :comment
                        (set/rename-keys args {:cid :content-id})))
   :notify [publish-user-score-update
            send-downvote-notifications]})

(def cast-post-vote
  "Resolve the cast post vote mutation."
  {:name ::cast-post-vote
   :prepare [(prepare/prevent-negative-votes-given :vote)
             (args/convert-arg-to-int :pid)
             (post/query-post :pid :content {})
             (args/verify-some [:content :pid] "Not found")
             (args/verify-fn [:content :deleted] #(= :NOT_DELETED %)
                             "Post deleted")
             (site/prevent-self-vote [:content :uid] :post)
             (sub/query-sub [:content :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
             (args/verify-fn :content #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [context args _]
     (resolve-cast-vote context :post
                        (set/rename-keys args {:pid :content-id})))
   :notify [publish-user-score-update
            send-downvote-notifications]})

(defn- update-key [uid instance]
  (str (symbol ::vote-status) ":" uid ":" instance))

(def update-key-set (str (symbol ::vote-status-set)))

(defn- create-update-key-instance
  [conn]
  (let [instance (util/rand-str 6)]
    (car/wcar conn (car/sadd update-key-set instance))
    instance))

(defn- del-update-key-instances
  [conn uid]
  (let [instances (car/wcar conn (car/smembers update-key-set))]
    (car/wcar conn (mapv #(car/del (update-key uid %)) instances))))

(defn- destroy-update-key-instance
  [conn instance]
  (car/wcar conn (car/srem update-key-set instance)))

(def start-remove-votes
  "Start the process of removing all user votes."
  {:name ::start-remove-votes
   :prepare [(prepare/user-votes-given-by-name :name :user)
             (args/verify-some :user "User not found")]
   :resolve
   (fn [{:keys [conn] :as context} {:keys [user]} _]
     (let [{:keys [uid upvotes-given downvotes-given]} user]
       (del-update-key-instances conn uid)
       (tasks/start-vote-removal context uid (+ upvotes-given downvotes-given))
       nil))})

(def stop-remove-votes
  "Stop the process of removing all user votes."
  {:name ::stop-remove-votes
   :prepare [(user-prepare/query-uid-by-name :name :uid)
             (args/verify-some :uid "User not found")]
   :resolve
   (fn [{:keys [conn] :as context} {:keys [uid]} _]
     (del-update-key-instances conn uid)
     (tasks/stop-vote-removal context uid)
     nil)})

(def update-frequency-ms 500)
(def update-cache-expire-s 1)

(defn- update-on-status-change
  "Call back with an update if there has been a change."
  [conn uid instance callback-fn]
  (let [k (update-key uid instance)
        previous (car/wcar conn (car/get k))
        current (tasks/get-vote-status conn uid)]
    (car/wcar conn (car/set k current :EX update-cache-expire-s))
    (when-not (= previous current)
      (log/debug {:uid uid :instance instance
                  :previous previous :current current} "status changed")
      (callback-fn current))))

(def stream-vote-removal-updates
  "Stream updates to admin removal of votes."
  {:name ::vote-removal
   :prepare [(user-prepare/query-uid-by-name :name :uid)
             (args/verify-some :uid "User not found")]
   :stream
   (fn [{:keys [conn]} {:keys [uid]} callback-fn]
     (let [instance (create-update-key-instance conn)
           action (stream/action-at-intervals
                   #(update-on-status-change conn uid instance callback-fn)
                   update-frequency-ms)]
       #(do
          (log/debug {:uid uid :instance instance} "closing subscription")
          (destroy-update-key-instance conn instance)
          (close! action))))})

(comment
  (require '[user :as u])

  (def conn (-> u/system :cache :conn))
  (car/wcar conn (car/set "foo" "value" :EX 5))
  (car/wcar conn (car/get "foo"))
  (car/wcar conn (car/expire "foo" 20))

  (u/q "mutation ($cid: String!, $vote: VoteDirection) {
          cast_comment_vote(cid: $cid, vote: $vote) {
            score
          }
        }"
       {:cid "ce302258-ffc4-4faf-a476-3bc6846d2066"
        :vote nil}
       "kelseycraig"))
