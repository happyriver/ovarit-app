;; gql/elements/comment/notify.clj -- Comment notification support for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.notify
  (:require
   [ovarit.app.gql.elements.comment.fixup :refer [fixup-comment-fields]]
   [ovarit.app.gql.elements.comment.query :as query]
   [ovarit.app.gql.elements.user.query :as user-query]
   [ovarit.app.gql.elements.user.resolve :as user-resolve]
   [ovarit.app.gql.protocols.bus :as bus]))

(defn- fetch-users-for-comment
  ;; Fetch the user objects, so that each subscription's resolver
  ;; doesn't have to.  Currently only querying for the basic info for
  ;; the user.
  [comment {:keys [db] :as context}]
  (let [checkoff (:ovarit.app.gql.elements.comment.resolve/checkoff comment)
        uids (->> [(:uid comment) (:uid checkoff)]
                  set
                  (remove nil?))
        users (->> (user-query/list-users-by-uid db {:uids uids})
                   (map (fn [u]
                          [(:uid u) (user-resolve/fixup-user u context)]))
                   (into {}))]
    (cond-> comment
      true     (assoc ::user-resolve/user (get users (:uid comment)))
      checkoff (assoc-in [:ovarit.app.gql.elements.comment.resolve/checkoff
                          ::user-resolve/user]
                         (get users (:uid checkoff))))))

(defn publish-comment-update
  "Fetch a comment from the db and publish it."
  [{:keys [bus db] :as context} _ _ {:keys [cid pid]}]
  (let [comment (-> (query/find-comments-by-cid db {:cids [cid]
                                                    :sticky? true
                                                    :author? true
                                                    :history? true
                                                    :checkoff? true})
                    first
                    (fixup-comment-fields context)
                    (fetch-users-for-comment context))]
    (bus/publish bus :ovarit.app.gql.elements.comment.resolve/comment-update
                 (str pid) comment)))
