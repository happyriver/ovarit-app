;; gql/elements/comment/prepare.clj -- Prepare comment arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.prepare
  (:require
   [ovarit.app.gql.elements.comment.fixup :refer [fixup-comment-fields]]
   [ovarit.app.gql.elements.comment.query :as query]
   [ovarit.app.gql.protocols.current-user :refer [get-uid]]
   [ovarit.app.gql.resolver-helpers.core :as args :refer [assoc-in*
                                                          get-in*
                                                          fail-verification]]))

(defn query-comment
  [cid-path comment-path options]
  {:name :query-comment
   :func
   (fn [{:keys [current-user db] :as context} args]
     (let [cid (get-in* args cid-path)
           params (merge {:cids [cid]
                          :uid (get-uid current-user)} options)
           comment (-> (query/find-comments-by-cid db params)
                       first
                       (fixup-comment-fields context))]
       (assoc-in* args comment-path comment)))})

(defn query-ancestors
  [cid-path parent-path ancestor-count-path]
  {:name :query-ancestors
   :func
   (fn [{:keys [db]} args]
     (let [cid (get-in* args cid-path)]
       (if (nil? cid)
         args
         (let [ancestors (query/select-ancestors db {:cid cid})]
           (-> args
               (assoc-in* parent-path (first ancestors))
               (assoc-in* ancestor-count-path (count ancestors)))))))})

(defn verify-parent-matches
  [pid-path parent-comment-path]
  {:name :verify-parent-matches
   :func
   (fn [_ args]
     (let [parent-pid (:pid (get-in* args parent-comment-path))]
       (if (or (nil? parent-pid)
               (not= (get-in* args pid-path) parent-pid))
         (fail-verification args "Not found")
         args)))})
