;; gql/elements/comment/fixup.clj -- Make comment SQL results match schema expectations
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.fixup
  (:require
   [clojure.set :as set]
   [ovarit.app.gql.elements.constants :as constants]))

(defn- status-for-resolver [status]
  (-> status
      constants/reverse-comment-status-map
      {:ACTIVE        :ACTIVE
       :USER_REMOVED  :DELETED_BY_USER
       :MOD_REMOVED   :DELETED_BY_MOD
       :ADMIN_REMOVED :DELETED_BY_ADMIN
       :ADMIN_DELETED :DELETED_BY_ADMIN}))

(defn- fixup-distinguish
  "Return nil for no distinguishing, or :MOD or :ADMIN."
  [val]
  (let [distinguish (constants/reverse-comment-distinguish-map val)]
    (when (not= distinguish :NORMAL) distinguish)))

(defn fixup-comment-fields
  "Make the database comment fields match the query comment fields."
  [comment _env]
  (-> comment
      (update :status status-for-resolver)
      (update :author-status constants/reverse-user-status-map)
      (update :distinguish fixup-distinguish)
      (update :vote constants/reverse-vote-value-map)
      ;; Turn a map with nil values into a nil.
      (update :checkoff #(when (:uid %) %))
      (set/rename-keys
       {:content-history :ovarit.app.gql.elements.comment.resolve/content-history
        :checkoff :ovarit.app.gql.elements.comment.resolve/checkoff})))
