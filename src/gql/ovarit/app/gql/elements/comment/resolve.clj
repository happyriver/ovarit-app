;; gql/elements/comment/resolve.clj -- GraphQL resolvers for comments
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.resolve
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.walk :as walk]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [ovarit.app.gql.elements.comment.fixup :as fixup
    :refer [fixup-comment-fields]]
   [ovarit.app.gql.elements.comment.notify :as comment-notify]
   [ovarit.app.gql.elements.comment.prepare :as prepare]
   [ovarit.app.gql.elements.comment.query :as query]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.post.notify :as post-notify]
   [ovarit.app.gql.elements.post.prepare :as post]
   [ovarit.app.gql.elements.site.prepare :as site]
   [ovarit.app.gql.elements.sub.prepare :as sub]
   [ovarit.app.gql.elements.user.prepare :as user]
   [ovarit.app.gql.elements.user.resolve :as-alias user-resolve]
   [ovarit.app.gql.protocols.bus :as bus]
   [ovarit.app.gql.protocols.current-user :refer [get-uid
                                                  is-admin?
                                                  moderates?]]
   [ovarit.app.gql.protocols.notify :as notify]
   [ovarit.app.gql.protocols.schema :as schema]
   [ovarit.app.gql.resolver-helpers.core :as args]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.resolver-helpers.pagination :refer [pagination
                                                       resolve-pagination
                                                       one-more] :as pag]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher] :as superlifter]))

;; Fetchers

(defn load-comments
  [selectors {:keys [db current-user]}]
  (let [cids (map :cid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:cids cids
                       :uid (get-uid current-user)})]
    (query/find-comments-by-cid db params)))

(def-superfetcher FetchCommentByCid [id]
  (data/make-fetcher-fn :cid load-comments fixup-comment-fields))

(defn- fetch-comment
  "Promise to fetch a comment by cid.
  Return a promesa promise."
  [context select bucket]
  (let [id (data/id-from-fetch-args select)]
    (superlifter/with-superlifter (get-in context [:request :superlifter])
      (superlifter/enqueue! bucket (->FetchCommentByCid id)))))

(defn- fetch-comments
  [context {:keys [first sort-by]} select]
  (let [commentdata (query/list-comments (:db context)
                                         {:first first
                                          :sort-by sort-by}
                                         select)]
    (if (nil? commentdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-comment-fields % context) commentdata))))

;;; Directive support

(defmethod schema/redact-content ::deleted-content
  [_ {:keys [current-user]} {:keys [status uid sid author-status] :as comment}
   roles]
  (let [dissoc-all #(dissoc % :content ::content-history)
        comment (if (= :DELETED author-status)
                  (dissoc comment :author-flair :distinguish)
                  comment)]
    (cond
      (= :ACTIVE status) comment

      (and (= :DELETED_BY_USER status)
           (= :DELETED author-status)) (dissoc-all comment)

      (and (= :DELETED_BY_USER status)
           (= uid (get-uid current-user))) (dissoc-all comment)

      (and (not (roles :IS_ADMIN))
           (not (moderates? current-user sid))) (dissoc-all comment)

      :else comment)))

;;; Fields and query construction

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the comment queries."
  {:sticky?   [:Comment/sticky]
   :author?   [:Comment/author_status
               :Comment/author_flair]
   :vote?     [:Comment/user_attributes]
   :view?     [:Comment/user_attributes]
   :history?  [:Comment/content_history]
   :checkoff? [:Comment/checkoff]})

(defn- wants
  "Determine which parts of the comment query need to be included."
  [{:keys [site-config] :as context}]
  (let [show-history? (:edit-history (site-config))]
    (-> (s/transform [s/MAP-VALS] (fn [fields]
                                    (data/wants-fields? context fields))
                     fields-by-query-section)
        (update :history? #(and % show-history?)))))

;; Resolvers

(def comments-by-reference
  {:name ::comments-by-reference
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} {:keys [uid]}]
     (let [comments (fetch-comments context (one-more args) {:uid uid})
           pagination-depth (cons [:Comment first] (pagination context))]
       (data/with-superlifter context
         (-> (prom/promise comments)
             (data/update-trigger! context :Comment/author :user-bucket
                                   data/raise-threshold-by-count)
             (prom/then
              (fn [result]
                (resolve-pagination result first pagination-depth)))))))})

(defn- resolve-comment-by-cid
  [{:keys [triggers-set?] :as context} cid bucket]
  (let [id (-> (wants context)
               (assoc :cid cid)
               data/id-from-fetch-args)]
    (data/with-superlifter context
      (cond-> (superlifter/enqueue! bucket (->FetchCommentByCid id))
        (not triggers-set?) (data/update-trigger! context :Comment/post
                                                  :post-bucket
                                                  data/inc-threshold)
        (not triggers-set?) (data/update-trigger! context :Comment/author
                                                  :user-bucket
                                                  data/inc-threshold)))))

(defn comment-by-cid
  "Fetch one comment by its cid."
  [context {:keys [cid]} _]
  (resolve-comment-by-cid context cid :immediate))

(defn comment-by-reference
  "Fetch one comment by the cid in the parent object."
  [context _ {:keys [cid]}]
  (when cid
    (resolve-comment-by-cid context cid :comment-bucket)))

(defn comments-by-cids
  "Fetch a list of comments by cid and sid.
  They are all required to belong to the same sub to simplify the
  authorization logic (a mod of one is a mod of all)."
  [{:keys [db current-user] :as context} {:keys [sid cids]} _]
  (if (empty? cids)
    []
    (let [comments (->> (assoc (wants context)
                               :cids cids
                               :uid (get-uid current-user)
                               :sid sid
                               :require-sid? true)
                        (query/find-comments-by-cid db)
                        (map #(fixup-comment-fields % context)))]
      (data/with-superlifter context
        (-> (prom/promise comments)
            (data/update-trigger! context :Comment/author :user-bucket
                                  data/raise-threshold-by-count))))))

(defn- assoc-child-count
  "Add the count of a node's descendants to the node.
  Only count descendants up to and including `level-limit`"
  [{:keys [level children] :as node} level-limit]
  (assoc node :count
         (if (< level level-limit)
           (+ (count children) (apply + (map :count children)))
           0)))

(defn- count-children-up-to-level
  "Add a :count field to each node with the count of the node's descendants.
  Only count descendants whose level is at or below `level-limit`."
  [comment-tree level-limit]
  (walk/postwalk #(if (map? %)
                    (assoc-child-count % level-limit)
                    %)
                 comment-tree))

(defn- fix-tree-node-status
  [status]
  (if (= status (:ACTIVE constants/comment-status-map))
    :ACTIVE
    :DELETED))

(defn- jsonify-tree
  "Produce the JSON representation of a comment tree."
  [tree]
  (->> tree
       (walk/postwalk #(if (map? %)
                         (-> %
                             (select-keys [:cid :checked :children :level :time
                                           :uid :status])
                             (update :status fix-tree-node-status))
                         %))
       cheshire/generate-string))

(defn- list-cids-in-tree
  "Return a flattened list of the cids in a comment tree.
  Do not include cids of any comments below `level` deep."
  [tree level]
  (->> tree
       (walk/postwalk
        #(if (map? %)
           (cond
             (< (:level %) level) (conj (:children %) (:cid %))
             (= (:level %) level) [(:cid %)]
             :else [])
           %))
       flatten))

;; Use COMMENTS to navigate to all comments in the tree.
(s/declarepath COMMENTS)
(s/declarepath COMMENT)
(s/providepath COMMENTS [s/ALL COMMENT])
(s/providepath COMMENT (s/continue-then-stay [:children COMMENTS]))

(defn- prune-to-cid
  "Return the branch of a comment tree containing `cid`.
  Include the parent, if there is one, and all the children of `cid`."
  [tree cid]
  (let [parent (s/select [COMMENTS #((set (map :cid (:children %))) cid)]
                         tree)
        remove-siblings (fn [children]
                          (filter #(= (:cid %) cid) children))]
    ;; Remove the siblings of the comment.
    (if (empty? parent)
      (remove-siblings tree)
      (update-in parent [0 :children] remove-siblings))))

(defn comment-tree-by-reference
  "Resolve a comment tree based on the post in which it is included.
  Include the entire tree as JSON and a list of comment ids for
  `comment-list-by-reference` to resolve.  Comment ids for top level
  comments and their descendants, in sorted order by `sort-by`, up to
  `level` deep, will be added to the list of comment ids, until the
  length of the list exceeds the argument `first`, or there are no
  more top-level comments.  Include the :checked field for each comment
  if the user is a mod or admin.  If `cid` is included, prune the tree
  to include only that comment, its parent and descendants."
  [{:keys [db current-user] :as context}
   {:keys [level sort-by cid] :as args}
   {:keys [pid sid default-sort sticky-cid]}]
  (let [num-to-fetch (:first args)
        include-checked-off? (or (moderates? current-user sid)
                                 (is-admin? current-user))
        tree (query/select-comment-tree db {:pid pid
                                            :sticky-cid sticky-cid
                                            :sort-by (or sort-by
                                                         default-sort)
                                            :checked-off? include-checked-off?
                                            :uid (get-uid current-user)})
        pruned-tree (if cid
                      (prune-to-cid tree cid)
                      tree)
        root-level (if (empty? pruned-tree)
                     0
                     (-> pruned-tree first :level))
        stop-level (+ level root-level)
        comments (loop [remaining (count-children-up-to-level pruned-tree
                                                              stop-level)
                        num 0
                        collected []]
                   (let [node (first remaining)]
                     (if (or (empty? remaining) (>= num num-to-fetch))
                       {:tree-json (jsonify-tree pruned-tree)
                        :sticky-cid sticky-cid
                        :cids (list-cids-in-tree collected stop-level)}
                       (recur (rest remaining)
                              (+ num 1 (:count node))  ; Add 1 to include self
                              (conj collected node)))))
        num (count (:cids comments))]
    (data/with-superlifter context
      (-> (prom/promise comments)
          (data/update-trigger! context :CommentTree/comments :comment-bucket
                                (data/raise-threshold-fn num))))))

(defn comment-list-by-reference
  "Resolve the comments in a comment tree."
  [context _ {:keys [sticky-cid cids]}]
  ;; Since we know the sticky-cid from the parent post query, we don't
  ;; have to ask the database to figure it out which it will do once
  ;; per comment, which can get expensive.
  (let [args (assoc (wants context) :sticky? false)
        set-sticky #(assoc % :sticky (= sticky-cid (:cid %)))
        fetch-one (fn [cid]
                    (fetch-comment context (assoc args :cid cid)
                                   :comment-bucket))]
    (data/with-superlifter context
      (-> (prom/all (map fetch-one cids))
          (prom/then #(map set-sticky %))
          (data/update-trigger! context :Comment/author :user-bucket
                                data/raise-threshold-by-count)))))

(defn user-attributes-by-reference
  "Resolve the comment user attributes.
  Use values fetched by the Comment resolver."
  [_ _ resolved]
  (select-keys resolved [:viewed :vote]))

(comment
  #_(require '[user :as u])

  (require '[clojure.pprint :refer [pprint]])
  (def db (:db u/system))
  (def tree (query/select-comment-tree db {:pid 11018
                                           :sort-by :TOP}))
  (comment-tree-by-reference {:db db}
                             {:sort_by :TOP
                              :count 100
                              :level 3}
                             {:pid 11018})

  (-> (query/select-comment-tree db {:pid 117514
                                     :sort-by :TOP})
      (count-children-up-to-level 50))

  (u/q "query ($cid: String) {comment_by_cid (cid: $cid) {author {name}}}"
       {:cid "2037a163-5ed1-496a-8e19-99bee8767226"})

  (-> "query ($pid: String, $sort_by: CommentSortMethod,
                  $first: Int, $level: Int)
{
post_by_pid(pid: $pid) {
  comment_tree(sort_by: $sort_by, first: $first, level: $level) {
    comments {
      cid
      time
      edited
      author {
        name
      }
      checkoff {
        user {
          name
        }
        time
      }
    }
  }
}
}"
      (u/q {:pid "50122" :sort_by "TOP" :first 10 :level 3}
           "cameronlindsay")
      :body))

(defn content-history-by-reference
  [_ _ resolved]
  (::content-history resolved))

(defn checkoff-by-reference
  [context _ {:keys [::checkoff]}]
  (if (or (nil? checkoff) (::user-resolve/user checkoff))
    checkoff
    (data/with-superlifter context
      (-> (prom/promise checkoff)
          (data/update-trigger! context :CommentCheckoff/user :user-bucket
                                data/inc-threshold)))))

;;; Streamers

(def stream-new-comments
  "Stream new comments, as they are received."
  {:name ::new-comments
   :prepare [(args/convert-arg-to-int :pid)
             (post/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :stream
   (fn [{:keys [bus current-user]} {:keys [pid]} source-stream-callback]
     (log/debug {:pid pid} "starting stream-new-comments")
     (let [callback (fn [{:keys [uid] :as comment}]
                      (log/debug {:cid (:cid comment)
                                  :comment (str comment)}
                                 "streaming a new comment")
                      ;; Remove the self-vote unless the stream
                      ;; subscriber is the same as the author.
                      (-> (if (= uid (get-uid current-user))
                            comment
                            (assoc comment :vote nil))
                          source-stream-callback))
           sub (bus/subscribe bus ::new-comment (str pid) callback)]
       #(bus/close-subscription bus sub)))})

(def stream-comment-updates
  "Stream changes to comments, as they are made."
  {:name ::comment-updates
   :prepare [(args/convert-arg-to-int :pid)
             (post/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :stream
   (fn [{:keys [bus current-user]} {:keys [pid]} source-stream-callback]
     (log/debug {:pid pid} "Starting stream-comment-updates")
     (let [callback (fn [{:keys [uid] :as comment}]
                      (log/debug {:cid (:cid comment)
                                  :comment comment}
                                 "Streaming comment update")
                      ;; Remove the self-vote unless the stream
                      ;; subscriber is the same as the author.
                      (-> (if (= uid (get-uid current-user))
                            comment
                            (assoc comment :vote nil))
                          source-stream-callback))
           sub (bus/subscribe bus ::comment-update (str pid) callback)]
       #(bus/close-subscription bus sub)))})

;;; Mutations

(def create-comment
  "The resolver for the create comment mutation."
  {:name ::create-comment
   :prepare
   [(site/verify-posting-enabled)
    (args/convert-arg-to-int :pid)
    (post/query-post :pid :post {:info? true
                                 :sticky? true
                                 :metadata? true})
    (args/verify-some [:post :pid] "Not found")
    (sub/unless-user-is-mod-or-admin
     [:post :sid]
     (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted" 403))
    (sub/unless-user-is-mod-or-admin
     [:post :sid]
     (args/verify-fn :post #(not (:locked %)) "Post locked" 403))
    (args/verify-fn :post #(not (:is-archived %)) "Post archived")
    (args/ignore-if-nil
     :parent-cid
     (prepare/query-ancestors :parent-cid :parent :ancestors))
    (args/ignore-if-nil
     :parent-cid
     (prepare/verify-parent-matches :pid :parent))
    (args/verify-fn :ancestors (fn [num]
                                 (or (nil? num)
                                     (< num 25))) "Nested too deeply")
    (sub/query-sub [:post :sid] :sub {:info? true
                                      :metadata? true
                                      :banned? true})
    (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
    (post/find-mentions :content :mentions)]
   :resolve
   (fn [{:keys [current-user db site-config] :as context}
        {:keys [post content parent-cid mentions]} _]
     (let [{:keys [pid sid]} post
           self-vote? (:self-vote-comments (site-config))
           params {:pid pid
                   :uid (get-uid current-user)
                   :content content
                   :mentions mentions
                   :parent-cid parent-cid
                   :cid (util/uuid4)
                   :self-vote? self-vote?
                   :pre-check? (moderates? current-user sid)}
           comment (-> (query/insert-comment db params)
                       (fixup-comment-fields context)
                       (assoc :vote (when self-vote? :UP)))]
       (log/info {:cid (:cid comment)} "Created comment")
       (data/with-superlifter context
         (-> (prom/promise comment)
             (data/update-trigger! context :Comment/author :user-bucket
                                   data/inc-threshold)))))
   :notify [post-notify/publish-post-update
            (fn [{:keys [bus]} {:keys [pid]} _ result]
              (bus/publish bus ::new-comment pid result))
            (fn [{:keys [notify] :as context} _ _ result]
              (notify/publish notify :recipient-notification-counts
                              context result))
            (fn [{:keys [notify] :as context} _ _ {:keys [mentioned-uids]}]
              (doseq [uid mentioned-uids]
                (notify/publish notify :recipient-notification-counts
                                context {:receivedby uid})))]})

(def update-comment-content
  "Replace a comment's content with new content.
  Update the comment's edit history.  Return true if a change was made."
  {:name ::update-comment-content
   :prepare [(prepare/query-comment :cid :comment {})
             (args/verify-some [:comment :cid] "Not found")
             (args/verify-fn :comment #(= (:status %) :ACTIVE)
                             "Comment deleted")
             (user/verify-current-user-uid-match [:comment :uid])
             (post/query-post [:comment :pid] :post {:info? true
                                                     :sticky? true})
             (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted")
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")
             (sub/query-sub [:comment :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)]
   :resolve
   (fn [{:keys [current-user db]} {:keys [cid content post]} _]
     (let [is-mod? (moderates? current-user (:sid post))
           updated? (-> db
                        (query/update-comment-content {:cid cid
                                                       :content content
                                                       :is-mod? is-mod?})
                        pos?)]
       (log/info {:cid cid :updated updated?} "Updated comment content")
       updated?))
   :notify
   [(fn [context {:keys [comment]} _ _]
      (comment-notify/publish-comment-update context _ _
                                             {:cid (:cid comment)
                                              :pid (:pid comment)}))]})

(def check-off-comment
  "Check off a comment.
  Return the new checkoff, or the old one if the comment is already checked off."
  {:name ::add-comment-checkoff
   :prepare [(prepare/query-comment :cid :comment {:checkoff? true})
             (args/verify-some [:comment :cid] "Not found")
             (args/verify= [:comment :sid] :sid "Wrong sub")
             (args/verify-fn :comment #(= (:status %) :ACTIVE)
                             "Comment deleted")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [cid comment]} _]
     (or
      (::checkoff comment)
      (let [checkoff (query/insert-checkoff db {:cid cid
                                                :uid (get-uid current-user)})]
        (log/info {:cid cid} "Added comment checkoff")
        checkoff)))
   :notify
   [(fn [context {:keys [comment]} _ _]
      (comment-notify/publish-comment-update context _ _
                                             {:cid (:cid comment)
                                              :pid (:pid comment)}))]})

(def un-check-comment
  "Un-check a comment.
  Return nil if successful, otherwise the old one if it couldn't be deleted."
  {:name ::remove-comment-checkoff
   :prepare [(prepare/query-comment :cid :comment {:checkoff? true})
             (args/verify-some [:comment :cid] "Not found")
             (args/verify= [:comment :sid] :sid "Wrong sub")
             (args/verify-fn :comment #(= (:status %) :ACTIVE)
                             "Comment deleted")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [cid comment]} _]
     (let [checkoff (::checkoff comment)
           uid (get-uid current-user)]
       (cond
         (nil? checkoff) nil
         (not= (:uid checkoff) uid) checkoff
         :else (do
                 (query/delete-checkoff db {:cid cid
                                            :uid uid})
                 (log/info {:cid cid} "Removed comment checkoff")
                 nil))))
   :notify
   [(fn [context {:keys [comment]} _ _]
      (comment-notify/publish-comment-update context _ _
                                             {:cid (:cid comment)
                                              :pid (:pid comment)}))]})

(def distinguish
  "Distinguish or un-distinguish a comment.
   Return the new distinguish value."
  {:name ::distinguish
   :prepare [(prepare/query-comment :cid :comment {})
             (args/verify-some [:comment :cid] "Not found")
             (user/verify-current-user-uid-match [:comment :uid])
             (post/query-post [:comment :pid] :post {:info? true
                                                     :sticky? true})
             (args/verify= :sid [:post :sid])
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [cid sid distinguish]} _]
     (if (or (and (= distinguish :MOD) (not (moderates? current-user sid)))
             (and (= distinguish :ADMIN) (not (is-admin? current-user))))
       (resolve/with-error nil {:message "Not authorized" :status 403})
       (let [distinguish-val ((or distinguish :NORMAL)
                              constants/comment-distinguish-map)]
         (query/update-distinguish db {:cid cid
                                       :distinguish distinguish-val})
         (log/info {:cid cid :distinguish distinguish}
                   "Set/cleared comment distinguish")
         distinguish)))
   :notify
   [(fn [context {:keys [comment]} _ _]
      (comment-notify/publish-comment-update context _ _
                                             {:cid (:cid comment)
                                              :pid (:pid comment)}))]})
