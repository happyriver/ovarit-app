;; gql/elements/comment/spec.clj -- Specs for comments for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.spec
  "Specs for comment SQL query arguments and fields."
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.spec :as element-spec])
  (:require
   [ovarit.app.gql.elements.comment.spec.content-history
    :as-alias content-history-spec]))

(spec/def ::author-flair (spec/nilable string?))
(spec/def ::author-status (-> constants/user-status-map vals set))
(spec/def ::best-score float?)
(spec/def ::cid string?)
(spec/def ::cids (spec/coll-of ::cid))
(spec/def ::content string?)

(spec/def ::content-history-spec/time double?)
(spec/def ::content-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [::content-history-spec/time
                        ::content]))))
(spec/def ::distinguish (spec/nilable (-> constants/comment-distinguish-map
                                          vals set)))
(spec/def ::downvotes int?)
(spec/def ::lastedit (spec/nilable ::element-spec/timestamp))
(spec/def ::parent-cid (spec/nilable string?))
(spec/def ::score int?)
(spec/def ::status (-> constants/comment-status-map vals set))
(spec/def ::sticky boolean?)
(spec/def ::time ::element-spec/timestamp)
(spec/def ::timestamp ::element-spec/timestamp)
(spec/def ::upvotes int?)
(spec/def ::viewed boolean?)
(spec/def ::views zero?)
(spec/def ::vote (spec/nilable (-> constants/vote-value-map vals set)))
