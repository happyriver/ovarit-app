;; gql/elements/comment/query.clj -- SQL queries for comments
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.comment.query
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.comment.spec :as comment-spec]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn
                                              wrap-db-fns
                                              wrap-snip-fn]]
   [ovarit.app.gql.elements.post.spec :as post-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.sql :refer [start-cte-snip
                                        end-cte-snip]]
   [ovarit.app.gql.elements.sub.spec :as sub-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec]
   [ovarit.app.gql.util :as util]))

(hugsql/def-db-fns "sql/comments.sql")

(wrap-snip-fn checked-off-field-snip)
(db-spec/fdef checked-off-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn checked-off-join-snip)
(db-spec/fdef checked-off-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn checked-off-snip)
(db-spec/fdef checked-off-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn author-field-snip)
(db-spec/fdef author-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn author-join-snip)
(db-spec/fdef author-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-response-notification-snip)
(db-spec/fdef comment-response-notification-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cid
                                            ::comment-spec/parent-cid
                                            ::post-spec/pid
                                            ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-order-by-time-snip)
(db-spec/fdef comment-order-by-time-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-order-by-score-snip)
(db-spec/fdef comment-order-by-score-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-where-pid-snip)
(db-spec/fdef comment-where-pid-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-where-parentcid-snip)
(db-spec/fdef comment-where-parentcid-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/parent-cid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn comment-where-uid-snip)
(db-spec/fdef comment-where-uid-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn history-cte-snip)
(db-spec/fdef history-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn history-field-snip)
(db-spec/fdef history-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn history-join-snip)
(db-spec/fdef history-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn delete-checkoff-snip)
(db-spec/fdef delete-checkoff-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn insert-checkoff-snip)
(db-spec/fdef insert-checkoff-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cid
                                            ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn insert-checkoff-field-snip)
(db-spec/fdef insert-checkoff-field-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn insert-checkoff-join-snip)
(db-spec/fdef insert-checkoff-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn update-checkoff-snip)
(db-spec/fdef update-checkoff-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn checkoff-field-snip)
(db-spec/fdef checkoff-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn checkoff-join-snip)
(db-spec/fdef checkoff-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-cid-snip)
(db-spec/fdef sticky-cid-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-false-snip)
(db-spec/fdef sticky-false-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn insert-vote-snip)
(db-spec/fdef insert-vote-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cid
                                            ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(spec/def :mention-notification-cte-snip/mentions
  (spec/and (spec/coll-of string?)
            #(every? (fn [s] (= s (str/lower-case s))) %)))
(wrap-snip-fn mention-notification-cte-snip)
(db-spec/fdef mention-notification-cte-snip
  :args (spec/cat :args
                  (spec/keys :req-un [:mention-notification-cte-snip/mentions
                                      ::comment-spec/cid
                                      ::post-spec/pid
                                      ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn mention-notification-field-snip)
(db-spec/fdef mention-notification-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn mention-notification-join-snip)
(db-spec/fdef mention-notification-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-response-notification-snip)
(db-spec/fdef post-response-notification-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cid
                                            ::post-spec/pid
                                            ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sid-where-snip)
(db-spec/fdef sid-where-snip
  :args (spec/cat :args (spec/keys :req-un [:sub-spec/sid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sort-by-age-snip)
(db-spec/fdef sort-by-age-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sort-by-best-snip)
(db-spec/fdef sort-by-best-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sort-by-top-snip)
(db-spec/fdef sort-by-top-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-field-snip)
(db-spec/fdef sticky-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-join-snip)
(db-spec/fdef sticky-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn view-field-snip)
(db-spec/fdef view-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn view-join-snip)
(db-spec/fdef view-join-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-cte-snip)
(db-spec/fdef vote-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::comment-spec/cids
                                            ::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-field-snip)
(db-spec/fdef vote-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-join-snip)
(db-spec/fdef vote-join-snip
  :ret ::db-spec/sqlvec)

;;; WIP db functions.

(wrap-db-fns insert-test-comment-report
             select-comments)

;;; Specs for db functions.

(wrap-db-fn _delete-checkoff)
(db-spec/fdef _delete-checkoff
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::comment-spec/cid
                                  ::user-spec/uid]))
  :ret nat-int?)

(wrap-db-fn _insert-checkoff)
(db-spec/fdef _insert-checkoff
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::comment-spec/cid
                                  ::user-spec/uid]))
  :ret (spec/and
        #(= 1 (count %))
        (spec/cat :first
                  (spec/keys :req-un [::comment-spec/cid
                                      ::comment-spec/time
                                      ::user-spec/uid]))))

(wrap-db-fn _insert-comment)
(spec/def :_insert-comment/receivedby (spec/nilable string?))
(spec/def :_insert-comment/insert-checkoff-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/insert-checkoff-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/insert-checkoff-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/insert-vote-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/mention-notification-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/mention-notification-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/mention-notification-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_insert-comment/mentioned-uids
  (spec/nilable (spec/coll-of ::user-spec/uid)))
(spec/def :_insert-comment/response-notification-snip ::db-spec/sqlvec)
(spec/def :_insert-comment/self-vote #{0 1})
(db-spec/fdef _insert-comment
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [:_insert-comment/insert-checkoff-snip
                                  :_insert-comment/insert-checkoff-field-snip
                                  :_insert-comment/insert-checkoff-join-snip
                                  :_insert-comment/insert-vote-snip
                                  :_insert-comment/mention-notification-cte-snip
                                  :_insert-comment/mention-notification-field-snip
                                  :_insert-comment/mention-notification-join-snip
                                  :_insert-comment/response-notification-snip
                                  :_insert-comment/self-vote
                                  ::comment-spec/cid
                                  ::comment-spec/content
                                  ::comment-spec/parent-cid
                                  ::post-spec/pid
                                  ::user-spec/uid]))
  :ret (spec/and
        #(= 1 (count %))
        (spec/cat :first
                  (spec/keys :req-un [:_insert-comment/receivedby
                                      ::comment-spec/best-score
                                      ::comment-spec/cid
                                      ::comment-spec/content
                                      ::comment-spec/status
                                      ::comment-spec/sticky
                                      ::comment-spec/downvotes
                                      ::comment-spec/parent-cid
                                      ::post-spec/pid
                                      ::comment-spec/score
                                      ::comment-spec/time
                                      ::user-spec/uid
                                      ::comment-spec/upvotes
                                      ::comment-spec/views]
                             :opt-un [:_insert-comment/mentioned-uids])))
  :fn (spec/and #(= (-> % :ret :first :cid) (-> % :args :args :cid))
                #(= (-> % :ret :first :content) (-> % :args :args :content))
                #(= (-> % :ret :first :parent-cid) (-> % :args :args :parent-cid))
                #(= (-> % :ret :first :pid) (-> % :args :args :pid))
                #(= (-> % :ret :first :uid) (-> % :args :args :uid))))

(wrap-db-fn select-ancestors)
(db-spec/fdef select-ancestors
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::comment-spec/cid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::post-spec/pid
                            ::comment-spec/cid
                            ::comment-spec/status])))

(wrap-db-fn _select-comment-tree)
(spec/def :_select-comment-tree/checked-off-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comment-tree/checked-off-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comment-tree/checked-off-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comment-tree/sort-by-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comment-tree/checked boolean?)
(spec/def :_select-comment-tree/path (spec/coll-of string?))
(spec/def :_select-comment-tree/scores
  (spec/* (spec/or :score int?
                   :best-score float?
                   :new #(= (type %) java.sql.Timestamp))))
(spec/def :_select-comment-tree/sticky (spec/nilable boolean?))
(db-spec/fdef _select-comment-tree
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::post-spec/pid
                                            :_select-comment-tree/sort-by-snip
                                            :_select-comment-tree/checked-off-snip
                                            :_select-comment-tree/checked-off-field-snip
                                            :_select-comment-tree/checked-off-join-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [:_select-comment-tree/path
                            :_select-comment-tree/scores
                            :_select-comment-tree/sticky]
                   :opt-un [:_select-comment-tree/checked])))

(wrap-db-fn _select-comments-by-cid)
(spec/def :_select-comments-by-cid/history-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/vote-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/author-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/checkoff-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/checkoff-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/history-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/sticky-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/sid-where-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/view-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/vote-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/author-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/history-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/sticky-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/view-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid/vote-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def :_select-comments-by-cid.checkoff/time
  (spec/nilable double?))
(spec/def :_select-comments-by-cid.checkoff/uid
  (spec/nilable ::user-spec/uid))
(spec/def :_select-comments-by-cid/checkoff
  (spec/nilable (spec/keys :req-un [:_select-comments-by-cid.checkoff/uid
                                    :_select-comments-by-cid.checkoff/time])))
(db-spec/fdef _select-comments-by-cid
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::comment-spec/cids
                                   ::db-spec/start-cte-snip
                                   ::db-spec/end-cte-snip
                                   :_select-comments-by-cid/history-cte-snip
                                   :_select-comments-by-cid/vote-cte-snip
                                   :_select-comments-by-cid/author-field-snip
                                   :_select-comments-by-cid/checkoff-field-snip
                                   :_select-comments-by-cid/checkoff-join-snip
                                   :_select-comments-by-cid/history-field-snip
                                   :_select-comments-by-cid/sticky-field-snip
                                   :_select-comments-by-cid/view-field-snip
                                   :_select-comments-by-cid/vote-field-snip
                                   :_select-comments-by-cid/author-join-snip
                                   :_select-comments-by-cid/history-join-snip
                                   :_select-comments-by-cid/sticky-join-snip
                                   :_select-comments-by-cid/view-join-snip
                                   :_select-comments-by-cid/vote-join-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [::comment-spec/cid
                            ::comment-spec/content
                            ::comment-spec/distinguish
                            ::comment-spec/downvotes
                            ::comment-spec/lastedit
                            ::comment-spec/parent-cid
                            ::post-spec/pid
                            ::comment-spec/score
                            ::sub-spec/sid
                            ::comment-spec/status
                            ::comment-spec/time
                            ::user-spec/uid
                            ::comment-spec/upvotes]
                   :opt-un [:_select-comments-by-cid/checkoff
                            ::comment-spec/author-flair
                            ::comment-spec/author-status
                            ::comment-spec/content-history
                            ::comment-spec/sticky
                            ::comment-spec/viewed
                            ::comment-spec/vote])))

(wrap-db-fn _update-comment-content)
(spec/def :_update-comment-content/checkoff-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(db-spec/fdef _update-comment-content
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::comment-spec/cid
                                   ::comment-spec/content
                                   :_update-comment-content/checkoff-cte-snip]))
  :ret nat-int?)

(wrap-db-fn update-distinguish)
(db-spec/fdef update-distinguish
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::comment-spec/cid
                                   ::comment-spec/distinguish]))
  :ret nat-int?)

(declare build-comment-tree)
(defn collect-children
  "Collect items matching `want-cid` off the front of the list `leaves`.
  Returned the processed items, turned into a map with keys
  :cid for the comment-id, :level for the depth in the tree,
  and :children for a recursive list of such maps, as well as the
  remaining items which did not match."
  [want-cid leaves fields level]
  (loop [remaining leaves
         selected-fields (select-keys (first leaves) fields)
         result []]
    (let [leaf (first remaining)
          path (:path leaf)
          current-cid (first path)]
      (if (and (seq remaining) (= current-cid want-cid))
        (recur (rest remaining)
               (select-keys (first remaining) fields)
               (if (seq (rest path))
                 (conj result (assoc (first remaining) :path (rest path)))
                 result))
        {:remaining remaining
         :elem (merge {:cid want-cid
                       :level level
                       :children (build-comment-tree result fields (inc level))}
                      selected-fields)}))))

(defn build-comment-tree
  "Convert a list of paths to comments to a tree of comments.
  `_select-comment-tree` returns a list of maps with the keys :path
  and :scores. Each represents a comment by an array of the comment's
  parent ids in order from original parent comment, with the cid of
  the comment at the end.  Convert these into a tree.  The keywords in
  `fields` will be copied from the input nodes to the output nodes,
  and a :level field will be added."
  ([flattened-tree fields] (build-comment-tree flattened-tree fields 0))
  ([flattened-tree fields level]
   (loop [leaves flattened-tree
          result []]
     (if (seq leaves)
       (let [leaf (first leaves)
             want-cid (first (:path leaf))
             {:keys [remaining elem]} (collect-children want-cid leaves
                                                        fields level)]
         (recur remaining (conj result elem)))
       result))))

(defn compare-leaves
  "Compare leaves of a comment tree by score in descending order.
  Use the comment ids in the paths as tiebreakers. Put children
  ahead of parents."
  [a b]
  (loop [x a
         y b]
    (let [{x-path :path
           x-scores :scores} x
          x-cid (first x-path)
          x-score (first x-scores)
          {y-path :path
           y-scores :scores} y
          y-cid (first y-path)
          y-score (first y-scores)
          cid-compare (when (and x-cid y-cid)
                        (compare x-cid y-cid))
          score-compare (when (and x-score y-score)
                          (compare x-score y-score))]
      (cond
        (and (nil? x-cid) (nil? y-cid)) 0
        (and x-cid (nil? y-cid)) -1
        (and (nil? x-cid) y-cid) 1
        (not= 0 score-compare) (- score-compare)
        (not= 0 cid-compare) (- cid-compare)
        :else (recur {:path (rest x-path)
                      :scores (rest x-scores)}
                     {:path (rest y-path)
                      :scores (rest y-scores)})))))

(defn- promote-sticky
  "If there are sticky comments, move them to the front of the list."
  [tree sticky-cids]
  (let [sticky? #(sticky-cids (:cid %))]
    (concat (filter sticky? tree) (filter #(not (sticky? %)) tree))))

(defn select-comment-tree
  "Return the tree of comments as a list of top-level nodes.
  Each node has keys :cid for the comment id and :children for a list
  of child comments, also nodes. If `checked-off?` is provided, include
  an :checked field which is a boolean, true if a checkoff record
  exists for the comment."
  [db {:keys [pid sort-by sticky-cid checked-off?]}]
  (let [sort-by-snip (case sort-by
                       :NEW (sort-by-age-snip)
                       :TOP (sort-by-top-snip)
                       :BEST (sort-by-best-snip {}))
        args {:pid pid
              :sort-by-snip sort-by-snip
              :checked-off-snip (when checked-off?
                                  (checked-off-snip))
              :checked-off-field-snip (when checked-off?
                                        (checked-off-field-snip))
              :checked-off-join-snip (when checked-off?
                                       (checked-off-join-snip))
              :sticky-field-snip (if sticky-cid
                                   (sticky-cid-snip {:cid sticky-cid})
                                   (sticky-false-snip))}
        leaves (->> (_select-comment-tree db args)
                    (sort compare-leaves))
        fields (->> [(when checked-off? :checked) :time :uid :status]
                    (remove nil?))
        sticky-cids (->> leaves
                         (filter :sticky)
                         (map #(last (:path %)))
                         set)]
    (-> (build-comment-tree leaves fields)
        (promote-sticky sticky-cids))))

(defn- comment-cursor-fn
  "Return a function to generate a cursor from a post."
  [sort]
  (condp = sort
    :NEW :time
    :TOP :score
    :HOT :score))

(defn- fix-timestamp
  [elem]
  (update elem :time util/sql-timestamp-from-epoch))

(defn- fix-edit-history-timestamps
  "Fix the edit history timestamps in a comment.
  Since they get packed up in an array of JSON objects, the driver
  doesn't convert timestamps automatically. This accomplishes the
  conversion by having the SQL extract the epochs, returning seconds
  as doubles, and converting them back here."
  [{:keys [content-history] :as comment}]
  (if content-history
    (update comment :content-history #(map fix-timestamp %))
    comment))

(defn- fix-checkoff-timestamp
  "Fix the checkoff timestamp in a comment."
  [{:keys [checkoff] :as comment}]
  (if checkoff
    (update comment :checkoff fix-timestamp)
    comment))

(defn- snips-for-query
  [{:keys [checkoff? author? history? sticky? view? vote?
           require-sid? sid] :as args}]
  {:start-cte-snip (when (or history? vote?) (start-cte-snip))
   :end-cte-snip (when (or history? vote?) (end-cte-snip))
   :checkoff-field-snip (when checkoff? (checkoff-field-snip))
   :checkoff-join-snip (when checkoff? (checkoff-join-snip))
   :author-field-snip (when author? (author-field-snip))
   :author-join-snip (when author? (author-join-snip {}))
   :history-cte-snip (when history? (history-cte-snip args))
   :history-field-snip (when history? (history-field-snip))
   :history-join-snip (when history? (history-join-snip))
   :sticky-field-snip (when sticky? (sticky-field-snip))
   :sticky-join-snip (when sticky? (sticky-join-snip))
   :view-field-snip (when view? (view-field-snip))
   :view-join-snip (when view? (view-join-snip args))
   :vote-cte-snip (when vote? (vote-cte-snip args))
   :vote-field-snip (when vote? (vote-field-snip))
   :vote-join-snip (when vote? (vote-join-snip))
   :sid-where-snip (when require-sid? (sid-where-snip {:sid sid}))})

(defn find-comments-by-cid
  "Find comments from their cids."
  [db {:keys [cids uid] :as args}]
  (->> (snips-for-query args)
       (merge {:cids cids :uid uid})
       (_select-comments-by-cid db)
       (map fix-edit-history-timestamps)
       (map fix-checkoff-timestamp)))

(defn list-comments
  "Return a list of comments. Uses the keys :first, :after and :sort_by in args."
  [db args select]
  (let [sort-by (condp = (:sort-by args)
                  :NEW (comment-order-by-time-snip)
                  :TOP (comment-order-by-score-snip)
                  :HOT (comment-order-by-score-snip))
        limit (:first args)
        where (cond
                (nil? select) nil
                (contains? select :pid) (comment-where-pid-snip select)
                (contains? select :parent-cid) (comment-where-parentcid-snip
                                                select)
                (contains? select :uid) (comment-where-uid-snip select))
        values (select-comments db {:where where
                                    :sort sort-by
                                    :limit limit})
        make-cursor (comment-cursor-fn (:sort-by args))]
    (map #(assoc % :cursor (make-cursor %)) values)))

(defn- mention-snips
  "Construct the snippets for inserting comment mention notifications."
  [{:keys [mentions] :as args}]
  {:mention-notification-cte-snip
   (when (seq mentions)
     (mention-notification-cte-snip
      (select-keys args [:cid :pid :uid :mentions])))

   :mention-notification-field-snip
   (when (seq mentions)
     (mention-notification-field-snip))

   :mention-notification-join-snip
   (when (seq mentions)
     (mention-notification-join-snip))})

(defn- checkoff-snips
  "Construct the snippets for entering a new comment checkoff."
  [{:keys [pre-check? cid uid]}]
  {:insert-checkoff-snip
   (when pre-check?
     (insert-checkoff-snip {:cid cid
                            :uid uid}))
   :insert-checkoff-field-snip
   (when pre-check?
     (insert-checkoff-field-snip {:uid uid}))

   :insert-checkoff-join-snip
   (when pre-check?
     (insert-checkoff-join-snip))})

(defn insert-comment
  [db {:keys [cid parent-cid uid self-vote?] :as args}]
  (let [notify-snip-fn (if parent-cid
                         comment-response-notification-snip
                         post-response-notification-snip)
        notify-snip (-> args
                        (select-keys [:cid :parent-cid :uid :pid])
                        notify-snip-fn)
        self-vote (if self-vote? 1 0)
        vote-snip (when self-vote?
                    (insert-vote-snip {:cid cid
                                       :uid uid}))
        params (-> args
                   (select-keys [:cid :pid :uid :content :parent-cid])
                   (merge (mention-snips args))
                   (merge (checkoff-snips args))
                   (assoc :insert-vote-snip vote-snip
                          :response-notification-snip notify-snip
                          :self-vote self-vote))]
    (-> (_insert-comment db params)
        first)))

(defn update-comment-content
  [db {:keys [cid content is-mod?]}]
  (let [checkoff-cte-snip (if is-mod?
                            (update-checkoff-snip)
                            (delete-checkoff-snip))]
    (_update-comment-content db {:cid cid
                                 :content content
                                 :checkoff-cte-snip checkoff-cte-snip})))

(defn insert-checkoff
  "Add an checkoff record for a comment, if it doesn't already have one."
  [db {:keys [cid uid]}]
  (first (_insert-checkoff db {:cid cid :uid uid})))

(defn delete-checkoff
  "Delete the checkoff record for a comment, if it has one."
  [db {:keys [cid uid]}]
  (_delete-checkoff db {:cid cid :uid uid}))

(comment
  (require 'user)
  (require '[clojure.pprint :refer [pprint]])
  (def db (:db user/system))
  (require 'clojure.walk)

  (->
   (find-comments-by-cid db {:cids ["b11270b6-9ead-48b6-880c-43bc6d85b089"]
                             :checkoff? true}))

  (-> (select-comment-tree db {:pid 178234 :sort-by :BEST :checked-off? true
                               :sticky-cid "a2860722-6dde-43a0-81a6-f7e61b68f5c4"})
      pprint)
  (select-ancestors db {:cid "e6b74ec3-3f4b-4ce3-9920-79d80c07defe"})
  )
