;; gql/elements/message/select.clj -- SQL queries for Message data
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.message.query
  (:require
   [clojure.spec.alpha :as spec]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn wrap-snip-fn]
    :as constants]
   [ovarit.app.gql.elements.message.spec :as message-spec]
   [ovarit.app.gql.elements.report.spec :as report-spec]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.sub.spec :as sub-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec]
   [ovarit.app.gql.util :as util])
  (:require
   [ovarit.app.gql.elements.message.spec.change-mailbox
    :as-alias change-mailbox]
   [ovarit.app.gql.elements.message.spec.count-direct-messages-since
    :as-alias count-direct-messages-since]
   [ovarit.app.gql.elements.message.spec.insert-contact-mods-thread-returning
    :as-alias insert-contact-mods-thread-returning]
   [ovarit.app.gql.elements.message.spec.insert-message-thread-returning
    :as-alias insert-message-thread-returning]
   [ovarit.app.gql.elements.message.spec.insert-modmail-reply-returning
    :as-alias insert-modmail-reply-returning]
   [ovarit.app.gql.elements.message.spec.insert-new-modmail-thread-returning
    :as-alias insert-new-modmail-thread-returning]
   [ovarit.app.gql.elements.message.spec.insert-or-append-admin-notification
    :as-alias insert-or-append-admin-notification]
   [ovarit.app.gql.elements.message.spec.log-entry
    :as-alias log-entry]
   [ovarit.app.gql.elements.message.spec.modmail-message
    :as-alias modmail-message]
   [ovarit.app.gql.elements.message.spec.nilable
    :as-alias nilable]
   [ovarit.app.gql.elements.message.spec.notification-counts
    :as-alias notification-counts]))

(hugsql/def-db-fns "sql/messages.sql")

(spec/def ::latest-posted ::message-spec/posted)
(spec/def ::report-id ::report-spec/id)
(spec/def ::target-uid (spec/nilable ::user-spec/uid))
(spec/def ::unread (spec/nilable ::user-spec/uid))
(spec/def ::nilable/receivedby (spec/nilable ::message-spec/receivedby))
(spec/def ::nilable/sid (spec/nilable ::sub-spec/sid))

(spec/def ::modmail-message/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER
             :USER_NOTIFICATION :MOD_NOTIFICATION :USER_TO_MODS))
      set))
(spec/def ::modmail-message/message
  (spec/keys :req-un [::message-spec/mid
                      ::message-spec/content
                      ::modmail-message/mtype
                      ::message-spec/sentby
                      ::nilable/receivedby
                      ::message-spec/first
                      ::message-spec/mtid
                      ::unread]))
(spec/def ::first-message ::modmail-message/message)
(spec/def ::latest-message ::modmail-message/message)

(spec/def ::reference-thread-id ::message-spec/mtid)
(wrap-snip-fn create-reference-thread-logs-snip)
(db-spec/fdef create-reference-thread-logs-snip
  :args (spec/cat :args
                  (spec/keys :req-un [::report-spec/report-id
                                      ::reference-thread-id
                                      ::message-spec/sentby
                                      ::message-spec/posted]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn create-report-logs-snip)
(db-spec/fdef create-report-logs-snip
  :args (spec/cat :args
                  (spec/keys :req-un [::report-spec/report-id
                                      ::message-spec/receivedby
                                      ::report-spec/report-type]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn admin-sub-snip)
(db-spec/fdef admin-sub-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sub-snip)
(db-spec/fdef sub-snip
  :args (spec/cat :args (spec/keys :req-un [::sub-spec/sid]))
  :ret ::db-spec/sqlvec)

(spec/def ::change-mailbox/updated ::element-spec/timestamp)
(wrap-db-fn change-mailbox)
(db-spec/fdef change-mailbox
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::message-spec/mtid
                                  ::message-spec/mailbox
                                  ::user-spec/uid
                                  ::change-mailbox/updated]))
  :ret int?)

(spec/def ::insert-contact-mods-thread-returning/receivedby
  nil?)
(spec/def ::insert-contact-mods-thread-returning/mtype
  #(= (:USER_TO_MODS constants/message-type-map) %))
(wrap-db-fn insert-contact-mods-thread-returning)
(db-spec/fdef insert-contact-mods-thread-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys
                :req-un [::message-spec/subject
                         ::message-spec/content
                         ::sub-spec/sid
                         ::message-spec/posted
                         ::insert-contact-mods-thread-returning/receivedby
                         ::message-spec/sentby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-contact-mods-thread-returning/mtype
                            ::insert-contact-mods-thread-returning/receivedby
                            ::message-spec/sentby
                            ::sub-spec/sid
                            ::message-spec/mtid])))

(spec/def ::insert-message-thread-returning/mtype
  #(= (:USER_TO_USER constants/message-type-map) %))
(spec/def ::insert-message-thread-returning/sid nil?)
(wrap-db-fn insert-message-thread-returning)
(db-spec/fdef insert-message-thread-returning
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::message-spec/subject
                                  ::message-spec/content
                                  ::message-spec/posted
                                  ::message-spec/sentby
                                  ::message-spec/receivedby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-message-thread-returning/mtype
                            ::insert-message-thread-returning/sid
                            ::message-spec/sentby
                            ::message-spec/receivedby
                            ::message-spec/mtid])))

(spec/def ::insert-modmail-reply-returning/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER))
      set))
(wrap-db-fn insert-modmail-reply-returning)
(db-spec/fdef insert-modmail-reply-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::message-spec/mtid
                                   ::message-spec/content
                                   ::message-spec/posted
                                   ::insert-modmail-reply-returning/mtype
                                   ::sub-spec/sid
                                   ::message-spec/sentby
                                   ::nilable/receivedby
                                   ::message-spec/mtid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-modmail-reply-returning/mtype
                            ::sub-spec/sid
                            ::message-spec/sentby
                            ::nilable/receivedby
                            ::message-spec/mtid])))

(spec/def ::insert-new-modmail-thread-returning/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER
             :USER_NOTIFICATION :MOD_NOTIFICATION))
      set))
(spec/def ::insert-new-modmail-thread-returning/reference-thread-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::insert-new-modmail-thread-returning/report-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::insert-new-modmail-thread-returning/sub-snip
  ::db-spec/sqlvec)
(wrap-db-fn insert-new-modmail-thread-returning)
(db-spec/fdef insert-new-modmail-thread-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys
                :req-un
                [::message-spec/subject
                 ::message-spec/content
                 ::message-spec/posted
                 ::sub-spec/sid
                 ::insert-new-modmail-thread-returning/mtype
                 ::insert-new-modmail-thread-returning/reference-thread-snip
                 ::insert-new-modmail-thread-returning/report-snip
                 ::insert-new-modmail-thread-returning/sub-snip
                 ::message-spec/sentby
                 ::nilable/receivedby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::insert-new-modmail-thread-returning/mtype
                            ::message-spec/posted
                            ::nilable/receivedby
                            ::message-spec/sentby
                            ::sub-spec/sid
                            ::message-spec/mtid])))

(spec/def ::insert-or-append-admin-notification/sid
  (spec/nilable ::sub-spec/sid))
(wrap-db-fn insert-or-append-admin-notification)
(db-spec/fdef insert-or-append-admin-notification
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::message-spec/subject
                           ::message-spec/content
                           ::log-entry/action
                           ::message-spec/posted
                           ::message-spec/uid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/mtid
                            ::insert-or-append-admin-notification/sid])))

(wrap-db-fn make-message-read)
(db-spec/fdef make-message-read
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-message-unread)
(db-spec/fdef make-message-unread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-thread-read)
(db-spec/fdef make-thread-read
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-thread-unread)
(db-spec/fdef make-thread-unread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn select-comment-report)
(db-spec/fdef select-comment-report
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret (spec/keys :req-un [::report-spec/id]))

(wrap-db-fn select-in-progress-modmail-threads)
(db-spec/fdef select-in-progress-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::sub-spec/sids]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mtid
                    ::message-spec/replies
                    ::message-spec/subject
                    ::sub-spec/sid
                    ::message-spec/mailbox
                    ::message-spec/posted
                    ::latest-posted
                    ::first-message
                    ::latest-message])))

(wrap-db-fn select-message-thread)
(db-spec/fdef select-message-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid]))
  :ret (spec/keys :req-un
                  [::message-spec/mtid
                   ::message-spec/replies
                   ::message-spec/subject
                   ::nilable/sid
                   ::message-spec/mtype
                   ::target-uid]))

(wrap-db-fn select-messages-by-mid)
(db-spec/fdef select-messages-by-mid
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mids]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mid
                    ::message-spec/content
                    ::message-spec/mtype
                    ::message-spec/posted
                    ::nilable/receivedby
                    ::message-spec/sentby
                    ::message-spec/first
                    ::message-spec/mtid
                    ::nilable/sid])))

(wrap-db-fn select-messages-in-thread)
(db-spec/fdef select-messages-in-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid
                                            ::element-spec/limit]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mid
                    ::message-spec/content
                    ::message-spec/mtype
                    ::message-spec/posted
                    ::message-spec/sentby
                    ::nilable/receivedby
                    ::message-spec/first
                    ::message-spec/mtid
                    ::nilable/sid
                    ::unread])))

(wrap-db-fn select-notification-messages)
(db-spec/fdef select-notification-messages
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::user-spec/name
                                            ::element-spec/limit]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mid
                    ::message-spec/content
                    ::message-spec/mtype
                    ::message-spec/posted
                    ::message-spec/sentby
                    ::nilable/receivedby
                    ::message-spec/first
                    ::message-spec/mtid
                    ::nilable/sid
                    ::unread])))

(spec/def ::log-entry/action
  (-> constants/message-log-action-map vals set))
(spec/def ::log-entry/time double?)
(spec/def ::log-entry/desc (spec/nilable string?))
(spec/def ::log-entries
  (spec/nilable (spec/coll-of
                 (spec/keys :req-un [::log-entry/action
                                     ::user-spec/uid
                                     ::log-entry/time
                                     ::log-entry/desc]))))
(wrap-db-fn _select-modmail-thread)
(db-spec/fdef _select-modmail-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret (spec/keys :req-un
                  [::message-spec/mtid
                   ::message-spec/replies
                   ::message-spec/subject
                   ::sub-spec/sid
                   ::target-uid
                   ::message-spec/mailbox
                   ::message-spec/posted
                   ::latest-posted
                   ::first-message
                   ::latest-message
                   ::log-entries]))

(wrap-db-fn select-modmail-threads)
(db-spec/fdef select-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtypes
                                            ::sub-spec/sids
                                            ::user-spec/uid
                                            ::message-spec/mailbox
                                            ::element-spec/limit]))
  :ret (spec/coll-of (spec/keys :req-un
                                [::message-spec/mtid
                                 ::message-spec/replies
                                 ::message-spec/subject
                                 ::sub-spec/sid
                                 ::message-spec/posted
                                 ::latest-posted
                                 ::first-message
                                 ::latest-message])))

(spec/def ::notification-counts/messages int?)
(spec/def ::notification-counts/notifications int?)
(spec/def ::notification-counts/unread-modmail-count int?)
(spec/def ::notification-counts/all-unread-modmail-count int?)
(spec/def ::notification-counts/new-unread-modmail-count int?)
(spec/def ::notification-counts/in-progress-unread-modmail-count int?)
(spec/def ::notification-counts/discussion-unread-modmail-count int?)
(spec/def ::notification-counts/notification-unread-modmail-count int?)
(spec/def ::notification-counts/open-report-count int?)
(spec/def ::notification-counts/closed-report-count int?)
(wrap-db-fn select-mods-with-notification-counts)
(db-spec/fdef select-mods-with-notification-counts
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sid]))
  :ret
  (spec/coll-of
   (spec/keys :req-un
              [::user-spec/uid
               ::notification-counts/messages
               ::notification-counts/notifications
               ::notification-counts/open-report-count
               ::notification-counts/closed-report-count
               ::notification-counts/unread-modmail-count
               ::notification-counts/all-unread-modmail-count
               ::notification-counts/new-unread-modmail-count
               ::notification-counts/in-progress-unread-modmail-count
               ::notification-counts/discussion-unread-modmail-count
               ::notification-counts/notification-unread-modmail-count])))

(wrap-db-fn select-new-modmail-threads)
(db-spec/fdef select-new-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sids
                                            ::user-spec/uid
                                            ::element-spec/limit]))
  :ret (spec/coll-of (spec/keys :req-un
                                [::message-spec/mtid
                                 ::message-spec/replies
                                 ::message-spec/subject
                                 ::sub-spec/sid
                                 ::message-spec/posted
                                 ::latest-posted
                                 ::first-message
                                 ::latest-message
                                 ::message-spec/mailbox]))  )

(wrap-db-fn select-post-report)
(db-spec/fdef select-post-report
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::report-spec/report-id
                                            ::sub-spec/sid
                                            ::user-spec/name]))
  :ret (spec/coll-of
        (spec/keys :req-un [::report-spec/id])))

(spec/def ::count-direct-messages-since/num nat-int?)
(wrap-db-fn count-direct-messages-since)
(db-spec/fdef count-direct-messages-since
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::user-spec/uid
                                  ::element-spec/timestamp]))
  :ret (spec/keys :req-un [::user-spec/name
                           ::count-direct-messages-since/num]))

;; for tests only
(wrap-db-fn select-direct-messages-inbox)

(defn find-messages-by-mid
  "Find messages by their mids."
  ;; This doesn't fetch the unread field.
  [db mids]
  (select-messages-by-mid db {:mids mids}))

(defn- fixup-mod-thread-timestamps
  "Fix the message timestamps for a mod thread query.
  The timestamps in :first-message and :last-message are strings due
  to the JSON conversion they go through.  Replace them with the SQL
  timestamps which were also fetched."
  [{:keys [posted latest-posted] :as thread}]
  (when thread
    (-> thread
        (assoc-in [:first-message :posted] posted)
        (assoc-in [:latest-message :posted] latest-posted)
        (dissoc :posted)
        (dissoc :latest-posted))))

(defn fixup-report-timestamps
  "Fix the report timestamps for a mod thread query."
  [{:keys [post-report-datetime comment-report-datetime post-report
           comment-report] :as thread}]
  (cond-> thread
    post-report (assoc-in [:post-report :datetime] post-report-datetime)
    comment-report (assoc-in [:comment-report :datetime]
                             comment-report-datetime)))

(defn get-mod-thread-by-id
  "Get a modmail thread by the thread id."
  [db {:keys [uid thread-id]}]
  (let [fix-time #(update % :time util/sql-timestamp-from-epoch)]
    (-> (_select-modmail-thread db {:uid uid :mtid thread-id})
        fixup-mod-thread-timestamps
        fixup-report-timestamps
        (update :log-entries #(map fix-time %)))))

(defn list-mod-threads
  "Return a list of modmail threads ordered by their most recent message."
  [db {:keys [first after uid sids mtypes mailbox unread-only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread-only unread-only
                :mailbox mailbox ; INBOX or ARCHIVED
                :mtypes mtypes
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (map fixup-mod-thread-timestamps (select-modmail-threads db params))))

(defn list-new-mod-threads
  "Return a list of threads started by users which don't have a reply yet."
  [db {:keys [first after uid sids unread-only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread-only unread-only
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (->> (select-new-modmail-threads db params)
         (map fixup-mod-thread-timestamps))))

(defn list-in-progress-mod-threads
  "Return threads from users to mods with replies and from mods to users. "
  [db {:keys [first after uid sids unread-only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread-only unread-only
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (map fixup-mod-thread-timestamps
         (select-in-progress-modmail-threads db params))))

(defn list-messages-in-thread
  "Return a list of the messages in a conversation."
  [db {:keys [first after uid mtid]}]
  (let [params {:uid uid
                :mtid mtid
                :limit first
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (select-messages-in-thread db params)))

(defn list-notification-messages
  "Return a list of the messages in a notification thread.
  Find the thread by username."
  [db {:keys [first after uid name]}]
  (let [params {:uid uid
                :name name
                :limit first
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (select-notification-messages db params)))

(defn insert-new-modmail-thread
  "Create a new modmail message and thread and other associated records."
  [db {:keys [as-admin? report-type reference-thread-id sid admin-sub-name
              _mtype _subject _content _posted _sentby _receivedby] :as params}]
  (let [sub-snip (if (and as-admin? (seq admin-sub-name))
                   (admin-sub-snip)
                   (sub-snip {:sid sid}))
        report-snip (when report-type
                      (create-report-logs-snip params))
        reference-thread-snip (when reference-thread-id
                                (create-reference-thread-logs-snip params))]
    (->> (assoc params
                :report-snip report-snip
                :reference-thread-snip reference-thread-snip
                :sub-snip sub-snip)
         (insert-new-modmail-thread-returning db))))

(comment
  (require 'user)
  (def db (:db user/system))
  (select-message-thread db {:mtid 55}))
