;; gql/elements/message/spec.clj -- Specs for messages
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.message.spec
  "Specs for message SQL query arguments and fields."
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec]))

(spec/def ::content string?)
(spec/def ::first boolean?)
(spec/def ::mailbox (-> constants/message-mailboxes vals set))
(spec/def ::mid int?)
(spec/def ::mids (spec/coll-of ::mid))
(spec/def ::mtid int?)
(spec/def ::mtype (-> constants/message-type-map vals set))
(spec/def ::mtypes (spec/coll-of ::mtype))
(spec/def ::posted ::element-spec/timestamp)
(spec/def ::receivedby ::user-spec/uid)
(spec/def ::replies int?)
(spec/def ::sentby ::user-spec/uid)
(spec/def ::subject string?)
