;; gql/elements/message/tasks.clj -- Async tasks for Message data
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.message.tasks
  (:require
   [cambium.core :as log]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.message.query :as query]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.protocols.notify :as notify]
   [ovarit.app.gql.util :as util]))

(defn notify-admins
  "Send a notification message to the mods of the designated admin sub."
  [{:keys [db clock notify] :as context}
   {:keys [uid action subject content]}]
  (let [msg (-> db
                (query/insert-or-append-admin-notification
                 {:content content
                  :action (constants/message-log-action-map action)
                  :subject subject
                  :uid uid
                  :posted (util/sql-now clock)})
                first)]
    (when-let [sid (:sid msg)]
      (log/info {:msg msg} "Admin notification sent")
      (notify/publish notify :modmail-notification-counts context
                      {:sid sid}))
    msg))

;; This job is submitted when the user turns the block direct messages
;; option on after having previously turned it off. Sending messages
;; in the interim could indicate abusive behavior, so notify the
;; admins.
(defmethod job-queue/handle-job! ::check-block-dms-abuse
  [job-queue _job-type {:keys [uid datetime] :as _payload}]
  (let [context (-> job-queue
                    (select-keys [:clock :conn :db :tr :notify]))
        tr (:tr context)
        formatted-date (jt/format "yyyy-MM-dd HH:mm:ss"
                                  (jt/zoned-date-time datetime))
        timestamp (jt/sql-timestamp (jt/zoned-date-time datetime))
        {:keys [num name]} (query/count-direct-messages-since
                            (:db context) {:uid uid
                                           :timestamp timestamp})]
    (log/debug {:num num :uid uid :datetime datetime :name name}
               "Checking for block_dms abuse")
    (when (pos? num)
      (notify-admins context
                     {:uid uid
                      :action :DM_BLOCKING_ABUSE
                      :subject (tr "Possible direct message blocking abuse")
                      :content
                      (->
                       (tr "[%s](/u/%s) previously had all direct messages
                           blocked, disabled blocking at %s (GMT), sent %s
                           messages in the interim, and just re-enabled
                           blocking."
                           name name formatted-date num)
                       util/condense-whitespace)}))))

(defn send-mod-action-message
  [{:keys [db clock notify site-config] :as context}
   {:keys [subject content sentby receivedby sid as-admin?]}]
  (let [thread (->> {:subject subject
                     :content content
                     :sentby sentby
                     :receivedby receivedby
                     :sid sid
                     :as-admin? as-admin?
                     :admin-sub-name (:admin-sub (site-config))
                     :posted (util/sql-now clock)
                     :mtype (:USER_NOTIFICATION constants/message-type-map)}
                    (query/insert-new-modmail-thread db)
                    first)]
    (log/info {:mid (:mid thread)
               :sentby sentby
               :receivedby receivedby} "Notified user of mod action")
    (notify/publish notify :recipient-notification-counts
                    context {:receivedby receivedby})))
