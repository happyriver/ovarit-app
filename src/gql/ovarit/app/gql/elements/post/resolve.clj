;; gql/elements/post/resolve.clj -- GraphQL resolvers for posts
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.resolve
  (:require
   [cambium.core :as log]
   [clojure.java.jdbc :as jdbc]
   [clojure.set :as set]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [com.walmartlabs.lacinia.schema :as lacinia-schema]
   [ovarit.app.gql.elements.config.core :as config]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.message.tasks :as message-tasks]
   [ovarit.app.gql.elements.post.fixup :refer [fixup-post-fields]]
   [ovarit.app.gql.elements.post.notify :as post-notify]
   [ovarit.app.gql.elements.post.prepare :as prepare]
   [ovarit.app.gql.elements.post.query :as query]
   [ovarit.app.gql.elements.sub.prepare :as sub]
   [ovarit.app.gql.elements.user.prepare :as user]
   [ovarit.app.gql.protocols.bus :as bus]
   [ovarit.app.gql.protocols.current-user :refer [get-uid is-admin? moderates?]]
   [ovarit.app.gql.protocols.image-manager :as image-manager]
   [ovarit.app.gql.protocols.notify :as notify]
   [ovarit.app.gql.protocols.schema :as schema]
   [ovarit.app.gql.resolver-helpers.core :as args]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.resolver-helpers.pagination :as pag]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher] :as superlifter]))

;; Fetchers and Fixers

(defn load-posts-by-pid
  "Load posts for the superfetcher."
  [selectors {:keys [current-user db]}]
  (let [pids (map :pid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:pids pids
                       :uid (get-uid current-user)})]
    (query/select-posts-by-pid db params)))

(def-superfetcher FetchPostByPid [id]
  (data/make-fetcher-fn :pid load-posts-by-pid fixup-post-fields))

(defn fetch-post
  "Promise to fetch a post by pid."
  [context select bucket]
  (let [id (data/id-from-fetch-args select)]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (->FetchPostByPid id)))))

(defn fetch-posts
  [context args select]
  (let [postdata (query/list-posts (:db context) args select)]
    (if (nil? postdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-post-fields % context) postdata))))

(defn fetch-default-posts
  [{:keys [db current-user] :as context} args]
  (let [uid (get-uid current-user)
        postdata (query/list-default-posts db args uid)]
    (if (nil? postdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-post-fields % context) postdata))))

;;; Schema directive support

;; This has to go before redacting the deletion reason.
(defmethod schema/redact-content ::deleted-content
  [_ {:keys [current-user]} {:keys [status uid sid author-status] :as post}
   roles]
  (let [dissoc-all #(dissoc % :link :title :thumbnail :content ::content-history
                            ::poll-options :poll-open :poll-closes-time
                            :poll-votes :slug)
        dissoc-content #(dissoc % :link :content :thumbnail :poll-options
                                :poll-open :poll-closes-time :poll-votes
                                :slug)
        dissoc-some #(dissoc % :thumbnail :slug)
        post (if (= :DELETED author-status)
               (dissoc post :author-flair :distinguish)
               post)]
    (cond
      (= :ACTIVE status) post

      (and (= :DELETED_BY_USER status)
           (= :DELETED author-status)) (dissoc-all post)

      (and (= :DELETED_BY_USER status)
           (= uid (get-uid current-user))) (dissoc-content post)

      (and (not (roles :IS_ADMIN))
           (not (moderates? current-user sid))) (dissoc-all post)

      :else (dissoc-some post))))

(defmethod schema/redact-content ::deletion-reason
  [_ {:keys [current-user]} {:keys [status uid sid] :as postcomment}
   roles]
  (cond (or (= :ACTIVE status)
            (roles :IS_ADMIN)
            (moderates? current-user sid))
        postcomment

        (and (= uid (get-uid current-user))
             (#{:DELETED_BY_ADMIN :DELETED_BY_MOD} status))
        (assoc postcomment :status :DELETED_BY_MOD)

        (= uid (get-uid current-user))
        postcomment

        :else (assoc postcomment :status :DELETED)))

(defmethod schema/redact-content ::vote-breakdown
  [_ {:keys [site-config current-user]} {:keys [sid] :as postcomment} roles]
  (if (or (roles :IS_ADMIN)
          (moderates? current-user sid)
          (:show-votes (site-config)))
    postcomment
    (dissoc postcomment :upvotes :downvotes)))

;;; Field selection

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the post queries."
  {:info? [:Post/content
           :Post/status
           :Post/distinguish
           :Post/link
           :Post/nsfw
           :Post/posted
           :Post/edited
           :Post/type
           :Post/score
           :Post/upvotes
           :Post/downvotes
           :Post/thumbnail
           :Post/title
           :Post/comment_count
           :Post/author
           :Post/flair
           :Post/slug
           ;; is_archived and default_sort depend on :Post/posted
           :Post/is_archived
           :Post/default_sort
           :Post/comment_tree]
   :title-history? [:Post/title_history]
   :content-history? [:Post/content_history]
   :metadata? [:Post/default_sort
               :Post/comment_tree
               :Post/locked
               :Post/sticky_cid
               :Post/poll_open
               :Post/poll_closes
               :Post/poll_hide_results]
   :open-reports? [:Post/open_reports]
   :poll? [:Post/poll_options
           :Post/poll_votes]
   :poll-vote? [:Post/user_attributes]
   :saved? [:Post/user_attributes]
   :sticky? [:Post/is_archived
             :Post/comment_tree
             :Post/sticky]
   :viewed? [:Post/user_attributes]
   :vote? [:Post/user_attributes]})

(defn- wants
  "Determine which parts of the post query need to be included."
  [{:keys [site-config] :as context}]
  (let [show-history? (:edit-history (site-config))]
    (-> (s/transform [s/MAP-VALS] (fn [fields]
                                    (data/wants-fields? context fields))
                     fields-by-query-section)
        (update :title-history? #(and % show-history?))
        (update :content-history? #(and % show-history?)))))

;;; Resolvers

(def post-by-pid
  {:name ::post-by-pid
   :prepare
   [(args/convert-arg-to-int :pid)]

   :resolve
   (fn [context {:keys [pid]} _]
     (let [params (-> (wants context)
                      (assoc :pid pid))
           id (data/id-from-fetch-args params)]
       (data/with-superlifter context
         (-> (superlifter/enqueue! :immediate (->FetchPostByPid id))
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/inc-threshold)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/inc-threshold)))))})

(defn post-by-reference
  [{:keys [triggers-set?] :as context} _ {:keys [pid]}]
  (when pid
    (let [params (-> (wants context)
                     (assoc :pid pid))
          id (data/id-from-fetch-args params)]
      (data/with-superlifter context
        (cond-> (superlifter/enqueue! :post-bucket (->FetchPostByPid id))
          (not triggers-set?) (data/update-trigger! context :Post/sub
                                                    :sub-bucket
                                                    data/inc-threshold)
          (not triggers-set?) (data/update-trigger! context :Post/author
                                                    :user-bucket
                                                    data/inc-threshold))))))

(def posts-by-reference
  {:name ::posts-by-reference
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} {:keys [sid]}]
     (let [select {:sid sid}
           postdata (fetch-posts context (pag/one-more args) select)
           pagination-depth (list [:Post first])
           raise-threshold (fn [trigger-opts posts]
                             (update trigger-opts :threshold + (count posts)))]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   raise-threshold)
             (data/update-trigger! context :Post/author :user-bucket
                                   raise-threshold)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(def all-posts
  {:name ::all-posts
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} _]
     (let [postdata (fetch-posts context (pag/one-more args) nil)
           pagination-depth (list [:Post first])]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/raise-threshold-by-count)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/raise-threshold-by-count)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(def default-posts
  {:name ::default-posts
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} _]
     (let [postdata (fetch-default-posts context (pag/one-more args))
           pagination-depth (list [:Post first])]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/raise-threshold-by-count)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/raise-threshold-by-count)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(defn content-history-by-reference
  [_ _ resolved]
  (::content-history resolved))

(defn title-history-by-reference
  [context _ resolved]
  (data/with-superlifter context
    (-> (prom/promise (::title-history resolved))
        (data/update-trigger! context :TitleHistory/user :user-bucket
                              data/raise-threshold-by-count))))

(defn user-attributes-by-reference
  [_ _ resolved]
  (select-keys resolved [:is-saved :vote :poll-vote :viewed]))

(defn open-reports-by-reference
  "Resolve the open reports field of the :Post object.
  Only the :id and :rtype fields are implemented."
  [_ _ {:keys [sid ::open-report-ids]}]
  (map (fn [id]
         ;; Include the sid so the schema can determine mod permissions.
         (-> {:id id
              :rtype :POST
              :sid sid}
             (lacinia-schema/tag-with-type :PostReport)))
       open-report-ids))

(defn poll-options-by-reference
  "Resolve the poll options field using data fetched by the outer resolver."
  [_ _ {:keys [::poll-options]}]
  poll-options)

;;; Streamers

(def stream-post-updates
  "Stream updates to a post, as received."
  {:name ::post-updates
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :stream
   (fn [{:keys [bus]} {:keys [pid]} source-stream-callback]
     (log/debug {:pid pid} "starting stream-post-updates")
     (let [callback #(do
                       (log/debug {:pid pid} "streaming post update")
                       (source-stream-callback %))
           sub (bus/subscribe bus ::post-update (str pid) callback)]
       #(bus/close-subscription bus sub)))})

(defn stream-announcement-post-id
  "Stream updates to the current site-wide announcement post."
  [{:keys [notify]} _ source-stream-callback]
  (log/debug {} "starting announcement post streamer")
  (let [add-handler (fn [{:keys [pid] :as payload}]
                      (log/debug {:payload payload}
                                 "Received announcement update message")
                      (source-stream-callback pid))
        rm-handler (fn [_]
                     (log/debug {} "Received delete announcement message")
                     (source-stream-callback ""))
        subs [(notify/subscribe-site notify :announcement add-handler)
              (notify/subscribe-site notify :rmannouncement rm-handler)]]
    (fn []
      (run! #(notify/close-subscription notify %) subs))))

;;; Mutations

(def save-post
  "Add or remove a post from the user's saved posts list."
  {:name ::save-post
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [pid save]} _]
     (let [params {:uid (get-uid current-user)
                   :pid pid}]
       (if save
         (query/insert-user-saved db params)
         (query/delete-user-saved db params))
       (log/info {:pid pid :save save} "Saved or unsaved post")
       save))})

(def set-post-flair
  "Change a post's flair."
  {:name ::change-post-flair
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE)))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")
             (sub/query-sub [:post :sid] :sub {:metadata? true})
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-user-can-set-flair :sub))
             (args/convert-arg-to-int :flair-id)
             (sub/query-post-flairs [:post :sid] :flair-choices)
             (prepare/verify-flair-id-match :flair-id :flair-choices :flair)
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn [:flair :mods-only] false? "Not authorized" 403))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-flair-post-type :flair [:post :type]))]
   :resolve
   (fn [{:keys [db]} {:keys [pid flair]} _]
     (query/change-post-flair db {:pid pid
                                  :flair (:text flair)})
     (log/info {:pid pid :flair (:text flair)} "Set post flair")
     flair)})

(def remove-post-flair
  "Remove a post's flair."
  {:name ::remove-post-flair
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE)))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")
             (sub/query-sub [:post :sid] :sub {:metadata? true})
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-user-can-delete-flair :sub))]
   :resolve
   (fn [{:keys [db]} {:keys [pid]} _]
     (query/change-post-flair db {:pid pid
                                  :flair nil})
     (log/info {:pid pid} "Removed post flair")
     pid)})

(defn- post-link
  "Create the URL for a post.
   TODO: a proper routing table."
  [{:keys [pid slug]} {:keys [name]}]
  (str "/o/" name "/" pid "/" slug))

(defn- short-post-link
  "Create the short URL for a post.
   TODO: a proper routing table."
  [{:keys [pid]} _]
  (str "/p/" pid))

(defn- sub-link
  "Create the URL for a sub.
   TODO: a proper routing table."
  [{:keys [name]}]
  (str "/o/" name))

(defn- self-update-post-title
  "Update a post's title by the author.
  Return a boolean, whether a change was actually made."
  [{:keys [db]} {:keys [post title]}]
  (let [slug (util/slugify title)
        updated? (-> (query/update-post-title db {:pid (:pid post)
                                                  :title title
                                                  :slug slug})
                     pos?)]
    updated?))

(defn- mod-update-post-title-notice
  [{:keys [tr]} post sub reason as-admin?]
  (let [markdown-post-link (tr "[your post](%s)" (post-link post sub))
        link (sub-link sub)
        markdown-sub-link (format "[%s](%s)" link link)]
    (if as-admin?
      (tr "The site administators changed the title of %s in %s. Reason: %s"
          markdown-post-link markdown-sub-link reason)
      (tr "The moderators of %s changed the title of %s. Reason: %s"
          markdown-sub-link markdown-post-link reason))))

(defn- message-user-re-mod-title-edit
  [{:keys [current-user tr] :as context} sub post reason as-admin?]
  (->> {:content (mod-update-post-title-notice context post sub
                                               reason as-admin?)
        :subject (tr "Moderation action: post title changed")
        :receivedby (:uid post)
        :sentby (get-uid current-user)
        :as-admin? as-admin?
        :sid (:sid post)}
       (message-tasks/send-mod-action-message context)))

(defn- mod-update-post-title
  "Update a post's title by a mod or admin.
  Send a message to the author to notify them."
  [{:keys [db current-user] :as context} {:keys [post sub title reason]}]
  (jdbc/with-db-transaction [xds (:ds db)]
    (let [xdb (assoc db :ds xds)
          slug (util/slugify title)
          post-with-slug (assoc post :slug slug)
          as-admin? (boolean
                     (and (is-admin? current-user)
                          (not (moderates? current-user (:sid sub)))))
          updated? (-> xdb
                       (query/update-post-title-by-mod
                        {:pid (:pid post)
                         :uid (:uid post)
                         :mod-uid (get-uid current-user)
                         :title title
                         :slug slug
                         :reason reason
                         :link (post-link post-with-slug sub)
                         :is-admin as-admin?})
                       pos?)]
      (when updated?
        (message-user-re-mod-title-edit (assoc context :db xdb)
                                        sub (assoc post :slug slug)
                                        reason as-admin?))
      updated?)))

(def update-post-title
  "Replace a posts's title.
  Update the post's title edit history.  Return true if a change was
  made."
  {:name ::update-post-title
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted"))
             (args/verify-fn :post #(not= (:status %) :USER_REMOVED,
                                          "Post deleted"))
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (sub/query-sub [:post :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-title-edit-time-limit :post))
             (user/unless-current-user-uid-match
              [:post :uid]
              (args/verify-fn :reason #(> (count %) 3) "Reason required"))]

   :resolve
   (fn [{:keys [current-user] :as context} {:keys [post] :as args} _]
     (let [self? (= (get-uid current-user) (:uid post))
           updated? (if self?
                      (self-update-post-title context args)
                      (mod-update-post-title context args))]
       (log/info {:pid (:pid post) :updated updated?} "Edited post title")
       updated?))

   :notify
   [(fn [context {:keys [pid]} _ _]
      (post-notify/publish-post-update context _ _ {:pid pid}))
    (fn [{:keys [current-user notify] :as context} {:keys [post]} _ _]
      (when (not= (get-uid current-user) (:uid post))
        (doseq [event [:modmail-notification-counts
                       :recipient-notification-counts]]
          (notify/publish notify event context {:sid (:sid post)
                                                :receivedby (:uid post)}))))]})

(def update-post-content
  "Replace a posts's content.
  Update the post's content edit history and last edited timestamp.
  Return true if a change was made."
  {:name ::update-post-content
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted")
             (args/verify-fn :post #(#{:TEXT :POLL} (:type %))
                             "Not a text post")
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")
             (user/verify-current-user-uid-match [:post :uid])
             (sub/query-sub [:post :sid] :sub {:banned? true
                                               :metadata? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
             (prepare/verify-content-length :content :sub :post)]
   :resolve
   (fn [{:keys [db]} {:keys [pid content]} _]
     (let [updated? (-> db
                        (query/update-post-content {:pid pid :content content})
                        pos?)]
       (log/info {:pid pid :updated updated?} "Edited post content")
       updated?))
   :notify
   [(fn [context {:keys [pid]} _ _]
      (post-notify/publish-post-update context _ _ {:pid pid}))]})

(def update-viewed
  "Record the time a post was viewed by the current user."
  {:name ::update-viewed
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [{:keys [current-user db clock]} {:keys [pid]} _]
     (query/update-viewed db {:pid pid
                              :uid (get-uid current-user)
                              :timestamp (util/sql-now clock)})
     (log/info {:pid pid} "Marked post viewed")
     nil)})

(def distinguish
  "Distinguish or un-distinguish a post.
   Return the new distinguish value."
  {:name ::distinguish
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (user/verify-current-user-uid-match [:post :uid])
             (args/verify= :sid [:post :sid])
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [pid sid distinguish]} _]
     (if (or (and (= distinguish :MOD) (not (moderates? current-user sid)))
             (and (= distinguish :ADMIN) (not (is-admin? current-user))))
       (resolve/with-error nil {:message "Not authorized" :status 403})
       (let [distinguish-val ((or distinguish :NORMAL)
                              constants/post-distinguish-map)]
         (query/update-distinguish db {:pid pid
                                       :distinguish distinguish-val})
         (log/info {:pid pid :distinguish distinguish}
                   "Set/cleared post distinguish")
         distinguish)))})

(def set-nsfw
  "Set a post's NSFW tag."
  {:name ::set-nsfw
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(not (:is-archived %)) "Post archived")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [post nsfw]} _]
     (cond
       (not (or (= (get-uid current-user) (:uid post))
                (is-admin? current-user)
                (moderates? current-user (:sid post))))
       (resolve/with-error nil {:message "Not authorized" :status 403})

       (= nsfw (:nsfw post))
       nsfw

       :else
       (do
         (query/set-nsfw db {:pid (:pid post) :nsfw nsfw})
         (log/info {:pid (:pid post) :nsfw nsfw} "Set/cleared post NSFW")
         nsfw)))})

(defn- delete-notif-content
  [{:keys [tr]} {:keys [post sub reason is-mod?]}]
  (let [postlink (format "/o/%s/%s" (:name sub) (:pid post))
        sublink (format "/o/%s" (:name sub))]
    (if is-mod?
      (tr
       (str/join "\n\n"
                 ["The moderators of %s deleted your post %s."
                  "Reason: %s"])
       sublink postlink reason)
      (tr
       (str/join "\n\n"
                 ["The site administrators deleted your post %s from %s."
                  "Reason: %s"])
       postlink sublink reason))))

(defn- message-user-re-mod-removal
  [{:keys [current-user tr] :as context}
   {:keys [post sub _reason is-mod?] :as args}]
  (->> {:subject (tr "Moderation action: post deleted")
        :content (delete-notif-content context args)
        :sid (:sid sub)
        :as-admin? (not is-mod?)
        :sentby (get-uid current-user)
        :receivedby (:uid post)}
       (message-tasks/send-mod-action-message context)))

(def remove-post
  "Remove a post from visibility in a sub.
  The post may still be visible to its author or to mods and admins."
  {::name ::remove-post
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-remove-time-limit :post))
             (sub/query-sub [:post :sid] :sub {})
             (prepare/verify-reason-if-not-author [:post :uid] :reason)]

   :resolve
   (fn [{:keys [clock current-user db] :as context}
        {:keys [pid post reason sub]} _]
     (let [is-mod? (moderates? current-user (:sid post))
           args {:pid pid
                 :sid (:sid sub)
                 :uid (get-uid current-user)
                 :postlink (short-post-link post sub)
                 :timestamp (util/sql-now clock)}]
       (if (= (get-uid current-user) (:uid post))
         (query/remove-by-author db args)
         (jdbc/with-db-transaction [xds (:ds db)]
           (let [xdb (assoc db :ds xds)]
             (query/remove-by-mod-or-admin xdb (assoc args
                                                      :is-mod? is-mod?
                                                      :reason reason))
             (message-user-re-mod-removal (assoc context :db xdb)
                                          {:post post
                                           :sub sub
                                           :reason reason
                                           :is-mod? is-mod?}))))
       (log/info {:pid pid
                  :by-author (= (get-uid current-user) (:uid post))
                  :is-mod is-mod?} "Removed post")))

   :notify [(fn [context {:keys [pid] :as args} _ _]
              (post-notify/publish-post-update context args nil {:pid pid}))
            (fn [{:keys [site-config] :as context} {:keys [pid]} _ _]
              (when  (= (str pid) (:announcement (site-config)))
                (config/clear-cached-announcement context)))]})

(def create-upload-url
  "Create a URL which may be uploaded to."
  {:name ::create-upload-url
   :prepare [(prepare/verify-user-upload-permission)]
   :func
   (fn [{:keys [image-manager] :as context} _ _]
     (log/info {:ctx (keys context)} "create-upload-url")
     ;; Verify that current user can upload
     (image-manager/create-upload-url image-manager))})

(def create-upload-post
  "Create a new upload post"
  {:name ::create-upload-post
   :prepare
   [(prepare/verify-user-upload-permission)
    (prepare/identify-upload :link :fileinfo)
    (args/verify-fn [:fileinfo :content-type]
                    #{"image/gif" "image/jpeg" "image/png"})
    (prepare/verify-not-recently-posted
     [:sub :sid] [:fileinfo :filename])]
   :resolve
   (fn [_ _ _]
     {:pid "91676"})})

(defn create-text-post
  "Create a text post.
  Currently for testing only."
  [{:keys [db current-user clock] :as context}
   {:keys [content title type sid nsfw]} _]
  (let [ptype  (get constants/post-type-map type)
        post (-> (query/insert-test-post db {:content (or content "")
                                             :title title
                                             :slug (util/slugify title)
                                             :ptype ptype
                                             :sid sid
                                             :nsfw nsfw
                                             :posted (util/sql-now clock)
                                             :uid (get-uid current-user)})
                 first
                 (fixup-post-fields context))]
    (data/with-superlifter context
      (-> (prom/promise post)
          (data/update-trigger! context :Post/author :user-bucket
                                data/inc-threshold)
          (data/update-trigger! context :Post/sub :sub-bucket
                                data/inc-threshold)))))

(def create-post
  "Create a new post."
  {:name ::create-post
   :prepare
   [(prepare/verify-sitewide-posting-enabled)
    (sub/query-sub :sid :sub {:banned? true
                              :metadata? true
                              :post-type-config? true})
    (args/verify-some [:sub :sid] "Sub not found")
    (args/verify-fn [:sub :banned] false? "User banned in sub")
    (sub/unless-user-is-mod-or-admin
     [:sub :sid]
     (args/verify-fn [:sub :restricted] false?
                     "Posting restricted to mods only"))
    (sub/unless-user-is-mod-or-admin
     [:sub :sid]
     (prepare/verify-post-type-permitted :sub :type))
    (prepare/verify-content-for-post-type :sub)
    (prepare/verify-link-for-post-type)
    (prepare/verify-user-posting-limits)
    (args/ignore-if-nil
     :flair-id
     (args/convert-arg-to-int :flair-id))
    (sub/unless-user-is-mod-or-admin
     [:sub :sid]
     (prepare/verify-flair-permissions :flair-id :sub))
    (args/ignore-if-nil
     :flair-id
     (sub/query-post-flairs :sid :flair-choices))
    (args/ignore-if-nil
     :flair-id
     (prepare/verify-flair-id-match :flair-id :flair-choices :flair))
    (args/ignore-if-nil
     :flair-id
     (sub/unless-user-is-mod-or-admin
      [:sub :sid]
      (prepare/verify-flair-not-mods-only :flair)))
    (args/ignore-if-nil
     :flair-id
     (sub/unless-user-is-mod-or-admin
      [:post :sid]
      (prepare/verify-flair-post-type :flair [:post :type])))]

   :resolve
   (fn [{:keys [env] :as context} {:keys [type] :as args} resolved]
     (cond
       (= :UPLOAD type)
       (args/run-resolver create-upload-post context args resolved)

       (and (= :test env) (= :TEXT type))
       (args/run-resolver create-text-post context args resolved)))
   :notify []})

(comment
  (require '[user :as u])
  (require '[ovarit.app.gql.elements.sub.query :as squery])
  (require '[ovarit.app.gql.elements.sub.fixup :as sfixup])
  (def db (:db u/system))



  (-> (squery/select-subs-by-sid db {:sids ["1f4dc68d-d7ec-40cf-935c-e77716cd4a33"]
                                     :uid "c516d5a9-2515-45ad-89b4-40e3ec159c86"
                                     :banned? true
                                     :metadata? true
                                     :post-type-config? true})
      (sfixup/fixup-sub-fields {}))


  )

;; test-only mutations

(defn create-test-post
  "Create a post, for testing only."
  [{:keys [db current-user clock] :as context}
   {:keys [content title type sid nsfw]} _]
  (let [ptype  (get constants/post-type-map type)
        post (-> (query/insert-test-post db {:content (or content "")
                                             :title title
                                             :slug (util/slugify title)
                                             :ptype ptype
                                             :sid sid
                                             :nsfw nsfw
                                             :posted (util/sql-now clock)
                                             :uid (get-uid current-user)})
                 first
                 (fixup-post-fields context))]
    (data/with-superlifter context
      (-> (prom/promise post)
          (data/update-trigger! context :Post/author :user-bucket
                                data/inc-threshold)
          (data/update-trigger! context :Post/sub :sub-bucket
                                data/inc-threshold)))))

;; Set the :AUTHOR role based on a report.
(defmethod schema/set-roles ::report-author
  [_ {:keys [auth-info current-user] :as context}]
  (let [reportedby (schema/find-in-auth-info auth-info :uid)]
    (if (get-uid current-user)
      (update context :roles set/union
              (when (= reportedby (get-uid current-user))
                #{:AUTHOR}))
      context)))

(defn create-report
  "Create a report, for testing only."
  [{:keys [db current-user]} {:keys [pid reason]} _]
  (-> (query/insert-test-post-report db {:pid (util/id-from-string pid)
                                         :uid (get-uid current-user)
                                         :datetime (util/sql-timestamp)
                                         :reason reason
                                         :send-to-admin false})
      first
      (lacinia-schema/tag-with-type :PostReport)))

(defn stick-post
  "Make a post sticky, for testing only."
  [{:keys [db]} {:keys [pid sid]} _]
  (jdbc/insert! (:ds db) "sub_metadata" {:sid sid
                                         :key "sticky"
                                         :value (parse-long pid)}))

(defn set-announcement-post
  "Make a post into the site-wide announcement, for testing only."
  [{:keys [db]} {:keys [pid]} _]
  (jdbc/insert! (:ds db) "site_metadata" {:key "announcement"
                                          :value pid}))
