;; gql/elements/post/fixup.clj -- Make post SQL results match schema expectations
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.fixup
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.util :as util]))

(def day-ms (* 24 60 60 1000))

(defn- fixup-distinguish
  "Return nil for no distinguishing, or :MOD or :ADMIN."
  [val]
  (let [distinguish (constants/reverse-post-distinguish-map val)]
    (when (not= distinguish :NORMAL) distinguish)))

(defn- set-enum-fields
  "Convert integers to enums in a post object."
  [{:keys [ptype] :as post}]
  (-> post
      (dissoc :ptype)
      (assoc :type (constants/reverse-post-type-map ptype))
      (update :deleted constants/reverse-post-deleted-map)
      (update :distinguish fixup-distinguish)
      (update :author-status constants/reverse-user-status-map)))

(defn- set-meta-fields
  "Set post fields from the metadata section of the query. "
  [{:keys [meta] :as post}]
  (-> post
      (dissoc :meta)
      (merge meta)
      (assoc :locked (:lock-comments meta))
      (update :sort #(some-> %
                             str/upper-case
                             keyword))
      (util/convert-fields-to-boolean [:hide-results
                                       :locked
                                       :poll-closed])))

(defn- set-poll-fields
  "Set fields of poll posts to conform to the schema."
  [{:keys [type poll-closed poll-closes-time poll-options
           hide-results] :as post}]
  (if-not (= type :POLL)
    post
    (let [closes (when poll-closes-time
                   (-> (parse-long poll-closes-time)
                       (* 1000)))
          now (-> (java.util.Date.)
                  .getTime)
          open? (and (not poll-closed) (or (nil? closes)
                                           (> closes now)))
          options (if (and hide-results open?)
                    (map #(dissoc % :votes) poll-options)
                    poll-options)]
      (-> post
          (assoc :poll-open open?)
          (assoc :poll-closes-time (when closes (str closes)))
          (assoc :ovarit.app.gql.elements.post.resolve/poll-options options)
          (dissoc :poll-closed :poll-options)))))

(defn- set-status
  "Set the status field based on the post's visibility."
  [{:keys [deleted] :as post}]
  (assoc post :status (deleted {:NOT_DELETED   :ACTIVE
                                :USER_REMOVED  :DELETED_BY_USER
                                :MOD_REMOVED   :DELETED_BY_MOD
                                :ADMIN_REMOVED :DELETED_BY_ADMIN
                                :ADMIN_DELETED :DELETED_BY_ADMIN})))

(defn- set-is-archived
  "Set the is-archived field depending on the age of the post."
  [{:keys [site-config clock]} {:keys [pid posted sticky] :as post}]
  (let [{:keys [archive-post-after archive-sticky-posts
                announcement-pid]} (site-config)
        archive-date (jt/with-clock clock
                       (jt/minus (jt/local-date-time)
                                 (jt/days archive-post-after)))]
    (assoc post :is-archived
           (and posted
                (not= (str pid) announcement-pid)
                (or archive-sticky-posts (not sticky))
                (jt/before? (jt/local-date-time posted) archive-date)))))

(defn- set-comment-sort
  "Set the fields describing default comment sort."
  [{:keys [site-config]} {:keys [posted sort] :as post}]
  (if (nil? posted)
    post
    (let [{:keys [best-comment-sort-init]} (site-config)
          best-sort-enabled (> (.getTime posted)
                               best-comment-sort-init)
          sort-option (or sort :BEST)
          sort-checked (if (or best-sort-enabled
                               (not= sort-option :BEST))
                         sort-option
                         :TOP)]
      (assoc post
             :best-sort-enabled best-sort-enabled
             :default-sort sort-checked))))

(defn- add-thumbnail-url
  "Add the URL prefix to the thumbnail field if it is set."
  [{:keys [site-config]} {:keys [thumbnail] :as post}]
  (if (seq thumbnail)
    (update post :thumbnail #(str (:thumbnail-url (site-config)) "/" %))
    post))

(defn- keywordize-vote
  [post]
  (update post :vote constants/reverse-vote-value-map))

(defn fixup-post-fields
  "Make the database post fields match the query post fields."
  [post context]
  (->> (set/rename-keys post {:content-history
                              :ovarit.app.gql.elements.post.resolve/content-history

                              :title-history
                              :ovarit.app.gql.elements.post.resolve/title-history

                              :open-report-ids
                              :ovarit.app.gql.elements.post.resolve/open-report-ids})
       set-enum-fields
       set-meta-fields
       set-poll-fields
       set-status
       (set-is-archived context)
       (set-comment-sort context)
       (add-thumbnail-url context)
       keywordize-vote))
