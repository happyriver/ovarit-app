;; gql/elements/post/query.clj -- SQL queries for posts and comments
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.query
  (:require
   [clojure.spec.alpha :as spec]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn
                                              wrap-snip-fn]
    :as constants]
   [ovarit.app.gql.elements.post.spec :as post-spec]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.sql :refer [start-cte-snip
                                        end-cte-snip]]
   [ovarit.app.gql.elements.sub.spec :as sub-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec])
  (:require
   [ovarit.app.gql.elements.post.spec.change-post-flair
    :as-alias change-post-flair]
   [ovarit.app.gql.elements.post.spec.delete-old-comment-views
    :as-alias delete-old-comment-views]
   [ovarit.app.gql.elements.post.spec.remove-post
    :as-alias remove-post]
   [ovarit.app.gql.elements.post.spec.poll-options
    :as-alias poll-options]
   [ovarit.app.gql.elements.post.spec.select-recent-post-counts
    :as-alias select-recent-post-counts]
   [ovarit.app.gql.elements.post.spec.select-post-by-pid
    :as-alias select-post-by-pid]
   [ovarit.app.gql.elements.post.spec.update-post-title-by-mod
    :as-alias update-post-title-by-mod]))

(hugsql/def-db-fns "sql/posts.sql")

;;; Specs for snippet functions.

(wrap-snip-fn post-order-by-posted-snip)
(db-spec/fdef post-order-by-posted-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-order-by-score-snip)
(db-spec/fdef post-order-by-score-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-order-by-hot-snip)
(db-spec/fdef post-order-by-hot-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-where-active-snip)
(db-spec/fdef post-where-active-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-where-active-by-uid-snip)
(db-spec/fdef post-where-active-by-uid-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-where-active-by-sid-snip)
(db-spec/fdef post-where-active-by-sid-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-where-active-from-subscriptions-snip)
(db-spec/fdef post-where-active-from-subscriptions-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-where-active-from-default-subs-snip)
(db-spec/fdef post-where-active-from-default-subs-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn content-history-cte-snip)
(db-spec/fdef content-history-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn content-history-field-snip)
(db-spec/fdef content-history-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn content-history-join-snip)
(db-spec/fdef content-history-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn delete-by-mod-sublog-entry-snip)
(db-spec/fdef delete-by-mod-sublog-entry-snip
  :args (spec/cat :args
                  (spec/keys :req-un [::user-spec/uid
                                      ::sub-spec/sid
                                      ::remove-post/reason
                                      ::remove-post/postlink
                                      ::remove-post/timestamp]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn info-field-snip)
(db-spec/fdef info-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-cte-snip)
(db-spec/fdef meta-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-field-snip)
(db-spec/fdef meta-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-join-snip)
(db-spec/fdef meta-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn saved-field-snip)
(db-spec/fdef saved-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn saved-join-snip)
(db-spec/fdef saved-join-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-post-field-snip)
(db-spec/fdef sticky-post-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn sticky-post-join-snip)
(db-spec/fdef sticky-post-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn title-history-cte-snip)
(db-spec/fdef title-history-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn title-history-field-snip)
(db-spec/fdef title-history-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn title-history-join-snip)
(db-spec/fdef title-history-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn open-reports-cte-snip)
(db-spec/fdef open-reports-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn open-reports-field-snip)
(db-spec/fdef open-reports-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn open-reports-join-snip)
(db-spec/fdef open-reports-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn poll-cte-snip)
(db-spec/fdef poll-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn poll-field-snip)
(db-spec/fdef poll-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn poll-join-snip)
(db-spec/fdef poll-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn poll-vote-field-snip)
(db-spec/fdef poll-vote-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn poll-vote-join-snip)
(db-spec/fdef poll-vote-join-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn viewed-field-snip)
(db-spec/fdef viewed-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn viewed-join-snip)
(db-spec/fdef viewed-join-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-cte-snip)
(db-spec/fdef vote-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid
                                            ::post-spec/pids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-field-snip)
(db-spec/fdef vote-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn vote-join-snip)
(db-spec/fdef vote-join-snip
  :ret ::db-spec/sqlvec)

;; Some currently test-only functions
(wrap-db-fn select-posts)
(wrap-db-fn insert-test-post)
(wrap-db-fn insert-test-post-report)

(spec/def ::delete-old-comment-views/startpid ::post-spec/pid)
(spec/def ::delete-old-comment-views/batch-size int?)
(spec/def ::delete-old-comment-views/deleted int?)
(spec/def ::delete-old-comment-views/remaining int?)
(spec/def ::delete-old-comment-views/pid (spec/nilable ::post-spec/pid))
(wrap-db-fn delete-old-comment-views)
(db-spec/fdef delete-old-comment-views
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::delete-old-comment-views/startpid
                                  ::delete-old-comment-views/batch-size]))
  :ret (spec/keys :req-un [::delete-old-comment-views/deleted
                           ::delete-old-comment-views/remaining
                           ::delete-old-comment-views/pid]))

(spec/def ::select-recent-post-counts/sub-post-count nat-int?)
(spec/def ::select-recent-post-counts/site-post-count nat-int?)
(wrap-db-fn select-recent-post-counts)
(db-spec/fdef select-recent-post-counts
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::sub-spec/sid]))
  :ret (spec/keys :req-un [::select-recent-post-counts/sub-post-count
                           ::select-recent-post-counts/site-post-count]))

(spec/def ::select-post-by-pid/content-history-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/content-history-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/content-history-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/info-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/meta-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/meta-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/meta-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/open-reports-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/open-reports-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/open-reports-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/poll-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/poll-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/poll-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/saved-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/saved-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/sticky-post-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/sticky-post-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/title-history-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/title-history-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/title-history-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/vote-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/vote-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/vote-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-post-by-pid/open-report-ids
  (spec/nilable (spec/coll-of ::post-spec/report-id)))

(spec/def ::remove-post/deleted
  (-> constants/post-deleted-map
      ((juxt :USER_REMOVED :MOD_REMOVED :ADMIN_REMOVED))
      set))
(spec/def ::remove-post/postlink string?)
(spec/def ::remove-post/reason (spec/nilable string?))
(spec/def ::remove-post/sublog-entry-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::remove-post/timestamp ::element-spec/timestamp)
(wrap-db-fn _remove-post)
(db-spec/fdef _remove-post
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::remove-post/deleted
                                            ::post-spec/pid
                                            ::user-spec/uid
                                            ::remove-post/postlink
                                            ::remove-post/reason
                                            ::remove-post/sublog-entry-snip
                                            ::remove-post/timestamp]))
  :ret int?)

(spec/def ::poll-options/id int?)
(spec/def ::poll-options/text string?)
(spec/def ::poll-options/votes nat-int?)
(spec/def ::select-post-by-pid/poll-vote (spec/nilable int?))
(spec/def ::poll-options
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [::poll-options/id
                        ::poll-options/text
                        ::poll-options/votes]))))
(spec/def ::select-post-by-pid/poll-votes nat-int?)
(wrap-db-fn _select-post-by-pid)
(db-spec/fdef _select-post-by-pid
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::db-spec/start-cte-snip
                                  ::db-spec/end-cte-snip
                                  ::select-post-by-pid/content-history-cte-snip
                                  ::select-post-by-pid/content-history-field-snip
                                  ::select-post-by-pid/content-history-join-snip
                                  ::select-post-by-pid/info-field-snip
                                  ::select-post-by-pid/meta-cte-snip
                                  ::select-post-by-pid/meta-field-snip
                                  ::select-post-by-pid/meta-join-snip
                                  ::select-post-by-pid/open-reports-cte-snip
                                  ::select-post-by-pid/open-reports-field-snip
                                  ::select-post-by-pid/open-reports-join-snip
                                  ::select-post-by-pid/poll-cte-snip
                                  ::select-post-by-pid/poll-field-snip
                                  ::select-post-by-pid/poll-join-snip
                                  ::select-post-by-pid/poll-vote-field-snip
                                  ::select-post-by-pid/poll-vote-join-snip
                                  ::select-post-by-pid/saved-field-snip
                                  ::select-post-by-pid/saved-join-snip
                                  ::select-post-by-pid/sticky-post-field-snip
                                  ::select-post-by-pid/sticky-post-join-snip
                                  ::select-post-by-pid/title-history-cte-snip
                                  ::select-post-by-pid/title-history-field-snip
                                  ::select-post-by-pid/title-history-join-snip
                                  ::select-post-by-pid/viewed-field-snip
                                  ::select-post-by-pid/viewed-join-snip
                                  ::select-post-by-pid/vote-cte-snip
                                  ::select-post-by-pid/vote-field-snip
                                  ::select-post-by-pid/vote-join-snip
                                  ::post-spec/pids]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-spec/uid
                            ::sub-spec/sid
                            ::post-spec/deleted
                            ::post-spec/pid]
                   :opt-un [
                            ;; content-history-field-snip
                            ;; info-field-snip
                            ::post-spec/content
                            ::post-spec/distinguish
                            ::post-spec/link
                            ::post-spec/nsfw
                            ::post-spec/posted
                            ::post-spec/edited
                            ::post-spec/title-edited
                            ::post-spec/ptype
                            ::post-spec/score
                            ::post-spec/upvotes
                            ::post-spec/downvotes
                            ::post-spec/slug
                            ::post-spec/thumbnail
                            ::post-spec/title
                            ::post-spec/comment-count
                            ::post-spec/flair
                            ;; meta-field-snip
                            ::post-spec/meta
                            ;; open-reports-field-snip
                            ::select-post-by-pid/open-report-ids
                            ;; poll-field-snip
                            ::select-post-by-pid/poll-options
                            ::select-post-by-pid/poll-votes
                            ;; poll-vote-field-snip
                            ::select-post-by-pid/poll-vote
                            ;; saved-field-snip
                            ::post-spec/is-saved
                            ;; sticky-post-field-snip
                            ::post-spec/sticky
                            ;; title-history-field-snip
                            ;; viewed-field-snip
                            ::post-spec/viewed
                            ;; vote-field-snip
                            ::post-spec/vote])))

(wrap-db-fn delete-user-saved)
(db-spec/fdef delete-user-saved
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::post-spec/pid])))

(wrap-db-fn insert-user-saved)
(db-spec/fdef insert-user-saved
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::post-spec/pid])))

(spec/def ::change-post-flair/flair
  (spec/nilable string?))
(wrap-db-fn change-post-flair)
(db-spec/fdef change-post-flair
  :args (spec/cat
         :db ::db-spec/db
         :args
         (spec/keys :req-un
                    [::post-spec/pid
                     ::change-post-flair/flair])))

(wrap-db-fn update-viewed)
(db-spec/fdef update-viewed
  :args (spec/cat
         :db ::db-spec/db
         :args
         (spec/keys :req-un
                    [::post-spec/pid
                     ::user-spec/uid
                     ::element-spec/timestamp])))

(wrap-db-fn update-post-content)
(db-spec/fdef update-post-content
  :args (spec/cat
         :db ::db-spec/db
         :args
         (spec/keys :req-un
                    [::post-spec/pid
                     ::post-spec/content]))
  :ret nat-int?)

(wrap-db-fn update-post-title)
(db-spec/fdef update-post-title
  :args (spec/cat
         :db ::db-spec/db
         :args
         (spec/keys :req-un
                    [::post-spec/pid
                     ::post-spec/title
                     ::post-spec/slug]))
  :ret nat-int?)

(spec/def ::update-post-title-by-mod/mod-uid
  ::user-spec/uid)
(spec/def ::update-post-title-by-mod/link
  string?)
(spec/def ::update-post-title-by-mod/reason
  string?)
(spec/def ::update-post-title-by-mod/is-admin
  boolean?)
(wrap-db-fn update-post-title-by-mod)
(db-spec/fdef update-post-title-by-mod
  :args (spec/cat
         :db ::db-spec/db
         :args
         (spec/keys
          :req-un
          [::post-spec/pid
           ::post-spec/title
           ::post-spec/slug
           ::update-post-title-by-mod/mod-uid
           ::update-post-title-by-mod/link
           ::update-post-title-by-mod/reason
           ::update-post-title-by-mod/is-admin]))
  :ret nat-int?)

(wrap-db-fn update-distinguish)
(db-spec/fdef update-distinguish
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::post-spec/pid
                                   ::post-spec/distinguish]))
  :ret nat-int?)

(wrap-db-fn set-nsfw)
(db-spec/fdef set-nsfw
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::post-spec/pid
                                   ::post-spec/nsfw])))

(def empty-snip-map
  (reduce #(assoc %1 %2 nil) {}
          [:start-cte-snip :end-cte-snip
           :info-field-snip
           :content-history-cte-snip :content-history-field-snip
           :content-history-join-snip
           :meta-cte-snip :meta-field-snip :meta-join-snip
           :open-reports-cte-snip :open-reports-field-snip
           :open-reports-join-snip
           :poll-cte-snip :poll-field-snip :poll-join-snip
           :poll-vote-field-snip :poll-vote-join-snip
           :saved-field-snip :saved-join-snip
           :sticky-post-field-snip :sticky-post-join-snip
           :title-history-cte-snip :title-history-field-snip
           :title-history-join-snip
           :viewed-field-snip :viewed-join-snip
           :vote-cte-snip :vote-field-snip :vote-join-snip]))

(defn- snips-for-query
  "Build the snips for a post query depending on the fields wanted."
  [{:keys [uid pids content-history? info? metadata? open-reports?
           poll? poll-vote? saved? sticky? title-history? viewed?
           vote?]}]
  (merge
   empty-snip-map
   (when (or metadata? vote? content-history? title-history? open-reports?
             poll?)
     {:start-cte-snip (start-cte-snip)
      :end-cte-snip   (end-cte-snip)})
   (when info?
     {:info-field-snip (info-field-snip)})
   (when metadata?
     {:meta-cte-snip   (meta-cte-snip {:pids pids})
      :meta-field-snip (meta-field-snip)
      :meta-join-snip  (meta-join-snip)})
   (when poll?
     {:poll-cte-snip   (poll-cte-snip {:pids pids})
      :poll-field-snip (poll-field-snip)
      :poll-join-snip  (poll-join-snip)})
   (when poll-vote?
     {:poll-vote-field-snip (poll-vote-field-snip)
      :poll-vote-join-snip  (poll-vote-join-snip {:uid uid})})
   (when saved?
     {:saved-field-snip (saved-field-snip)
      :saved-join-snip  (saved-join-snip {:uid uid})})
   (when sticky?
     {:sticky-post-field-snip (sticky-post-field-snip)
      :sticky-post-join-snip (sticky-post-join-snip)})
   (when viewed?
     {:viewed-field-snip (viewed-field-snip)
      :viewed-join-snip (viewed-join-snip {:uid uid})})
   (when vote?
     {:vote-cte-snip (vote-cte-snip {:uid uid :pids pids})
      :vote-field-snip (vote-field-snip)
      :vote-join-snip (vote-join-snip)})
   (when content-history?
     {:content-history-cte-snip (content-history-cte-snip {:pids pids})
      :content-history-field-snip (content-history-field-snip)
      :content-history-join-snip (content-history-join-snip)})
   (when title-history?
     {:title-history-cte-snip (title-history-cte-snip {:pids pids})
      :title-history-field-snip (title-history-field-snip)
      :title-history-join-snip (title-history-join-snip)})
   (when open-reports?
     {:open-reports-cte-snip (open-reports-cte-snip {:pids pids})
      :open-reports-field-snip (open-reports-field-snip)
      :open-reports-join-snip (open-reports-join-snip)})))

(defn select-posts-by-pid
  "Find posts from their pids."
  [db {:keys [pids] :as args}]
  (let [params {:pids pids}
        snips (snips-for-query args)]
    (_select-post-by-pid db (merge snips params))))

(defn post-cursor-fn
  "Return a function to generate a cursor from a post."
  [sort]
  (condp = sort
    :NEW :posted
    :TOP :score
    :HOT :score))

(defn post-sort-snip [sort]
  (condp = sort
    :NEW (post-order-by-posted-snip)
    :TOP (post-order-by-score-snip)
    :HOT (post-order-by-hot-snip)))

(defn add-cursors-to-posts
  [posts sort-by]
  (let [make-cursor (post-cursor-fn sort-by)]
    (map #(assoc % :cursor (make-cursor %)) posts)))

(defn list-posts
  "Return a list of posts.
  Uses the keys :first, :after and :sort_by in args."
  [db {:keys [sort-by first]} select]
  (let [where (cond
                (nil? select) (post-where-active-snip)
                (contains? select :uid) (post-where-active-by-uid-snip select)
                (contains? select :sid) (post-where-active-by-sid-snip select))
        posts (select-posts db {:where-snip where
                                :sort-snip (post-sort-snip sort-by)
                                :limit first})]
    (add-cursors-to-posts posts sort-by)))

(defn list-default-posts
  "Return posts from the user's subscriptions or the default subs.
  Uses the keys :first, :after and :sort-by in args."
  [db {:keys [sort-by first]} uid]
  (let [where (if uid
                (post-where-active-from-subscriptions-snip {:uid uid})
                (post-where-active-from-default-subs-snip))
        posts (select-posts db {:where-snip where
                                :sort-snip (post-sort-snip sort-by)
                                :limit first})]
    (add-cursors-to-posts posts sort-by)))

(defn remove-by-author
  "Mark a post as removed by its author."
  [db {:keys [pid timestamp uid postlink]}]
  (_remove-post db {:pid pid
                    :timestamp timestamp
                    :uid uid
                    :postlink postlink
                    :reason nil
                    :deleted (:USER_REMOVED constants/post-deleted-map)
                    :sublog-entry-snip nil}))

(defn remove-by-mod-or-admin
  "Mark a post as removed by mod or admin."
  [db {:keys [is-mod? pid uid sid reason postlink timestamp]}]
  (let [sublog-entry-snip (-> {:uid uid
                               :sid sid
                               :reason reason
                               :postlink postlink
                               :admin (not is-mod?)
                               :timestamp timestamp}
                              delete-by-mod-sublog-entry-snip)]
    (->> {:deleted (-> (if is-mod? :MOD_REMOVED :ADMIN_REMOVED)
                       constants/post-deleted-map)
          :pid pid
          :uid uid
          :postlink postlink
          :reason reason
          :timestamp timestamp
          :sublog-entry-snip sublog-entry-snip}
         (_remove-post db))))

(comment
  (require 'user)
  (def db (:db user/system))
  (->
   (select-posts-by-pid db {:pids [2016]
                            :info? true
                            :vote? true
                            :uid "d92ad898-4d1b-4d24-ab50-d6d9612af56a"})))
