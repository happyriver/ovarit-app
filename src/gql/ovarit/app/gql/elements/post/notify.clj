;; gql/elements/post/notify.clj -- Post notification support for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.notify
  (:require
   [cambium.core :as log]
   [ovarit.app.gql.elements.post.fixup :refer [fixup-post-fields]]
   [ovarit.app.gql.elements.post.query :as query]
   [ovarit.app.gql.protocols.bus :as bus]))

(defn publish-post-update
  "Fetch a post from the db and publish it."
  [{:keys [bus db] :as context} _ _ {:keys [pid]}]
  (let [post (-> (query/select-posts-by-pid db {:pids [pid]
                                                :info? true
                                                :metadata? true
                                                :sticky? true})
                 first
                 (fixup-post-fields context))]
    (log/debug {:post (select-keys post [:pid :status :score :upvotes :downvotes
                                         :comment_count :locked])}
               "Publishing post update to bus")
    (bus/publish bus :ovarit.app.gql.elements.post.resolve/post-update
                 (str pid) post)))
