;; gql/elements/post/prepare.clj -- Prepare post arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.prepare
  (:require
   [com.rpl.specter :as s]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.post.fixup :refer [fixup-post-fields]]
   [ovarit.app.gql.elements.post.query :as query]
   [ovarit.app.gql.protocols.current-user :refer [get-uid is-admin? user]]
   [ovarit.app.gql.protocols.image-manager :as image-manager]
   [ovarit.app.gql.resolver-helpers.core :as args :refer [assoc-in*
                                                          get-in*
                                                          fail-verification]]
   [ovarit.app.gql.util :as util])
  (:require
   [ovarit.app.gql.elements.sub :as-alias sub]))

(defn query-post
  "Fetch a post object and add it to `args` at `post-path`."
  [pid-path post-path options]
  {:name ::query-post
   :func
   (fn [{:keys [current-user db] :as context} args]
     (let [post-id (get-in* args pid-path)
           params (merge {:pids [post-id]
                          :uid (get-uid current-user)
                          ;; These two make is-archived work.
                          :info? true
                          :sticky? true} options)
           post (some-> (query/select-posts-by-pid db params)
                        first
                        (fixup-post-fields context))]
       (assoc-in* args post-path post)))})

(defn find-mentions
  "Find user mentions in the content and attach their names at `mention-path`."
  [content-path mention-path]
  {:name ::find-mentions
   :func
   (fn [{:keys [current-user]} args]
     (let [level (:level (user current-user))
           allowed-num (cond
                         (> level 30) 15
                         (> level 20) 10
                         :else        5)]
       (->> (get-in* args content-path)
            util/mentions
            (take allowed-num)
            (assoc-in* args mention-path))))})

(defn verify-user-can-set-flair
  "Ensure that users are allowed to add flair in the sub."
  [sub-path]
  {:name ::verify-user-can-set-flair
   :func
   (fn [_ args]
     (let [sub (get-in* args sub-path)
           {user-must-flair? :user-must-flair
            user-can-flair? :user-can-flair} sub]
       (if (or user-must-flair? user-can-flair?)
         args
         (fail-verification args "Not authorized" 403))))})

(defn verify-user-can-delete-flair
  "Verify that users are allowed to remove flair in the sub."
  [sub-path]
  {:name ::verify-can-delete-flair
   :func
   (fn [_ args]
     (let [sub (get-in* args sub-path)]
       (if (:user-must-flair sub)
         (fail-verification args "Not authorized" 403)
         args)))})

(defn verify-flair-id-match
  "Verify that the flair id exists in the list of flairs.
  If found, add it to `args` at `flair-path`."
  [flair-id-path flair-choices-path flair-path]
  {:name ::verify-flair-id-match
   :func
   (fn [_ args]
     (let [flair-id (get-in* args flair-id-path)
           flair (->> (get-in* args flair-choices-path)
                      (filter #(= flair-id (:id %)))
                      first)]
       (if flair
         (assoc-in* args flair-path flair)
         (fail-verification args "Flair not found"))))})

(defn verify-flair-not-mods-only
  "Verify that the flair provided is not marked mods-only."
  [flair-path]
  {:name ::verify-flair-not-mods-only
   :func
   (fn [_ args]
     (let [flair (get-in* args flair-path)]
       (if (:mods-only flair)
         (fail-verification args "Flair not permitted")
         args)))})

(defn verify-flair-permissions
  "Check that flairs are provided or not when and if required."
  [flair-id-path sub-path]
  {:name ::verify-flair-permissions
   :func
   (fn [_ args]
     (let [flair-id (get-in* args flair-id-path)
           sub (get-in* args sub-path)
           {user-must-flair? :user-must-flair
            user-can-flair? :user-can-flair} sub]
       (cond
         (and (nil? flair-id) user-must-flair?)
         (fail-verification args "Flair required")

         (and flair-id
              (not (or user-must-flair? user-can-flair?)))
         (fail-verification args "Flair not permitted")

         :else
         args)))})

(defn verify-flair-post-type
  "Verify that the post type is permitted for the flair."
  [flair-path post-type-path]
  {:name ::verify-flair-post-type
   :func
   (fn [_ args]
     (let [disallowed? (->> (get-in* args flair-path)
                            :disallowed-ptypes
                            (map constants/reverse-post-type-map)
                            set)
           type (get-in* args post-type-path)]
       (if (disallowed? type)
         (fail-verification args "Flair not permitted")
         args)))})

(defn verify-title-edit-time-limit
  "Fail verification if outside the configured title edit time limit."
  [post-path]
  {:name ::verify-title-edit-time-limit
   :func
   (fn [{:keys [site-config clock]} args]
     (let [posted (-> (get-in* args post-path) :posted)
           timeout (:title-edit-timeout (site-config))
           time-limit (jt/with-clock clock
                        (jt/minus (jt/local-date-time)
                                  (jt/seconds timeout)))]
       (if (jt/before? (jt/local-date-time posted) time-limit)
         (fail-verification args "Time expired")
         args)))})

(defn verify-content-length
  "Fail verification if content length is not in configured limits."
  [content-path sub-path post-path]
  {:name ::verify-content-length-minimum
   :func
   (fn [{:keys [site-config]} args]
     (let [content-length (-> (get-in* args content-path)
                              count)
           site-minimum (:text-post-min-length (site-config))
           site-maximum (:text-post-max-length (site-config))
           sub-minimum (-> (get-in* args sub-path)
                           :text-post-min-length)
           post-type (-> (get-in* args post-path)
                         :type)]
       (cond
         (and (= post-type :TEXT)
              (or (< content-length site-minimum)
                  (and sub-minimum (< content-length sub-minimum))))
         (fail-verification args "Content too short")

         (> content-length site-maximum)
         (fail-verification args "Content too long")

         :else
         args)))})

(defn verify-reason-if-not-author
  "Fail verification if non-author does not supply a reason for deletion."
  [author-path reason-path]
  {:name ::verify-reason-if-not-author
   :func
   (fn [{:keys [current-user]} args]
     (let [author-uid (get-in* args author-path)
           reason (get-in* args reason-path)]
       (if (= (get-uid current-user) author-uid)
         args
         (cond
           (empty? reason)
           (fail-verification args "Reason required")

           (< (count reason) 3)
           (fail-verification args "Reason too short")

           (> (count reason) 1024)
           (fail-verification args "Reason too long")

           :else
           args))))})

(defn verify-remove-time-limit
  "Fail verification if the timeout for removal has expired."
  [post-path]
  {:name ::verify-remove-time-limit
   :func
   (fn [{:keys [site-config clock]} args]
     (let [{:keys [posted type]} (get-in* args post-path)
           timeout (:link-post-remove-timeout (site-config))
           time-limit (jt/with-clock clock
                        (jt/minus (jt/local-date-time)
                                  (jt/seconds timeout)))]
       (if (and (= type :LINK)
                (pos-int? timeout)
                (jt/before? (jt/local-date-time posted) time-limit))
         (fail-verification args "Time expired")
         args)))})

(defn verify-user-upload-permission
  "Fail verification if the current user isn't permitted to upload files. "
  []
  {:name ::verify-user-upload-permission
   :func
   (fn [{:keys [current-user site-config]} args]
     (let [{:keys [allow-uploads upload-min-level]} (site-config)]
       (if (or (is-admin? current-user)
               (and allow-uploads
                    (>= (:level current-user) upload-min-level)))
         args
         (fail-verification args "Not authorized" 403))))})

(defn verify-sitewide-posting-enabled
  "Fail verification if posting is disabled, site-wide.
  Admins are exempt."
  []
  {:name ::verify-sitewide-posting-enabled
   :func
   (fn [{:keys [current-user site-config]} args]
     (if (or (:enable-posting (site-config))
             (is-admin? current-user))
       args
       (fail-verification args "Posting disabled" 403)))})

(defn verify-post-type-permitted
  "Fail verification if the post type is not permitted in the sub."
  [sub-path post-type-path]
  {:name ::verify-post-type-permitted
   :func
   (fn [_ args]
     (let [post-type (get-in* args post-type-path)
           sub (get-in* args sub-path)]
       (if (s/select-one [::sub/post-type-config
                          s/ALL
                          #(= post-type (:post-type %))
                          :mods-only] sub)
         (fail-verification args "Post type not permitted")
         args)))})

(defn verify-content-for-post-type
  "Check for post content presence and length.
  Fail verification if it is inappropriate for post type."
  [sub-path]
  {:name ::verify-content-for-post-type
   :func
   (fn [{:keys [site-config]} args]
     (let [post-type (:type args)
           content-length (-> args :content count)
           site-minimum (:text-post-min-length (site-config))
           site-maximum (:text-post-max-length (site-config))
           sub-minimum (-> (get-in* args sub-path) :text-post-min-length)]
       (cond
         (#{:UPLOAD :LINK} post-type)
         args

         ;; Polls are allowed to omit the content.
         (and (= post-type :POLL) (zero? content-length))
         args

         (or (< content-length site-minimum)
             (and sub-minimum (< content-length sub-minimum)))
         (fail-verification args "Content too short")

         (> content-length site-maximum)
         (fail-verification args "Content too long")

         :else
         args)))})

(defn verify-link-for-post-type
  "Verify that a link is provided and parsable for link and upload posts."
  []
  {:name ::verify-link-for-post-type
   :func
   (fn [_ {:keys [type link] :as args}]
     (if (or (#{:TEXT :POLL} type) (util/valid-protocol? link))
       args
       (fail-verification args "Invalid link")))})

(defn verify-user-posting-limits
  "Fail verification if the user has exceeded their posting limits."
  []
  {:name ::verify-user-posting-limits
   :func
   (fn [{:keys [db clock current-user site-config]} {:keys [sid] :as args}]
     (let [{:keys [daily-site-posting-limit
                   daily-sub-posting-limit]} (site-config)
           {:keys [sub-post-count site-post-count]}
           (query/select-recent-post-counts db {:uid (get-uid current-user)
                                                :sid sid
                                                :now (util/sql-now clock)})]
       (if (or (>= sub-post-count daily-sub-posting-limit)
               (>= site-post-count daily-site-posting-limit))
         (fail-verification args "Too many recent posts")
         args)))})

(defn identify-upload
  "Identify the file uploaded to the given url.
  Place a map containing its filename and content-type into `args` at
  `fileinfo-path`. Fail verification if the file was not uploaded or
  cannot be identified."
  [url-path fileinfo-path]
  {:name ::identify-upload
   :func
   ;; this is going to need some error handling
   (fn [{:keys [image-manager]} args]
     (let [url (get-in* args url-path)
           fileinfo (image-manager/identify-upload image-manager url)]
       (if-not fileinfo
         (fail-verification args "Upload not found or not readable")
         (assoc-in* args fileinfo-path fileinfo))))})

(defn verify-not-recently-posted
  "Fail verification if a file has been recently posted to the same sub."
  [_sid-path _filename-path]
  {:name ::verify-not-recently-posted
   :func
   (fn [args]
     args)})
