;; gql/elements/post/tasks.clj -- Task descriptions for ovarit-app posts
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.post.tasks
  (:require [cambium.core :as log]
            [ovarit.app.gql.elements.post.query
             :refer [delete-old-comment-views]]
            [ovarit.app.gql.util :refer [expound-str]]))

(defn remove-old-comment-views-batch
  "Remove some old comment views, and return a map describing the work done."
  [{:keys [db] :as context} values]
  (try
    (delete-old-comment-views db values)
    (catch Exception e
      (log/error {:exception-message (.getMessage e)
                  :expound (expound-str (ex-data e) context)}
                 "remove old comment views task exception")
      {:deleted 0
       :remaining 0
       :completed-pid nil})))

(defn remove-old-comment-views
  "Remove old comment views."
  [{:keys [options state] :as context}]
  (let [ctx (assoc-in context [:db :counter] (atom 0))]
    (when (nil? @state)
      (swap! state assoc :startpid -1))
    (loop [removed 0]
      (let [startpid (:startpid @state)
            rm-options (assoc options
                              :startpid startpid)
            {:keys [deleted remaining pid]}
            (remove-old-comment-views-batch ctx rm-options)]
        ;; pid will be nil if no archived posts remain.  In that case,
        ;; leave the starting pid alone so we can look again later
        ;; for newly archived posts.  Otherwise, if there are no views
        ;; remaining to delete for posts at or before this pid, bump
        ;; up the startpid so we don't check them again.
        (when (and pid (zero? remaining))
          (swap! state assoc :startpid pid))

        ;; If we looked at a group of posts and found no views to delete,
        ;; continue looking at posts until some views are deleted or until we
        ;; run out of archived posts.
        (let [sum (+ removed deleted)]
          (if (and pid (pos? deleted) (pos? remaining)
                   (< sum (:batch-limit options)))
            (recur sum)
            (log/info {:next-pid (:startpid @state)
                       :remaining remaining
                       :deleted sum}
                      "Ran old comment view cleanup")))))))
