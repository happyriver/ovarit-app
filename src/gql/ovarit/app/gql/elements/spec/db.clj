;; gql/elements/spec/db.clj -- Database function specs for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.spec.db
  (:require
   [clojure.spec.alpha :as spec]
   [orchestra.spec.test :as stest]))

(spec/def ::sqlvec (spec/and vector? #(string? (first %))))
(spec/def ::start-cte-snip (spec/nilable ::sqlvec))
(spec/def ::end-cte-snip (spec/nilable ::sqlvec))

;; Try to catch callers who pass something that is obviously not the
;; db component.
(spec/def ::ds map?)
(spec/def ::db (spec/keys :req-un [::ds]))

(defn qualify
  [sym-name]
  (symbol (str *ns*) (str sym-name)))

(defmacro fdef
  "Define a function spec and instrument the function automatically."
  [name & forms]
  (when spec/*compile-asserts*
    `(do
       (spec/fdef ~name ~@forms)
       (stest/instrument '~(qualify name)))))
