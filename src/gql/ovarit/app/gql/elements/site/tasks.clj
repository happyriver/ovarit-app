;; gql/elements/site/tasks.clj -- Site-related tasks for ovarit-app
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.site.tasks
  (:require
   [cambium.core :as log]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn]]
   [ovarit.app.gql.protocols.bus :as bus]
   [ovarit.app.gql.protocols.payment-subscription :as subscription]))

(hugsql/def-db-fns "sql/site.sql")
(wrap-db-fn pick-a-user)

(defn update-funding-progress
  [{:keys [bus db options state]}]
  (let [subscription (:payment-subscription options)
        db-with-counter (assoc db :counter (atom 0))]
    (try
      (log/debug {} "Updating funding progress")
      (let [user (pick-a-user db-with-counter)
            topic :ovarit.app.gql.elements.site.resolve/site
            update (fn [old-value]
                     (let [new-value (subscription/stats subscription user)]
                       (if new-value
                         (do
                           (when (not= old-value new-value)
                             (log/info {:new-value new-value}
                                       "Funding progress update")
                             (bus/publish bus topic
                                          "funding-progress" new-value))
                           new-value)
                         old-value)))]
        (swap! state update))
      (catch Exception e
        (log/error e "Error updating funding progress")))))
