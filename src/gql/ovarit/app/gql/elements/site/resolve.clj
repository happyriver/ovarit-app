;; gql/elements/site/resolve.clj -- GraphQL resolvers for site information for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.site.resolve
  "Contains custom resolvers for site queries."
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [close! go]]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.site.query :as query]
   [ovarit.app.gql.protocols.bus :as bus]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.protocols.current-user :refer [get-uid]]
   [ovarit.app.gql.protocols.schema :as schema]
   [ovarit.app.gql.protocols.tasks :as tasks]
   [ovarit.app.gql.protocols.visitor-counter :as visitor-counter]
   [ovarit.app.gql.resolver-helpers.core :as args :refer [fail-verification]]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.resolver-helpers.pagination :as pag]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]))

;;; Parameter checking

(defn verify-timespan
  []
  {:name ::verify-timespan
   :func
   (fn [_ {:keys [since until] :as args}]
     (let [since-val (try
                       (Long/parseLong since)
                       (catch java.lang.NumberFormatException _
                         :err))
           until-val (try
                       (Long/parseLong until)
                       (catch java.lang.NumberFormatException _
                         :err))]
       (if (or (and since (= since-val :err))
               (and until (= until-val :err))
               (and since until (< until-val since-val))
               (and since (neg? since-val))
               (and until (neg? until-val)))
         (fail-verification args "Invalid timespan" 400)
         args)))})

(defn- verify-by
  []
  {:name ::verify-by
   :func (fn [_ {:keys [by] :as args}]
           (if-not (pos-int? by)
             (fail-verification args "Invalid timespan" 400)
             args))})

;; Check that a string for use in username bans is not empty and
;; contains only alphanumerics and hyphens.  Underscores are banned
;; because they are a SQL wildcard and would have to be escaped.
(defmethod schema/check-argument ::username-ban-string?
  [_ _ arg]
  (some? (re-matches #"[a-zA-Z0-9-]+" arg)))

;;; Schema queries

(def stats
  {:name ::stats
   :prepare [(verify-timespan)]
   :resolve
   (fn [{:keys [db] :as context} {:keys [since until]} _]
     (let [params {:since (when since (parse-long since))
                   :until (when until (parse-long until))
                   :users-who? (data/wants-fields? context
                                                   [:SiteStats/users_who_commented
                                                    :SiteStats/users_who_posted
                                                    :SiteStats/users_who_voted])}]
       (query/select-stats db params)))})

(def visitor-counts
  {:name ::visitor-counts
   :prepare [(verify-timespan)
             (verify-by)]
   :resolve
   (fn [{:keys [visitor-counter]} {:keys [since until by]} _]
     (let [since-val (if since (parse-long since) 1)
           until-val (if until (parse-long until) 1)
           results (visitor-counter/visitors visitor-counter
                                             (java.sql.Timestamp. since-val)
                                             (java.sql.Timestamp. until-val) by)]
       (map (fn [{:keys [day count]}]
              {:start (str (+ since-val (* day 24 60 60 1000)))
               :count count})
            results)))})

(defn banned-username-strings
  "Resolve the query for banned username strings."
  [{:keys [db]} _ _]
  ;; From the database we get a list of maps with a string and a name.
  ;; Combine all the usernames matching the same string into a list.
  (->> (query/select-banned-username-strings db)
       (map (fn [elem] {(:value elem) (when (:name elem)
                                        [(:name elem)])}))
       (apply (partial merge-with concat))
       (map (fn [[k v]] {:banned k :users v}))
       (into [])))

(defn invite-code-settings
  "Resolve the query for the invite code settings."
  [{:keys [db]} _ _]
  (query/select-invite-code-settings db))

(defn- update-invite-code-triggers
  [context invite-codes page-size]
  (let [requested-codes (take page-size invite-codes)
        creating-user-count (count requested-codes)
        used-by-user-count (->> requested-codes
                                (map #(count (:uids %)))
                                (apply +))]
    (-> (prom/promise invite-codes)
        (data/update-trigger! context :InviteCode/created_by :user-bucket
                              (data/raise-threshold-fn creating-user-count))
        (data/update-trigger! context :InviteCode/used_by :user-bucket
                              (data/raise-threshold-fn used-by-user-count)))))

(def invite-codes
  "Resolve the query for invite codes with pagination."
  {:name ::invite-codes
   :prepare [(pag/verify-pagination-not-nested)
             (pag/verify-query-size :first)]
   :resolve
   (fn [{:keys [db] :as context} {:keys [first after search]} _]
     (let [select-args {:limit (inc first)
                        :before (when after
                                  (pag/timestamp-from-cursor after))
                        :search search}
           invite-code-data (->> (query/select-invite-codes db select-args)
                                 (map (fn [ic]
                                        (update ic :uids #(remove nil? %)))))
           invite-codes (map #(assoc % :cursor
                                     (pag/make-cursor (:created %)))
                             invite-code-data)
           pagination-depth (list [:InviteCode first])]
       (data/with-superlifter context
         (-> (update-invite-code-triggers context invite-codes first)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(defn current-funding-progress
  "Resolve funding progress."
  [{:keys [current-user taskrunner]} _ _]
  (when (get-uid current-user)
    @(tasks/state taskrunner :funding-progress-update)))

(defn current-announcement
  "Resolve site announcement post id."
  [{:keys [site-config]} _ _]
  (:announcement-pid (site-config)))

(defn sitelog-entries
  "Fetch the site's most recent log entries.
  This implementation is incomplete and should only be used for testing."
  [{:keys [db] :as context} _ _]
  (let [first 10
        fixup #(-> %
                   (update :action constants/reverse-site-log-action-map)
                   (assoc :cursor (pag/make-cursor (:time %))))
        entries (->> (query/select-sitelogs db {})
                     (map fixup))
        target-count (->> entries
                          (map :target-uid)
                          (remove nil?)
                          count)]
    (data/with-superlifter context
      (-> (prom/promise entries)
          (data/update-trigger! context :SiteLogEntry/user :user-bucket
                                data/raise-threshold-by-count)
          (data/update-trigger! context :SiteLogEntry/target_user :user-bucket
                                (data/raise-threshold-fn target-count))
          (prom/then
           (fn [result]
             (pag/resolve-pagination result first
                                     (list [:SiteLogEntry first]))))))))

;; Schema mutations

(defn ban-string-in-usernames
  "Resolve the mutation that adds another banned string for usernames.
  Return the list of names of users affected by the ban."
  [{:keys [clock current-user db]} {:keys [banned]} _]
  (let [values {:value banned
                :uid (get-uid current-user)
                :timestamp (util/sql-now clock)}
        result (map :name
                    (query/insert-banned-username-string db values))]
    (log/info {:banned banned} "Banned string in usernames")
    {:banned banned
     :users result}))

(defn unban-string-in-usernames
  "Resolve the mutation that removes a banned string for usernames."
  [{:keys [clock current-user db]} {:keys [banned]} _]
  (let [values {:value banned
                :uid (get-uid current-user)
                :timestamp (util/sql-now clock)}]
    (query/delete-banned-username-string db values)
    (log/info {:banned banned} "Unbanned string in usernames")
    banned))

(defn set-invite-code-settings
  "Resolve the mutation that sets the invite code settings."
  [{:keys [cache clock current-user db]}
   {:keys [required visible minimum-level per-user] :as args} _]
  (let [params {:uid (get-uid current-user)
                :timestamp (util/sql-now clock)
                :required required
                :visible visible
                :minimum-level minimum-level
                :per-user per-user}]
    (query/update-invite-code-settings db params)
    (doseq [key ["flask_cache_site.invite_level"
                 "flask_cache_site.invite_max"
                 "flask_cache_site.require_invite_code"
                 "flask_cache_site.invitations_visible_to_users"]]
      (cache/cache-del cache key))
    (log/info {:args args} "Updated invite code settings")
    args))

(defn generate-invite-code
  "Resolve the mutation to generate a new invite code."
  [{:keys [current-user clock db] :as context}
   {:keys [code max-uses expires]} _]
  (let [rand-char #(nth "abcdefghijklmnopqrstuvwxyz0123456789" (rand 36))
        new-code (if (empty? code)
                   (apply str (take 32 (repeatedly rand-char)))
                   code)
        expires-timestamp (when expires
                            (java.sql.Timestamp. (parse-long expires)))
        params {:uid (get-uid current-user)
                :code new-code
                :created (util/sql-now clock)
                :max-uses max-uses
                :uses 0
                :expires expires-timestamp}
        code (-> (query/insert-invite-code db params)
                 (assoc :uids []))]
    (log/info {:code new-code :max-uses max-uses} "Created invite code")
    (data/with-superlifter context
      (-> (prom/promise code)
          (data/update-trigger! context :InviteCode/created-by :user-bucket
                                data/inc-threshold)))))

(def expire-invite-codes
  "Resolve the mutation to change invite code expirations."
  {:name ::expire-invite-codes
   :prepare
   [(args/verify-fn :codes seq "No invite code ids provided" 400)
    (args/convert-arg-to-int-seq :codes)
    (args/ignore-if-nil :expires (args/convert-arg-to-int :expires))]

   :resolve
   (fn [{:keys [db]} {:keys [codes expires]} _]
     (let [expires-timestamp (when expires
                               (java.sql.Timestamp. expires))]
       (query/update-invite-codes-expire db {:ids codes
                                             :expires expires-timestamp})
       (log/info {:codes codes :expires expires}
                 "Changed invite code expiration")
       codes))})

;;; Schema subscriptions

(defn stream-funding-progress
  "Stream funding progress."
  [{:keys [bus current-user]} _args source-stream-callback]
  (if-not (get-uid current-user)
    (let [action (go (source-stream-callback nil))]
      #(close! action))
    (let [callback #(do
                      (log/debug {:value %} "streaming funding update")
                      (source-stream-callback %))
          sub (bus/subscribe bus ::site "funding-progress" callback)]
      #(bus/close-subscription bus sub))))

(comment
  (require 'user)
  (def db (:db user/system))

  (invite-codes {:db db} {:first 20 :limit nil} nil)

  (into [] {:a 1})
  (def res (query/select-invite-codes db {:limit 10}))
  (:uids (first res)))
