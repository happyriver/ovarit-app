;; gql/elements/site/prepare.clj -- Prepare site-related arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.site.prepare
  (:require
   [ovarit.app.gql.protocols.current-user :refer [get-uid
                                                  is-admin?]]
   [ovarit.app.gql.resolver-helpers.core :as prepare
    :refer [get-in* fail-verification]]))

(defn verify-posting-enabled
  []
  {:name :verify-posting-enabled
   :func
   (fn [{:keys [current-user site-config]} args]
     (if (and (not (is-admin? current-user))
              (not (:enable-posting (site-config))))
       (fail-verification args "Not authorized" 403)
       args))})

(defn prevent-self-vote
  "Only permit self-voting if configured."
  [author-path content-type]
  {:name :prevent-self-vote
   :func
   (fn [{:keys [current-user site-config]} args]
     (let [author-uid (get-in* args author-path)
           permitted? (get (site-config) (if (= :comment content-type)
                                           :self-vote-comments
                                           :self-vote-posts))]
       (if (or permitted? (not= (get-uid current-user) author-uid))
         args
         (fail-verification args "No self-voting"))))})
