;; gql/elements/site/query.clj -- Build SQL queries for site information for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.site.query
  "Build SQL queries for site info"
  (:require
   [clojure.spec.alpha :as spec]
   [honey.sql :as sql]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn
                                              wrap-snip-fn]]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.user.spec :as user-spec])
  (:require
   [ovarit.app.gql.elements.site.spec.banned-username-string
    :as-alias banned-username-string]
   [ovarit.app.gql.elements.site.spec.invite-code
    :as-alias invite-code]
   [ovarit.app.gql.elements.site.spec.invite-code-settings
    :as-alias invite-code-settings]
   [ovarit.app.gql.elements.site.spec.select-stats
    :as-alias select-stats]))

(hugsql/def-db-fns "sql/site.sql")

(spec/def ::comment-vote-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::comment-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::downvote-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::downvotes-snip ::db-spec/sqlvec)
(spec/def ::post-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::sub-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::upvote-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::upvotes-snip ::db-spec/sqlvec)
(spec/def ::user-where-timespan-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::users-who-snip (spec/nilable ::db-spec/sqlvec))

(wrap-snip-fn downvotes-all-time-snip)
(db-spec/fdef downvotes-all-time-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn downvotes-snip)
(db-spec/fdef downvotes-snip
  :args (spec/cat :args (spec/keys :req-un [::downvote-where-timespan-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn upvotes-all-time-snip)
(db-spec/fdef upvotes-all-time-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn upvotes-snip)
(db-spec/fdef upvotes-snip
  :args (spec/cat :args (spec/keys :req-un [::upvote-where-timespan-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn users-who-snip)
(db-spec/fdef users-who-snip
  :args (spec/cat :args (spec/keys :req-un
                                   [::post-where-timespan-snip
                                    ::comment-where-timespan-snip
                                    ::comment-vote-where-timespan-snip]))
  :ret ::db-spec/sqlvec)

(spec/def ::banned-username-string/value string?)
(spec/def ::banned-username-string/name (spec/nilable ::user-spec/name))
(wrap-db-fn select-banned-username-strings)
(db-spec/fdef select-banned-username-strings
  :args (spec/cat :db ::db-spec/db)
  :ret (spec/coll-of (spec/keys :req-un [::banned-username-string/value
                                         ::banned-username-string/name])))

(wrap-db-fn insert-banned-username-string)
(db-spec/fdef insert-banned-username-string
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::user-spec/uid
                                    ::banned-username-string/value
                                    ::element-spec/timestamp]))
  :ret (spec/coll-of (spec/keys :req-un [::user-spec/name])))

(wrap-db-fn delete-banned-username-string)
(db-spec/fdef delete-banned-username-string
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::user-spec/uid
                                    ::banned-username-string/value
                                    ::element-spec/timestamp]))
  :ret int?)

(spec/def ::invite-code/before (spec/nilable ::element-spec/timestamp))
(spec/def ::invite-code/code string?)
(spec/def ::invite-code/created ::element-spec/timestamp)
(spec/def ::invite-code/expires (spec/nilable ::element-spec/timestamp))
(spec/def ::invite-code/id int?)
(spec/def ::invite-code/ids (spec/coll-of ::invite-code/id))
(spec/def ::invite-code/limit nat-int?)
(spec/def ::invite-code/max-uses int?)
(spec/def ::invite-code/search (spec/nilable string?))
(spec/def ::invite-code/uids (spec/coll-of
                              (spec/nilable ::user-spec/uid)))
(spec/def ::invite-code/uses int?)

(wrap-db-fn insert-invite-code)
(db-spec/fdef insert-invite-code
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::user-spec/uid
                                    ::invite-code/code
                                    ::invite-code/created
                                    ::invite-code/expires
                                    ::invite-code/uses
                                    ::invite-code/max-uses]))
  :ret (spec/keys :req-un [::user-spec/uid
                           ::invite-code/code
                           ::invite-code/created
                           ::invite-code/expires
                           ::invite-code/uses
                           ::invite-code/max-uses]))

(wrap-db-fn select-invite-codes)
(db-spec/fdef select-invite-codes
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::invite-code/limit
                                    ::invite-code/before
                                    ::invite-code/search]))
  :ret (spec/coll-of (spec/keys :req-un [::invite-code/id
                                         ::user-spec/uid
                                         ::invite-code/code
                                         ::invite-code/created
                                         ::invite-code/expires
                                         ::invite-code/uses
                                         ::invite-code/max-uses
                                         ::invite-code/uids])))

(wrap-db-fn update-invite-codes-expire)
(db-spec/fdef update-invite-codes-expire
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::invite-code/ids
                                    ::invite-code/expires]))
  :ret int?)

(spec/def ::invite-code-settings/required boolean?)
(spec/def ::invite-code-settings/visible boolean?)
(spec/def ::invite-code-settings/minimum-level int?)
(spec/def ::invite-code-settings/per-user int?)
(wrap-db-fn select-invite-code-settings)
(db-spec/fdef select-invite-code-settings
  :args (spec/cat :db ::db-spec/db)
  :ret (spec/keys :req-un [::invite-code-settings/required
                           ::invite-code-settings/visible
                           ::invite-code-settings/minimum-level
                           ::invite-code-settings/per-user]))

(wrap-db-fn update-invite-code-settings)
(db-spec/fdef update-invite-code-settings
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::invite-code-settings/required
                                            ::invite-code-settings/visible
                                            ::invite-code-settings/minimum-level
                                            ::invite-code-settings/per-user
                                            ::user-spec/uid
                                            ::element-spec/timestamp]))
  :ret int?)

(spec/def ::select-stats/users int?)
(spec/def ::select-stats/subs int?)
(spec/def ::select-stats/posts int?)
(spec/def ::select-stats/comments int?)
(spec/def ::select-stats/upvotes (spec/nilable int?))
(spec/def ::select-stats/downvotes (spec/nilable int?))
(spec/def ::select-stats/users-who-posted int?)
(wrap-db-fn _select-stats)
(db-spec/fdef _select-stats
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-where-timespan-snip
                                            ::sub-where-timespan-snip
                                            ::post-where-timespan-snip
                                            ::comment-where-timespan-snip
                                            ::users-who-snip
                                            ::upvotes-snip
                                            ::downvotes-snip]))
  :ret (spec/merge (spec/keys :req-un [::select-stats/users
                                       ::select-stats/subs
                                       ::select-stats/posts
                                       ::select-stats/comments
                                       ::select-stats/upvotes
                                       ::select-stats/downvotes])
                   (spec/keys :opt-un [::select-stats/users-who-posted])))

(wrap-db-fn select-sitelogs)

(defn where-timespan
  "Construct a snippet containing a where clause with a timespan.
  Either the since time or the until time may be omitted.  The
  `extra-clause` will be ANDed in with the timespan, if it is
  supplied."
  ([timespan var]
   (where-timespan timespan var nil))
  ([{:keys [since until]} var extra-clause]
   (when (or since until extra-clause)
     (sql/format
      {:where (into [:and] [extra-clause
                            (when since [:> var since])
                            (when until [:< var until])])}))))

(defn select-stats
  [db {:keys [since until users-who?]}]
  (let [timespan {:since (when since (java.sql.Timestamp. since))
                  :until (when until (java.sql.Timestamp. until))}
        where-snips
        {:user-where-timespan-snip (where-timespan timespan :joindate)
         :sub-where-timespan-snip (where-timespan timespan :creation)
         :post-where-timespan-snip (where-timespan timespan :posted)
         :comment-where-timespan-snip (where-timespan timespan :time)

         :upvote-where-timespan-snip
         (where-timespan timespan :datetime
                         [:raw "positive = 1"])

         :downvote-where-timespan-snip
         (where-timespan timespan :datetime
                         [:raw "positive = 0"])

         :comment-vote-where-timespan-snip
         (where-timespan timespan :datetime)

         :post-vote-where-timespan-snip
         (where-timespan timespan :datetime)}

        snips {:upvotes-snip (if (or since until)
                               (upvotes-snip where-snips)
                               (upvotes-all-time-snip))
               :downvotes-snip (if (or since until)
                                 (downvotes-snip where-snips)
                                 (downvotes-all-time-snip))
               :users-who-snip (when users-who?
                                 (users-who-snip where-snips))}]
    (_select-stats db (merge where-snips snips))))

(comment
  (sql/format {:where [:and [:raw "positive = 0"] nil]})
  (sql/format {:where [:and]})
  (sql/format nil)

  )
