;; gql/elements/sub/query.clj -- SQL queries for Sub data
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.sub.query
  (:require [clojure.spec.alpha :as spec]
            [clojure.string :as str]
            [hugsql.core :as hugsql]
            [ovarit.app.gql.elements.constants :refer [wrap-db-fn
                                                       wrap-snip-fn]]
            [ovarit.app.gql.elements.post.spec :as post-spec]
            [ovarit.app.gql.elements.spec :as element-spec]
            [ovarit.app.gql.elements.spec.db :as db-spec]
            [ovarit.app.gql.elements.sql :refer [start-cte-snip
                                                 end-cte-snip]]
            [ovarit.app.gql.elements.sub.spec :as sub-spec]
            [ovarit.app.gql.elements.user.spec :as user-spec])
  (:require [ovarit.app.gql.elements.sub.spec.in-names-snip
             :as-alias in-names-snip]
            [ovarit.app.gql.elements.sub.spec.insert-or-update-user-flair
             :as-alias insert-or-update-user-flair]
            [ovarit.app.gql.elements.sub.spec.pag-after-snip
             :as-alias pag-after-snip]
            [ovarit.app.gql.elements.sub.spec.post-flair
             :as-alias post-flair]
            [ovarit.app.gql.elements.sub.spec.post-type-config
             :as-alias post-type-config]
            [ovarit.app.gql.elements.sub.spec.ptype-list-snip
             :as-alias ptype-list-snip]
            [ovarit.app.gql.elements.sub.spec.rule
             :as-alias rule]
            [ovarit.app.gql.elements.sub.spec.select-subs
             :as-alias select-subs]
            [ovarit.app.gql.elements.sub.spec.select-subs.agg-meta
             :as-alias select-subs.agg-meta]
            [ovarit.app.gql.elements.sub.spec.select-subs.banned
             :as-alias select-subs.banned]
            [ovarit.app.gql.elements.sub.spec.select-subs.meta
             :as-alias select-subs.meta]
            [ovarit.app.gql.elements.sub.spec.select-subs.mod-stats
             :as-alias select-subs.mod-stats]
            [ovarit.app.gql.elements.sub.spec.select-subs.mods
             :as-alias select-subs.mods]
            [ovarit.app.gql.elements.sub.spec.update-post-flair-order
             :as-alias update-post-flair-order]
            [ovarit.app.gql.elements.sub.spec.update-post-type-config
             :as-alias update-post-type-config]
            [ovarit.app.gql.elements.sub.spec.update-post-type-flairs
             :as-alias update-post-type-flairs]
            [ovarit.app.gql.elements.sub.spec.update-rule-order
             :as-alias update-rule-order]
            [ovarit.app.gql.elements.sub.spec.user-flair
             :as-alias user-flair]))

(spec/def ::post-flair/id int?)
(spec/def ::post-flair/text string?)
(spec/def ::post-flair/mods-only boolean?)
(spec/def ::post-flair/all-ptypes
  (spec/coll-of (spec/coll-of ::post-spec/ptype)))
(spec/def ::post-flair/allowed-ptypes-snip ::db-spec/sqlvec)
(spec/def ::post-flair/disallowed-ptypes-snip ::db-spec/sqlvec)
(spec/def ::post-flair/disallowed-ptypes
  (spec/coll-of ::post-spec/ptype))

(spec/def ::post-type-config/rules (spec/nilable string?))
(spec/def ::post-type-config/is-admin boolean?)
(spec/def ::post-type-config/mods-only boolean?)

(spec/def ::rule/id int?)
(spec/def ::rule/text string?)

(spec/def ::user-flair/flair string?)

;; SQL queries

(hugsql/def-db-fns "sql/subs.sql")

(wrap-snip-fn banned-cte-snip)
(db-spec/fdef banned-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn banned-field-snip)
(db-spec/fdef banned-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn banned-join-snip)
(db-spec/fdef banned-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn false-snip)
(db-spec/fdef false-snip
  :ret ::db-spec/sqlvec)

(spec/def ::in-names-snip/names
  (spec/coll-of ::sub-spec/name))
(wrap-snip-fn in-names-snip)
(db-spec/fdef in-names-snip
  :args (spec/cat :args (spec/keys :req-un [::in-names-snip/names]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn in-sids-snip)
(db-spec/fdef in-sids-snip
  :args (spec/cat :args (spec/keys :req-un [::sub-spec/sids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn info-fields-snip)
(db-spec/fdef info-fields-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-cte-snip)
(db-spec/fdef meta-cte-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-fields-snip)
(db-spec/fdef meta-fields-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-joins-snip)
(db-spec/fdef meta-joins-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn mod-stats-cte-snip)
(db-spec/fdef mod-stats-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn mod-stats-fields-snip)
(db-spec/fdef mod-stats-fields-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn mod-stats-joins-snip)
(db-spec/fdef mod-stats-joins-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn moderators-cte-snip)
(db-spec/fdef moderators-cte-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn moderators-field-snip)
(db-spec/fdef moderators-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn moderators-join-snip)
(db-spec/fdef moderators-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn order-by-name-snip)
(db-spec/fdef order-by-name-snip
  :ret ::db-spec/sqlvec)

(spec/def ::pag-after-snip/after ::sub-spec/name)
(wrap-snip-fn pag-after-snip)
(db-spec/fdef pag-after-snip
  :args (spec/cat :args (spec/keys :req-un [::pag-after-snip/after]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn pag-limit-snip)
(db-spec/fdef pag-limit-snip
  :args (spec/cat :args (spec/keys :req-un [::element-spec/limit]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-type-config-cte-snip)
(db-spec/fdef post-type-config-cte-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-type-config-field-snip)
(db-spec/fdef post-type-config-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn post-type-config-join-snip)
(db-spec/fdef post-type-config-join-snip
  :ret ::db-spec/sqlvec)

(spec/def ::ptype-list-snip/ptypes
  (spec/coll-of ::post-spec/ptype))
(wrap-snip-fn ptype-list-snip)
(db-spec/fdef ptype-list-snip
  :args (spec/cat :args (spec/keys :req-un [::ptype-list-snip/ptypes]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-choices-cte-snip)
(db-spec/fdef user-flair-choices-cte-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-choices-field-snip)
(db-spec/fdef user-flair-choices-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-choices-join-snip)
(db-spec/fdef user-flair-choices-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-cte-snip)
(db-spec/fdef user-flair-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-field-snip)
(db-spec/fdef user-flair-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn user-flair-join-snip)
(db-spec/fdef user-flair-join-snip
  :ret ::db-spec/sqlvec)

(wrap-db-fn default-subs)
(db-spec/fdef default-subs
  :args (spec/cat :db ::db-spec/db)
  :ret (spec/coll-of (spec/keys :req-un [::sub-spec/sid
                                         ::sub-spec/name])))

(wrap-db-fn delete-post-flair)
(db-spec/fdef delete-post-flair
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::post-flair/id
                                            ::sub-spec/sid]))
  :ret int?)

(wrap-db-fn delete-rule)
(db-spec/fdef delete-rule
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::rule/id
                                            ::sub-spec/sid]))
  :ret int?)

(wrap-db-fn delete-user-flair)
(db-spec/fdef delete-user-flair
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::sub-spec/sid]))
  :ret int?)

(wrap-db-fn insert-post-flair)
(db-spec/fdef insert-post-flair
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sid
                                            ::post-flair/text]))
  :ret (spec/nilable
        (spec/keys :req-un [::post-flair/id
                            ::post-flair/text
                            ::post-flair/mods-only])))

(wrap-db-fn insert-rule)
(db-spec/fdef insert-rule
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sid
                                            ::rule/text]))
  :ret (spec/nilable
        (spec/keys :req-un [::rule/id
                            ::rule/text])))

(spec/def ::insert-or-update-user-flair/user-flair string?)
(spec/def ::insert-or-update-user-flair/user-flair-id
  (spec/nilable int?))
(wrap-db-fn insert-or-update-user-flair)
(db-spec/fdef insert-or-update-user-flair
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::sub-spec/sid
                           ::user-spec/uid
                           ::insert-or-update-user-flair/user-flair
                           ::insert-or-update-user-flair/user-flair-id]))
  :ret int?)

(wrap-db-fn insert-user-flair-choice-returning-choices)
(db-spec/fdef insert-user-flair-choice-returning-choices
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::sub-spec/sid
                           ::user-flair/flair]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-flair/flair])))

(wrap-db-fn select-post-flairs)
(db-spec/fdef select-post-flairs
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::sub-spec/sid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::post-flair/id
                            ::post-flair/text
                            ::post-flair/mods-only
                            ::post-flair/disallowed-ptypes])))

(wrap-db-fn select-rules)
(db-spec/fdef select-rules
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::sub-spec/sid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::rule/id
                            ::rule/text])))

(spec/def ::select-subs/start-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/banned-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/meta-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/mod-stats-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/moderators-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/post-type-config-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-choices-cte-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/end-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/banned-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/info-fields-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/meta-fields-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/mod-stats-fields-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/moderators-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/post-type-config-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-choices-field-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/banned-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/meta-joins-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/mod-stats-joins-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/moderators-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/post-type-config-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-choices-join-snip
  (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/user-flair-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/where-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/order-by-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::select-subs/limit-snip (spec/nilable ::db-spec/sqlvec))

(spec/def ::select-subs/banned boolean?)
(spec/def ::select-subs/nsfw boolean?)
(spec/def ::select-subs/sidebar string?)
(spec/def ::select-subs/status (spec/nilable int?))
(spec/def ::select-subs/title string?)
(spec/def ::select-subs/creation ::element-spec/timestamp)
(spec/def ::select-subs/subscriber-count int?)
(spec/def ::select-subs/post-count int?)

(spec/def ::boolean-ish #{"0" "1"})

(spec/def ::select-subs.meta/freeform-user-flairs ::boolean-ish)
(spec/def ::select-subs.meta/mod ::user-spec/uid)
(spec/def ::select-subs.meta/restricted ::boolean-ish)
(spec/def ::select-subs.meta/sort (spec/nilable string?))
(spec/def ::select-subs.meta/sub-banned-users-private ::boolean-ish)
(spec/def ::select-subs.meta/sublog-private ::boolean-ish)
(spec/def ::select-subs.meta/ucf ::boolean-ish)
(spec/def ::select-subs.meta/umf ::boolean-ish)
(spec/def ::select-subs.meta/user-can-flair-self ::boolean-ish)
(spec/def ::select-subs.meta/wiki string?)
(spec/def ::select-subs/meta
  (spec/keys :req-un [::select-subs.meta/mod]
             :opt-un [::select-subs.meta/restricted
                      ::select-subs.meta/sort
                      ::select-subs.meta/sub-banned-users-private
                      ::select-subs.meta/sublog-private
                      ::select-subs.meta/ucf
                      ::select-subs.meta/umf
                      ::select-subs.meta/user-can-flair-self
                      ::select-subs.meta/wiki]))
(spec/def ::select-subs.agg-meta/sticky
  (spec/coll-of string?))
(spec/def ::select-subs.agg-meta/xmod2
  (spec/coll-of ::user-spec/uid))
(spec/def ::select-subs.agg-meta
  (spec/nilable
   (spec/keys :opt-un [::select-subs.agg-meta/sticky
                       ::select-subs.agg-meta/xmod2])))
(spec/def ::select-subs.mod-stats/open int?)
(spec/def ::select-subs.mod-stats/closed int?)
(spec/def ::select-subs/post-report-counts
  (spec/nilable
   (spec/keys :req-un [::select-subs.mod-stats/open
                       ::select-subs.mod-stats/closed])))
(spec/def ::select-subs/comment-report-counts
  (spec/nilable
   (spec/keys :req-un [::select-subs.mod-stats/open
                       ::select-subs.mod-stats/closed])))
(spec/def ::select-subs/unread-modmail-count int?)
(spec/def ::select-subs/new-modmail-count int?)
(spec/def ::select-subs/all-modmail-count int?)
(spec/def ::select-subs.mods/power-level #{0 1 2})
(spec/def ::select-subs/mods
  (spec/coll-of
   (spec/keys :req-un [::user-spec/uid
                       ::select-subs.mods/power-level])))
(spec/def ::select-subs/post-type-config
  (spec/coll-of
   (spec/keys :req-un [::post-spec/ptype
                       ::post-type-config/rules
                       ::post-type-config/mods-only])))
(spec/def ::select-subs/user-flair-choices
  (spec/nilable
   (spec/coll-of
    (spec/map-of keyword? ::user-flair/flair))))
(wrap-db-fn _select-subs)
(db-spec/fdef _select-subs
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::select-subs/start-cte-snip
                           ::select-subs/banned-cte-snip
                           ::select-subs/meta-cte-snip
                           ::select-subs/mod-stats-cte-snip
                           ::select-subs/moderators-cte-snip
                           ::select-subs/post-type-config-cte-snip
                           ::select-subs/user-flair-choices-cte-snip
                           ::select-subs/user-flair-cte-snip
                           ::select-subs/end-cte-snip
                           ::select-subs/banned-field-snip
                           ::select-subs/info-fields-snip
                           ::select-subs/meta-fields-snip
                           ::select-subs/mod-stats-fields-snip
                           ::select-subs/moderators-field-snip
                           ::select-subs/post-type-config-field-snip
                           ::select-subs/user-flair-choices-field-snip
                           ::select-subs/user-flair-field-snip
                           ::select-subs/banned-join-snip
                           ::select-subs/meta-joins-snip
                           ::select-subs/mod-stats-joins-snip
                           ::select-subs/moderators-join-snip
                           ::select-subs/post-type-config-join-snip
                           ::select-subs/user-flair-choices-join-snip
                           ::select-subs/user-flair-join-snip
                           ::select-subs/where-snip
                           ::select-subs/order-by-snip
                           ::select-subs/limit-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [::sub-spec/sid
                            ::sub-spec/name]
                   :opt-un [
                            ;; banned-field-snip
                            ::select-subs/banned
                            ;; info-fields-snip
                            ::select-subs/nsfw
                            ::select-subs/sidebar
                            ::select-subs/status
                            ::select-subs/title
                            ::select-subs/creation
                            ::select-subs/subscriber-count
                            ::select-subs/post-count
                            ;; meta-fields-snip
                            ::select-subs/meta
                            ::select-subs.agg-meta
                            ;; mod-stats-fields-snip
                            ::select-subs/post-report-counts
                            ::select-subs/comment-report-counts
                            ::select-subs/unread-modmail-count
                            ::select-subs/new-modmail-count
                            ::select-subs/all-modmail-count
                            ;; moderators-field-snip
                            ::select-subs/mods
                            ;; post-type-config-field-snip
                            ::select-subs/post-type-config
                            ;; user-flair-choices-field-snip
                            ::select-subs/user-flair-choices
                            ;; user-flair-field-snip
                            ::user-flair/flair])))

(wrap-db-fn _update-post-flair)
(db-spec/fdef _update-post-flair
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::sub-spec/sid
                                    ::post-flair/id
                                    ::post-flair/mods-only
                                    ::post-flair/all-ptypes
                                    ::post-flair/allowed-ptypes-snip
                                    ::post-flair/disallowed-ptypes-snip]))
  :ret int?)

(spec/def ::update-post-flair-order/ordering
  (spec/coll-of
   (spec/cat :new-order? int
             :id ::post-flair/id)))
(wrap-db-fn update-post-flair-order)
(db-spec/fdef update-post-flair-order
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::update-post-flair-order/ordering
                           ::sub-spec/sid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::post-flair/id
                            ::post-flair/text
                            ::post-flair/mods-only
                            ::post-flair/disallowed-ptypes])))

(spec/def ::update-rule-order/ordering
  (spec/coll-of
   (spec/cat :new-order? int
             :id ::rule/id)))
(wrap-db-fn update-rule-order)
(db-spec/fdef update-rule-order
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::update-rule-order/ordering
                           ::sub-spec/sid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::rule/id
                            ::rule/text])))

(spec/def ::update-post-type-config/mods-only (spec/nilable boolean?))
(spec/def ::update-post-type-config/post-type string?)
(wrap-db-fn update-post-type-config)
(db-spec/fdef update-post-type-config
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::sub-spec/sid
                           ::post-spec/ptype
                           ::update-post-type-config/post-type
                           ::update-post-type-config/mods-only
                           ::post-type-config/rules
                           ::user-spec/uid
                           ::post-type-config/is-admin]))
  :ret int?)

(wrap-db-fn select-sublogs)

(def empty-snip-map
  (reduce #(assoc %1 %2 nil) {}
          [:start-cte-snip :end-cte-snip
           :where-snip :order-by-snip :limit-snip
           :info-fields-snip
           :meta-cte-snip :meta-fields-snip :meta-joins-snip
           :mod-stats-cte-snip :mod-stats-fields-snip :mod-stats-joins-snip
           :moderators-cte-snip :moderators-field-snip :moderators-join-snip
           :post-type-config-cte-snip :post-type-config-field-snip :post-type-config-join-snip
           :banned-cte-snip :banned-field-snip :banned-join-snip
           :user-flair-choices-cte-snip :user-flair-choices-field-snip
           :user-flair-choices-join-snip
           :user-flair-cte-snip :user-flair-field-snip :user-flair-join-snip]))

(defn- snips-for-query
  "Build the snips for a sub query depending on the fields wanted."
  [{:keys [uid banned? info? mod-stats? moderators? metadata?
           post-type-config? user-flair? user-flair-choices?]}]
  (merge
   empty-snip-map
   (when (or banned? metadata? moderators? mod-stats?
             post-type-config? user-flair-choices? user-flair?)
     {:start-cte-snip (start-cte-snip)
      :end-cte-snip   (end-cte-snip)})
   (when info?
     {:info-fields-snip (info-fields-snip)})
   (when banned?
     {:banned-cte-snip   (banned-cte-snip {:uid uid})
      :banned-field-snip (banned-field-snip)
      :banned-join-snip  (banned-join-snip)})
   (when metadata?
     {:meta-cte-snip    (meta-cte-snip)
      :meta-fields-snip (meta-fields-snip)
      :meta-joins-snip  (meta-joins-snip)})
   (when mod-stats?
     {:mod-stats-cte-snip    (mod-stats-cte-snip {:uid uid})
      :mod-stats-fields-snip (mod-stats-fields-snip)
      :mod-stats-joins-snip  (mod-stats-joins-snip)})
   (when moderators?
     {:moderators-cte-snip   (moderators-cte-snip)
      :moderators-field-snip (moderators-field-snip)
      :moderators-join-snip  (moderators-join-snip)})
   (when post-type-config?
     {:post-type-config-cte-snip   (post-type-config-cte-snip)
      :post-type-config-field-snip (post-type-config-field-snip)
      :post-type-config-join-snip  (post-type-config-join-snip)})
   (when user-flair-choices?
     {:user-flair-choices-cte-snip   (user-flair-choices-cte-snip)
      :user-flair-choices-field-snip (user-flair-choices-field-snip)
      :user-flair-choices-join-snip  (user-flair-choices-join-snip)})
   (when user-flair?
     {:user-flair-cte-snip   (user-flair-cte-snip {:uid uid})
      :user-flair-field-snip (user-flair-field-snip)
      :user-flair-join-snip  (user-flair-join-snip)})))

(defn select-subs-by-name
  "Find subs from their names."
  [db {:keys [names uid] :as args}]
  (let [params {:uid uid
                :where-snip (in-names-snip {:names (map str/lower-case names)})}
        snips (snips-for-query args)]
    (_select-subs db (merge snips params))))

(defn select-subs-by-sid
  "Find subs from their sids."
  [db {:keys [sids uid] :as args}]
  (let [params {:uid uid
                :where-snip (in-sids-snip {:sids sids})}
        snips (snips-for-query args)]
    (_select-subs db (merge snips params))))

(defn list-subs
  "Return a list of subs, paginated by name.
  Uses the keys :first, :after and :sort_by in args."
  [db {:keys [first after] :as args}]
  (let [where (when after
                (pag-after-snip {:after after}))
        params {:where-snip where
                :limit-snip (pag-limit-snip {:limit first})
                :order-by-snip (order-by-name-snip)}
        snips (snips-for-query args)
        subs (_select-subs db (merge snips params))]
    (map #(assoc % :cursor (:name %)) subs)))

(defn- ptypes-snip
  [ptypes]
  (if (seq ptypes)
    (ptype-list-snip {:ptypes ptypes})
    (false-snip)))

(defn update-post-flair
  "Update the options on a post flair"
  [db {:keys [sid id mods-only allowed-ptypes disallowed-ptypes]}]
  (-> db
      (_update-post-flair
       {:sid sid
        :id id
        :mods-only mods-only
        :all-ptypes (->> (concat allowed-ptypes disallowed-ptypes)
                         (map list))
        :allowed-ptypes-snip (ptypes-snip allowed-ptypes)
        :disallowed-ptypes-snip (ptypes-snip disallowed-ptypes)})
      :count))

(comment
  (require 'user)
  (def db (:db user/system))
  (default-subs db)
  )
