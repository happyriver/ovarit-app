;; gql/elements/sub/fixup.clj -- Make sub SQL results match schema expectations
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.sub.fixup
  (:require
   [clojure.set :as set]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.sub :as-alias sub]
   [ovarit.app.gql.util :as util]))

(defn sum-report-counts
  "Total the open and closed post and comment reports."
  [{:keys [post-report-counts comment-report-counts] :as sub}]
  (let [{post-open :open post-closed :closed} post-report-counts
        {comment-open :open comment-closed :closed} comment-report-counts]
    (-> sub
        (assoc :open-report-count (+ (or post-open 0)
                                     (or comment-open 0)))
        (assoc :closed-report-count (+ (or post-closed 0)
                                       (or comment-closed 0)))
        (dissoc :post-report-counts :comment-report-counts))))

(defn fixup-moderator
  "Fix up a moderator record from the sub query."
  [{:keys [uid power-level]}]
  {:uid uid
   :moderation-level (constants/reverse-moderation-level-map power-level)})

(defn- fixup-post-type
  "Convert the post type to a keyword in a post type config object."
  [{:keys [ptype] :as post-type-config}]
  (-> post-type-config
      (assoc :post-type (constants/reverse-post-type-map ptype))
      (dissoc :ptype)))

(defn fixup-sub-fields
  "Merge the metadata fields, and rename some of them."
  [{:keys [meta agg-meta user-flair-choices] :as sub} _]
  (let [metadata (-> (merge meta agg-meta)
                     (set/rename-keys {:mod :creator-uid
                                       :ucf :user-can-flair
                                       :umf :user-must-flair
                                       :xmod2 :ex-mod-uids})
                     (util/convert-fields-to-boolean [:allow-link-posts
                                                      :allow-polls
                                                      :allow-text-posts
                                                      :allow-upload-posts
                                                      :freeform-user-flairs
                                                      :restricted
                                                      :sub-banned-users-private
                                                      :sublog-private
                                                      :user-can-flair
                                                      :user-can-flair-self
                                                      :user-must-flair]))]
    (-> (merge sub metadata)
        (update :banned true?)
        (assoc ::sub/user-flair-choices-with-ids user-flair-choices)
        (update :user-flair-choices #(vals (apply merge %)))
        (update :post-type-config #(map fixup-post-type %))
        (set/rename-keys {:mods ::sub/mods
                          :post-type-config ::sub/post-type-config})
        (dissoc :meta :agg-meta)
        (update :text-post-min-length (fnil parse-long "0"))
        (sum-report-counts))))

(defn fix-flair-post-types
  "Construct the list of allowed post types.
  The SQL query returns the disallowed ones.  Do the set arithmetic
  and convert to keywords."
  [{:keys [disallowed-ptypes] :as flair}]
  (let [ptypes (set/difference (set (vals constants/post-type-map))
                               (set disallowed-ptypes))]
    (-> flair
        (assoc :post-types (map constants/reverse-post-type-map ptypes))
        (dissoc :disallowed-post-types))))
