;; gql/elements/sub/resolve.clj -- GraphQL resolvers for Sub data
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.sub.resolve
  "Custom resolvers for subs."
  (:require
   [cambium.core :as log]
   [clojure.java.jdbc :as jdbc]
   [clojure.set :as set]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.sub :as-alias sub]
   [ovarit.app.gql.elements.sub.fixup :as fixup :refer [fixup-sub-fields
                                                        fixup-moderator]]
   [ovarit.app.gql.elements.sub.prepare :as prepare]
   [ovarit.app.gql.elements.sub.query :as query]
   [ovarit.app.gql.elements.user.prepare :as user]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.protocols.current-user :refer [get-uid is-admin?]]
   [ovarit.app.gql.resolver-helpers.core :refer [assoc-in*
                                                 fail-verification] :as args]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.resolver-helpers.pagination :as pag]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher] :as superlifter]))

;; Data fetchers

(defn load-subs
  "Load subs by sid for the superfetcher."
  [selectors {:keys [db current-user]}]
  (let [sids (map :sid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:sids sids
                       :uid (get-uid current-user)})]
    (query/select-subs-by-sid db params)))

(def-superfetcher FetchSubBySid [id]
  (data/make-fetcher-fn :sid load-subs fixup-sub-fields))

(defn- fetch-sub
  "Promise to fetch a sub by sid or by name."
  [context select bucket]
  (let [id (data/id-from-fetch-args select)]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (->FetchSubBySid id)))))

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the sub queries."
  {:info? [:Sub/sidebar
           :Sub/status
           :Sub/title
           :Sub/creation
           :Sub/subscriber_count
           :Sub/post_count]
   :mod-stats? [:Sub/open_report_count
                :Sub/closed_report_count
                :Sub/all_modmail_count
                :Sub/new_modmail_count
                :Sub/unread_modmail_count
                :Sub/new_unread_modmail_count
                :Sub/in_progress_unread_modmail_count
                :Sub/all_unread_modmail_count
                :Sub/discussion_unread_modmail_count
                :Sub/notification_unread_modmail_count]
   :metadata? [:Sub/creator
               :Sub/restricted
               :Sub/freeform_user_flairs
               :Sub/user_can_flair_self
               :Sub/sub_banned_users_private
               :Sub/sublog_private
               :Sub/user_can_flair
               :Sub/user_must_flair]
   :post-type-config? [:Sub/post_type_config]
   :banned? [:Sub/user_attributes]
   :user-flair? [:Sub/user_attributes]
   :user-flair-choices? [:Sub/user_flair_choices]
   :moderators? [:Sub/moderators]})

(defn- wants
  "Determine which parts of the sub query need to be included."
  [context]
  (->> fields-by-query-section
       (s/transform [s/MAP-VALS] (fn [fields]
                                   (data/wants-fields? context fields)))))

;; Schema Queries

(defn sub-by-name
  "Fetch a sub by its name."
  [{:keys [db current-user] :as context} args _]
  (let [uid (get-uid current-user)
        params (merge {:names [(:name args)]
                       :uid uid}
                      (wants context))
        subdata (query/select-subs-by-name db params)]
    (if (empty? subdata)
      (resolve/with-error nil {:message "Not found"})
      (data/with-superlifter context
        (-> (fixup-sub-fields (first subdata) context)
            prom/promise
            (data/update-trigger! context :Sub/creator :user-bucket
                                  data/inc-threshold))))))

(defn sub-by-reference
  "Fetch a sub by the sid reference in the parent object."
  [context _ {:keys [sid]}]
  (let [params (merge {:sid sid}
                      (wants context))]
    (data/with-superlifter context
      (-> (fetch-sub context params :sub-bucket)
          (data/update-trigger! context :Sub/creator :user-bucket
                                data/inc-threshold)))))

(defn default-subscriptions
  [context _ _]
  (cache/lookup (:cache context) ::default-subscriptions 30
                #(query/default-subs (:db context))))

(defn- fetch-subs
  "Fetch a paginated list of subs."
  [context args]
  (let [subdata (query/list-subs (:db context) args)]
    (if (nil? subdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-sub-fields % context) subdata))))

(defn all-subs
  [{:keys [current-user] :as context} {:keys [first] :as args} _]
  (let [params (-> args
                   pag/one-more
                   (assoc :uid (get-uid current-user))
                   (merge (wants context)))]
    (cond
      (pag/pagination context)
      (resolve/with-error nil {:message "Query too complex"})

      (not (pag/query-size-ok? first))
      (resolve/with-error nil {:message "Too many items requested"})

      :else
      (let [subs (fetch-subs context params)]
        (data/with-superlifter context
          (-> (prom/promise subs)
              (data/update-trigger! context :Sub/creator :user-bucket
                                    data/raise-threshold-by-count)
              (prom/then (fn [result]
                           (pag/resolve-pagination result first
                                                   (list [:Subs first]))))))))))

(defn rules-by-reference
  [{:keys [db]} _ {:keys [sid]}]
  (query/select-rules db {:sid sid}))

(defn flairs-by-reference
  [{:keys [db]} _ {:keys [sid]}]
  (->> (query/select-post-flairs db {:sid sid})
       (map fixup/fix-flair-post-types)))

(defn moderators-by-reference
  [context _ {:keys [::sub/mods]}]
  (data/with-superlifter context
    (-> (map fixup-moderator mods)
        prom/promise
        (data/update-trigger! context :SubModerator/mod :user-bucket
                              data/raise-threshold-by-count))))

(defn post-type-config-by-reference
  [_ _ {:keys [::sub/post-type-config]}]
  post-type-config)

(defn user-attributes-by-reference
  "Resolve the current user's attributes in the sub.
  Uses fields fetched by the parent query."
  [_ _ resolved]
  (select-keys resolved [:banned :user-flair]))

(defn sublog-entries
  "Fetch a sub's most recent log entries.
  This implementation is incomplete and should only be used for testing."
  [{:keys [db] :as context} {:keys [sid]} _]
  (let [first 10
        fixup #(-> %
                   (update :action constants/reverse-sub-log-action-map)
                   (assoc :cursor (pag/make-cursor (:time %))))
        entries (->> (query/select-sublogs db {:sid sid})
                     (map fixup))
        target-count (->> entries
                          (map :target-uid)
                          (remove nil?)
                          count)]
    (data/with-superlifter context
      (-> (prom/promise entries)
          (data/update-trigger! context :SubLogEntry/user :user-bucket
                                data/raise-threshold-by-count)
          (data/update-trigger! context :SubLogEntry/target_user :user-bucket
                                (data/raise-threshold-fn target-count))
          (prom/then
           (fn [result]
             (pag/resolve-pagination result first
                                     (list [:SubLogEntry first]))))))))

;; Mutations

(defn create-rule
  "Create a new sub rule."
  [{:keys [db]} args _]
  (let [result (query/insert-rule db args)]
    (if (empty? result)
      ;; This error could be caused by one of two things: too many
      ;; rules already in the database, and an admin attempting to
      ;; add a rule to a sub that does not exist.  Only the first
      ;; possibility is likely.
      (resolve/with-error nil {:message "Too many rules" :status 400})
      (do
        (log/info {:sid (:sid args) :rule (:text args)} "Created sub rule")
        result))))

(def delete-rule
  "Delete a sub rule."
  {:name ::delete-rule
   :prepare
   [(args/convert-arg-to-int :id)]

   :resolve
   (fn [{:keys [db]} {:keys [sid id]} _]
     (let [result (query/delete-rule db {:sid sid :id id})]
       (when (zero? result)
         (resolve/with-error nil {:message "Rule does not exist"
                                  :status 400}))))})

(def reorder-rules
  "Rearrange the order of rules in a sub."
  {:name ::reorder-rules
   :prepare [(args/convert-arg-to-int-seq :ids)]
   :resolve
   (fn [{:keys [db]} {:keys [sid ids]} _]
     (let [ordering (map-indexed (fn [num id] [num id]) ids)]
       (query/update-rule-order db {:sid sid :ordering ordering})))})

(defn create-post-flair
  "Create a new sub post flair."
  [{:keys [db]} args _]
  (let [result (query/insert-post-flair db args)]
    (if (empty? result)
      ;; This error could also indicate an admin trying to create a flair
      ;; in a sub which does not exist.
      (resolve/with-error nil {:message "Too many flairs" :status 400})
      (assoc result :post-types (keys constants/post-type-map)))))

(def delete-post-flair
  "Delete a sub post flair."
  {:name ::delete-post-flair
   :prepare [(args/convert-arg-to-int :id)]
   :resolve
   (fn [{:keys [db]} {:keys [sid id]} _]
     (let [result (query/delete-post-flair db {:sid sid :id id})]
       (when (zero? result)
         (resolve/with-error nil {:message "Flair does not exist"
                                  :status 400}))))})

(def update-post-flair
  "Update the options of a post flair."
  {:name ::update-post-flair
   :prepare [(args/convert-arg-to-int :id)]
   :resolve
   (fn [{:keys [db]} {:keys [sid id mods-only post-types]} _]
     (let [ptypes (->> post-types
                       (map constants/post-type-map)
                       set)
           all-ptypes (-> constants/post-type-map vals set)
           disallowed-ptypes (set/difference all-ptypes ptypes)]
       (query/update-post-flair db {:sid sid
                                    :id id
                                    :mods-only mods-only
                                    :all-ptypes all-ptypes
                                    :allowed-ptypes ptypes
                                    :disallowed-ptypes disallowed-ptypes})
       nil))})

(def reorder-post-flairs
  "Rearrange the order of post flairs in a sub."
  {:name ::reorder-post-flairs
   :prepare [(args/convert-arg-to-int-seq :ids)]
   :resolve
   (fn [{:keys [db]} {:keys [sid ids]} _]
     (let [ordering (map-indexed (fn [num id] [num id]) ids)]
       (->> {:sid sid :ordering ordering}
            (query/update-post-flair-order db)
            (map fixup/fix-flair-post-types))))})

(def update-post-type-config
  "Update the settings for a post type."
  {:name ::update-post-type-config
   :prepare [(prepare/query-sub :sid :sub {})
             (args/verify-some [:sub :sid] "Not found")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [sid post-type mods-only rules]} _]
     (let [is-admin (boolean (is-admin? current-user))
           ptype (post-type constants/post-type-map)]
       (query/update-post-type-config db {:sid sid
                                          :post-type (-> post-type
                                                         name
                                                         str/lower-case)
                                          :ptype ptype
                                          :mods-only mods-only
                                          :rules rules
                                          :uid (get-uid current-user)
                                          :is-admin is-admin}))
     nil)})

(defn- find-user-flair-id
  "If the user flair is one of the presets, add its id to `args`."
  []
  {:name ::find-user-flair-id
   :func
   (fn [_ {:keys [sub user-flair] :as args}]
     (let [user-flair-choices (::sub/user-flair-choices-with-ids sub)
           user-flair-id (some #(when (= (val %) user-flair)
                                  (-> (key %)
                                      name
                                      Integer/parseInt))
                               (apply merge user-flair-choices))]
       (assoc-in* args :user-flair-id user-flair-id)))})

(defn- verify-self-flair-permitted
  "Check whether the sub allows non-mods to flair themselves."
  []
  {:name ::verify-self-flair-permitted
   :func
   (fn [_ {:keys [sub user-flair user-flair-id] :as args}]
     (let [{:keys [freeform-user-flairs user-can-flair-self]} sub]
       (cond
         ;; This option permits any flair.
         freeform-user-flairs
         args

         ;; This option makes changing flairs mod-only.
         (not user-can-flair-self)
         (fail-verification args "Not authorized" 403)

         ;; Without freeform, deleting and preset choices are
         ;; allowed.
         (or (nil? user-flair) user-flair-id)
         args

         :else
         (fail-verification args "Not authorized" 403))))})

(def change-user-flair
  "Change a user's flair on a sub."
  {:name ::change-user-flair
   :prepare
   [(prepare/query-sub :sid :sub {:metadata? true
                                  :user-flair-choices? true
                                  :banned? true})
    (args/verify-some [:sub :sid] "Not found")
    (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
    (prepare/unless-user-is-mod-or-admin
     :sid (user/verify-current-user-uid-match :uid))
    (find-user-flair-id)
    (prepare/unless-user-is-mod-or-admin :sid (verify-self-flair-permitted))]

   :resolve
   (fn [{:keys [db]} {:keys [sid uid user-flair user-flair-id]} _]
     (let [args' {:uid uid :sid sid
                  :user-flair user-flair
                  :user-flair-id user-flair-id}]
       (if user-flair
         (query/insert-or-update-user-flair db args')
         (query/delete-user-flair db args'))
       user-flair))})

;; TODO can you delete a flair
(def create-user-flair-choice
  "Create a user flair choice on a sub."
  {:name ::create-user-flair-choice
   :prepare [(prepare/query-sub :sid :sub {:user-flair-choices? true})
             (args/verify-some [:sub :sid] "Not found")
             (args/verify-fn [:sub :user-flair-choices] #(< (count %) 100)
                             "Too many flairs" 400)]
   :resolve
   (fn
     [{:keys [db]} {:keys [sid user-flair]} _]
     (let [choices (->> {:sid sid :flair user-flair}
                        (query/insert-user-flair-choice-returning-choices db)
                        (map :flair))]
       {:sid sid
        :user-flair-choices choices}))})

(defn create-sub
  "Create a sub.
  This function is incomplete and should only be used for testing."
  [{:keys [clock current-user db]} {:keys [name title nsfw] :as _args} _]
  (let [ds (:ds db)
        sid (util/uuid4)
        uid (get-uid current-user)
        timestamp (util/sql-now clock)

        ;; Set up default sub metadata.
        meta (mapv (fn [[k v]] {:sid sid :key k :value v})
                   {"mod" uid})]
    (jdbc/with-db-transaction [xds ds]
      (jdbc/insert! xds "public.sub" {:name name
                                      :nsfw nsfw
                                      :sid sid
                                      :title title
                                      :creation timestamp})
      (jdbc/insert-multi! xds "public.sub_metadata" meta)
      (jdbc/insert! xds "public.sub_mod" {:sid sid :uid uid :power_level 0
                                          :invite false})
      (jdbc/insert! xds "public.sub_subscriber" {:sid sid :uid uid
                                                 :status 1
                                                 :time (util/sql-timestamp)})
      (jdbc/insert! xds "public.sub_log"
                    {:action (:CREATE constants/sub-log-action-map)
                     :admin true
                     :uid uid :sid sid
                     :time timestamp})
      (doseq [ptype (keys constants/post-type-map)]
        (jdbc/insert! xds "public.post_type_config"
                      {:sid sid
                       :ptype (ptype constants/post-type-map)
                       :mods_only (= ptype :POLL)
                       :rules ""})))
    {:sid sid
     :name name
     :nsfw nsfw
     :sidebar ""
     :title title
     :sort nil
     :creation timestamp
     :subscriber-count 1
     :post-count 0
     :posts []
     :open-report-count 0
     :closed-report-count 0
     :new-modmail-count 0
     :all-modmail-count 0
     :unread-modmail-count 0}))
