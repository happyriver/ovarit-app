;; gql/elements/sub/prepare.clj -- Prepare sub-related arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.sub.prepare
  (:require
   [ovarit.app.gql.elements.sub.fixup :refer [fixup-sub-fields]]
   [ovarit.app.gql.elements.sub.query :as query]
   [ovarit.app.gql.protocols.current-user :refer [get-uid
                                                  is-admin?
                                                  moderates?]]
   [ovarit.app.gql.resolver-helpers.core :as args :refer [assoc-in*
                                                          get-in*]]))

(defn unless-user-is-mod-or-admin
  "Run a nested argument prep function only if the user is a normal user.
  Check if the user is a moderator using the sid at `sid-path` in `args`"
  [sid-path {:keys [name func]}]
  {:name ::unless-user-is-mod-or-admin
   :func
   (fn [{:keys [current-user] :as context} args]
     (let [sid (get-in* args sid-path)]
       (if (and (not (moderates? current-user sid))
                (not (is-admin? current-user)))
         (-> (func context (assoc args ::args/name name))
             (dissoc ::name))
         args)))})

(defn query-sub
  "Fetch a sub object and add it to `args` at `sub-path`."
  [sid-path sub-path options]
  {:name ::query-sub
   :func
   (fn [{:keys [current-user db] :as context} args]
     (let [sid (get-in* args sid-path)
           params (merge {:sids [sid]
                          :uid (get-uid current-user)} options)
           sub (some-> (query/select-subs-by-sid db params)
                       first
                       (fixup-sub-fields context))]
       (assoc-in* args sub-path sub)))})

(defn query-post-flairs
  "Fetch the list of post flairs for a sub and add it to `args`."
  [sid-path flair-path]
  {:name ::query-post-flairs
   :func
   (fn [{:keys [db]} args]
     (let [sid (get-in* args sid-path)
           flairs (query/select-post-flairs db {:sid sid})]
       (assoc-in* args flair-path flairs)))})
