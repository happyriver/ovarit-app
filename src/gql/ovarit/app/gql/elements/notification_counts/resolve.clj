;; gql/elements/notification_counts/resolve.clj -- GraphQL resolvers for notification-counts for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.notification-counts.resolve
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [go close!]]
   [clojure.walk :as walk]
   [ovarit.app.gql.protocols.current-user :refer [get-uid subs-moderated]]
   [ovarit.app.gql.protocols.notify :as notify]))

(defn make-notification-message-handler
  "Handle a notification count message.
  These may originate with the Python server, in which case for mods
  they only contain the count of unread modmails, or they may come
  from the GraphQL server with a full breakdown of mod stats."
  [callback]
  (fn [content]
    (let [{:strs [messages notifications modmail subs]} (get content "count")
          subs (or (when (seq subs)
                     (walk/keywordize-keys subs))
                   (map (fn [[sid num]]
                          {:sid sid
                           :unread-modmail-count num})
                        modmail))]
      (log/debug {:content content} "Received notification message")
      (callback {:unread-message-count messages
                 :unread-notification-count notifications
                 :sub-notifications subs}))))

(defn make-mod-update-message-handler
  "Handle a moderator report count message."
  [callback]
  (fn [content]
    (let [[sid post-report-count comment-report-count]  (get content "update")
          total (+ (or post-report-count 0) (or comment-report-count 0))]
      (log/debug {:content content
                  :content-type (type content)
                  :keys (str (keys content))
                  :vals (str (vals content))
                  :sid sid
                  :post-report-count post-report-count
                  :comment-report-count comment-report-count}
                 "Received mod-notification message")
      (callback {:sub-notifications [{:sid sid
                                      :open-report-count total}]}))))

(defn stream-notification-updates
  "Stream updates to notification counts, as they are received."
  [{:keys [notify current-user] :as _context} _ source-stream-callback]
  (if-not (get-uid current-user)
    (let [action (go (source-stream-callback nil))]
      #(close! action))
    (let [uid         (get-uid current-user)
          msg-handler (make-notification-message-handler source-stream-callback)
          mod-handler (make-mod-update-message-handler source-stream-callback)
          subs        (->>
                       [(notify/subscribe-user notify :notification uid
                                               msg-handler)
                        (when (seq (subs-moderated current-user))
                          (notify/subscribe-user notify :mod-notification uid
                                                 mod-handler))]
                       (remove nil?))]
      (fn []
        (run! #(notify/close-subscription notify %) subs)))))
