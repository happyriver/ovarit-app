;; gql/elements/link/prepare.clj -- Prepare arguments for resolver execution
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.link.prepare
  (:require
   [ovarit.app.gql.resolver-helpers.core :refer [get-in* fail-verification]]
   [ovarit.app.gql.util :as util]))

(defn verify-url
  "Check that the url argument is parseable and uses http/https."
  [url-path]
  {:name ::verify-url
   :func
   (fn [_ args]
     (let [url (get-in* args url-path)]
       (if (util/valid-protocol? url)
         args
         (fail-verification args "Invalid URL"))))})
