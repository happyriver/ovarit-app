;; elements/link/query.clj -- Database queries for link data
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.link.query
  (:require [hugsql.core :as hugsql]))

(hugsql/def-db-fns "sql/links.sql")

;; reasons to keep url metadata in database:
;; - oembed links
;; - using the canonical url to check for duplicate postings
;; - diagnosing failed grab titles

(comment
  (require 'user)
  (def db (:db user/system))
  (recent-url-metadata db {:url "https://youtu.be/SDsQdvr_1e0"
                           :age 36})
  )
