;; elements/link/resolve.clj -- GraphQL resolvers for link data
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.link.resolve
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [go close!]]
   [clojure.java.jdbc :as jdbc]
   [ovarit.app.gql.elements.link.prepare :as prepare]
   [ovarit.app.gql.elements.link.tasks :as tasks]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.protocols.pubsub :as pubsub]))

;; Subscriptions

(defn- select-metadata-keys
  [result]
  (select-keys result [:title :description :content-type]))

(defn- make-url-metadata-handler
  "Handle a url metadata result."
  [callback]
  (fn [content]
    (log/info {:content content} "Received url metadata message")
    (-> content
        select-metadata-keys
        callback)
    (callback nil)))

(def stream-url-metadata
  "Fetch metadata from an external website and stream it when ready."
  {:name ::url-metadata
   :prepare [(prepare/verify-url :url)]
   :stream
   (fn [{:keys [db cache job-queue pubsub]}
        {:keys [url]} source-stream-callback]
     (let [cache-key (tasks/url-metadata-cache-key url)
           recent (cache/cache-get cache cache-key)]
       (if recent
         (let [action (go (-> recent
                              select-metadata-keys
                              source-stream-callback)
                          (source-stream-callback nil))]
           (log/info {:content recent} "Using cached metadata result")
           #(close! action))
         (let [handler (make-url-metadata-handler source-stream-callback)
               sub (pubsub/subscribe pubsub cache-key handler)]
           (jdbc/with-db-transaction [dbx (:ds db)]
             (job-queue/enqueue! job-queue dbx ::tasks/fetch-url-metadata
                                 {:url url}))
           #(pubsub/close-subscription pubsub sub)))))})
