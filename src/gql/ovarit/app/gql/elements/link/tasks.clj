;; elements/link/tasks.clj -- Async link processing for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.link.tasks
  (:require
   [clojure.java.jdbc :as jdbc]
   [ovarit.app.gql.elements.link.query :as query]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.protocols.pubsub :as pubsub]
   [ovarit.app.gql.util.url-metadata :as url-metadata]
   [pandect.algo.sha256 :refer [sha256]]))

(def url-metadata-cache-timeout
  "Time in seconds to cache url metadata for fetches by subscription."
  3600)

(def cache-key-prefix
  (let [kw ::url-metadata]
    (str (namespace kw) "/" (name kw) "/")))

(defn url-metadata-cache-key
  [url]
  (str cache-key-prefix (sha256 url)))

(defn update-link-metadata
  [{:keys [db cache pubsub]} url]
  (let [cache-key (url-metadata-cache-key url)
        metadata (url-metadata/get-metadata url)]
    (jdbc/with-db-transaction [xds (:ds db)]
      (let [xdb (assoc db :ds xds)
            recs (->> (select-keys metadata [:title :content-type
                                             :description :url :site-name
                                             :oembed-xml :oembed-json])
                      (filter #(some? (val %)))
                      (map (fn [[k v]]
                             [(name k) v])))
            all-recs (concat recs
                             (map (fn [img] ["image" img])
                                  (:images metadata))
                             (map (fn [icon] ["icon" icon])
                                  (:icons metadata)))]
        (if (seq all-recs)
          (query/insert-url-metadata xdb {:url url
                                          :meta all-recs})
          (query/insert-url-metadata-error xdb
                                           {:url url
                                            :error (:error metadata)}))
        (cache/cache-set cache cache-key metadata
                         url-metadata-cache-timeout)
        (pubsub/publish pubsub cache-key metadata)))))

(defmethod job-queue/handle-job! ::fetch-url-metadata
  [job-queue _job-type {:keys [url]}]
  (update-link-metadata job-queue url))

(comment
  ;; elements/link handles all link metadata
  ;; graphql:
  ;; mutation: submit_link
  ;;    - give it a link
  ;;    - returns an id
  ;;    - starts work on thumbnail, title
  ;; subscription: link_title
  ;;    - give it an id
  ;;    - get back title
  ;; subscription: link_thumbnail
  ;;    - id -> title
  )


(comment
  (require 'user)
  (def db (:db user/system))
  (hugsql/def-db-fns "sql/dev/.sql")
  (query/select-links db {:lim 5})

  (import '[java.util.concurrent Executors])
  (def stop (atom false))
  (defn update-links
    "Fetch metadata for links without it."
    [{:keys [db]}]
    (let [db-with-counter (assoc db :counter (atom 0))
          pool  (Executors/newFixedThreadPool 25)
          links (->> (select-links db-with-counter {:lim 100000})
                     shuffle
                     (take 1000))
          tasks (map (fn [{:keys [url]}]
                       #(when-not @stop
                          (update-link-metadata db-with-counter url)))
                     links)]
      (doseq [future (.invokeAll pool tasks)]
        (.get future))
      (.shutdown pool)))

  (def task (future (update-links {:db db})))
  (reset! stop true)
  @stop
  (Thread/activeCount)
  )
