;; gql/elements/config/query.clj -- SQL queries for config for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.config.query
  (:require
   [clojure.spec.alpha :as spec]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.config.spec :as config-spec]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn]]
   [ovarit.app.gql.elements.spec.db :as db-spec]))

(hugsql/def-db-fns "sql/config.sql")

(wrap-db-fn select-metadata-by-keys)
(db-spec/fdef select-metadata-by-keys
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::config-spec/keys]))
  :ret (spec/coll-of
        (spec/keys :req-un [::config-spec/key
                            ::config-spec/value])))
