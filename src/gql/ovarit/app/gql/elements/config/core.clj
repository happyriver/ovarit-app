;; gql/elements/config/core.clj -- Configuration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.config.core
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.string :as str]
   [dotenv]
   [ovarit.app.gql.elements.config.tasks :as tasks]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.util :as util]))

(defn get-stored-config
  "Make the database query to get config fields, and cache the results."
  [{:keys [env cache] :as context}]
  (or (and (not= :test env)
           (-> (cache/cache-get cache ::site-config)
               ;; Prevent 40 seconds of crashing after next deploy
               util/kebab-case-keys))
      (tasks/fetch-config context)))

(defn clear-cached-announcement
  [{:keys [cache]}]
  (cache/cache-del cache ::site-config)
  (cache/cache-del cache "flask_cache_app.misc.getAnnouncementPid_memver")
  (cache/cache-del cache "flask_cache_app.misc.getAnnouncement_memver"))

(def default-footer
  {"About Ovarit" "/wiki/about"
   "Sitewide Rules" "/wiki/rules"
   "Sitewide Guidelines" "/wiki/guidelines"
   "Terms of Service" "/wiki/tos"
   "Privacy Policy" "/wiki/privacy"
   "License" "/license"})

(def default-donate-presets [5 10 20])

(def default-expando-sites
  ["youtube.com"
   "www.youtube.com"
   "youtu.be"
   "gfycat.com"
   "streamja.com"
   "streamable.com"
   "player.vimeo.com"
   "vimeo.com"])

(defn json-from-env
  "If `envvar` is set, decode its value from JSON and return the result."
  [envvar]
  (when-let [value (dotenv/env envvar)]
    (try
      (cheshire/parse-string value)
      (catch Exception e
        (log/error {:exception-message (.getMessage e)
                    :envvar envvar}
                   "JSON parse error")
        nil))))

(defn enforce-footer-rules
  "Require some minimal content in the page footer."
  [footer]
  (if footer
    (assoc footer "License" (get default-footer "License"))
    default-footer))

(defn make-footer-list [footer]
  (reduce-kv (fn [result k v]
               (conj result {:name k :link v})) [] footer))

(defn load-mottos [filename]
  (let [contents (when filename
                   (try (slurp filename)
                        (catch Exception e
                          (log/warn {:filename filename
                                     :exception-message (.getMessage e)}
                                    "Unable to read mottos file")
                          nil)))]
    (if (or (empty? contents) (str/blank? contents))
      []
      (try
        (cheshire/parse-string contents)
        (catch Exception _
          (str/split-lines contents))))))

(defn site-logo
  "Logo read from a file, if the name is supplied by :SITE_LOGO"
  []
  (let [logo-file-name (dotenv/env :SITE_LOGO)]
    (when logo-file-name
      (try
        (slurp logo-file-name)
        (catch java.io.FileNotFoundException _
          (log/error {:filename logo-file-name} "SITE_LOGO file not found")
          nil)))))

(defn build-site-config
  "The response to the site_configuration query."
  []
  {:sub-prefix (or (dotenv/env "SITE_SUB_PREFIX") "o")
   :footer (->
            (or (json-from-env "SITE_FOOTER_LINKS")
                default-footer)
            enforce-footer-rules
            make-footer-list)
   :last-word (or (dotenv/env "SITE_LAST_WORD") "")
   :logo (site-logo)
   :enable-totp (some? (#{"True" "true" "1"} (dotenv/env "SITE_ENABLE_TOTP")))
   :donate-presets (or (json-from-env "SITE_DONATE_PRESETS")
                       default-donate-presets)
   :donate-minimum (or (util/env-integer "SITE_DONATE_MINIMUM") 1)
   :thumbnail-url (or (dotenv/env "STORAGE_THUMBNAILS_URL") "/files")
   :metadata-image (dotenv/env "SITE_METADATA_IMAGE")
   :metadata-image-width (dotenv/env "SITE_METADATA_IMAGE_WIDTH")
   :metadata-image-height (dotenv/env "SITE_METADATA_IMAGE_HEIGHT")
   :expando-sites (or (json-from-env "SITE_EXPANDO_SITES")
                      default-expando-sites)
   ::fathom {:data-site (dotenv/env "FATHOM_DATA_SITE")
             :goal (dotenv/env "FATHOM_GOAL")}
   :upload-max-size (or (util/env-integer "SITE_UPLOAD_MAX_SIZE") 16777216)
   :software
   [{:name "Ovarit App"
     :license-name "GNU AGPL 3.0 or later"
     :license-link "https://gnu.org/licenses/agpl-3.0.html"
     :source-code-link "https://gitlab.com/happyriver/ovarit-app"}
    {:name "Throat"
     :license-name "MIT"
     :license-link
     "https://gitlab.com/feminist-conspiracy/throat/-/blob/master/LICENSE"
     :source-code-link "https://gitlab.com/feminist-conspiracy/throat"}]
   :languages (or (json-from-env "APP_LANGUAGES") ["en"])
   :mottos (load-mottos (dotenv/env "SITE_MOTTOS"))})

(def built-site-config (build-site-config))

(defn site-config
  [{:keys [env] :as context}]
  (try
    (merge (if (#{:dev :test} env) (build-site-config) built-site-config)
           (get-stored-config context))
    (catch Exception e
      (log/error {:expound (util/expound-str (ex-data e) context)}
                 e "Error fetching site configuration"))))
