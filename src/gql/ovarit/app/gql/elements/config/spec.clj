;; gql/elements/config/spec.clj -- Specs for config for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.config.spec
  (:require
   [clojure.spec.alpha :as spec]))

(spec/def ::key #{"announcement"
                  "banned_domain"
                  "banned_email_domain"
                  "banned_username_string"
                  "best_comment_sort_init"
                  "default"
                  "site.admin_sub"
                  "site.allow_uploads"
                  "site.allow_video_uploads"
                  "site.anonymous_modding"
                  "site.archive_post_after"
                  "site.archive_sticky_posts"
                  "site.block_anon_stalking"
                  "site.changelog_sub"
                  "site.copyright"
                  "site.daily_site_posting_limit"
                  "site.daily_sub_posting_limit"
                  "site.description"
                  "site.downvotes.count"
                  "site.downvotes.days"
                  "site.downvotes.personal.count"
                  "site.downvotes.personal.days"
                  "site.downvotes.silence"
                  "site.downvotes.thread.comments"
                  "site.downvotes.thread.percent"
                  "site.edit_history"
                  "site.enable_chat"
                  "site.enable_modmail"
                  "site.enable_posting"
                  "site.enable_registration"
                  "site.force_sublog_public"
                  "site.front_page_submit"
                  "site.funding_goal"
                  "site.invitations_visible_to_users"
                  "site.invite_level"
                  "site.invite_max"
                  "site.lema"
                  "site.link_post.remove_timeout"
                  "site.name"
                  "site.notifications_on_icon"
                  "site.nsfw.anon.blur"
                  "site.nsfw.anon.show"
                  "site.nsfw.new_user_default.blur"
                  "site.nsfw.new_user_default.show"
                  "site.placeholder_account"
                  "site.recent_activity.comments_only"
                  "site.recent_activity.defaults_only"
                  "site.recent_activity.enabled"
                  "site.recent_activity.max_entries"
                  "site.require_invite_code"
                  "site.self_voting.comments"
                  "site.self_voting.posts"
                  "site.send_pm_to_user_min_level"
                  "site.show_votes"
                  "site.sitelog_public"
                  "site.sub_creation_admin_only"
                  "site.sub_creation_min_level"
                  "site.sub_ownership_limit"
                  "site.text_post.max_length"
                  "site.text_post.min_length"
                  "site.title_edit_timeout"
                  "site.top_posts.show_score"
                  "site.upload_min_level"
                  "site.username_change.display_days"
                  "site.username_change.limit"
                  "site.username_change.limit_days"
                  "site.username_max_length"})
(spec/def ::value string?)
(spec/def ::keys (spec/coll-of ::key))
