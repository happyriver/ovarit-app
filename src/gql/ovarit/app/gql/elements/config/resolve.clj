;; gql/elements/config/resolve.clj -- Schema resolvers for the config queries
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.config.resolve
  (:require
   [cambium.core :as log]
   [clojure.set :as set]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [ovarit.app.gql.elements.config.core :as config]))

(defn site-configuration
  "Resolve the site-configuration query."
  [{:keys [env] :as context} _ _]
  (let [conf (if (#{:dev :test} env)
               (config/build-site-config)
               config/built-site-config)]
    (try
      (-> conf
          (merge (config/get-stored-config context))
          (set/rename-keys {:footer ::footer
                            :software ::software}))
      (catch Exception e
        (log/error e "Error in config resolver")
        (let [err (resolve/with-error
                    "" {:message "Internal Server Error"})]
          (assoc conf :name err :lema err :copyright err))))))

(defn footer-link-list-by-reference
  [_ _ {:keys [::footer]}]
  footer)

(defn software-list-by-reference
  [_ _ {:keys [::software]}]
  software)
