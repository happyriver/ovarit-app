;; gql/elements/user/query.clj -- SQL queries for User data
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.query
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fn
                                              wrap-db-fns
                                              wrap-snip-fn
                                              subscription-status-map]]
   [ovarit.app.gql.elements.message.spec :as message-spec]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.spec.db :as db-spec]
   [ovarit.app.gql.elements.sql :refer [start-cte-snip
                                        end-cte-snip]]
   [ovarit.app.gql.elements.user.spec :as user-spec]
   [ovarit.app.gql.util :as util])
  (:require
   [ovarit.app.gql.elements.user.spec.require-name-change
    :as-alias require-name-change]
   [ovarit.app.gql.elements.user.spec.update-preferences
    :as-alias update-preferences]))

(hugsql/def-db-fns "sql/user.sql")

;;; Specs for snippet functions.

(spec/def ::where-snip ::db-spec/sqlvec)

(wrap-snip-fn badges-cte-snip)
(db-spec/fdef badges-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::where-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn badges-field-snip)
(db-spec/fdef badges-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn badges-join-snip)
(db-spec/fdef badges-join-snip
  :ret ::db-spec/sqlvec)

(spec/def ::is-admin? boolean?)
(wrap-snip-fn by-name-snip)
(db-spec/fdef by-name-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/name
                                            ::is-admin?]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn by-uids-snip)
(db-spec/fdef by-uids-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn content-blocks-field-snip)
(db-spec/fdef content-blocks-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-fields-snip)
(db-spec/fdef meta-fields-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-cte-snip)
(db-spec/fdef notification-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-field-snip)
(db-spec/fdef notification-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-join-snip)
(db-spec/fdef notification-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn personal-field-snip)
(db-spec/fdef personal-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-cte-snip)
(db-spec/fdef score-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::where-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-field-snip)
(db-spec/fdef score-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-join-snip)
(db-spec/fdef score-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn subs-moderated-field-snip)
(db-spec/fdef subs-moderated-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn subscription-field-snip)
(db-spec/fdef subscription-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn username-history-field-snip)
(db-spec/fdef username-history-field-snip
  :ret ::db-spec/sqlvec)

;;; Specs for db functions.
(wrap-db-fns _delete-subscription-status
             _update-subscription-status
             insert-user
             select-customer-update-log
             uid-by-name
             update-customer-update-log)

(wrap-db-fn _select-users)
(spec/def ::start-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::end-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::subscription-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::meta-fields-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::subs-moderated-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::content-blocks-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-join-snip (spec/nilable ::db-spec/sqlvec))
(db-spec/fdef _select-users
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::badges-cte-snip
                                            ::badges-field-snip
                                            ::badges-join-snip
                                            ::content-blocks-field-snip
                                            ::end-cte-snip
                                            ::meta-fields-snip
                                            ::notification-cte-snip
                                            ::notification-field-snip
                                            ::notification-join-snip
                                            ::personal-field-snip
                                            ::score-cte-snip
                                            ::score-field-snip
                                            ::score-join-snip
                                            ::start-cte-snip
                                            ::subs-moderated-field-snip
                                            ::subscription-field-snip
                                            ::where-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-spec/given
                            ::user-spec/joindate
                            ::user-spec/name
                            ::user-spec/status
                            ::user-spec/uid]
                   :opt-un [
                            ;; badges-field-snip
                            ::user-spec/badges
                            ;; content-blocks-field-snip
                            ::user-spec/content-blocks
                            ;; meta-fields-snip
                            ::user-spec/meta
                            ;; notification-field-snip
                            ::user-spec/unread-message-count
                            ::user-spec/unread-notification-count
                            ;; personal-field-snip
                            ::user-spec/email
                            ::user-spec/language
                            ::user-spec/resets
                            ;; score-field-snip
                            ::user-spec/score
                            ::user-spec/level
                            ::user-spec/progress
                            ;; subs-moderated-field-snip
                            ::user-spec/mods
                            ;; subscription-field-snip
                            ::user-spec/subs])))

(spec/def ::update-preferences/datetime ::element-spec/timestamp)
(spec/def ::update-preferences/prefs
  (spec/coll-of (spec/tuple string? string?)))
(wrap-db-fn update-preferences)
(db-spec/fdef update-preferences
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::user-spec/language
                                            ::update-preferences/prefs]))
  :ret (spec/coll-of
        (spec/keys :req-un [::update-preferences/datetime])))

(spec/def ::require-name-change/id int?)
(spec/def ::require-name-change/message string?)
(spec/def ::require-name-change/admin-uid ::user-spec/uid)
(spec/def ::require-name-change/old-name ::user-spec/name)
(spec/def ::require-name-change/current-name ::user-spec/name)
(spec/def ::require-name-change/admin-name ::user-spec/name)
(spec/def ::require-name-change/notification-mtid ::message-spec/mtid)
(spec/def ::require-name-change/datetime ::element-spec/timestamp)
(spec/def ::require-name-change/required-at ::element-spec/timestamp)
(wrap-db-fn select-required-name-change)
(db-spec/fdef select-required-name-change
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::user-spec/name]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-spec/uid
                            ::require-name-change/admin-uid
                            ::require-name-change/message
                            ::require-name-change/required-at])))

(wrap-db-fn insert-required-name-change)
(db-spec/fdef insert-required-name-change
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un
                                   [::user-spec/uid
                                    ::require-name-change/admin-uid
                                    ::require-name-change/message
                                    ::require-name-change/datetime]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-spec/uid
                            ::require-name-change/admin-uid
                            ::require-name-change/message
                            ::require-name-change/required-at])))

(wrap-db-fn delete-required-name-change)
(db-spec/fdef delete-required-name-change
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::require-name-change/admin-uid
                                            ::require-name-change/datetime]))
  :ret int?)

(wrap-db-fn select-required-name-change-for-notification)
(db-spec/fdef select-required-name-change-for-notification
  :args (spec/cat :db ::db-spec/db
                  :args map?)
  :ret (spec/coll-of
        (spec/keys :req-un [::require-name-change/id
                            ::user-spec/uid
                            ::require-name-change/old-name
                            ::require-name-change/current-name
                            ::require-name-change/admin-name
                            ::require-name-change/message])))

(wrap-db-fn update-required-name-change)
(db-spec/fdef update-required-name-change
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::require-name-change/id
                                            ::require-name-change/notification-mtid]))
  :ret int?)

(defn- snips-for-query
  [{:keys [attributes? badges? content-block? notification-counts?
           personal? score? subs-moderated? subscriptions?
           username-history? uid uids] :as args}
   where-snip]
  (let [use-cte? (or notification-counts? score? badges?)]
    (assoc args
           :start-cte-snip            (when use-cte? (start-cte-snip))
           :end-cte-snip              (when use-cte? (end-cte-snip))
           :where-snip                where-snip
           :badges-cte-snip           (when (or badges? score?)
                                        (badges-cte-snip
                                         {:uids uids
                                          :where-snip where-snip}))
           :badges-field-snip         (when badges? (badges-field-snip))
           :badges-join-snip          (when badges? (badges-field-snip))
           :content-blocks-field-snip (when content-block?
                                        (content-blocks-field-snip))
           :meta-fields-snip          (when attributes? (meta-fields-snip))
           :notification-cte-snip     (when notification-counts?
                                        (notification-cte-snip {:uid uid}))
           :notification-field-snip   (when notification-counts?
                                        (notification-field-snip))
           :notification-join-snip    (when notification-counts?
                                        (notification-join-snip))
           :personal-field-snip       (when personal? (personal-field-snip))
           :subs-moderated-field-snip (when subs-moderated?
                                        (subs-moderated-field-snip))
           :subscription-field-snip   (when subscriptions?
                                        (subscription-field-snip))
           :score-cte-snip            (when score?
                                        (score-cte-snip
                                         {:uids uids
                                          :where-snip where-snip}))
           :score-field-snip          (when score? (score-field-snip))
           :score-join-snip           (when score? (score-join-snip))
           :username-history-field-snip (when username-history?
                                          (username-history-field-snip)))))

(defn- fix-username-history-timestamps
  [user]
  (s/transform [:username-history s/ALL :changed]
               util/sql-timestamp-from-epoch user))

(defn list-users-by-uid [db args]
  (->> (snips-for-query args (by-uids-snip args))
       (_select-users db)
       (map fix-username-history-timestamps)))

(defn select-user-by-name [db {:keys [name uid is-admin?] :as args}]
  (let [params {:name (str/lower-case name)
                :uid uid
                :is-admin? (boolean is-admin?)}]
    (->> (snips-for-query args (by-name-snip params))
         (_select-users db)
         (map fix-username-history-timestamps)
         first)))

(defn user-by-uid-with-notification-counts
  "Get a user record along with notification counts and modded subs."
  [db {:keys [uid]}]
  (first (list-users-by-uid db {:uid uid
                                :uids [uid]
                                :notification-counts? true
                                :subs-moderated? true})))

(defn update-subscription-status
  "Subscribe to or block a sub, and update its subscription count."
  [db {:keys [status] :as args}]
  (let [adjust (if (= (subscription-status-map :SUBSCRIBED) status) 1 0)]
    (_update-subscription-status db (assoc args :adjust adjust))))

(defn delete-subscription-status
  "Unsubscribe to or unblock a sub, and update its subscription count."
  [db {:keys [status] :as args}]
  (let [adjust (if (= (subscription-status-map :SUBSCRIBED) status) 1 0)]
    (_delete-subscription-status db (assoc args :adjust adjust))))

(comment
  (require 'user)
  (def db (:db user/system))
  (select-user-by-name db {:name "lanewilliam"
                           :score? true
                           :subscriptions? true
                           :notification-counts? true
                           :attributes? true
                           :subs-moderated? true
                           :uid "e7d7004c-fad8-4b58-8488-9c640fe6ec8e"})
  (snips-for-query {:subscriptions? true})

  (insert-require-name-change db
                              {:uid "4fb76c1c-55c4-4f60-812a-f984331c0ddc"
                               :admin-uid "13776e4f-3b4d-4211-95ef-9e2cd906bcf7"
                               :datetime (util/sql-timestamp)
                               :message "testing"
                               })

  )
