;; sql/elements/user/prepare.clj -- Prepare user-related arguments for resolver execution
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.prepare
  (:require
   [ovarit.app.gql.elements.user.query :as query]
   [ovarit.app.gql.protocols.current-user :refer [get-uid can-admin?]]
   [ovarit.app.gql.resolver-helpers.core :as prepare
    :refer [assoc-in* get-in* fail-verification]]))

(defn verify-current-user-uid-match
  "Fail argument verification unless `uid-path` matches the current user uid."
  [uid-path]
  {:name ::current-user-uid-match
   :func
   (fn [{:keys [current-user]} args]
     (let [uid (get-in* args uid-path)]
       (if (and uid (= uid (get-uid current-user)))
         args
         (fail-verification args "Not authorized" 403))))})

(defn unless-current-user-uid-match
  "Run a nested argument prep function only if the user's uid does not match.
  Use the uid at `uid-path` in `args`"
  [uid-path {:keys [name func]}]
  {:name ::unless-current-user-uid-match
   :func
   (fn [{:keys [current-user] :as context} args]
     (let [uid (get-in* args uid-path)]
       (if (not= uid (get-uid current-user))
         (-> (func context (assoc args ::name name))
             (dissoc ::name))
         args)))})

(defn query-uid-by-name
  "Get the uid for a user by name."
  [username-path uid-path]
  {:name :query-uid-by-name
   :func
   (fn [{:keys [db]} args]
     (let [username (get-in* args username-path)
           uid (when username
                 (:uid (query/uid-by-name db {:name username})))]
       (assoc-in* args uid-path uid)))})

(defn verify-language-valid
  "Fail argument verification if the language is not configured."
  [language-path]
  {:name ::verify-language-valid
   :func
   (fn [{:keys [site-config]} args]
     (let [language (get-in* args language-path)]
       (if (or (nil? language) ((set (:languages (site-config))) language))
         args
         (fail-verification args "Language not supported" 400))))})

(defn verify-admin-dm-block
  "Fail argument verification if an admin is trying to block DMs."
  [block-dms-path]
  {:name ::verify-admin-dm-block
   :func
   (fn [{:keys [current-user]} args]
     (if (and (can-admin? current-user) (get-in* args block-dms-path))
       (fail-verification args)
       args))})
