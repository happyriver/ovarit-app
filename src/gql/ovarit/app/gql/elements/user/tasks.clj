;; gql/elements/user/tasks.clj -- Task descriptions for ovarit-app users
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.tasks
  (:require
   [cambium.core :as log]
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [ovarit.app.gql.elements.message.tasks :as message-tasks]
   [ovarit.app.gql.elements.user.query :as query]
   [ovarit.app.gql.protocols.payment-subscription :as subscription]
   [ovarit.app.gql.util :as util]))

(defn update-customers
  "Update the customer update log."
  [{:keys [db options]}]
  (let [subscription (:payment-subscription options)
        db-with-counter (assoc db :counter (atom 0))
        log-entries (query/select-customer-update-log db-with-counter)]
    (log/info {:count (count log-entries)} "Updating customer subscriptions")
    (doseq [entry log-entries]
      (let [{:keys [id action uid resets value]} entry
            user {:uid uid :resets resets}
            result (cond
                     (= "change_email" action)
                     (subscription/update-email subscription user value)

                     (= "cancel_subscription" action)
                     (subscription/cancel subscription user)

                     :else
                     (do
                       (log/error {:customer-update-log entry}
                                  "Unrecognized action in customer_update_log")
                       :failure))]
        (when (not= :retry result)
          (query/update-customer-update-log db-with-counter
                                            {:id id
                                             :completed (util/sql-timestamp)
                                             :success (= :success result)}))))))

(defn- send-name-change-notification
  [{:keys [db tr] :as context}
   {:keys [id uid old-name current-name admin-name message]}]
  (let [msg (message-tasks/notify-admins
             context
             {:uid uid
              :action :REQUIRED_NAME_CHANGE
              :subject (tr "Required name change by /u/%s" old-name)
              :content
              (str/join "\n\n"
                        [(->
                          (tr "The user formerly known as %s has
                               changed their username to [%s](/u/%s)
                               following a name change requirement."
                              old-name current-name current-name)
                          util/condense-whitespace)
                         (tr "Requiring admin: [%s](/u/%s)"
                             admin-name admin-name)
                         (tr "Reason: %s" message)])})]
    (query/update-required-name-change db
                                       {:id id
                                        :notification-mtid (:mtid msg)})))

(defn notify-required-name-changes
  "Notify admins when required name changes are completed."
  [{:keys [db] :as context}]
  (jdbc/with-db-transaction [xds (:ds db)]
    (let [db-with-counter (assoc db :ds xds :counter (atom 0))
          name-change (-> db-with-counter
                          (query/select-required-name-change-for-notification
                           {})
                          first)]
      (when name-change
        (log/info {:name-change (->> [:uid :old-name :current-name]
                                     (select-keys name-change))}
                  "Completed required name change pending notification")
        (-> (assoc context :db db-with-counter)
            (send-name-change-notification name-change))))))
