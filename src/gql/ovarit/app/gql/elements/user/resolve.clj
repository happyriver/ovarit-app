;; gql/elements/user/resolve.clj -- GraphQL resolvers for User data
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.resolve
  "Contains custom resolvers for Users."
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [go close!]]
   [clojure.java.jdbc :as jdbc]
   [clojure.set :as set]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.message.tasks :as message-tasks]
   [ovarit.app.gql.elements.user.prepare :as prepare]
   [ovarit.app.gql.elements.user.query :as query]
   [ovarit.app.gql.protocols.bus :as bus]
   [ovarit.app.gql.protocols.current-user :refer [get-uid is-admin?
                                                  subs-moderated
                                                  totp-expiration
                                                  user-if-realized]]
   [ovarit.app.gql.protocols.job-queue :as job-queue]
   [ovarit.app.gql.protocols.notify :as notify]
   [ovarit.app.gql.protocols.schema :as schema]
   [ovarit.app.gql.resolver-helpers.data :as data]
   [ovarit.app.gql.util :as util]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher]  :as superlifter]))

;;; Data Fetchers and fixers

(defn- prune-username-history
  "Remove entries older than the configured number of days.
  Admins, mods, and users looking at themselves can see old entries."
  [history {:keys [uid]} current-user {days :username-change-display-days}]
  (if (or (= uid (get-uid current-user))
          (is-admin? current-user)
          (seq (subs-moderated current-user)))
    history
    (let [now (jt/zoned-date-time)
          limit (jt/minus now (jt/days days))]
      (filter (fn [entry]
                (jt/after? (-> (:changed entry)
                               (jt/zoned-date-time "UTC"))
                           limit))
              history))))

(defn fixup-user
  "Make the database user fields match the query user fields."
  [{:keys [uid status] :as user}
   {:keys [current-user site-config]}]
  (-> (if (keyword? status)
        user
        (update user :status constants/reverse-user-status-map))
      (update :username-history
              prune-username-history user current-user (site-config))
      (update :language #(when (seq %) %))
      (set/rename-keys {:subs ::subs
                        :mods ::mods
                        :meta ::meta
                        :content-blocks ::content-blocks
                        :username-history ::username-history})
      (assoc-in [::meta :uid] uid)))

(defn load-users-by-uid
  "Load users for the superfetcher."
  [selectors {:keys [db current-user]}]
  (let [uids (map :uid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:uids uids
                       :uid (get-uid current-user)})]
    (query/list-users-by-uid db params)))

(def-superfetcher FetchUserByUid [id]
  (data/make-fetcher-fn :uid load-users-by-uid fixup-user))

(defn- fetch-user
  "Promise to fetch a user by uid.
  Return a promesa promise"
  [context select bucket]
  (let [id (data/id-from-fetch-args select)]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (->FetchUserByUid id)))))

;;; Directive support

(defmethod schema/redact-content ::name-of-deleted-user
  [_ _ user roles]
  (if (and (= (:status user) :DELETED) (not (roles :IS_ADMIN)))
    (assoc user :name nil)
    user))

;;; Fields and query construction

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the user queries."
  {:subscriptions?       [:User/subscriptions]
   :score?               [:User/score
                          :User/level
                          :User/progress]
   :notification-counts? [:User/unread_message_count
                          :User/unread_notification_count]
   :attributes?          [:User/attributes]
   :subs-moderated?      [:User/subs_moderated]
   :content-block?       [:User/content_blocks]
   :personal?            [:User/language :User/resets]
   :username-history?    [:User/username_history]})

(defn- wants
  "Determine which parts of the user query need to be included."
  [context]
  (s/transform [s/MAP-VALS] (fn [fields]
                              (data/wants-fields? context fields))
               fields-by-query-section))

;; Not a resolver, but produces a cleaned-up user object for load-user.
(defn resolved-user
  "Get a user object, for use by load-user.
  Has everything except the notification counts"
  [{:keys [db]} {:keys [uid]}]
  (let [want-all-fields (into {} (map #(vector % true)
                                      (keys fields-by-query-section)))
        params (-> {:uid uid :uids [uid]}
                   (merge want-all-fields)
                   (assoc :notification-counts? false))]
    (-> (query/list-users-by-uid db params)
        first
        (update :status constants/reverse-user-status-map))))

;;; Schema Queries

(defn user-by-name
  [{:keys [current-user db] :as context} args _]
  (let [args (-> args
                 (merge (wants context))
                 (assoc :uid (get-uid current-user)
                        :name (:name args)
                        :is-admin? (is-admin? current-user)))]
    (-> (query/select-user-by-name db args)
        (fixup-user context))))

(defn user-by-uid
  [context args _]
  (let [params (merge args (wants context))]
    (fetch-user context params :immediate)))

(defn user-by-reference
  "Resolve a user object based on results of the parent resolver.
  If ::user is bound, it contains a user object that has been
  prefetched, otherwise fetch the user using the uid at :uid."
  [context _ {:keys [uid ::user]}]
  (or user
      (when uid
        (let [params (merge {:uid uid} (wants context))]
          (fetch-user context params :user-bucket)))))

(defn user-list-by-reference
  [context _ {:keys [uids]}]
  (let [select (wants context)
        fetch-one (fn [uid]
                    (let [args (assoc select :uid uid)]
                      (fetch-user context args :user-bucket)))]
    (prom/all (map fetch-one uids))))

(defn attributes-by-reference
  "Resolve the user's attributes.
  Uses metadata fetched by the resolved parent :User query."
  [{:keys [current-user]} _ {:keys [::meta]}]
  (-> meta
      (set/rename-keys {:admin      :can-admin
                        :labrat     :lab-rat
                        :nochat     :no-chat})
      (util/convert-fields-to-boolean [:can-admin
                                       :lab-rat
                                       :no-chat
                                       :no-collapse
                                       :nsfw
                                       :nsfw-blur
                                       :block-dms])
      (as-> $ (assoc $ :enable-collapse-bar (not (:no-collapse $))))
      (assoc :totp-expiration (some-> (totp-expiration current-user)
                                      str))))

(defn content-blocks-by-reference
  "Resolve the user's content blocks.
  Use data fetched by the resolved parent :User query."
  [context _ {:keys [::content-blocks]}]
  (let [blocks (->> content-blocks
                    (map #(update % :content-block
                                  constants/reverse-user-content-block-map)))]
    (data/with-superlifter context
      (-> (prom/promise blocks)
          (data/update-trigger! context :UserContentBlock/user :user-bucket
                                data/raise-threshold-by-count)))))

(defn subscriptions-by-reference
  "Resolve the user's subscriptions.
  Uses the list fetched by the resolved parent :User query."
  [_ _ {:keys [::subs]}]
  (->> subs
       (remove nil?)
       (map #(update % :status constants/reverse-subscription-status-map))))

(defn username-history-by-reference
  "Resolve the username history.
  Uses the resolved parent :User query."
  [_ _ {:keys [::username-history]}]
  username-history)

(defn moderates-by-reference
  "Resolve the subs the user moderates.
  Uses the list fetched by the resolved parent :User query."
  [context _ {:keys [::mods]}]
  (let [fixup-submod (fn [{:keys [sid power-level]}]
                       {:sid sid
                        :moderation-level
                        (constants/reverse-moderation-level-map power-level)})]
    (data/with-superlifter context
      (-> (prom/promise (map fixup-submod mods))
          (data/update-trigger! context :SubModeration/sub :sub-bucket
                                data/raise-threshold-by-count)))))

(defn current-user
  "Resolve the logged-in user."
  [{:keys [current-user] :as context} _ _]
  (when-let [uid (get-uid current-user)]
    (let [wants (wants context)
          ;; Fetching the logged in user record is common, so use that
          ;; result if we already have it.
          user (user-if-realized current-user)]
      (if (and user (not (:notification-counts? wants)))
        (fixup-user user context)
        (fetch-user context (merge {:uid uid} wants) :immediate)))))

(defn required-name-change
  "Return the requirement on a user account to change the name, if set."
  [{:keys [db] :as context} {:keys [name]} _]
  (data/with-superlifter context
    (some-> (query/select-required-name-change db {:name name})
            first
            prom/promise
            (data/update-trigger! context :RequiredNameChange/admin :user-bucket
                                  data/inc-threshold))))

;;; Resolver factories

(defn user-by-reference-factory
  "Generate a resolver to load a user reference from a uid."
  [key]
  (fn [context args resolved]
    (when-let [uid (key resolved)]
      (user-by-reference context args (assoc resolved :uid uid)))))

;;; Streamers

(defn stream-subscription-updates
  "Stream updates to subscriptions, as they are received."
  [{:keys [bus current-user]} _ source-stream-callback]
  (if-not (get-uid current-user)
    (let [action (go (source-stream-callback nil))]
      #(close! action))
    (let [uid (get-uid current-user)
          sub (bus/subscribe bus ::subscription-update uid
                             source-stream-callback)]
      #(bus/close-subscription bus sub))))

(defn stream-score-updates
  "Stream updates to the user's score, as they are received."
  [{:keys [notify current-user]} _ source-stream-callback]
  (if-not (get-uid current-user)
    (let [action (go (source-stream-callback nil))]
      #(close! action))
    (let [uid (get-uid current-user)
          handler #(source-stream-callback (get % "score"))
          subscription (notify/subscribe-user notify :user-score uid handler)]
      #(notify/close-subscription notify subscription))))

;;; Schema Mutations

(defn- new-status-from-change
  "Give the desired new status for a subscription based on the change."
  [change]
  (-> change
      {:SUBSCRIBE   :SUBSCRIBED
       :UNSUBSCRIBE :SUBSCRIBED
       :BLOCK       :BLOCKED
       :UNBLOCK     :BLOCKED}
      constants/subscription-status-map))

(defn- run-and-fixup-update-subscription-status
  "Insert, update or delete a subscription status, and return the new state."
  [db change params]
  (-> (if (#{:SUBSCRIBE :BLOCK} change)
        (query/update-subscription-status db params)
        (query/delete-subscription-status db params))
      first
      (update :status constants/reverse-subscription-status-map)))

(defn change-subscription-status
  "Subscribe, unsubscribe, block or unblock a sub."
  [{:keys [current-user db bus]} {:keys [sid change]} _]
  (let [status (new-status-from-change change)
        uid (get-uid current-user)
        params {:uid uid :sid sid :status status}
        subscr (run-and-fixup-update-subscription-status db change params)]
    (bus/publish bus ::subscription-update uid subscr)
    (log/info {:sid sid :status status} "Updated user subscription status")
    subscr))

(def update-preferences
  "Resolve the update user preferences mutation."
  {:prepare [(prepare/verify-language-valid [:preferences :language])
             (prepare/verify-admin-dm-block [:preferences :block-dms])]
   :resolve
   (fn [{:keys [db clock current-user]} {:keys [preferences]} _]
     (let [{:keys [nsfw nsfw-blur lab-rat enable-collapse-bar block-dms
                   language]} preferences
           datetime (jt/with-clock clock
                      (jt/format :iso-offset-date-time (jt/zoned-date-time)))
           prefs (->> [["block_dms" block-dms]
                       ["labrat" lab-rat]
                       ["no_collapse" (not enable-collapse-bar)]
                       ["nsfw" nsfw]
                       ["nsfw_blur" nsfw-blur]]
                      (map (fn [[k v]]
                             [k (if v "1" "0")])))
           result (query/update-preferences db {:uid (get-uid current-user)
                                                :prefs prefs
                                                :language language
                                                :datetime datetime})]
       (log/info {:preferences preferences
                  :result result} "Updated user preferences")
       (if (empty? result)
         :void
         (-> (first result)
             (assoc :uid (get-uid current-user))))))
   :notify
   [(fn [{:keys [db job-queue]} _ _ resolution]
      (when (and (not= :void resolution)
                 (seq resolution))
        (let [{:keys [uid datetime]} resolution]
          (jdbc/with-db-transaction [dbx (:ds db)]
            (job-queue/enqueue! job-queue dbx
                                ::message-tasks/check-block-dms-abuse
                                {:uid uid
                                 :datetime datetime})))))]})

(defn require-name-change
  "Impose a name change requirement.
  If one exists, return it unchanged, otherwise return the new one."
  [{:keys [db current-user clock] :as context} {:keys [uid message]} _]
  (data/with-superlifter context
    (let [name-change (->> {:uid uid
                            :admin-uid (get-uid current-user)
                            :message message
                            :datetime (util/sql-now clock)}
                           (query/insert-required-name-change db)
                           first)]
      (log/info {:uid uid :datetime (:datetime name-change)
                 :name-change name-change}
                "Set name change requirement.")
      (-> name-change
          prom/promise
          (data/update-trigger! context :RequiredNameChange/admin :user-bucket
                                data/inc-threshold)))))

(defn cancel-required-name-change
  "Cancel a name change requirement."
  [{:keys [db current-user clock]} {:keys [uid]} _]
  (query/delete-required-name-change db {:uid uid
                                         :admin-uid (get-uid current-user)
                                         :datetime (util/sql-now clock)})
  (log/info {:uid uid} "Cancelled name change requirement.")
  :void)

(defn register-user
  "Register user.
  This function is incomplete and should only be used for tests."
  [{:keys [db env]} {:keys [name _invite-code]} _]
  (cond
    (not= env :test)
    (resolve/resolve-as nil {:message "Use for testing only."
                             :status 404})
    :else
    (let [uid (util/uuid4)
          joindate (util/sql-timestamp)
          values {:uid uid
                  :joindate joindate
                  :name name
                  :status 0}]
      (query/insert-user db values)
      {:uid uid
       :joindate joindate
       :name name
       :score 0
       :given 0
       :status 0
       :language nil
       :posts nil
       :comments nil
       :attributes {}
       :subscriptions []
       :subs-moderated []})))
