;; gql/elements/user/spec.clj -- Specs for users
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.elements.user.spec
  "Specs for user SQL query arguments and fields."
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.spec :as element-spec]
   [ovarit.app.gql.elements.sub.spec :as sub-spec]))

(spec/def ::uid string?)
(spec/def ::given int?)
(spec/def ::upvotes-given int?)
(spec/def ::downvotes-given int?)
(spec/def ::joindate ::element-spec/timestamp)
(spec/def ::name string?)
(spec/def ::names (spec/coll-of ::name))
(spec/def ::status (-> constants/user-status-map vals set))
(spec/def ::uids (spec/coll-of ::uid))
(spec/def :ovarit.app.gql.elements.user.spec.badges/name string?)
(spec/def :ovarit.app.gql.elements.user.spec.badges/alt string?)
(spec/def :ovarit.app.gql.elements.user.spec.badges/icon string?)
(spec/def ::badges
  (spec/coll-of
   (spec/keys :req-un [:ovarit.app.gql.elements.user.spec.badges/name
                       :ovarit.app.gql.elements.user.spec.badges/alt
                       :ovarit.app.gql.elements.user.spec.badges/icon])))
(spec/def ::content-block
  (spec/nilable
   (-> constants/user-content-block-map vals set)))
(spec/def ::content-blocks
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [::uid
                        ::content-block]))))
(spec/def :ovarit.app.gql.elements.user.spec.meta/admin #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/labrat #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/invitecode string?)
(spec/def :ovarit.app.gql.elements.user.spec.meta/nochat #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/nostyles #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/nsfw #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/nsfw-blur #{"1" "0"})
(spec/def :ovarit.app.gql.elements.user.spec.meta/subtheme string?)
(spec/def ::meta
  (spec/nilable
   (spec/keys :opt-un [:ovarit.app.gql.elements.user.spec.meta/admin
                       :ovarit.app.gql.elements.user.spec.meta/labrat
                       :ovarit.app.gql.elements.user.spec.meta/invitecode
                       :ovarit.app.gql.elements.user.spec.meta/nochat
                       :ovarit.app.gql.elements.user.spec.meta/nostyles
                       :ovarit.app.gql.elements.user.spec.meta/nsfw
                       :ovarit.app.gql.elements.user.spec.meta/nsfw-blur
                       :ovarit.app.gql.elements.user.spec.meta/subtheme])))
(spec/def ::unread-message-count nat-int?)
(spec/def ::unread-notification-count nat-int?)
(spec/def ::language (spec/nilable string?))
(spec/def ::resets nat-int?)
(spec/def ::score int?)
(spec/def ::level (spec/and number? #(<= 0 %)))
(spec/def ::progress (spec/and number? #(<= 0 % 1)))
(spec/def :ovarit.app.gql.elements.user.spec.mods/power-level
  (-> constants/moderation-level-map vals set))
(spec/def :ovarit.app.gql.elements.user.spec.mods/sub-name string?)
(spec/def ::mods
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:ovarit.app.gql.elements.user.spec.mods/power-level
                        ::sub-spec/sid
                        :ovarit.app.gql.elements.user.spec.mods/sub-name]))))
(spec/def :ovarit.app.gql.elements.user.spec.subs/order (spec/nilable int?))
(spec/def :ovarit.app.gql.elements.user.spec.subs/status
  (-> constants/subscription-status-map vals set))
(spec/def :ovarit.app.gql.elements.user.spec.subs/name string?)
(spec/def ::subs
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:ovarit.app.gql.elements.user.spec.subs/order
                        :ovarit.app.gql.elements.user.spec.subs/status
                        :ovarit.app.gql.elements.user.spec.subs/name
                        ::sub-spec/sid]))))
