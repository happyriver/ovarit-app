;; gql/views/index.clj -- Index page for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.views.index
  (:require [cheshire.core :as cheshire]
            [hiccup.page :refer [html5]]
            [ovarit.app.gql.protocols.asset-path :refer [asset-path]]))

(defn- script-path [manifest env id]
  (str (when (not= env :dev) "/fe")
       "/js/compiled/"
       (get manifest id)))

(defn make-index-html
  "Make the HTML for the index page."
  [{:keys [assets manifest env session csrf-token site-name head-data
           body-data]}]
  (let [lang (:language session)
        language (if (or (nil? lang) (empty? lang)) "en" lang)
        {:keys [title meta links]} head-data
        scripts (->> (dissoc manifest :app :polyfill)
                     (map (fn [[module _]]
                            [module (script-path manifest env module)]))
                     (into {}))
        body-data (assoc body-data :scripts scripts)
        css-links (concat ["/css/purecss/base-min.css"
                           "/css/purecss/forms-min.css"
                           "/css/purecss/buttons-min.css"
                           "/css/purecss/grids-min.css"
                           "/css/purecss/grids-responsive-min.css"
                           "/css/purecss/menus-min.css"]
                          (if (= env :dev)
                            ["/css/tachyons.css"
                             "/css/main.css"
                             "/css/dark.css"
                             "/css/ovarit-app.css"
                             "/css/custom.css"
                             "/css/pikaday/pikaday.css"]
                            ["/css/ovarit-app.min.css"]))]
    (html5
     {:lang language}
     (into
      [:head
       [:title title]
       [:meta {:charset "utf-8"}]
       [:meta {:name "viewport"
               :content "width=device-width, initial-scale=1.0"}]
       [:link {:rel "icon" :type "image/png"
               :href (asset-path assets "/img/icon.png?v9")}]
       [:meta {:name "theme-color" :content "white"}]
       [:meta {:name "msapplication-navbutton-color" :content "white"}]
       [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
       [:meta {:name "apple-mobile-web-app-status-bar-style"
               :content "white-translucent"}]
       [:link {:rel "apple-touch-icon" :sizes "180x180"
               :href "/static/img/apple-touch-icon.png"}]
       [:link {:rel "icon" :type "image/png" :sizes "32x32"
               :href "/static/img/favicon-32x32.png"}]
       [:link {:rel "icon" :type "image/png" :sizes "16x16"
               :href "/static/img/favicon-16x16.png"}]
       [:link {:rel "manifest" :href "/static/img/site.webmanifest"}]]

      (concat (map (fn [m] [:meta m]) meta)
              (map (fn [l] [:link l]) links)
              (map (fn [css]
                     [:link {:rel "stylesheet"
                             :type "text/css"
                             :href (asset-path assets css)}])
                   css-links)))
     [:body
      [:noscript
       (str site-name
            " is a JavaScript app.  Please enable JavaScript to continue.")]
      (into
       [:div#app
        (when csrf-token
          [:label {:id "csrf" :data-value csrf-token}])]
       (map (fn [[k v]]
              [:label {:id (name k) :data-value (cheshire/generate-string v)}])
            body-data))
      [:script {:src (script-path manifest env :polyfill)}]
      [:script {:src (script-path manifest env :app)}]])))
