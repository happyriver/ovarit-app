;; gql/system/job_queue.clj -- Job queue implementation for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.job-queue
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.spec.alpha :as spec]
   [com.stuartsierra.component :as component]
   [hugsql.core :as hugsql]
   [ovarit.app.gql.protocols.job-queue :as protocol]
   [proletarian.job :as job]
   [proletarian.protocols]
   [proletarian.worker :as worker]))

(defn- log-level [x]
  (case x
    ::worker/queue-worker-shutdown-error         :error
    ::worker/handle-job-exception-with-interrupt :error
    ::worker/handle-job-exception                :error
    ::worker/job-worker-error                    :error
    ::worker/polling-for-jobs                    :trace
    :proletarian.retry/not-retrying              :error
    :info))

(defn- logger
  [x data]
  (let [e (or (:exception data) (:throwable data))
        explain (some-> (ex-data e)
                        spec/explain-out
                        with-out-str)]
    (log/log (log-level x) data e (name x))
    (when (and explain (not= "Success!\n" explain))
      (log/debug {:explain explain} "Spec error"))))

(defn- json-serializer
  "Create a JSON serializer for Proletarian.
  JSON has support in Postgres and Rust, and at this time
  Proletarian's default serializer format transit does not."
  []
  (reify proletarian.protocols/Serializer
    (encode [_ data] (cheshire/generate-string data))
    (decode [_ data-string] (cheshire/parse-string data-string true))))

(hugsql/def-db-fns "sql/job-queue.sql")
(declare job-counts)

(defrecord DbJobQueue [env clock redis-uri tr db cache notify pubsub]
  component/Lifecycle
  (start [this]
    (let [db-with-counter (assoc db :counter (atom 0))
          counts (try
                   (job-counts db-with-counter {})
                   (catch Exception e
                     {:error e}))
          serializer (json-serializer)
          job-context (assoc this
                             :db db-with-counter
                             :conn {:pool {} :spec {:uri redis-uri}})
          worker (worker/create-queue-worker
                  (get-in db [:ds-nolog :datasource])
                  #(protocol/handle-job! job-context %1 %2)
                  {:proletarian/log logger
                   :proletarian/serializer serializer})]
      (log/info {:counts counts} "Starting job queue")
      (worker/start! worker)
      (assoc this
             :worker worker
             :counts counts
             :serializer serializer)))

  (stop [this]
    (worker/stop! (:worker this))
    (log/info {:counts (job-counts (assoc db :counter (atom 0)) {})}
              "Stopping job queue")
    (dissoc this :worker))

  protocol/JobQueue
  (enqueue! [this db-tx job-type payload]
    (log/info {:job-type job-type :payload payload}
              "Enqueuing job")
    (job/enqueue! (:connection db-tx) job-type payload
                  {:proletarian/serializer (:serializer this)})))
