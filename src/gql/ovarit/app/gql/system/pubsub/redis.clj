;; gql/system/pubsub/redis.clj -- Publish and subscribe for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.pubsub.redis
  "Publish and subscribe to messages on Redis. Serialize using JSON."
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.edn :as edn]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.pubsub :as protocol]
   [taoensso.carmine :as car]))

(defrecord RedisPubSub [uri]
  component/Lifecycle
  (start [this]
    (assoc this
           :conn {:pool {} :spec {:uri uri}}
           :listeners (atom nil)))
  (stop [this]
    (assoc this :conn nil :listeners nil)))

(defn wrap-handler
  "Add serialization, error handling and logging to a subscription handler."
  [handler {:keys [format]}]
  (let [deserialize (if (= format :json)
                      #(cheshire/parse-string %)
                      edn/read-string)]
    (fn [[typ channel payload]]
      (log/debug {:type typ :channel channel :payload payload :format format}
                 "Received message")
      (when (= typ "message")
        (try
          (handler (deserialize payload))
          (catch Exception e
            (log/error {:channel channel :payload payload} e
                       "Error parsing or handling message")))))))

(defn wrap-pattern-handler
  "Add serialization, error handling and logging to a pattern handler."
  [handler {:keys [format]}]
  (let [deserialize (if (= format :json)
                      #(cheshire/parse-string %)
                      edn/read-string)]
    (fn [msg]
      (let [[typ pattern channel payload] msg]
        (log/debug {:type typ :pattern pattern
                    :channel channel :payload payload :format format}
                   "Received pattern message")
        (when (= typ "pmessage")
          (try
            (handler channel (deserialize payload))
            (catch Exception e
              (log/error {:channel channel :payload payload} e
                         "Error parsing or handling message"))))))))

(extend-protocol protocol/PubSub
  RedisPubSub

  (publish
    ([this address value]
     (protocol/publish this address value {:format :edn}))

    ([{:keys [conn]} address value {:keys [format]}]
     (let [serialize (if (= format :json)
                       cheshire/generate-string
                       pr-str)]
       (log/debug {:address address :payload value :format format}
                  "Publishing message")
       (->> value
            serialize
            (car/publish address)
            (car/wcar conn)))))

  (subscribe
    ([this address handler]
     (protocol/subscribe this address handler {:format :edn}))

    ([{:keys [conn]} address handler options]
     (log/debug {:address address} "Creating a subscription")
     (car/with-new-pubsub-listener (:spec conn)
       {address (wrap-handler handler options)}
       (car/subscribe address))))

  (psubscribe
    ([this pattern handler]
     (protocol/psubscribe this pattern handler {:format :edn}))

    ([this pattern handler options]
     (log/debug {:pattern pattern} "Creating a pattern subscription")
     (car/with-new-pubsub-listener (:spec (:conn this))
       {pattern (wrap-pattern-handler handler options)}
       (car/psubscribe pattern))))

  (close-subscription [_ subscription]
    (log/debug "Closing a subscription")
    (try
      (car/close-listener subscription)
      (catch Exception e
        (log/error {:subscription subscription} e
                   "Failure closing redis subscription")))))

(comment
  (def conn {:pool {}
             :spec {:uri "redis://redis:6379"}})
  (def pubsub (map->RedisPubSub {:uri "redis://redis:6379"}))
  (def started (component/start pubsub))

  (def sub1 (protocol/subscribe started "chan1"
                                (fn [msg] (println "Channel match: " msg))))
  (def sub2 (protocol/psubscribe started "chan*"
                                 (fn [pat msg]
                                   (println "Pattern match" pat msg))))

  (protocol/publish started "chan1" [1 2 3])
  (car/wcar conn (car/publish "chan1" "[msg 1]"))
  (car/wcar conn (car/publish "chan2" "[msg 2]"))

  (protocol/close-subscription started sub1)
  (protocol/close-subscription started sub2))
