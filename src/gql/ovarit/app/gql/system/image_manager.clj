;; gql/system/image_manager.clj -- Image manager implementation for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.image-manager
  (:require [cambium.core :as log]
            [cheshire.core :as cheshire]
            [clj-http.client :as client]
            [com.stuartsierra.component :as component]
            [ovarit.app.gql.protocols.image-manager :as protocol]
            [ovarit.app.gql.protocols.cache :as cache]))

(def upload-timeout-seconds
  "Time during which we expect an upload to complete."
  (* 60 60 24))

(defn- make-request
  [image-service-url request]
  (let [req (-> request
                (assoc :headers {"Content-Type" "application/json"})
                (update :url #(str image-service-url %)))]
    (try
      (let [response (client/request req)]
        (log/info {:request req}
                  "Image service request completed")
        (cheshire/parse-string (:body response) true))
      (catch Exception e
        (let [{:keys [status body]} (ex-data e)]
          (log/error {:request req
                      :message (ex-message e)
                      :status status :body body}
                     "Image service request failed"))))))

(defn- url-cache-key
  ;; TODO use a hash because amazon signed urls are long
  [url]
  (str (symbol ::signed-url) " " url))

(defn- create-upload-url
  [{:keys [cache image-service-url]}]
  (let [{:keys [url filename]} (make-request image-service-url
                                             {:url "/signed_url"
                                              :method :post
                                              :body "{}"})]
    (when filename
      (log/info {:filename filename} "Created an upload URL")
      (cache/cache-set cache (url-cache-key url) filename
                       upload-timeout-seconds)
      url)))

(defn- identify-upload
  "Determine the content type of an uploaded file.
  Rename it with the correct extension and return the new name and
  mime type, or nil if there was an error."
  [{:keys [cache image-service-url]} upload-url]
  (when-let [uploaded (cache/cache-get cache (url-cache-key upload-url))]
    (let [url (str "/identify/" uploaded)
          {:keys [filename content-type]} (make-request image-service-url
                                                        {:url url
                                                         :method :post})]
      (when filename
        (log/info {:content-type content-type
                   :filename uploaded
                   :identified filename} "Identified upload"))
      {:filename filename
       :content-type content-type})))

(defn- create-thumbnail
  "Create a thumbnail for an uploaded file.
  Return the name of the thumbnail, or nil if thumbnail creation
  failed."
  [{:keys [image-service-url]} uploaded]
  (let [url (str "/thumbnail/" uploaded)
        {:keys [filename]} (make-request image-service-url
                                         {:url url
                                          :method :post})]
    (when filename
      (log/info {:filename uploaded :thumbnail filename} "Generated thumbnail"))
    filename))

(defn- make-public
  "Copy a file from temporary to public-facing storage.
  Return a new filename."
  [{:keys [image-service-url]} prev-filename]
  (let [url (str "/make_public/" prev-filename)
        {:keys [filename]} (make-request image-service-url
                                         {:url url
                                          :method :post})]
    (when filename
      (log/info {:filename filename
                 :prev-filename prev-filename} "Made file public"))
    filename))

(defn- remove-metadata
  "Remove metadata from a file.
   Return a new filename, or nil if metadata removal failed."
  [{:keys [image-service-url]} original]
  (let [url (str "/remove_metadata/" original)
        {:keys [filename]} (make-request image-service-url
                                         {:url url
                                          :method :post})]
    (when filename
      (log/info {:filename original :thumbnail filename} "Removed metadata"))
    filename))

(defrecord ImageManager [cache image-service-url]
  component/Lifecycle
  (start [this]
    ;; TODO If the first create-upload-url fails, then the system
    ;; won't start.  This should catch errors, issue a warning, and
    ;; retry until successful.
    (let [url (create-upload-url this)
          parsed (java.net.URL. url)
          upload-service (str (.getProtocol parsed) "://"
                              (.getAuthority parsed))]
      (log/info {:image-service-url image-service-url
                 :upload-service upload-service} "Starting image manager")
      (assoc this :upload-service upload-service)))

  (stop [this]
    (log/info {:image-service-url image-service-url} "Stopping image manager")
    this)

  protocol/ImageManager
  (create-upload-url [this]
    (create-upload-url this))

  (identify-upload [this uploaded-url]
    (identify-upload this uploaded-url))

  (fetch-external-content
    [this external-url]
    "Fetch external content.
     Return a filename for the content fetched.")

  (create-thumbnail
    [this filename]
    (create-thumbnail this filename))

  (remove-metadata
    [this filename]
    (remove-metadata this filename))

  (make-public
    [this filename]
    (make-public this filename)))
