;; gql/system/bus.clj -- Message interchange for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.bus
  "Send messages within and between instances."
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [manifold.bus :as mf-bus]
   [manifold.executor :as mf-executor]
   [manifold.stream :as mf-stream]
   [ovarit.app.gql.protocols.bus :as protocol]
   [ovarit.app.gql.protocols.pubsub :as pubsub]))

(defn- pkey
  [prefix topic]
  (str prefix "|" (str (symbol topic))))

(defn- wildcard
  [prefix topic]
  (str (pkey prefix topic) "|*"))

(defn- pkey-id
  [prefix topic id]
  (str (pkey prefix topic) "|" (str id)))

(defn- republish
  [bus chan payload]
  (let [id (->> (str/split chan #"\|")
                (drop 2)
                (str/join "|"))]
    (log/debug {:chan chan :id id :payload payload}
               "Bus message received")
    (mf-bus/publish! bus id payload)))

(defn- wrap-handler
  [handler topic id]
  (fn [payload]
    (log/debug {:topic topic :id id :payload payload}
               "Message bus delivery")
    (handler payload)))

(defn- create-pool
  "Create a fixed-size instrumented thread pool."
  [prefix cpu-count]
  (let [thread-count (atom 0)
        thread-factory (mf-executor/thread-factory
                        #(str "bus-" prefix "-pool-"
                              (swap! thread-count inc))
                        (deliver (promise) nil))
        stats-callback (fn [stats]
                         (log/info {:prefix prefix :stats stats}
                                   "thread pool statistics"))
        options {:stats-callback stats-callback
                 :thread-factory thread-factory}]
    (mf-executor/fixed-thread-executor cpu-count options)))

(defrecord MessageBus [pubsub prefix]
  component/Lifecycle

  (start [this]
    (let [cpu-count (.availableProcessors (Runtime/getRuntime))
          pool (create-pool prefix cpu-count)]
      (log/info {:cpu-count cpu-count} "Starting message bus")
      (assoc this
             :topics (atom nil)
             :pool pool
             :stream #(mf-stream/stream 0 nil pool))))

  (stop [{:keys [pool topics] :as this}]
    (log/info {:topics (doall
                        (s/transform [s/MAP-VALS]
                                     (fn [{:keys [bus]}]
                                       (count (mf-bus/topic->subscribers bus)))
                                     @topics))}
              "Stopping message bus")
    (run! (fn [[_ {:keys [sub]}]]
            (pubsub/close-subscription pubsub sub))
          @topics)
    (.shutdownNow pool)
    (dissoc this :pool :topics :stream))

  protocol/Bus

  (subscribe [{:keys [prefix pubsub stream topics]} topic id handler]
    ;; Create a bus and Redis subscription for the topic if
    ;; one does not exist.
    (dosync
     (when-not (get @topics topic)
       (log/info {:topic topic} "Creating message bus topic")
       (swap! topics assoc topic
              (let [bus (mf-bus/event-bus stream)]
                {:bus bus
                 :sub (pubsub/psubscribe pubsub (wildcard prefix topic)
                                         (partial republish bus))}))))
    (let [{:keys [bus]} (get @topics topic)
          stream (mf-bus/subscribe bus id)]
      (log/debug {:topic topic :id id} "Subscribing")
      (mf-stream/consume (wrap-handler handler topic id) stream)
      stream))

  (publish [{:keys [pubsub prefix]} topic id payload]
    (log/debug {:topic topic :id id :payload payload} "Publishing")
    (pubsub/publish pubsub (pkey-id prefix topic id) payload))

  (close-subscription [_ subscription]
    (mf-stream/close! subscription)))
