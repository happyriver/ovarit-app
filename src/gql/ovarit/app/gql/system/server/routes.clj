;; gql/system/server/routes.clj -- Route handlers for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.routes
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.core.async :refer [>!! <!! chan close! offer! put! thread]]
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [com.walmartlabs.lacinia :as lacinia]
   [com.walmartlabs.lacinia.parser :as parser]
   [com.walmartlabs.lacinia.pedestal :refer [inject] :as lp]
   [com.walmartlabs.lacinia.pedestal.subscriptions :as lps]
   [com.walmartlabs.lacinia.pedestal2 :as lp2]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [com.walmartlabs.lacinia.validator :as validator]
   [hiccup.page]
   [io.pedestal.http :as http]
   [io.pedestal.http.ring-middlewares :as middlewares]
   [io.pedestal.http.route :as route]
   [io.pedestal.http.route.prefix-tree :as prefix-tree]
   [io.pedestal.http.route.router :as router]
   [io.pedestal.interceptor.chain :as chain]
   [io.pedestal.interceptor.helpers :as interceptor.helpers]
   [java-time.api :as jt]
   [ovarit.app.gql.elements.config.core :as config]
   [ovarit.app.gql.protocols.cache :as cache]
   [ovarit.app.gql.protocols.current-user :refer [get-uid moderates-by-name?
                                                  is-admin?]]
   [ovarit.app.gql.protocols.ratelimit :as ratelimit]
   [ovarit.app.gql.protocols.scheduler :as scheduler]
   [ovarit.app.gql.protocols.schema :as schema]
   [ovarit.app.gql.protocols.visitor-counter :as visitor-counter]
   [ovarit.app.gql.system.server.flask-compat :as compat]
   [ovarit.app.gql.system.server.routes.interceptors :as interceptors]
   [ovarit.app.gql.util :as util]
   [ovarit.app.gql.views.index :as index]
   [ring.middleware.cookies :as cookies]
   [ring.util.response :as ring-resp]
   [superlifter.core :as superlifter]))

(defn failure-response
  "Generate a failure response."
  [status body]
  {:status status
   :headers {}
   :body body})

(def content-length-interceptor
  "Add a Content-Length header to the app context."
  (interceptors/interceptor
   {:name ::content-length
    :leave (fn [context]
             (let [body (get-in context [:response :body])
                   len (count (when body (.getBytes body)))]
               (cond-> context
                 (pos? len) (assoc-in [:response :headers "Content-Length"]
                                      (str len)))))}))

(def put-sql-query-counter-in-app-context-interceptor
  "Put the SQL query counter in the app context."
  (interceptors/interceptor
   {:name ::sql-query-counter-in-app-context
    :enter (fn [context]
             (let [sql-query-counter (-> context :request
                                         ::interceptors/sql-query-counter)]
               (-> context
                   (assoc-in [:request :lacinia-app-context :db :counter]
                             sql-query-counter)
                   (assoc-in [:request :lacinia-app-context :visitor-counter
                              :db :counter]
                             sql-query-counter))))}))

(defn make-site-config-interceptor
  "Add a function to fetch the site configuration to the app context.
  For short-lived http requests, use a delay object so it's only fetched
  once.  For possibly long-lived websocket subscriptions, fetch
  it (from redis) each time it's requested."
  [protocol]
  (interceptors/interceptor
   {:name ::site-config
    :enter
    (fn [context]
      (let [app-context (get-in context [:request :lacinia-app-context])
            site-config (if (= :http protocol)
                          (let [conf (delay (config/site-config app-context))]
                            (fn [] @conf))
                          #(config/site-config app-context))]
        (-> context
            (assoc-in [:request :lacinia-app-context :site-config] site-config)
            (assoc-in [:request :site-config] site-config))))}))

(def visitor-counter-interceptor
  "Record a visit in the database."
  (interceptors/interceptor
   {:name ::visitor-counter
    :enter (fn [{:keys [request] :as context}]
             (let [visitor-counter (get-in request [:lacinia-app-context
                                                    :visitor-counter])
                   remote-addr (:real-remote-addr request)]
               (visitor-counter/add! visitor-counter remote-addr)
               context))}))

(def parse-cookies-interceptor
  "Parse cookies in the request map.
  Differs from middlewares/cookies in that it does not create a
  Set-Cookie header in the response."
  (interceptors/interceptor
   {:name ::parse-cookies
    :enter
    (fn [context]
      (update context :request cookies/cookies-request))}))

(defn make-decode-cookie-interceptor
  "On entry, decipher the session and remember-me cookies.
  On exit, encode and sign them."
  [{:keys [env secret-key session-lifetime]}]
  (interceptors/interceptor
   {:name ::cookie-values
    :enter
    (fn [context]
      (let [ctx (update-in context [:request :cookies]
                           compat/decode-cookies secret-key session-lifetime)
            uid (compat/session-uid (:request ctx))]
        (assoc-in ctx [:request :logging-context :request :uid] uid)))
    :leave
    (fn [{:keys [request] :as context}]
      (let [flask-cookies (compat/encode-cookies (:cookies request)
                                                 env secret-key)]
        (update-in context [:response :cookies] merge flask-cookies)))}))

(defn make-csrf-token-interceptor
  "If the session does not have a CSRF token, generate one.  Either way,
  sign the CSRF token and place it in the context for use by the index page."
  [{:keys [secret-key]}]
  (interceptors/interceptor
   {:name ::csrf-token
    :enter
    (fn [{:keys [request] :as context}]
      (let [csrf-token (or (get-in request compat/csrf-token-path)
                           (compat/new-csrf-token))
            signed-csrf-token (compat/sign-csrf-token csrf-token secret-key)]
        (-> context
            (update :request assoc-in compat/csrf-token-path csrf-token)
            (assoc-in [:request :signed-csrf-token] signed-csrf-token))))}))

(defn make-check-origin-interceptor
  "Resolve the query as an error unless the Origin header matches the
  server name.  This is necessary for subscriptions because browsers do
  not enforce cross-origin policies on websockets."
  [{:keys [env site-name]}]
  (interceptors/interceptor
   {:name ::check-origin
    :enter
    (fn [{:keys [request] :as context}]
      (let [origin (get-in request [:headers "origin"])
            site-url (if (= env :prod)
                       (str "https://" site-name)
                       (str "http://" site-name))]
        (if (or (= env :test ) (= origin site-url))
          context
          (do
            (log/info {:origin origin :site-url site-url}
                      "origin header mismatch")
            (throw (ex-info "Not authorized"
                            {::pass-msg true
                             :interceptor ::check-origin}))))))}))

(def subscriptions-only-interceptor
  "Resolve queries and mutations as errors, and permit subscriptions."
  (interceptors/interceptor
   {:name ::subscriptions-only
    :enter
    (fn [{:keys [request] :as context}]
      (if (str/starts-with? (str/trim (:query request)) "subscription")
        context
        (throw (ex-info "Bad Request" {::pass-msg true
                                       :interceptor ::subscriptions-only}))))}))

(def current-user-interceptor
  "Add delay objects which can get attributes of the current user."
  (interceptors/interceptor
   {:name ::current-user
    :enter
    (fn [context]
      (let [app-context (get-in context [:request :lacinia-app-context])
            user (compat/make-current-user-session app-context)]
        (-> context
            (assoc-in [:request :lacinia-app-context :current-user] user)
            (assoc-in [:request :current-user] user))))}))

(def current-user-subscription-interceptor
  "Add delay objects which can get attributes of the current user.
  For subscriptions this object is created when the websocket
  connection is initialized.  Put it in the lacinia app context."
  (interceptors/interceptor
   {:name ::current-user-subscription
    :enter
    (fn [{:keys [request] :as context}]
      (let [user (:current-user request)]
        (-> context
            (assoc-in [:request :lacinia-app-context :current-user] user)
            (assoc-in [:request :logging-context :request :uid]
                      (compat/session-uid request)))))}))

(def subscription-error-interceptor
  "Send an error message to a websocket client.  Use the exception
  message if the Exception was created by `ex-info` and has the
  `::pass-msg` key set, otherwise send an internal server error."
  (interceptors/interceptor
   {:name ::error-message
    :error
    (fn [context ^Throwable ex]
      (try
        (let [{:keys [id response-data-ch]} (:request context)
              ;; Pedestal may add a wrapper Exception
              cause (or (.getCause ex) ex)
              msg (if-not (::pass-msg (ex-data cause))
                    (do
                      (log/error ex "Exception in subscription interceptor")
                      (.getMessage cause))
                    (do
                      (log/info {:message (ex-message cause)
                                 :data (ex-data cause)}
                                "Subscription request failed")
                      (ex-message cause)))
              payload {:type :error
                       :id id
                       :payload {:message msg}}]
          (put! response-data-ch payload)
          (close! response-data-ch))
        (catch Exception e
          (log/error {:ex ex} e "Subscription error handling failure"))))}))

(defn make-log-graphql-query-interceptor
  "Create an interceptor to add graphql queries to the log."
  [env]
  (interceptors/interceptor
   {:name ::log-graphql-query
    :enter
    (fn [{:keys [request] :as context}]
      (let [query (or (:graphql-query request)  ; http queries
                      (:query request))         ; subscriptions
            [_ query-id] (str/split (or query "") #" ")]
        (cond
          (nil? query) context

          (and query-id (re-matches #"[a-f0-9]{10}" query-id))
          (assoc-in context [:request :logging-context :query-id] query-id)

          (not= env :prod)
          (assoc-in context [:request :logging-context :query] query)

          :else context)))}))

(defn make-query-parser-interceptor
  "Create an interceptor that parses the query in the request.  Replaces
  lp2/query-parser-interceptor."
  [{:keys [env schema-provider]}]
  (interceptors/interceptor
   {:name ::query-parser
    :enter
    (fn [context]
      (try
        (let [{:keys [graphql-query]} (:request context)
              {:keys [key parsed]} (schema/parse schema-provider graphql-query)]
          (-> context
              (assoc-in [:request :parsed-lacinia-query] parsed)
              (assoc-in [:request :logging-context :query-name] key)))
        (catch Exception e
          (when (not= env :prod)
            (log/error e "Error parsing query"))
          (assoc context :response (failure-response 400 "Bad Request")))))}))

(defn make-subscription-query-parser-interceptor
  "An interceptor that parses the query and places a prepared and validated
  query into the :parsed-lacinia-query key of the request."
  [{:keys [env schema-provider]}]
  (interceptors/interceptor
   {:name ::query-parser
    :enter
    (fn [context]
      (let [{:keys [query variables]} (:request context)
            actual-schema (if (= env :dev)
                            ((:schema schema-provider))
                            (:schema schema-provider))
            {:keys [key parsed]} (try
                                   (schema/parse schema-provider query)
                                   (catch Throwable t
                                     (throw (ex-info (.getMessage t)
                                                     {::errors (-> (ex-data t)
                                                                   :errors)}
                                                     t))))
            prepared (parser/prepare-with-query-variables parsed variables)
            errors (validator/validate actual-schema prepared {})]
        (when (seq errors)
          (log/error {:errors errors} "Subscription query validation error"))
        (if (seq errors)
          (throw (Exception. "Query validation error"))
          (-> context
              (assoc-in [:request :parsed-lacinia-query] prepared)
              (assoc-in [:request :logging-context :query-name] key)))))}))

(defn rate-limit
  "Create a 429 response if a rate-limit check fails."
  ([context ratelimiter rate-limit-type]
   (rate-limit context ratelimiter rate-limit-type 1))
  ([context ratelimiter rate-limit-type n]
   (if (ratelimit/check! ratelimiter rate-limit-type
                         (get-in context [:request :real-remote-addr]) n)
     context
     (assoc context :response (failure-response
                               429 "Too Many Requests")))))

(defn make-body-data-size-limit-interceptor
  "Limit the size of request bodies."
  [{:keys [request-body-size-limit]}]
  (interceptors/interceptor
   {:name ::body-data-size-limit
    :enter
    (fn [{:keys [request] :as context}]
      (if (> (count (:body request)) request-body-size-limit)
        (assoc context :response (failure-response
                                  400 "Bad Request"))
        context))}))

(defn make-ratelimit-query-count-interceptor
  "Rate limit the number of queries by IP."
  [ratelimiter rate-key]
  (interceptors/interceptor
   {:name ::ratelimit-query-count
    :enter
    (fn [context]
      (rate-limit context ratelimiter rate-key))}))

(defn inject-ratelimit-interceptors
  "Add a rate-limiting interceptor to the interceptor list."
  [interceptors ratelimiter rate-key]
  (-> interceptors
      (inject (make-ratelimit-query-count-interceptor ratelimiter rate-key)
              :after ::cookie-values)))

(defn make-crowdthinner-interceptor
  "Limit the number of requests waiting to be processed."
  [{:keys [in-queue]} request-type]
  (interceptors/interceptor
   {:name ::crowdthinner
    :enter (fn [context]
             (thread
               (if (offer! in-queue true)
                 context
                 (case request-type
                   :http (assoc context
                                :response (failure-response
                                           503 "Service Unavailable")
                                ::thinned true)
                   :ws (assoc context
                              ::chain/error
                              (ex-info "Service Unavailable"
                                       {::pass-msg true
                                        :interceptor ::crowdthinner})
                              ::thinned true)))))
    :error (fn [context exception]
             (when-not (::thinned context)
               (<!! in-queue))
             (assoc context ::chain/error exception))
    :leave (fn [context]
             (when-not (::thinned context)
               (<!! in-queue))
             context)}))

(defn gatekeeper-test-mode
  "A stub that can be used in the REPL to make requests return 503s."
  [_request-type]
  false
  #_(= request-type :ws))

(defn make-gatekeeper-interceptor
  "Limit the number of queries being processed at once."
  [{:keys [env request-timeout in-process]} request-type]
  (interceptors/interceptor
   {:name ::gatekeeper
    :enter (fn [context]
             (thread
               (>!! in-process true)
               ;; If the request has waited too long, punt now rather than
               ;; making the client wait any longer.
               (if (or (> (interceptors/age-ms context) request-timeout)
                       (and (= env :dev) (gatekeeper-test-mode request-type)))
                 (case request-type
                   :http (assoc context :response (failure-response
                                                   503 "Service Unavailable"))
                   :ws (assoc context
                              ::chain/error
                              (ex-info "Service Unavailable"
                                       {::pass-msg true
                                        :interceptor ::gatekeeper})))
                 context)))
    :error (fn [context exception]
             (thread
               (<!! in-process)
               (assoc context ::chain/error exception)))
    :leave (fn [context]
             (thread
               (<!! in-process)
               context))}))

(defn buckets
  "Create the bucket configuration for the superlifter data loader."
  [env]
  ;; We don't want to be relying on the interval trigger, because it
  ;; causes latency.  However if you don't have a backup interval
  ;; trigger and set the threshold incorrectly, it can hang a thread
  ;; waiting for fetches that never come. Compromise by setting it to
  ;; 50 in production to keep things moving, and 500 in development to
  ;; make interval latency easy to find in the logs.
  (let [interval (if (= :dev env) 500 50)
        data-bucket {:triggers {:elastic  {:threshold 0}
                                :interval {:interval interval}}}
        bucket-keys [:comment-bucket :message-bucket :post-bucket
                     :sub-bucket :user-bucket]
        data-buckets (reduce #(assoc %1 %2 data-bucket) {} bucket-keys)]
    (merge {:default        {:triggers {:interval {:interval interval}}}
            :immediate      {:triggers {:queue-size {:threshold 1}}}}
           data-buckets)))

(defn make-superlifter-interceptor
  "Start a superlifter for the request."
  [{:keys [env superlifter-pool]}]
  (let [path [:request :lacinia-app-context :request :superlifter]
        buckets (buckets env)]
    (interceptors/interceptor
     {:name ::inject-superlifter
      :enter
      (fn [{:keys [request] :as context}]
        (let [app-context (:lacinia-app-context request)
              logging-context (get-in app-context [:request :logging-context])
              superlifter-env (-> app-context
                                  (select-keys [:env :current-user :db
                                                :site-config :clock])
                                  (assoc :logging-context logging-context))
              superlifter-args {:buckets buckets
                                :urania-opts {:env superlifter-env
                                              :executor superlifter-pool}}]
          (assoc-in context path (superlifter/start! superlifter-args))))
      :error
      (fn [context exception]
        (-> context
            (update-in path superlifter/stop!)
            (assoc ::chain/error exception)))
      :leave
      (fn [context]
        (update-in context path superlifter/stop!))})))

(defn graphql-interceptors
  "Create an interceptor list to process graphql queries."
  [{:keys [env app-context] :as server-context}]
  (let [interceptors [;; lp2/initialize-tracing-interceptor
                      (interceptors/metrics-interceptor env :http)
                      content-length-interceptor
                      lp2/json-response-interceptor
                      (interceptors/remote-addr-interceptor server-context)
                      (interceptors/logging-context-interceptor server-context)
                      (make-crowdthinner-interceptor server-context :http)
                      (make-gatekeeper-interceptor server-context :http)
                      interceptors/error-response-interceptor
                      middlewares/cookies
                      (make-decode-cookie-interceptor server-context)
                      lp2/body-data-interceptor
                      (make-body-data-size-limit-interceptor server-context)
                      lp2/graphql-data-interceptor
                      lp2/status-conversion-interceptor
                      lp2/missing-query-interceptor
                      (make-log-graphql-query-interceptor env)
                      (make-query-parser-interceptor server-context)
                      lp2/disallow-subscriptions-interceptor
                      lp2/prepare-query-interceptor
                      (lp2/inject-app-context-interceptor app-context)
                      put-sql-query-counter-in-app-context-interceptor
                      (make-site-config-interceptor :http)
                      visitor-counter-interceptor
                      current-user-interceptor
                      (make-superlifter-interceptor server-context)
                      ;; lp2/enable-tracing-interceptor
                      lp2/async-query-executor-handler]]

    (if (ratelimit/enabled? (:ratelimiter app-context))
      (inject-ratelimit-interceptors interceptors
                                     (:ratelimiter app-context)
                                     :graphql-query)
      interceptors)))

(defn subscription-interceptors
  "Create the interceptors for a subscription request."
  [{:keys [app-context env] :as server-context}]

  [subscription-error-interceptor
   lps/send-operation-response-interceptor
   (interceptors/metrics-interceptor env :ws)
   (make-check-origin-interceptor server-context)
   subscriptions-only-interceptor
   (interceptors/logging-context-interceptor server-context)
   (make-crowdthinner-interceptor server-context :ws)
   (make-gatekeeper-interceptor server-context :ws)
   (make-log-graphql-query-interceptor app-context)
   (make-subscription-query-parser-interceptor server-context)
   (lps/inject-app-context-interceptor app-context)
   put-sql-query-counter-in-app-context-interceptor
   (make-site-config-interceptor :ws)
   current-user-subscription-interceptor
   lps/execute-operation-interceptor])

(defn make-html-response-interceptor
  "Convert the query response to an HTML response."
  [{:keys [env assets manifest headers]}]
  (interceptors/interceptor
   {:name ::html-response
    :leave
    (fn [{:keys [request response] :as context}]
      (let [{:keys [signed-csrf-token site-config]} request
            session (get-in request compat/session-path)
            {:keys [body body-data head-data status]} response
            html (if (and (map? body) (= status 200))
                   (index/make-index-html
                    {:env env
                     :assets assets
                     :manifest manifest
                     :session session
                     :head-data head-data
                     :site-name (:name (site-config))
                     :csrf-token signed-csrf-token
                     :body-data body-data})
                   "")]
        (if (map? body)
          (assoc context :response {:status status
                                    :headers (merge (:headers response)
                                                    headers)
                                    :body html})
          context)))}))

(defn make-index-query-interceptor
  "Insert the index query into the request body."
  [{:keys [query-hashes]}]
  (let [index-query (util/find-newest-query-version query-hashes :index-query)]
    (interceptors/interceptor
     {:name ::index-query
      :enter
      (fn [context]
        (update context :request
                assoc
                :graphql-query (str "query " (:hash index-query))
                :graphql-vars {}
                :graphql-operation-name nil))})))

(defn html-interceptors
  "Create the interceptors for serving a html page.
  The index page includes some query results, and the strategy for
  getting them is to swap them into the body of the request, run
  the normal /graphql interceptor chain, and at the very end rewrite
  lacinia's response map into index.html."
  [{:keys [env app-context] :as server-context}
   content-interceptors]
  (let [interceptors
        (-> (concat [(interceptors/metrics-interceptor env :http)
                     content-length-interceptor
                     (make-html-response-interceptor server-context)
                     (interceptors/remote-addr-interceptor server-context)
                     (interceptors/logging-context-interceptor server-context)
                     (make-crowdthinner-interceptor server-context :http)
                     (make-gatekeeper-interceptor server-context :http)
                     (make-index-query-interceptor server-context)
                     interceptors/error-response-interceptor
                     middlewares/cookies
                     (make-decode-cookie-interceptor server-context)
                     (make-csrf-token-interceptor server-context)
                     lp2/status-conversion-interceptor
                     (make-log-graphql-query-interceptor env)
                     (make-query-parser-interceptor server-context)
                     lp2/disallow-subscriptions-interceptor
                     lp2/prepare-query-interceptor
                     (lp2/inject-app-context-interceptor app-context)
                     put-sql-query-counter-in-app-context-interceptor
                     (make-site-config-interceptor :http)
                     visitor-counter-interceptor
                     current-user-interceptor
                     (make-superlifter-interceptor server-context)]
                    content-interceptors
                    [lp2/async-query-executor-handler])
            vec)]
    (if (ratelimit/enabled? (:ratelimiter app-context))
      (inject-ratelimit-interceptors interceptors
                                     (:ratelimiter app-context)
                                     :graphql-query)
      interceptors)))

(defn- default-og-tags
  [{:keys [lema name metadata_image metadata_image_height
           metadata_image_width]}]
  (->> [{:property "og:site_name" :content name}
        {:property "og:description" :content lema}
        {:property "og:image" :content metadata_image}
        {:property "og:image:height" :content metadata_image_height}
        {:property "og:image:width" :content metadata_image_width}]
       (filter #(seq (:content %)))))

(def default-head-data-interceptor
  "Create the default page title and list of meta tags."
  (interceptors/interceptor
   {:name ::default-head-data
    :leave
    (fn [{:keys [request] :as context}]
      (let [site-config ((:site-config request))
            {:keys [description lema]} site-config
            meta-tags (concat
                       [{:name "description" :content description}]
                       (default-og-tags site-config))]
        (-> context
            (assoc-in [:response :head-data :title] lema)
            (assoc-in [:response :head-data :meta] meta-tags))))}))

(def default-body-data-interceptor
  "Create the data to be inserted in the HTML body.
  Attach it to the result in the form of a map.  The keys
  of the map will become the ids of <label> tags in the HTML
  body, and the values of the map will be attached to the
  data-value attributes of the labels."
  (interceptors/interceptor
   {:name ::default-body-data
    :leave
    (fn [{:keys [response] :as context}]
      (if (map? (:body response))
        (assoc-in context [:response :body-data :data] (:body response))
        context))}))

(def fathom-interceptor
  "Add the fathom config to the body data."
  (interceptors/interceptor
   {:name ::fathom
    :leave
    (fn [{:keys [request] :as context}]
      (let [site-config ((:site-config request))]
        (assoc-in context [:response :body-data :fathom]
                  (::config/fathom site-config))))}))

(defn index-interceptors
  "Create interceptors to produce the default HTML page.
   Sets up head and body data for the HTML from the query results."
  [server-context]
  (html-interceptors server-context [default-head-data-interceptor
                                     fathom-interceptor
                                     default-body-data-interceptor]))

(defn funding-progress-interceptors
  "Create interceptors to produce the funding progress page.
  This is used as iframe content. It is the same as the index
  page, except that the fathom config is not included."
  [server-context]
  (-> (index-interceptors server-context)
      (inject nil :replace ::fathom)))

(defn make-single-post-query-interceptor
  "Perform the single post query."
  [{:keys [query-hashes schema-provider]}]
  (let [post-query (util/find-newest-query-version query-hashes
                                                   :get-single-post)
        {:keys [parsed]} (->> (str "query " (:hash post-query))
                              (schema/parse schema-provider))]
    (interceptors/interceptor
     {:name ::single-post-query
      :enter
      (fn [{:keys [request] :as context}]
        (let [ch (chan 1)
              app-context (-> (:lacinia-app-context request)
                              (update-in [:request :logging-context] assoc
                                         :query-name :get-single-post
                                         :query-id (:hash post-query)))
              current-user (:current-user app-context)
              {:keys [pid sub]} (:path-params request)
              vars {:pid pid
                    :is_mod (boolean (or (moderates-by-name? current-user sub)
                                         (is-admin? current-user)))
                    :is_authenticated (some? (get-uid current-user))}
              query-result (lacinia/execute-parsed-query-async
                            parsed vars app-context)
              apply-result #(assoc-in context [:request :get-single-post]
                                      {:query :get-single-post
                                       :variables vars
                                       :result %})]
          (resolve/on-deliver! query-result
                               (fn [result]
                                 (put! ch (apply-result result))))
          ch))})))

(defn- og-tags
  "Construct a list of OpenGraph properties for the single-post page."
  [{:keys [metadata-image metadata-image-width metadata-image-height name]}
   {:keys [status thumbnail title type]}
   description]
  (let [og-type (if (and (= status :ACTIVE) (= type :TEXT))
                  "article" "website")
        image-url (if (seq thumbnail)
                    thumbnail
                    metadata-image)
        height (if (seq thumbnail) "70" metadata-image-height)
        width (if (seq thumbnail) "70" metadata-image-width)]
    (->> [{:property "og:site_name"    :content name}
          {:property "og:description"  :content description}
          {:property "og:title"        :content (or title "Deleted post")}
          {:property "og:type"         :content og-type}
          {:property "og:image"        :content image-url}
          {:property "og:image:height" :content height}
          {:property "og:image:width"  :content width}]
         (filter #(seq (:content %))))))

(def single-post-head-data-interceptor
  "Modify the title and meta tags for a single post page response."
  (interceptors/interceptor
   {:name ::single-post-head-data
    :leave
    (fn [{:keys [request] :as context}]
      (let [result (:get-single-post request)]
        (if (or (nil? result)
                (instance? Throwable result)
                (some? (:errors result)))
          context
          (let [site-config ((:site-config request))
                post (get-in result [:result :data :post_by_pid])
                {:keys [author sub title]} post
                sub-title (if (seq (:title sub)) (:title sub) (:name sub))
                description (if (and title (:name author))
                              (str "Posted in " (:name sub)
                                   " by " (:name author))
                              sub-title)
                meta-tags (concat [{:name "description" :content description}]
                                  (og-tags site-config post description))]
            (if post
              (-> context
                  (assoc-in [:response :head-data :title]
                            (if title
                              (str title " | " (:name sub))
                              (:name sub)))
                  (assoc-in [:response :head-data :meta] meta-tags))
              context)))))}))

(def single-post-body-data-interceptor
  "Prepare the single post query result for the HTML body interceptor."
  (interceptors/interceptor
   {:name ::single-post-body-data
    :leave
    (fn [{:keys [request] :as context}]
      (let [query-result (:get-single-post request)
            {:keys [result]} query-result]
        (if (instance? Throwable result)
          (do
            (log/error {} result "Error in single post query")
            context)
          (update-in context [:response :body-data :queries]
                     conj query-result))))}))

(defn single-post-interceptors
  "Create the interceptors for serving the single-post page."
  [server-context]
  (html-interceptors server-context
                     [single-post-head-data-interceptor
                      default-head-data-interceptor
                      fathom-interceptor
                      single-post-body-data-interceptor
                      default-body-data-interceptor
                      (make-single-post-query-interceptor server-context)]))

(def redirect-home-when-logged-in-interceptor
  "Return a redirect response when the user is logged in.
  Otherwise serve the requested page."
  (interceptors/interceptor
   {:name ::redirect-when-logged-in
    :enter
    (fn [{:keys [request] :as context}]
      (if (-> request
              (get-in [:lacinia-app-context :current-user])
              get-uid)
        (assoc context :response (ring-resp/redirect "/"))
        context))}))

(defn redirect-home-when-logged-in-interceptors
  "Create interceptors to redirect logged in users to the home page.
  Otherwise serve the index page."
  [server-context]
  (html-interceptors server-context
                     [redirect-home-when-logged-in-interceptor
                      default-head-data-interceptor
                      default-body-data-interceptor]))

(def log-handler
  "Create the handler for the front-end logging route."
  (interceptors/interceptor
   {:name ::log-handler
    :enter (fn [{:keys [request] :as context}]
             (let [body (cheshire/parse-string (:body request) true)]
               (log/with-logging-context (:logging-context request)
                 (log/info {:body body} "front-end log")))
             (assoc context :response {:status 200
                                       :headers {}
                                       :body {:status "ok"}}))}))

(defn log-interceptors
  "Create an interceptor list for the logging route."
  [{:keys [env app-context] :as server-context}]
  (let [interceptors [(interceptors/metrics-interceptor env :http
                                                        :suppress-logging)
                      content-length-interceptor
                      lp2/json-response-interceptor
                      (interceptors/remote-addr-interceptor server-context)
                      (interceptors/logging-context-interceptor server-context)
                      interceptors/error-response-interceptor
                      middlewares/cookies
                      (make-decode-cookie-interceptor server-context)
                      lp2/body-data-interceptor
                      (make-body-data-size-limit-interceptor server-context)
                      log-handler]]
    (if (and (ratelimit/enabled? (:ratelimiter app-context))
             (not= :dev env))
      (inject-ratelimit-interceptors interceptors
                                     (:ratelimiter app-context)
                                     :front-end-logging)
      interceptors)))

(def relative-path-interceptor
  "An interceptor to adjust the path for the file middleware.
  Remove the routing prefix from the start of the path."
  (interceptor.helpers/on-request
   ::relative-path
   (fn [request]
     (let [path (get-in request [:path-params :path])]
       (-> request
           (assoc-in [:path-info] (str "/" path))
           (assoc-in [::static-file-route] true))))))

(def cache-control-header-interceptor
  "An interceptor to add a cache control header to static files."
  (interceptor.helpers/on-response
   ::cache-control
   (fn [response]
     (if (= 200 (:status response))
       (assoc-in response [:headers "Cache-Control"]
                 "public, max-age=604800, immutable")
       response))))

(defn file-interceptors
  "The interceptors for serving static files."
  [{:keys [assets env] :as server-context}]
  [(interceptors/metrics-interceptor env :http)
   (interceptors/remote-addr-interceptor server-context)
   (interceptors/logging-context-interceptor server-context)
   http/not-found
   relative-path-interceptor
   cache-control-header-interceptor
   (middlewares/file (:root-path assets))])

(defn schedule-stop
  "At a set hour of the day tomorrow, set the ::stop flag in Redis.
  If `stop-hour` is nil, do nothing."
  [{:keys [cache scheduler]} stop-hour]
  (cache/cache-del cache ::stop)
  (when stop-hour
    (let [now (jt/local-date-time)
          hour (parse-long stop-hour)
          target (-> now
                     (jt/plus (jt/days 1))
                     (jt/adjust (jt/local-time hour)))
          millis (jt/time-between now target :millis)
          duration (* 10 60)]
      (log/info {:stop-hour stop-hour :millis millis}
                "Scheduling server reboot")
      (scheduler/after scheduler millis
                       #(do
                          (log/info {:duration duration}
                                    "Set server stop flag")
                          (cache/cache-set cache ::stop true duration))))))

(defn health-check-interceptor
  "Generate a response for a health check request.
  If the ::stop flag is set in the cache, answer with a 503."
  [{:keys [cache]}]
  (interceptors/interceptor
   {:name ::health-check
    :enter
    (fn [context]
      (assoc context :response
             (if (cache/cache-get cache ::stop)
               {:status 503 :body "Service Unavailable"}
               {:status 200 :body "OK"})))}))

(defn pending-action-handler
  [{:keys [env site-name]}]
  (interceptors/interceptor
   {:name ::pending-action-handler
    :enter
    (fn [context]
      (let [app-context (get-in context [:request :lacinia-app-context])
            {:keys [current-user db]} app-context
            origin (if (= :dev env)
                     (str "http://" site-name)
                     (str "https://" site-name))
            row (fn [{:keys [name pending_action]}]
                  (let [link-type (first (keys pending_action))
                        code (-> (first (vals pending_action))
                                 :code)]
                    [:tr
                     [:td name]
                     [:td (str link-type)]
                     [:td (case link-type
                            :Verify
                            (str origin
                                 "/register/confirm?code=" code)
                            :Reset
                            (str origin
                                 "/reset/complete?code=" code "&name=" name)
                            :NewEmail
                            (str origin
                                 "/settings/account/confirm_email?code=" code))]]))
            table (fn [rows]
                    [:body 
                     [:table
                      (into [:tbody] rows)]])]
        (if (is-admin? current-user)
          (assoc context :response
                 {:status 200
                  :headers {"Content-Type" "text/html"}
                  :body (->> ["select name, pending_action
                                   from user_login ul
                                        left join public.user u
                                        on u.uid = ul.uid
                                 where pending_action is not null"]
                             (jdbc/query (:ds db))
                             (map row)
                             table
                             (hiccup.page/html5 {} [:head]))})
          (assoc context :response
                 {:status 403 :body "FORBIDDEN"}))))}))

(defn pending-action-interceptors
  [{:keys [env app-context] :as server-context}]
  [(interceptors/metrics-interceptor env :http)
   (interceptors/remote-addr-interceptor server-context)
   (interceptors/logging-context-interceptor server-context)
   (make-crowdthinner-interceptor server-context :http)
   (make-gatekeeper-interceptor server-context :http)
   interceptors/error-response-interceptor
   middlewares/cookies
   (make-decode-cookie-interceptor server-context)
   (lp2/inject-app-context-interceptor app-context)
   put-sql-query-counter-in-app-context-interceptor
   (make-site-config-interceptor :http)
   visitor-counter-interceptor
   current-user-interceptor
   (pending-action-handler server-context)])

(defrecord Router [prefix-tree-router not-found-route]
  router/Router
  (find-route [_ {:keys [request-method] :as request}]
    (or (router/find-route prefix-tree-router request)
        (and (= :get request-method) not-found-route))))

(defn- fallback-router
  "Create a prefix tree router that falls back to the route `not-found`.
  `not-found` should be a single route in Pedestal table format."
  [not-found routes]
  (let [not-found-route (-> #{not-found}
                            route/expand-routes
                            first)]
    (->Router (prefix-tree/router routes) not-found-route)))

(defn table
  "Construct a list of routes with non-default handlers."
  [{:keys [env sub-prefix] :as server-context}]
  (let [auth-redirect (redirect-home-when-logged-in-interceptors server-context)
        file (file-interceptors server-context)
        funding-progress (funding-progress-interceptors server-context)
        graphql (graphql-interceptors server-context)
        health [(health-check-interceptor server-context)]
        log (log-interceptors server-context)
        pending-actions (pending-action-interceptors server-context)
        single-post (single-post-interceptors server-context)]
    (-> #{["/fe/*path"
           :get file
           :route-name :file]
          ["/graphql"
           :post graphql
           :route-name :graphql]
          ["/health"
           :get health
           :route-name :health]
          ["/do/record_log"
           :post log
           :route-name :log]
          ["/register"
           :get auth-redirect
           :route-name :register]
          ["/login"
           :get auth-redirect
           :route-name :login]
          ["/donate/funding_progress"
           :get funding-progress
           :route-name :funding-progress]
          [(str "/" sub-prefix "/:sub/:pid")
           :get single-post
           :route-name :sub/view-post
           :constraints {:pid "[0-9]+"}]
          [(str "/" sub-prefix "/:sub/:pid/:slug")
           :get single-post
           :route-name :sub/view-post-slug
           :constraints {:pid #"[0-9]+"}]
          [(str "/" sub-prefix "/:sub/:pid/:slug/:cid")
           :get single-post
           :route-name :sub/view-post-slug-cid
           :constraints {:pid #"[0-9]+"}]
          [(str "/test/" sub-prefix "/:sub/:pid")
           :get single-post
           :route-name :sub/test-view-post
           :constraints {:pid "[0-9]+"}]
          [(str "/test/" sub-prefix "/:sub/:pid/:slug")
           :get single-post
           :route-name :sub/test-view-post-slug
           :constraints {:pid #"[0-9]+"}]
          [(str "/test/" sub-prefix "/:sub/:pid/:slug/:cid")
           :get single-post
           :route-name :sub/test-view-post-slug-cid
           :constraints {:pid #"[0-9]+"}]}
        (cond-> (= :dev env)
          (conj ["/admin/ovarit_auth"
                 :get pending-actions
                 :route-name :admin/pending-actions]))
        route/expand-routes)))

(defn router
  "Return a router creation function.
  The function, given a list of routes, will create a Pedestal
  prefix-tree router that serves the index page instead of a 404 when
  the route is not recognized."
  [server-context]
  (partial fallback-router
           ["/not-found" :get (index-interceptors server-context)
            :route-name :not-found]))
