;; gql/system/server/flask-compat.clj -- Flask compatibility utilities for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.flask-compat
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.java.io :as io]
   [clojure.string :as str]
   [ovarit.app.gql.elements.constants :as constants]
   [ovarit.app.gql.elements.user.resolve :as resolve-user]
   [ovarit.app.gql.protocols.current-user :as protocol]
   [pandect.algo.sha1 :refer [sha1-hmac-bytes sha1-bytes]]
   [pandect.algo.sha512 :refer [sha512-hmac]])
  (:import #_java.util.zip.Deflater
           #_java.util.zip.DeflaterOutputStream
           java.io.ByteArrayInputStream
           java.io.ByteArrayOutputStream
           java.util.Base64
           java.util.zip.Inflater
           java.util.zip.InflaterInputStream))

(defn now
  "Return the current time, UNIX-style."
  []
  (quot (System/currentTimeMillis) 1000))

(defn- remove-padding
  "Remove the trailing ='s created by Base64 encoding."
  [b64]
  (-> b64
      (str/split #"=")
      (nth 0)))

(defn- encode [to-encode]
  (remove-padding (.encodeToString (Base64/getUrlEncoder)
                                   (.getBytes to-encode))))

(defn- encode-bytes [to-encode]
  (remove-padding (.encodeToString (Base64/getUrlEncoder) to-encode)))

(defn- decode [to-decode]
  (String. (.decode (Base64/getUrlDecoder) to-decode)))

(defn- decode-bytes [to-decode]
  (.decode (Base64/getUrlDecoder) to-decode))

#_(defn deflate
    "Compress a byte array."
    ([^bytes input]
     (let [out-stream (ByteArrayOutputStream.)]
       (with-open [deflate-stream (DeflaterOutputStream. out-stream
                                    (Deflater. Deflater/DEFLATED false))]
         (.write deflate-stream input))
       (.toByteArray out-stream))))

(defn- inflate
  "Uncompress compressed data in a byte-array.  Return a byte array."
  ([^bytes input]
   (let [out-stream (ByteArrayOutputStream.)]
     (with-open [in-stream  (ByteArrayInputStream. input)
                 inf-stream (InflaterInputStream. in-stream (Inflater. false))]
       (io/copy inf-stream out-stream))
     (.toByteArray out-stream))))


;; Working with Flask cookies

(defn- session-signing-key
  "Construct the key used by Flask to sign session cookies."
  [secret-key]
  (sha1-hmac-bytes (.getBytes "cookie-session") (.getBytes secret-key)))

(defn- generate-signature
  [value signing-key]
  (let [byte-sig (sha1-hmac-bytes value signing-key)]
    (encode-bytes byte-sig)))

(defn- check-signature
  [token signing-key lifetime]
  (let [parts (str/split token #"\.")
        sig (first (take-last 1 parts))
        encoded-timestamp (first (take-last 2 parts))
        timestamp (try
                    (BigInteger. (decode-bytes encoded-timestamp))
                    (catch Exception _ nil))
        value (str/join "." (butlast parts))]
    (and timestamp
         (> lifetime (- (now) timestamp))
         (= sig (generate-signature value signing-key)))))

(defn- fixup-user-id
  "Throat's user id may be followed by the number of password resets.
  Separate that into a separate key.  Expects a map with a :user_id key,
  and returns a map with :user_id and :resets keys."
  [user-id-map]
  (if-let [user_id (:_user_id user-id-map)]
    (let [splits (str/split user_id #"\$")
          user_id (nth splits 0)
          resets (or (and (= 2 (count splits)) (nth splits 1))
                     "0")]
      (-> user-id-map
          (assoc :user_id user_id
                 :resets (Integer/parseInt resets))
          (dissoc :_user_id)))
    user-id-map))

(defn- decode-flask-token
  "Decode a Flask token and return the result, or return nil if the
  signature or time check fails."
  [token signing-key lifetime]
  (when (check-signature token signing-key lifetime)
    (try
      (let [parts (butlast (str/split token #"\."))
            compressed? (= (first parts) "")
            body (nth parts (if compressed? 1 0))
            decoded (if compressed?
                      (-> (decode-bytes body)
                          inflate
                          slurp)
                      (decode body))]
        (-> decoded
            (cheshire/parse-string true)))
      (catch java.util.zip.ZipException _
        (log/error "Error decompressing session cookie")))))

(defn decode-session
  "Decode a Flask session cookie and return the result as a map, or
  return nil if the signature or time check fails."
  [token secret-key session-lifetime]
  (when token
    (let [decoded (decode-flask-token token (session-signing-key secret-key)
                                      session-lifetime)]
      (fixup-user-id decoded))))

(defn encode-session
  "Encode a map as a Flask session cookie.  Return a signed string."
  [{:keys [user_id resets] :as session} secret-key]
  (let [resets-if-pos (if (and resets (pos? resets))
                        (str "$" resets)
                        "")
        user-id-with-resets (when user_id
                              (str user_id resets-if-pos))
        session' (if user-id-with-resets
                   (-> session
                       (dissoc :user_id)
                       (dissoc :resets)
                       (assoc :_user_id user-id-with-resets))
                   session)
        encoded-session (-> session'
                            cheshire/generate-string
                            encode)
        timestamp (-> (quot (System/currentTimeMillis) 1000)
                      biginteger
                      .toByteArray
                      encode-bytes)
        signing-target (str encoded-session "." timestamp)
        signature (generate-signature signing-target
                                      (session-signing-key secret-key))
        session-cookie (str signing-target "." signature)]
    session-cookie))

;; Why, you may ask, latin1 and utf-8 and sha512 and sha1 all in this
;; small section of code? Because that's what Flask and Flask-Login
;; use.
(defn- remember-signing-key [secret-key]
  (.getBytes secret-key "latin1"))

(defn decode-remember-token
  "Decode a Flask remember me cookie, and return its value as a map
  containing the uid and reset count, or nil if the signature check
  fails."
  [token secret-key]
  (when token
    (let [[value signature] (str/split token #"\|")]
      (when (= signature
               (sha512-hmac (.getBytes value "utf-8")
                            (remember-signing-key secret-key)))
        (fixup-user-id {:_user_id value})))))

(defn sign-remember-token
  "Sign a Flask remember me cookie."
  [{:keys [user_id resets]} secret-key]
  (let [value (str user_id "$" resets)
        signature (sha512-hmac (.getBytes value "utf-8")
                               (remember-signing-key secret-key))]
    (str value "|" signature)))

(def session-path
  "Path to find the decoded session cookie in a request or response map."
  [:cookies :decoded-cookies :session])

(def csrf-token-path
  "Path to find the CSRF token in a request map."
  (conj session-path :csrf_token))

(defn reconstitute-session
  "Regenerate a session from a remember token."
  [decoded-session remember-me]
  (if (nil? (:user_id remember-me))
    decoded-session
    (-> decoded-session
        (merge remember-me)
        (assoc :_fresh false))))

(defn session-uid
  "Return the uid from the session if there is one."
  [request]
  (-> (get-in request session-path)
      :user_id))

(defn decode-cookies
  "Decipher the session and remember-me cookies and add them to the passed map.
  `cookies` should be the map produced by the Ring middleware
  `cookies-request`."
  [cookies secret-key session-lifetime]
  (let [session-cookie (get-in cookies ["session" :value])
        remember-token (get-in cookies ["remember_token" :value])]
    (try
      (let [remember-me (when remember-token
                          (decode-remember-token remember-token secret-key))
            decoded-session (when session-cookie
                              (decode-session session-cookie
                                              secret-key session-lifetime))
            session (if (:user_id decoded-session)
                      decoded-session
                      (reconstitute-session decoded-session remember-me))]
        (update cookies :decoded-cookies
                assoc :session session
                :remember-token remember-me))
      (catch Exception e
        (log/error {:cookie cookies} e "Error deciphering cookie")
        cookies))))

(defn ->cookie
  "Turn a cookie value into a map with attributes."
  [value env]
  (let [cookie-value (if (string? value)
                       value
                       (:value value))]
    {:value cookie-value
     :path "/"
     :http-only true
     :secure (not= env :dev)}))

(defn encode-cookies
  "Return a map with updated cookies."
  [{:keys [decoded-cookies]} env secret-key]
  (when-let [session (:session decoded-cookies)]
    {"session" (-> session
                   (encode-session secret-key)
                   (->cookie env))}))

;; Working with CSRF tokens

(defn new-csrf-token
  "Generate a new csrf token for the session cookie."
  []
  (let [hex "01234567890abcdef"]
    (apply str (map (fn [_] (.charAt hex (rand-int 16))) (range 40)))))

(defn- csrf-signing-key [secret-key]
  (sha1-bytes (.getBytes
               (str "wtf-csrf-token" "signer" secret-key))))

(defn sign-csrf-token [csrf-token secret-key]
  (let [body (encode (str "\"" csrf-token "\""))
        timestamp (encode-bytes (-> (str (now))
                                    BigInteger.
                                    .toByteArray))
        token (str body "." timestamp)
        sig (generate-signature token (csrf-signing-key secret-key))]
    (str token "." sig)))

(defn decode-csrf-token
  "Decode a Flask CSRF token."
  [signed-csrf-token secret-key session-lifetime]
  (decode-flask-token signed-csrf-token
                      (csrf-signing-key secret-key)
                      session-lifetime))

;; Development stuff

(comment ;; decode something from a Flask cookie without the sig check
  (let [token "IjIxZGIzMzBkYWQ2MTAxYjk2YzMwYjYxZGQ2NDBlNzBkYTk5YmVkMTEi.YNICZg.2gCggz_9SaY4-mHI1gUN8Lctf3o"
        parts (butlast (str/split token #"\."))
        compressed? (= (first parts) "")
        body (nth parts (if compressed? 1 0))
        decoded (if compressed?
                  (-> (decode-bytes body)
                      inflate
                      slurp)
                  (decode body))]
    decoded))

;; Create a current user object from a session cookie.

(defn- load-user
  "Return the logged in user's user record if it is a valid login session."
  [{:keys [request] :as context}]
  (let [session (get-in request session-path)
        uid (:user_id session)]
    (when uid
      (resolve-user/resolved-user context {:uid uid}))))

(defn- get-user-can-admin
  "Determine whether the user is an admin."
  [user]
  (get-in @user [:meta :admin]))

(defn- get-user-is-admin
  "Determine whether the user is an admin with admin powers enabled."
  [context can-admin totp-expiration]
  (and @can-admin
       (or (not (:enable-totp? context))
           (and totp-expiration
                (< (.getTime (java.util.Date.)) totp-expiration)))))

(defn- get-subs-moderated
  "Return sub_mod records for the subs moderated by the current user."
  [user]
  (:mods @user))

(defrecord CurrentUserSession [uid resets user can-admin? is-admin?
                               subs-moderated])

(defn make-current-user-session
  [{:keys [request] :as context}]
  (let [session (get-in request session-path)
        user (delay (load-user context))
        can-admin (delay (get-user-can-admin user))
        totp-expiration (when (:apriv session)
                          (* (+ (int (:apriv session)) 7200) 1000))]
    (map->CurrentUserSession
     {:uid (:uid session)
      :resets (:resets session)
      :user user
      :can-admin? can-admin
      :is-admin? (delay (get-user-is-admin context can-admin totp-expiration))
      :subs-moderated (delay (get-subs-moderated user))
      :totp-expiration totp-expiration})))

(extend-protocol protocol/CurrentUser
  CurrentUserSession

  (get-uid [this]
    (let [resets (:resets this)
          user @(:user this)]
      (when (and (= resets (:resets user))
                 (= (:status user) :ACTIVE))
        (:uid user))))

  (get-name [this]
    (let [resets (:resets this)
          user @(:user this)]
      (and (= resets (:resets user))
           (= (:status user) :ACTIVE)
           (:name user))))

  (user-if-realized [this]
    (when (realized? (:user this))
      @(:user this)))

  (user [this]
    @(:user this))

  (subs-moderated [this]
    @(:subs-moderated this))

  (moderates? [this sid]
    (some #{sid} (map :sid @(:subs-moderated this))))

  (moderates-by-name? [this sub-name]
    (some? (some #{sub-name} (map :sub_name @(:subs-moderated this)))))

  (moderation-level [this sid]
    (->> @(:subs-moderated this)
         (some #(when (= sid (:sid %))
                  (:power-level %)))
         constants/reverse-moderation-level-map))

  (can-admin? [this]
    @(:can-admin? this))

  (is-admin? [this]
    @(:is-admin? this))

  (totp-expiration [this]
    (let [exp (:totp-expiration this)
          now (.getTime (java.util.Date.))]
      (when (and exp (< now exp))
        exp))))
