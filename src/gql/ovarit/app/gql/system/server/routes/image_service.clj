;; gql/system/server/routes/image_service.clj -- Image service route handlers for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.routes.image-service
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clj-uuid :as uuid]
   [clojure.java.io :as io]
   [clojure.java.shell :as shell]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.pedestal2 :as lp2]
   [io.pedestal.http :as http]
   [io.pedestal.http.ring-middlewares :as middlewares]
   [io.pedestal.http.route :as route]
   [io.pedestal.interceptor :as interceptor]
   [ovarit.app.gql.protocols.storage :as storage]
   [ovarit.app.gql.system.server.routes.interceptors :as interceptors]
   [ovarit.app.gql.system.storage.local]
   [ovarit.app.gql.util :as util]
   [pandect.algo.blake2b-512 :as blake2b-512]
   [route-swagger.doc :as sw.doc]
   [route-swagger.interceptor :as sw.int]
   [schema.core :as schema])
  (:import
   [java.io ByteArrayInputStream ByteArrayOutputStream]
   [javax.imageio ImageIO IIOImage]
   [net.coobird.thumbnailator Thumbnailator]
   [org.apache.tika Tika]
   [ovarit.app.gql.system.storage.local LocalStorage]))

(def ^Tika mime-type-detector (Tika.))

(defn- detect-mime-type
  [storage {:keys [filename] :as location}]
  (with-open [is (storage/input-stream storage location)]
    (try
      (let [result (.detect mime-type-detector is)]
        (slurp is)  ; S3 complains if you don't read the whole stream.
        result)
      (catch Exception e
        (log/error {:filename filename} e "Error parsing file for mime type")
        nil))))

(def extensions
  "Mime types that we recognize and their corresponding file extensions."
  {"image/jpeg"    ".jpg"
   "image/png"     ".png"
   "image/gif"     ".gif"
   "image/svg+xml" ".svg"
   "image/svg"     ".svg"
   "video/mp4"     ".mp4"
   "video/webm"    ".webm"})
(def mime-types
  "The set of mime types that we recongnize."
  (-> extensions keys set))

(def file-namespace
  "Namespace used in filename generation.
  This value was inherited from the Python app."
  #uuid "acd2da84-91a2-4169-9fdb-054583b364c4")
(def thumb-namespace
  "Namespace used in thumbnail filename generation.
  This value was inherited from the Python app."
  #uuid "f674f09a-4dcf-4e4e-a0b2-79153e27e387")

(defn- calculate-basename
  "Calculate a basename based on the content of the file.
  Look for the file in the temporary container.  This algorithm
  produces matching results to the Python server.  The Python server
  calculates the name of image files before removing the metadata."
  [namespace stream]
  (let [hash (blake2b-512/blake2b-512 stream)]
    (uuid/v5 namespace hash)))

(def swagger-json (interceptor/interceptor (sw.int/swagger-json)))
(def swagger-ui (interceptor/interceptor (sw.int/swagger-ui)))

(def opt schema/optional-key)
(def req schema/required-key)
(schema/defschema Filename
  {(req :filename) schema/Str})

(defn- health-check-interceptor
  "Generate a response for a health check request."
  ;; TODO check that things are working
  [_]
  (interceptors/interceptor
   {:name ::health-check
    :enter
    (fn [context]
      (assoc context :response {:status 200 :body "OK"}))}))

(def parse-json-body-interceptor
  (interceptors/interceptor
   {:name ::parse-json-body
    :enter
    (fn [context]
      (try
        (update-in context [:request :body] cheshire/parse-string true)
        (catch Exception e
          (assoc context :response
                 {:status 400
                  :headers {}
                  :body (str "Failed to parse request body: "
                             (.getMessage e))}))))}))

(defn- file-exists-interceptor
  "If a filename parameter is passed, raise a 404 if it does not exist."
  [{:keys [storage containers]}]
  (interceptors/interceptor
   {:name ::file-exists
    :enter
    (fn [{:keys [request] :as context}]
      (log/info {:filename (-> request :path-params :filename)}
                "checking if file exists")
      (if-let [filename (-> request :path-params :filename)]
        (if (storage/exists? storage {:temporary containers
                                      :filename filename})
          context
          (assoc context :response {:status 404
                                    :headers {}
                                    :body "File not found"}))
        context))}))

(defn- common-interceptors
  [{:keys [env] :as server-context}]
  [(interceptors/metrics-interceptor env)
   (interceptors/logging-context-interceptor)
   lp2/json-response-interceptor
   interceptors/error-response-interceptor
   lp2/body-data-interceptor
   parse-json-body-interceptor
   (sw.int/coerce-request)
   (sw.int/validate-response)
   (file-exists-interceptor server-context)])

(defn- fetch-interceptor
  [_]
  (sw.doc/annotate
   {:summary "Fetch a file from an external URL. Return a filename for it."
    :responses {200 {:body Filename}}}
   (interceptors/interceptor
    {:name ::fetch
     :enter
     (fn [context]
       (assoc context :response {:status 200 :body "OK"}))})))

(schema/defschema IdentifyResponseBody
  {(req :filename) schema/Str
   (opt :content-type) schema/Str})
(defn- identify-interceptor
  [{:keys [storage containers]}]
  (sw.doc/annotate
   {:summary "Identify a file's mime type by its content.
              If the mime type is acceptable, rename the file to a
              hash of its content with the correct extension. Include
              \"upload\" in the new file name, so we know it's the
              original uploaded file before metadata removal.
              Return the new filename, and the mime type."
    :parameters {:path-params Filename}
    :responses {200 {:body IdentifyResponseBody}
                400 {:body schema/Str}}}
   (interceptors/interceptor
    {:name :identify
     :enter
     (fn [{:keys [request] :as context}]
       (let [filename (-> request :path-params :filename)
             location {:container (:temporary containers)
                       :filename filename}
             mime-type (detect-mime-type storage location)
             new-name (if (mime-types mime-type)
                        (with-open [stream (storage/input-stream storage
                                                                 location)]
                          (str (calculate-basename file-namespace stream)
                               ".upload"
                               (get extensions mime-type)))
                        filename)]
         (when mime-type
           (log/info {:filename filename :new-name new-name
                      :content-type mime-type}
                     "Identified file type by content"))
         (when-not (= filename new-name)
           (storage/rename-file storage location
                                (assoc location :filename new-name)))
         (assoc context :response
                {:status 200
                 :body {:filename new-name
                        :content-type mime-type}})))})))

(defn- base-filename
  [filename]
  (let [parts (-> filename (str/split #"\."))
        basename (first parts)
        extension (last parts)]
    (str basename extension)))

(defn- make-public-interceptor
  [{:keys [storage containers]}]
  (sw.doc/annotate
   {:summary "Move a file to public-facing storage.
              Return a possibly different filename for it."
    :parameters {:path-params Filename}
    :responses {200 {:body Filename}}}
   (interceptors/interceptor
    {:name ::save
     :enter
     (fn [{:keys [request] :as context}]
       (let [filename (-> request :path-params :filename)
             new-filename (base-filename filename)
             {:keys [public temporary]} containers]
         (storage/rename-file storage
                              {:container temporary :filename filename}
                              {:container public    :filename new-filename})
         (log/info {:filename filename :public-filename new-filename}
                   "Moved content to public container")
         (assoc context :response {:status 200
                                   :body {:filename new-filename}})))})))

(schema/defschema SignedUrlResponseBody
  {(req :filename) schema/Str
   (req :url) schema/Str})
(defn- signed-url-interceptor
  [{:keys [storage service-url containers]}]
  (sw.doc/annotate
   {:summary "Create a signed url which may be uploaded to."
    :responses {200 {:body SignedUrlResponseBody}}}
   (interceptors/interceptor
    {:name :signed-url
     :enter
     (fn [context]
       (let [token (util/uuid4)
             filename (str token ".upload")
             location {:container (:temporary containers)
                       :filename filename}]
         (log/info {:token token} "Generating signed url")
         (assoc context :response
                {:status 200
                 :headers {"Content-Type" "text/plain"}
                 :body {:url (if (= LocalStorage (type storage))
                               (str service-url "/upload/" filename)
                               (storage/signed-url storage location))
                        :filename filename}})))})))

(defn- orientation-value
  [storage location]
  (with-open [in-stream (storage/input-stream storage location)]
    (let [{:keys [exit out err]}
          (shell/sh "exiftool" "-Orientation" "-n" "-"
                    :in in-stream :in-enc nil)]
      (if (or (not (zero? exit)) (seq err))
        (do
          (log/error {:exit exit :err err :filename (:filename location)}
                     "Failed to read orientation from metadata")
          nil)
        (let [val (-> (str/split out #"\s+") last)]
          (when (seq val)
            (log/info {:orientation val :filename (:filename location)}
                      "Found orientation in metadata")
            val))))))

(defn- write-orientation
  [storage location output-location orientation]
  (with-open [in-stream (storage/input-stream storage location)]
    (let [cmd ["exiftool" (str "-Orientation=" orientation) "-n" "-"
               :in in-stream :in-enc nil :out-enc :bytes]
          {:keys [exit out err]} (apply shell/sh cmd)]
      (when (or (not (zero? exit)) (seq err))
        (let [info {:exit exit :err err :filename (:filename location)
                    :orientation orientation}]
          (log/error info "Failed to write orientation metadata")
          (throw (ex-info "Failed to write orientation metadata" info))))
      (storage/write-file storage output-location (ByteArrayInputStream. out)
                          (count out))
      (log/info {:filename (:filename output-location) :orientation orientation}
                "Wrote orientation metadata"))))

(defn- alter-filename
  [{:keys [filename] :as location} to-insert]
  (let [parts (-> filename (str/split #"\."))
        basename (first parts)
        extension (last parts)
        new-filename (str basename "." to-insert "." extension)]
    (assoc location :filename new-filename)))

(defn- rewrite-image
  "Use javax.ImageIO to re-encode the image.
  This removes the image metadata, and may defuse any attacks aimed at
  browsers before delivering the image to our users. Rewriting does
  add additional compression losses to JPEGs, but our previous image
  upload code was doing that for years and no one ever noticed."
  [storage location output-location]
  (with-open [in-stream (storage/input-stream storage location)
              out-stream (ByteArrayOutputStream.)
              img-out-stream (ImageIO/createImageOutputStream out-stream)]
    (let [extension (-> location :filename (str/split #"\.") last)
          buffered-image (ImageIO/read in-stream)
          io-image (IIOImage. buffered-image nil nil)
          writer (.next (ImageIO/getImageWritersBySuffix extension))]
      (.setOutput writer img-out-stream)
      (.write writer io-image)
      (let [bytes (.toByteArray out-stream)]
        (storage/write-file storage output-location
                            (ByteArrayInputStream. bytes)
                            {:content-length (count bytes)})))))

(defn- remove-metadata
  "Remove image metadata, preserving image orientation if present."
  [storage location output-location]
  (let [orientation (orientation-value storage location)]
    (if (nil? orientation)
      (rewrite-image storage location output-location)
      (let [rewrite-location (alter-filename location "rewrite")]
        (rewrite-image storage location rewrite-location)
        (write-orientation storage rewrite-location output-location
                           orientation)))
    (log/info {:filename (:filename output-location)} "Removed metadata")))

(defn- remove-metadata-interceptor
  [{:keys [containers storage]}]
  (sw.doc/annotate
   {:summary "Remove metadata from an image, and return a new file name."
    :parameters {:path-params Filename}
    :responses {200 {:body Filename}}}
   (interceptors/interceptor
    {:name ::remove-metadata
     :enter
     (fn [{:keys [request] :as context}]
       (let [filename (-> request :path-params :filename)
             location {:container (:temporary containers)
                       :filename filename}
             no-meta (alter-filename location "nometa")]
         (remove-metadata storage location no-meta)
         (assoc context :response {:status 200
                                   :body {:filename no-meta}})))})))

(comment
  (with-open [in-stream (-> "images/thumb.jpg"
                            io/input-stream)]
    (with-open [out-stream (-> "images/thumb2.jpg"
                               io/output-stream)]

      ;; These both complain that method "of" cannot be found.
      (.. Thumbnails (of "samples/flower.jpg") (size 200 200)
          (format "jpeg") (toOutputStream out-stream))
      (-> Thumbnails
          (.of in-stream)
          (.size 200 200)
          (.format "jpeg")
          (.toOutputStream out-stream)))))

(defn- thumbnail-interceptor
  [{:keys [storage containers]}]
  (sw.doc/annotate
   {:summary "Create a thumbnail for an image."
    :parameters {:path-params Filename}
    :responses {200 {:body Filename}}}
   (interceptors/interceptor
    {:name ::thumbnail
     :enter
     (fn [{:keys [request] :as context}]
       (let [location {:container (:temporary containers)
                       :filename (-> request :path-params :filename)}]
         (with-open [in-stream (storage/input-stream storage location)
                     out-stream (ByteArrayOutputStream.)]
           (.. Thumbnailator
               (createThumbnail in-stream out-stream "jpeg" 70 70))
           (let [bytes (.toByteArray out-stream)
                 basename (calculate-basename thumb-namespace
                                              (ByteArrayInputStream. bytes))
                 thumb-location {:container (:temporary containers)
                                 :filename (str basename ".jpg")}]
             (storage/write-file storage thumb-location
                                 (ByteArrayInputStream. bytes)
                                 {:content-length (count bytes)})
             (log/info {:filename (:filename location)
                        :thumbname (:filename thumb-location)}
                       "Created thumbnail")
             (assoc context :response
                    {:status 200
                     :body {:filename (:filename thumb-location)}})))))})))

(schema/defschema UnfurlParams
  {(req :url) schema/Str})
(defn- unfurl-interceptor
  [_]
  (sw.doc/annotate
   {:summary "Fetch metadata from an external URL."
    :parameters {:body-params UnfurlParams}
    :responses {200 {:body schema/Str}}}
   (interceptors/interceptor
    {:name ::unfurl
     :enter (fn [context]
              (assoc context :response {:status 200 :body "OK"}))})))

(defn- upload-interceptor
  [{:keys [storage containers]}]
  (sw.doc/annotate
   {:summary "Upload a file and save it. For development and testing only."
    :parameters {:path-params Filename}
    :responses {200 {:body schema/Str}}}
   (interceptors/interceptor
    {:name ::upload
     :enter
     (fn [{:keys [request] :as context}]
       (let [location {:container (:temporary containers)
                       :filename (-> request :path-params :filename)}]
         (with-open [stream (io/input-stream (:body request))]
           (storage/write-file storage location stream)))
       (assoc context :response {:status 200 :body "OK"}))})))

(defn- api-routes
  [server-context]
  (let [common-incps (common-interceptors server-context)
        wrap (fn [make-interceptor]
               (conj common-incps
                     (make-interceptor server-context)))]
    (->> [["/fetch" :post fetch-interceptor]
          ["/identify/:filename" :post identify-interceptor]
          ["/make_public/:filename" :post make-public-interceptor]
          ["/signed_url" :post signed-url-interceptor]
          ["/remove_metadata/:filename" :post remove-metadata-interceptor]
          ["/thumbnail/:filename" :post thumbnail-interceptor]
          ["/unfurl" :get unfurl-interceptor]]
         (s/transform [s/ALL 2] wrap))))

(defn- dev-routes
  [{:keys [env storage _containers] :as server-context}]
  (->> [(when (and (#{:dev :test} env)
                   (= LocalStorage (type storage)))
          ["/upload/:filename"
           :put [(middlewares/multipart-params)
                 (upload-interceptor server-context)]])
        (when (= :dev env)
          ["/swagger.json" :get [http/json-body swagger-json]])
        (when (= :dev env)
          ;; In order for the Swagger UI to work (development only),
          ;; download the latest release from:
          ;; https://github.com/swagger-api/swagger-ui
          ;; and put the contents of the dist directory into
          ;; dev-resources/swagger-ui.  Then navigate to
          ;; http://localhost:8898/swagger-ui/index.html.
          ;; Enter http://localhost:8898/swagger.json into the Explore box.
          ["/swagger-ui/*resource" :get swagger-ui])]
       (remove nil?)))

(defn other-routes
  [server-context]
  [["/health"
    :get [(health-check-interceptor server-context)]]])

(defn table
  [{:keys [_env _storage _containers _service-url] :as server-context}]
  (-> (concat (api-routes server-context)
              (dev-routes server-context)
              (other-routes server-context))
      set
      route/expand-routes
      (sw.doc/with-swagger
        {:info {:title       "Ovarit Image Service"
                :description "Store and manipulate images for ovarit-app."
                :version     "0.1"}})))

(comment
  (table {:env :prod}))
