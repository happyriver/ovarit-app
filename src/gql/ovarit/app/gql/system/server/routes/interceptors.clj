;; gql/system/server/routes/interceptors.clj -- Common interceptors
;; Copyright (C) 2020-2024  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server.routes.interceptors
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [io.pedestal.interceptor :as interceptor]))

(defn interceptor
  "Build an interceptor, wrapping the functions in the supplied
  interceptor map to make them execute with a logging context."
  [interceptor-map]
  (interceptor/interceptor
   (s/transform [s/MAP-VALS]
                (fn [func]
                  (if-not (fn? func)
                    func
                    (fn [{:keys [request] :as context} & args]
                      (log/with-logging-context (:logging-context request)
                        (apply func (into [context] args))))))
                interceptor-map)))

(def tap-interceptor
  "Create an interceptor useful for debugging the other interceptors."
  (interceptor/interceptor
   {:name ::tap
    :enter (fn [context]
             (tap> (assoc context :tap-interceptor :enter))
             context)
    :leave (fn [context]
             (tap> (assoc context :tap-interceptor :leave))
             context)}))

(defn logging-interceptor
  "Create an interceptor to help you debug the other interceptors."
  [id extract]
  (interceptor
   {:name ::logging
    :enter (fn [context]
             (log/debug {id (extract context)} "ENTER logging-interceptor")
             context)
    :leave (fn [context]
             (log/debug {id (extract context)} "LEAVE logging-interceptor")
             context)}))

(defn age-ms
  "Get the age of a request from the timestamp in the context."
  [context]
  (when-let [ts (get-in context [:request ::timestamp])]
    (- (.getTime (java.util.Date.)) ts)))

(defn metrics-interceptor
  "Add timestamp and SQL query counter to the context.
  Report the results when leaving."
  ([env] (metrics-interceptor env :http))
  ([env protocol] (metrics-interceptor env protocol :logging))
  ([env protocol logging-flag]
   (interceptor
    {:name ::metrics
     :enter (fn [{:keys [request] :as context}]
              (let [servlet (:servlet-request request)]
                (when (and (= logging-flag :logging) (= protocol :ws))
                  (log/debug {:remote-addr (:remote-addr request)}
                             "Received subscription request"))
                (update-in context [:request] assoc
                           ::timestamp (if (and (= protocol :ws) servlet)
                                         (.getTimeStamp servlet)
                                         (.getTime (java.util.Date.)))
                           ::sql-query-counter (atom 0))))
     :leave (fn [{:keys [request response] :as context}]
              (let [counter (::sql-query-counter request)
                    ms (age-ms context)
                    in-ms-str (when (and ms (= :dev env))
                                (str " in " ms " ms"))]
                (when (= logging-flag :logging)
                  (log/with-logging-context (:logging-context request)
                    (if (= protocol :http)
                      (log/info {:status (:status response)
                                 :ms ms
                                 :sql-queries (if counter @counter 0)}
                                (str "Request completed" in-ms-str))
                      (log/info {:ms ms
                                 :sql-queries (if counter @counter 0)}
                                (str "Subscription request completed"
                                     in-ms-str))))))
              context)})))

(defn- parse-http-list
  "Parse a list from a HTTP header.
  This works the same way as python urllib's parse_http_list, which
  claims to be an implementation of RFC 2068 Section 2."
  [string]
  (map str/trim
       (loop [state {:parts []
                     :part []
                     :escape? false
                     :quote? false
                     :ch (first (seq string))
                     :remaining (rest (seq string))}]
         (let [{:keys [parts part escape? quote? ch remaining]} state]
           (if (nil? ch)
             (if (empty? part)
               parts
               (conj parts (apply str part)))
             (let [updated-state
                   (cond
                     escape?
                     (-> state
                         (update :part #(conj % ch))
                         (assoc :escape? false))

                     (and quote? (= ch \\))
                     (assoc state :escape? true)

                     (and quote? (= ch \"))
                     (-> state
                         (assoc :quote? false)
                         (update :part #(conj % ch)))

                     quote?
                     (update state :part #(conj % ch))

                     (= ch \,)
                     (-> state
                         (update :parts #(conj % (apply str part)))
                         (assoc :part []))

                     (= ch \")
                     (-> state
                         (assoc :quote? true)
                         (update :part #(conj % ch) ))

                     :else
                     (update state :part #(conj % ch)))
                   next-state
                   (-> updated-state
                       (assoc :ch (first remaining)
                              :remaining (rest remaining)))]
               (recur next-state)))))))

(defn- find-remote-addr
  "Return the IP address of the client.
  Use X-Forwarded-For and proxy configuration to avoid
  IP spoofing."
  [{:keys [headers remote-addr]} trusted-proxy-count]
  (if-let [addr (when-let [xff (get headers "x-forwarded-for")]
                  (let [proxies (parse-http-list xff)]
                    (when (and (pos? trusted-proxy-count)
                               (>= (count proxies) trusted-proxy-count))
                      (nth proxies (- (count proxies) trusted-proxy-count)))))]
    addr
    ;; No X-Forwarded-For, so this is client address.
    remote-addr))

(defn assoc-remote-addr
  "Determine the request remote address and add it to the context."
  [context trusted-proxy-count]
  (let [remote-addr (find-remote-addr (:request context)
                                      trusted-proxy-count)]
    (-> context
        (assoc-in [:request :real-remote-addr] remote-addr))))

(defn remote-addr-interceptor
  "Put the remote address of the request in the context."
  [{:keys [trusted-proxy-count]}]
  (interceptor
   {:name ::remote-addr
    :enter #(assoc-remote-addr % (or trusted-proxy-count 0))}))

(defn logging-context-interceptor
  "Construct a map to use for the logging context, and put it in the request."
  ([] (logging-context-interceptor {}))
  ([{:keys [headers-to-log]}]
   (interceptor
    {:name ::logging-context
     :enter (fn [context]
              (let [request (:request context)
                    remote-addr (:real-remote-addr request)
                    headers (->> (-> (:headers request)
                                     (select-keys headers-to-log))
                                 (s/transform [s/MAP-KEYS] keyword))
                    uri (:uri request)
                    method (:request-method request)
                    request-info (cond-> {}
                                   uri           (assoc :uri uri)
                                   method        (assoc :method method)
                                   remote-addr   (assoc :remote-addr remote-addr)
                                   (seq headers) (assoc :headers headers))]
                (assoc-in context [:request :logging-context :request]
                          request-info)))})))

(def error-response-interceptor
  "Create an internal server error response for unhandled exceptions."
  (interceptor
   {:name ::error-response
    :error (fn [context ex]
             (log/error ex "Unhandled exception in interceptor")
             (assoc context :response {:status 500
                                       :body "Internal Server Error"}))}))
