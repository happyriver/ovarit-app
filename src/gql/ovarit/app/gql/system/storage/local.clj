;; gql/system/storage/local.clj -- Local file storage for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.storage.local
  (:require
   [cambium.core :as log]
   [clojure.java.io :as io]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.storage :as protocol])
  (:import
   [java.io File]))

(defn- path
  [container fileid]
  (str container "/" fileid))

(defrecord LocalStorage []
  component/Lifecycle
  (start [this]
    (log/info {} "Starting local storage")
    this)

  (stop [this]
    (log/info {} "Stopping local storage")
    this)

  protocol/Storage
  (input-stream [_ {:keys [container filename]}]
    (log/debug {:container container :filename filename}
               "Create input stream")
    (io/input-stream (path container filename)))

  (write-file [this location stream]
    (protocol/write-file this location stream {}))

  (write-file [_ {:keys [container filename]} stream _metadata]
    (io/copy stream (io/file container filename))
    (log/info {:container container :filename filename} "Wrote file"))

  (rename-file [_ {container :container filename :filename}
                {new-container :container new-name :filename}]
    (.renameTo (File. (path container filename))
               (File. (path new-container new-name)))
    (log/info {:container container :new-container new-container
               :filename filename :new-name new-name} "Renamed file"))

  (exists? [_ {:keys [container filename]}]
    (let [result (.exists (File. (path container filename)))]
      (when-not result
        (log/error {:container container :filename filename}
                   "File not found"))
      result)))
