;; gql/system/storage/s3.clj -- S3 file storage for ovarit-app
;; Copyright (C) 2024 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.storage.s3
  (:require
   [amazonica.aws.s3 :as client]
   [cambium.core :as log]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.storage :as protocol])
  (:import
   [com.amazonaws.services.s3.model AmazonS3Exception]))

(defrecord S3Storage [credentials]
  component/Lifecycle
  (start [this]
    (log/info {:endpoint (:endpoint credentials)} "Starting S3 storage")
    this)

  (stop [this]
    (log/info {:endpoint (:endpoint credentials)} "Stopping S3 storage")
    this)

  protocol/Storage
  (write-file [this location stream]
    (protocol/write-file this location stream {}))

  (write-file [_ {:keys [container filename]} stream metadata]
    (client/put-object credentials
                       :bucket-name container
                       :key filename
                       :input-stream stream
                       :metadata metadata)
    (log/info {:container container :filename filename
               :metadata metadata} "Wrote file"))

  (input-stream [_ {:keys [container filename]}]
    (log/debug {:container container :filename filename}
               "Create input stream")
    (-> (client/get-object credentials container filename)
        :input-stream))

  (rename-file [_ {container :container filename :filename}
                {new-container :container new-name :filename}]
    (when (or (not= container new-container) (not= filename new-name))
      (client/copy-object credentials
                          container filename new-container new-name)
      (client/delete-object credentials
                            :bucket-name container :key filename)
      (log/info {:container container :new-container new-container
                 :filename filename :new-name new-name} "Renamed file")))

  (signed-url [_ {:keys [container filename]}]
    ;; TODO need a date
    (.toString (client/generate-presigned-url credentials
                                              {:bucket-name container
                                               :method "PUT"
                                               :key filename})))

  (exists? [_ {:keys [container filename]}]
    (try
      (-> (client/get-object-metadata
           credentials
           {:bucket-name container
            :key filename})
          :content-length)
      true
      (catch Exception e
        (log/error {:container container :filename filename} e
                   "File not found")
        false))))
