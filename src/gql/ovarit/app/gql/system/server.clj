;; gql/system/server.clj -- Http servers for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.server
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [chan]]
   [clojure.string :as str]
   [com.stuartsierra.component :as component]
   [com.walmartlabs.lacinia.pedestal2 :as lp2]
   [io.pedestal.http :as http]
   [manifold.executor :as mf-executor]
   [ovarit.app.gql.protocols.current-user :refer [get-uid]]
   [ovarit.app.gql.system.server.flask-compat :as compat]
   [ovarit.app.gql.system.server.routes :as routes]
   [ovarit.app.gql.system.server.routes.interceptors :as interceptors]
   [ovarit.app.gql.system.server.routes.image-service :as image-service]
   [ring.middleware.cookies :as cookies])
  (:import [org.eclipse.jetty.server.handler.gzip GzipHandler]
           [org.eclipse.jetty.websocket.servlet ServletUpgradeRequest]))

(def media-sites
  "List of media sites which can be embedded in iframes."
  ["https://gfycat.com"
   "https://streamable.com"
   "https://streamja.com"
   "https://www.youtube.com"
   "https://player.vimeo.com"])

(defn- csp
  "Create the content security policy."
  [{:keys [env site-name upload-service]}]
  (str/join "; "
            [(str "default-src 'self'"
                  (when (= env :dev) " 'unsafe-eval'"))
             (str "script-src-elem 'self' "
                  "https://cdn.usefathom.com")
             (str "script-src 'self' "
                  (when (= env :dev) "'unsafe-eval' ")
                  "https://cdn.usefathom.com")
             (str/join " " (into ["child-src" "'self'"] media-sites))
             "img-src 'self' data: https:"
             "media-src 'self' https:"
             (str "style-src 'self'"
                  (when (= env :dev) " 'unsafe-inline'"))
             (str/join " " (concat ["connect-src" "'self'"
                                    "https://cdn.usefathom.com"
                                    upload-service]
                                   (when site-name
                                     [(str "wss://" site-name)
                                      (str "ws://" site-name)])
                                   (when (= env :dev)
                                     ["ws://localhost:9630"])))]))

(defn- make-headers
  "Create response headers."
  {:arglists '([{:keys [env site-name upload-service]}])}
  [server-info]
  (let [content-security-policy (csp server-info)]
    {"Content-Type" "text/html"
     "Vary" "Cookie"
     "X-Frame-Options" "SAMEORIGIN"
     "X-XSS-Protection" "1; mode=block"
     "X-Content-Type-Options" "nosniff"
     "Referrer-Policy" "strict-origin-when-cross-origin"
     "Content-Security-Policy" content-security-policy
     "X-Content-Security-Policy" content-security-policy}))

(defn- get-current-user
  "Get the current user delay object and add it to the context."
  [request app-context]
  (let [user (-> app-context
                 (assoc :request request)
                 (assoc-in [:db :counter] (atom 0))
                 compat/make-current-user-session)]
    ;; Load the user object on the Pedestal thread instead of the
    ;; async pool used by Lacinia subscriptions.
    (get-uid user)
    (assoc request :current-user user)))

(defn enable-subscriptions
  "Modify the service map to allow websocket connections."
  [service-map
   {:keys [app-context secret-key session-lifetime
           trusted-proxy-count] :as server-context}]
  (lp2/enable-subscriptions
   service-map nil
   {:subscriptions-path "/graphql-ws"
    :subscription-interceptors (routes/subscription-interceptors server-context)
    :init-context
    (fn [ctx ^ServletUpgradeRequest req _resp]
      (let [ts (.getTime (java.util.Date.))
            servlet-req (.getHttpServletRequest req)
            result (-> ctx
                       (assoc-in [:request :headers "cookie"]
                                 (.getHeader servlet-req "Cookie"))
                       (update :request cookies/cookies-request)
                       (update-in [:request :cookies] compat/decode-cookies
                                  secret-key session-lifetime)
                       (update :request get-current-user app-context)
                       (assoc-in [:request :headers "x-forwarded-for"]
                                 (.getHeader servlet-req "X-Forwarded-For"))
                       (interceptors/assoc-remote-addr trusted-proxy-count)
                       (assoc-in [:request :headers "origin"]
                                 (.getHeader servlet-req "Origin"))
                       (assoc-in [:request :remote-addr]
                                 (.getRemoteAddr servlet-req)))]
        (log/debug {:ms (- (.getTime (java.util.Date.)) ts)
                    :remote-addr (get-in result [:request :real-remote-addr])
                    :uid (compat/session-uid (:request result))}
                   "Subscription context initialized")
        result))
    :values-chan-fn #(chan 10)}))

(defn gzip-configurator
  "Configure Jetty to compress responses."
  [context]
  (let [gzip-handler (GzipHandler.)]
    (.addIncludedMethods gzip-handler (into-array ["GET" "POST"]))
    (.addIncludedMimeTypes gzip-handler (into-array ["application/json"
                                                     "text/html"]))
    (.setExcludedAgentPatterns gzip-handler (make-array String 0))
    (.setMinGzipSize gzip-handler 1000)
    (.setGzipHandler context gzip-handler)
    context))

(defn enable-gzip [service-map]
  ;; lacinia-pedestal has added a function to enable websocket endpoints.
  ;; Call our configurator, then call theirs.
  (update-in service-map [::http/container-options :context-configurator]
             (fn [configurator-fn]
               (fn [context]
                 (-> context
                     gzip-configurator
                     configurator-fn)))))

(defn service-map
  "Build the service map for the GraphQL server."
  [{:keys [env] :as server-context} host port]
  (let [routes (if (= :dev env)
                 #(routes/table server-context)
                 (routes/table server-context))]
    (-> {::http/routes routes
         ::http/router (routes/router server-context)
         ::http/host host
         ::http/port port
         ::http/type :jetty
         ::http/join? false}
        (enable-subscriptions server-context)
        (enable-gzip))))

(defn- create-fixed-pool
  "Create a fixed-size instrumented thread pool."
  [env pool-size]
  (let [thread-count (atom 0)
        thread-factory (mf-executor/thread-factory
                        #(str "server-" (name env) "-pool-"
                              (swap! thread-count inc))
                        (deliver (promise) nil))
        stats-callback (fn [stats]
                         (log/info {:env env :stats stats}
                                   "thread pool statistics"))
        options {:stats-callback stats-callback
                 :thread-factory thread-factory}]
    (mf-executor/fixed-thread-executor pool-size options)))

;; Create a web server to serve the index page, the static assets
;; and graphql.
(defrecord Server [host port env assets request-timeout pool-size
                   process-queue-size waiting-queue-size headers-to-log
                   secret-key trusted-proxy-count session-lifetime
                   site-name admin-totp? sub-prefix query-hashes
                   stop-hour redis-uri clock manifest

                   bus cache db image-manager job-queue notify pubsub ratelimiter
                   scheduler schema-provider taskrunner visitor-counter

                   server pool stop]
  component/Lifecycle
  (start [this]
    (try
      (let [pool (create-fixed-pool env pool-size)
            server-context {:db db
                            :schema-provider schema-provider
                            :assets assets
                            :manifest manifest
                            :cache cache
                            :ratelimiter ratelimiter
                            :request-timeout request-timeout
                            :in-process (chan process-queue-size)
                            :in-queue (chan waiting-queue-size)
                            :headers-to-log headers-to-log
                            :env env
                            :secret-key secret-key
                            :trusted-proxy-count trusted-proxy-count
                            :session-lifetime session-lifetime
                            :headers (make-headers {:env env
                                                    :site-name site-name
                                                    :upload-service (-> image-manager :upload-service)})
                            :site-name site-name
                            :sub-prefix sub-prefix
                            :superlifter-pool pool
                            :query-hashes query-hashes
                            :app-context {:db db
                                          :cache cache
                                          :conn {:pool {} :spec {:uri redis-uri}}
                                          :bus bus
                                          :image-manager image-manager
                                          :job-queue job-queue
                                          :notify notify
                                          :pubsub pubsub
                                          :ratelimiter ratelimiter
                                          :schema schema-provider
                                          :taskrunner taskrunner
                                          :visitor-counter visitor-counter
                                          :env env
                                          :admin-totp? admin-totp?
                                          :tr format
                                          :clock clock}
                            ;; Allow 64K post limit plus a bit.
                            :request-body-size-limit 70000}]
        (log/info {:env env :host host :port port} "Starting server")
        (assoc this :server (-> server-context
                                (service-map host port)
                                http/create-server
                                http/start)
               :pool pool
               :stop (routes/schedule-stop {:cache cache
                                            :scheduler scheduler} stop-hour)))
      (catch Throwable e
        (log/error {:info (ex-data e)} e "Error starting server")
        this)))

  (stop [this]
    (when server
      (log/info {:env env :host host :port port} "Stopping server")
      (http/stop server))
    (when pool
      (.shutdownNow pool))
    (when stop
      (stop))
    (dissoc this :pool :server :stop)))

;; Create a webserver to answer /health.
(defrecord HealthCheckServer [host port env server cache]
  component/Lifecycle
  (start [this]
    (log/info {:env env :host host :port port} "Starting health check server")
    (let [health [(routes/health-check-interceptor {:cache cache})]]
      (assoc this :server
             (-> {:env env
                  ::http/routes [["/health" {:get health}]]
                  ::http/type :jetty
                  ::http/host host
                  ::http/port port
                  ::http/join? false}
                 http/create-server
                 http/start))))
  (stop [this]
    (when server
      (log/info {:env env :host host :port port} "Stopping health check server")
      (http/stop server))
    (assoc this :server nil)))

(defn- swagger-ui-csp-settings
  "Content security policy for the Swagger UI in dev mode."
  [env]
  (when (= :dev env)
    {:content-security-policy-settings
     {:object-src "'none'"
      :script-src "'self'"
      :frame-ancestors "'none'"}}))

(defn- allowed-origins
  "Allowed origins for CORS policy."
  [env site-name]
  [(str (if (= :prod env) "https://" "http://") site-name)])

;; Create the image service server.
(defrecord ImageService [host port env server service-url site-name containers

                         storage]
  component/Lifecycle
  (start [this]
    (log/info {:env env :host host :port port :site-name site-name}
              "Starting image processing service")
    (let [server-context {:env env
                          :storage storage
                          :containers containers
                          :service-url service-url}
          routes (if (= :dev env)
                   #(image-service/table server-context)
                   (image-service/table server-context))]
      (assoc this :server
             (-> {:env env
                  ::http/routes routes
                  ::http/secure-headers (swagger-ui-csp-settings env)
                  ::http/allowed-origins (allowed-origins env site-name)
                  ::http/type :jetty
                  ::http/host host
                  ::http/port port
                  ::http/join? false}
                 http/create-server
                 http/start))))
  (stop [this]
    (when server
      (log/info {:env env :host host :port port}
                "Stopping image processing service")
      (http/stop server))
    (assoc this :server nil)))
