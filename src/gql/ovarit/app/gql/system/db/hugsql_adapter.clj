;; gql/system/db/hugsql-adapter.clj -- HugSQL adapter for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.db.hugsql-adapter
  (:gen-class)
  (:require
   [cambium.core :as log]
   [clojure.java.jdbc :as jdbc]
   [hugsql.adapter :as adapter]))

(defn- inc-counter!
  "Add one to the query counter. Log a warning if the query counter is
  not present."
  [db]
  (if (:counter db)
    (swap! (:counter db) inc)
    (log/warn "SQL counter missing")))

(deftype HugsqlAdapterClojureJavaJdbcC3P0Counter []

  adapter/HugsqlAdapter
  (execute [_ db sqlvec options]
    (inc-counter! db)
    (if (some #(= % (:command options)) [:insert :i!])
      (jdbc/db-do-prepared-return-keys (:ds db) sqlvec)
      (apply jdbc/execute! (:ds db) sqlvec (:command-options options))))

  (query [_ db sqlvec options]
    (inc-counter! db)
    (apply jdbc/query (:ds db) sqlvec (:command-options options)))

  (result-one [_ result _]
    (first result))

  (result-many [_ result _]
    result)

  (result-affected [_ result _]
    (first result))

  (result-raw [_ result _]
    result)

  (on-exception [_ exception]
    (throw exception)))

(defn hugsql-adapter-clojure-java-jdbc-c3p0-counter []
  (->HugsqlAdapterClojureJavaJdbcC3P0Counter))
