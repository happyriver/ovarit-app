;; gql/system/ratelimit/token_bucket.clj -- Rate limiting for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.ratelimit.token-bucket
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [com.rpl.specter :refer [transform MAP-VALS]]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.ratelimit :as protocol]
   [taoensso.carmine :as car :refer (wcar)]))

;; Implement a token-bucket rate limiter:
;;  - Each client address gets some buckets of tokens, one for each type
;;    of action, and each bucket has a size depending on the type of action.
;;  - The tokens are replenished over time at a steady rate, again depending
;;    on the type of action.
;;  - Each action the client wants to do has a cost in tokens.
;;  - If the bucket for that type of action doesn't have enough tokens to
;;    pay the cost, the action is prevented.
;;  - Each bucket is implemented by an integer with a time-to-live, kept
;;    in Redis.

(defn truncate-ip
  "Shorten IPv6 addresses and leave IPv4's as is."
  [ip-addr]
  (try
    (let [addr (.getHostAddress (java.net.InetAddress/getByName ip-addr))
          splits (str/split addr #":")]
      (if-not (= (count splits) 8)
        addr
        ;; Take the /64.
        (str/join ":" (-> addr
                          (str/split #":")
                          (subvec 0 4)))))
    (catch Exception e
      (log/error {:ip-addr ip-addr
                  :exception-message (.getMessage e)}
                 "Error parsing IP")
      ip-addr)))

(def ms-per {:second 1000
             :minute (* 60 1000)
             :hour (* 60 60 1000)
             :day (* 24 60 60 1000)})

(defn make-ms-bucket
  "Translate bucket units into ms."
  [{:keys [size replenish cost]}]
  (let [interval (* ((:unit replenish) ms-per)
                    (:every replenish))]
    {:size (* interval size)
     :replenish-per-ms (:quantity replenish)
     :cost (* interval cost)
     :max-ttl (/ (* interval size) (:quantity replenish))}))

(def buckets-human-units
  {:graphql-query {:size 300
                   :replenish {:unit :minute
                               :every 1
                               :quantity 5}
                   :cost 1}
   :posting {:size 60
             :replenish {:unit :minute
                         :every 1
                         :quantity 1}
             :cost 1}
   :auth {:size 25
          :replenish {:unit :minute
                      :every 1
                      :quantity 1}
          :cost 1}
   :signup {:size 5
            :replenish {:unit :minute
                        :every 5
                        :quantity 1}
            :cost 1}
   :send-message {:size 10
                  :replenish {:unit :minute
                              :every 1
                              :quantity 2}
                  :cost 1}
   :front-end-logging {:size 100
                       :replenish {:unit :minute
                                   :every 1
                                   :quantity 10}
                       :cost 1}})

(def buckets
  (transform [MAP-VALS] make-ms-bucket buckets-human-units))

(defn make-key [bucket ip]
  (format "graphql-rl-%s-%s" (name bucket) (truncate-ip ip)))

(def lua-check
  "Lua script to check and update a rate limit atomically."
  "
local previous_value = redis.call(\"get\", KEYS[1])
local pttl = redis.call(\"pttl\", KEYS[1])
local size = ARGV[1]
local max_ttl = ARGV[3]
local cost = ARGV[4]
local value = size
if (previous_value and pttl > 0)
then
    local replenish_per_ms = ARGV[2]
    value = math.min(tonumber(size),
                     previous_value + (max_ttl - pttl) * replenish_per_ms)
end
value = value - cost
redis.call(\"psetex\", KEYS[1], max_ttl, value)
return value > 0
")

(defn check!
  "Check a rate limit and return whether it is OK to do something.
  `context` should supply the redis connection.  `btype` is a keyword
  describing the type of action (see the bucket map above) and `ip` the
  IP address or another string describing the entity to limit.  If
  `num` is present, apply that many actions towards the limit."
  ([context btype ip num]
   (let [conn (get-in context [:conn])
         key (make-key btype ip)
         {:keys [size replenish-per-ms max-ttl cost]} (btype buckets)
         result (wcar conn (car/eval* lua-check 1 key
                                      size replenish-per-ms max-ttl
                                      (* cost num)))]
     (if result
       true
       (do (log/info {:type (name btype) :ip ip} "Rate limit exceeded")
           false)))))

(defrecord RateLimiter [enabled? uri ratelimit]
  component/Lifecycle

  (start [this]
    (log/info {:enabled enabled?} "Starting token-bucket rate limiter")
    (assoc this :enabled? enabled? :conn {:pool {}
                                          :spec {:uri uri}}))

  (stop [this]
    (log/info "Stopping token-bucket rate limiter")
    (assoc this :enabled false :conn nil)))

(extend-protocol protocol/RateLimitCheck
  RateLimiter
  (enabled? [ratelimiter]
    (:enabled? ratelimiter))
  (check!
    ([ratelimiter btype ip] (check! ratelimiter btype ip 1))
    ([ratelimiter btype ip num] (check! ratelimiter btype ip num))))
