;; gql/system/notify/throat_py.clj -- Notifications implementation for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.notify.throat-py
  "Exchange messages with Python Throat.

  Publish messages to Redis with keys of the form /send:<event>:<room>
  and values in the structure expected by the Python code (Clojure
  maps will be converted to JSON and then to Python dictionaries).
  Throat will subscribe to those keys on Redis and send those messages
  out to subscribers via its socketio service and echo them back on
  Redis with keys of the form /snt:<event>:<room>, where they can be
  subscribed to here.

  Rooms used by Python Throat:

  user<uid>              User notification counts
  <pid>                  Post scores, comment counts, post title edits
  /all/new               New posts
  <sid>                  Chatroom
  chat                   Chatroom
  <model>-<id>           Thumbnailing results

  Messages sent without a room:

  announcement
  rmannouncement

  "
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [manifold.bus :as mf-bus]
   [manifold.stream :as mf-stream]
   [ovarit.app.gql.elements.message.query :as message]
   [ovarit.app.gql.elements.sub.fixup :as sub-fixup]
   [ovarit.app.gql.elements.sub.query :as sub]
   [ovarit.app.gql.elements.user.query :as user]
   [ovarit.app.gql.protocols.current-user :refer [get-uid]]
   [ovarit.app.gql.protocols.notify :as protocol]
   [ovarit.app.gql.protocols.pubsub :as pubsub]
   [ovarit.app.gql.util :as util]))

(defn- to-user-address
  "Construct the key for an event in the form expected by py-throat's
  socketio.py."
  [{:keys [outgoing]} event uid]
  (str outgoing ":" event ":user" uid))

;; Example of a 'notification' event payload:
;; {'count': {'notifications': 0, 'messages': 1,
;;            'modmail': {'0745580b-7bfc-41d0-934c-99f7320b2a05': 1},
;;            'unread': {'0745580b-7bfc-41d0-934c-99f7320b2a05':
;;                 {'NEW': 1,
;;                  'ALL': 1,
;;                  'IN_PROGRESS': 0,
;;                  'MOD_DISCUSSIONS': 0,
;;                  'MOD_NOTIFICATIONS': 0}}}}

(defn- make-notification-payload
  "Make a payload for a 'notification' message."
  [messages notifications modmails subs]
  {:count {:messages messages
           :notifications notifications
           :modmail modmails
           :subs (->> subs
                      (map sub-fixup/sum-report-counts))}})

(defn- get-and-send-modmail-counts
  "Given a user record and the sids of the subs they moderate,
  get the unread modmail counts and send the notification."
  [{:keys [prefixes pubsub]} db
   {:keys [uid unread-message-count unread-notification-count]} sids]
  (let [subs (when (seq sids)
               (sub/select-subs-by-sid db {:uid uid
                                           :sids sids
                                           :mod-stats? true}))
        modmails (into {} (map (fn [{:keys [:sid :unread-modmail-count]}]
                                 [sid unread-modmail-count])
                               subs))
        address (to-user-address prefixes "notification" uid)
        payload (make-notification-payload unread-message-count
                                           unread-notification-count
                                           modmails subs)]
    (log/debug {:address address
                :payload payload} "Sending notif to Python")
    (pubsub/publish pubsub address payload {:format :json})))

(defmulti publish-event
  "Send an event notification using Redis."
  (fn [_notify event-type _context _payload]
    event-type))

(defmethod publish-event :modmail-notification-counts
  ;; Send message counts to all mods in a sub.
  [{:keys [pubsub prefixes]} _event {:keys [db]} {:keys [sid]}]
  (let [mods (message/select-mods-with-notification-counts db {:sid sid})]
    (doseq [{:keys [uid messages notifications
                    unread_modmail_count] :as mod} mods]
      (let [sub-stats (-> mod
                          (select-keys [:open-report-count
                                        :closed-report-count
                                        :unread-modmail-count
                                        :new-unread-modmail-count
                                        :in-progress-unread-modmail-count
                                        :all-unread-modmail-count
                                        :discussion-unread-modmail-count
                                        :notification-unread-modmail-count])
                          (assoc :sid sid))]
        (pubsub/publish pubsub (to-user-address prefixes "notification" uid)
                        (make-notification-payload messages notifications
                                                   {sid unread_modmail_count}
                                                   [sub-stats])
                        {:format :json})))))

(defmethod publish-event :user-notification-counts
  ;; Send the current user updated notification counts.
  [notify _event {:keys [db current-user]} _]
  (let [uid (get-uid current-user)
        user (user/user-by-uid-with-notification-counts db {:uid uid})
        sids (map :sid (:mods user))]
    (get-and-send-modmail-counts notify db user sids)))

(defmethod publish-event :recipient-notification-counts
  ;; Send updated notification counts to the recipient of a message or
  ;; notification.  The uid to send to should be in the `receivedby`
  ;; key of the payload.
  [notify _event {:keys [db current-user]} {:keys [receivedby]}]
  (when (and receivedby (not= (get-uid current-user) receivedby))
    (let [user (user/user-by-uid-with-notification-counts db {:uid receivedby})
          sids (map :sid (:mods user))]
      (get-and-send-modmail-counts notify db user sids))))

(defmethod publish-event :user-score
  ;; Send an updated user score
  [{:keys [prefixes pubsub]} _event _context {:keys [uid score]}]
  (log/debug {:address (to-user-address prefixes "uscore" uid)
              :payload {:score score}} "Sending score to Python")
  (pubsub/publish pubsub (to-user-address prefixes "uscore" uid)
                  {:score score} {:format :json}))

(defn- republish-user
  "Take a user message from Redis and republish it on the bus."
  [bus chan payload]
  (let [uid (-> chan
                (str/split #":")
                (nth 2)
                (subs (count "user")))]
    (log/debug {:chan chan :uid uid :payload payload}
               "throat-py user message received")
    (mf-bus/publish! bus uid payload)))

(defn- republish-announcement
  "Take an announcement message from Redis and republish it on the bus."
  [bus payload]
  (log/debug {:payload payload}
             "throat-py announcement message received")
  (mf-bus/publish! bus :announcement {:pid (get payload "pid")}))

(defn- republish-rmannouncement
  "Take a remove announcement message from Redis and republish it on the bus."
  [bus payload]
  (log/debug {:payload payload}
             "throat-py rmannouncement message received")
  (mf-bus/publish! bus :rmannouncement {}))

(defn- wrap-user-handler
  [handler event uid]
  (fn [payload]
    (log/debug {:event event :uid uid :payload payload}
               "Bus user message delivery")
    (handler payload)))

(defn- wrap-site-handler
  [handler event]
  (fn [payload]
    (log/debug {:event event :payload payload}
               "Bus site message delivery")
    (handler payload)))

(defrecord ThroatNotify [env prefixes pubsub]
  component/Lifecycle
  (start [this]
    (log/info {} "Starting subscription to throat-py events")
    (let [prefix (:incoming prefixes)
          bus {:notification     (mf-bus/event-bus)
               :mod-notification (mf-bus/event-bus)
               :site             (mf-bus/event-bus)
               :user-score       (mf-bus/event-bus)}
          sub {:notification
               (pubsub/psubscribe pubsub (str prefix ":notification:user*")
                                  (partial republish-user (:notification bus))
                                  {:format :json})

               :mod-notification
               (pubsub/psubscribe pubsub (str prefix ":mod-notification:user*")
                                  (partial republish-user
                                           (:mod-notification bus))
                                  {:format :json})

               :announcement
               (pubsub/subscribe pubsub (str prefix ":announcement")
                                 (partial republish-announcement (:site bus))
                                 {:format :json})

               :rmannouncement
               (pubsub/subscribe pubsub (str prefix ":rmannouncement")
                                 (partial republish-rmannouncement (:site bus))
                                 {:format :json})

               :user-score
               (pubsub/psubscribe pubsub (str prefix ":uscore:*")
                                  (partial republish-user (:user-score bus))
                                  {:format :json})}]
      (assoc this
             :sub sub
             :bus bus)))

  (stop [{:keys [bus sub] :as this}]
    (log/info {:bus (doall (s/transform [s/MAP-VALS]
                                        #(count (mf-bus/topic->subscribers %))
                                        bus))}
              "Unsubscribing from throat-py events")
    (run! (fn [[_ subscription]]
            (pubsub/close-subscription pubsub subscription))
          sub)
    (dissoc this :bus :sub))

  protocol/Notify
  (publish [this event-or-events context resolution]
    (doseq [event (if (keyword? event-or-events)
                    [event-or-events] event-or-events)]
      (try
        (publish-event this event context resolution)
        (catch Exception e
          (log/error {:event event
                      :expound (util/expound-str (ex-data e) this)
                      :resolution resolution} e "Error publishing event")))))

  (subscribe-user
    [{:keys [bus]} event uid handler]
    (log/debug {:event event :uid uid} "Creating a user subscription")
    (let [stream (mf-bus/subscribe (get bus event) uid)]
      (mf-stream/consume (wrap-user-handler handler event uid) stream)
      stream))

  (subscribe-site
    [{:keys [bus]} event handler]
    (log/debug {:event event} "Creating a site subscription")
    (let [stream (mf-bus/subscribe (get bus :site) event)]
      (mf-stream/consume (wrap-site-handler handler event) stream)
      stream))

  (close-subscription [_ subscription]
    (log/debug {} "Closing a subscription")
    (mf-stream/close! subscription)))

(comment
  (def bus (mf-bus/event-bus))
  (mf-d/chain (mf-bus/publish! bus "chan1" "hello1")
              #(println "publish result" %))

  (def s (mf-bus/subscribe bus "chan1"))
  (mf-d/chain (mf-stream/consume
               #(println "consume" %) s))

  (def s2 (mf-bus/subscribe bus "chan1"))
  (mf-d/chain (mf-stream/consume
               #(println "consume2" %) s2))

  (mf-stream/take! s)

  (mf-stream/close! s)
  (mf-stream/close! s2)
  (mf-stream/description s)
  (mf-stream/downstream s)

  (mf-bus/topic->subscribers bus)

  ;; from example
  (d/let-flow [room (read-room conn)]
              (s/connect (p/subscribe bus room)
                         (s/buffer msg-len 1e3 conn)
                         {:timeout 1e4})
              (s/consume (partial b/publish! bus room)
                         (s/throttle 1 10 conn)))
  )
