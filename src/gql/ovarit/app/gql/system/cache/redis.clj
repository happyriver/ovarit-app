;; gql/system/cache/redis.clj -- Caching for ovarit-app with Redis
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.cache.redis
  (:require
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.cache :as protocol]
   [taoensso.carmine :as car]))

(defrecord RedisCache [uri]
  component/Lifecycle
  (start [this]
    ;; This will be the place to subscribe to things.
    (assoc this :conn {:pool {}
                       :spec {:uri uri}}))
  (stop [this]
    ;; This will be the place to unsubscribe from things.
    (assoc this :conn nil)))

(defn- full-name [sym-or-str]
  (if (keyword? sym-or-str)
    (str (symbol sym-or-str))
    sym-or-str))

(defn generic-lookup
  [this cache-key seconds action]
  (let [conn (:conn this)
        keyname (full-name cache-key)
        previous-result (car/wcar conn (car/get keyname))]
    (if previous-result
      previous-result
      (let [new-result (action)]
        (car/wcar conn (car/setex keyname seconds new-result))
        new-result))))

(defn generic-get
  "Get the value for a key, or nil."
  [{:keys [conn]} cache-key]
  (let [keyname (full-name cache-key)]
    (car/wcar conn (car/get keyname))))

(defn generic-set
  "Set the value for a key."
  ([vc cache-key val]
   (generic-set vc cache-key val 300))
  ([{:keys [conn]} cache-key val timeout]
   (let [keyname (full-name cache-key)]
     (car/wcar conn (car/setex keyname timeout val)))))

(defn generic-del
  "Remove a key."
  [{:keys [conn]} cache-key]
  (let [keyname (full-name cache-key)]
    (car/wcar conn (car/del keyname))))

(extend-protocol protocol/Cache
  RedisCache
  (lookup [this cache-key seconds action]
    (generic-lookup this cache-key seconds action))
  (cache-get [this cache-key]
    (generic-get this cache-key))
  (cache-set
    ([this cache-key value]
     (generic-set this cache-key value))
    ([this cache-key value timeout]
     (generic-set this cache-key value timeout)))
  (cache-del [this cache-key]
    (generic-del this cache-key)))

(comment
  (def cache {:conn {:pool {}
                     :spec {:uri "redis://127.0.0.1:6379"}}}))
