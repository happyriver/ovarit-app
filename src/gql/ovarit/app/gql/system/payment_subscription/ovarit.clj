;; gql/system/payment_subscription/ovarit.clj -- Payment subscription implementation for ovarit-app
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.payment-subscription.ovarit
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clj-http.client :as client]
   [ovarit.app.gql.protocols.payment-subscription :as protocol]
   [ovarit.app.gql.system.server.flask-compat :as flask-compat]))

(defrecord OvaritSubscription [host secret-key])

(defn- make-session
  "Make a session cookie for authentication to the ovarit-subscription
  server."
  [uid resets secret-key]
  (flask-compat/encode-session {:user_id uid
                                :resets resets
                                :_fresh true
                                :remember_me false
                                :csrf_token "todo"}
                               secret-key))

(comment
  (let [uid "9b187214-82e2-4059-834e-6f4d75294421"
        resets 0
        secret-key "PUT SOMETHING HERE"
        session (make-session uid resets secret-key)]
    #_(client/request {:method :post
                       :url "http://ovarit-subs:5003/subscription/user"
                       :headers {"Cookie" (str "session=" session)}})
    (client/request {:method :delete
                     :url "http://ovarit-subs:5003/subscription"
                     :headers {"Cookie" (str "session=" session)}})))

(defn- make-request
  "Add a session cookie to the passed request and send it to the
  ovarit-subscription server.  Based on the response, return
  a map with the keys :status (value :success, :failure or :retry)
  and :response, with the parsed body of the response."
  [{:keys [host secret-key]} {:keys [uid resets] :as user} request]
  (try
    (let [session (make-session uid resets secret-key)
          req (-> request
                  (assoc-in [:headers "Cookie"] (str "session=" session))
                  (assoc :throw-exceptions false)
                  (update :url #(str host %)))
          response (client/request req)
          {:keys [status body]} response
          result {:response (try
                              (cheshire/parse-string body true)
                              (catch Exception _ body))}]
      (cond
        (= 200 status)
        (assoc result :status :success)

        (= 401 status)
        (do
          (log/warn {:user user :request request}
                    "Update subscription authentication failed")
          (assoc result :status :failed))

        (= 428 status)
        ;; Either they're not a customer or they don't have a subscription.
        ;; Either way, there is no work to do.
        (assoc result :status :success)

        :else
        (do
          (log/error {:user user
                      :request request
                      :status status
                      :body body}
                     "Update subscription unrecognized response")
          (assoc result :status :retry))))

    (catch java.net.UnknownHostException e
      (log/error {:host host} e
                 "ovarit-subscription host not found")
      {:status :retry})
    (catch java.net.MalformedURLException e
      (log/error {:host host} e
                 "OVARIT_SUBSCRIPTION_HOST not configured correctly")
      {:status :retry})
    (catch java.net.ConnectException e
      (log/error {:host host} e
                 "ovarit-subscription host refused connection")
      {:status :retry})
    (catch Exception e
      (log/error {:user user :request request} e
                 "Update subscription request error")
      {:status :retry})))

(extend-protocol protocol/PaymentSubscription
  OvaritSubscription

  (update-email
    [this {:keys [uid] :as user} _new-email]
    (log/info {:uid uid} "Update email")
    ;; The ovarit-subscription server gets the new email from the
    ;; database, so we don't send it.
    (-> (make-request this user {:method :post
                                 :url "/subscription/user"
                                 :headers {"Content-Type" "application/json"}
                                 :body (cheshire/generate-string {})})
        :status))

  (cancel
    [this {:keys [uid] :as user}]
    (log/info {:uid uid} "Cancel subscription")
    (-> (make-request this user {:method :delete
                                 :url "/subscription"})
        :status))

  (stats
    [this user]
    (let [result (make-request this user
                               {:method :get
                                :url "/progress"
                                :headers {"Content-Type" "application/json"}})]
      (when (= :success (:status result))
        (get-in result [:response :paid])))))

(comment
  (protocol/update-email
   (map->OvaritSubscription  {:host "http://ovarit-subs:5003"
                              :secret-key "PUT SOMETHING HERE"})
   {:uid "34e065b8-b356-4c57-b200-78b06dafd1fe"
    :resets nil}
   "new-email@example.com")
  (protocol/stats
   (map->OvaritSubscription  {:host "http://ovarit-subs:5003"
                              :secret-key "PUT SOMETHING HERE"})
   {:uid "34e065b8-b356-4c57-b200-78b06dafd1fe"
    :resets nil}))
