;; gql/system/tasks.clj -- Task descriptions for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.tasks
  "Start scheduled tasks."
  (:require
   [cambium.core :as log]
   [clojure.string :as str]
   [com.stuartsierra.component :as component]
   [ovarit.app.gql.protocols.scheduler :as scheduler]
   [ovarit.app.gql.protocols.tasks :as tasks]
   [ovarit.app.gql.util :as util]
   [taoensso.carmine :as car]))

(def keepalive 5000)
(def keepalive-set (str (symbol ::instance)))

(defn- run-if-main-instance
  "Return a function that runs func only if this is the designated
  taskrunner instance (first in alphabetical order by instance
  name)."
  [func conn parent instance]
  (fn []
    (let [keynames (car/wcar conn (car/zrange keepalive-set 0 -1))
          instances (->> keynames
                         (filter #(str/starts-with? % (str parent "-")))
                         sort)]
      (when (or (empty? instances) (= (str parent "-" instance)
                                      (first instances)))
        (func)))))

(defn- update-keepalive
  [conn instance]
  (try
    (let [now (-> (java.util.Date.) .getTime)]
      (doto conn
        (car/wcar (car/zadd keepalive-set now instance))
        (car/wcar (car/zremrangebyscore keepalive-set 0 (- now keepalive)))))
    (catch Throwable e
      ;; Don't stop updating if Redis has a moment of downtime.
      (log/error {:instance instance} e "failed to update instance set"))))

(defn- start-keepalive
  "Start a task that keeps a key alive on Redis."
  [scheduler conn parent instance]
  (scheduler/interspaced scheduler (* keepalive 0.9)
                         #(update-keepalive conn (str parent "-" instance))
                         {:desc (str "taskrunner redis update " instance)}))

(defn- start-task
  "Start a single task.
  If :single-instance? is set for the task, use Redis to prevent
  multiple instances from running the task at the same time."
  [{:keys [task-name run-task-fn options single-instance?
           interspaced? description]}
   {:keys [conn parent instance scheduler]}]
  (let [wrapped-fn (if single-instance?
                     (run-if-main-instance run-task-fn conn parent instance)
                     run-task-fn)]
    (log/info {:task task-name
               :options (select-keys options [:interval
                                              :batch-size
                                              :batch-limit
                                              :initial-delay
                                              :test-mode?])}
              "Starting scheduled task")
    (cond
      (:test-mode? options) #(constantly nil)

      interspaced?
      (scheduler/interspaced scheduler (:interval options)
                             wrapped-fn
                             {:desc description
                              :initial-delay (:initial-delay options)})
      :else
      (scheduler/every scheduler (:interval options)
                       wrapped-fn
                       {:desc description
                        :initial-delay (:initial-delay options)}))))

(defn- add-context
  "Add a state atom, a context map and error handling to a task."
  [{:keys [task-name options task-fn] :as task} context]
  (let [state (atom (:initial-state options))
        task-context (assoc context
                            :options options
                            :state state)
        run-task-fn #(try
                       (task-fn task-context)
                       (catch Throwable e
                         (log/error {:task-name task-name
                                     :expound (util/expound-str (ex-data e)
                                                                context)
                                     :msg (ex-message e)}
                                    e "error running task")))]
    (assoc task
           :state state
           :run-task-fn run-task-fn)))

(defrecord TaskRunner [uri parent taskrunner? tasks env
                       db bus cache notify scheduler
                       instance tasks-with-state stop-tasks stop-keepalive]
  component/Lifecycle

  (start [this]
    (let [conn {:pool {} :spec {:uri uri}}
          tasks' (map #(add-context % {:env env
                                       :conn conn
                                       :db db
                                       :bus bus
                                       :cache cache
                                       :notify notify
                                       :tr format}) tasks)
          instance (util/rand-str 6)
          params {:conn conn
                  :instance instance
                  :parent parent
                  :scheduler scheduler}]
      (log/info {:num-tasks (count tasks)
                 :parent parent
                 :instance instance} "Starting scheduled tasks")
      (assoc this
             :instance instance
             :tasks tasks'
             :stop-tasks (doall (map #(start-task % params) tasks'))
             :stop-keepalive (when taskrunner?
                               (start-keepalive scheduler conn parent
                                                instance)))))

  (stop [this]
    (log/info {:instance instance} "Stopping tasks")
    (doseq [stop-task stop-tasks]
      (stop-task))
    (when stop-keepalive
      (stop-keepalive))
    (dissoc this :stop-tasks :stop-keepalive))

  tasks/TaskList
  (state [_ name]
    (-> (filter #(= (:task-name %) name) tasks)
        first
        :state))

  (testrun [_ name]
    (let [{:keys [run-task-fn]} (-> (filter #(= (:task-name %) name) tasks))]
      (log/debug {:task-name name
                  :tasks (vec tasks)} "Invoking task function for test")
      (try
        (run-task-fn)
        (catch Exception e
          (log/error {:task-name name} e "Exception in task function"))))))
