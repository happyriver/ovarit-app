;; gql/system/visitor_counter/pg_hyperloglog.clj -- Visitor counting for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.system.visitor-counter.pg-hyperloglog
  (:require
   [cambium.core :as log]
   [com.stuartsierra.component :as component]
   [hugsql.core :as hugsql]
   [manifold.executor :as mf-executor]
   [manifold.stream :as mf-stream]
   [ovarit.app.gql.elements.constants :refer [wrap-db-fns]]
   [ovarit.app.gql.protocols.visitor-counter :as protocol]
   [ovarit.app.gql.util :as util]))

(hugsql/def-db-fns "sql/visitor-counter.sql")
(wrap-db-fns insert-visit
             update-daily-uniques
             select-visitor-counts)

(defn add-visitor!
  "Add a visitor to the site visitor count for today."
  [db {:keys [ipaddr timestamp]}]
  (try
    (insert-visit db {:ipaddr ipaddr
                      :timestamp (or timestamp (util/sql-timestamp))})
    (catch Throwable t
      (log/error {:ip ipaddr} t "Error inserting visitor IP address"))))

(defn update-uniques-batch
  "Accumulate some visit records into the daily uniques table,
  and return a map containing progress information."
  [db batch-size]
  (try
    (let [{:keys [processed remaining latest]}
          (update-daily-uniques db {:batchsize batch-size})
          status (if (nil? latest)
                   "complete"
                   (.toString latest))]
      {:processed processed
       :remaining remaining
       :status status})
    (catch org.postgresql.util.PSQLException e
      (log/error {:exception-message (.getMessage e)}
                 "visitor counter update exception")
      {:processed 0
       :remaining nil
       :status :error})))

(defn update-uniques [{:keys [db batch-size batch-limit]}]
  (let [counter (atom 0)
        db-with-counter (assoc db :counter counter)]
    (log/debug "Starting visitor count update")
    (loop [num 0]
      (let [{:keys [processed remaining status]}
            (update-uniques-batch db-with-counter batch-size)]
        (if (and (not= status :error)
                 (pos? remaining)
                 (or (nil? batch-limit) (< (+ num processed) batch-limit)))
          (recur (+ num processed))
          (log/info {:queries @counter :rows (+ num processed)
                     :remaining remaining :status status}
                    "Finished visitor count update"))))))

(defn visitor-counts
  "Produce counts of unique visitors for ranges of `by` days.
  Start at `start`, up to and not including `end`. Both dates
  should be SQL timestamps."
  [db start end by]
  (select-visitor-counts db {:startdate start :enddate end :days by}))

(comment
  (def per-second 300)

  (defn bulk-insert-visitors
    [db]
    (let [counter (atom 0)
          db' (assoc db :counter counter)]
      (doseq [_ (range per-second)]
        (add-visitor! db'
                      {:ipaddr (str "192.168."  (rand-int 256)
                                    "." (rand-int 256))})))))

(defn create-pool
  "Create a thread pool to do visitor counter database insertions."
  []
  (let [cpu-count (.availableProcessors (Runtime/getRuntime))
        thread-count (atom 0)
        thread-factory (mf-executor/thread-factory
                        #(str "visitor-counter-pool-" (swap! thread-count inc))
                        (deliver (promise) nil))
        stats-callback (fn [stats]
                         (log/info {:stats stats}
                                   "thread pool statistics"))
        options {:stats-callback stats-callback
                 :thread-factory thread-factory}]
    (mf-executor/fixed-thread-executor cpu-count options)))

(defrecord VisitorCounter [db]
  component/Lifecycle

  (start [this]
    ;; Move database insertion onto a new thread, in order
    ;; not to tie up the calling thread with I/O.
    (log/info {} "Starting visitor counter")
    (let [pool (create-pool)
          stream (mf-stream/stream 100 nil pool)
          db-counter (atom 0)
          db' (assoc db :counter db-counter)]
      (mf-stream/consume (partial add-visitor! db') stream)
      (assoc this
             :db-counter db-counter
             :pool pool
             :stream stream)))

  (stop [{:keys [db-counter pool stream] :as this}]
    (log/info {:queries @db-counter} "Stopping visitor counter")
    (mf-stream/close! stream)
    (.shutdownNow pool)
    (dissoc this :db-counter :pool :stream)))

(extend-protocol protocol/VisitorCounter
  VisitorCounter

  (add!
    ([{:keys [stream]} ip]
     (mf-stream/put! stream {:ipaddr ip}))
    ([{:keys [stream]} ip timestamp]
     (mf-stream/put! stream {:ipaddr ip :timestamp timestamp})))

  (visitor-count [{:keys [db-counter]}]
    @db-counter)

  (visitors
    ([{:keys [db]} start end by]
     (visitor-counts db start end by))))

(comment
  (require 'user)
  (def db (:db user/system))

  (defn ts [y m d]
    (-> (java.util.Date. (- y 1900) (dec m) d)
        .getTime
        java.sql.Timestamp.))

  (visitor-counts db (ts 2021 06 20) (ts 2021 06 27) 3)

  (add-visitor! db "192.168.0.0" nil)

  (def generate-count 100000)
  (def days 60)
  (def day-ms (* 24 60 60 1000))
  (def timespan (* 24 60 60 days))
  (def end-date (java.util.Date.))
  (def begin-date (java.util.Date. (- (.getTime end-date)
                                      (* 1000 timespan))))

  (defn generate-ip-addr
    []
    (str "192.168."  (rand-int 256) "." (rand-int 256)))

  (defn generate-timestamp
    []
    (let [now (.getTime end-date)
          then (- now (* 1000 (rand-int timespan)))]
      (java.sql.Timestamp. then)))

  (defn generate-visitors
    []
    (map (fn [_] {:ipaddr (generate-ip-addr)
                  :timestamp (generate-timestamp)})
         (range generate-count)))

  (def visitors (generate-visitors))
  (count visitors)

  (defn insert-visitors
    [vc visitors]
    (doseq [{:keys [ipaddr timestamp]} visitors]
      (protocol/add! vc ipaddr timestamp)))
  (insert-visitors (:visitor-counter user/system) visitors))
