;; gql/resolver-helpers/pagination.clj -- Schema pagination for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.resolver-helpers.pagination
  "Support pagination for resolver functions."
  (:require
   [clojure.string :as str]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [java-time.api :as jt]
   [ovarit.app.gql.resolver-helpers.core :refer [fail-verification get-in*]]
   [ovarit.app.gql.util :as util]))

(def query-size-limit 1000)

(defn query-size-ok?
  [size]
  (and (pos? size) (<= size query-size-limit)))

;; Cursors

(def cursor-format "yyyyMMddHHmmssSSS")

(defn make-cursor
  "Make a cursor to use in pagination from a timestamp."
  [timestamp]
  (->> (jt/zoned-date-time timestamp "UTC")
       (jt/format cursor-format)
       util/encode))

(defn timestamp-from-cursor
  "Get the timestamp from a cursor."
  [cursor]
  (->> cursor
       util/decode
       (jt/sql-timestamp cursor-format)))

;; Pagination

(defn pagination [context]
  (::pagination-depth context))

(defn make-node [val]
  {:node (dissoc val :cursor)
   :cursor (:cursor val)})

(defn make-connection-object
  "Make a connection object out of a list of items. Each item should
  be a map with a :cursor entry."
  [more? data]
  (when (seq data)
    (let [edges (map make-node data)]
      {:edges edges
       :pageInfo {:hasPreviousPage false
                  :hasNextPage more?
                  :startCursor (:cursor (first edges))
                  :endCursor (:cursor (first (take-last 1 edges)))}})))

(defn resolve-pagination
  "Resolve a paginated result by converting a raw list of maps to a list
  of resolved results, and wrapping that list with a Connection
  object.  Inject into the context 'pagination-depth', which should
  be a sequence of two-item vectors containing the type of object
  queried (:Post, :Comment, etc) and the number of objects requested."
  [data number-requested pagination-depth]
  (if (resolve/is-resolver-result? data)
    data
    ;; Callers asked the database for one extra item, which
    ;; is how we determine that there are more pages available.
    (let [more? (> (count data) number-requested)
          items (if more? (butlast data) data)]
      (resolve/with-context (make-connection-object more? items)
        {::pagination-depth pagination-depth}))))

(defn- make-connection-object-v2
  [items {:keys [more? forward? cursor]}]
  (when (seq items)
    {:edges (map make-node items)
     :pageInfo {:hasPreviousPage (boolean (or (and (not forward?) more?)
                                              (and forward? cursor)))
                :hasNextPage (boolean (or (and forward? more?)
                                          (and (not forward?) cursor)))
                :startCursor (:cursor (first items))
                :endCursor (:cursor (last items))}}))



(defn resolve-pagination-v2
  "Resolve a paginated result.
  Converting a raw list of maps to a list of resolved results, and
  wrapping that list with a Connection object.  Inject into the
  context 'pagination-depth', which should be a sequence of two-item
  vectors containing the type of object queried (:Post, :Comment, etc)
  and the number of objects requested.  Pass bidirectional pagination
  arguments as set up by `prep-args` below."
  ([data args pagination-depth]
   (resolve-pagination-v2 data args pagination-depth {}))
  ([data {:keys [page-size forward?] :as args} pagination-depth extra-context]
   (if (resolve/is-resolver-result? data)
     data
     ;; Callers asked the database for one extra item, which
     ;; is how we determine that there are more pages available.
     (let [more? (> (count data) page-size)
           items (cond-> data
                   more? butlast
                   (not forward?) reverse)]
       (resolve/with-context
         (make-connection-object-v2 items (assoc args :more? more?))
         (merge {::pagination-depth pagination-depth}
                extra-context))))))

(defn one-more
  "Ask the database for one more item than requested by the query."
  [args]
  (assoc args :first (inc (:first args))))

;; Encoded identifiers

(defn encode-id
  "Base64 encode an integer combined with a keyword."
  [id model]
  (util/encode (str (name model) " " id)))

(defn decode-id
  "Decode an integer from a Base64 string.
  Ensure that the keyword included in the string matches
  `expected-model`.  Return nil if the keyword is not found or the
  integer cannot be parsed."
  [base64id expected-model]
  (try
    (let [elems (str/split (util/decode base64id) #" ")
          [model value] elems]
      (when (= (keyword model) expected-model)
        (Integer/parseInt value)))
    (catch Exception _
      nil)))

(comment (encode-id 4 :MessageThread)
         (-> (encode-id 4 :Message)
             (decode-id :Message))
         (-> (encode-id "a" :Message)
             (decode-id :Message))
         (-> "barf"
             (decode-id :Message))
         (-> (encode-id 100 :Message)
             (decode-id :Thread)))

;; Validators

(defn verify-pagination-not-nested
  "Verify that there is no nested pagination in the context."
  []
  {:name ::verify-pagination-not-nested
   :func
   (fn [context args]
     (if (pagination context)
       (fail-verification args "Query too complex" 400)
       args))})

(defn verify-query-size
  "Verify that the total number of items requested is within limits."
  [first-path]
  {:name ::verify-query-size
   :func
   (fn [_ args]
     (if (query-size-ok? (get-in* args first-path))
       args
       (fail-verification args "Too many items requested" 400)))})

(defn prep-args
  "Parse the pagination arguments.
  Set :page-size, :cursor and :forward? in `args`, based on
  :first, :last, :before and :after.  Fail if the number of items
  requested exceeds `max-page-size`."
  [max-page-size]
  {:name ::prep-args
   :func
   (fn [_ {:keys [first last before after] :as args}]
     (let [cursor (or after before)
           page-size (or first (and before last))
           forward? (some? (or (nil? cursor) after))]
       (cond
         (nil? page-size)
         (fail-verification args "No items requested" 400)

         (> page-size max-page-size)
         (fail-verification args "Too many items requested" 400)

         (or (and first last) (and before after))
         (fail-verification args "Conflicting arguments" 400)

         :else
         (assoc args
                :cursor cursor
                :page-size page-size
                :forward? forward?))))})
