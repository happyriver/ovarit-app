;; gql/resolver-helpers/core.clj -- GraphQL resolver utilities
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.resolver-helpers.core
  (:require [cambium.core :as log]
            [com.walmartlabs.lacinia.resolve :as resolve]
            [ovarit.app.gql.resolver-helpers.stream :as stream]
            [ovarit.app.gql.util :as util]
            [promesa.core :as prom]))

(defn fail-verification
  "Cause a resolver prepare sequence to fail by throwing an exception."
  ([args] (fail-verification args "Bad Request"))
  ([args message-or-seq]
   (if (string? message-or-seq)
     (fail-verification args message-or-seq 400)
     (apply fail-verification (cons args message-or-seq))))
  ([args message status]
   (throw (ex-info "Verification failure"
                   {::message message
                    ::status status
                    ::args args}))))

(defn assoc-in*
  "Run `assoc` if `path` is a single value and `assoc-in` if it is a sequence."
  [coll path val]
  (if (keyword? path)
    (assoc coll path val)
    (assoc-in coll path val)))

(defn update-in*
  "Run `update` if `path` is a single value otherwise `update-in`."
  [coll path fn]
  (if (keyword? path)
    (update coll path fn)
    (update-in coll path fn)))

(defn get-in*
  "Do a `get` if `path` is a single value and `get-in` if it is a sequence."
  [coll path]
  (if (keyword? path)
    (get coll path)
    (get-in coll path)))

(defn- explain-invalid [path]
  (str "Invalid argument" (when (keyword? path)
                            (str ": " (name path)))))

(defn convert-arg-to-int
  "Convert the value at `path` in `args` to an integer."
  [path]
  {:name :convert-arg-to-int
   :func
   (fn [_ args]
     (try
       (if-let [parsed (parse-long (get-in* args path))]
         (assoc-in* args path parsed)
         (fail-verification args (explain-invalid path)))
       (catch java.lang.IllegalArgumentException _
         (fail-verification args (explain-invalid path)))))})

(defn convert-arg-to-int-seq
  "Convert the values at `path` in `args` to a sequence of integers."
  [path]
  {:name :convert-arg-to-int-seq
   :func
   (fn [_ args]
     (let [parsed (->> (get-in* args path)
                       (map parse-long))]
       (if (some nil? parsed)
         (fail-verification args (explain-invalid path))
         (assoc-in* args path parsed))))})

(defn verify-some
  "Verify that the value at `path` in `args` is non-nil."
  [path & fail-as]
  {:name :verify-some
   :func
   (fn [_ args]
     (if (some? (get-in* args path))
       args
       (fail-verification args fail-as)))})

(defn verify-fn
  "Verify that `func` returns truthy when applied to the value at `path`."
  [path func & fail-as]
  {:name :verify-fn
   :func
   (fn [_ args]
     (if (func (get-in* args path))
       args
       (fail-verification args fail-as)))})

(defn verify=
  "Verify that the object at `path1` is equal to the object at `path2`."
  [path1 path2 & fail-as]
  {:name :verify=
   :func
   (fn [_ args]
     (if (= (get-in* args path1) (get-in* args path2))
       args
       (fail-verification args fail-as)))})

(defn ignore-if-nil
  "Run `verify-fn` only if the value at `path` in args is non-nil."
  [path {:keys [name func]}]
  {:name :ignore-if-nil
   :func
   (fn [context args]
     (if (nil? (get-in* args path))
       args
       (-> (func context (assoc args ::name name))
           (dissoc ::name))))})

(defn prepare-args
  "Run the arguments through the list of argument preparers.
  Return the modified arguments, or a resolved error if a
  validation problem was found."
  [{:keys [name prepare]} context args]
  (try
    (let [run-one (fn [args {:keys [func name]}]
                    (let [result (func context (assoc args ::name name))]
                      (log/trace {:name name :args args :result result}
                                 "Ran prepare fn")
                      result))
          prepared (-> (reduce run-one args prepare)
                       (dissoc ::name))]
      (log/trace {:name name
                  :args-before args
                  :args-after prepared}
                 "Prepared arguments for resolver")
      prepared)
    (catch clojure.lang.ExceptionInfo e
      (let [{:keys [::message ::status]} (ex-data e)]
        (if message
          (log/info {:name name
                     :args (::args (ex-data e))
                     :status status}
                    "Resolver args failed validation")
          (log/error {:name name
                      :expound (util/expound-str (ex-data e) context)} e
                     "Error preparing resolver args"))
        (resolve/resolve-as nil
                            {:message (or message "Internal server error")
                             :status (or status 500)})))))

(defn run-resolver
  "Resolve a GraphQL query using the functions in a resolver map."
  [resolver context args resolved]
  (if (fn? resolver)
    (resolver context args resolved)
    (let [{:keys [name resolve notify]} resolver
          prepared-args (prepare-args resolver context args)]
      (if (resolve/is-resolver-result? prepared-args)
        prepared-args
        (try
          (let [result (resolve context prepared-args resolved)
                notify-all #(doseq [notifier notify]
                              (notifier context prepared-args resolved %))]
            (if (prom/promise? result)
              (prom/then result notify-all)
              (notify-all result))
            result)
          (catch Exception e
            (log/error {:name name
                        :expound (util/expound-str (ex-data e) context)}
                       e "Error in resolver")
            (resolve/with-error nil
              {:message "Internal server error" :status 500})))))))

(defn run-streamer
  "Start a GraphQL subscription using the functions in a streamer map."
  [streamer context args source-stream-callback]
  (log/debug {:is-fn (fn? streamer)} "Starting streamer")
  (if (fn? streamer)
    (streamer context args source-stream-callback)
    (let [_ (log/debug {:streamer-name (:name streamer)} "run-streamer map")
          {:keys [name stream]} streamer
          prepared-args (prepare-args streamer context args)]
      (if (resolve/is-resolver-result? prepared-args)
        (stream/stream-resolution prepared-args source-stream-callback)
        (try
          (log/debug {:streamer-name name
                      :is-fn (fn? stream)
                      :args prepared-args} "running streamer fn")
          (stream context prepared-args source-stream-callback)
          (catch Exception e
            (log/error {:name name
                        :expound (util/expound-str (ex-data e) context)}
                       e "Error in streamer")
            (-> (resolve/resolve-as nil {:message "Internal server error"})
                (stream/stream-resolution source-stream-callback))))))))
