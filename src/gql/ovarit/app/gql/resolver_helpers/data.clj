;; gql/resolver-helpers/data.clj -- Dataloader support for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.gql.resolver-helpers.data
  "Support for resolvers."
  (:require
   [cambium.core :as log]
   [clojure.edn :as edn]
   [com.walmartlabs.lacinia.executor :as executor]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [ovarit.app.gql.util :as util]
   [superlifter.api :as superlifter]))

(defmacro with-superlifter [ctx body]
  `(superlifter/with-superlifter (get-in ~ctx [:request :superlifter])
     ~body))

;;; Error handling

(defn resolve-exception [e]
  (log/error e "Resolver error")
  (resolve/with-error nil {:message "Internal server error"
                           :status 500}))

(defn resolve-exception-list [many]
  (let [resolution (resolve/with-error nil {:message "Internal server error"
                                            :status 500})]
    (map (fn [_] resolution) many)))

;;; Generic fetch things

(defn id-from-fetch-args
  "Make a unique id for caching and batching loads."
  [select]
  (.toString select))

(defn fetch-args-from-id
  "Turn the caching and batching id back into a map."
  [id]
  (edn/read-string id))

(defn make-fetcher-fn [by loader fixer]
  (fn [many env]
    (log/with-logging-context (:logging-context env)
      (when (> (count many) 1)
        (log/debug {:object-count (count many) :by by} "Combining object load"))
      (try
        (let [fetch-args (map #(fetch-args-from-id (:id %)) many)
              identifiers (map #(by %) fetch-args)
              data (loader fetch-args env)
              ;; Put the data records into the same order as the identifiers.
              datamap (apply merge (map #(hash-map (by %) %) data))
              ordered (map #(get datamap %) identifiers)]
          (map (fn [item]
                 (if (nil? item)
                   (resolve/with-error nil {:message "Not found"})
                   (fixer item env)))
               ordered))
        (catch Throwable e
          (log/error {:expound (util/expound-str (ex-data e) env)}
                     e "Error in superfetcher")
          (resolve-exception-list many))))))

(defn make-list-fetcher-fn [by loader fixer]
  (fn [many env]
    (log/with-logging-context (:logging-context env)
      (log/debug {:object-count (count many) :by by}
                 "Combining object list load")
      (try
        (let [fetch-args (map #(fetch-args-from-id (:id %)) many)
              identifiers (map #(by %) fetch-args)
              data (loader fetch-args env)
              ordered (map #(get data %) identifiers)]
          (map (fn [entries]
                 (map #(fixer % env) entries))
               ordered))
        (catch Throwable e
          (log/error {:expound (util/expound-str (ex-data e) env)}
                     e "Error in list superfetcher")
          (resolve-exception-list many))))))

;; Decide what to fetch.

(defn wants-fields?
  "Determine whether a sub query needs one of the fields in a list."
  [context fields]
  (some #(executor/selects-field? context %) fields))

;; Expand superfetcher buckets for contained objects.

(defn inc-threshold
  "Increment the trigger threshold for an elastic bucket."
  [trigger-opts _]
  (log/trace {} "inc-threshold")
  (update trigger-opts :threshold inc))

(defn raise-threshold-by-count
  "Raise the trigger threshold by the count of fetched objects."
  [trigger-opts objs]
  (if (resolve/is-resolver-result? objs)
    trigger-opts
    (do
      (log/trace {:count (count objs)} "raise-threshold-by-count")
      (update trigger-opts :threshold + (count objs)))))

(defn raise-threshold-fn
  "Create a function to raise the trigger threshold for an elastic bucket."
  [by]
  (fn [trigger-opts _]
    (log/trace {:by by} "raise-threshold-fn")
    (update trigger-opts :threshold + by)))

(defn update-trigger!
  "Update a trigger if a subselection requires it."
  [p context field bucket fn]
  (log/trace {:field-selected (wants-fields? context [field])
              :field field
              :bucket bucket} "update-trigger!")
  (cond-> p
    (wants-fields? context [field])
    (superlifter/update-trigger! bucket :elastic fn)))
