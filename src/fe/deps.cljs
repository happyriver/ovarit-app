{:npm-dev-deps {"shadow-cljs"           "2.20.7"}
 :npm-deps {"markdown-it"                 "13.0.2"
            "markdown-it-link-attributes" "4.0.1"
            "markdown-it-reddit-spoiler"  "1.0.1"
            "markdown-it-sub"             "1.0.0"
            "markdown-it-sup"             "1.0.0"
            "pikaday"                     "1.8.2"
            "moment"                      "2.29.4"}}
