;; fe/db.cljs -- Application data for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.db
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.spec :as content]
   [ovarit.app.fe.routes :as routes]
   [ovarit.app.fe.routes.table :refer [routing-table]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user.spec :as user]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

(def default-db
  {
   ;; What do we want to look at?
   :view {:active-panel :initializing}

   ;; What have we loaded to look at?
   :content {:announcement nil
             :motto ""
             :recent-errors #{}}

   :reldb (-> {}
              ;; What objects have we retrieved from the server?
              (rel/mat [[:from :Comment] [:unique :cid]])
              (rel/mat [[:from :CommentNode] [:unique :cid :sort]])
              (rel/mat [[:from :ContentStatus]
                        [:unique [loader/get-content-status-id]]])
              (rel/mat [[:from :DownvoteNotification]
                        [:unique :mid]])
              (rel/mat [[:from :RequireNameChange] [:unique :name]])
              (rel/mat [[:from :Message] [:unique :mid]])
              (rel/mat [[:from :MessageThread] [:unique :thread-id]])
              (rel/mat [[:from :PollOption] [:unique :option-id]])
              (rel/mat [[:from :Post] [:unique :pid]])
              (rel/mat [[:from :PostUserAttributes] [:unique :pid]])
              (rel/mat [[:from :User] [:unique :uid]])
              (rel/mat [[:from :UserStats] [:unique :uid]])
              (rel/mat [[:from :Sub] [:unique :sid]])
              (rel/mat [[:from :SubMod] [:unique :sid :uid]])
              (rel/mat [[:from :SubModNotificationCount] [:unique :sid]])
              (rel/mat [[:from :SubName] [:unique :sid]])
              (rel/mat [[:from :SubName] [:unique :name]])
              (rel/mat [[:from :SubName] [:unique [str/lower-case :name]]])
              (rel/mat [[:from :SubPostFlair] [:unique :id]])
              (rel/mat [[:from :SubPostTypeConfig] [:unique :name :post-type]])
              (rel/mat [[:from :SubUserFlair] [:unique :sid :uid]])
              (rel/mat [[:from :SubRule] [:unique :id]])

              ;; What user state are we tracking?
              (rel/mat [[:from :FormState] [:unique :id]])
              (rel/mat [[:from :SubPostFlairState] [:unique :id]]))

   ;; What's in process?
   :status {:user-email            :not-loaded
            :comment-tree          :not-loaded
            :content-blocks        :not-loaded
            :content               :not-loaded
            :more-comments         {}
            :single-post           :not-loaded
            :subs                  :not-loaded
            :subscriptions         :not-initialized
            :payments              :not-loaded
            :registration-settings :not-loaded}

   ;; What app-wide settings do we have?
   :settings {:default-language :en
              :day-night :light
              :sub-prefix "s"
              ;; Page size to request for messages.
              :page-size 25
              ;; Tree size to request for comments.
              :comment-tree {:level 5
                             :num 50}}

   ;; What websocket subscriptions do we have?
   :subscriptions {:status :not-ready
                   ;; Events to dispatch when the client is ready.
                   :on-start []
                   ;; Keywords of subscriptions to stop when we route
                   ;; to a different page.
                   :stop-on-routing []}

   ;; Results of recent GraphQL queries.
   :query-cache []

   ;; What is the state of user interface elements?
   :ui-state {:menu :closed}

   ;; Statecharts used by forms.
   :fsm {:generic-form forms/generic-form-machine}

   ;; The routing table derived from the Python app.
   :routes routing-table

   ;; Result of parsing the current route.
   :route {}

   ;; Information on the currently logged-in user, or nil if anonymous.
   :current-user nil

   ;; All errors from network operations, debug mode only.
   :errors []

   ;; Incremented with each new routing event, used to mark old
   ;; content that can be discarded.
   :epoch 0

   ;; A recent time; used to update relative post times.
   :now (.getTime (js/Date.))})

(re-frame/reg-event-db ::initialize-db
  (fn-traced [_ _]
    default-db))

;; These are the invariants about default-db, checked after every event
;; when config/debug? is set.

;; Status of data requested from the server.
(spec/def :db.status/comment-tree   #{:not-loaded :loading :loaded})
(spec/def :db.status/content        #{:not-loaded :loading :loaded})
(spec/def :db.status/content-blocks #{:not-loaded :loading :loaded})
(spec/def :db.status/more-comments  (spec/map-of string? #{:loading}))
(spec/def :db.status/payments       #{:not-loaded :loading :loaded :error})
(spec/def :db.status/registration-settings #{:not-loaded :loading :loaded :error})
(spec/def :db.status/single-post    #{:not-loaded :loading :loaded})
(spec/def :db.status/subs           #{:not-loaded :loading :loaded})
(spec/def :db.status/subscriptions  #{:not-initialized :initializing :ready})
(spec/def ::status (spec/keys :req-un [:db.status/comment-tree
                                       :db.status/content
                                       :db.status/content-blocks
                                       :db.status/more-comments
                                       :db.status/payments
                                       :db.status/single-post
                                       :db.status/subs
                                       :db.status/subscriptions]
                              :opt-un [::content/content-loader]))

;; Status of UI elements.
(spec/def :db.ui-state/menu #{:open :closed})
(spec/def ::ui-state
  (spec/merge (spec/keys :req-un [:db.ui-state/menu])
              ::content/ui-state))

;; Settings
(spec/def :db.settings/default-language keyword?)
(spec/def :db.settings/day-night #{:light :dark :dank})
(spec/def :db.settings/sub-prefix string?)
(spec/def :db.settings/page-size pos?)
(spec/def :db.settings.comment-tree/level nat-int?)
(spec/def :db.settings.comment-tree/num nat-int?)
(spec/def :db.settings/comment-tree
  (spec/keys :req-un [:db.settings.comment-tree/level
                      :db.settings.comment-tree/num]))
(spec/def ::settings (spec/keys :req-un [:db.settings/default-language
                                         :db.settings/day-night
                                         :db.settings/sub-prefix
                                         :db.settings/comment-tree]))

;; Query Cache
(spec/def :query-cache/query keyword?)
(spec/def :query-cache/variables map?)
(spec/def :query-cache/result map?)
#_(spec/def :query-cache/expires (spec/nilable int?))
(spec/def ::query-cache
  (spec/nilable
   (spec/coll-of (spec/keys :req-un [:query-cache/query
                                     :query-cache/variables
                                     :query-cache/result
                                     #_:query-cache/expires]))))

;; Environment
(spec/def ::now int?)

;; History
(spec/def ::epoch int?)

(spec/def ::db (spec/keys :req-un [::content/content
                                   ::user/current-user
                                   ::epoch
                                   ::now
                                   ::query-cache
                                   ::settings
                                   ::status
                                   ::ui-state
                                   ::routes/route
                                   ::routes/view]))

;; A subscription that needs a home to avoid a circular dependency.
(re-frame/reg-sub :settings/current-language
  (fn [db _]
    (or (get-in db [:current-user :language])
        :en)))
