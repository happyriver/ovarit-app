;; fe/log.cljs -- Logging macros for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.log
  (:require [ajax.core :as ajax]
            [clojure.walk :as walk]
            [lambdaisland.glogi :as glogi]
            [ovarit.app.fe.config :as config]
            [ovarit.app.fe.content.settings :as-alias settings]
            [re-frame.core :as re-frame])
  (:require-macros [ovarit.app.fe.log]))

(defn log
  "Output a log message to the given logger.
  Optionally provide an exception to be logged."
  ([name lvl message]
   (glogi/log name lvl message nil))
  ([name lvl message exception]
   (glogi/log name lvl message exception)))

(defn db-handler
  "Record errors and warnings in the db."
  [{:keys [level] :as log-record}]
  (when (>= (glogi/level-value level) (glogi/level-value :warning))
    (re-frame/dispatch [::settings/add-log-message log-record])))

(defn- mask-passwords
  "Strip possible passwords out of log records sent to the back end."
  [log-record]
  (walk/postwalk (fn [node]
                   (if (map? node)
                     (let [{:keys [password new_password]} node]
                       (cond-> node
                         password (assoc :password "***")
                         new_password (assoc :new_password "***")))
                     node))
                 log-record))

(defn- dissoc-graphql
  ;; GraphQL results are useful in the browser console, but
  ;; avoid sending them back to the server.
  [{:keys [logger-name message] :as  log-record}]
  (cond-> log-record
    (and (= logger-name "ovarit.app.fe.graphql")
         (map? message))
    (update-in [:message :response] dissoc :data)))

(defn- exception-message
  "Strip exception data, leaving the messages."
  [log-record]
  (walk/postwalk (fn [node]
                   (if (map? node)
                     (let [{:keys [exception]} node]
                       (cond-> node
                         exception (assoc :exception
                                          (str (.-name exception)
                                               ": "
                                               (.-message exception)))))
                     node))
                 log-record))

(def volume-limit 5)
(def volume
  (atom {}))

(defn backend-handler
  "Send log messages to the back end."
  [{:keys [logger-name] :as log-record}]
  (try
    (let [handler (constantly nil)
          error-handler (constantly nil)
          log-record (-> log-record
                         dissoc-graphql
                         mask-passwords
                         exception-message)]
      (swap! volume update logger-name (fnil inc 0))
      (when (< (get @volume logger-name) volume-limit)
        (ajax/POST "/do/record_log"
                   {:params log-record
                    :response-format :json
                    :keywords? true
                    :format :json
                    :handler handler
                    :error-handler error-handler})))
    (catch js/Error e
      (when config/debug?
        (js/console.log "log/backend-handler error" e)))))
