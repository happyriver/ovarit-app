;; fe/content/load_more.cljs -- Content loader for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.load-more
  (:require
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [re-frame.core :as re-frame]))

;; This file is required by window.cljs which is required by lots of things.
;; To avoid circular dependencies, use fully namespaced keywords rather than
;; events.
(def check-invite-codes
  [:ovarit.app.fe.content.admin/invite-code-table-check-load-more])

(re-frame/reg-event-fx ::check-load-more
  ;; :doc Check whether more content needs to be loaded.
  (fn-traced [{:keys [db]} [_ _]]
    (let [panel (get-in db [:view :active-panel])
          ready (= (get-in db [:status :content]) :loaded)
          recent-errors (get-in db [:content :recent-errors])]
      (when (and ready (empty? recent-errors))
        {:fx [(when (= panel :admin-invite-codes)
                [:dispatch check-invite-codes])]}))))
