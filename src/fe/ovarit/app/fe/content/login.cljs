;; fe/content/login.clj -- Login for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.login
  (:require
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.login :as-alias login-form]
   [ovarit.app.fe.util :as util]))

(util/reg-ajax-event-chain ::login
  ;; :doc Log in and redirect on success.
  {:call
   (fn-traced [_ [_ {:keys [form]}]]
     (let [{:keys [username password remember-me?]} form]
       {:fx [[::util/ajax-post
              {:url "/ovarit_auth/authenticate"
               :payload {:username (str/trim username)
                         :password password
                         :remember_me remember-me?}
               :form form}]]}))
   :success
   (fn-traced [{:keys [db]} [_ {:keys [form]}]]
     {:db (forms/transition-after-response db (:id form))
      :fx [[::routes/redirect (:next-url form)]]})
   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form error-response]}]]
     (let [{:keys [reason message]} (-> error-response :response)
           error-info {:event (:id form)
                       :errors [error-response]
                       :describe-error-fn #(errors/describe-auth-error db %)}]
       {:db (-> db
                (errors/assoc-errors error-info)
                (forms/transition-after-response (:id form)))
        :fx [(when (= reason "Name change required")
               [:dispatch [::forms/edit (:id form)
                           :name-change-message message]])]}))})

(util/reg-ajax-event-chain ::change-name
  ;; :doc Send a change-name request when one is required to login.
  {:call
   (fn-traced [_ [_ {:keys [form]}]]
     (let [{:keys [username password new-username]} form]
       {:fx [[::util/ajax-post
              {:url "/ovarit_auth/change_username"
               :payload {:username (str/trim username)
                         :new_username (str/trim new-username)
                         :password password}
               :form form}]]}))
   :success
   (fn-traced [{:keys [db]} [_ {:keys [form]}]]
     {:db (-> db
              (assoc-in [:view :options :notify] :name-change)
              (forms/transition-after-response (:id form)))
      :fx [[:dispatch [::forms/edit (:id form) :name-change-message
                       nil]]
           [:dispatch [::forms/edit (:id form) :username
                       (:new-username form)]]]})
   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form error-response]}]]
     (let [error-info {:event (:id form)
                       :errors [error-response]
                       :describe-error-fn #(errors/describe-auth-error db %)}]
       {:db (-> db
                (errors/assoc-errors error-info)
                (forms/transition-after-response (:id form)))}))})
