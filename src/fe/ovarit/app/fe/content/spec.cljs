;; fe/content/spec.cljs --  Content specs for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.spec
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.graphql-spec :as gspec]
   [weavejester.dependency :as dep])
  (:require
   [ovarit.app.fe.content.spec.content-loader :as-alias content-loader]))

;; Specifications for the content map in the app db.

(spec/def :content/announcement-post
  (spec/keys :req-un [:type.Post/pid]))

(spec/def :content.pagination/more-items-available? (spec/nilable boolean?))
(spec/def :content.pagination/cursor (spec/nilable ::gspec/non-empty-string))

(spec/def :content/motto string?)

(spec/def :content.recent-error/msg
  (spec/or :message string?
           :html vector?))
(spec/def :content.recent-error/event keyword?)
(spec/def :content.recent-error/timestamp #(= (type %) js/Date))
(spec/def :content/recent-error (spec/keys :req-un
                                           [:content.recent-error/msg
                                            :content.recent-error/event
                                            :content.recent-error/timestamp]))
(spec/def :content/recent-errors
  (spec/and set? (spec/coll-of :content/recent-error)))

;; Comments

(spec/def :content.comments.by-post/updates
  (spec/coll-of map?))
(spec/def :content/comments
  (spec/map-of :type.Post/pid
               (spec/keys :opt-un [:content.comments.by-post/updates])))

;; UI state for content elements

;; UI state for comments
(spec/def :content.ui-state.comment.by-cid/checked-off? boolean?)
(spec/def :content.ui-state.comment.by-cid/content-history-index (spec/nilable nat-int?))
(spec/def :content.ui-state.comment.by-cid/content-dom-node
  (spec/nilable some?)) ;; how do you spec a dom element?
(spec/def :content.ui-state.comment.by-cid/height nat-int?)
(spec/def :content.ui-state.comment.by-cid/highight #{:new})
(spec/def :content.ui-state.comment.by-cid/hover-collapse (spec/nilable boolean?))
(spec/def :content.ui-state.comment.by-cid/marked-viewed? boolean?)
(spec/def :content.ui-state.comment.by-cid/show-reply-editor? (spec/nilable boolean?))
(spec/def :content.ui-state.comment.by-cid/toggle-collapse boolean?)
(spec/def :content.ui-state/comment
  (spec/map-of :type.Comment/cid
               (spec/nilable
                (spec/keys :opt-un [:content.ui-state.comment.by-cid/checked-off?
                                    :content.ui-state.comment.by-cid/content-dom-node
                                    :content.ui-state.comment.by-cid/height
                                    :content.ui-state.comment.by-cid/highlight
                                    :content.ui-state.comment.by-cid/hover-collapse
                                    :content.ui-state.comment.by-cid/marked-viewed?
                                    :content.ui-state.comment.by-cid/show-reply-editor?
                                    :content.ui-state.comment.by-cid/toggle-collapse]))))

;; A FSM per comment for changing the view of the content
(spec/def :content.ui-state.comment-content-view/_state #{:html :source :editor})
(spec/def :content.ui-state/comment-content-view
  (spec/map-of :type.Comment/cid
               (spec/keys :req-un [:content.ui-state.comment-content-view/_state])))

;; State that applies to the entire comment listing on the single-post page.
(spec/def :content.ui-state.comments/last-toggled-cid
  :type.Comment/cid)
(spec/def :content.ui-state/comments
  (spec/keys :opt-un [:content.ui-state.comments/last-toggled-cid]))

(spec/def :content.ui-state.post/content-height nat-int?)
(spec/def :content.ui-state.post/scroll-to-cid
  (spec/nilable :type.Comment/cid))
(spec/def :content.ui-state.post/show-content-editor? boolean?)
(spec/def :content.ui-state.post/show-edit-post-flair? boolean?)
(spec/def :content.ui-state.post/show-login? boolean?)
(spec/def :content.ui-state.post/show-mod-delete-post-form? boolean?)
(spec/def :content.ui-state.post/show-mod-undelete-post-form? boolean?)
(spec/def :content.ui-state.post/show-post-title-editor? boolean?)
(spec/def :content.ui-state.post/show-report-form? boolean?)
(spec/def :content.ui-state.post/show-source? boolean?)
(spec/def :content.ui-state.post/sort-path
  (spec/or :sort #{[:BEST] [:TOP] [:NEW]}
           :direct (spec/cat :key #{:direct} :val :type.Comment/cid)))
(spec/def :content.ui-state/post
  (spec/keys :opt-un [:content.ui-state.post/content-height
                      :content.ui-state.post/scroll-to-cid
                      :content.ui-state.post/show-content-editor?
                      :content.ui-state.post/show-edit-post-flair?
                      :content.ui-state.post/show-login?
                      :content.ui-state.post/show-mod-delete-post-form?
                      :content.ui-state.post/show-mod-undelete-post-form?
                      :content.ui-state.post/show-post-title-editor?
                      :content.ui-state.post/show-report-form?
                      :content.ui-state.post/show-source?
                      :content.ui-state.post/sort-path]))

(spec/def :content.ui-state.comment-modals/show-mod-delete-comment-form?
  boolean?)
(spec/def :content.ui-state.comment-modals/show-mod-undelete-comment-form?
  boolean?)
(spec/def :content.ui-state/comment-modals
  (spec/keys
   :opt-un
   [:content.ui-state.comment-modals/show-mod-delete-comment-form?
    :content.ui-state.comment-modals/show-mod-undelete-comment-form?]))

(spec/def ::ui-state
  (spec/keys :opt-un [:content.ui-state/post
                      :content.ui-state/comment
                      :content.ui-state/comments
                      :content.ui-state/comment-content-view
                      :content.ui-state/comment-modals]))

(spec/def ::content (spec/keys :req-un [:content/motto
                                        :content/recent-errors]
                               :opt-un [:content/announcement-post
                                        :content/comments
                                        :content/modmail]))

;; Content loader FSM state

(spec/def ::content-loader/_state #{:initial :loading :error :loaded})
(spec/def ::content-loader/epoch int?)
(spec/def ::content-loader/completed
  (spec/map-of some? #{:error :done}))
(spec/def ::content-loader/graph
  #(= (type %) dep/MapDependencyGraph))
(spec/def ::content-loader
  (spec/keys :req-un [::content-loader/_state
                      ::content-loader/epoch
                      ::content-loader/completed
                      ::content-loader/graph]))
