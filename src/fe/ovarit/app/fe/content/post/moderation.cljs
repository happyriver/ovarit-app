;; fe/content/post/moderation.cljs -- Single post moderation for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.post.moderation
  (:require
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.post :as-alias post]
   [ovarit.app.fe.content.post.db :refer [single-post assoc-post
                                          next-sticky-post-sort]]
   [ovarit.app.fe.content.settings :as-alias settings]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :as graphql]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.ui :as ui]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.moderation :as mod-forms]
   [ovarit.app.fe.ui.page-title :as-alias page-title]
   [ovarit.app.fe.user :as-alias user]
   [ovarit.app.fe.util :as util]
   [re-frame.core :as re-frame]))

(re-frame/reg-event-db ::toggle-mod-delete-post-form
  ;; :doc Toggle the display of the moderator delete post reason form.
  (fn-traced [db [_ _]]
    (-> db
        (mod-forms/initialize-action-form :delete-post)
        (update-in [:ui-state :post :show-mod-delete-post-form?] not))))

(defn- hide-mod-delete-post-form
  "Hide the moderator delete post reason form."
  [db]
  (assoc-in db [:ui-state :post :show-mod-delete-post-form?] false))

(re-frame/reg-sub ::show-mod-delete-post-form?
  ;; :doc Extract whether the moderator delete post reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-mod-delete-post-form?])))

(re-frame/reg-event-fx ::mod-delete-post
  ;; :doc Send a mod post delete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [pid reason]} form
          state {:form form
                 :restore #(assoc-post % pid :status :ACTIVE)}
          is-mod? (internal/is-mod-of-sub? db)]
      {:db (assoc-post db pid :status (if is-mod?
                                        :DELETED_BY_MOD
                                        :DELETED_BY_ADMIN))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :remove-post
                         :id (str :remove-post pid)
                         :variables {:pid pid
                                     :reason reason}
                         :handler
                         [::receive-mod-delete-post-response state]}]]]})))

(re-frame/reg-event-db ::receive-mod-delete-post-response
  ;; :doc Handle a server response to a mod delete post mutation.
  (fn-traced [db [_ {:keys [form restore response]}]]
    (let [errors (:errors response)
          db' (if (empty? errors)
                (hide-mod-delete-post-form db)
                (-> db
                    (errors/assoc-errors {:event (:id form)
                                          :errors errors})
                    restore))]
      (forms/transition-after-response db' (:id form)))))

(re-frame/reg-event-db ::toggle-mod-undelete-post-form
  ;; :doc Toggle the display of the moderator undelete post reason form.
  (fn-traced [db [_ _]]
    (-> db
        (mod-forms/initialize-action-form :undelete-post)
        (update-in [:ui-state :post :show-mod-undelete-post-form?] not))))

(defn- hide-mod-undelete-post-form
  "Hide the moderator undelete post reason form."
  [db]
  (assoc-in db [:ui-state :post :show-mod-undelete-post-form?] false))

(re-frame/reg-sub ::show-mod-undelete-post-form?
  ;; :doc Extract whether the moderator undelete post reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-mod-undelete-post-form?])))

(re-frame/reg-event-fx ::mod-undelete-post
  ;; :doc Send a mod post undelete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [pid reason]} form
          status (:status (single-post db))
          state {:form form
                 :restore #(assoc-post % pid :status status)}]
      {:db (assoc-post db pid :status :ACTIVE)
       :fx [[:dispatch
             [::util/ajax-csrf-form-post
              {:url (route-util/url-for db :do/undelete-post)
               :payload {:post pid
                         :reason reason}
               :success-event
               [::receive-mod-undelete-post-response]
               :error-event
               [::receive-mod-undelete-post-error-response]}]]]})))

(re-frame/reg-event-db ::receive-mod-undelete-post-response
  ;; :doc Handle a server response to a mod undelete post request.
  (fn-traced [db [_ {:keys [form]}]]
    (-> db
        (forms/transition-after-response (:id form))
        hide-mod-undelete-post-form)))

(re-frame/reg-event-db ::receive-mod-undelete-post-error-response
  ;; :doc Handle an error response to a mod undelete post request.
  (fn-traced [db [_ {:keys [form restore error-response]}]]
    (let [errors (errors/errors-from-ajax-error-response error-response)]
      (-> db
          restore
          (errors/assoc-errors {:event (:id form)
                                :errors errors})
          (forms/transition-after-response (:id form))))))

(re-frame/reg-event-fx ::toggle-post-sticky
  ;; :doc Stick or unstick a post.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid sticky]} (single-post db)
          restore #(assoc-post % :pid :sticky sticky)]
      {:db (assoc-post db pid :sticky (not sticky))
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-sticky :post pid)
               {:post pid}
               [::receive-toggle-sticky-response restore]
               [::errors/receive-ajax-error
                ::toggle-post-sticky restore]]]]]})))

(defn- describe-toggle-sticky-error
  "Pass through an already translated error message from Python."
  [{:keys [message]}]
  {:msg message})

(re-frame/reg-event-db ::receive-toggle-sticky-response
  ;; :doc Handle a response to a sticky toggle request.
  (fn-traced [db [event restore {:keys [status error]}]]
    (if (= "ok" status)
      db
      (-> db
          restore
          (errors/assoc-errors
           {:event event
            :errors (errors/errors-from-do-api error)
            :describe-error-fn describe-toggle-sticky-error})))))

(re-frame/reg-event-fx ::set-announcement-post
  ;; :doc Set the current post to be the site-wide announcement.
  (fn-traced [{:keys [db]} [_ _]]
    (let [pid (get-in db [:view :options :pid])
          former (get-in db [:content :announcement-pid])
          restore #(assoc-in % [:content :announcement-pid] former)]
      {:db (assoc-in db [:content :announcement-pid] pid)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/make-announcement)
               {:post pid}
               nil
               [::errors/receive-ajax-error
                ::set-announcement-post restore]]]]]})))

(re-frame/reg-event-fx ::change-sticky-post-sort
  ;; :doc Send a request to set the post's default comment sort.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid default-sort] :as post} (single-post db)
          sort (next-sticky-post-sort post)
          restore #(assoc-post db pid :default-sort default-sort)]
      {:db (assoc-post db pid :default-sort sort)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-sort :post pid)
               {}
               [::receive-change-sticky-post-sort-response pid]
               [::errors/receive-ajax-error
                ::change-sticky-post-sort restore]]]]]})))

(re-frame/reg-event-fx ::receive-change-sticky-post-sort-response
  ;; :doc Receive the response to the comment sort change.
  ;; :doc Reload the post to get resorted comments.
  (fn-traced [{:keys [db]} [_ pid _]]
    (let [sub (get-in db [:view :options :sub])]
      {:fx [[:dispatch [::post/load-single-post {:sub sub
                                                 :pid pid}]]]})))

(re-frame/reg-event-fx ::toggle-post-comments-locked
  ;; :doc Send request to the server to toggle locking of comments.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid locked]} (single-post db)
          restore #(assoc-post db pid :locked locked)]
      {:db (assoc-post db pid :locked (not locked))
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/toggle-lock-comments :post pid)
               {}
               nil
               [::errors/receive-ajax-error
                ::toggle-post-comments-locked restore]]]]]})))

(re-frame/reg-event-fx ::edit-post-author-distinguish
  ;; :doc Send a post author distinguish request to the server.
  ;; :doc If distinguish is set, it will be cleared, otherwise
  ;; :doc it will be set according to the user's mod or admin status.
  ;; :doc If the user is both mod and admin, supply
  ;; :doc `distinguish-as` with the user's choice.
  (fn-traced [{:keys [db]} [event distinguish-as]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          is-mod? (internal/is-mod-of-sub? db)
          distinguish (:distinguish (single-post db))
          new-distinguish (cond
                            (some? distinguish) nil
                            (= :admin distinguish-as) :ADMIN
                            (= :mod distinguish-as) :MOD
                            is-mod? :MOD
                            :else :ADMIN)
          restore #(assoc-post % pid :distinguish distinguish)]
      {:db (assoc-post db pid :distinguish new-distinguish)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :distinguish-post
                         :id (str :distinguish-post pid)
                         :variables {:pid pid
                                     :sid (internal/sid-from-name db sub)
                                     :distinguish new-distinguish}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))
