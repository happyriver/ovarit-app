;; fe/content/modmail.cljs -- Modmail content for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.modmail
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.message :as message]
   [ovarit.app.fe.errors :refer [assoc-errors] :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

;; Import graphql queries.
(defgraphql graphql "graphql/modmail.graphql")

;; Helper functions
(defn- precalc-message-attributes
  "Precalculate message attributes needed by the view functions."
  [{:keys [mtype sender receiver unread] :as message} sub]
  (let [sid (:sid sub)
        get-sids-modded (fn [submods]
                          (map #(get-in % [:sub :sid]) submods))
        sender-subs (get-sids-modded (:subs-moderated sender))
        receiver-subs (get-sids-modded (:subs-moderated receiver))]
    (-> message
        (assoc :sender-is-mod? (some? ((set sender-subs) sid))
               :receiver-is-mod? (some? ((set receiver-subs) sid))
               :sent-with-name-hidden? (some? (#{:MOD_TO_USER_AS_MOD
                                                 :USER_NOTIFICATION}
                                               mtype))
               :unread? unread)
        (dissoc :unread))))

(defn- prepare-message
  "Clean up a message received from a query."
  [message sub thread-id]
  (-> message
      (update :mtype keyword)
      (update :time util/to-ISO-time)
      (update :sender :uid)
      (update :receiver :uid)
      (assoc :thread-id thread-id)
      (precalc-message-attributes sub)))

(defn- prepare-log-entry
  [{:keys [user] :as entry} thread-id]
  (-> entry
      (update :action keyword)
      (update :mailbox keyword)
      (assoc :uid (:uid user)
             :thread-id thread-id)
      (update :time util/to-ISO-time)
      (update :rtype keyword)
      (update :notification-type keyword)
      (update :reference-type keyword)
      (dissoc :user)))

(defn- prepare-thread
  "Clean up a message thread received from a query."
  [{:keys [sub first-message] :as thread}]
  (-> thread
      (update :mailbox keyword)
      (set/rename-keys {:id :thread-id})
      (assoc :first-mid (:mid first-message)
             :sid (:sid sub))
      (dissoc :sub :first-message :latest-message :log)))

(defn linkable-to-report?
  "Determine if a composed message is linked to a report."
  [form {:keys [user sub report-type]}]
  (and (some? report-type)
       (= (:username form) user)
       (= (:sub form) sub)
       (:send-to-user? form)))

(re-frame/reg-sub ::linkable-to-report?
  ;; :doc Determine if the form is still linkable to the report, if any.
  :<- [::forms/form ::compose-form]
  :<- [:view/options]
  (fn [[form options] _]
    (linkable-to-report? form options)))

(re-frame/reg-event-fx ::send-modmail
  ;; :doc Send a modmail, creating a new thread.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [options (get-in db [:view :options])
          {:keys [sub send-to-user? show-username? username
                  subject content report-type report-id]} form
          linked-report? (linkable-to-report? form options)
          args (merge {:subject subject
                       :content content
                       :sid (internal/sid-from-name db sub)}
                      (when send-to-user?
                        {:username username
                         :show_mod_username show-username?})
                      (when linked-report?
                        {:report_type (str/upper-case report-type)
                         :report_id report-id}))]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :create-modmail-message
                         :variables args
                         :handler [::message/compose-response
                                   {:form form}]}]]]})))

(re-frame/reg-event-fx ::send-modmail-reply
  ;; :doc Send a reply to a modmail message.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [mailbox (-> db :view :options :mailbox)
          {:keys [thread-id send-to-user? show-username? content]} form
          args (merge {:thread_id thread-id
                       :content content
                       :send_to_user send-to-user?}
                      (when send-to-user?
                        {:show_mod_username show-username?}))]
      {:fx [(if (and (= :notifications mailbox) send-to-user?)
              [:dispatch [::send-linked-modmail form]]
              [:dispatch [::graphql/mutate
                          {:graphql graphql
                           :name :create-modmail-reply
                           :variables args
                           :handler [::add-reply-to-thread
                                     {:form form}]}]])]})))

(re-frame/reg-event-fx ::send-linked-modmail
  ;; :doc Send a modmail, creating a new thread linked to the current thread.
  (fn-traced [{:keys [db]} [_ form]]
    (let [{:keys [sub show-username? username subject content thread-id]} form
          args {:subject subject
                :content content
                :sid (internal/sid-from-name db sub)
                :username username
                :show_mod_username show-username?
                :reference_thread_id thread-id}]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :create-modmail-message
                         :variables args
                         :handler [::compose-linked-thread-response
                                   {:form form}]}]]]})))

(re-frame/reg-event-db ::compose-linked-thread-response
  ;; :doc Process the server's response to starting a new linked thread.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          new-thread-id (-> data :create-modmail-message :thread-id)]
      (cond-> db
        errors (errors/assoc-errors
                {:event (:id form)
                 :errors errors
                 :describe-error-fn #(message/describe-error %)})
        new-thread-id (update :reldb rel/transact
                              [:insert :SubMessageLog
                               {:action :RELATED_THREAD
                                :uid (-> db :current-user :uid)
                                :time (util/to-ISO-time (:now db))
                                :thread-id (:thread-id form)
                                :related-thread-id new-thread-id
                                :reference-type :NEW}])
        true (forms/transition-after-response (:id form))))))

(re-frame/reg-event-db ::add-reply-to-thread
  ;; :doc Handle the server response from a mod mail reply.
  ;; :doc If the reply was successful, add the message to the thread.
  ;; :doc The reply count will be out of date but it's not shown in
  ;; :doc thread view.  Pass the response along to the form's response
  ;; :doc handler for the display of error messages, if any, and updates
  ;; :doc to the form state.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          {:keys [id sub thread-id]} form
          sub (rel/q (:reldb db) [[:from :SubName] [:where [= :name sub]]])
          message (when-let [reply (:create-modmail-reply data)]
                    (prepare-message reply sub thread-id))]
      (-> (if (or errors (nil? message))
            (errors/assoc-errors db {:event ::message/compose-response
                                     :errors errors
                                     :describe-error-fn
                                     #(message/describe-error %)})
            (update db :reldb rel/transact [:insert :Message message]))
          (forms/transition-after-response id)))))

(defn sid-list
  "Return a list of sids that should be fetched, from the modmail sub option,
  which should be either :all-subs or a sub name."
  [db sub]
  (let [sid (when (not= sub :all-subs) (internal/sid-from-name db sub))
        user-subs (set (internal/subs-moderated-names db))
        is-admin? (get-in db [:current-user :attributes :is-admin])]
    (cond
      (= sub :all-subs) (internal/subs-moderated-sids db)
      (and sid (or is-admin? (user-subs sub))) [sid]
      :else nil)))

(re-frame/reg-event-fx ::load-modmails
  ;; :doc Fetch modmails from the server, from the thread identified by id.
  (fn-traced [{:keys [db]}
              [_ {:keys [thread-id sub cursor] :as options} on-load]]
    (let [page-size (get-in db [:settings :page-size])
          sids (sid-list db sub)
          args (merge {:first page-size
                       :thread_id thread-id}
                      (when cursor
                        {:after cursor}))]
      {:db (assoc-in db [:status :content] :loading)
       :fx [(if (seq sids)
              [:dispatch [::graphql/query
                          {:graphql graphql
                           :name :get-modmail-thread-by-id
                           :id (str :get-modmail-thread-by-id thread-id)
                           :variables args
                           :handler [::receive-threaded-modmails
                                     {:options options
                                      :on-load on-load}]}]]
              [:dispatch on-load])]})))

(defn- fixup-user
  [user]
  (-> user
      (dissoc :subs-moderated)
      (update :status keyword)))

(defn- extract-users
  [edges]
  (->> edges
       (map (comp (juxt :sender :receiver) :node))
       (apply concat)
       (remove nil?)
       (map fixup-user)
       set))

(re-frame/reg-event-fx ::receive-threaded-modmails
  ;; :doc Receive a modmail thread with messages from the server.
  (fn-traced [{:keys [db]} [event {:keys [options on-load response]}]]
    (let [{:keys [cursor thread-id]} options
          {:keys [data errors]} response
          page-info (get-in data [:modmail-thread :page-info])
          {:keys [sub first-message]} (:message-thread-by-id data)
          thread (prepare-thread (:message-thread-by-id data))
          edges (-> data :modmail-thread :edges)
          users (set/union (extract-users edges)
                           (->> data :message-thread-by-id :log
                                (map (comp fixup-user :user))
                                set))
          messages (conj
                    (map #(prepare-message (:node %) sub thread-id) edges)
                    (prepare-message first-message sub thread-id))
          logs (->> data :message-thread-by-id :log
                    (map #(prepare-log-entry % thread-id)))
          refresh? (not= cursor (-> db :content :modmail :thread :cursor))]
      {:db (-> db
               (assoc-errors {:event event :errors errors})
               (assoc-in [:status :content] :loaded)
               (cond-> refresh?
                 (update :reldb rel/transact [:delete :Message
                                              [= :thread-id thread-id]]))
               (cond-> (nil? errors)
                 (->
                  (assoc-in [:content :modmail :pagination]
                            {:more-items-available? (:has-next-page page-info)
                             :cursor (:end-cursor page-info)})
                  (update :reldb rel/transact
                          [:insert-or-replace :MessageThread thread]
                          [:insert-or-replace :SubName sub]
                          (into [:insert-or-replace :User] users)
                          (into [:insert-or-replace :Message] messages)
                          [:delete :SubMessageLog [= :thread-id thread-id]]
                          (into [:insert :SubMessageLog] logs)))))
       :fx [(when on-load
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::load-modmail-category
  ;; :doc Fetch a modmail thread category from the server.
  (fn-traced [{:keys [db]} [_ {:keys [sub mailbox cursor] :as options}
                            on-load]]
    (let [page-size (get-in db [:settings :page-size])
          sids (sid-list db sub)
          base-args {:first page-size
                     :sids sids
                     :category (case mailbox
                                 :all :ALL
                                 :new :NEW
                                 :in-progress :IN_PROGRESS
                                 :archived :ARCHIVED
                                 :discussions :MOD_DISCUSSIONS
                                 :notifications :MOD_NOTIFICATIONS)
                     :unread_only false}
          args (if cursor
                 (assoc base-args :after cursor)
                 base-args)]
      {:db (assoc-in db [:status :content] :loading)
       :fx [(if (seq sids)
              [:dispatch [::graphql/query
                          {:graphql graphql
                           :name :get-modmail-category
                           :variables args
                           :handler [::receive-modmail-category
                                     {:options options
                                      :on-load on-load}]}]]
              [:dispatch on-load])]})))

(defn- extract-category-users
  [edges]
  (->> edges
       (map (juxt :first-message :latest-message))
       (apply concat)
       (map (juxt :sender :receiver))
       (apply concat)
       (remove nil?)
       (map #(-> %
                 (dissoc :subs-moderated)
                 (update :status keyword)))
       set))

(defn- extract-first-and-last-message
  [{:keys [first-message latest-message sub id]}]
  [(prepare-message first-message sub id)
   (prepare-message latest-message sub id)])

(re-frame/reg-event-fx ::receive-modmail-category
  ;; :doc Receive messages from a modmail mailbox from the server.
  (fn-traced [{:keys [db]} [event {:keys [options on-load response]}]]
    (let [{:keys [cursor]} options
          {:keys [data errors]} response
          page-info (get-in data [:modmail-threads :page-info])
          nodes (->> (get-in data [:modmail-threads :edges])
                     (map :node))
          users (extract-category-users nodes)
          messages (->> nodes
                        (map extract-first-and-last-message)
                        (apply concat)
                        set)
          threads (map #(prepare-thread %) nodes)
          path [:content :modmail :pagination]
          refresh? (not= cursor (get-in db (conj path :cursor)))]
      {:db (-> db
               (assoc-errors {:event event :errors errors})
               (assoc-in [:status :content] :loaded)
               (cond-> refresh?
                 (update :reldb rel/transact
                         [:delete :MessageThread]))
               (cond-> (nil? errors)
                 (->
                  (update :reldb rel/transact
                          (into [:insert-or-replace :User] users)
                          (into [:insert-or-replace :MessageThread] threads)
                          (into [:insert-or-replace :Message] messages))
                  (assoc-in path
                            {:more-items-available? (:has-next-page page-info)
                             :cursor (:end-cursor page-info)}))))
       :fx [(when on-load
              [:dispatch on-load])]})))

(re-frame/reg-event-fx ::set-modmail-sub-selection
  ;; :doc Select a sub for the modmail mailboxes view.
  (fn-traced [{:keys [db]} [_ value]]
    (let [mailbox (get-in db [:view :options :mailbox])
          route [:modmail/mailbox :mailbox mailbox]
          route-args (if (= value :all-subs)
                       route
                       (into route [:query-args {:sub value}]))
          url (apply (partial route-util/url-for db) route-args)]
      {:fx [[::routes/set-url url]]})))

(re-frame/reg-sub ::modmail-sub-selection
  ;; :doc Extract the sub for the modmail mailboxes view.
  (fn [db _]
    (or (get-in db [:view :options :sub]) :all-subs)))

(re-frame/reg-sub ::modmail-notification-counts
  ;; :doc Extract subs with unread counts from the db.
  :<- [::internal/reldb]
  :<- [::modmail-sub-selection]
  (fn [[reldb sub] _]
    (rel/q reldb (if (= sub :all-subs)
                   [[:from :SubModNotificationCount]]
                   [[:from :SubModNotificationCount]
                    [:join :SubName {:sid :sid}]
                    [:where [= :name sub]]]))))

(re-frame/reg-sub ::summed-unread-counts
  ;; :doc Add up unread counts by mailbox.
  :<- [::modmail-notification-counts]
  (fn [counts _]
    (let [sum (fnil + 0)]
      (reduce
       (fn [sums {:keys [new-unread-modmail-count
                         in-progress-unread-modmail-count
                         all-unread-modmail-count
                         discussion-unread-modmail-count
                         notification-unread-modmail-count]}]
         (-> sums
             (update :new           sum new-unread-modmail-count)
             (update :in-progress   sum in-progress-unread-modmail-count)
             (update :all           sum all-unread-modmail-count)
             (update :discussions   sum discussion-unread-modmail-count)
             (update :notifications sum notification-unread-modmail-count)))
       {} counts))))

(re-frame/reg-sub ::unread-by-mailbox
  ;; :doc Get the count of unread messages for a mailbox.
  :<- [::summed-unread-counts]
  (fn [sums [_ mailbox]]
    (get sums mailbox)))

(re-frame/reg-sub ::modmail-mailbox
  ;; :doc Extract threads for current category and sub selection.
  ;; :doc Extend each thread with most recent message in the thread.
  :<- [::internal/reldb]
  :<- [::modmail-sub-selection]
  (fn [[reldb sub] _]
    (rel/q reldb (->> [[:from :MessageThread]
                       [:join :Message {:thread-id :thread-id}]
                       [:agg [:thread-id] [:latest [rel/top 1 :time]]]
                       [:extend [:latest-time [first :latest]]]
                       [:without :latest]
                       [:join :MessageThread {:thread-id :thread-id}]
                       [:join :SubName {:sid :sid}]
                       [:rename {:name :sub-name}]
                       (when-not (= sub :all-subs)
                         [:where [= :sub-name sub]])
                       [:join :Message {:latest-time :time}]
                       [:left-join :User {:sender :uid}]
                       [:rename {:name :sender-name}]
                       [:left-join :User {:receiver :uid}]
                       [:rename {:name :receiver-name}]
                       [:without :uid :status]
                       [:sort [:latest-time :desc]]]
                      (remove nil?)
                      vec))))

(re-frame/reg-sub ::supervising-admin?
  ;; :doc Determine whether an admin is supervising.
  ;; :doc (Looking at messages for a sub they don't mod.)
  :<- [::modmail-sub-selection]
  :<- [::user/subs-moderated-names]
  :<- [::user/is-admin?]
  (fn [[sub sub-names is-admin?] _]
    (and
     is-admin?
     (not (= sub :all-subs))
     (not ((set sub-names) sub)))))

(defn get-sid-class
  "Return a CSS class name for consistent colorization of subs."
  [sid]
  ;; Use the first character of the sid to pick a color.
  (get ["fill-dark-red" "fill-red" "fill-light-red" "fill-orange"
        "fill-gold" "fill-yellow" "fill-light-green" "fill-green"
        "fill-dark-green" "fill-light-teal" "fill-light-blue"
        "fill-dark-blue" "fill-blue" "fill-light-purple" "fill-purple"
        "fill-dark-pink"]
       ;; If sids are ever not hex, the max 0 protects against
       ;; ##NaN.
       (max 0 (js/parseInt (first sid) 16))))

(defn- add-ui-info
  "Add HTML content to a message."
  [{:keys [mailbox content] :as message}]
  (assoc message
         :archived? (= mailbox :ARCHIVED)
         :html-content (util/markdown-to-html content)))

(defn- add-thread-ui-info
  "Precalculate values useful for displaying a modmail thread."
  [{:keys [sid] :as thread}]
  (-> thread
      (assoc :sub-color-class (get-sid-class sid))
      add-ui-info))

(re-frame/reg-sub ::modmail-mailbox-threads
  ;; :doc Provide list of threads in a mailbox with sub colors.
  :<- [::modmail-mailbox]
  (fn [threads _]
    (map add-thread-ui-info threads)))

(re-frame/reg-sub ::modmail-pagination
  ;; :doc Extract pagination info on the content currently being viewed.
  (fn [db _]
    (get-in db [:content :modmail :pagination])))

(re-frame/reg-sub ::modmail-thread-raw-info
  ;; :doc Extract the currently viewed modmail thread.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [thread-id]}] _]
    (rel/row reldb [[:from :MessageThread]
                    [:where [= :thread-id thread-id]]
                    [:join :SubName {:sid :sid}]
                    [:rename {:name :sub-name}]])))

(re-frame/reg-sub ::modmail-thread-info
  ;; :doc Produce the currently viewed modmail thread.
  ;; :doc Includes calculated values for UI to use.
  :<- [::modmail-thread-raw-info]
  (fn [{:keys [sid mailbox] :as thread} _]
    (-> thread
        (assoc :sub-color-class (get-sid-class sid)
               :archived? (= mailbox :ARCHIVED)))))

(re-frame/reg-sub ::modmail-thread-all-raw-messages
  ;; :doc Extract the messages in the current thread.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [thread-id]}]]
    (rel/q reldb [[:from :Message]
                  [:where [= :thread-id thread-id]]
                  [:left-join :User {:sender :uid}]
                  [:rename {:name :sender-name}]
                  [:left-join :User {:receiver :uid}]
                  [:rename {:name :receiver-name}]
                  [:without :uid :status]
                  [:sort [:time :asc]]])))

(re-frame/reg-sub ::modmail-thread-raw-parent
  ;; :doc Extract the original message of the current thread.
  :<- [::modmail-thread-info]
  :<- [::modmail-thread-all-raw-messages]
  (fn [[{:keys [first-mid]} messages] _]
    (->> messages
         (filter #(= first-mid (:mid %)))
         first)))

(re-frame/reg-sub ::modmail-thread-raw-messages
  ;; :doc Extract the reply messages in the current thread.
  :<- [::modmail-thread-info]
  :<- [::modmail-thread-all-raw-messages]
  (fn [[{:keys [first-mid]} messages] _]
    (filter #(not= first-mid (:mid %)) messages)))

(re-frame/reg-sub ::modmail-thread-logs
  ;; :doc Extract the logs from the current thread
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [thread-id]}] _]
    (rel/q reldb [[:from :SubMessageLog]
                  [:where [= :thread-id thread-id]]
                  [:left-join :User {:uid :uid}]
                  [:without :uid :status]
                  [:sort [:time :asc]]])))

(re-frame/reg-sub ::modmail-linked-item
  ;; :doc Supply the linkage log record, if any.
  :<- [::modmail-thread-logs]
  (fn [logs _]
    (let [{:keys [action reference-type] :as first-log} (first logs)]
      (when (or (= :RELATED_REPORT action)
                (and (= :RELATED_THREAD action)
                     (= :NOTIFICATION reference-type)))
        first-log))))

(re-frame/reg-sub ::modmail-thread
  ;; :doc Provide messages and log entries in thread in time order.
  :<- [::modmail-thread-raw-messages]
  :<- [::modmail-thread-logs]
  (fn [[messages logs] _]
    (let [messages (->> messages
                        (map add-ui-info)
                        (map #(assoc % :type :message)))
          logs (map #(assoc % :type :log) logs)]
      (->> (concat messages logs)
           (sort-by (comp util/to-epoch-time :time))))))

(re-frame/reg-sub ::modmail-thread-parent
  ;; :doc Provide the parent message of the thread with sub colors.
  :<- [::modmail-thread-raw-parent]
  (fn [parent _]
    (add-ui-info parent)))

(re-frame/reg-sub ::modmail-thread-any-unread
  ;; :doc Determine whether any messages in the thread are unread.
  :<- [::modmail-thread-raw-parent]
  :<- [::modmail-thread-raw-messages]
  (fn [[parent messages] _]
    (or (:unread? parent)
        (some :unread? messages))))

(re-frame/reg-event-fx ::set-unread
  ;; :doc Set the unread flag on a message by message id.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ mid value]]
    {:db (update db :reldb rel/transact
                 [:update :Message {:unread? value}
                  [= :mid mid]])
     :fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :update-message-unread
                       :variables {:mid mid :unread value}
                       :handler [::handle-message-unread-update
                                 {:event ::set-unread}]}]]]}))

(re-frame/reg-event-db ::handle-message-unread-update
  ;; :doc Handle the response from updating unread or mailbox on the server.
  (fn-traced [db [_ {:keys [event response]}]]
    (assoc-errors db {:event event :errors (:errors response)})))

(re-frame/reg-event-fx ::set-thread-unread
  ;; :doc Set the unread flag on a message thread by thread id.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ thread-id value]]
    {:db (update db :reldb rel/transact
                 [:update :Message {:unread? value}
                  [= :thread-id thread-id]])
     :fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :update-thread-unread
                       :variables {:thread_id thread-id :unread value}
                       :handler [::handle-message-update
                                 {:event ::set-unread}]}]]]}))

(re-frame/reg-event-fx ::set-archived
  ;; :doc Archive or unarchive a thread.
  ;; :doc Optimistically update the db and then tell the server.
  (fn-traced [{:keys [db]} [_ thread-id archived?]]
    (let [value (if archived? :ARCHIVED :INBOX)]
      {:db (update db :reldb rel/transact
                   [:update :MessageThread {:mailbox [:_ value]}
                    [= :thread-id thread-id]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :update-modmail-mailbox
                         :variables {:thread_id thread-id
                                     :mailbox value}
                         :handler [::handle-message-update
                                   {:event ::set-archived}]}]]]})))

(re-frame/reg-event-db ::handle-message-update
  ;; :doc Handle the response from updating unread or mailbox on the server.
  (fn-traced [db [_ {:keys [event response]}]]
    (assoc-errors db {:event event :errors (:errors response)})))
