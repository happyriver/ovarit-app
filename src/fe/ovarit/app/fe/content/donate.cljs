;; fe/content/donate.cljs -- Donation content for ovarit.app.fe
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.donate
  (:require
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :as graphql :refer-macros [defgraphql]]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :as util]
   [re-frame.core :as re-frame]))

;; Import graphql queries.
(defgraphql graphql "graphql/donate.graphql")

(def usd-amount-regex #"\$?([1-9]+[0-9]*)(.00)?")

(defn parse-amount
  "Parse an currency amount in USD entered by the user.
  Return zero for anything that can't be parsed, otherwise the amount
  in dollars.  Ignore any commas."
  [custom]
  (let [sans-commas (apply str (filter #(not= % \,) custom))
        m (re-matches usd-amount-regex sans-commas)]
    (if m
      (let [[_ dollars] m]
        (js/parseInt dollars))
      0)))

(defn format-usd
  "Format an integer representing pennies into USD, and return a map
  containing both the original and formatted values."
  [dollars]
  {:text (str "$" dollars)
   :value dollars})

(re-frame/reg-event-fx ::continue-to-checkout
  ;; :doc Ask the server for a payment intent.
  ;; :doc On success redirect to the checkout page.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [selection custom-amount recurring?]} form
          amount (if (= selection :custom)
                   (parse-amount custom-amount)
                   selection)
          existing-sub (get-in db [:current-user :donate-subscription])
          is-subscriber? (and existing-sub (nil? (:end existing-sub)))]
      {:fx [[:dispatch [::util/ajax-csrf-form-post
                        {:url "/subscription"
                         :payload {:amount amount
                                   :sub (and recurring?
                                             (not is-subscriber?))}
                         :success-event [::on-checkout]
                         :error-event [::on-checkout-error
                                       {:form form}]}]]]})))

(re-frame/reg-event-fx ::on-checkout
  ;; :doc Process a successful response from the subscription post.
  (fn-traced [_ [_ {:keys [response]}]]
    {:fx [[::routes/redirect (:url response)]]}))

(re-frame/reg-event-db ::on-checkout-error
  ;; :doc Process an unsuccessful response from the subscription.
  (fn-traced [db [_ {:keys [form error-response]}]]
    (let [errors (errors/errors-from-ajax-error-response error-response)]
      (-> db
          (errors/assoc-errors {:event (:id form) :errors errors})
          (forms/transition-after-response (:id form))))))

(re-frame/reg-sub ::presets
  ;; :doc Extract the list of configured preset amounts.
  (fn [db _]
    (get-in db [:settings :donate-presets])))

(re-frame/reg-sub ::formatted-preset-amounts
  ;; :doc Compute the formatted preset amounts.
  :<- [::presets]
  (fn [presets _]
    (map format-usd presets)))

(re-frame/reg-sub ::intent
  ;; :doc Extract the user's donation intent.
  (fn [db _]
    (get-in db [:content :donation-intent])))

(re-frame/reg-sub ::intent-amount
  ;; :doc Extract the amount chosen by the user.
  :<- [::intent]
  (fn [intent _]
    (:amount intent)))

(re-frame/reg-sub ::formatted-intent-amount
  ;; :doc Format the amount chosen by the user in USD.
  :<- [::intent-amount]
  (fn [amount _]
    (:text (format-usd amount))))

(re-frame/reg-sub ::intent-recurring?
  ;; :doc Extract whether the user has chosen to subscribe, vs a one-time donation.
  :<- [::intent]
  (fn [intent _]
    (:recurring? intent)))

(util/reg-ajax-event-chain ::load-session
  ;; :doc Load information about a Stripe session from its session id.
  {:call
   (fn-traced [{:keys [db]} [_ session-id]]
     (if (and session-id (seq session-id))
       {:db (update db :status assoc :content :loading :payments :loading)
        :fx [[::util/ajax-get {:url (str "/subscription/checkout?session_id="
                                         session-id)}]]}
       (do
         (log/error nil "Missing session id")
         nil)))

   :success
   (fn-traced [{:keys [db]} [_ {:keys [response]}]]
     {:db (-> db
              (assoc-in [:content :completed-donation-session] response)
              (update :status assoc :content :loaded :payments :loaded))})

   :failure
   (fn-traced [{:keys [db]} [_ {:keys [error-response]}]]
     {:db
      (-> db
          (errors/assoc-errors {:event ::load-session
                                :errors (errors/errors-from-ajax-error-response
                                         error-response)})
          (update-in [:content] dissoc :completed-donation-session)
          (update :status assoc :content :loaded :payments :error))})})

(re-frame/reg-sub ::completed-session
  ;; :doc Extract the donation session.
  (fn [db _]
    (get-in db [:content :completed-donation-session])))

(re-frame/reg-sub ::formatted-completed-amount
  ;; :doc Format the amount chosen by the user in USD.
  :<- [::completed-session]
  (fn [session _]
    (:text (format-usd (:amount session)))))

(re-frame/reg-sub ::completed-recurring?
  ;; :doc Extract whether the user chose to subscribe, vs a one-time donation.
  :<- [::completed-session]
  (fn [session _]
    (:sub session)))

(util/reg-ajax-event-chain ::load-subscription
  ;; :doc Find out whether a user has a existing donation subscription.
  {:call
   (fn-traced [_ [_ _]]
     {:fx [[::util/ajax-get {:url "/subscription"}]]})

   :success
   (fn-traced [{:keys [db]} [_ {:keys [response]}]]
     {:db (-> db
              (assoc-in [:current-user :donate-subscription] response)
              (assoc-in [:status :payments] :loading)
              (assoc-in [:status :content] :loaded))})

   :failure
   ;; We get an error if the user doesn't have a subscription, in
   ;; which case just clear whatever subscription info was in the db.
   (fn-traced [{:keys [db]} [event {:keys [error-response]}]]
     {:db
      (let [{:keys [status]} error-response
            no-subscription? (= status 428)
            error? (not no-subscription?)]
        (cond-> (-> db
                    (assoc-in [:status :content] :loaded)
                    (assoc-in [:status :payments] (if error? :error :loaded))
                    (update-in [:current-user] dissoc :donate-subscription))
          error? (errors/assoc-errors
                  {:event event
                   :errors (errors/errors-from-ajax-error-response
                            error-response)})))})})

(re-frame/reg-sub ::configured-funding-goal
  ;; :doc Extract the funding goal.
  (fn [db _]
    (get-in db [:settings :funding-goal])))

(re-frame/reg-event-fx ::subscribe-funding-progress
  ;; :doc Subscribe to updates on funding progress.
  (fn-traced [{:keys [db]} _]
    (let [is-authenticated? (get-in db [:current-user :uid])]
      {:db (update-in db [:subscriptions :stop-on-routing] conj
                      :funding-progress)
       :fx [(when is-authenticated?
              ;; In case this is happening at initial load, allow time
              ;; for websocket initialization to complete first.
              [:dispatch-later
               {:ms 200
                :dispatch [::graphql/subscribe
                           {:graphql graphql
                            :name :funding-progress
                            :variables {}
                            :handler [::receive-funding-progress]}]}])]})))

(re-frame/reg-event-db ::receive-funding-progress
  ;; :doc Receive a funding progress update.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (update-in [:content :funding-progress]
                     #(or (:funding-progress data) %))))))

(re-frame/reg-sub ::funding-progress
  ;; :doc Extract the funding progress.
  (fn [db _]
    (get-in db [:content :funding-progress])))

(re-frame/reg-sub ::show-funding-progress?
  ;; :doc Determine whether the funding info is available.
  :<- [::configured-funding-goal]
  :<- [::user/is-authenticated?]
  (fn [[funding-goal is-authenticated?] _]
    (and is-authenticated?
         (some? funding-goal)
         (pos? funding-goal))))

(def dollars
  "A currency formatter for US dollars."
  (js/Intl.NumberFormat. "en-US" #js {:currency "USD"
                                      :style "currency"}))

(defn- dollar-format
  "Format a numeric value as US dollars, without cents."
  [val]
  (when val
    (-> (.format dollars val)
        (str/split #"\.")
        first)))

(re-frame/reg-sub ::funding-goal
  ;; :doc Format the funding goal.
  :<- [::configured-funding-goal]
  (fn [goal _]
    (dollar-format goal)))

(re-frame/reg-sub ::funds-raised
  ;; :doc Format the amount raised.
  :<- [::funding-progress]
  (fn [progress _]
    (dollar-format progress)))

(re-frame/reg-sub ::funding-completion-percentage
  ;; :doc Calculate and format the percentage completion of the funding goal.
  :<- [::funding-progress]
  :<- [::configured-funding-goal]
  (fn [[paid goal] _]
    (if (and paid goal (pos? goal))
      (-> (/ paid goal)
          (max 0)
          (min 1)
          (* 100)
          (str "%"))
      "0%")))

(re-frame/reg-sub ::payment-server-status
  ;; :doc Extract the status of communications with the payment server.
  (fn [db _]
    (get-in db [:status :payments])))

(re-frame/reg-sub ::donate-subscription
  ;; :doc Extract the user's donation subscription information.
  (fn [db _]
    (get-in db [:current-user :donate-subscription])))

(re-frame/reg-sub ::is-subscriber?
  ;; :doc Determine whether the user has a current active subscription.
  :<- [::donate-subscription]
  (fn [sub _]
    (and (some? sub) (nil? (:end sub)))))

(re-frame/reg-sub ::subscribed-amount
  ;; :doc Extract the amount of the user's existing subscription.
  :<- [::donate-subscription]
  (fn [sub _]
    (:amount sub)))

(re-frame/reg-sub ::formatted-subscribed-amount
  ;; :doc Format the amount chosen by the user in USD.
  :<- [::subscribed-amount]
  (fn [amount _]
    (:text (format-usd amount))))

(re-frame/reg-sub ::subscription-next-date
  ;; :doc Get the date of the next payment in the user's existing subscription.
  ;; :doc Format it according to the browser locale.
  :<- [::donate-subscription]
  (fn [sub _]
    (.toLocaleDateString (js/Date. (:next sub)))))

(re-frame/reg-event-fx ::cancel-subscription
  ;; :doc Delete an existing subscription.
  (fn-traced [_ [_ _]]
    {:fx [[::util/ajax-delete ["/subscription"
                               [::on-cancel-subscription]
                               [::on-cancel-subscription-error]]]]}))

(re-frame/reg-event-fx ::on-cancel-subscription
  ;; :doc Receive acknowledgement from the server of a cancelled subscription.
  (fn-traced [{:keys [db]} [_ payload]]
    {:db (assoc-in db [:current-user :donate-subscription] payload)
     :fx [[::routes/set-url (route-util/url-for db :donate/cancel)]]}))

(re-frame/reg-event-fx ::on-cancel-subscription-error
  ;; :doc Handle an error when cancelling a subscription.
  (fn-traced [{:keys [db]} [event error-response]]
    {:db (errors/assoc-errors db {:event event
                                  :errors (errors/errors-from-ajax-error-response
                                           error-response)})}))
