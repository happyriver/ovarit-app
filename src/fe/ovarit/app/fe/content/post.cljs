;; fe/content/post.cljs -- Single post content for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.post
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.comment :as comment]
   [ovarit.app.fe.content.internal :refer [sub-info-by-name] :as internal]
   [ovarit.app.fe.content.post.db :refer [single-post assoc-post update-post
                                          assoc-post-user-attributes
                                          next-sticky-post-sort
                                          single-post-user-attributes]]
   [ovarit.app.fe.content.post.moderation :as-alias post-mod]
   [ovarit.app.fe.content.settings :as-alias settings]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :as graphql]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.tr :refer [trx] :as tr]
   [ovarit.app.fe.ui :as ui]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.login :as login-form]
   [ovarit.app.fe.ui.forms.post :as post-forms]
   [ovarit.app.fe.ui.forms.report :as report-form]
   [ovarit.app.fe.ui.page-title :as-alias page-title]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as-alias user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame])
  (:import [goog Uri]))

(defn init-single-post
  "Set up the db for loading a single post with comments."
  [db {:keys [sub pid cid target-cid slug sort]}]
  (-> db
      (assoc :view {:active-panel :single-post
                    :options {:sub sub
                              :pid pid
                              :cid cid
                              :scroll-to-cid target-cid
                              :slug slug
                              :sort-choice sort}})
      (assoc-in [:status :content] :loading)
      (assoc-in [:status :single-post] :loading)
      (assoc-in [:status :comment-tree] :loading)
      (assoc-in [:status :more-comments] {})
      (login-form/initialize-login-form (get-in db [:route :url]))
      (update :ui-state dissoc :post :comment :comments
              :comment-content-view :comment-modals)))

(re-frame/reg-event-fx ::load-single-post
  ;; :doc Load a single post with author and up to date sub info.
  (fn-traced [{:keys [db]} [_ {:keys [cid pid]}]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin]))
                            boolean)
          {:keys [num level]} (get-in db [:settings :comment-tree])]
      (log/debug {:pid pid} "Dispatching single post queries")
      {:fx [[:dispatch [::graphql/query
                        {:graphql subs/graphql
                         :name :get-single-post
                         :id (str :get-single-post "-" pid)
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid}
                         :handler [::receive-single-post]}]]
            [:dispatch [::graphql/query
                        {:graphql subs/graphql
                         :name :get-comment-tree
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid :cid cid
                                     :first num :level level
                                     :sort_by nil}
                         :handler [::receive-comment-tree
                                   {:cid cid :pid pid}]}]]]})))

(defn- post-sort
  "Determine the comment sort on the post from the post and view options."
  [db {:keys [best-sort-enabled default-sort]}]
  (let [{:keys [sort-choice]} (get-in db [:view :options])]
    ;; If we have a sort from routing and it is valid, use that.
    (if (and (#{"top" "best" "new"} sort-choice)
             (or best-sort-enabled
                 (not= "best" sort-choice)))
      (-> sort-choice
          str/upper-case
          keyword)
      ;; Otherwise use default for the post.
      default-sort)))

(defn- fixup-user-attributes
  [{:keys [pid user-attributes]}]
  (when user-attributes
    (-> user-attributes
        (update :viewed util/to-ISO-time)
        (update :vote keyword)
        (assoc :pid pid))))

(defn- prepare-post
  "Make post fields more clojure-friendly."
  [{:keys [sub] :as post}]
  (-> post
      (update :status keyword)
      (update :type keyword)
      (update :default-sort keyword)
      (assoc :uid (get-in post [:author :uid]))
      (dissoc :author)
      (update :distinguish keyword)
      (assoc :sid (:sid sub))
      (update :posted util/to-ISO-time)
      (update :edited util/to-ISO-time)
      (update :title-edited util/to-ISO-time)
      (update :poll-closes-time util/to-ISO-time)
      (dissoc :sub :user-attributes :poll-options)))

(defn- post-404?
  [{:keys [sub]} post errors]
  (let [sub-name (get-in post [:sub :name])
        sub-match? (and (:pid post) sub-name (= (str/lower-case sub)
                                                (str/lower-case sub-name)))
        not-found-error? (some #(= "Not found" (:message %)) errors)]
    (or not-found-error? (and sub-name (not sub-match?)))))

(defn- fix-slugs-of-deleted-posts
  [{:keys [status] :as post} db]
  (if (= "ACTIVE" status)
    post
    (assoc post :slug (trx db "deleted"))))

(defn- slug-mismatch? [options post]
  (not= (:slug options) (:slug post)))

(defn post-url
  "Create the url to view a post."
  [db {:keys [sub pid slug cid]}]
  (if cid
    (route-util/url-for db :sub/view-direct-link
                        :sub sub :pid pid :slug slug :cid cid)
    ;; bidi is not getting this one right
    (let [base (route-util/url-for db :sub/view-single-post
                                   :sub sub :pid pid)]
      (if slug
        (str base "/" slug)
        base))))

(defn- add-post-to-db [db {:keys [author author-flair poll-options
                                  pid sub] :as post}
                       options]
  (let [user-attributes (fixup-user-attributes post)
        user (update author :status keyword)
        poll-options (->> poll-options
                          (map #(-> %
                                    (set/rename-keys {:id :option-id})
                                    (assoc :pid pid)))
                          (map-indexed #(assoc %2 :order %1)))
        post (prepare-post post)
        sort-path (if-let [cid (:cid options)]
                    [:direct cid]
                    [(post-sort db post)])]
    (-> db
        (assoc-in [:status :single-post] :loaded)
        (update :reldb rel/transact [:insert-or-replace :Post post])
        (update :reldb rel/transact (into [:insert-or-replace :PollOption]
                                          poll-options))
        (update :reldb rel/transact [:insert-or-replace :SubName
                                     (select-keys sub [:name :sid])])
        (cond-> user-attributes
          (update :reldb rel/transact [:insert-or-replace :PostUserAttributes
                                       user-attributes]))
        (update :reldb rel/transact [:insert-or-replace :User user])
        (assoc-in [:ui-state :post :sort-path] sort-path)
        (subs/update-sub sub)
        (subs/update-sub-user-flair (:sid sub) (:uid user) author-flair))))

(re-frame/reg-event-fx ::receive-single-post
  ;; :doc "Receive the results of the single post query."
  (fn-traced [{:keys [db]} [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          options (get-in db [:view :options])
          post (-> (:post-by-pid data)
                   (fix-slugs-of-deleted-posts db))
          sub (:sub post)
          filtered-errors (filter #(not= "Not found" (:message %)) errors)
          db' (errors/assoc-errors db {:event event
                                       :errors filtered-errors})]
      (log/debug {:post-404 (post-404? options post errors)
                  :post-missing (nil? post)
                  :slug-missing (slug-mismatch? options post)}
                 "Checking single-post url")
      (cond
        ;; Post does not exist or sub does not match.
        (post-404? options post errors)
        {:db db'
         :fx [[:dispatch [::routes/show-error :unmatched-route]]]}

        ;; Error, post not loaded.
        (nil? post)
        {:db db'
         :fx [[:dispatch [::routes/show-error :server-error]]]}

        ;; Slug in URL does not match, so fix it.
        (slug-mismatch? options post)
        {:db db'
         :fx [[::routes/replace-url
               (post-url db (assoc options
                                   :slug (:slug post)
                                   :sub (:name sub)))]]}

        ;; All good.
        :else
        {:db (add-post-to-db db' post options)
         :fx [[:dispatch [::update-page-title]]
              [:dispatch [::update-post-viewed post]]]}))))

(re-frame/reg-sub ::loaded?
  :<- [::window/statuses]
  (fn [statuses _]
    (= :loaded (:single-post statuses))))

(re-frame/reg-event-fx ::update-post-viewed
  ;; :doc Tell the server that we're reading a post.
  (fn-traced [{:keys [db]} [event {:keys [is-archived pid]}]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))]
      {:fx [(when (and authenticated? (not is-archived))
              [:dispatch [::graphql/mutate
                          {:graphql subs/graphql
                           :name :update-post-viewed
                           :id (str :update-post-viewed pid)
                           :variables {:pid pid}
                           :handler [::graphql/receive-mutation-handler
                                     {:event event}]}]])]})))

(defn- make-id [kw pid]
  (str (symbol kw) "-" pid))

(re-frame/reg-event-fx ::subscribe-new-comments
  ;; :doc Subscribe to the stream of new comments on the post.
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::new-comment pid)]
      {:db (update-in db [:subscriptions :stop-on-routing] conj id)
       :fx [[:dispatch [::graphql/subscribe
                        {:graphql subs/graphql
                         :name :new-comment
                         :id id
                         :variables {:pid pid}
                         :handler [::receive-new-comment pid]}]]]})))

(re-frame/reg-event-fx ::subscribe-post-updates
  ;; :doc Subscribe to updates on the post.
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::post-update pid)]
      {:db (update-in db [:subscriptions :stop-on-routing] conj id)
       :fx [[:dispatch [::graphql/subscribe
                        {:graphql subs/graphql
                         :name :post-update
                         :id id
                         :variables {:pid pid}
                         :handler [::receive-post-update {:pid pid}]}]]]})))

(re-frame/reg-event-fx ::subscribe-comment-updates
  (fn-traced [{:keys [db]} [_ pid]]
    (let [id (make-id ::comment-update pid)
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin])))]
      (when mod-or-admin?
        {:db (update-in db [:subscriptions :stop-on-routing] conj id)
         :fx [[:dispatch [::graphql/subscribe
                          {:graphql subs/graphql
                           :name :comment-update
                           :id id
                           :variables {:pid pid :is_mod true}
                           :handler [::comment/receive-comment-update
                                     {:pid pid}]}]]]}))))

(re-frame/reg-event-fx ::start-becoming-visible-watcher
  ;; :doc Ask server for status update when page becomes visible.
  (fn-traced [{:keys [db]} [_ pid]]
    {:db (update-in db [:view :on-leave] conj
                    [::stop-becoming-visible-watcher])
     :window/on-becoming-visible {:id ::watch-visibility
                                  :event [::request-post-update pid]}}))

(re-frame/reg-event-fx ::stop-becoming-visible-watcher
  ;; :doc Stop watching for the page to become visible.
  (fn-traced [_ _]
    {:window/stop-on-scroll {:id ::becoming-visible-watcher}}))

(re-frame/reg-event-fx ::request-post-update
  ;; :doc Request an update of post status from the server.
  (fn-traced [_ [_ pid]]
    {:fx [[:dispatch [::graphql/query
                      {:graphql subs/graphql
                       :name :post-status-update
                       :id (str ::request-post-update pid)
                       :variables {:pid pid}
                       :handler [::receive-post-update {:pid pid}]}]]]}))

(re-frame/reg-event-fx ::subscribe-post
  ;; :doc Subscribe to post object updates and new comments.
  ;; :doc Watch for the page becoming visible and ask for a post
  ;; :doc status update, in case the websocket connection was dropped
  ;; :doc and a subscribed update was missed.
  (fn-traced [_ [_ pid]]
    {:fx [[:dispatch [::subscribe-post-updates pid]]
          [:dispatch [::subscribe-comment-updates pid]]
          [:dispatch [::start-becoming-visible-watcher pid]]
          #_[:dispatch [::subscribe-new-comments pid]]]}))

(re-frame/reg-event-db ::receive-post-update
  ;; :doc Receive a post update from the server and merge it into the post.
  ;; :doc Handle results of both the :post-update subscription and the
  ;; :doc :post-status-update query.
  (fn-traced [db [event {:keys [pid response]}]]
    (let [{:keys [data errors]} response
          update (some-> (or (:post-update data)
                             (:post-by-pid data))
                         (update :status keyword)
                         (set/rename-keys {:locked :locked-update
                                           :status :status-update}))]
      (cond-> db
        errors
        (errors/assoc-errors {:event event :errors errors})

        update
        (update-post pid #(merge % update))))))

(re-frame/reg-event-db ::receive-new-comment
  (fn-traced [db [_ _pid {:keys [_response]}]]
    db))

(re-frame/reg-sub ::single-post
  ;; :doc Extract the single post being viewed.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [pid]}] _]
    (-> (rel/q reldb [[:from :Post] [:where [= :pid pid]]])
        first)))

(re-frame/reg-sub ::user-attributes
  ;; :doc Extract the user attributes of the single post being viewed.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [pid]}] _]
    (-> (rel/q reldb [[:from :PostUserAttributes] [:where [= :pid pid]]])
        first)))

(re-frame/reg-sub ::single-post-sort-path
  ;; :doc Extract the comment sort path for the current single post.
  ;; :doc May be [:BEST], [:NEW], [:TOP] or [:direct cid] when viewing a
  ;; :doc comment direct link.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :sort-path])))

(re-frame/reg-sub ::single-post-status
  ;; :doc Get the status of the post being viewed.
  :<- [::single-post]
  (fn [{:keys [status]} _]
    status))

(re-frame/reg-sub ::show-single-comment-thread?
  ;; :doc Determine whether a single comment's thread is shown.
  :<- [:view/options]
  (fn [{:keys [cid]}]
    (some? cid)))

(re-frame/reg-event-fx ::update-page-title
  ;; :doc Update the page title on the single post page.
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [status title]} (single-post db)
          sub (get-in db [:view :options :sub])
          {:keys [name]} (internal/sub-info-by-name db sub)
          page-title (str (if (= :ACTIVE status)
                            title
                            (trx db "[Deleted Post]"))
                          " | "
                          name)]
      {:fx [[:dispatch [::page-title/update page-title]]]})))

(re-frame/reg-sub ::single-post-author
  ;; :doc Get author info for a post.
  :<- [::internal/reldb]
  :<- [::single-post]
  (fn [[reldb {:keys [uid]}] _]
    (-> reldb
        (rel/q [[:from :User] [:where [= :uid uid]]])
        first)))

(re-frame/reg-sub ::single-post-author-flair
  ;; :doc Get an author's flair for a post, if any.
  :<- [::internal/reldb]
  :<- [::single-post]
  (fn [[reldb {:keys [sid uid]}] _]
    (-> reldb
        (rel/q [[:from :SubUserFlair]
                [:where [= :uid uid] [= :sid sid]]])
        first
        :text)))

(re-frame/reg-sub ::is-post-author?
  ;; :doc Determine whether the current user is the author of the post.
  :<- [::user/is-authenticated?]
  :<- [::user/current-user]
  :<- [::single-post-author]
  (fn [[is-authenticated? current-user author]]
    (and is-authenticated? (= (:name current-user)
                              (:name author)))))

;; TODO archived could be calculated here not on server
(re-frame/reg-sub ::single-post-is-archived?
  ;; :doc Get whether a post is archived.
  :<- [::single-post]
  (fn [post _]
    (get-in post [:is-archived])))

(re-frame/reg-sub ::single-post-nsfw?
  ;; :doc Return whether the currently viewed post contains NSFW content.
  :<- [::single-post]
  :<- [::subs/viewed-sub-info]
  (fn [[post sub] _]
    (or (:nsfw post) (:nsfw sub))))

(re-frame/reg-sub ::single-post-blur
  ;; :doc Return blur if any for the currently viewed post.
  :<- [::single-post-nsfw?]
  :<- [::user/nsfw-settings]
  (fn [[nsfw? {:keys [show? blur?]}] _]
    (when (and nsfw? show? blur?)
      :nsfw-blur)))

(re-frame/reg-sub ::single-post-domain
  ;; :doc Get the domain of a link post.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (when link
      (.getDomain (Uri. link)))))

(re-frame/reg-sub ::post-title-edit-history
  ;; :doc Get a post's title edit history.
  :<- [::single-post]
  :<- [::user/is-mod-or-admin?]
  (fn [[post is-mod-or-admin?] _]
    (when is-mod-or-admin?
      (:title-history post))))

(re-frame/reg-sub ::post-title-edit-history-index
  ;; :doc Get the edit history index for the post title.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :title-history-index])))

(re-frame/reg-event-db ::post-title-see-newer-version
  ;; :doc Look at a newer version of the post title.
  (fn-traced [db _]
    (let [num (count (:title-history (single-post db)))]
      (update-in db [:ui-state :post :title-history-index]
                 (fn [index]
                   (when (and index (< (inc index) num))
                     (inc index)))))))

(re-frame/reg-event-db ::post-title-see-older-version
  ;; :doc Look at an older version of the post title.
  (fn-traced [db _]
    (let [num (count (:title-history (single-post db)))]
      (update-in db [:ui-state :post :title-history-index]
                 (fn [index]
                   (cond
                     (nil? index) (dec num)
                     (zero? index) 0
                     :else (dec index)))))))

(re-frame/reg-sub ::post-title-edit-history-state
  ;; :doc Construct a map with state for the edit history widget.
  :<- [::post-title-edit-history]
  :<- [::post-title-edit-history-index]
  :<- [::single-post-author]
  (fn [[edit-history index author] _]
    (let [older-index (cond
                        (nil? index) (dec (count edit-history))
                        (zero? index) nil
                        :else (dec index))
          edited-by (when older-index
                      (-> edit-history
                          (nth older-index)
                          :user))]
      {:show-history? (seq edit-history)
       :num-edits (inc (count edit-history))
       :index index
       :oldest? (zero? index)
       :newest? (nil? index)
       :by-mod? (not= (:uid edited-by) (:uid author))
       :author (:name edited-by)})))

(re-frame/reg-sub ::post-title
  ;; :doc Get the currently viewed version of the post title
  :<- [::single-post]
  :<- [::post-title-edit-history-index]
  (fn [[{:keys [title-history] :as post} index] _]
    (if index
      (-> title-history
          (nth index)
          :content)
      (:title post))))

(re-frame/reg-sub ::post-content-edit-history
  ;; :doc Get a post's content edit history.
  :<- [::single-post]
  :<- [::user/is-mod-or-admin?]
  (fn [[post is-mod-or-admin?] _]
    (when is-mod-or-admin?
      (:content-history post))))

(re-frame/reg-sub ::post-content-edit-history-index
  ;; :doc Get the edit history index for the post content.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :content-history-index])))

(re-frame/reg-sub ::post-content-edit-history-state
  ;; :doc Construct a map with state for the edit history widget.
  :<- [::post-content-edit-history]
  :<- [::post-content-edit-history-index]
  (fn [[edit-history index] _]
    {:show-history? (seq edit-history)
     :num-edits (inc (count edit-history))
     :index index
     :oldest? (zero? index)
     :newest? (nil? index)}))

(re-frame/reg-event-db ::post-content-see-newer-version
  ;; :doc Look at a newer version of the post content history.
  (fn-traced [db _]
    (let [num (count (:content-history (single-post db)))]
      (update-in db [:ui-state :post :content-history-index]
                 (fn [index]
                   (when (and index (< (inc index) num))
                     (inc index)))))))

(re-frame/reg-event-db ::post-content-see-older-version
  ;; :doc Look at an older version of the post content history.
  (fn-traced [db _]
    (let [num (count (:content-history (single-post db)))]
      (update-in db [:ui-state :post :content-history-index]
                 (fn [index]
                   (cond
                     (nil? index) (dec num)
                     (zero? index) 0
                     :else (dec index)))))))

(re-frame/reg-sub ::post-content
  ;; :doc Get the currently viewed version of the post content.
  :<- [::single-post]
  :<- [::post-content-edit-history-index]
  (fn [[{:keys [content-history] :as post} index] _]
    (if index
      (-> content-history
          (nth index)
          :content)
      (:content post))))

(re-frame/reg-sub ::post-html-content
  ;; :doc Produce the html version of the post content.
  :<- [::post-content]
  (fn [content _]
    (when content
      (util/markdown-to-html content))))

(re-frame/reg-event-db ::record-post-html-content-height
  ;; :doc Save the height of the html content element in the db.
  (fn-traced [db [_ val]]
    (assoc-in db [:ui-state :post :content-height] val)))

(re-frame/reg-sub ::post-html-height
  ;; :doc Extract the height of the post html content element from the db.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :content-height])))

(re-frame/reg-sub ::post-editor-height
  ;; :doc Calculate the height for the post content editor.
  :<- [::post-html-height]
  (fn [height _]
    (when height
      (max 100 (+ 28 height)))))

(re-frame/reg-sub ::embeddable-image?
  ;; :doc Determine if a post is a link to an image.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (and link
         (re-find #"\.(png|jpg|gif|tiff|svg|bmp|jpeg)$" link))))

(re-frame/reg-sub ::embeddable-video?
  ;; :doc Determine if a post is a link to a video.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (and link
         (re-find #"\.(mp4|webm)$" link))))

(defn- gfycat-iframe-url
  [^Uri uri]
  (when (= "gfycat.com" (.getDomain uri))
    (str "https://gfycat.com/ifr/" (subs (.getPath uri) 1))))

(defn- streamable-iframe-url
  [^Uri uri]
  (when (= "streamable.com" (.getDomain uri))
    (str "https://streamable.com/o/" (subs (.getPath uri) 1))))

(defn- streamja-iframe-url
  [^Uri uri]
  (when (= "streamja.com" (.getDomain uri))
    (str "https://streamja.com/embed/" (subs (.getPath uri) 1))))

(defn- youtube-id
  [^Uri uri]
  (let [domain (.getDomain uri)
        path (.getPath uri)]
    (cond
      (and (#{"www.youtube.com" "youtube.com" "m.youtube.com"} domain)
           (= "/watch" path))
      (.getParameterValue uri "v")

      (= "youtu.be" domain)
      (subs path 1))))

(defn- youtube-iframe-url
  [uri]
  (when-let [id (youtube-id uri)]
    (str "https://www.youtube.com/embed/" id)))

(defn- vimeo-iframe-url
  [^Uri uri]
  (when (= "vimeo.com" (.getDomain uri))
    (str "https://player.vimeo.com/video/" (subs (.getPath uri) 1))))

(def iframe-video-support-functions
  [youtube-iframe-url
   vimeo-iframe-url
   gfycat-iframe-url
   streamable-iframe-url
   streamja-iframe-url])

(re-frame/reg-sub ::iframe-video-link
  ;; :doc Construct the link to embed a video in an iframe.
  :<- [::single-post]
  (fn [{:keys [link]} _]
    (when link
      (let [uri (Uri. link)]
        (some (fn [iframe-func]
                (iframe-func uri)) iframe-video-support-functions)))))

;;; Post and comment bottom bar menu element styling

(re-frame/reg-sub ::bottombar-colors
  ;; :doc Get colors for a bottombar menu item.
  ;; :doc Pass it :normal, :subdued or :alert.
  :<- [::window/day?]
  (fn [day? [_ typ]]
    (case typ
      :subdued (if day?
                 :gray.hover-medium-blue
                 :mid-gray.hover-medium-blue)
      :alert   (if day?
                 :dark-red.hover-red
                 :reddish-brown:hover-dark-red)
      (if day?
        :blue.hover-medium-blue
        :dark-blue.hover-medium-blue))))

(re-frame/reg-sub ::post-comment-sort
  ;; :doc Get the currently selected comment sort method.
  :<- [::ui/state]
  (fn [state _]
    (first (get-in state [:post :sort-path]))))

(re-frame/reg-event-fx ::set-comment-sort
  ;; :doc Set the comment sort method.
  (fn-traced [{:keys [db]} [_ val]]
    (let [authenticated? (some? (get-in db [:current-user :uid]))
          mod-or-admin? (-> (or (internal/is-mod-of-sub? db)
                                (get-in db [:current-user :attributes
                                            :is-admin]))
                            boolean)
          pid (get-in db [:view :options :pid])
          {:keys [num level]} (get-in db [:settings :comment-tree])
          first-fetch? (nil? (get-in db [:content :comments pid val]))]
      {:db (cond-> db
             first-fetch? (assoc-in [:status :comment-tree] :loading)
             true (assoc-in [:ui-state :post :sort-path] [val]))
       :fx [[:dispatch [::graphql/query
                        {:graphql subs/graphql
                         :name :get-comment-tree
                         :variables {:is_authenticated authenticated?
                                     :is_mod mod-or-admin?
                                     :pid pid :first num :level level
                                     :sort_by val}
                         :handler [::receive-comment-tree {:pid pid
                                                           :sort-by val}]}]]]})))

(re-frame/reg-event-db ::receive-comment-tree
  ;; :doc "Receive the results of the comment tree query."
  (fn-traced [db [event {:keys [cid pid sort-by response]}]]
    (let [{:keys [data errors]} response
          result (:post-by-pid data)
          sort-path (or (and sort-by [sort-by])
                        (and cid [:direct cid])
                        [(-> result :default-sort keyword)])]
      (-> db
          (assoc-in [:status :comment-tree] :loaded)
          (assoc-in [:ui-state :post :sort-path] sort-path)
          (errors/assoc-errors {:event event :errors errors})
          (cond-> (nil? errors)
            (comment/update-comments (:comment-tree result) pid sort-path))))))

(re-frame/reg-sub ::comment-tree-loaded?
  ;; :doc Extract the load status of the comment tree.
  :<- [::window/statuses]
  (fn [statuses _]
    (= :loaded (:comment-tree statuses))))

;;; Post bottombar menu

(re-frame/reg-sub ::post-can-reply?
  ;; :doc Determine whether the user can reply to a post.
  :<- [::subs/sub-banned?]
  :<- [::single-post]
  :<- [::single-post-author]
  :<- [::user/is-authenticated?]
  :<- [::user/is-mod-or-admin?]
  (fn [[banned? {:keys [is-archived locked status]} author
        is-authenticated? is-mod-or-admin?] _]
    (and is-authenticated?
         (not banned?)
         (not= :DELETED (:status author))
         (not= :DELETED status)
         (not is-archived)
         (or is-mod-or-admin? (not locked)))))

(re-frame/reg-sub ::can-show-post-source?
  ;; :doc Determine whether to show the source link for a post.
  :<- [::single-post-status]
  :<- [::post-content]
  (fn [[status content] _]
    (and (not= :DELETED status) (seq content))))

(re-frame/reg-event-fx ::toggle-post-source
  ;; :doc Toggle the display of the post source.
  ;; :doc If the post content editor is open, close it.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (update-in db [:ui-state :post :show-source?] not)
     :fx [[:dispatch [::hide-edit-post-content]]]}))

(re-frame/reg-event-db ::hide-post-source
  ;; :doc Hide the post content source.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :show-source?] false)))

(re-frame/reg-sub ::show-post-source?
  ;; :doc Extract whether the post source should be shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-source?])))

(re-frame/reg-sub ::can-save-post?
  :<- [::single-post-status]
  (fn [status _]
    (= :ACTIVE status)))

(re-frame/reg-sub ::single-post-saved?
  ;; :doc Extract whether the current single post is saved.
  :<- [::user-attributes]
  (fn [post _]
    (:is-saved post)))

(re-frame/reg-event-fx ::toggle-post-saved
  ;; :doc Change whether the current post is in the user's saved posts list.
  (fn-traced [{:keys [db]} [event _]]
    (let [{:keys [pid is-saved]} (single-post-user-attributes db)
          restore #(assoc-post-user-attributes % pid :is-saved is-saved)]
      {:db (assoc-post-user-attributes db pid :is-saved (not is-saved))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :save-post
                         :id (str :save-post pid)
                         :variables {:pid pid :save (not is-saved)}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-sub ::can-flair-post?
  ;; :doc Determine whether the user has permission to edit the post flair.
  :<- [::subs/viewed-sub-info]
  :<- [::subs/post-flairs]
  :<- [::single-post-status]
  :<- [::single-post-is-archived?]
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  (fn [[sub post-flairs status is-archived? is-author? is-mod-or-admin?] _]
    (and (= :ACTIVE status)
         (not is-archived?)
         (seq post-flairs)
         (or (and is-author?
                  (or (:user-can-flair sub)
                      (:user-must-flair sub)))
             is-mod-or-admin?))))

(re-frame/reg-event-db ::edit-single-post-flair
  ;; :doc Toggle the edit post flair modal.
  (fn-traced [db _]
    (-> db
        (update-in [:ui-state :post :show-edit-post-flair?] not)
        post-forms/initialize-select-flair-form)))

(re-frame/reg-sub ::show-single-post-flair-modal?
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-edit-post-flair?])))

(re-frame/reg-event-fx ::change-post-flair
  ;; :doc Change the post flair on the server.
  (fn-traced [{:keys [db]} [_ {:keys [form args]}]]
    (let [{:keys [pid]} form]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :set-post-flair
                         :id (str :set-post-flair pid)
                         :variables {:pid pid
                                     :flair_id (first args)}
                         :handler [::receive-post-flair-change
                                   {:form form}]}]]]})))

(re-frame/reg-event-db ::receive-post-flair-change
  ;; :doc Receive a response to a post flair change from the server.
  ;; :doc On success, update the post's flair and hide the modal.
  ;; :doc On failure, record the errors and leave the form visible.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [id pid]} form
          {:keys [data errors]} response
          new-flair (get-in data [:set-post-flair :text])
          db' (if errors
                (errors/assoc-errors db {:errors errors :event id})
                (-> db
                    (assoc-in [:ui-state :post :show-edit-post-flair?] false)
                    (assoc-post pid :flair new-flair)))]
      (forms/transition-after-response db' id))))

(re-frame/reg-event-fx ::remove-post-flair
  ;; :doc Ask the server to remove a post flair.
  (fn-traced [_ [_ {:keys [form]}]]
    (let [{:keys [pid]} form]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :remove-post-flair
                         :id (str :remove-post-flair pid)
                         :variables {:pid pid}
                         :handler [::receive-post-flair-removal
                                   {:form form}]}]]]})))

(re-frame/reg-event-db ::receive-post-flair-removal
  ;; :doc Receive a response to a post flair removal from the server.
  ;; :doc On success, delete the post's flair and hide the modal.
  ;; :doc On failure, record the errors and leave the form visible.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [id pid]} form
          errors (:errors response)
          db' (if errors
                (errors/assoc-errors db {:errors errors :event id})
                (-> db
                    (assoc-in [:ui-state :post :show-edit-post-flair?] false)
                    (assoc-post pid :flair nil)))]
      (forms/transition-after-response db' id))))

(re-frame/reg-sub ::can-edit-post-content?
  ;; :doc Determine whether the user can edit the post content.
  :<- [::is-post-author?]
  :<- [::single-post]
  :<- [::single-post-status]
  (fn [[is-author? {:keys [type is-archived]} status] _]
    (and is-author?
         (#{:TEXT :POLL} type) (= :ACTIVE status) (not is-archived))))

(re-frame/reg-event-fx ::toggle-edit-post-content
  ;; :doc Toggle display of the post content editor.
  ;; :doc Initialize the related form if that has not yet been done.
  ;; :doc If the markdown source is shown, and the edit form is toggled,
  ;; :doc switch content display back to html.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (update-in [:ui-state :post :show-content-editor?] not)
             post-forms/initialize-edit-content-form)
     :fx [[:dispatch [::hide-post-source]]]}))

(defn- hide-edit-post-content
  "Hide the post content editor."
  [db]
  (assoc-in db [:ui-state :post :show-content-editor?] false))

(re-frame/reg-sub ::show-post-content-editor?
  ;; :doc Extract whether the edit post content form is shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-content-editor?])))

(re-frame/reg-event-fx ::send-post-edit
  ;; :doc Update post content on the server.
  (fn-traced [{:keys [db]}
              [_ {:keys [form]}]]
    (let [{:keys [id pid content]} form
          existing-content (:content (single-post db))
          unchanged? (= (str/trim content) (str/trim existing-content))]
      (if unchanged?
        {:db (-> db
                 hide-edit-post-content
                 (forms/transition-after-response id))}
        {:fx [[:dispatch [::graphql/mutate
                          {:graphql subs/graphql
                           :name :update-post-content
                           :id (str :update-post-content pid)
                           :variables {:pid pid :content content}
                           :handler [::receive-post-edit-response
                                     {:form form}]}]]]}))))

(defn update-post-content
  "Update the content and content history of a post."
  [{:keys [content] :as post} new-content now]
  (-> post
      (update :content-history concat [{:content content}])
      (assoc :content new-content)
      (assoc :edited (util/to-ISO-time now))))

(defn describe-post-edit-error
  [db {:keys [extensions message] :as error}]
  (let [status (:status extensions)]
    (cond
      (#{"Not found"
         "Post deleted"
         "Not a text post"
         "Post archived"
         "Not authorized"} message) {:msg (trx db "Post cannot be edited")
                                     :status status}

      (= "Content too short" message) {:msg (trx db "Content too short")
                                            :status status}

      (= "Content too long" message) {:msg (trx db "Content too long")
                                           :status status}

      :else
      (errors/describe-error "Error: " error))))

(re-frame/reg-event-fx ::receive-post-edit-response
  ;; :doc Receive a response to a post content edit from the server.
  ;; :doc On success, update the post's content and edit history and
  ;; :doc hide the form. On failure, record the errors and leave the
  ;; :doc form visible.
  [(re-frame/inject-cofx ::window/now)]
  (fn-traced [{:keys [db now]} [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          {:keys [id content pid]} form
          changed? (:update-post-content data)
          describe-error-fn (partial describe-post-edit-error db)
          db' (if (or errors (not changed?))
                (errors/assoc-errors db {:errors errors
                                         :event id
                                         :describe-error-fn describe-error-fn})
                (-> db
                    (update-post pid #(update-post-content % content now))
                    hide-edit-post-content))]
      {:db (forms/transition-after-response db' id)})))

(re-frame/reg-sub ::can-edit-post-title?
  ;; :doc Determine whether the user can edit the post title.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  :<- [::single-post-status]
  :<- [::settings/site-config]
  :<- [:ovarit.app.fe.ui.window/now]
  :<- [::show-post-title-editor?]
  (fn [[is-author? is-mod?
        {:keys [posted is-archived]} status site-config now is-visible?] _]
    (or is-visible?  ; Allow hiding it even if expired.
        ;; Mods should only be able to edit titles of posts not deleted by users
        (and is-mod? (not= :DELETED_BY_USER status) (not is-archived))
        (and is-author?
             (= :ACTIVE status)
             (< (/ (- now (.getTime (js/Date. posted))) 1000)
                (:title-edit-timeout site-config))))))

(re-frame/reg-event-db ::toggle-edit-post-title
  ;; :doc Show the edit post title form.
  (fn-traced [db [_ _]]
    (-> db
        (update-in [:ui-state :post :show-post-title-editor?] not)
        (post-forms/initialize-edit-title-form))))

(defn- hide-edit-post-title
  "Hide the post title editor."
  [db]
  (assoc-in db [:ui-state :post :show-post-title-editor?] false))

(re-frame/reg-sub ::show-post-title-editor?
  ;; :doc Extract whether the edit post title form is shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-post-title-editor?])))

(re-frame/reg-event-fx ::send-post-title-edit
  ;; :doc Update a post title on the server.
  (fn-traced [{:keys [db]}
              [_ {:keys [form]}]]
    (let [{:keys [pid title reason]} form
          existing-title (:title (single-post db))
          unchanged? (= (str/trim title) (str/trim existing-title))]
      (if unchanged?
        {:db (-> db
                 hide-edit-post-title
                 (forms/transition-after-response (:id form)))}
        {:fx [[:dispatch [::graphql/mutate
                          {:graphql subs/graphql
                           :name :update-post-title
                           :id (str :update-post-title pid)
                           :variables {:pid pid :title title :reason reason}
                           :handler [::receive-post-title-edit-response
                                     {:form form}]}]]]}))))

(defn update-post-title
  "Update the title and title edit history of a post."
  [{:keys [title] :as post} user new-title]
  (-> post
      (update :title-history concat [{:content title
                                      :user (select-keys user [:name :uid])}])
      (assoc :title new-title)))

(re-frame/reg-event-db ::receive-post-title-edit-response
  ;; :doc Receive a response to a post title edit from the server.
  ;; :doc On success, update the post's title and edit history and
  ;; :doc hide the form. On failure, record the errors and leave the
  ;; :doc form visible.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          user (:current-user db)
          changed? (:update-post-title data)
          {:keys [id pid title]} form
          db' (if (or errors (not changed?))
                (errors/assoc-errors db {:errors errors :event id})
                (-> db
                    (update-post pid #(update-post-title % user title))
                    hide-edit-post-title))]
      (forms/transition-after-response db' id))))

(re-frame/reg-sub ::can-delete-post?
  ;; :doc Determine whether the user can delete the post.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  :<- [:ovarit.app.fe.ui.window/now]
  :<- [::settings/site-config]
  (fn [[is-author? is-mod-or-admin? {:keys [status type posted]}
        now site-config] _]
    (and (or (and is-author?
                  (or (not= :LINK type)
                      (< (/ (- now (.getTime (js/Date. posted))) 1000)
                         (:link-post-remove-timeout site-config))))
             is-mod-or-admin?)
         (= :ACTIVE status))))

(re-frame/reg-event-fx ::delete-post
  ;; :doc Delete a post.
  (fn-traced [{:keys [db]} [event _]]
    (let [{:keys [pid uid] :as post} (single-post db)
          fields (select-keys post [:status :link :content])
          restore (fn [db]
                    (update-post db pid #(merge % fields)))
          current-user-uid (get-in db [:current-user :uid])]
      (if (= current-user-uid uid)
        {:db (update-post db pid #(merge % {:status :DELETED_BY_USER
                                            :link nil
                                            :content nil}))
         :fx [[:dispatch [::graphql/mutate
                          {:graphql subs/graphql
                           :name :remove-post
                           :id (str :remove-post pid)
                           :variables {:pid pid}
                           :handler [::graphql/receive-mutation-handler
                                     {:event event
                                      :restore restore}]}]]]}
        {:fx [[:dispatch [::post-mod/toggle-mod-delete-post-form]]]}))))

(re-frame/reg-sub ::can-undelete-post?
  ;; :doc Determine whether the user can undelete the post.
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post-status]
  (fn [[is-mod? is-admin? status] _]
    (or (and is-admin? (#{:DELETED_BY_MOD
                          :DELETED_BY_ADMIN} status))
        (and is-mod? (= :DELETED_BY_MOD status)))))

(re-frame/reg-sub ::can-tag-post-nsfw?
  ;; :doc Determine whether the user can change nsfw on the post
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post-status]
  :<- [::subs/viewed-sub-info]
  :<- [::single-post-is-archived?]
  (fn [[is-author? is-mod-or-admin? status sub is-archived?] _]
    (and (or is-author? is-mod-or-admin?)
         (= :ACTIVE status)
         (not (:nsfw sub))
         (not is-archived?))))

(re-frame/reg-event-fx ::toggle-post-nsfw
  ;; :doc Send request to the server to toggle a post's NSFW flag.
  (fn-traced [{:keys [db]} [event _]]
    (let [{:keys [pid nsfw]} (single-post db)
          restore #(assoc-post % pid :nsfw nsfw)]
      {:db (assoc-post db pid :nsfw (not nsfw))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :set-post-nsfw
                         :id (str :set-post-nsfw pid)
                         :variables {:pid pid
                                     :nsfw (not nsfw)}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(re-frame/reg-sub ::can-close-poll?
  ;; :doc Determine whether the the user can close a poll.
  :<- [::is-post-author?]
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod-or-admin? {:keys [status type poll-open]}] _]
    (and (= :ACTIVE status)
         (= :POLL type)
         poll-open
         (or is-author? is-mod-or-admin?))))

(re-frame/reg-event-fx ::close-poll
  ;; :doc Send a close poll request to the server
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [pid]} (single-post db)
          restore #(assoc-post % pid :poll-open true)]
      {:db (assoc-post db pid :poll-open false)
       :fx [[:dispatch
             [::util/ajax-form-post
              [(route-util/url-for db :do/close-poll)
               {:post pid}
               nil
               [::errors/receive-ajax-error ::close-poll restore]]]]]})))

(re-frame/reg-sub ::can-stick-or-unstick-post?
  ;; :doc Determine whether the user can mark the post sticky.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post-status]
  (fn [[is-mod? status] _]
    (and is-mod? (= :ACTIVE status))))

(re-frame/reg-sub ::can-set-announcement-post?
  ;; :doc Determine whether the user can make this post the site announcement.
  :<- [::subs/normalized-sub-name]
  :<- [::user/is-admin?]
  :<- [:settings/current-language]
  :<- [::single-post]
  :<- [::settings/announcement-pid]
  (fn [[sub-name is-admin? lang {:keys [pid status]} announcement-pid] _]
    (and is-admin?
         (= (trx lang "announcements") (str/lower-case sub-name))
         (= :ACTIVE status)
         (not (= pid announcement-pid)))))

(re-frame/reg-sub ::can-change-sticky-post-sort?
  ;; :doc Determine if the user can change the sticky sort.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post]
  :<- [::single-post-status]
  (fn [[is-mod? {:keys [sticky]} status] _]
    (and is-mod? sticky (= :ACTIVE status))))

(re-frame/reg-sub ::next-sticky-post-sort
  ;; :doc Get the next sort type in the rotation.
  :<- [::single-post]
  (fn [post _]
    (next-sticky-post-sort post)))

(re-frame/reg-sub ::can-lock-comments?
  ;; :doc Determine if the user can lock or unlock comments on the post.
  :<- [::user/is-mod-of-sub?]
  :<- [::single-post-status]
  :<- [::single-post-is-archived?]
  (fn [[is-mod? status is-archived?] _]
    (and is-mod? (= :ACTIVE status) (not is-archived?))))

(re-frame/reg-sub ::can-report-post?
  ;; :doc Determine whether the user can report the current post.
  :<- [::is-post-author?]
  :<- [::single-post-status]
  (fn-traced [[is-author? status] _]
    (and (not is-author?) (= :ACTIVE status))))

(re-frame/reg-event-fx ::toggle-report-content-form
  ;; :doc Toggle the display of the report content form.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [rules]} (sub-info-by-name db sub)]
      {:db
       (-> db
           (report-form/initialize-report-form cid)
           (update-in [:ui-state :post :show-report-form?] not))
       :fx [(when (empty? rules)
              [:dispatch [::subs/load-sub-rules]])]})))

(re-frame/reg-sub ::show-report-content-form?
  ;; :doc Extract whether the report content form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:post :show-report-form?])))

(re-frame/reg-event-fx ::report-post
  ;; :doc Send a post report to the Python server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [id pid reason rule explanation]} form
          sub (get-in db [:view :options :sub])
          {:keys [rules]} (sub-info-by-name db sub)
          rule-text (->> rules
                         (filter #(= rule (:id %)))
                         first
                         :text)
          reason-text (case reason
                        :sub-rule (str (trx db "Circle Rule: ")
                                       (if (= :other rule)
                                         explanation
                                         rule-text))
                        :site-rule (trx db "Sitewide Rule violation")
                        :other explanation)]
      {:fx [[:dispatch
             [::util/ajax-csrf-form-post
              {:url (route-util/url-for db :do/report)
               :payload {:post pid
                         :send_to_admin (not= reason :sub-rule)
                         :reason reason-text}
               :form form
               :success-event [::receive-report-post-response]
               :error-event [::receive-report-post-error-response]}]]]})))

(defn- describe-report-post-error
  "Pass through an already translated error message from Python."
  [{:keys [message]}]
  {:msg message})

(re-frame/reg-event-db ::receive-report-post-response
  ;; :doc Handle a server response to a report post request.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [error (:error response)]
      (-> db
          (errors/assoc-errors
           {:event (:id form)
            :errors (errors/errors-from-do-api error)
            :describe-error-fn describe-report-post-error})
          (forms/transition-after-response (:id form))))))

(re-frame/reg-event-db ::receive-report-post-error-response
  ;; :doc Handle an error response to a report post request.
  (fn-traced [db [_ {:keys [form error-response]}]]
    (-> db
        (errors/assoc-errors
         {:event (:id form)
          :errors (errors/errors-from-ajax-error-response error-response)})
        (forms/transition-after-response (:id form)))))

(re-frame/reg-sub ::show-post-reports-link?
  ;; :doc Determine whether to show the link to open post reports.
  :<- [::user/is-mod-or-admin?]
  :<- [::single-post]
  (fn [[is-mod-or-admin? post] _]
    (and is-mod-or-admin? (seq (:open-reports post)))))

(re-frame/reg-sub ::open-post-report-count
  ;; :doc Get the count of open reports on the current post.
  :<- [::single-post]
  (fn [post _]
    (count (:open-reports post))))

(re-frame/reg-sub ::first-open-post-report-id
  ;; :doc Get the id of the first open post report.
  :<- [::single-post]
  (fn [post _]
    (-> post :open-reports first :id)))

(re-frame/reg-sub ::can-distinguish-post-author-oneshot?
  ;; :doc Determine whether the user can distinguish the post author.
  ;; :doc Admins get an option of how to distinguish if they also mod
  ;; :doc the sub.
  ;; :doc If the post is already distinguished, no need to give the
  ;; :doc admin the option.
  :<- [::is-post-author?]
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? (= :ACTIVE status)
         (or is-admin? is-mod?)
         (or (not is-admin?) (not is-mod?) (some? distinguish)))))

(re-frame/reg-sub ::can-distinguish-post-author-choice?
  ;; :doc Determine whether the user has a choice of how to distinguish.
  :<- [::is-post-author?]
  :<- [::user/is-mod-of-sub?]
  :<- [::user/is-admin?]
  :<- [::single-post]
  (fn [[is-author? is-mod? is-admin? {:keys [status distinguish]}] _]
    (and is-author? is-mod? is-admin? (nil? distinguish) (= :ACTIVE status))))

(re-frame/reg-event-db ::toggle-login-to-continue
  ;; :doc Toggle the login to continue modal.
  (fn-traced [db _]
    (-> db
        (update-in [:ui-state :post :show-login?] not))))

(re-frame/reg-sub ::show-login-to-continue?
  ;; :doc Extract whether the login modal should be shown.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :show-login?])))

(re-frame/reg-sub ::can-vote?
  ;; :doc Determine whether the user has active vote arrows on this post.
  ;; :doc Anons get active vote arrows that bring up the login modal.
  :<- [::is-post-author?]
  :<- [::subs/sub-banned?]
  :<- [::single-post-is-archived?]
  :<- [::single-post-author]
  :<- [::single-post-status]
  (fn [[is-author? banned? is-archived? author status
        {:keys [self-vote-posts]}] _]
    (and (or self-vote-posts (not is-author?))
         (not is-archived?)
         (not banned?)
         (= :ACTIVE status)
         (not= :DELETED (:status author)))))

(re-frame/reg-event-fx ::vote-post
  ;; :doc Send a post vote request to the server.
  (fn-traced [{:keys [db]} [_ direction]]
    (let [{:keys [pid vote]} (single-post-user-attributes db)
          sub (-> db :view :options :sub)
          is-mod? (internal/is-mod-or-admin? db sub)
          in-flight? (get-in db [:status :post-vote-in-flight?])
          new-vote (when (not= vote direction)
                     direction)
          restore #(-> %
                       (assoc-post-user-attributes pid :vote vote)
                       (assoc-in [:status :post-vote-in-flight?] false))]
      (when-not in-flight?
        {:db (-> db
                 (assoc-post-user-attributes pid :vote new-vote)
                 (assoc-in [:status :post-vote-in-flight?] true))
         :fx [[:dispatch [::graphql/mutate
                          {:graphql subs/graphql
                           :name :cast-post-vote
                           :id (str :cast-post-vote pid)
                           :variables {:pid pid
                                       :vote new-vote
                                       :is_mod is-mod?}
                           :handler [::receive-vote-post-response
                                     {:pid pid
                                      :restore restore}]}]]]}))))

(re-frame/reg-event-db ::receive-vote-post-response
  ;; :doc Receive the response to a post vote request.
  (fn-traced [db [event {:keys [pid restore response]}]]
    (let [{:keys [data errors]} response
          {:keys [score upvotes downvotes]} (:cast-post-vote data)
          db (assoc-in db [:status :post-vote-in-flight?] false)]
      (if errors
        (-> db
            (errors/assoc-errors {:event event :errors errors})
            restore)
        (update db :reldb rel/transact
                [:update :Post #(assoc %
                                       :score score
                                       :upvotes upvotes
                                       :downvotes downvotes)
                 [= :pid pid]])))))

(re-frame/reg-sub ::see-poll-results?
  ;; :doc Extract whether the user has chosen to see the poll results.
  :<- [::ui/state]
  (fn [state _]
    (get-in state [:post :see-poll-results?])))

(re-frame/reg-event-db ::see-poll-results
  ;; :doc Choose to see the poll results.
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :post :see-poll-results?] true)))

(re-frame/reg-sub ::show-see-results-option?
  ;; :doc Determine whether to show the "See Results" button.
  :<- [::single-post]
  :<- [::see-poll-results?]
  :<- [::can-vote-poll?]
  (fn [[{:keys [hide-results poll-votes]} see-results? can-vote?] _]
    (and (not hide-results) can-vote? (not see-results?)
         poll-votes (pos? poll-votes))))

(re-frame/reg-sub ::show-poll-results?
  ;; :doc Determine whether to show the poll results.
  :<- [::single-post]
  :<- [::can-vote-poll?]
  :<- [::see-poll-results?]
  (fn [[{:keys [poll-open poll-votes hide-results]} can-vote? see-results?] _]
    (and (or (not poll-open) (not hide-results))
         (or (not can-vote?) see-results?)
         (and poll-votes (pos? poll-votes)))))

(re-frame/reg-sub ::poll-votes
  ;; :doc Extract the total poll votes.
  :<- [::single-post]
  (fn [post _]
    (:poll-votes post)))

(re-frame/reg-sub ::poll-option-data
  ;; :doc Extract the poll options from the reldb.
  :<- [:view/options]
  :<- [::internal/reldb]
  (fn [[{:keys [pid]} reldb] [_ _]]
    (rel/q reldb [[:from :PollOption]
                  [:where [= pid :pid]]
                  [:sort [:order :asc]]])))

(re-frame/reg-sub ::poll-options
  ;; :doc Get the poll options with percentages if available.
  :<- [::poll-votes]
  :<- [::poll-option-data]
  (fn [[poll-votes poll-options] [_ _]]
    (map (fn [{:keys [votes] :as option}]
           (assoc option :percent
                  (when (and votes poll-votes (pos? poll-votes))
                    (-> votes
                        (/ poll-votes)
                        (* 100)
                        (+ 0.5)
                        int))))
         poll-options)))

(re-frame/reg-sub ::can-vote-poll?
  ;; :doc Determine whether the user can vote in the poll.
  :<- [::single-post]
  :<- [::user-attributes]
  :<- [::user/is-authenticated?]
  :<- [::subs/sub-banned?]
  :<- [::single-post-is-archived?]
  (fn [[{:keys [poll-open]} {:keys [poll-vote]} is-authenticated? sub-banned?
        is-archived?]
       _]
    (and poll-open
         is-authenticated?
         (not sub-banned?)
         (nil? poll-vote)
         (not is-archived?))))

(defn- set-poll-vote [db id val]
  (let [pid (:pid (single-post db))]
    (-> db
        (assoc-post-user-attributes pid :poll-vote val)
        (update :reldb rel/transact
                [:update :PollOption #(update % :votes (if val inc dec))
                 [= id :option-id]])
        (update-post pid #(update % :poll-votes (if val inc dec))))))

(re-frame/reg-event-fx ::vote-poll
  ;; :doc Vote on a poll.
  (fn-traced [{:keys [db]} [_ id]]
    (let [pid (:pid (single-post db))
          restore #(set-poll-vote % id nil)]
      {:db (set-poll-vote db id id)
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/cast-vote :pid pid :oid id)
                         {}
                         [::receive-poll-vote {:restore restore}]
                         [::errors/receive-ajax-error ::vote-poll
                          restore]]]]]})))

(re-frame/reg-sub ::can-withdraw-poll-vote?
  ;; :doc Determine whether the user can withdraw a poll vote.
  :<- [::single-post]
  :<- [::user-attributes]
  :<- [::user/is-authenticated?]
  :<- [::subs/sub-banned?]
  :<- [::single-post-is-archived?]
  (fn [[{:keys [poll-open]} {:keys [poll-vote]} is-authenticated? sub-banned?
        is-archived?] _]
    (and poll-open
         is-authenticated?
         (not sub-banned?)
         poll-vote
         (not is-archived?))))

(re-frame/reg-event-fx ::withdraw-poll-vote
  ;; :doc Withdraw a poll vote.
  (fn-traced [{:keys [db]} [_ _]]
    (let [pid (:pid (single-post db))
          vote-id (:poll-vote (single-post-user-attributes db))
          see? (get-in db [:ui-state :post :see-poll-results])
          restore #(-> %
                       (assoc-in [:ui-state :post :see-poll-results?] see?)
                       (set-poll-vote vote-id vote-id))]
      {:db (-> db
               (assoc-in [:ui-state :post :see-poll-results?] true)
               (set-poll-vote vote-id nil))
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/remove-vote :pid pid)
                         {}
                         [::receive-poll-vote {:restore restore}]
                         [::errors/receive-ajax-error ::withdraw-poll-vote
                          restore]]]]]})))

(re-frame/reg-event-db ::receive-poll-vote
  ;; :doc Receive the server response to a poll vote or withdrawn vote.
  (fn [db [event {:keys [restore]} {:keys [status error]}]]
    (let [db-with-errors (errors/assoc-errors
                          db
                          {:event event
                           :errors (errors/errors-from-do-api error)
                           :describe-error-fn describe-report-post-error})]
      (cond
        (= status "ok") db
        ;; The poll closed message is translated so we really shouldn't
        ;; test it.
        (= error "Poll is closed") (restore db-with-errors)
        ;; If we got here it means the user already voted from another
        ;; browser tab.  When this route is converted to GraphQL we
        ;; should probably get the current state of the user's vote,
        ;; or use a subscription to be up to date.
        :else db-with-errors))))

(defn update-post-user-flair
  "Update the user's flair on all the posts in the sub in the db."
  ;; TODO get that author-flair field out of the post and just use the
  ;; sub object.
  [db sid flair]
  (let [current-user-uid (get-in db [:current-user :uid])]
    (-> db
        (update :reldb rel/transact
                [:update :Post
                 #(assoc % :author-flair flair)
                 [:and [= :sid sid] [= :uid current-user-uid]]]))))
