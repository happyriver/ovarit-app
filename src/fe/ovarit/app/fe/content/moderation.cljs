;; fe/content/moderation.cljs -- Moderator pages for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.moderation
  (:require
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :refer [sub-info-by-name] :as internal]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.moderation :as mod-forms]
   [ovarit.app.fe.user :as-alias user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

;; Import graphql queries.
(defgraphql graphql "graphql/moderation.graphql")

(defn- update-post-type-config
  [db name post-type vals]
  (update db :reldb rel/transact
          [:update :SubPostTypeConfig #(merge % vals)
           [:and
            [= name :name]
            [= [:_ post-type] :post-type]]]))

(re-frame/reg-event-fx ::update-post-type-config
  ;; :doc Update a post type's settings on the server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [id name post-type mods-only? rules]} form
          {:keys [sid]} (sub-info-by-name db name)
          existing (internal/get-post-type-config (:reldb db) name post-type)
          changed? (or (not= (str/trim rules) (:rules existing))
                       (not= mods-only? (:mods-only? existing)))
          restore #(update-post-type-config
                    % name post-type
                    (select-keys existing [:mods-only? :rules]))
          vals {:rules (str/trim rules)
                :mods_only mods-only?}]
      (if changed?
        {:db (update-post-type-config db name post-type vals)
         :fx [[:dispatch [::graphql/mutate
                          {:graphql graphql
                           :name :update-sub-post-type-config
                           :variables (merge vals {:sid sid
                                                   :post_type post-type})
                           :handler [::forms/generic-graphql-handler
                                     {:restore restore
                                      :form form}]}]]]}
        {:db (forms/transition-after-response db id)}))))

(re-frame/reg-event-fx ::create-sub-post-flair
  ;; :doc Create a new sub post flair.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [id flair]} form
          sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :create-sub-post-flair
                         :variables {:text flair :sid sid}
                         :handler [::receive-create-sub-post-flair
                                   {:sid sid
                                    :form form}]}]]]})))

(defn- new-flair-order
  "Return an order value larger than any of the sub rules for `sid.`"
  [{:keys [reldb]} sid]
  (let [largest (-> reldb
                    (rel/q [[:from :SubPostFlair]
                            [:where [= :sid sid]]
                            [:agg [] [:max-order [max :order]]]])
                    first
                    :max-order)]
    (inc (or largest -1))))

(re-frame/reg-event-db ::receive-create-sub-post-flair
  ;; :doc Receive the results of the create a sub post flair mutation.
  ;; :doc Add the new post flair to the db.
  (fn-traced [db [_ {:keys [sid form response]}]]
    (let [{:keys [data errors]} response
          post-flair (-> (:create-sub-post-flair data)
                         (assoc :order (new-flair-order db sid)
                                :sid sid)
                         (update :post-types util/set-of-keywords))]
      (cond-> db
        errors     (errors/assoc-errors {:event (:id form) :errors errors})
        post-flair (update :reldb rel/transact
                           [:insert :SubPostFlair post-flair])
        true       (forms/transition-after-response (:id form))))))

(re-frame/reg-sub ::post-flairs-ui-state
  ;; :doc Extract the UI state for a post flair by id.
  :<- [::internal/reldb]
  (fn [reldb [_ id]]
    (rel/row reldb [[:from :SubPostFlairState]
                    [:where [= id :id]]])))

(defn- set-post-flair-options-open
  [db id val]
  (update db :reldb rel/transact
          [:insert-or-replace :SubPostFlairState
           {:id id :options-open? val}]))

(re-frame/reg-event-db ::show-post-flair-options
  ;; :doc Open the options field for a post flair.
  ;; :doc Initialize the associated form.
  (fn [db [_ id]]
    (-> db
        (set-post-flair-options-open id true)
        (mod-forms/initialize-edit-post-flair id))))

(re-frame/reg-event-db ::hide-post-flair-options
  ;; :doc Close the options field for a post flair.
  (fn [db [_ id]]
    (set-post-flair-options-open db id false)))

(re-frame/reg-event-fx ::update-sub-post-flair
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [id flair-id mods-only post-types]} form
          sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)
          existing-flair (rel/row (:reldb db) [[:from :SubPostFlair]
                                               [:where [= flair-id] :id]])
          restore #(update % :reldb rel/transact
                           [:insert-or-replace :SubPostFlair existing-flair])]
      {:db (update db :reldb rel/transact
                   [:update :SubPostFlair
                    #(merge % {:mods-only mods-only
                               :post-types post-types})
                    [= :id flair-id]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :update-sub-post-flair
                         :variables {:id flair-id :sid sid
                                     :mods_only mods-only
                                     :post_types (vec post-types)}
                         :handler [::forms/generic-graphql-handler
                                   {:form form
                                    :restore restore}]}]]]})))

(re-frame/reg-event-fx ::delete-sub-post-flair
  ;; :doc Delete a sub post flair.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [flair-id]} form
          sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)
          flair (rel/row (:reldb db)
                         [[:from :SubPostFlair] [:where [= :id flair-id]]])
          restore #(update % :reldb rel/transact
                           [:insert :SubPostFlair flair])]
      {:db (update db :reldb rel/transact
                   [:delete :SubPostFlair [= :id flair-id]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :delete-sub-post-flair
                         :variables {:id flair-id :sid sid}
                         :handler [::forms/generic-graphql-handler
                                   {:form form
                                    :restore restore}]}]]]})))

(re-frame/reg-event-fx ::reorder-post-flairs
  ;; :doc Reorder the post flairs for the currently viewed sub.
  (fn-traced [{:keys [db]} [_ move-index dest-index]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)
          flairs (-> (:reldb db)
                     (rel/q [[:from :SubPostFlair]
                             [:where [= :sid sid]]
                             [:sort [:order :asc]]]))
          reordered-flairs (util/reorder-list flairs move-index dest-index)
          update-order (internal/assign-order-func reordered-flairs)]
      {:db (update db :reldb rel/transact
                   [:update :SubPostFlair update-order
                    [= :sid sid]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :reorder-sub-post-flairs
                         :variables {:sid sid :ids (map :id reordered-flairs)}
                         :handler [::receive-reordered-post-flairs
                                   {:sid sid}]}]]]})))

(re-frame/reg-event-fx ::receive-reordered-post-flairs
  ;; :doc Receive the results of the sub post flair reordering from the server.
  (fn-traced [{:keys [db]} [event {:keys [sid response]}]]
    (let [{:keys [data errors]} response
          post-flairs (:reorder-sub-post-flairs data)]
      {:db (cond-> db
             true        (errors/assoc-errors {:event event :errors errors})
             post-flairs (internal/update-flairs sid post-flairs))})))

(re-frame/reg-event-fx ::reorder-rules
  ;; :doc Reorder the rules for the currently viewed sub.
  (fn-traced [{:keys [db]} [_ move-index dest-index]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)
          rules (rel/q (:reldb db) [[:from :SubRule]
                                    [:where [= :sid sid]]
                                    [:sort [:order :asc]]])
          reordered-rules (util/reorder-list rules move-index dest-index)
          update-order (internal/assign-order-func reordered-rules)]
      {:db (update db :reldb rel/transact
                   [:update :SubRule update-order
                    [= :sid sid]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :reorder-sub-rules
                         :variables {:sid sid :ids (map :id reordered-rules)}
                         :handler [::receive-reordered-rules {:sid sid}]}]]]})))

(re-frame/reg-event-fx ::receive-reordered-rules
  ;; :doc Receive the results of the sub rule reordering from the server.
  (fn-traced [{:keys [db]} [event {:keys [sid response]}]]
    (let [{:keys [data errors]} response
          rules (:reorder-sub-rules data)]
      {:db (cond-> db
             errors (errors/assoc-errors {:event event :errors errors})
             rules  (internal/update-rules sid rules))})))

(re-frame/reg-event-fx ::create-sub-rule
  ;; :doc Create a new sub rule.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [id rule]} form
          sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :create-sub-rule
                         :variables {:text rule :sid sid}
                         :handler [::receive-create-sub-rule
                                   {:sid sid
                                    :form form}]}]]]})))

(defn- new-rule-order
  "Return an order value larger than any of the sub rules for `sid.`"
  [{:keys [reldb]} sid]
  (let [largest (-> reldb
                    (rel/q [[:from :SubRule]
                            [:where [= :sid sid]]
                            [:agg [] [:max-order [max :order]]]])
                    first
                    :max-order)]
    (inc (or largest -1))))

(re-frame/reg-event-db ::receive-create-sub-rule
  ;; :doc Receive the results of the create a sub rule mutation.
  ;; :doc Add the new rule to the db.
  (fn-traced [db [_ {:keys [sid form response]}]]
    (let [{:keys [data errors]} response
          rule (-> (:create-sub-rule data)
                   (assoc :order (new-rule-order db sid)
                          :sid sid))]
      (cond-> db
        true (errors/assoc-errors {:event (:id form) :errors errors})
        rule (update :reldb rel/transact [:insert :SubRule rule])
        true (forms/transition-after-response (:id form))))))

(re-frame/reg-event-fx ::delete-sub-rule
  ;; :doc Delete a sub rule.
  (fn-traced [{:keys [db]} [_ id]]
    (let [sub (get-in db [:view :options :sub])
          {:keys [sid]} (sub-info-by-name db sub)]
      {:db (update db :reldb rel/transact [:delete :SubRule [= :id id]])
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :delete-sub-rule
                         :variables {:id id :sid sid}
                         :handler [::receive-delete-sub-rule]}]]]})))

(re-frame/reg-event-db ::receive-delete-sub-rule
  ;; :doc Receive the results of the delete a sub rule mutation.
  (fn-traced [db [event {:keys [response]}]]
    (errors/assoc-errors db {:event event :errors (:errors response)})))

(re-frame/reg-event-fx ::load-current-user-mod-stats
  ;; :doc Fetch statistics on the subs moderated by the current user.
  (fn-traced [_ [_ _]]
    {:dispatch [::graphql/query
                {:graphql graphql
                 :name :current-user-mod-stats
                 :variables {}
                 :handler [::receive-current-user-mod-stats]}]}))

(re-frame/reg-event-db ::receive-current-user-mod-stats
  ;; :doc Receive statistics on moderated subs from the server.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          subs (->> (get-in data [:current-user :subs-moderated])
                    (map :sub))
          compare-by-name (fn [a b]
                            (compare (str/lower-case (:name a))
                                     (str/lower-case (:name b))))
          notifs #(select-keys % [:sid
                                  :open-report-count
                                  :unread-modmail-count
                                  :new-unread-modmail-count
                                  :in-progress-unread-modmail-count
                                  :all-unread-modmail-count
                                  :discussion-unread-modmail-count
                                  :notification-unread-modmail-count])]
      (-> db
          (errors/assoc-errors {:event event
                                :errors errors})
          (assoc-in [:content :mod-stats :subs-moderated]
                    (sort compare-by-name subs))
          (update :reldb rel/transact
                  (into [:insert-or-replace :SubModNotificationCount]
                        (map notifs subs)))))))

(re-frame/reg-event-fx ::load-admin-mod-stats
  ;; :doc Fetch moderation statistics on all subs for the admin.
  (fn-traced [{:keys [db]} [_ {:keys [cursor] :as options}]]
    (let [page-size (* 2 (get-in db [:settings :page-size]))
          args (if cursor
                 {:first page-size :after cursor}
                 {:first page-size})]
      {:db (assoc-in db [:status :content] :loading)
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql
                         :name :admin-mod-stats
                         :variables args
                         :handler [::receive-admin-mod-stats options]}]]]})))

(re-frame/reg-event-db ::receive-admin-mod-stats
  ;; :doc Receive moderation statistics on all subs for the admin
  (fn-traced [db [event {:keys [cursor response]}]]
    (let [{:keys [data errors]} response
          page-info (get-in data [:all-subs :page-info])
          results (get-in data [:all-subs :edges])
          new-subs (map :node results)
          path [:content :mod-stats :all-sub-stats]
          existing-subs (if (= cursor (get-in db (conj path :cursor)))
                          (get-in db (conj path :subs))
                          [])
          subs (concat existing-subs new-subs)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:status :content] :loaded)
          (assoc-in [:content :mod-stats :all-sub-stats]
                    {:subs subs
                     :more-items-available? (:has-next-page page-info)
                     :cursor (:end-cursor page-info)})))))

(re-frame/reg-sub ::-mod-stats
  ;; :doc Extract the sub moderation data.
  (fn [db _]
    (get-in db [:content :mod-stats])))

(re-frame/reg-sub ::moderated-sub-stats
  ;; :doc Extract the list of moderated subs with statistics.
  :<- [::-mod-stats]
  :<- [:view/options]
  :<- [::user/subs-not-moderated-names]
  (fn [[mod-stats {:keys [show-other-circles?]} subs-not-modded-names] _]
    (let [not-modded? (set subs-not-modded-names)]
      (if show-other-circles?
        (filter #(not-modded? (:name %))
                (get-in mod-stats [:all-sub-stats :subs]))
        (get mod-stats :subs-moderated)))))

(re-frame/reg-sub ::more-moderated-sub-stats-available?
  ;; :doc Are there more subs with moderation statistics that could be fetched?
  :<- [::-mod-stats]
  :<- [:view/options]
  (fn [[mod-stats {:keys [show-other-circles?]}] _]
    (when show-other-circles?
      (get-in mod-stats [:all-sub-stats :more-items-available?]))))

(re-frame/reg-sub ::moderated-sub-stats-cursor
  ;; :doc Extract the cursor for fetching more sub stats.
  :<- [::-mod-stats]
  :<- [:view/options]
  (fn [[mod-stats {:keys [show-other-circles?]}] _]
    (when show-other-circles?
      (get-in mod-stats [:all-sub-stats :cursor]))))
