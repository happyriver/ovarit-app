;; fe/content/db.cljs -- DB functions for ovarit-app content
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.internal
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

(defn sub-info-by-name
  "Look up a sub record in the db by name."
  [{:keys [reldb]} name]
  (when name
    (-> reldb
        (rel/q [[:from :SubName]
                [:left-join :Sub {:sid :sid}]
                [:where [= [str/lower-case :name] (str/lower-case name)]]])
        first)))

(defn sid-from-name
  "Look up a sid from a name, using the sub list in the db."
  [{:keys [reldb]} name]
  (when name
    (-> reldb
        (rel/q [[:from :SubName]
                [:where [= [str/lower-case :name] (str/lower-case name)]]])
        first
        :sid)))

(defn sub-post-flairs
  "Get the preset post flairs for a sub."
  [{:keys [reldb]} sid]
  (rel/q reldb [[:from :SubPostFlair]
                [:where [= :sid sid]]
                [:sort [:order :asc]]]))

(defn current-user-sub-flair
  "Get the current user's flair in the current sub."
  [{:keys [reldb] :as db}]
  (let [sub-name (get-in db [:view :options :sub])
        sid (sid-from-name db sub-name)
        uid (get-in db [:current-user :uid])]
    (-> reldb
        (rel/q [[:from :SubUserFlair]
                [:where [:and [= :sid sid] [= :uid uid]]]])
        first
        :text)))

(defn sub-rules
  "Get the rules for a sub."
  [{:keys [reldb]} sid]
  (rel/q reldb [[:from :SubRule]
                [:where [= :sid sid]]
                [:sort [:order :asc]]]))

(defn subs-moderated-names
  "Get the list of names of subs moderated by the user."
  [db]
  (->> (rel/q (:reldb db)
              [[:from :SubMod]
               [:where [= :uid (-> db :current-user :uid)]]
               [:left-join :SubName {:sid :sid}]])
       (map :name)
       (sort-by str/lower-case)))

(defn subs-moderated-sids
  "Get the list of sids of subs moderated by the user."
  [db]
  (->> (rel/q (:reldb db)
              [[:from :SubMod]
               [:where [= :uid (-> db :current-user :uid)]]])
       (map :sid)))

(defn is-mod-of-sub?
  "Determine if the current user is mod of the current sub."
  ([db] (is-mod-of-sub? db (-> db :view :options :sub)))
  ([db sub-name]
   (some? (rel/row (:reldb db)
                   [[:from :SubMod]
                    [:where [= :uid (-> db :current-user :uid)]]
                    [:left-join :SubName {:sid :sid}]
                    [:where [= :name sub-name]]]))))

(defn is-mod-or-admin?
  "Determine if the current user is either an admin or mod."
  [db sub]
  (->
   (or (get-in db [:current-user :attributes :is-admin])
       (is-mod-of-sub? db sub))
   boolean))

(re-frame/reg-sub ::content
  ;; :doc Extract the content section of the db.
  (fn [db _]
    (:content db)))

(re-frame/reg-sub ::reldb
  ;; :doc Extract the database of objects from the db.
  (fn [db _]
    (:reldb db)))

(defn get-post-type-config
  "Look up config for a post type in the reldb."
  [reldb sub-name post-type]
  (rel/row reldb [[:from :SubPostTypeConfig]
                  [:where [:and
                           [= [:_ post-type] :post-type]
                           [= sub-name :name]]]]))

(defn assign-order-func
  "Return a function to update the :order field of a list of items."
  [items]
  (let [ids (map :id items)
        ordering (->> (map vector ids (range))
                      (into {}))]
    (fn [{:keys [id] :as item}]
      (assoc item :order (get ordering id)))))

(defn update-flairs
  "Replace the existing post flairs for sid in the db."
  [db sid flairs]
  (let [flairs (map (fn [flair num]
                      (-> flair
                          (assoc :sid sid :order num)
                          (update :post-types util/set-of-keywords)))
                    flairs (range))]
    (-> db
        (update :reldb rel/transact [:delete :SubPostFlair [= :sid sid]])
        (update :reldb rel/transact (into [:insert :SubPostFlair] flairs)))))

(defn update-rules
  "Replace the existing sub rules in the db."
  [db sid rules]
  (let [rules (map (fn [rule num]
                     (assoc rule :sid sid :order num)) rules (range))]
    (-> db
        (update :reldb rel/transact [:delete :SubRule [= :sid sid]])
        (update :reldb rel/transact (into [:insert :SubRule] rules)))))

(defn fixup-name-history
  [hist uid]
  (-> hist
      (assoc :uid uid)
      (update :changed js/parseInt)))

(defn not-a-username?
  "Determine if a string contains acceptable username characters."
  [username]
  (->> username
       str/trim
       (re-matches #"[A-Za-z0-9_-]+")
       nil?))
