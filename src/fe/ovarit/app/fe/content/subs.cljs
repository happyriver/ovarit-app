;; fe/content/subs.cljs -- Subs for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.subs
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.tr :as tr :refer [trmx-html trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user :as-alias user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

;; Import graphql queries.

(defgraphql graphql "graphql/subs.graphql")

;; ::info is used for the sub details in the get_single_post
;; and sub_info queries.
(defmethod loader/content-status-id ::info [{:keys [name]}]
  (util/add-suffix ::info name))
(defmethod loader/content-status ::info [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::post-flairs is used for the sub's post flairs in the
;; sub_post_flairs query.
(defmethod loader/content-status-id ::post-flairs [{:keys [name]}]
  (util/add-suffix ::post-flairs name))
(defmethod loader/content-status ::post-flairs [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::post-types is used for the list of post types configurations
;; in the post_type_config and submit_post_config queries.
(defmethod loader/content-status-id ::post-types [{:keys [name]}]
  (util/add-suffix ::post-types name))
(defmethod loader/content-status ::post-types [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::sub-names is used for the list of sub names in the
;; get_all_subs query.
(defmethod loader/content-status-id ::sub-names [_]
  ::sub-names)
(defmethod loader/content-status ::sub-names [_]
  ::loader/content-status-fields)

;; ::sub-rules is used for the list of sub rules in the sub_rules
;; query.
(defmethod loader/content-status-id ::sub-rules [{:keys [name]}]
  (util/add-suffix ::sub-rules name))
(defmethod loader/content-status ::sub-rules [_]
  (spec/merge ::loader/content-status-fields
              (spec/keys :req-un [:type.Sub/name])))

;; ::submit-post-settings is used for some fields in the
;; db :settings map pertaining to the submit post form.
(defmethod loader/content-status-id ::submit-post-settings [_]
  ::submit-post-settings)
(defmethod loader/content-status ::submit-post-settings [_]
  ::loader/content-status-fields)

(loader/reg-loader ::sub-names
  ;; :doc Load all sub names from the server.
  {:make-content-status
   (fn [_] {:content-type ::sub-names})

   :load
   (fn-traced [{:keys [db]} _]
     {:db (update db :reldb rel/transact [:delete :SubName])
      :fx [[:dispatch [::graphql/query
                       {:graphql graphql
                        :name :get-all-subs
                        :variables {:first 100}
                        :handler [::receive-sub-names]}]]]})

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           next-page? (get-in data [:all-subs :page-info :has-next-page])
           end-cursor (get-in data [:all-subs :page-info :end-cursor])
           subs (get-in data [:all-subs :edges])]
       {:db (-> db
                (update :reldb rel/transact
                        (into [:insert-or-replace :SubName]
                              (map :node subs)))
                (errors/assoc-errors {:event event :errors errors}))
        :fx  [(if next-page?
                [:dispatch [::graphql/query
                            {:graphql graphql
                             :name :get-all-subs
                             :variables {:first 500 :after end-cursor}
                             :handler [::receive-sub-names]}]]
                [:dispatch [::sub-names-completed
                            (if (some? errors) :failure :success)]])]}))})

(re-frame/reg-event-fx ::check-sub
  ;; :doc Check that the sub name in [:view :options] exists.
  ;; :doc Fix the case of the name if it is incorrect, or set it to
  ;; :doc nil if no such sub exists..
  ;; :doc Load all sub names from the server before dispatching this event.
  (fn-traced [{:keys [db]} [event _]]
    (let [sub-name (get-in db [:view :options :sub])
          sub (when sub-name
                (rel/row (:reldb db)
                         [[:from :SubName]
                          [:where [= [str/lower-case :name]
                                   (str/lower-case sub-name)]]]))]
      {:db (assoc-in db [:view :options :sub] (:name sub))
       :fx [[:dispatch [::loader/update-on-completion [event] :success]]]})))

(re-frame/reg-event-fx ::load-all-subs
  ;; :doc Start loading all subs.
  (fn-traced [{:keys [db]} [_ on-load]]
    {:db (assoc-in db [:status :subs] :loading)
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql
                       :name :get-all-subs
                       :variables {:first 100}
                       :handler [::receive-all-subs {:on-load on-load}]}]]]}))

(re-frame/reg-event-fx ::receive-all-subs
  ;; :doc Collect all the subs using multiple queries if necessary.
  ;; :doc Probably want to do this lazily if the number of subs
  ;; :doc gets large.
  (fn-traced [{:keys [db]} [event {:keys [on-load response]}]]
    (let [{:keys [data errors]} response
          next-page? (get-in data [:all-subs :page-info :has-next-page])
          end-cursor (get-in data [:all-subs :page-info :end-cursor])
          subs (get-in data [:all-subs :edges])
          dbx (-> db
                  (update :reldb rel/transact
                          (into [:insert-or-replace :SubName] (map :node subs)))
                  (errors/assoc-errors {:event event
                                        :errors errors}))]
      {:db (if next-page?
             dbx
             (assoc-in dbx [:status :subs] :loaded))
       :fx  [(if next-page?
               [:dispatch [::graphql/query
                           {:graphql graphql
                            :name :get-all-subs
                            :variables {:first 500 :after end-cursor}
                            :handler [::receive-all-subs {:on-load on-load}]}]]
               (when on-load
                 [:dispatch on-load]))]})))

(re-frame/reg-sub ::all-subs
  ;; :doc Extract the list of subs.
  :<- [::internal/reldb]
  (fn [reldb _]
    (rel/q reldb [[:from :Sub]])))

(re-frame/reg-sub ::all-sub-names
  ;; :doc Extract the list of sub names.
  :<- [::internal/reldb]
  (fn [reldb _]
    (->> (rel/q reldb [[:from :SubName]])
         (map :name)
         (sort-by str/lower-case))))

(re-frame/reg-sub ::sub-load-status
  ;; :doc Extract the status of loading subs from the server.
  (fn [db _]
    (get-in db [:status :subs])))

(re-frame/reg-sub ::viewed-sub-info
  ;; :doc Return info on the sub targeted by the view options.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [sub]}] _]
    (when sub
      (-> reldb
          (rel/q [[:from :SubName]
                  [:left-join :Sub {:sid :sid}]
                  [:where [= [str/lower-case :name] (str/lower-case sub)]]])
          first))))

(re-frame/reg-sub ::sub-banned?
  ;; :doc Extract whether the user is banned in the current sub.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (get-in sub [:user-attributes :banned])))

(re-frame/reg-sub ::normalized-sub-name
  ;; :doc Return the sub name routed to with the correct capitalization.
  ;; :doc Return the empty string if it's not a valid sub name.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (or (:name sub) "")))

(re-frame/reg-sub ::raw-sidebar
  ;; :doc Extract the sub sidebar pre-markdown text.
  :<- [::viewed-sub-info]
  (fn [sub _]
    (:sidebar sub)))

(re-frame/reg-sub ::sidebar
  ;; :doc Format the sub's sidebar text with markdown.
  :<- [::raw-sidebar]
  (fn [text _]
    (if (empty? text)
      ""
      (util/markdown-to-html text))))

(re-frame/reg-sub ::moderators
  ;; :doc Extract the list of sub moderators and their levels
  :<- [::viewed-sub-info]
  (fn [sub _]
    (:moderators sub)))

(re-frame/reg-sub ::active-moderators
  ;; :doc Filter out deleted moderators from the moderator list.
  :<- [::moderators]
  (fn [mods _]
    (filter #(= :ACTIVE (get-in % [:mod :status])) mods)))

(re-frame/reg-sub ::mod-names
  ;; :doc Get names of mods of the current sub.
  ;; :doc Levels can be a set of moderation level keywords
  ;; :doc or nil for all.
  :<- [::active-moderators]
  (fn [mods [_ levels]]
    (let [level? (or levels #{:OWNER :MODERATOR :JANITOR})]
      (->> mods
           (filter #(level? (:moderation-level %)))
           (map #(get-in % [:mod :name]))
           sort))))

(re-frame/reg-sub ::sub-user-flair
  ;; :doc Get a user's flair in a sub.
  :<- [::internal/reldb]
  (fn [reldb [_ sid uid]]
    (-> reldb
        (rel/q [[:from :SubUserFlair]
                [:where [:and [= :sid sid] [= :uid uid]]]])
        first
        :text)))

(loader/reg-loader ::sub-rules
  ;; :doc Load the post flairs for a sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::sub-rules
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql
                                            :name :sub-rules
                                            :variables {:name sub-name}
                                            :handler [::receive-sub-rules]}]]
               [:dispatch [::sub-rules-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           sub-rules (:sub-by-name data)
           {:keys [rules sid]} sub-rules]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              rules  (internal/update-rules sid rules))
        :fx [[:dispatch [::sub-rules-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::rules
  ;; :doc Extract the rules for the currently viewed sub.
  :<- [::internal/reldb]
  :<- [::viewed-sub-info]
  (fn [[reldb {:keys [sid]}] _]
    (rel/q reldb [[:from :SubRule]
                  [:where [= :sid sid]]
                  [:sort [:order :asc]]])))

(loader/reg-loader ::post-flairs
  ;; :doc Load the post flairs for a sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::post-flairs
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql
                                            :name :sub-post-flairs
                                            :variables {:name sub-name}
                                            :handler [::receive-post-flairs]}]]
               [:dispatch [::post-flairs-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           {:keys [post-flairs sid]} (:sub-by-name data)]
       {:db (cond-> db
              errors      (errors/assoc-errors {:event event :errors errors})
              post-flairs (internal/update-flairs sid post-flairs))
        :fx [[:dispatch [::post-flairs-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::post-flairs
  ;; :doc Extract the post flairs for the currently viewed sub.
  :<- [::internal/reldb]
  :<- [::viewed-sub-info]
  (fn [[reldb {:keys [sid]}] _]
    (rel/q reldb [[:from :SubPostFlair]
                  [:where [= :sid sid]]
                  [:sort [:order :asc]]])))


(defn permitted-flairs
  "Get the permitted flair objects for the selected post type."
  [reldb {:keys [sub-name is-mod-or-admin? post-type]}]
  (let [usable? (fn [{:keys [mods-only post-types]}]
                  (or is-mod-or-admin?
                      (and (not mods-only)
                           (post-types post-type))))]
    (rel/q reldb [[:from :SubPostFlair]
                  [:join :SubName {:sid :sid}]
                  [:where [= :name sub-name]]
                  [:where [usable?]]
                  [:sort [:order :asc]]])))

(defn- fixup-post-type-config
  [name config]
  (-> config
      (assoc :name name)
      (update :post-type keyword)))

(loader/reg-loader ::post-types
  ;; :doc Load the post types configuration for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::post-types
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql
                                            :name :post-type-config
                                            :variables {:name sub-name}
                                            :handler [::receive-post-types]}]]
               [:dispatch [::post-types-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           name (get-in data [:sub-by-name :name])
           configs (->> (get-in data [:sub-by-name :post-type-config])
                        (map #(fixup-post-type-config name %)))]
       {:db (-> db
                (errors/assoc-errors {:event event :errors errors})
                (cond-> data
                  (update :reldb rel/transact
                          (into [:insert-or-replace :SubPostTypeConfig]
                                configs))))
        :fx [[:dispatch [::post-types-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-sub ::post-type-config
  ;; :doc Extract a post type config for the current sub.
  :<- [::internal/reldb]
  :<- [::normalized-sub-name]
  (fn [[reldb sub-name] [_ post-type]]
    (internal/get-post-type-config reldb sub-name post-type)))

(defn- fixup-sub
  "Make sub details fields more clojure-friendly."
  [sub]
  (let [keywordize-mod #(-> %
                            (update :moderation-level keyword)
                            (update-in [:mod :status] keyword))]
    (-> sub
        (update-in [:creator :status] keyword)
        (update :moderators #(map keywordize-mod %))
        (update :creation util/to-ISO-time)
        (dissoc :name :post-flairs))))

(defn update-sub
  "Update or add a sub to the subs in the db."
  [db {:keys [sid post-flairs user-attributes] :as new-sub}]
  (let [sub (fixup-sub new-sub)
        uid (get-in db [:current-user :uid])]
    (-> db
        (update :reldb rel/transact
                [:insert-or-replace :SubName
                 (select-keys new-sub [:sid :name])])
        (update :reldb rel/transact [:insert-or-replace :Sub sub])
        (update :reldb rel/transact [:insert-or-replace :ContentStatus
                                     {:content-type ::info
                                      :name (:name new-sub)
                                      :status :success
                                      :epoch (:epoch db)}])
        (internal/update-flairs sid post-flairs)
        (cond-> user-attributes
          (update :reldb rel/transact
                  [:insert-or-replace :SubUserAttributes
                   {:sid sid
                    :banned (:banned user-attributes)}]))
        (cond-> (and user-attributes (:user-flair user-attributes))
          (update :reldb rel/transact
                  [:insert-or-replace :SubUserFlair
                   {:sid sid
                    :uid uid
                    :text (:user-flair user-attributes)}])))))

(defn update-sub-user-flair
  "Update a user's flair in the subs in the db."
  [db sid uid new-flair]
  (if (nil? new-flair)
    (update db :reldb rel/transact
            [:delete :SubUserFlair
             [= :sid sid] [= :uid uid]])
    (update db :reldb rel/transact
            [:insert-or-replace :SubUserFlair
             {:sid sid :uid uid :text new-flair}])))

(loader/reg-loader ::info
  ;; :doc Load the sidebar info, flairs etc. for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [db]
     (when-let [sub (get-in db [:view :options :sub])]
       {:content-type ::info
        :name sub}))

   :load
   (fn-traced [{:keys [db]} _]
     (let [sub-name (get-in db [:view :options :sub])
           authenticated? (some? (get-in db [:current-user :uid]))
           variables {:name sub-name
                      :is_authenticated authenticated?}]
       {:fx [(if sub-name
               [:dispatch [::graphql/query {:graphql graphql
                                            :name :sub-info
                                            :variables variables
                                            :handler [::receive-info]}]]
               [:dispatch [::info-completed :success]])]}))

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           sub (:sub-by-name data)]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              data (update-sub sub))
        :fx [[:dispatch [::info-completed
                         (if (some? errors) :failure :success)]]]}))})

(loader/reg-loader ::submit-post-settings
  ;; :doc Load the sidebar info, flairs etc. for the current sub.
  ;; :doc Requires sub names to already be loaded.
  {:make-content-status
   (fn [_]
     {:content-type ::submit-post-settings})

   :load
   (fn-traced [_ _]
     {:fx [[:dispatch [::graphql/query
                       {:graphql graphql
                        :name :submit-post-settings
                        :variables {}
                        :handler [::receive-submit-post-settings]}]]]})

   :receive
   (fn-traced [{:keys [db]} [event {:keys [response]}]]
     (let [{:keys [data errors]} response
           settings (:site-configuration data)]
       {:db (cond-> db
              errors (errors/assoc-errors {:event event :errors errors})
              data (update :settings merge settings))
        :fx [[:dispatch [::submit-post-settings-completed
                         (if (some? errors) :failure :success)]]]}))})

(re-frame/reg-event-fx ::submit-post
  ;; :doc Submit a new post.
  ;; :doc Upload posts use graphql, other types use ajax.
  (fn-traced [_ [_ {:keys [form]}]]
    {:fx [[:dispatch #_(if (= :UPLOAD (:selected-post-type form))
                         [::submit-upload-step-one {:form form}]
                         [::submit-post-ajax form])
           [::submit-post-ajax form]]]}))

#_(re-frame/reg-event-fx ::submit-upload-step-one
    ;; :doc First step in submitting an upload post.
    ;; :doc Get a URL to upload to.
    (fn-traced [_ [_ {:keys [form]}]]
      (let [url (:uploaded-url form)]
        {:fx [(if url
                [:dispatch [::submit-upload-step-three {:form form
                                                        :uploaded-url url}]]
                [:dispatch [::graphql/mutate
                            {:graphql graphql
                             :name :create-upload-url
                             :variables {}
                             :handler [::submit-upload-step-two
                                       {:form form}]}]])]})))

#_(re-frame/reg-event-fx ::submit-upload-step-two
    ;; :doc Step two in submitting an upload post.
    ;; :doc Send the user's file to the upload URL.
    (fn-traced [{:keys [db]} [_ {:keys [form response]}]]
      (let [{:keys [data errors]} response
            url (:create-upload-url data)
            {:keys [id file]} form]
        (if errors
          {:db (-> db
                   (errors/assoc-errors {:event id :errors errors})
                   (forms/transition-after-response id))}
          {:fx [[::util/ajax-put
                 {:url url
                  :content-type (.-type file)
                  :payload file
                  :form form
                  :success-event [::submit-upload-step-three
                                  {:form form
                                   :uploaded-url url}]
                  :error-event [::submit-upload-step-two-error]}]]}))))

#_(re-frame/reg-event-db ::submit-upload-step-two-error
    (fn-traced [db [_ {:keys [form error-response]}]]
      (let [errors (errors/errors-from-ajax-error-response error-response)]
        (log/info {:errors errors} "File upload failed")
        (-> db
            (errors/assoc-errors {:event (:id form)
                                  :errors errors})
            (forms/transition-after-response (:id form))))))

#_(re-frame/reg-event-fx ::submit-upload-step-three
    (fn-traced [{:keys [db]} [_ {:keys [form uploaded-url]}]]
      (let [{:keys [id sub title selected-flair nsfw?]
             existing-uploaded-url :uploaded-url} form
            sid (internal/sid-from-name db sub)]
        (when-not (= existing-uploaded-url uploaded-url)
          (log/info {:url uploaded-url} "File upload successful"))
        {:db (update db :reldb rel/transact
                     [:update :FormState {:uploaded-url uploaded-url}
                      [= [:_ id] :id]])
         :fx [[:dispatch [::graphql/mutate
                          {:graphql graphql
                           :name :create-post
                           :id :create-post
                           :variables {:content nil
                                       :flair_id selected-flair
                                       :link uploaded-url
                                       :nsfw nsfw?
                                       :type :UPLOAD
                                       :title title
                                       :sid sid}
                           :handler [::receive-submit-upload-response
                                     {:form form}]}]]]})))

#_(re-frame/reg-event-fx ::receive-submit-upload-response
    (fn-traced [{:keys [db]} [_ {:keys [form response]}]]
      (let [{:keys [data errors]} response
            pid (-> data :create-post :pid)]
        {:db (-> db (errors/assoc-errors {:event (:id form) :errors errors})
                 (forms/transition-after-response (:id form)))
         :fx [(when pid
                [::routes/set-url (route-util/url-for db :sub/view-single-post
                                                      :sub (:sub form)
                                                      :pid pid)])]})))

(defn- describe-submit-post-error
  "Pass through an already translated error message from Python.
  If the message contains a url, construct the html for it."
  [db {:keys [message url]}]
  {:msg (if url
          (trmx-html db "This link was %{ref} on this circle."
                     {:ref [:a {:href url} (trx db "recently posted")]})
          message)})

(util/reg-ajax-form-event-chain ::submit-post-ajax
  ;; :doc Send a submit post form to the server and handle the response.
  {:call
   (fn-traced [{:keys [db]} [_ form]]
     (let [{:keys [sub title content link file selected-post-type
                   selected-flair nsfw? poll-options hide-results?
                   close-date close-poll?]} form
           ptype (-> selected-post-type name str/lower-case)
           valid-options (filter seq poll-options)
           append-options #(doall (map-indexed
                                   (fn [num opt]
                                     (.append % (str "options-" num) opt))
                                   valid-options))
           payload (doto (js/FormData.)
                     (.append "sub" sub)
                     (.append "title" title)
                     (.append "content" content)
                     (.append "link" link)
                     (.append "ptype" ptype)
                     (.append "flair" (or selected-flair ""))
                     (.append "nsfw" (str nsfw?))
                     (.append "hideresults" (str hide-results?))
                     (.append "closetime" (if close-poll?
                                            (.toISOString close-date) ""))
                     (.append "files" (or file ""))
                     append-options)]
       {:fx [[:dispatch
              [::util/ajax-csrf-formdata-post
               {:url (route-util/url-for db :subs/submit :ptype ptype)
                :form form
                :payload payload}]]]}))
   :success
   (fn-traced [{:keys [db]} [_ {:keys [form response]}]]
     (let [pid (:pid response)
           {:keys [id sub]} form]
       {:db (forms/transition-after-response db id)
        :fx [(when pid
               [::routes/set-url (route-util/url-for db :sub/view-single-post
                                                     :sub sub :pid pid)])]}))
   :failure
   (fn-traced [{:keys [db]} [_ {:keys [form error-response]}]]
     {:db (-> db
              (errors/assoc-errors
               {:event (:id form)
                :errors [(:response error-response)]
                :describe-error-fn (partial describe-submit-post-error db)})
              (forms/transition-after-response (:id form)))})})
