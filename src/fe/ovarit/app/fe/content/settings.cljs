;; fe/content/settings.cljs -- Site settings and configuration for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.settings
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [clojure.walk :as walk]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [goog.object]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes.table :as routing-table]
   [ovarit.app.fe.ui.page-title :as page-title]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]))

;; Import graphql queries.
(defgraphql graphql "graphql/site.graphql")

(re-frame/reg-event-fx ::load-software-licenses
  (fn-traced [{:keys [db]} [_ _]]
    (when (empty? (get-in db [:content :software-licenses]))
      {:dispatch [::graphql/query {:graphql graphql
                                   :name :software
                                   :variables {}
                                   :handler [::set-software-licenses]}]})))

(re-frame/reg-event-db ::set-software-licenses
  (fn-traced [db [_ {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event ::set-software-licenses :errors errors})
          (assoc-in [:content :software-licenses]
                    (get-in data [:site-configuration :software]))))))

(re-frame/reg-sub ::software-licenses
  (fn [db _]
    (get-in db [:content :software-licenses])))

(re-frame/reg-sub ::site-config
  ;; :doc Get the site configuration from the db.
  (fn [db _]
    (:settings db)))

(re-frame/reg-sub ::sub-prefix
  ;; :doc Extract the sub prefix to use in routes.
  :<- [::site-config]
  (fn [site-config _]
    (:sub-prefix site-config)))

(re-frame/reg-sub ::page-size
  ;; :doc Extract the page size.
  :<- [::site-config]
  (fn [site-config _]
    (:page-size site-config)))

(re-frame/reg-event-fx ::choose-new-motto
  ;; :doc Update the db with a randomly selected new motto.
  [(re-frame/inject-cofx ::motto-choice)]
  (fn-traced [{:keys [motto-choice db]} [_ _]]
    {:db (assoc-in db [:content :motto] motto-choice)}))

(re-frame/reg-cofx ::motto-choice
  ;; :doc Choose a new motto from the list fetched from the server.
  (fn [{:keys [db] :as cofx} _]
    (assoc cofx :motto-choice
           (let [mottos (get-in db [:settings :mottos])]
             (if (empty? mottos)
               ""
               (nth mottos (rand-int (count mottos))))))))

(re-frame/reg-sub ::motto
  ;; :doc Get the current motto.
  (fn [db _]
    (get-in db [:content :motto])))

(re-frame/reg-sub ::name
  ;; :doc Get the site name.
  :<- [::site-config]
  (fn [site-config _]
    (:name site-config)))

(re-frame/reg-sub ::lema
  ;; :doc Get the site slogan, called lema for some reason.
  :<- [::site-config]
  (fn [site-config _]
    (:lema site-config)))

(re-frame/reg-sub ::copyright
  ;; :doc Get the site copyright holder.
  :<- [::site-config]
  (fn [site-config _]
    (:copyright site-config)))

(re-frame/reg-sub ::footer-links
  ;; :doc Get the list of links to go in the page footer.
  :<- [::site-config]
  (fn [site-config _]
    (:footer site-config)))

(re-frame/reg-sub ::last-word
  ;; :doc Get the last word, to go at the bottom of the page.
  :<- [::site-config]
  (fn [site-config _]
    (:last-word site-config)))

(re-frame/reg-sub ::logo-svg
  ;; :doc Get the logo SVG.
  :<- [::site-config]
  (fn [site-config _]
    (:logo site-config)))

(defn- link-icon-info
  [elem]
  (try
    (when (= "icon" (.getAttribute elem "rel"))
      [(.getAttribute elem "sizes")
       (.getAttribute elem "href")])
    (catch js/Error _ nil)))

(re-frame/reg-cofx ::icons
  ;; :doc Extract the site icons from the HTML document.
  (fn [cofx]
    (let [elems (.getElementsByTagName js/document "link")
          icons (->> (goog.object/map elems link-icon-info)
                     js->clj
                     (map second)    ; JS gives us [index value].
                     (remove nil?)   ; Remove non-icons.
                     (filter first)  ; Remove missing sizes.
                     (into {}))]
      (assoc cofx :icons icons))))

(re-frame/reg-sub ::icons
  ;; :doc Extract the site icons from the db.
  :<- [::site-config]
  (fn [site-config _]
    (:icons site-config)))

(defn csrf-token
  "Get the CSRF token from an element in the HTML."
  []
  (let [csrf-elem (.getElementById js/document "csrf")]
    (if csrf-elem
      (.getAttribute csrf-elem "data-value")
      (do
        (js/console.log "CSRF token missing")
        ""))))

(re-frame/reg-cofx ::csrf-token
  ;; :doc Get the value of the CSRF token from an element in the HTML.
  (fn [cofx _]
    (assoc cofx :csrf-token (csrf-token))))

(defn manifest
  "Get the module manifest from an element in the html."
  []
  (let [script-elem (.getElementById js/document "scripts")]
    (if script-elem
      (-> (.getAttribute script-elem "data-value")
          util/parse-json
          walk/keywordize-keys)
      (do
        (js/console.log "Script manifest missing")
        ""))))

(re-frame/reg-cofx ::manifest
  ;; :doc Get the module manifest from an element in the HTML.
  (fn [cofx _]
    (assoc cofx :manifest (manifest))))

(defn re-graph-option-map
  "Construct the option map for initializing re-graph."
  ([]
   (re-graph-option-map (csrf-token)))
  ([csrf-token]
   {:http {:supported-operations #{:query
                                   :mutate}}
    :ws {:supported-operations #{:subscribe}
         :connection-init-payload {:token csrf-token}
         ;; Spread out the impact on the server of all clients
         ;; reconnecting after a service restart.
         :reconnect-timeout (+ 4000 (rand-int 3000))}}))

(re-frame/reg-event-fx ::load-html-head
  ;; :doc Get information from the HTML head into the db.
  [(re-frame/inject-cofx ::csrf-token)
   (re-frame/inject-cofx ::icons)
   (re-frame/inject-cofx ::manifest)]
  (fn-traced [{:keys [db csrf-token icons manifest]} [_ _]]
    {:db (-> db
             (assoc-in [:settings :csrf-token] csrf-token)
             (assoc-in [:settings :icons] icons)
             (assoc-in [:status :manifest] manifest))}))

(re-frame/reg-sub ::csrf-token
  ;; :doc Extract the CSRF token from the db.
  :<- [::site-config]
  (fn [site-config _]
    (:csrf-token site-config)))

(defn- cleanup-current-user
  "Tweak the current user object for inclusion in the db."
  [current-user enable-totp? now]
  (let [totp (get-in current-user [:attributes :totp-expiration])
        totp-expiration (when totp (js/parseInt totp))]
    (some-> current-user
            (dissoc :subs-moderated)
            (assoc-in [:attributes :totp-expiration] totp-expiration)
            (assoc-in [:attributes :is-admin]
                      (and (get-in current-user [:attributes :can-admin])
                           (or (not enable-totp?)
                               (and (some? totp-expiration)
                                    (> totp-expiration now)))))
            (update :subscriptions (fn [subscriptions]
                                     (map #(update % :status keyword)
                                          subscriptions))))))

(defn- prepare-subname [{:keys [sub]}]
  {:sid (:sid sub)
   :name (:name sub)})

(defn- prepare-submod [{:keys [sub moderation-level]} uid]
  {:sid (:sid sub)
   :uid uid
   :moderation-level (keyword moderation-level)})

(defn- prepare-notif-counts [{:keys [sub]}]
  (select-keys sub [:sid
                    :open-report-count
                    :unread-modmail-count
                    :new-unread-modmail-count
                    :in-progress-unread-modmail-count
                    :all-unread-modmail-count
                    :discussion-unread-modmail-count
                    :notification-unread-modmail-count]))

(defn assoc-subs-moderated
  [db uid subs-moderated]
  (let [subs-modded (map #(prepare-submod % uid) subs-moderated)
        sub-names (map prepare-subname subs-moderated)
        sub-notifs (map prepare-notif-counts subs-moderated)]
    (update db :reldb rel/transact
            (into [:insert-or-replace :SubName] sub-names)
            (into [:insert-or-replace :SubMod] subs-modded)
            (into [:insert-or-replace :SubModNotificationCount]
                  sub-notifs))))

(defn- assoc-user
  "Clean up the graphql user object and put it in the database."
  [db {:keys [name uid subs-moderated] :as user}]
  (let [enable-totp? (get-in db [:settings :enable-totp])]
    (-> db
        (assoc :current-user
               (cleanup-current-user user enable-totp? (:now db)))
        (assoc-subs-moderated uid subs-moderated)
        (cond-> uid
          (update :reldb rel/transact
                  [:insert-or-replace :User {:uid uid
                                             :name name
                                             :status :ACTIVE}])))))

(defn- assoc-index-query-results
  "Put results from the index query into the db."
  [db {:keys [data errors]}]
  (let [{:keys [site-configuration current-user default-subs
                funding-progress announcement-post-id]} data
        existing-sub-prefix (get-in db [:settings :sub-prefix])
        sub-prefix (or (:sub-prefix site-configuration) existing-sub-prefix)]
    (-> db
        (errors/assoc-errors {:event ::set-index-query-results :errors errors})
        (update :settings merge site-configuration)
        (assoc-user current-user)
        (assoc-in [:content :funding-progress] funding-progress)
        (assoc-in [:settings :default-subs] default-subs)
        (assoc :routes (routing-table/build-routing-table sub-prefix))
        (assoc-in [:content :announcement-pid] announcement-post-id))))

(re-frame/reg-cofx ::index-query-results
  ;; :doc Get the result of the index query from the HTML.
  (fn [cofx _]
    (let [elem (.getElementById js/document "data")]
      (assoc cofx :query-results
             (when elem
               (-> (.getAttribute elem "data-value")
                   util/parse-json
                   util/kebab-case-keys))))))

(re-frame/reg-event-fx ::load-index-query
  ;; :doc Load index query results.
  ;; :doc The server includes the result of the index-query
  ;; :doc in an element in index.html.
  [(re-frame/inject-cofx ::index-query-results)]
  (fn-traced [{:keys [db query-results]} [_ _]]
    (log/info {:index-query query-results} "loading index query")
    (let [db' (assoc-index-query-results db query-results)]
      {:db db'
       :fx [(when (str/blank? (get-in db' [:content :motto]))
              [:dispatch [::choose-new-motto]])
            [:dispatch [::util/start-markdown-renderer
                        {:sub-prefix (get-in db' [:settings :sub-prefix])}]]
            [:dispatch [::page-title/refresh]]
            (when (get-in db' [:settings :enable-totp])
              [:dispatch [::totp-countdown]])
            [::window/update-body-class db']]})))

(re-frame/reg-event-fx ::totp-countdown
  ;; :doc Update the admin TOTP time remaining and the active admin mode.
  ;; :doc Does nothing if the current user is not an admin.
  [(re-frame/inject-cofx ::window/now)]
  (fn-traced [{:keys [db now]} _]
    (let [current-user (:current-user db)
          {:keys [totp-expiration can-admin]} (:attributes current-user)
          db' (if (nil? current-user)
                db
                (-> db
                    (assoc-in [:current-user :attributes :is-admin]
                              (boolean (and can-admin totp-expiration
                                            (> totp-expiration now))))
                    (assoc-in [:current-user :attributes :totp-remaining]
                              (when totp-expiration
                                (- totp-expiration now)))))]
      {:db db'
       :fx [[::window/update-body-class db']
            (when-not config/debug?
              [:dispatch-later [{:ms 5000 :dispatch [::totp-countdown]}]])]})))

(re-frame/reg-event-fx
  ::init-subscriptions
  (fn-traced [{:keys [db]} [_ iframe?]]
    (let [status (get-in db [:status :subscriptions])]
      (when (= :not-initialized status)
        {:db (assoc-in db [:status :subscriptions] :initializing)
         ;; Allow time for the websocket client and server to
         ;; exchange the connection init payload before subscribing.
         ;; Pick a time large for a computer and small for a person.
         :fx [[:dispatch-later
               {:ms 200
                :dispatch [::start-subscriptions iframe?]}]]}))))

(re-frame/reg-event-fx ::start-subscriptions
  ;; :doc Start all GraphQL subscriptions.
  ;; :doc If `iframe?` is set, don't start the usual subscriptions.
  (fn-traced [{:keys [db]} [_ iframe?]]
    (let [authenticated? (get-in db [:current-user :uid])
          events (concat (when (and authenticated? (not iframe?))
                           [[::user/subscribe-notification-counts]
                            [::user/subscribe-subscription-updates]
                            [::subscribe-announcement-pid]])
                         (when authenticated?
                           (get-in db [:subscriptions :on-start])))]
      (log/debug {:event events} "Starting subscriptions")
      {:db (assoc-in db [:subscriptions :status] :ready)
       :fx (map (fn [ev] [:dispatch ev]) events)})))

(re-frame/reg-event-fx ::stop-subscriptions-on-routing
  ;; :doc Stop the GraphQL subscriptions on the stop-on-routing list.
  (fn-traced [{:keys [db]} [_ _]]
    (let [subscriptions (get-in db [:subscriptions :stop-on-routing])]
      (when (seq subscriptions)
        (log/info {:subscriptions subscriptions} "Unsubscribing"))
      {:db (assoc-in db [:subscriptions :stop-on-routing] [])
       :fx (map (fn [k] [:dispatch [::re-graph/unsubscribe {:id k}]])
                subscriptions)})))

(re-frame/reg-event-fx ::subscribe
  ;; :doc Send a GraphQL subscription event after the client is ready.
  (fn-traced [{:keys [db]} [_ event]]
    (let [ready? (= :ready (get-in db [:subscriptions :status]))]
      (log/debug {:ready ready?
                  :event event} "subscribe")
      {:db (if ready?
             db
             (update-in db [:subscriptions :on-start] conj event))
       :fx [(when ready?
              [:dispatch event])]})))

(re-frame/reg-event-fx ::subscribe-announcement-pid
  ;; :doc Subscribe to updates to the site-wide announcement post.
  (fn-traced [_ _]
    {:fx [[:dispatch [::graphql/subscribe
                      {:graphql graphql
                       :name :announcement-post-id
                       :variables {}
                       :handler [::receive-announcement-pid]}]]]}))

(re-frame/reg-event-db ::receive-announcement-pid
  ;; :doc Receive changes to the site-wide announcment post.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          pid (:announcement-post-id data)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          ;; An empty string indicates the announcement was un-announced.
          (assoc-in [:content :announcement-pid] (when (seq pid) pid))))))

(re-frame/reg-sub ::announcement-pid
  ;; :doc Extract the site-wide announcement post id.
  (fn [db _]
    (get-in db [:content :announcement-pid])))

(re-frame/reg-sub ::enable-totp?
  ;; :doc Extract whether TOTP is configured.
  :<- [::site-config]
  (fn [site-config _]
    (:enable-totp site-config)))

(re-frame/reg-sub ::totp-remaining
  ;; :doc Extract the TOTP countdown in ms.
  (fn [db _]
    (get-in db [:current-user :attributes :totp-remaining])))

(re-frame/reg-sub ::totp-time
  ;; :doc Format the TOTP countdown as HH:MM.
  :<- [::totp-remaining]
  (fn [ms _]
    (let [seconds (int (/ ms 1000))
          hours (int (/ seconds 3600))
          minutes (int (/ (- seconds (* hours 3600)) 60))]
      (str hours ":" (util/zero-pad minutes 2)))))

(re-frame/reg-event-db ::add-log-message
  ;; :doc Add a log record to the db.
  (fn-traced [db [_ log-record]]
    (update db :logs conj log-record)))

(re-frame/reg-sub ::logs
  ;; :doc Extract log records from the db.
  (fn [db _]
    (:logs db)))

(re-frame/reg-sub ::nsfw-anon-show?
  ;; :doc Extract whether NSFW content should be shown to anonymous users.
  :<- [::site-config]
  (fn [site-config _]
    (:nsfw-anon-show site-config)))

(re-frame/reg-sub ::nsfw-anon-blur?
  ;; :doc Extract whether NSFW content should be blurred for anonymous users.
  :<- [::site-config]
  (fn [site-config _]
    (:nsfw-anon-blur site-config)))

(re-frame/reg-sub ::upload-post-mime-types
  ;; :doc Return the set of acceptable mime types for upload posts.
  :<- [::site-config]
  (fn [{:keys [allow-video-uploads]} _]
    (set/union #{"image/jpeg" "image/png" "image/gif"}
               (when allow-video-uploads
                 #{"video/mp4" "video/webm"}))))

;; Fathom support

(re-frame/reg-cofx ::fathom-config
  ;; :doc Get the fathom config from the HTML.
  (fn [cofx _]
    (let [elem (.getElementById js/document "fathom")]
      (assoc cofx :fathom-config
             (when elem
               (-> (.getAttribute elem "data-value")
                   util/parse-json
                   util/kebab-case-keys))))))

(defn- on-fathom-load
  "When the fathom script is loaded, send it agoal."
  [goal]
  (.. js/window -fathom (trackGoal goal 1)))

(re-frame/reg-fx ::load-fathom
  ;; :doc Load the fathom script
  (fn-traced [{:keys [data-site goal ::authenticated?]}]
    (let [script-tag (. js/document (createElement "script"))
          on-load #(when authenticated?
                     (on-fathom-load goal))]
      (set! (. script-tag -src) "https://cdn.usefathom.com/script.js")
      (. script-tag (setAttribute "data-site" data-site))
      (. script-tag (setAttribute "data-spa" "auto"))
      (set! (. script-tag -onreadystatechange) on-load)
      (set! (. script-tag -onload) on-load)
      (. (. js/document -body) (appendChild script-tag)))))

(re-frame/reg-event-fx ::start-fathom
  [(re-frame/inject-cofx ::fathom-config) user/authenticated?]
  (fn-traced [{:keys [db fathom-config authenticated?]} [_ _]]
    {:db (assoc-in db [:settings :fathom] fathom-config)
     :fx [(when fathom-config
            [::load-fathom (assoc fathom-config
                                  ::authenticated? authenticated?)])]}))
