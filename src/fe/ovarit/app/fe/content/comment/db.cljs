;; fe/content/comment/db.cljs -- Comment reldb access for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.comment.db
  (:require
   [ovarit.app.fe.util.relic :as rel]))

(defn assoc-comment
  "Assoc a field in a comment object in the db."
  [db cid kw val]
  (update db :reldb rel/transact
          [:update :Comment #(assoc % kw val) [= :cid cid]]))

(defn update-comment
  "Update a comment object in the db."
  [db cid func]
  (update db :reldb
          rel/transact [:update :Comment func [= :cid cid]]))

(defn sort-from-sort-path
  "Get the sort field used in CommentNode from the sort-path."
  [sort-path]
  (if (= :direct (first sort-path))
    (second sort-path)
    (first sort-path)))

(defn query-comment-nodes
  "Get all comment nodes on the current page for `sort-path`."
  [reldb pid sort-path]
  (rel/q reldb [[:from :CommentNode]
                [:where [= :pid pid]]
                [:where [= :sort [:_ (sort-from-sort-path sort-path)]]]]))

(defn map-from-nodes
  "Convert a list of comment nodes into a map indexed by the cids."
  [nodes]
  (->> nodes
       (map (fn [{:keys [cid] :as node}]
              [cid node]))
       (into {})))

(defn descendant-cids
  "Return a list of the cids of the children of a node.
  Include the cid of the node itself.  If a filter function is
  supplied, only include cids of nodes that pass the filter."
  ([cid node-map]
   (descendant-cids cid node-map (constantly true)))
  ([cid node-map filter-fn]
   (let [node (get node-map cid)]
     (cond
       (not (filter-fn node)) []
       (empty? (:children node)) [cid]
       :else (-> (apply concat (map #(descendant-cids % node-map)
                                    (:children node)))
                 (conj cid))))))
