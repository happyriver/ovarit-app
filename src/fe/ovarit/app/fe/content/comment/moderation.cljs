;; fe/content/comment/moderation.cljs -- Comment moderation for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.comment.moderation
  (:require
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.comment.db :refer [assoc-comment
                                             descendant-cids
                                             map-from-nodes
                                             query-comment-nodes
                                             sort-from-sort-path
                                             update-comment]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.post :as-alias post]
   [ovarit.app.fe.content.settings :as-alias settings]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :as graphql]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.ui :as ui]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.moderation :as mod-forms]
   [ovarit.app.fe.user :as-alias user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]))

(re-frame/reg-event-db ::toggle-mod-delete-comment-form
  ;; :doc Toggle the display of the moderator delete comment reason form.
  (fn-traced [db [_ cid]]
    (-> db
        (mod-forms/initialize-delete-comment-form cid)
        (update-in [:ui-state :comment-modals :show-mod-delete-comment-form?]
                   not))))

(defn- hide-mod-delete-comment-form
  "Hide the moderator delete comment reason form."
  [db]
  (update-in db [:ui-state :comment-modals]
             dissoc :show-mod-delete-comment-form?))

(re-frame/reg-sub ::show-mod-delete-comment-form?
  ;; :doc Extract whether the moderator delete comment reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:comment-modals :show-mod-delete-comment-form?])))

(re-frame/reg-event-db ::toggle-mod-undelete-comment-form
  ;; :doc Toggle the display of the moderator undelete comment reason form.
  (fn-traced [db [_ cid]]
    (-> db
        (mod-forms/initialize-action-form :undelete-comment cid)
        (update-in [:ui-state :comment-modals :show-mod-undelete-comment-form?]
                   not))))

(defn- hide-mod-undelete-comment-form
  "Hide the moderator undelete comment reason form."
  [db]
  (update-in db [:ui-state :comment-modals]
             dissoc :show-mod-undelete-comment-form?))

(re-frame/reg-sub ::show-mod-undelete-comment-form?
  ;; :doc Extract whether the moderator undelete comment reason form is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (get-in state [:comment-modals :show-mod-undelete-comment-form?])))

(defn- existing-status-map [db cid delete-children?]
  (let [pid (get-in db [:view :options :pid])
        sort-path (get-in db [:ui-state :post :sort-path])
        node-map (-> (:reldb db)
                     (query-comment-nodes pid sort-path)
                     map-from-nodes)
        cids (if delete-children?
               (descendant-cids cid node-map :loaded?)
               [cid])]
    (->> cids
         (map (fn [cid]
                [cid (-> (:reldb db)
                         (rel/row :Comment [= :cid cid])
                         :status)]))
         (into {}))))

(re-frame/reg-event-fx ::mod-delete-comment
  ;; :doc Send a mod comment delete request to the server.
  ;; :doc Update statuses of child comments.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [cid reason delete-children? delete-children-reason]} form
          statuses (existing-status-map db cid delete-children?)
          deleted-status (if (internal/is-mod-of-sub? db)
                           :DELETED_BY_MOD
                           :DELETED_BY_ADMIN)
          restore-one (fn [db [cid status]]
                        (update db :reldb rel/transact
                                [:update :Comment
                                 #(assoc % :status status)
                                 [= :cid cid]]))
          restore #(reduce restore-one % statuses)
          delete-one (fn [db [cid status]]
                       (cond-> db
                         (= :ACTIVE status)
                         (update :reldb rel/transact
                                 [:update :Comment
                                  #(assoc % :status deleted-status)
                                  [= :cid cid]])))]
      {:db (reduce delete-one db statuses)
       :fx [[:dispatch
             [::util/ajax-csrf-form-post
              {:url (route-util/url-for db :do/delete-comment)
               :payload {:cid cid
                         :reason (str/trim reason)
                         :children delete-children?
                         :message (str/trim delete-children-reason)}
               :form form
               :restore restore
               :success-event [::receive-mod-delete-comment-response]
               :error-event
               [::receive-mod-delete-comment-error-response]}]]]})))

(re-frame/reg-event-db ::receive-mod-delete-comment-response
  ;; :doc Handle a server response to a mod delete comment request.
  (fn-traced [db [_ {:keys [form restore response]}]]
    (let [{:keys [status error]} response
          describe-error-fn (fn [err] {:msg (:message err)})]
      (if (= "ok" status)
        (hide-mod-delete-comment-form db)
        (-> db
            restore
            (errors/assoc-errors
             {:event (:id form)
              :errors (errors/errors-from-do-api error)
              :describe-error-fn describe-error-fn})
            (forms/transition-after-response (:id form)))))))

(re-frame/reg-event-db ::receive-mod-delete-comment-error-response
  ;; :doc Handle an error response to a mod delete comment request.
  (fn-traced [db [_ {:keys [form restore error-response]}]]
    (-> db
        restore
        (errors/assoc-errors
         {:event (:id form)
          :errors (errors/errors-from-ajax-error-response
                   error-response)})
        (forms/transition-after-response (:id form)))))

(re-frame/reg-event-fx ::mod-undelete-comment
  ;; :doc Send a mod comment undelete request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [cid reason]} form
          status (:status (rel/row (:reldb db) :Comment [= :cid cid]))
          set-status (fn [db val]
                       (assoc-comment db cid :status val))
          restore #(set-status % status)
          state {:form form
                 :restore restore}]
      {:db (set-status db :ACTIVE)
       :fx [[:dispatch
             [::util/ajax-csrf-form-post
              {:url (route-util/url-for db :do/undelete-comment)
               :payload {:cid cid
                         :reason reason}
               :success-event
               [::receive-mod-undelete-comment-response]
               :failure-event
               [::receive-mod-undelete-comment-error-response]}]]]})))

(re-frame/reg-event-db ::receive-mod-undelete-comment-response
  ;; :doc Handle a server response to a mod undelete comment request.
  (fn-traced [db [_ {:keys [form]}]]
    (-> db
        hide-mod-undelete-comment-form
        (forms/transition-after-response (:id form)))))

(re-frame/reg-event-db ::receive-mod-undelete-comment-error-response
  ;; :doc Handle an error response to a mod undelete comment request.
  (fn-traced [db [_ {:keys [form restore error-response]}]]
    (-> db
        restore
        (errors/assoc-errors
         {:event (:id form)
          :errors (errors/errors-from-ajax-error-response error-response)})
        (forms/transition-after-response (:id form)))))

(re-frame/reg-event-fx ::edit-distinguish
  ;; :doc Send a comment author distinguish request to the server.
  ;; :doc If distinguish is set, it will be cleared, otherwise
  ;; :doc it will be set according to the user's mod or admin status.
  ;; :doc If the user is both mod and admin, supply
  ;; :doc `distinguish-as` with the user's choice.
  (fn-traced [{:keys [db]} [event cid distinguish-as]]
    (let [{:keys [sub]} (get-in db [:view :options])
          is-mod? (internal/is-mod-of-sub? db)
          distinguish (:distinguish (rel/row (:reldb db) :Comment [= :cid cid]))
          new-distinguish (cond
                            (some? distinguish) nil
                            (= :admin distinguish-as) :ADMIN
                            (= :mod distinguish-as) :MOD
                            is-mod? :MOD
                            :else :ADMIN)
          set-distinguish (fn [db val]
                            (assoc-comment db cid :distinguish val))
          restore #(set-distinguish % distinguish)]
      {:db (set-distinguish db new-distinguish)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :distinguish-comment
                         :id (str :distinguish-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)
                                     :distinguish new-distinguish}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event
                                    :restore restore}]}]]]})))

(defn- update-sticky-flags
  "Set a new sticky comment and unset the old one.
  Either can be nil."
  [db existing-sticky-cid new-sticky-cid]
  (if (= new-sticky-cid existing-sticky-cid)
    (update-comment db new-sticky-cid
                    #(update % :sticky not))
    (cond-> db
      existing-sticky-cid (assoc-comment existing-sticky-cid :sticky false)
      new-sticky-cid      (assoc-comment new-sticky-cid :sticky true))))

(re-frame/reg-event-fx ::toggle-sticky
  ;; :doc Toggle the stickyness of a comment.
  ;; :doc If another comment is set sticky, unsticky it.
  ;; :doc Does not move the new sticky comment to the top of the list.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [pid (get-in db [:view :options :pid])
          existing-sticky-cid
          (-> (rel/row (:reldb db) :Comment [:and [= :pid pid] :sticky])
              :cid)

          restore #(update-sticky-flags % cid existing-sticky-cid)]
      {:db (update-sticky-flags db existing-sticky-cid cid)
       :fx [[:dispatch [::util/ajax-form-post
                        [(route-util/url-for db :do/set-sticky-comment
                                             :comment cid)
                         {}
                         nil
                         [::errors/receive-ajax-error ::toggle-sticky
                          restore]]]]]})))

(defn set-checkoff
  "Set the checkoff field of a commment."
  [db cid checkoff]
  (-> db
      (update :reldb rel/transact
              [:update :Comment #(assoc % :checkoff checkoff)
               [= :cid cid]])
      (assoc-in [:ui-state :comment cid :checked-off?] true)))

(re-frame/reg-event-fx ::check-off
  ;; :doc Check off a comment.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          username (get-in db [:current-user :name])
          restore #(set-checkoff db cid nil)
          state {:pid pid :cid cid :restore restore
                 :mutation-key :checkoff-comment}]
      {:db (set-checkoff db cid {:user {:name username}
                                 :time (:now db)})
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :checkoff-comment
                         :id (str :checkoff-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)}
                         :handler [::receive-checkoff state]}]]]})))

(re-frame/reg-event-fx ::uncheck
  ;; :doc Un-check a comment.
  (fn-traced [{:keys [db]} [_ cid]]
    (let [{:keys [sub pid]} (get-in db [:view :options])
          checkoff (:checkoff (rel/row (:reldb db) :Comment [= :cid cid]))
          restore #(set-checkoff db cid checkoff)
          state {:pid pid :cid cid :restore restore
                 :mutation-key :un-checkoff-comment}]
      {:db (set-checkoff db cid nil)
       :fx [[:dispatch [::graphql/mutate
                        {:graphql subs/graphql
                         :name :un-checkoff-comment
                         :id (str :un-checkoff-comment cid)
                         :variables {:cid cid
                                     :sid (internal/sid-from-name db sub)}
                         :handler [::receive-checkoff state]}]]]})))

(re-frame/reg-event-db ::receive-checkoff
  ;; :doc Receive the response from the checkoff or uncheckoff mutation.
  (fn-traced [db [event {:keys [cid restore mutation-key response]}]]
    (let [{:keys [data errors]} response
          result (get data mutation-key)
          checkoff (when result
                     (update result :time util/to-ISO-time))]
      (if errors
        (-> db
            (errors/assoc-errors {:event event :errors errors})
            restore)
        (set-checkoff db cid checkoff)))))
