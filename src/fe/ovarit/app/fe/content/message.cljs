;; fe/content/message.cljs -- Message content for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.message
  (:require
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.tr :refer [tr] :as tr]
   [ovarit.app.fe.ui.forms.core :as forms]
   [re-frame.core :as re-frame]))

;; Import graphql queries.
(defgraphql graphql "graphql/message.graphql")

(re-frame/reg-event-fx ::send-contact-mods
  ;; :doc Send a modmail message.  Or maybe any message.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [sub subject content]} form]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :create-message-to-mods
                         :variables {:subject subject
                                     :content content
                                     :sid (internal/sid-from-name db sub)}
                         :handler [::compose-response
                                   {:form form}]}]]]})))

(defn describe-error
  "Describe errors specific to message sending."
  [{:keys [message] :as error}]
  (let [description (tr "Could not send message:")]
    (cond
      (= message "Recipient not found")
      {:msg (str description " " (tr "recipient does not exist"))
       :status 400}

      (= message "Sender and recipient are the same")
      {:msg (str description " " (tr "you can't send a message to yourself"))
       :status 400}

      :else
      (errors/describe-error description error))))

(re-frame/reg-event-db ::compose-response
  ;; :doc Process the server's response to sending a new message.
  (fn-traced [db [_ {:keys [form response]}]]
    (-> db
        (errors/assoc-errors {:event ::compose-response
                              :errors (:errors response)
                              :describe-error-fn #(describe-error %)})
        (forms/transition-after-response (:id form)))))
