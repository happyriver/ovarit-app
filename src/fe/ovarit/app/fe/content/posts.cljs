;; fe/content/posts.cljs -- Post content for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.posts
  (:require
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.errors :refer [assoc-errors describe-error] :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.tr :refer [trx] :as tr]
   [re-frame.core :as re-frame])
  (:import [goog Uri]))

;; Import graphql queries.
(defgraphql graphql "graphql/posts.graphql")

(re-frame/reg-event-fx ::load-posts
  ;; :doc Make the query to get a list of posts.
  (fn-traced [{:keys [db]} [_ {:keys [post-source sort] :as options}]]
    (let [query-key (if (= post-source :all) :all-posts :default-posts)]
      {:db (assoc-in db [:status :content] :loading)
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql
                         :name query-key
                         :variables {:first 25 :sort_by (-> sort
                                                            name
                                                            str/upper-case
                                                            keyword)}
                         :handler [::set-posts options]}]]]})))

;; :single-sub
;; [:dispatch [::re-graph/query
;;             (get-query graphql :sub-by-name)
;;             {:name (:sub options)
;;              :first 25 :sort_by (-> sort
;;                                     name
;;                                     str/upper-case
;;                                     keyword)}
;;             [::set-posts options]]]

;; :user
;; [:dispatch [::re-graph/query
;;             (get-query graphql :user-by-name)
;;             {:name (:user options)
;;              :first 25 :sort_by (-> sort
;;                                     name
;;                                     str/upper-case
;;                                     keyword)}
;;             [::set-posts options]]]

(re-frame/reg-event-db ::set-posts
  ;; :doc Receive the results of the post list query and update the db.
  (fn-traced [db [_ {:keys [post-source sort response]}]]
    (let [{:keys [data errors]} response
          query-key (if (= post-source :all) :all-posts :default-posts)
          edges (get-in data [query-key :edges])
          ;; Convert the enum types from strings to keywords.
          fixed (map (fn [{:keys [node]}]
                       (-> node
                           (update :status keyword)
                           (update :type keyword)
                           (update :vote keyword)
                           (update :distinguish #(when % (keyword %)))))
                     edges)]
      (-> db
          (assoc-in [:status :content] :loaded)
          (assoc-in [:content :posts post-source sort] fixed)
          (assoc-errors {:event ::set-posts
                         :errors errors
                         :describe-error-fn
                         (partial describe-error
                                  (trx db "Error loading posts: "))})))))

(re-frame/reg-sub ::post-db
  ;; :doc Extract the post list.
  (fn [db _]
    (get-in db [:content :posts])))

(re-frame/reg-sub ::announcement
  ;; :doc Extract the announcement post.
  (fn [db _]
    (get-in db [:content :announcement])))

(re-frame/reg-sub ::post-list
  ;; :doc Get the specific post list that we want to show.

  :<- [::post-db]
  :<- [:view/post-source]
  :<- [:view/sort]
  (fn [[post-db post-source sort] _]
    (get-in post-db [post-source sort])))

(re-frame/reg-sub ::posts
  ;; :doc Enhance the post list with parsed domains.
  :<- [::post-list]
  (fn [post-list _]
    (map #(assoc % :domain (.getDomain (Uri. (:link %)))) post-list)))

;; (re-frame/reg-event-fx
;;  :load-posts
;;  (fn-traced [_ [_ url]]
;;    {:ajax-get [url #(re-frame/dispatch [:set-posts %])]}))

;; Pagination prototype
;; (re-frame/reg-event-fx
;;  :set-topbar-subs
;;  (fn-traced [{:keys [db]} [_ {:keys [data errors] :as payload}]]
;;    (let [next-page? (get-in data [:getSubs :pageInfo :hasNextPage])
;;          end-cursor (get-in data [:getSubs :pageInfo :endCursor])
;;          subs (map #(:node %) (get-in data [:getSubs :edges]))
;;          subs-so-far (concat (:topbar-subs db) subs)]
;;      {:db (assoc db :topbar-subs subs-so-far)
;;       :dispatch-n (list (when next-page?
;;                           [::re-graph/query
;;                            (queries/default-subs)
;;                            {:count 100 :after end-cursor}
;;                            [:set-topbar-subs]]))})))


(def graphics-formats
  "Graphics formats supported by expandos."
  [".png" ".jpg" ".gif" ".tiff" ".bmp" ".jpeg"])

(def video-formats
  "Graphics formats supported by expandos."
  [".mp4" ".webm" ".gifv"])

(re-frame/reg-sub ::post-expando-type
  ;; :doc Determine the type of expando to show, if any.
  :<- [::single-post]
  :<- [::single-post-domain]
  :<- [::settings/site-config]
  (fn [[{:keys [link]} domain {:keys [expando-sites]}] _]
    (cond
      ((set expando-sites) domain) "play"
      (some #(str/ends-with? link %) graphics-formats) "image"
      (some #(str/ends-with? link %) video-formats) "play")))
