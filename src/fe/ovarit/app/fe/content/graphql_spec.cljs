;; fe/content/graphql-spec.cljs -- Specs for the graphql queries
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.graphql-spec
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.spec.gen.alpha :as gen]))

;; The specs in this file should describe the expected results of the
;; graphql queries.  Their purpose is for generating test data and for
;; quickly flagging in development unexpected data coming from the
;; graphql server.

;;; Utility specs

(spec/def ::non-empty-string (spec/and string? seq))
(spec/def ::unix-timestamp
  (spec/with-gen (spec/and string? #(not= (js/parseInt %) ##NaN))
    #(gen/fmap (fn [num] (str (+ 1652387181715 num))) (spec/gen int?))))

;;; GraphQL

(spec/def ::scalar.Void (spec/nilable #(= "void" %)))

(spec/def ::type.ContentBlockType #{"HIDE" "BLUR"})
(spec/def ::type.CommentSortMethod #{"TOP" "NEW" "BEST"})
(spec/def ::type.DistinguishType (spec/nilable #{"MOD" "ADMIN"}))
(spec/def ::type.MessageType #{"USER_TO_USER"
                               "USER_TO_MODS"
                               "MOD_TO_USER_AS_USER"
                               "MOD_TO_USER_AS_MOD"
                               "MOD_DISCUSSION"
                               "USER_NOTIFICATION"
                               "MOD_NOTIFICATION"})
(spec/def ::type.MessageMailbox #{"INBOX"
                                  "SENT"
                                  "SAVED"
                                  "ARCHIVED"
                                  "TRASH"
                                  "DELETED"})
(spec/def ::type.ModerationLevel #{"OWNER" "MODERATOR" "JANITOR"})
(spec/def ::type.PostType #{"LINK" "POLL" "TEXT" "UPLOAD"})
(spec/def ::type.SubscriptionStatus #{"SUBSCRIBED" "BLOCKED"})
(spec/def ::type.Status #{"ACTIVE"
                          "DELETED"
                          "DELETED_BY_ADMIN"
                          "DELETED_BY_MOD"
                          "DELETED_BY_USER"})
(spec/def ::type.UserStatus #{"ACTIVE" "DELETED" "BANNED"})
(spec/def ::type.Vote #{"UP" "DOWN"})

(spec/def :type.Comment/cid ::non-empty-string)
(spec/def :type.Comment/content (spec/nilable string?))
(spec/def :type.Comment/edited (spec/nilable ::unix-timestamp))
(spec/def :type.Comment/score int?)
(spec/def :type.Comment/sticky boolean?)
(spec/def :type.Comment/upvotes (spec/nilable nat-int?))
(spec/def :type.Comment/downvotes (spec/nilable nat-int?))
(spec/def :type.Comment/vote (spec/nilable ::type.Vote))
(spec/def :type.Comment/status ::type.Status)
(spec/def :type.Comment/time ::unix-timestamp)
(spec/def :type.Comment/distinguish (spec/nilable ::type.DistinguishType))
(spec/def :type.Comment/author_flair (spec/nilable ::non-empty-string))
(spec/def :type.Comment/viewed boolean?)

(spec/def :type.CommentTree/tree_json string?)

(spec/def :type.ContentHistory/content string?)
(spec/def :type.ContentHistory/time ::unix-timestamp)

(spec/def :type.FooterLink/name string?)
(spec/def :type.FooterLink/link string?)

(spec/def :type.InviteCode/id ::non-empty-string)
(spec/def :type.InviteCode/code ::non-empty-string)
(spec/def :type.InviteCode/created_on ::unix-timestamp)
(spec/def :type.InviteCode/expires (spec/nilable ::unix-timestamp))
(spec/def :type.InviteCode/uses nat-int?)
(spec/def :type.InviteCode/max_uses nat-int?)

(spec/def :type.InviteCodeSettings/required boolean?)
(spec/def :type.InviteCodeSettings/visible boolean?)
(spec/def :type.InviteCodeSettings/minimum_level nat-int?)
(spec/def :type.InviteCodeSettings/per_user nat-int?)

(spec/def :type.Message/mid ::non-empty-string)
(spec/def :type.Message/subject ::non-empty-string)
(spec/def :type.Message/content ::non-empty-string)
(spec/def :type.Message/mtype ::type.MessageType)
(spec/def :type.Message/time ::unix-timestamp)
(spec/def :type.Message/first boolean?)
(spec/def :type.Message/unread boolean?)

(spec/def :type.MessageThread/id ::non-empty-string)
(spec/def :type.MessageThread/subject ::non-empty-string)
(spec/def :type.MessageThread/mailbox ::type.MessageMailbox)
(spec/def :type.MessageThread/reply_count nat-int?)

(spec/def :type.PollOption/id string?)
(spec/def :type.PollOption/text string?)
(spec/def :type.PollOption/votes (spec/nilable nat-int?))

(spec/def :type.Post/pid
  (spec/with-gen (spec/and string? #(re-matches #"[0-9]+" %))
    #(gen/fmap str (gen/int))))
(spec/def :type.Post/content (spec/nilable string?))
(spec/def :type.Post/status ::type.Status)
(spec/def :type.Post/link (spec/nilable ::non-empty-string))
(spec/def :type.Post/nsfw boolean?)
(spec/def :type.Post/posted ::unix-timestamp)
(spec/def :type.Post/edited (spec/nilable ::unix-timestamp))
(spec/def :type.Post/is_archived boolean?)
(spec/def :type.Post/type ::type.PostType)
(spec/def :type.Post/score int?)
(spec/def :type.Post/upvotes (spec/nilable int?))
(spec/def :type.Post/downvotes (spec/nilable int?))
(spec/def :type.Post/thumbnail (spec/nilable string?))
(spec/def :type.Post/slug (spec/nilable ::non-empty-string))
(spec/def :type.Post/title (spec/nilable ::non-empty-string))
(spec/def :type.Post/flair (spec/nilable ::non-empty-string))
(spec/def :type.Post/author_flair (spec/nilable ::non-empty-string))
(spec/def :type.Post/distinguish (spec/nilable ::type.DistinguishType))
(spec/def :type.Post/best_sort_enabled boolean?)
(spec/def :type.Post/default_sort ::type.CommentSortMethod)
(spec/def :type.Post/comment_count int?)
(spec/def :type.Post/sticky_cid (spec/nilable ::non-empty-string))
(spec/def :type.Post/locked boolean?)
(spec/def :type.Post/sticky boolean?)

(spec/def :type.PostUserAttributes/is_saved boolean?)
(spec/def :type.PostUserAttributes/poll_vote (spec/nilable string?))
(spec/def :type.PostUserAttributes/viewed (spec/nilable ::unix-timestamp))
(spec/def :type.PostUserAttributes/vote (spec/nilable ::type.Vote))

(spec/def :type.Sub/all_modmail_count nat-int?)
(spec/def :type.Sub/banned boolean?)
(spec/def :type.Sub/closed_report_count nat-int?)
(spec/def :type.Sub/creation ::unix-timestamp)
(spec/def :type.Sub/freeform_user_flairs boolean?)
(spec/def :type.Sub/name ::non-empty-string)
(spec/def :type.Sub/nsfw boolean?)
(spec/def :type.Sub/open_report_count nat-int?)
(spec/def :type.Sub/open_report_count nat-int?)
(spec/def :type.Sub/post_count nat-int?)
(spec/def :type.Sub/restricted boolean?)
(spec/def :type.Sub/sid ::non-empty-string)
(spec/def :type.Sub/sidebar (spec/nilable string?))
(spec/def :type.Sub/sub_banned_users_private boolean?)
(spec/def :type.Sub/sublog_private boolean?)
(spec/def :type.Sub/subscriber_count nat-int?)
(spec/def :type.Sub/text_post_min_length nat-int?)
(spec/def :type.Sub/title string?)
(spec/def :type.Sub/unread_modmail_count nat-int?)
(spec/def :type.Sub/unread_modmail_count nat-int?)
(spec/def :type.Sub/user_can_flair_self boolean?)
(spec/def :type.Sub/user_can_flair boolean?)
(spec/def :type.Sub/user_flair (spec/nilable ::non-empty-string))
(spec/def :type.Sub/user_flair_choices
  (spec/nilable (spec/coll-of string?)))
(spec/def :type.Sub/user_must_flair boolean?)

(spec/def :type.SubMessageLogEntry/action #{"CHANGE_MAILBOX"
                                            "HIGHLIGHT"
                                            "MOD_NOTIFICATION"
                                            "RELATED_REPORT"
                                            "RELATED_THREAD"})
(spec/def :type.SubMessageLogEntry/time ::unix-timestamp)
(spec/def :type.SubMessageLogHighlightChange/highlighted boolean?)
(spec/def :type.SubMessageLogMailboxChange/mailbox #{"INBOX" "ARCHIVED"})

(spec/def :type.SubPostTypeConfig/mods_only boolean?)
(spec/def :type.SubPostTypeConfig/post_type ::type.PostType)
(spec/def :type.SubPostTypeConfig/rules (spec/nilable string?))

(spec/def :type.SiteConfiguration/name ::non-empty-string)
(spec/def :type.SiteConfiguration/lema string?)
(spec/def :type.SiteConfiguration/description string?)
(spec/def :type.SiteConfiguration/sub_prefix ::non-empty-string)
(spec/def :type.SiteConfiguration/copyright string?)
(spec/def :type.SiteConfiguration/last_word string?)
(spec/def :type.SiteConfiguration/enable_totp boolean?)
(spec/def :type.SiteConfiguration/donate_presets (spec/* nat-int?))
(spec/def :type.SiteConfiguration/donate_minimum nat-int?)
(spec/def :type.SiteConfiguration/funding_goal nat-int?)
(spec/def :type.SiteConfiguration/languages (spec/coll-of string?))
(spec/def :type.SiteConfiguration/logo (spec/nilable string?))
(spec/def :type.SiteConfiguration/mottos (spec/* string?))
(spec/def :type.SiteConfiguration/force_sublog_public boolean?)
(spec/def :type.SiteConfiguration/nsfw_anon_blur boolean?)
(spec/def :type.SiteConfiguration/nsfw_anon_show boolean?)
(spec/def :type.SiteConfiguration/self_vote_comments boolean?)
(spec/def :type.SiteConfiguration/self_vote_posts boolean?)
(spec/def :type.SiteConfiguration/expando_sites
  (spec/coll-of string?))
(spec/def :type.SiteConfiguration/username_change_limit int?)
(spec/def :type.SiteConfiguration/link_post_remove_timeout int?)
(spec/def :type.SiteConfiguration/title_edit_timeout int?)
(spec/def :type.SiteConfiguration/admin_sub (spec/nilable string?))

(spec/def :type.SiteStats/users nat-int?)
(spec/def :type.SiteStats/subs nat-int?)
(spec/def :type.SiteStats/posts nat-int?)
(spec/def :type.SiteStats/comments nat-int?)
(spec/def :type.SiteStats/upvotes nat-int?)
(spec/def :type.SiteStats/downvotes nat-int?)
(spec/def :type.SiteStats/users_who_posted nat-int?)
(spec/def :type.SiteStats/users_who_commented nat-int?)
(spec/def :type.SiteStats/users_who_voted nat-int?)

(spec/def :type.SoftwareProject/name ::non-empty-string)
(spec/def :type.SoftwareProject/license_name ::non-empty-string)
(spec/def :type.SoftwareProject/license_link ::non-empty-string)
(spec/def :type.SoftwareProject/source_code_link ::non-empty-string)

(spec/def :type.SubModeration/moderation_level ::type.ModerationLevel)

(spec/def :type.SubPostFlair/id ::non-empty-string)
(spec/def :type.SubPostFlair/text ::non-empty-string)
(spec/def :type.SubPostFlair/mods_only boolean?)
(spec/def :type.SubPostFlair/post_types
  (spec/coll-of ::type.PostType))

(spec/def :type.SubRule/id ::non-empty-string)
(spec/def :type.SubRule/text ::non-empty-string)

(spec/def :type.Subscription/sid :type.Sub/sid)
(spec/def :type.Subscription/name :type.Sub/name)
(spec/def :type.Subscription/order (spec/nilable int?))
(spec/def :type.Subscription/status (spec/nilable ::type.SubscriptionStatus))

(spec/def :type.VisitorCount/start ::unix-timestamp)
(spec/def :type.VisitorCount/count nat-int?)

(spec/def :type.Vote/direction ::type.Vote)
(spec/def :type.Vote/datetime ::unix-timestamp)

(spec/def :type.User/name (spec/nilable ::non-empty-string))
(spec/def :type.User/uid ::non-empty-string)
(spec/def :type.User/status ::type.UserStatus)
(spec/def :type.User/language (spec/nilable string?))
(spec/def :type.User/score int?)
(spec/def :type.User/unread_message_count nat-int?)
(spec/def :type.User/unread_notification_count nat-int?)
(spec/def :type.User/joindate ::unix-timestamp)
(spec/def :type.User/level int?)
(spec/def :type.User/given int?)
(spec/def :type.User/upvotes_given int?)
(spec/def :type.User/downvotes_given int?)

(spec/def :type.UserAttributes/can_admin boolean?)
(spec/def :type.UserAttributes/totp_expiration (spec/nilable ::unix-timestamp))
(spec/def :type.UserAttributes/enable_collapse_bar boolean?)
(spec/def :type.UserAttributes/block_dms boolean?)
(spec/def :type.UserAttributes/lab_rat boolean?)
(spec/def :type.UserAttributes/nsfw boolean?)
(spec/def :type.UserAttributes/nsfw_blur boolean?)

(spec/def :type.UserNameStringBan/banned ::non-empty-string)
(spec/def :type.UserNameStringBan/users (spec/* :type.User/name))

(spec/def :type.UserNameHistory/changed ::unix-timestamp)
(spec/def :type.UserNameHistory/required_by_admin boolean?)

;;; Common fragments

(spec/def :fragment.pageInfo/hasNextPage boolean?)
(spec/def :fragment.pageInfo/endCursor ::non-empty-string)
(spec/def :fragment/pageInfo
  (spec/keys :req-un [:fragment.pageInfo/hasNextPage
                      :fragment.pageInfo/endCursor]))

(spec/def :fragment.bidirectional-pageInfo/hasPreviousPage boolean?)
(spec/def :fragment.bidirectional-pageInfo/startCursor ::non-empty-string)
(spec/def :fragment/bidirectional-pageInfo
  (spec/keys :req-un [:fragment.pageInfo/hasNextPage
                      :fragment.pageInfo/endCursor
                      :fragment.bidirectional-pageInfo/hasPreviousPage
                      :fragment.bidirectional-pageInfo/startCursor]))

;;; admin.graphql

(spec/def :query.stats-all-time/site_stats
  (spec/keys :req-un [:type.SiteStats/users
                      :type.SiteStats/subs
                      :type.SiteStats/posts
                      :type.SiteStats/comments
                      :type.SiteStats/upvotes
                      :type.SiteStats/downvotes]))
(spec/def ::query.stats-all-time
  (spec/keys :req-un [:query.stats-all-time/site_stats]))

(spec/def :query.stats-timespan/site_stats
  (spec/keys :req-un [:type.SiteStats/users
                      :type.SiteStats/subs
                      :type.SiteStats/posts
                      :type.SiteStats/comments
                      :type.SiteStats/upvotes
                      :type.SiteStats/downvotes
                      :type.SiteStats/users_who_posted
                      :type.SiteStats/users_who_commented
                      :type.SiteStats/users_who_voted]))
(spec/def ::query.stats-timespan
  (spec/keys :req-un [:query.stats-timespan/site_stats]))

(spec/def :query.visitor-counts/site_visitor_counts
  (spec/* (spec/keys :req-un [:type.VisitorCount/start
                              :type.VisitorCount/count])))
(spec/def ::query.visitor-counts
  (spec/keys :req-un [:query.visitor-counts/site_visitor_counts]))

(spec/def :query.banned-username-strings/banned_username_strings
  (spec/* (spec/keys :req-un [:type.UserNameStringBan/banned
                              :type.UserNameStringBan/users])))
(spec/def ::query.banned-username-strings
  (spec/keys :req-un [:query.banned-username-strings/banned_username_strings]))

(spec/def :mutation.ban-string-in-usernames/ban_string_in_usernames
  (spec/keys :req-un [:type.UserNameStringBan/banned
                      :type.UserNameStringBan/users]))
(spec/def ::mutation.ban-string-in-usernames
  (spec/keys :req-un [:mutation.ban-string-in-usernames/ban_string_in_usernames]))

(spec/def :mutation.unban-string-in-usernames/unban_string_in_usernames
  :type.UserNameStringBan/banned)
(spec/def ::mutation.unban-string-in-usernames
  (spec/keys :req-un [:mutation.unban-string-in-usernames/unban_string_in_usernames]))

(spec/def :fragment.InviteCodeInfo/created_by
  (spec/keys :req-un [:type.User/name
                      :type.User/status]))
(spec/def :fragment.InviteCodeInfo/used_by
  (spec/* (spec/keys :req-un [:type.User/name
                              :type.User/status])))
(spec/def ::fragment.InviteCodeInfo
  (spec/keys :req-un [:type.InviteCode/id
                      :type.InviteCode/code
                      :fragment.InviteCodeInfo/created_by
                      :type.InviteCode/created_on
                      :type.InviteCode/expires
                      :fragment.InviteCodeInfo/used_by
                      :type.InviteCode/uses
                      :type.InviteCode/max_uses]))

(spec/def :fragment.InviteCodeListing.edges/node ::fragment.InviteCodeInfo)
(spec/def :fragment.InviteCodeListing/edges
  (spec/* (spec/keys :req-un [:fragment.InviteCodeListing.edges/node])))
(spec/def ::fragment.InviteCodeListing
  (spec/nilable
   (spec/keys :req-un [:fragment.InviteCodeListing/edges
                       :fragment.pageInfo/pageInfo])))

(spec/def :query.invite-codes-and-settings/invite_code_settings
  (spec/keys :req-un [:type.InviteCodeSettings/required
                      :type.InviteCodeSettings/visible
                      :type.InviteCodeSettings/minimum_level
                      :type.InviteCodeSettings/per_user]))
(spec/def :query.invite-codes-and-settings/invite_codes
  ::fragment.InviteCodeListing)
(spec/def ::query.invite-codes-and-settings
  (spec/keys :req-un [:query.invite-codes-and-settings/invite_code_settings
                      :query.invite-codes-and-settings/invite_codes]))

(spec/def :query.invite-codes/invite_codes ::fragment.InviteCodeListing)
(spec/def ::query.invite-codes
  (spec/keys :req-un [:query.invite-codes/invite_codes]))

(spec/def :mutation.invite-code-settings/set_invite_code_settings
  (spec/keys :req-un [:type.InviteCodeSettings/required
                      :type.InviteCodeSettings/visible
                      :type.InviteCodeSettings/minimum_level
                      :type.InviteCodeSettings/per_user]))
(spec/def ::mutation.invite-code-settings
  (spec/keys :req-un [:mutation.invite-code-settings/set_invite_code_settings]))

(spec/def :mutation.generate-invite-code/generate_invite_code
  ::fragment.InviteCodeInfo)
(spec/def ::mutation.generate-invite-code
  (spec/keys :req-un [:mutation.generate-invite-code/generate_invite_code]))

(spec/def :mutation.expire-invite-codes/expire_invite_codes
  (spec/* :type.InviteCode/id))
(spec/def ::mutation.expire-invite-codes
  (spec/keys :req-un [:mutation.expire-invite-codes/expire_invite_codes]))

(spec/def :query.votes/user_by_name
  (spec/keys :req-un [:type.User/uid
                      :type.User/status
                      :type.User/joindate
                      :type.User/score
                      :type.User/level
                      :type.User/upvotes_given
                      :type.User/downvotes_given]))
(spec/def :query.votes.edges.node.post/sub
  (spec/keys :req-un [:type.Sub/name]))
(spec/def :query.votes.edges.node.post-or-comment/author
  (spec/keys :req-un [:type.User/uid
                      :type.User/name
                      :type.User/status]))
(spec/def :query.votes.edges.node/post
  (spec/keys :req-un [:type.Post/pid
                      :type.Post/slug
                      :type.Post/sub
                      :query.votes.edges.node.post-or-comment/author]))
(spec/def :query.votes.edges.node.comment/post
  (spec/keys :req-un [:type.Post/pid
                      :type.Post/slug
                      :type.Post/sub]))
(spec/def :query.votes.edges.node/comment
  (spec/keys :req-un [:type.Comment/cid
                      :query.votes.edges.node.comment/post
                      :query.votes.edges.node.post-or-comment/author]))
(spec/def :query.votes.edges/node
  (spec/keys :opt-un [:query.votes.edges.node/post
                      :query.votes.edges.node/comment]
             :req-un [:type.Vote/direction
                      :type.Vote/datetime]))
(spec/def :query.votes.edges/cursor ::non-empty-string)
(spec/def :query.votes/edges
  (spec/coll-of (spec/keys :req-un [:query.votes.edges/node
                                    :query.votes.edges/cursor])))
(spec/def :query.votes/votes
  (spec/nilable
   (spec/keys :req-un [:query.votes/edges
                       :fragment.bidirectional-pageInfo/pageInfo])))
(spec/def ::query.votes
  (spec/keys :req-un [:query.votes/votes
                      :query.votes/user_by_name]))

(spec/def :mutation.start-remove-votes/start_remove_votes nil?)
(spec/def ::mutation.start-remove-votes
  (spec/keys :req-un [:mutation.start-remove-votes/start_remove_votes]))

(spec/def :mutation.stop-remove-votes/stop_remove_votes nil?)
(spec/def ::mutation.stop-remove-votes
  (spec/keys :req-un [:mutation.stop-remove-votes/stop_remove_votes]))

(spec/def :subscription.vote-removal-update/total (spec/nilable int?))
(spec/def :subscription.vote-removal-update/remaining  (spec/nilable int?))
(spec/def :subscription.vote-removal-update/vote_removal_update
  (spec/keys :req-un [:subscription.vote-removal-update/total
                      :subscription.vote-removal-update/remaining]))
(spec/def ::subscription.vote-removal-update
  (spec/keys :req-un [:subscription.vote-removal-update/vote_removal_update]))

(spec/def :query.required-name-change.user-by-name/username_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.User/name
                        :type.UserNameHistory/changed
                        :type.UserNameHistory/required_by_admin]))))
(spec/def :query.required-name-change/user_by_name
  (spec/keys :req-un [:type.User/uid
                      :query.required-name-change.user-by-name/username_history]))
(spec/def :query.required-name-change.required-name-change/message string?)
(spec/def :query.required-name-change.required-name-change/admin
  (spec/keys :req-un [:type.User/name]))
(spec/def :query.required-name-change.required-name-change/required_at
  ::unix-timestamp)
(spec/def :query.required-name-change/required_name_change
  (spec/nilable
   (spec/keys :req-un
              [:query.required-name-change.required-name-change/message
               :query.required-name-change.required-name-change/admin
               :query.required-name-change.required-name-change/required_at])))
(spec/def ::query.required-name-change
  (spec/keys :req-un [:query.required-name-change/user_by_name
                      :query.required-name-change/required_name_change]))

(spec/def :mutation.require-name-change.require-name-change/message string?)
(spec/def :mutation.require-name-change.require-name-change/admin
  (spec/keys :req-un [:type.User/name]))
(spec/def :mutation.require-name-change.require-name-change/required_at
  ::unix-timestamp)
(spec/def :mutation.require-name-change/require_name_change
  (spec/nilable
   (spec/keys :req-un
              [:mutation.require-name-change.require-name-change/message
               :mutation.require-name-change.require-name-change/admin
               :mutation.require-name-change.require-name-change/required_at])))
(spec/def ::mutation.require-name-change
  (spec/keys :req-un [:mutation.require-name-change/require_name_change]))

(spec/def :mutation.cancel-required-name-change/cancel_required_name_change
  ::scalar.Void)
(spec/def ::mutation.cancel-required-name-change
  (spec/keys :req-un
             [:mutation.cancel-required-name-change/cancel_required_name_change]))


;;; donate.graphql

(spec/def :subscription.funding-progress/funding_progress int?)
(spec/def ::subscription.funding-progress
  (spec/keys :req-un [:subscription.funding-progress/funding_progress]))

;;; messages.graphql

(spec/def :fragment.UserInfo.subs_moderated/sub
  (spec/keys :req-un [:type.Sub/sid]))
(spec/def :fragment.UserInfo/subs_moderated
  (spec/* (spec/keys :req-un [:fragment.UserInfo.subs_moderated/sub])))
(spec/def :fragment/UserInfo
  (spec/keys :req-un [:type.User/uid
                      :type.User/name
                      :type.User/status
                      :fragment.UserInfo/subs_moderated]))

(comment (gen/sample (spec/gen :fragment/UserInfo)))

(spec/def :fragment.MessageFields/receiver (spec/nilable :fragment/UserInfo))
(spec/def :fragment.MessageFields/sender (spec/nilable :fragment/UserInfo))
(spec/def :fragment/MessageFields
  (spec/and
   (spec/keys :req-un [:type.Message/mid
                       :type.Message/mtype
                       :type.Message/content
                       :type.Message/time
                       :fragment.MessageFields/receiver
                       :fragment.MessageFields/sender
                       :type.Message/unread])
   (fn [{:keys [mtype sender receiver]}]
     (case mtype
       "USER_TO_USER"        (and (some? sender) (some? receiver))
       "USER_TO_MODS"        (and (some? sender) (nil? receiver))
       "MOD_TO_USER_AS_USER" (and (some? sender) (some? receiver))
       ;; MOD_TO_USER_AS_MOD should have receiver set, but some older messages
       ;; do not. Whatever bug was causing that seems to be fixed now.
       "MOD_TO_USER_AS_MOD"  (some? sender)
       "MOD_DISCUSSION"      (and (some? sender) (nil? receiver))
       "USER_NOTIFICATION"   (and (some? sender) (some? receiver))
       "MOD_NOTIFICATION"    (and (some? sender) (nil? receiver))))))

(comment (gen/sample (spec/gen :fragment/MessageFields)))

(spec/def :mutation.create-modmail-message/thread_id
  :type.MessageThread/id)
(spec/def :mutation/create_modmail_message
  (spec/keys :req-un [:type.Message/mid
                      :mutation.create-modmail-message/thread_id]))
(spec/def ::mutation.create-modmail-message
  (spec/keys :req-un [:mutation/create_modmail_message]))

(spec/def :mutation/create_modmail_reply :fragment/MessageFields)
(spec/def ::mutation.create-modmail-reply
  (spec/keys :req-un [:mutation/create_modmail_reply]))

(spec/def :mutation/create_message_to_mods
  (spec/keys :req-un [:type.Message/mid]))
(spec/def ::mutation.create-message-to-mods
  (spec/keys :req-un [:mutation/create_message_to_mods]))

(spec/def :query.get-modmail-category/sub
  (spec/keys :req-un [:type.Sub/sid
                      :type.Sub/name]))
(spec/def :query.get-modmail-category/mailbox #{"INBOX" "ARCHIVED"})
(spec/def :query.get-modmail-category/first_message :fragment/MessageFields)
(spec/def :query.get-modmail-category/latest_message :fragment/MessageFields)
(spec/def :query.get-modmail-category/node
  (spec/keys :req-un [:type.MessageThread/id
                      :query.get-modmail-category/sub
                      :type.MessageThread/subject
                      :type.MessageThread/mailbox
                      :type.MessageThread/reply_count
                      :query.get-modmail-category/first_message
                      :query.get-modmail-category/latest_message]))
(spec/def :query.get-modmail-category/edges
  (spec/* (spec/keys :req-un [:query.get-modmail-category/node])))
(spec/def :query.get-modmail-category/modmail_threads
  (spec/nilable
   (spec/keys :req-un [:query.get-modmail-category/edges
                       :fragment/pageInfo])))
(spec/def ::query.get-modmail-category
  (spec/keys :req-un [:query.get-modmail-category/modmail_threads]))

(spec/def :fragment.MessageSummary/node :fragment/MessageFields)
(spec/def :fragment.MessageSummary/edges
  (spec/* (spec/keys :req-un [:fragment.MessageSummary/node])))
(spec/def :fragment/MessageSummary
  (spec/keys :req-un [:fragment.MessageSummary/edges
                      :fragment/pageInfo]))

(comment (gen/sample (spec/gen :fragment/MessageSummary)))

(spec/def :query.get-modmail-thread-by-id/sub
  (spec/keys :req-un [:type.Sub/sid
                      :type.Sub/name]))
(spec/def :query.get-modmail-thread-by-id/first_message :fragment/MessageFields)
(spec/def :query.get-modmail-thread-by-id/mailbox #{"INBOX" "ARCHIVED"})
(spec/def :query.get-modmail-thread-by-id.log/user :fragment/UserInfo)
(spec/def :query.get-modmail-thread-by-id.log/report_id ::non-empty-string)
(spec/def :query.get-modmail-thread-by-id.log/rtype #{"POST" "COMMENT"})
(spec/def :query.get-modmail-thread-by-id.log/related_thread_id
  ::non-empty-string)
(spec/def :query.get-modmail-thread-by-id.log/notification_type
  #{"DOWNVOTE" "DM_BLOCKING_ABUSE" "REQUIRED_NAME_CHANGE"})
(spec/def :query.get-modmail-thread-by-id.log/reference_type
  #{"NOTIFICATION" "NEW"})
(spec/def :query.get-modmail-thread-by-id/log
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.SubMessageLogEntry/action
                        :query.get-modmail-thread-by-id.log/user
                        :type.SubMessageLogEntry/time]
               :opt-un [:type.SubMessageLogMailboxChange/mailbox
                        :type.SubMessageLogHighlightChange/highlighted
                        :query.get-modmail-thread-by-id.log/report_id
                        :query.get-modmail-thread-by-id.log/rtype
                        :query.get-modmail-thread-by-id.log/notification_type
                        :query.get-modmail-thread-by-id.log/related_thread_id
                        :query.get-modmail-thread-by-id.log/reference_type]))))
(spec/def :query.get-modmail-thread_by-id/message_thread_by_id
  (spec/keys :req-un [:type.MessageThread/id
                      :query.get-modmail-thread-by-id/sub
                      :type.MessageThread/subject
                      :query.get-modmail-thread-by-id/mailbox
                      :type.MessageThread/reply_count
                      :query.get-modmail-thread-by-id/first_message
                      :query.get-modmail-thread-by-id/log]))
(spec/def :query.get-modmail-thread-by-id/modmail_thread :fragment/MessageSummary)
(spec/def ::query.get-modmail-thread-by-id
  (spec/keys :req-un [:query.get-modmail-thread_by-id/message_thread_by_id
                      :query.get-modmail-thread-by-id/modmail_thread]))

(spec/def :query.get-single-message/message_by_mid :fragment/MessageFields)
(spec/def ::query.get-single-message
  (spec/keys :req-un [:query.get-single-message/message_by_mid]))

(spec/def :mutation/update_message_unread :type.Message/mid)
(spec/def ::mutation.update-message-unread
  (spec/keys :req-un [:mutation/update_message_unread]))

(spec/def :mutation/update_thread_unread :type.MessageThread/id)
(spec/def ::mutation.update-thread-unread
  (spec/keys :req-un [:mutation/update_thread_unread]))

(spec/def :mutation.update-modmail-mailbox/update_modmail_mailbox
  (spec/keys :req-un [:type.MessageThread/id]))
(spec/def ::mutation.update-modmail-mailbox
  (spec/keys :req-un [:mutation.update-modmail-mailbox/update_modmail_mailbox]))

;;; site.graphql

(spec/def :query.software.site_configuration/software
  (spec/* (spec/keys :req-un [:type.SoftwareProject/name
                              :type.SoftwareProject/license_name
                              :type.SoftwareProject/license_link
                              :type.SoftwareProject/source_code_link])))
(spec/def :query.software/site_configuration
  (spec/keys :req-un [:query.software.site_configuration/software]))
(spec/def ::query.software
  (spec/keys :req-un [:query.software/site_configuration]))

(spec/def :fragment.CurrentUserInfo/attributes
  (spec/keys :req-un [:type.UserAttributes/can_admin
                      :type.UserAttributes/totp_expiration
                      :type.UserAttributes/lab_rat
                      :type.UserAttributes/nsfw
                      :type.UserAttributes/nsfw_blur
                      :type.UserAttributes/enable_collapse_bar]))
(spec/def :fragment.CurrentUserInfo.subscriptions/status
  ::type.SubscriptionStatus)
(spec/def :fragment.CurrentUserInfo/subscriptions
  (spec/* (spec/keys :req-un [:type.Subscription/name
                              :fragment.CurrentUserInfo.subscriptions/status
                              :type.Subscription/order])))
(spec/def :fragment.CurrentUserInfo.subs_moderated/sub
  (spec/keys :req-un [:type.Sub/name
                      :type.Sub/sid
                      :type.Sub/open_report_count
                      :type.Sub/unread_modmail_count]))
(spec/def :fragment.CurrentUserInfo/subs_moderated
  (spec/* (spec/keys :req-un [:fragment.CurrentUserInfo.subs_moderated/sub
                              :type.SubModeration/moderation_level])))
(spec/def ::fragment.CurrentUserInfo
  (spec/keys :req-un [:type.User/name
                      :type.User/uid
                      :type.User/language
                      :type.User/score
                      :fragment.CurrentUserInfo/attributes
                      :fragment.CurrentUserInfo/subscriptions
                      :fragment.CurrentUserInfo/subs_moderated
                      :type.User/unread_message_count
                      :type.User/unread_notification_count]))

(spec/def :query.index-query/current_user
  (spec/nilable ::fragment.CurrentUserInfo))

(spec/def :query.index-query.site-configuration/footer
  (spec/* (spec/keys :req-un [:type.FooterLink/name
                              :type.FooterLink/link])))
(spec/def :query.index-query/site_configuration
  (spec/keys :req-un [:type.SiteConfiguration/name
                      :type.SiteConfiguration/lema
                      :type.SiteConfiguration/description
                      :type.SiteConfiguration/sub_prefix
                      :type.SiteConfiguration/copyright
                      :query.index-query.site-configuration/footer
                      :type.SiteConfiguration/last_word
                      :type.SiteConfiguration/enable_totp
                      :type.SiteConfiguration/donate_presets
                      :type.SiteConfiguration/donate_minimum
                      :type.SiteConfiguration/funding_goal
                      :type.SiteConfiguration/logo
                      :type.SiteConfiguration/mottos
                      :type.SiteConfiguration/force_sublog_public
                      :type.SiteConfiguration/nsfw_anon_blur
                      :type.SiteConfiguration/nsfw_anon_show
                      :type.SiteConfiguration/self_vote_comments
                      :type.SiteConfiguration/self_vote_posts
                      :type.SiteConfiguration/expando_sites
                      :type.SiteConfiguration/link_post_remove_timeout
                      :type.SiteConfiguration/title_edit_timeout
                      :type.SiteConfiguration/admin_sub]))
(spec/def :query.index-query.default-subs/name :type.Sub/name)
(spec/def :query.index-query/default_subs
  (spec/* (spec/keys :req-un [:type.Subscription/name])))
(spec/def :query.index-query/funding_progress (spec/nilable nat-int?))
(spec/def :query.index-query/announcement_post_id (spec/nilable :type.Post/pid))
(spec/def ::query.index-query
  (spec/keys :req-un [:query.index-query/current_user
                      :query.index-query/site_configuration
                      :query.index-query/default_subs
                      :query.index-query/funding_progress
                      :query.index-query/announcement_post_id]))

(spec/def :query.current-user/current_user
  (spec/nilable ::fragment.CurrentUserInfo))
(spec/def ::query.current-user
  (spec/keys :req-un [:query.index-query/current_user]))

(spec/def :subscription.announcement-post-id/announcement_post_id
  string?)
(spec/def ::subscription.announcement-post-id
  (spec/keys :req-un [:subscription.announcement-post-id/announcement_post_id]))

(spec/def :query.registration-settings.current-user/uid
  (spec/nilable :type.User/uid))
(spec/def :query.registration-settings/current_user
  (spec/nilable
   (spec/keys :req-un [:query.registration-settings.current-user/uid])))
(spec/def :query.registration-settings.site-configuration/registration_enabled
  boolean?)
(spec/def :query.registration-settings.site-configuration/password_length
  nat-int?)
(spec/def :query.registration-settings.site-configuration/password_complexity
  nat-int?)
(spec/def :query.registration-settings.site-configuration/username_max_length
  nat-int?)
(spec/def :query.registration-settings/site_configuration
  (spec/keys :req-un [:query.registration-settings.site-configuration/registration_enabled
                      :query.registration-settings.site-configuration/password_length
                      :query.registration-settings.site-configuration/password_complexity
                      :query.registration-settings.site-configuration/username_max_length]))
(spec/def :query.registration-settings.invite-code-settings/required
  boolean?)
(spec/def :query.registration-settings/invite_code_settings
  (spec/keys :req-un [:query.registration-settings.invite-code-settings/required]))
(spec/def ::query.registration-settings
  (spec/keys :req-un [:query.registration-settings/current_user
                      :query.registration-settings/site_configuration
                      :query.registration-settings/invite_code_settings]))

(spec/def :query.rename-settings/username_max_length int?)
(spec/def :query.rename-settings/username_change_limit int?)
(spec/def :query.rename-settings/username_change_limit_days int?)
(spec/def :query.rename-settings/username_change_display_days int?)
(spec/def :query.rename-settings.current-user.username-history/changed
  ::unix-timestamp)
(spec/def :query.rename-settings.current-user/username_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.User/name
                        :type.UserNameHistory/changed]))))
(spec/def :query.rename-settings/current_user
  (spec/keys :req-un [:query.rename-settings.current-user/username_history]))
(spec/def :query.rename-settings/site_configuration
  (spec/keys :req-un [:query.rename-settings/username_max_length
                      :query.rename-settings/username_change_limit
                      :query.rename-settings/username_change_limit_days
                      :query.rename-settings/username_change_display_days]))
(spec/def ::query.rename-settings
  (spec/keys :req-un [:query.rename-settings/current_user
                      :query.rename-settings/site_configuration]))

;;; subs.graphql

(spec/def ::fragment.SubInfo
  (spec/keys :req-un [:type.Sub/sid
                      :type.Sub/name
                      :type.Sub/subscriber_count
                      :type.Sub/post_count]))

(spec/def :fragment.SubSidebarInfo/creator
  (spec/keys :req-un [:type.User/name
                      :type.User/status]))
(spec/def :fragment.SubSidebarInfo.moderators/mod
  (spec/keys :req-un [:type.User/name
                      :type.User/status]))
(spec/def :fragment.SubSidebarInfo/moderators
  (spec/*
   (spec/keys :req-un [:fragment.SubSidebarInfo.moderators/mod
                       :type.SubModeration/moderation_level])))
(spec/def :fragment.SubSidebarInfo/post_flairs
  (spec/*
   (spec/keys :req-un [:type.SubPostFlair/id
                       :type.SubPostFlair/text
                       :type.SubPostFlair/mods_only
                       :type.SubPostFlair/post_types])))
(spec/def ::fragment.SubSidebarInfo
  (spec/keys :req-un [:type.Sub/title
                      :fragment.SubSidebarInfo/creator
                      :type.Sub/creation
                      :type.Sub/sidebar
                      :type.Sub/restricted
                      :type.Sub/nsfw
                      :type.Sub/user_flair_choices
                      :type.Sub/freeform_user_flairs
                      :type.Sub/user_can_flair_self
                      :type.Sub/sub_banned_users_private
                      :type.Sub/sublog_private
                      :type.Sub/text_post_min_length
                      :fragment.SubSidebarInfo/post_flairs
                      :type.Sub/user_can_flair
                      :type.Sub/user_must_flair]))

(spec/def :fragment.CommentInfo/author
  (spec/keys :req-un [:type.User/name
                      :type.User/uid
                      :type.User/status]))
(spec/def :fragment.CommentInfo/parent_post
  (spec/keys :req-un [:type.Post/pid]))
(spec/def ::fragment.CommentInfo
  (spec/keys :req-un [:type.Comment/cid
                      :type.Comment/content
                      :type.Comment/edited
                      :type.Comment/sticky
                      :type.Comment/score
                      :type.Comment/upvotes
                      :type.Comment/downvotes
                      :type.Comment/status
                      :type.Comment/time
                      :fragment.CommentInfo/author
                      :type.Comment/author_flair
                      :type.Comment/distinguish]))

(spec/def :fragment.SinglePostInfo/sub
  (spec/merge ::fragment.SubInfo ::fragment.SubSidebarInfo))
(spec/def :fragment.SinglePostInfo.author/name (spec/nilable :type.User/name))
(spec/def :fragment.SinglePostInfo/author
  (spec/keys :req-un [:fragment.SinglePostInfo.author/name
                      :type.User/uid
                      :type.User/status]))
(spec/def ::fragment.SinglePostInfo
  (spec/keys :req-un [:fragment.SinglePostInfo/author
                      :type.Post/author_flair
                      :type.Post/best_sort_enabled
                      :type.Post/comment_count
                      :type.Post/content
                      :type.Post/default_sort
                      :type.Post/distinguish
                      :type.Post/downvotes
                      :type.Post/edited
                      :type.Post/flair
                      :type.Post/is_archived
                      :type.Post/link
                      :type.Post/locked
                      :type.Post/sticky
                      :type.Post/nsfw
                      :type.Post/pid
                      :type.Post/posted
                      :type.Post/score
                      :type.Post/slug
                      :type.Post/status
                      :type.Post/thumbnail
                      :type.Post/title
                      :type.Post/type
                      :type.Post/upvotes]))

(spec/def :query.get-all-subs.all-subs.edges/node ::fragment.SubInfo)
(spec/def :query.get-all-subs.all-subs/edges
  (spec/* (spec/keys :req-un [:query.get-all-subs.all-subs.edges/node])))
(spec/def :query.get-all-subs/all_subs
  (spec/keys :req-un [:query.get-all-subs.all-subs/edges
                      :fragment/pageInfo]))
(spec/def ::query.get-all-subs
  (spec/keys :req-un [:query.get-all-subs/all_subs]))

(spec/def :query.get-single-post/content_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.ContentHistory/content]))))
(spec/def :query.get-single-post/title_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.ContentHistory/content]))))
(spec/def :query.get-single-post.post-by-pid.open-reports/id
  ::non-empty-string)
(spec/def :query.get-single-post.post-by-pid/open_reports
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:query.get-single-post.post-by-pid.open-reports/id]))))
(spec/def :query.get-single-post.post-by-pid/hide_results
  (spec/nilable boolean?))
(spec/def :query.get-single-post.post-by-pid/poll_open
  (spec/nilable boolean?))
(spec/def :query.get-single-post.post-by-pid/poll_options
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.PollOption/id
                        :type.PollOption/text
                        :type.PollOption/votes]))))
(spec/def :query.get-single-post.post-by-pid/poll_closes_time
  (spec/nilable ::unix-timestamp))
(spec/def :query.get-single-post.post-by-pid/poll_votes
  (spec/nilable nat-int?))
(spec/def :query.get-single-post.post-by-pid/user_attributes
  (spec/keys :req-un [:type.PostUserAttributes/is_saved
                      :type.PostUserAttributes/poll_vote
                      :type.PostUserAttributes/viewed
                      :type.PostUserAttributes/vote]))
(spec/def :query.get-single-post.post-by-pid.sub/user_attributes
  (spec/keys :req-un [:type.Sub/banned
                      :type.Sub/user_flair]))
(spec/def :query.get-single-post.post-by-pid/sub
  (spec/merge ::fragment.SubInfo
              ::fragment.SubSidebarInfo
              (spec/keys :opt-un [:query.get-single-post.post-by-pid.sub/user_attributes])))
(spec/def :query.get-single-post/post_by_pid
  (spec/merge ::fragment.SinglePostInfo
              (spec/keys :req-un [:query.get-single-post.post-by-pid/sub
                                  :query.get-single-post.post-by-pid/hide_results
                                  :query.get-single-post.post-by-pid/poll_open
                                  :query.get-single-post.post-by-pid/poll_options
                                  :query.get-single-post.post-by-pid/poll_closes_time
                                  :query.get-single-post.post-by-pid/poll_votes]
                         :opt-un [:query.get-single-post/content_history
                                  :query.get-single-post/title_history
                                  :query.get-single-post.post-by-pid/open_reports
                                  :query.get-single-post.post-by-pid/user_attributes])))
(spec/def ::query.get-single-post
  (spec/keys :req-un [:query.get-single-post/post_by_pid]))

(spec/def :query.sub-info.sub-by-name/user_attributes
  (spec/keys :req-un [:type.Sub/banned
                      :type.Sub/user_flair]))
(spec/def :query.sub-info/sub_by_name
  (spec/merge ::fragment.SubInfo
              ::fragment.SubSidebarInfo
              (spec/keys :opt-un [:query.sub-info.sub-by-name/user_attributes])))
(spec/def ::query.sub-info
  (spec/keys :req-un [:query.sub-info/sub_by_name]))

(spec/def :query.get-comment-tree.post-by-pid.comment-tree.comments.checkoff/time
  ::unix-timestamp)
(spec/def :query.get-comment-tree.post-by-pid.comment-tree.comments.checkoff/user
  (spec/keys :req-un [:type.User/name]))
(spec/def :query.get-comment-tree.post-by-pid.comment-tree.comments/checkoff
  (spec/nilable
   (spec/keys :req-un [:query.get-comment-tree.post-by-pid.comment-tree.comments.checkoff/user
                       :query.get-comment-tree.post-by-pid.comment-tree.comments.checkoff/time])))
(spec/def :query.get-comment-tree.post-by-pid.comment-tree.comments/content_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.ContentHistory/content]))))
(spec/def :query.get-comment-tree.post-by-pid.comment-tree.comments/user_attributes
  (spec/keys :req-un [:type.Comment/vote
                      :type.Comment/viewed]))
(spec/def :query.get-comment-tree.post-by-pid.comment-tree/comments
  (spec/*
   (spec/merge ::fragment.CommentInfo
               (spec/keys :opt-un [:query.get-comment-tree.post-by-pid.comment-tree.comments/content_history
                                   :query.get-comment-tree.post-by-pid.comment-tree.comments/checkoff
                                   :query.get-comment-tree.post-by-pid.comment-tree.comments/user_attributes]))))
(spec/def :query.get-comment-tree.post-by-pid/comment_tree
  (spec/keys :req-un [:type.CommentTree/tree_json
                      :query.get-comment-tree.post-by-pid.comment-tree/comments]))
(spec/def :query.get-comment-tree/post_by_pid
  (spec/keys :req-un [:type.Post/default_sort
                      :query.get-comment-tree.post-by-pid/comment_tree]))

(spec/def ::query.get-comment-tree
  (spec/keys :req-un [:query.get-comment-tree/post_by_pid]))

(spec/def :query.comments-by-cid.comments.checkoff/user
  (spec/keys :req-un [:type.User/name]))
(spec/def :query.comments-by-cid.comments.checkoff/time
  ::unix-timestamp)
(spec/def :query.comments-by-cid.comments/checkoff
  (spec/nilable
   (spec/keys :req-un [:query.comments-by-cid.comments.checkoff/user
                       :query.comments-by-cid.comments.checkoff/time])))
(spec/def :query.comments-by-cid.comments/content_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.ContentHistory/content]))))
(spec/def :query.comments-by-cid.comments/user_attributes
  (spec/keys :req-un [:type.Comment/vote
                      :type.Comment/viewed]))
(spec/def :query.comments-by-cid/comments_by_cid
  (spec/*
   (spec/merge ::fragment.CommentInfo
               (spec/keys :opt-un [:query.comments-by-cid.comments/user_attributes
                                   :query.comments-by-cid.comments/content_history]))))
(spec/def ::query.comments-by-cid
  (spec/keys :req-un [:query.comments-by-cid/comments_by_cid]))

(spec/def :query.sub-rules.sub-by-name/rules
  (spec/* (spec/keys :req-un [:type.SubRule/id
                              :type.SubRule/text])))
(spec/def :query.sub-rules/sub_by_name
  (spec/keys :req-un [:type.Sub/sid
                      :query.sub-rules.sub-by-name/rules]))
(spec/def ::query.sub-rules
  (spec/keys :req-un [:query.sub-rules/sub_by_name]))

(spec/def :query.sub-post-flairs.sub-by-name/post_flairs
  (spec/* (spec/keys :req-un [:type.SubPostFlair/id
                              :type.SubPostFlair/text
                              :type.SubPostFlair/mods_only
                              :type.SubPostFlair/post_types])))
(spec/def :query.sub-post-flairs/sub_by_name
  (spec/keys :req-un [:type.Sub/sid
                      :query.sub-post-flairs.sub-by-name/post_flairs]))
(spec/def ::query.sub-post-flairs
  (spec/keys :req-un [:query.sub-post-flairs/sub_by_name]))

(spec/def :mutation.create-comment.create-comment/user_attributes
  (spec/keys :req-un [:type.Comment/vote]))
(spec/def :mutation.create-comment/create_comment
  (spec/merge ::fragment.CommentInfo
              (spec/keys :req-un [:mutation.create-comment.create-comment/user_attributes])))
(spec/def ::mutation.create-comment
  (spec/keys :req-un [:mutation.create-comment/create_comment]))

(spec/def :mutation.save-post/save_post boolean?)
(spec/def ::mutation.save-post
  (spec/keys :req-un [:mutation.save-post/save_post]))

(spec/def :subscription.post-update/post_update
  (spec/keys :req-un [:type.Post/status
                      :type.Post/score
                      :type.Post/upvotes
                      :type.Post/downvotes
                      :type.Post/locked]))
(spec/def ::subscription.post-update
  (spec/keys :req-un [:subscription.post-update/post_update]))

(spec/def :query.post-status-update/post_by_pid
  (spec/keys :req-un [:type.Post/status
                      :type.Post/score
                      :type.Post/upvotes
                      :type.Post/downvotes
                      :type.Post/locked]))
(spec/def ::query.post-status-update
  (spec/keys :req-un [:query.post-status-update/post_by_pid]))

(spec/def :subscription.new-comment/new_comment
  (spec/keys :req-un [:type.Comment/cid
                      :type.Comment/parent_cid]))
(spec/def ::subscription.new-comment
  (spec/keys :req-un [:subscription.new-comment/new_comment]))

(spec/def :subscription.comment-update.checkoff/time
  ::unix-timestamp)
(spec/def :subscription.comment-update.checkoff/user
  (spec/keys :req-un [:type.User/name]))
(spec/def :subscription.comment-update/checkoff
  (spec/nilable
   (spec/keys :req-un [:subscription.comment-update.checkoff/user
                       :subscription.comment-update.checkoff/time])))
(spec/def :subscription.comment-update/content_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:type.ContentHistory/content]))))
(spec/def :subscription.comment-update/comment_update
  (spec/merge ::fragment.CommentInfo
              (spec/keys :req-un [:subscription.comment-update/content_history
                                  :subscription.comment-update/checkoff])))
(spec/def ::subscription.comment-update
  (spec/keys :req-un [:subscription.comment-update/comment_update]))

(spec/def :mutation.set-post-flair/set_post_flair
  (spec/keys :req-un [:type.SubPostFlair/text]))
(spec/def ::mutation.set-post-flair
  (spec/keys :req-un [:mutation.set-post-flair/set_post_flair]))

(spec/def :mutation.remove-post-flair/remove_post_flair
  :type.Post/pid)
(spec/def ::mutation.remove-post-flair
  (spec/keys :req-un [:mutation.remove-post-flair/remove_post_flair]))

(spec/def :mutation.update-post-title/update_post_title boolean?)
(spec/def ::mutation.update-post-title
  (spec/keys :req-un [:mutation.update-post-title/update_post_title]))

(spec/def :mutation.update-post-content/update_post_content boolean?)
(spec/def ::mutation.update-post-content
  (spec/keys :req-un [:mutation.update-post-content/update_post_content]))

(spec/def :mutation.update-comment-content/update_comment_content boolean?)
(spec/def ::mutation.update-comment-content
  (spec/keys :req-un [:mutation.update-comment-content/update_comment_content]))

(spec/def :mutation.checkoff-comment.checkoff-comment/user
  (spec/keys :req-un [:type.User/name]))
(spec/def :mutation.checkoff-comment.checkoff-comment/time
  ::unix-timestamp)
(spec/def :mutation.checkoff-comment/checkoff_comment
  (spec/nilable
   (spec/keys :req-un [:mutation.checkoff-comment.checkoff-comment/user
                       :mutation.checkoff-comment.checkoff-comment/time])))
(spec/def ::mutation.checkoff-comment
  (spec/keys :req-un [:mutation.checkoff-comment/checkoff_comment]))

(spec/def :mutation.un-checkoff-comment.un-checkoff-comment/user
  (spec/keys :req-un [:type.User/name]))
(spec/def :mutation.un-checkoff-comment.un-checkoff-comment/time
  ::unix-timestamp)
(spec/def :mutation.un-checkoff-comment/un_checkoff_comment
  (spec/nilable
   (spec/keys :req-un [:mutation.un-checkoff-comment.un-checkoff-comment/user
                       :mutation.un-checkoff-comment.un-checkoff-comment/time])))
(spec/def ::mutation.un-checkoff-comment
  (spec/keys :req-un [:mutation.un-checkoff-comment/un_checkoff_comment]))

(spec/def :mutation.distinguish-comment/distinguish_comment
  ::type.DistinguishType)
(spec/def ::mutation.distinguish-comment
  (spec/keys :req-un [:mutation.distinguish-comment/distinguish_comment]))

(spec/def :mutation.distinguish-post/distinguish_post
  ::type.DistinguishType)
(spec/def ::mutation.distinguish-post
  (spec/keys :req-un [:mutation.distinguish-post/distinguish_post]))

(spec/def :mutation.update-post-viewed/update_post_viewed
  (spec/nilable :type.Post/pid))
(spec/def ::mutation.update-post-viewed
  (spec/keys :req-un [:mutation.update-post-viewed/update_post_viewed]))

(spec/def :mutation.set-post-nsfw/set_post_nsfw boolean?)
(spec/def ::mutation.set-post-nsfw
  (spec/keys :req-un [:mutation.set-post-nsfw/set_post_nsfw]))

(spec/def :query.post-type-config.sub-by-name/post-type-config
  (spec/keys :req-un [:type.SubPostTypeConfig/mods_only
                      :type.SubPostTypeConfig/post_type
                      :type.SubPostTypeConfig/rules]))
(spec/def :query.post-type-config.sub-by-name/post_type_config
  (spec/coll-of :query.post-type-config.sub-by-name/post-type-config))
(spec/def :query.post-type-config/sub_by_name
  (spec/keys :req-un [:type.Sub/name
                      :query.post-type-config.sub-by-name/post_type_config]))
(spec/def ::query.post-type-config
  (spec/keys :req-un [:query.post-type-config/sub_by_name]))

(spec/def :query.submit-post-settings.site-configuration/allow_uploads
  boolean?)
(spec/def :query.submit-post-settings.site-configuration/allow_video_uploads
  boolean?)
(spec/def :query.submit-post-settings.site-configuration/enable_posting
  boolean?)
(spec/def :query.submit-post-settings.site-configuration/upload_max_size int?)
(spec/def :query.submit-post-settings.site-configuration/upload_min_level int?)
(spec/def :query.submit-post-settings/site_configuration
  (spec/keys :req-un
             [:query.submit-post-settings.site-configuration/allow_uploads
              :query.submit-post-settings.site-configuration/allow_video_uploads
              :query.submit-post-settings.site-configuration/enable_posting
              :query.submit-post-settings.site-configuration/upload_max_size
              :query.submit-post-settings.site-configuration/upload_min_level]))
(spec/def ::query.submit-post-settings
  (spec/keys :req-un [:query.submit-post-settings/site_configuration]))

(spec/def :subscription.url-metadata.url-metadata/title
  (spec/nilable string?))
(spec/def :subscription.url-metadata.url-metadata/description
  (spec/nilable string?))
(spec/def :subscription.url-metadata.url-metadata/content_type
  (spec/nilable string?))
(spec/def :subscription.url-metadata/url_metadata
  (spec/keys :req-un
             [:subscription.url-metadata.url-metadata/title
              :subscription.url-metadata.url-metadata/description
              :subscription.url-metadata.url-metadata/content_type]))
(spec/def ::subscription.url-metadata
  (spec/keys :req-un [:subscription.url-metadata/url_metadata]))

(spec/def :mutation.cast-post-vote/cast_post_vote
  (spec/keys :req-un [:type.Post/score]
             :opt-un [:type.Post/upvotes
                      :type.Post/downvotes]))
(spec/def ::mutation.cast-post-vote
  (spec/keys :req-un [:mutation.cast-post-vote/cast_post_vote]))

(spec/def :mutation.cast-comment-vote/cast_comment_vote
  (spec/keys :req-un [:type.Comment/score]
             :opt-un [:type.Comment/upvotes
                      :type.Comment/downvotes]))
(spec/def ::mutation.cast-comment-vote
  (spec/keys :req-un [:mutation.cast-comment-vote/cast_comment_vote]))

(spec/def :mutation.create-post/create_post
  (spec/keys :req-un [:type.Post/pid]))
(spec/def ::mutation.create-post
  (spec/keys :req-un [:mutation.create-post/create_post]))

(spec/def :mutation.remove-post/remove_post
  ::scalar.Void)
(spec/def ::mutation.remove-post
  (spec/keys :req-un [:mutation.remove-post/remove_post]))

(spec/def :mutation.create-upload-url/create_upload_url
  string?)
(spec/def ::mutation.create-upload-url
  (spec/keys :req-un [:mutation.create-upload-url/create_upload_url]))

;;; moderation.graphql

(spec/def :query.current-user-mod-stats.current_user.subs_moderated/sub
  (spec/keys :req-un [:type.Sub/sid
                      :type.Sub/name
	              :type.Sub/subscriber_count
	              :type.Sub/open_report_count
	              :type.Sub/closed_report_count
	              :type.Sub/all_modmail_count
	              :type.Sub/unread_modmail_count]))
(spec/def :query.current-user-mod-stats.current_user/subs_moderated
  (spec/*
   (spec/keys :req-un [:query.current-user-mod-stats.current_user.subs_moderated/sub])))
(spec/def :query.current-user-mod-stats/current_user
  (spec/nilable
   (spec/keys :req-un [:query.current-user-mod-stats.current_user/subs_moderated])))
(spec/def ::query.current-user-mod-stats
  (spec/keys :req-un [:query.current-user-mod-stats/current_user]))

(spec/def :query.admin-mod-stats.all-subs.edges/node
  (spec/keys :req-un [:type.Sub/sid
                      :type.Sub/name
                      :type.Sub/subscriber_count
                      :type.Sub/open_report_count
                      :type.Sub/closed_report_count
                      :type.Sub/new_modmail_count]))
(spec/def :query.admin-mod-stats.all-subs/edges
  (spec/* (spec/keys :req-un [:query.admin-mod-stats.all-subs.edges/node])))
(spec/def :query.admin-mod-stats/all_subs
  (spec/keys :req-un [:query.admin-mod-stats.all-subs/edges
                      :fragment/pageInfo]))
(spec/def ::query.admin-mod-stats
  (spec/keys :req-un [:query.admin-mod-stats/all_subs]))

(spec/def :mutation.update-sub-post-type-config/update_sub_post_type_config
  nil?)
(spec/def ::mutation.update-sub-post-type-config
  (spec/keys
   :req-un [:mutation.update-sub-post-type-config/update_sub_post_type_config]))

(spec/def :mutation.create-sub-post-flair/create_sub_post_flair
  (spec/keys :req-un [:type.SubPostFlair/id
                      :type.SubPostFlair/text
                      :type.SubPostFlair/mods_only
                      :type.SubPostFlair/post_types]))
(spec/def ::mutation.create-sub-post-flair
  (spec/keys :req-un [:mutation.create-sub-post-flair/create_sub_post_flair]))

(spec/def :mutation.delete-sub-post-flair/delete_sub_post_flair nil?)
(spec/def ::mutation.delete-sub-post-flair
  (spec/keys :req-un [:mutation.delete-sub-post-flair/delete_sub_post_flair]))

(spec/def :mutation.reorder-sub-post-flairs/reorder_sub_post_flairs
  (spec/* (spec/keys :req-un [:type.SubPostFlair/id
                              :type.SubPostFlair/text
                              :type.SubPostFlair/mods_only
                              :type.SubPostFlair/post_types])))
(spec/def ::mutation.reorder-sub-post-flairs
  (spec/keys :req-un [:mutation.reorder-sub-post-flairs/reorder_sub_post_flairs]))

(spec/def :mutation.update-sub-post-flair/update_sub_post_flair nil?)
(spec/def ::mutation.update-sub-post-flair
  (spec/keys :req-un [:mutation.update-sub-post-flair/update_sub_post_flair]))

(spec/def :mutation.create-sub-rule/create_sub_rule
  (spec/keys :req-un [:type.SubRule/id
                      :type.SubRule/text]))
(spec/def ::mutation.create-sub-rule
  (spec/keys :req-un [:mutation.create-sub-rule/create_sub_rule]))

(spec/def :mutation.delete-sub-rule/delete_sub_rule nil?)
(spec/def ::mutation.delete-sub-rule
  (spec/keys :req-un [:mutation.delete-sub-rule/delete_sub_rule]))

(spec/def :mutation.reorder-sub-rules/reorder_sub_rules
  (spec/* (spec/keys :req-un [:type.SubRule/id
                              :type.SubRule/text])))
(spec/def ::mutation.reorder-sub-rules
  (spec/keys :req-un [:mutation.reorder-sub-rules/reorder_sub_rules]))

;;; user.graphql

(spec/def :query.content-blocks.current-user.content-blocks/user
  (spec/keys :req-un [:type.User/uid]))
(spec/def :query.content-blocks.current-user.content-blocks/content_block
  ::type.ContentBlockType)
(spec/def :query.content-blocks.current-user/content_blocks
  (spec/coll-of
   (spec/keys :req-un
              [:query.content-blocks.current-user.content-blocks/user
               :query.content-blocks.current-user.content-blocks/content_block])))
(spec/def :query.content-blocks/current_user
  (spec/keys :req-un [:query.content-blocks.current-user/content_blocks]))
(spec/def ::query.content-blocks
  (spec/keys :req-un [:query.content-blocks/current_user]))

(spec/def :subscription.notification-counts.notification-counts.sub-notifications/open_report_count
  (spec/nilable :type.Sub/open_report_count))
(spec/def :subscription.notification-counts.notification-counts.sub-notifications/unread_modmail_count
  (spec/nilable :type.Sub/unread_modmail_count))
(spec/def :subscription.notification-counts.notification-counts/sub_notifications
  (spec/*
   (spec/keys :req-un [:type.Sub/sid
                       :subscription.notification-counts.notification-counts.sub-notifications/open_report_count
                       :subscription.notification-counts.notification-counts.sub-notifications/unread_modmail_count])))
(spec/def :subscription.notification-counts/notification_counts
  (spec/keys :req-un [:type.NotificationCounts/unread_message_count
                      :type.NotificationCounts/unread_notification_count
                      :subscription.notification-counts.notification-counts/sub_notifications]))
(spec/def ::subscription.notification-counts
  (spec/keys :req-un [:subscription.notification-counts/notification_counts]))

(spec/def :mutation.change-subscription/change_subscription_status
  (spec/keys :req-un [:type.Subscription/name
                      :type.Subscription/status
                      :type.Subscription/order]))
(spec/def ::mutation.change-subscription
  (spec/keys :req-un [:mutation.change-subscription-status/change_subscription_status]))

(spec/def :subscription.subscription-updates/subscription_update
  (spec/keys :req-un [:type.Subscription/name
                      :type.Subscription/status
                      :type.Subscription/order]))
(spec/def ::subscription.subscription-updates
  (spec/keys :req-un [:subscription.subscription-updates/subscription_update]))

(spec/def :mutation.change-user-flair/change_user_flair
  (spec/nilable string?))
(spec/def ::mutation.change-user-flair
  (spec/keys :req-un [:mutation.change-user-flair/change_user_flair]))

(spec/def :query.user-preference-settings/invite_code_settings
  (spec/keys :req-un [:type.InviteCodeSettings/required]))
(spec/def :query.user-preference-settings/site_configuration
  (spec/keys :req-un [:type.SiteConfiguration/username_change_limit
                      :type.SiteConfiguration/languages]))
(spec/def :query.user-preference-settings.current-user/attributes
  (spec/keys :req-un [:type.UserAttributes/nsfw
                      :type.UserAttributes/nsfw_blur
                      :type.UserAttributes/lab_rat
                      :type.UserAttributes/block_dms
                      :type.UserAttributes/enable_collapse_bar]))
(spec/def :query.user-preference-settings/current_user
  (spec/keys :req-un [:query.user-preference-settings.current-user/attributes
                      :type.User/language]))
(spec/def ::query.user-preference-settings
  (spec/keys :req-un [:query.user-preference-settings/invite_code_settings
                      :query.user-preference-settings/site_configuration
                      :query.user-preference-settings/current_user]))

(spec/def :mutation.update-user-preferences/update_user_preferences ::scalar.Void)
(spec/def ::mutation.update-user-preferences
  (spec/keys :req-un [:mutation.update-user-preferences/update_user_preferences]))
