;; fe/content/admin.cljs -- Administrator pages for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.admin
  (:require
   [cljs-time.coerce :as coerce-time]
   [cljs-time.core :as time]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.load-more :as load-more]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.graphql :refer-macros [defgraphql] :as graphql]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.ui :as ui]
   [ovarit.app.fe.ui.forms.admin :as-alias admin-forms]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame]
   [statecharts.core :as fsm]))

;; Import graphql queries.
(defgraphql graphql "graphql/admin.graphql")

;; Site Statistics

(re-frame/reg-event-fx ::load-stats
  ;; :doc Load site statistics.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query {:graphql graphql
                                       :name :stats-all-time
                                       :variables {}
                                       :handler [::set-stats]}]]]}))

(re-frame/reg-event-db ::set-stats
  ;; :doc Receive site statistics.
  (fn-traced [db [_ {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event ::set-stats :errors errors})
          (assoc-in [:content :stats] (:site-stats data))))))

(re-frame/reg-sub ::stats
  ;; :doc Extract site statistics.
  (fn [db _]
    (get-in db [:content :stats])))

(defn date-to-long
  "Return the start of the day as milliseconds since the Unix epoch."
  [date]
  (-> (time/date-time (:year date)
                      (:month date)
                      (:day date))
      coerce-time/to-long))

(defn long-to-date
  "Return a time in milliseconds since the epoch (in a string) as a date."
  [ms]
  (let [datetime (-> (js/parseInt ms)
                     coerce-time/from-long)]
    {:year (time/year datetime)
     :month (time/month datetime)
     :day (time/day datetime)}))

(re-frame/reg-sub ::this-year
  ;; :doc Get the current year.
  :<- [::window/now]
  (fn [now _]
    (time/year (coerce-time/from-long now))))

(re-frame/reg-event-fx ::load-stats-timespan
  ;; :doc Load site statistics for a timespan.
  (fn-traced [{:keys [db]} [_ _]]
    (let [until (-> (get-in db [:view :options ::stats-until])
                    date-to-long)
          since (-> (get-in db [:view :options ::stats-since])
                    date-to-long)]
      {:fx [[:dispatch [::graphql/query {:graphql graphql
                                         :name :stats-timespan
                                         :variables {:since (str since)
                                                     :until (str until)}
                                         :handler [::set-stats-timespan
                                                   {:since since
                                                    :until until}]}]]]})))

(re-frame/reg-event-db ::set-stats-timespan
  ;; :doc Receive site statistics for a timespan.
  (fn-traced [db [_ {:keys [since until response]}]]
    (let [{:keys [data errors]} response
          now-long (coerce-time/to-long (time/now))
          complete? (and (< until now-long)
                         (< since now-long))
          entries (get-in db [:content :stats-timespan])
          match? #(= (select-keys % [:since :until])
                     {:since since :until until})
          entry {:since since
                 :until until
                 :complete? complete?
                 :stats (:site-stats data)}
          new-entries (if (some match? entries)
                        (map #(if (match? %) entry %) entries)
                        (conj entries entry))]
      (-> db
          (errors/assoc-errors {:event ::set-stats-timespan :errors errors})
          (assoc-in [:content :stats-timespan] new-entries)))))

(re-frame/reg-sub ::stats-timespan-entries
  ;; :doc Get entries in list of recent statistics.
  (fn [db _]
    (get-in db [:content :stats-timespan])))

(re-frame/reg-sub ::stats-timespan-entry
  ;; :doc Extract current recent site statistics entry.
  :<- [::stats-since-date]
  :<- [::stats-until-date]
  :<- [::stats-timespan-entries]
  (fn [[since-date until-date entries] _]
    (let [since (date-to-long since-date)
          until (date-to-long until-date)]
      (some (fn [entry]
              (when (= (select-keys entry [:since :until])
                       {:since since :until until})
                entry))
            entries))))

(re-frame/reg-sub ::stats-timespan-complete?
  ;; :doc Get whether recent statistics are final (no partial last day).
  :<- [::stats-timespan-entry]
  (fn [entry _]
    (:complete? entry)))

(re-frame/reg-sub ::stats-timespan
  ;; :doc Extract recent site statistics.
  :<- [::stats-timespan-entry]
  (fn [entry _]
    (:stats entry)))

(re-frame/reg-event-fx ::load-visitor-counts
  ;; :doc Load visitor counts, daily and weekly.
  (fn-traced [{:keys [db]} [_ option]]
    (let [start-date (get-in db [:view :options ::visitor-count-date option])
          since (date-to-long start-date)
          interval (if (= option :daily)
                     (time/days 7)
                     (time/months 1))
          until-date (time/plus (time/date-time (:year start-date)
                                                (:month start-date)
                                                (:day start-date))
                                interval)
          until (coerce-time/to-long until-date)
          now-long (coerce-time/to-long (time/now))
          complete? (and (< until now-long) (< since now-long))]
      {:db (assoc-in db [:content :visitor-counts-complete? option] complete?)
       :fx [[:dispatch
             [::graphql/query
              {:graphql graphql
               :name :visitor-counts
               :id (str :visitor-counts option)
               :variables {:since (str since)
                           :until (str until)
                           :by (if (= option :daily) 1 7)}
               :handler [::set-visitor-counts {:option option}]}]]]})))

(re-frame/reg-event-db ::set-visitor-counts
  ;; :doc Receive visitor counts.
  (fn-traced [db [_ {:keys [option response]}]]
    (let [{:keys [data errors]} response
          entries (:site-visitor-counts data)
          dates (into {} (map (fn [rec]
                                [(js/parseInt (:start rec)) (:count rec)])
                              entries))]
      (-> db
          (errors/assoc-errors {:event ::set-visitor-counts :errors errors})
          (update-in [:content :visitor-counts option] merge dates)))))

(re-frame/reg-sub ::visitor-counts-complete?
  ;; :doc Extract whether visitor counts may include incomplete data.
  (fn [db [_ option]]
    (get-in db [:content :visitor-counts-complete? option])))

(re-frame/reg-sub ::visitor-count-data
  ;; :doc Extract visitor count data.
  (fn [db [_ option]]
    (get-in db [:content :visitor-counts option])))

(re-frame/reg-sub ::visitor-counts
  ;; :doc Get visitor count data for the currently selected date.
  ;; :doc Provide unpacked dates and an average.
  (fn [[_ option]]
    [(re-frame/subscribe [::visitor-count-data option])
     (re-frame/subscribe [::visitor-count-date option])])

  (fn [[data start-date] [_ option]]
    (let [num (if (= option :daily) 7 4)
          by (if (= option :daily) 1 7)
          start-datetime (time/date-time (:year start-date)
                                         (:month start-date)
                                         (:day start-date))
          dates (map (fn [day]
                       (-> start-datetime
                           (time/plus (time/days (* day by)))
                           coerce-time/to-long))
                     (range num))
          selected-values (map (fn [k] [k (or (get data k) 0)])
                               dates)
          entries (map (fn [[start count]]
                         (assoc (long-to-date start) :value count))
                       selected-values)
          sum (reduce + (map :value entries))
          average (-> (/ sum num)
                      (+ 0.5)
                      int)]
      {:entries entries
       :average average})))

(defn todays-date
  "Return the current date as a map."
  []
  (let [now (time/now)]
    {:year (time/year now)
     :month (time/month now)
     :day (time/day now)}))

(defn week-ago-date
  "Return the date from one week ago."
  []
  (let [then (time/minus (time/now) (time/days 7))]
    {:year (time/year then)
     :month (time/month then)
     :day (time/day then)}))

(defn month-ago-date
  "Return the date from one month ago."
  []
  (let [then (time/minus (time/now) (time/months 1))]
    {:year (time/year then)
     :month (time/month then)
     :day (time/day then)}))

(defn update-date
  "Update a date map with a new month, day or year.
  Fix the day if the number of days in the month changes."
  [previous key value]
  (let [date (assoc previous key (js/parseInt value))
        days-in-month (time/day (time/last-day-of-the-month (:year date)
                                                            (:month date)))]
    {:year (:year date)
     :month (:month date)
     :day (if (> (:day date) days-in-month) 1 (:day date))}))

(defn init-dates
  [db]
  (-> db
      (update-in [:view :options] assoc
                 ::stats-since (month-ago-date)
                 ::stats-until (todays-date))
      (update-in [:view :options ::visitor-count-date] assoc
                 :daily (week-ago-date)
                 :weekly (month-ago-date))))

(re-frame/reg-event-fx ::set-stats-since-date
  ;; :doc Set the month, day or year of the start date for recent statistics.
  (fn-traced [{:keys [db]} [_ key value]]
    (let [old (get-in db [:view :options ::stats-since])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::stats-since] new)
       :fx [(when (not= old new)
              [:dispatch [::load-stats-timespan]])]})))

(re-frame/reg-sub ::stats-since-date
  ;; :doc Extract the start date for recent statistics.
  (fn [db _]
    (get-in db [:view :options ::stats-since])))

(re-frame/reg-event-fx ::set-stats-until-date
  ;; :doc Set the month, day or year of the end date for recent statistics.
  (fn-traced [{:keys [db]} [_ key value]]
    (let [old (get-in db [:view :options ::stats-until])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::stats-until] new)
       :fx [(when (not= old new)
              [:dispatch [::load-stats-timespan]])]})))

(re-frame/reg-sub ::stats-until-date
  ;; :doc Extract the end date for recent statistics.
  (fn [db _]
    (get-in db [:view :options ::stats-until])))

(re-frame/reg-event-fx ::set-visitor-count-date
  ;; :doc Set the month, day or year of the start date for counting visitors.
  (fn-traced [{:keys [db]} [_ option key value]]
    (let [old (get-in db [:view :options ::visitor-count-date option])
          new (update-date old key value)]
      {:db (assoc-in db [:view :options ::visitor-count-date option] new)
       :fx [(when (not= old new)
              [:dispatch [::load-visitor-counts option]])]})))

(re-frame/reg-sub ::visitor-count-date
  ;; :doc Extract the start date for visitor counts.
  (fn [db [_ option]]
    (get-in db [:view :options ::visitor-count-date option])))

;; Banned strings in user names.

(re-frame/reg-event-fx ::load-banned-username-strings
  ;; :doc Request the list of banned username strings and affected users.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql
                       :name :banned-username-strings
                       :variables {}
                       :handler [::receive-banned-username-strings]}]]]}))

(re-frame/reg-event-db ::receive-banned-username-strings
  ;; :doc Receive the list of banned username strings and affected users.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:content :banned-username-strings]
                    (:banned-username-strings data))))))

(re-frame/reg-event-fx ::ban-username-string
  ;; :doc Send a new string to ban in usernames.
  (fn-traced [_ [_ {:keys [form]}]]
    (let [content (:content form)]
      {:fx [[:dispatch
             [::graphql/mutate
              {:graphql graphql
               :name :ban-string-in-usernames
               :id (str :ban-string-in-usernames content)
               :variables {:banned content}
               :handler [::receive-ban-username-string {:form form}]}]]]})))

(re-frame/reg-event-db ::receive-ban-username-string
  ;; :doc Receive the response from the server after adding a new string to ban.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          existing (get-in db [:content :banned-username-strings])
          new-ban (:ban-string-in-usernames data)
          banned (:banned new-ban)
          present? (some #(= banned (:banned %)) existing)
          updated (cond
                    present? (map #(if (= banned (:banned %)) new-ban %)
                                  existing)
                    new-ban (conj existing new-ban)
                    :else existing)]
      (-> db
          (errors/assoc-errors {:event (:id form) :errors errors})
          (assoc-in [:content :banned-username-strings] updated)
          (forms/transition-after-response (:id form))))))

(re-frame/reg-event-fx ::unban-username-string
  ;; :doc Unban a string in usernames.
  (fn-traced [_ [_ banned]]
    {:fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :unban-string-in-usernames
                       :id (str :unban-string-in-usernames banned)
                       :variables {:banned banned}
                       :handler [::receive-unban-username-string]}]]]}))

(re-frame/reg-event-db ::receive-unban-username-string
  ;; :doc Receive the response from the server after removing a banned string.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          banned (:unban-string-in-usernames data)
          existing (get-in db [:content :banned-username-strings])
          updated (filter #(not= (:banned %) banned) existing)]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (assoc-in [:content :banned-username-strings] updated)))))

(re-frame/reg-event-db ::set-ban-username-string-sort
  ;; :doc Change the sort type or direction in the banned strings table.
  ;; :doc Options are :string and :count.
  (fn-traced [db [_ typ]]
    (let [toggle? (= typ (get-in db [:view :options :sort-typ]))]
      (-> db
          (assoc-in [:view :options :sort-typ] typ)
          (update-in [:view :options :sort-asc?]
                     #(if toggle?
                        (not %)
                        (= typ :string)))))))

(re-frame/reg-event-db ::show-all-users
  ;; :doc Set the flag to show all users instead of just the first 5.
  ;; :doc Applies to one of the strings in the banned username strings table.
  (fn-traced [db [_ banned]]
    (assoc-in db [:view :options :show-all banned] true)))

(re-frame/reg-sub ::banned-username-data
  ;; :doc Extract the list of banned strings in usernames with user lists.
  (fn [db _]
    (get-in db [:content :banned-username-strings])))

(re-frame/reg-sub ::banned-username-sort-options
  ;; :doc Extract the sort options for banned strings in usernames.
  (fn [db _]
    (select-keys (get-in db [:view :options]) [:sort-typ :sort-asc?])))

(re-frame/reg-sub ::banned-username-show-all
  ;; :doc Extract the show all option for usernames in the banned strings table.
  (fn [db _]
    (get-in db [:view :options :show-all])))

(re-frame/reg-sub ::banned-username-strings
  ;; :doc Compute the sorted list of banned strings in usernames.
  :<- [::banned-username-data]
  :<- [::banned-username-show-all]
  :<- [::banned-username-sort-options]
  (fn [[data show-all {:keys [sort-typ sort-asc?]}] _]
    (let [num-to-show 5
          data-with-show (map
                          (fn [{:keys [banned users] :as elem}]
                            (let [show-all? (get show-all banned)]
                              (assoc elem
                                     :show (if show-all?
                                             (count users) num-to-show)
                                     :more? (and (not show-all?)
                                                 (> (count users) num-to-show)))))
                          data)
          extract (if (= sort-typ :string)
                    :banned
                    #(count (:users %)))
          sorted (sort-by extract data-with-show)]
      (if sort-asc?
        sorted
        (reverse sorted)))))

;;; Admin require name change page

(re-frame/reg-event-fx ::load-required-name-change
  ;; :doc Load the user's require name change status.
  (fn-traced [{:keys [db]} [_ _]]
    (let [name (-> db :view :options :name)]
      {:db (-> db
               (assoc-in [:status :content] :loading)
               (update :reldb rel/transact
                       [:delete :RequireNameChange]
                       [:delete :UserNameHistory]))
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql
                         :name :required-name-change
                         :variables {:name name}
                         :handler [::receive-required-name-change
                                   {:name name}]}]]]})))

(defn- fixup-required-name-change-info
  [info name]
  (-> info
      (assoc :name name)
      (assoc :admin-name (-> info :admin :name))
      (dissoc :admin)
      (update :required-at util/to-ISO-time)))

(re-frame/reg-event-fx ::receive-required-name-change
  ;; :doc Receive the response to the request for a required name change.
  (fn-traced [{:keys [db]} [event {:keys [name response]}]]
    (let [{:keys [data errors]} response]
      (if (= "Not found" (get-in errors [0 :message]))
        {:fx [[:dispatch [::routes/show-error :unmatched-route]]]}
        (let [{:keys [user-by-name required-name-change]} data
              info (fixup-required-name-change-info required-name-change name)
              {:keys [uid username-history]} user-by-name
              history (map #(internal/fixup-name-history % uid)
                           username-history)]
          {:db (-> db
                   (assoc-in [:status :content] :loaded)
                   (assoc-in [:view :options :uid] uid)
                   (errors/assoc-errors {:event event :errors errors})
                   (cond-> required-name-change
                     (update :reldb rel/transact
                             [:insert :RequireNameChange info]))
                   (cond-> (seq history)
                     (update :reldb rel/transact
                             (into [:insert :UserNameHistory] history))))})))))

(re-frame/reg-sub ::-require-name-change-info
  ;; :doc Extract the current require name change data.
  ;; :doc Result will be nil if require name change is not active.
  :<- [:view/options]
  :<- [::internal/reldb]
  (fn-traced [[{:keys [name]} reldb] [_ _]]
    (rel/row reldb [[:from :RequireNameChange]
                    [:where [= name :name]]])))

(re-frame/reg-sub ::require-name-change-info
  ;; :doc Prepare require name change info for display.
  :<- [::-require-name-change-info]
  (fn [info _]
    (some-> info
            (update :message util/markdown-to-html))))

(re-frame/reg-sub ::name-change-history
  ;; :doc Extract the user's name change history.
  :<- [:view/options]
  :<- [::internal/reldb]
  (fn [[{:keys [uid]} reldb] _]
    (->> (rel/q reldb [[:from :UserNameHistory]
                       [:where [= uid :uid]]
                       [:sort [:changed :desc]]])
         (map #(update % :changed util/to-ISO-time)))))

(re-frame/reg-event-fx ::send-require-name-change
  ;; :doc Send a require name change request to the server.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [{:keys [message]} form
          {:keys [name uid]} (-> db :view :options)]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :require-name-change
                         :id :require-name-change
                         :variables {:uid uid
                                     :message message}
                         :handler [::receive-require-name-change
                                   {:form form
                                    :name name}]}]]]})))

(re-frame/reg-event-db ::receive-require-name-change
  ;; :doc Handle the response to requiring a name change.
  (fn-traced [db [_ {:keys [form name response]}]]
    (let [{:keys [data errors]} response
          require-name-change (:require-name-change data)
          info (fixup-required-name-change-info require-name-change name)]
      (-> db
          (errors/assoc-errors {:event (:id form) :errors errors})
          (cond-> (nil? errors)
            (update :reldb rel/transact [:insert :RequireNameChange info]))
          (forms/transition-after-response (:id form))))))

(re-frame/reg-event-fx ::cancel-required-name-change
  ;; :doc Ask the server to cancel a required name change.
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc-in db [:status :cancel-require-name-change] :SENDING)
     :fx [[:dispatch [::graphql/mutate
                      {:graphql graphql
                       :name :cancel-required-name-change
                       :id :cancel-required-name-change
                       :variables {:uid (-> db :view :options :uid)}
                       :handler [::receive-cancel-required-name-change]}]]]}))

(re-frame/reg-event-db ::receive-cancel-required-name-change
  (fn-traced [db [event {:keys [response]}]]
    ;; :doc Receive the response to cancelling a required name change.
    (let [{:keys [errors]} response]
      (-> db
          (errors/assoc-errors {:event event :errors errors})
          (update :status dissoc :cancel-require-name-change)
          (cond-> (nil? errors)
            (update :reldb rel/transact
                    [:delete :RequireNameChange]))))))

(re-frame/reg-sub ::cancel-required-name-change-state
  :<- [::window/statuses]
  (fn [statuses _]
    (:cancel-require-name-change statuses)))

;;; Admin Invite Codes page

;; Fetch the data for the admin invite code page.

(def invite-code-table-request-size
  "Number of invite codes to get from the server on each request."
  100)

(re-frame/reg-event-fx ::load-invite-codes-and-settings
  ;; :doc Request the invite code settings and the first page of invite codes.
  (fn-traced [{:keys [db]} _]
    {:db (assoc-in db [:status :content] :loading)
     :fx [[:dispatch [::graphql/query
                      {:graphql graphql
                       :name :invite-codes-and-settings
                       :variables {:first invite-code-table-request-size}
                       :handler [::receive-invite-codes-and-settings]}]]]}))

(defn update-invite-codes
  "Update the list of invite codes to add new data at the end.
  Pass the results of the invite_codes query, after kebab-case."
  [existing-codes new-data]
  (let [new-codes (->> (:edges new-data)
                       (map :node))]
    (-> existing-codes
        (merge (:page-info new-data))
        (assoc :codes (concat (:codes existing-codes) new-codes)))))

(defn- update-settings
  [existing {:keys [required visible minimum-level per-user]}]
  (merge existing {:required required
                   :visible visible
                   :minimum-level (str minimum-level)
                   :per-user (str per-user)}))

(re-frame/reg-event-fx ::receive-invite-codes-and-settings
  ;; :doc Receive the invite code settings and the first page of invite codes.
  (fn-traced [{:keys [db]} [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          {:keys [invite-code-settings invite-codes]} data
          form-id ::admin-forms/invite-code-settings]
      {:db
       (-> db
           (errors/assoc-errors {:event event :errors errors})
           (assoc-in [:content :invite-code-settings] invite-code-settings)
           (assoc-in [:content :invite-codes :all]
                     (update-invite-codes {} invite-codes))
           (assoc-in [:status :content] :loaded)
           (update :reldb rel/transact
                   [:update :FormState
                    #(update-settings % invite-code-settings)
                    [= [:_ form-id] :id]])
           (forms/transition form-id {:type :edit}))
       :fx [[:dispatch-later
             [{:ms 100
               :dispatch [::load-more/check-load-more]}]]]})))

(re-frame/reg-event-fx ::update-invite-code-settings
  ;; :doc Send modified invite code settings to the server.
  (fn-traced [_ [_ {:keys [form]}]]
    (let [{:keys [required? visible? minimum-level per-user]} form]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :invite-code-settings
                         :id :invite-code-settings
                         :variables {:required required?
                                     :visible visible?
                                     :minimum_level (js/parseInt minimum-level)
                                     :per_user (js/parseInt per-user)}
                         :handler [::receive-invite-code-settings
                                   {:form form}]}]]]})))

(re-frame/reg-event-db ::receive-invite-code-settings
  ;; :doc Receive the response after changing invite code settings.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [ {:keys [data errors]} response
          existing (get-in db [:content :invite-code-settings])
          new (:set-invite-code-settings data)
          current (merge existing new)]
      (-> db
          (errors/assoc-errors {:event (:id form)
                                :errors errors})
          (assoc-in [:content :invite-code-settings] current)
          (update :reldb rel/transact
                  [:update :FormState
                   #(update-settings % new)
                   [= [:_ ::admin-forms/invite-code-settings] :id]])
          (forms/transition-after-response (:id form))))))

(re-frame/reg-sub ::invite-code-tables
  ;; :doc Extract the invite code table.
  (fn [db _]
    (get-in db [:content :invite-codes])))

(re-frame/reg-sub ::invite-code-search-code
  (fn [db _]
    (get-in db [:view :options :search])))

(re-frame/reg-sub ::invite-codes-by-search-code
  :<- [::invite-code-tables]
  :<- [::invite-code-search-code]
  (fn [[tables search] _]
    (get-in tables [search :codes])))

(defn format-code-date
  "Format a date in an invite code record, given a java unix epoch
  value in a string, and the current time as a java unix epoch date
  in an integer.  If the date given is nil, return nil.  If `time?`
  is true and the date is within 48 hours of right now, include the
  time in the formatted date."
  [ms now time?]
  (when ms
    (let [ms-int (js/parseInt ms)
          diff (js/Math.abs (- now ms-int))
          datetime (js/Date. ms-int)]
      (if (and time? (< diff (* 1000 60 60 24 2)))
        (.toLocaleString datetime)
        (.toLocaleDateString datetime)))))

(defn format-code-dates
  "Format the dates in an invite code record from the server."
  [{:keys [expires uses max-uses] :as code} now]
  (let [expires-ms (when expires (js/parseInt expires))]
    (-> code
        (update :created-on format-code-date now false)
        (update :expires format-code-date now true)
        (assoc :expired? (or (>= uses max-uses)
                             (and expires-ms (> now expires-ms)))))))

(re-frame/reg-sub ::invite-codes
  ;; :doc Produce the invite code table with expirations.
  :<- [::invite-codes-by-search-code]
  :<- [::window/now]
  (fn [[codes now] _]
    (map #(format-code-dates % now) codes)))

(re-frame/reg-sub ::invite-codes-empty?
  :<- [::invite-codes]
  (fn [codes _]
    (zero? (count codes))))

(re-frame/reg-sub ::invite-code-ids
  ;; :doc Produce the list of invite code ids.
  :<- [::invite-codes]
  (fn [codes _]
    (map :id codes)))

(re-frame/reg-sub ::invite-code
  ;; :doc Get data for one invite code.
  :<- [::invite-codes]
  (fn [codes [_ id]]
    (first (filter #(= (:id %) id) codes))))

(re-frame/reg-event-fx ::load-invite-codes
  ;; :doc Request a page of invite codes from the server.
  (fn-traced [{:keys [db]} [_ search]]
    (let [invite-codes (get-in db [:content :invite-codes search])
          {:keys [has-next-page end-cursor]} invite-codes]
      (when (or (nil? invite-codes) has-next-page)
        {:db (assoc-in db [:status :content]
                       (if (nil? invite-codes)
                         :loading :loaded))
         :fx [[:dispatch [::graphql/query
                          {:graphql graphql
                           :name :invite-codes
                           :variables {:first invite-code-table-request-size
                                       :after end-cursor
                                       :search (when (not= search :all) search)}
                           :handler [::receive-invite-codes
                                     {:search search}]}]]]}))))

(re-frame/reg-event-fx ::receive-invite-codes
  ;; :doc Receive a page of invite codes from the server.
  (fn-traced [{:keys [db]} [event {:keys [search response]}]]
    (let [{:keys [data errors]} response
          {:keys [invite-codes]} data
          existing-codes (get-in db [:content :invite-codes search])]
      {:db
       (cond-> db
         true (errors/assoc-errors {:event event :errors errors})
         true (assoc-in [:status :content] :loaded)
         (nil? errors) (-> (assoc-in [:content :invite-codes search]
                                     (update-invite-codes existing-codes
                                                          invite-codes))))
       :fx [[:dispatch-later [{:ms 100
                               :dispatch [::load-more/check-load-more]}]]]})))

(re-frame/reg-event-db ::set-bottom-of-invite-code-table
  ;; :doc Keep track of the bottom element of the invite code table in the db.
  (fn-traced [db [_ elem]]
    (assoc-in db [:ui-state :invite-code-table :bottom] elem)))

(re-frame/reg-event-fx ::invite-code-table-check-load-more
  ;; :doc Check whether more invite codes need to be loaded.
  (fn-traced [{:keys [db]} [_ _]]
    (let [search (get-in db [:view :options :search])
          elem (get-in db [:ui-state :invite-code-table :bottom])
          y (when elem (.-offsetTop elem))
          more? (get-in db [:content :invite-codes search :has-next-page])
          {:keys [height scroll-y]} (get-in db [:ui-state :window])]
      {:fx [(when (and more?
                       elem
                       (< (- y 500) (+ height scroll-y)))
              [:dispatch [::load-invite-codes search]])]})))

(re-frame/reg-event-fx ::generate-invite-code
  ;; :doc Generate a new invite code.
  (fn-traced [_ [_ {:keys [form]}]]
    (let [{:keys [code max-uses expiration]} form
          expires (when expiration (str (.getTime expiration)))]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :generate-invite-code
                         :variables {:code code
                                     :max_uses (js/parseInt max-uses)
                                     :expires expires}
                         :handler [::receive-generate-invite-code
                                   {:form form}]}]]]})))

(re-frame/reg-event-db ::receive-generate-invite-code
  ;; :doc Receive the response after generating an invite code.
  (fn-traced [db [_ {:keys [form response]}]]
    (let [{:keys [data errors]} response
          code (:generate-invite-code data)]
      (-> db
          (errors/assoc-errors {:event (:id form) :errors errors})
          (update-in [:content :invite-codes :all :codes] #(cons code %))
          (update-in [:content :invite-codes (:code code)] #(cons code %))
          (forms/transition-after-response (:id form))))))

(re-frame/reg-event-fx ::expire-invite-codes
  ;; :doc Change the expiration of selected invite codes
  [(re-frame/inject-cofx ::window/now)]
  (fn-traced [{:keys [now]} [_ {:keys [form]}]]
    (let [{:keys [selected option expiration]} form
          expires (when expiration (str (.getTime expiration)))
          expire-time (condp = option
                        :at expires
                        :never nil
                        :now (str now))]
      {:fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :expire-invite-codes
                         :variables {:codes selected :expires expire-time}
                         :handler [::receive-expire-invite-codes
                                   {:form form
                                    :expire-time expire-time}]}]]]})))

(re-frame/reg-event-fx ::receive-expire-invite-codes
  ;; :doc Receive the response after changing invite code expirations.
  (fn-traced [{:keys [db]} [_ {:keys [expire-time form response]}]]
    (let [{:keys [data errors]} response
          search (get-in db [:view :options :search])
          ids (set (:expire-invite-codes data))
          update-expire #(if (contains? ids (:id %))
                           (assoc % :expires expire-time)
                           %)]
      {:db (-> db
               (errors/assoc-errors {:event ::expire-invite-codes
                                     :errors errors})
               (update-in [:content :invite-codes search :codes]
                          #(map update-expire %))
               (forms/transition-after-response (:id form)))
       :fx [[:dispatch [::window/clock-update]]]})))

(re-frame/reg-event-fx ::search-invite-code
  ;; :doc Search for invite codes.
  (fn-traced [{:keys [db]} [_ {:keys [form]}]]
    (let [trimmed-code (-> form :code str/trim)]
      {:db (assoc-in db [:view :options :search] trimmed-code)
       :fx [[:dispatch [::load-invite-codes trimmed-code]]
            [:dispatch [::forms/edit ::admin-forms/expire-invite-code
                        :selected #{}]]]})))

(def ExpireInviteCodeForm
  [[:from :FormState]
   [:where [= [:_ ::admin-forms/expire-invite-code] :id]]])

(re-frame/reg-event-fx ::select-invite-code
  ;; :doc Toggle whether an invite code id is selected
  (fn-traced [{:keys [db]} [_ id selected?]]
    (let [selections (-> (:reldb db)
                         (rel/row ExpireInviteCodeForm)
                         :selected)
          new-selections (if selected?
                           (conj selections id)
                           (disj selections id))]
      {:fx [[:dispatch [::forms/edit ::admin-forms/expire-invite-code
                        :selected new-selections]]]})))

(re-frame/reg-event-fx ::show-all-invite-codes
  ;; :doc Show all invite codes, and deselect any that are selected.
  (fn-traced [{:keys [db]} _]
    {:db (assoc-in db [:view :options :search] :all)
     :fx [[:dispatch [::forms/edit ::admin-forms/expire-invite-code
                      :selected #{}]]]}))

;;; The admin user voting page

(defn- have-nil-update?
  [{:keys [update]} _]
  (and update (nil? (:total update))))

(defn- have-progress-update?
  [{:keys [update]} _]
  (and update (:total update)))

(defn- loaded-votes?
  [{:keys [loaded-votes?]} _]
  loaded-votes?)

(def vote-removal-machine
  "A statechart for the UI for the asynchronous back end vote remover."
  {:id :vote-removal
   :initial :loading
   :context nil
   :states
   {:loading {:on {:received-votes [{:target :show-votes
                                     :guard have-nil-update?}
                                    {:target :removing
                                     :guard have-progress-update?}]
                   :received-nil-update [{:target :show-votes
                                          :guard loaded-votes?}]
                   :received-progress-update [{:target :removing
                                               :guard loaded-votes?}]}}
    :show-votes {:on {:received-progress-update [{:target :removing}]
                      :start-remove-votes [{:target :removing}]}}
    :removing {:on {:received-nil-update [{:target :finished}]
                    :stop-remove-votes [{:target :stopping
                                         :guard have-progress-update?}
                                        {:target :finished
                                         :guard have-nil-update?}]}}
    :stopping {:on {:received-nil-update [{:target :show-votes
                                           :guard loaded-votes?}]
                    :received-votes [{:target :show-votes
                                      :guard have-nil-update?}]}}
    :finished {}}})
(assert (= vote-removal-machine (fsm/machine vote-removal-machine)))

(defn init-vote-removal-machine
  [db]
  (assoc-in db [:content :admin-voting :fsm]
            (fsm/initialize vote-removal-machine)))

(defn- transition-voting-fsm
  [state event]
  (fsm/transition vote-removal-machine state event
                  {:ignore-unknown-event? true}))

(re-frame/reg-event-fx ::subscribe-vote-removal
  ;; :doc Subscribe to updates on vote removal.
  (fn-traced [{:keys [db]} [_ name]]
    (let [is-admin? (get-in db [:current-user :attributes :is-admin])]
      {:db (update-in db [:subscriptions :stop-on-routing] conj
                      :vote-removal-update)
       :fx [(when is-admin?
              [:dispatch [::graphql/subscribe
                          {:graphql graphql
                           :name :vote-removal-update
                           :variables {:name name}
                           :handler [::receive-removal-update]}]])]})))

(re-frame/reg-event-db ::receive-removal-update
  ;; :doc Receive a vote removal progress update.
  (fn-traced [db [event {:keys [response]}]]
    (let [{:keys [data errors]} response
          update (:vote-removal-update data)
          fsm-event (if (some? (:remaining update))
                      :received-progress-update
                      :received-nil-update)]
      (cond-> db
        errors (errors/assoc-errors {:event event :errors errors})
        update (assoc-in [:content :admin-voting :fsm :update] update)
        update (update-in [:content :admin-voting :fsm]
                          transition-voting-fsm fsm-event)))))

(re-frame/reg-sub ::admin-voting-content
  ;; :doc Extract the content for the admin voting page.
  :<- [::internal/content]
  (fn [content _]
    (:admin-voting content)))

(re-frame/reg-sub ::vote-removal-state
  ;; :doc Extract the state of the vote removal process.
  :<- [::admin-voting-content]
  (fn [{:keys [fsm]} _]
    (:_state fsm)))

(re-frame/reg-sub ::removal-progress
  ;; :doc Extract the current vote counts.
  :<- [::admin-voting-content]
  (fn [{:keys [fsm]} _]
    (:update fsm)))

(re-frame/reg-sub ::removal-percent
  ;; :doc Calculate the percentage of votes removed.
  :<- [::removal-progress]
  (fn [{:keys [remaining total]} _]
    (when (pos-int? total)
      (int (/ (* (- total remaining) 100) total)))))

(def votes-page-size 50)

(re-frame/reg-event-fx ::load-votes
  ;; :doc Request a page of votes from the server.
  (fn-traced [{:keys [db]} [_ {:keys [name first last before after types
                                      paging?]}]]
    (let [first-page? (every? nil? [first last before after])
          types (case types
                  :post    [:POST]
                  :comment [:COMMENT]
                  :all     [:POST :COMMENT])]
      {:db (-> db
               (update-in [:content :admin-voting] dissoc :page-info)
               (update :reldb rel/transact [:delete :Vote]))
       :fx [[:dispatch [::graphql/query
                        {:graphql graphql
                         :name :votes
                         :variables {:name name
                                     :first (if first-page?
                                              votes-page-size
                                              first)
                                     :first_notifs 5
                                     :last last
                                     :before before
                                     :after after
                                     :types types}
                         :handler [::receive-votes {:paging? paging?}]}]]]})))

(defn- prepare-user [{:keys [post comment]}]
  (-> (or post comment)
      :author
      (update :status keyword)))

(defn prepare-user-stats [stats]
  (-> stats
      (dissoc :status)
      (update :joindate util/to-ISO-time)))

(defn- prepare-vote [{:keys [post comment] :as vote}]
  (let [post (or post (:post comment))]
    (-> vote
        (assoc :cid (:cid comment)
               :pid (:pid post))
        (assoc :sub-name (-> post :sub :name))
        (assoc :uid (-> (or comment post) :author :uid))
        (assoc :post-slug (:slug post))
        (update :direction keyword)
        (update :datetime util/to-ISO-time)
        (dissoc :post :comment))))

(defn- prepare-notif
  [notif]
  (-> notif
      :node
      (update :time util/to-ISO-time)))

(defn- update-db-with-votes-query
  "Unpack the results of the votes query and add to the db."
  [db {:keys [votes user-by-name modmail-notification-thread]}]
  (let [name (-> db :view :options :name)
        nodes (->> votes :edges (map :node))
        status (-> user-by-name :status keyword)
        stats (-> user-by-name prepare-user-stats)
        notifs (->> modmail-notification-thread :edges (map prepare-notif))
        more-notifs? (-> modmail-notification-thread :page-info :has-next-page)
        voter {:uid (:uid stats)
               :name name
               :status status}]
    (-> db
        (assoc-in [:content :admin-voting :page-info] (:page-info votes))
        (assoc-in [:content :admin-voting :more-notifs?] more-notifs?)
        (update :reldb rel/transact
                [:insert-or-replace :UserStats stats]
                [:insert-or-replace :User voter]
                (into [:insert-or-replace :User] (map prepare-user nodes))
                (into [:insert :Vote] (map prepare-vote nodes))
                [:delete :DownvoteNotification]
                (into [:insert :DownvoteNotification] notifs)))))

(re-frame/reg-event-fx ::receive-votes
  ;; :doc Receive a response to the admin votes query.
  (fn-traced [{:keys [db]} [event {:keys [paging? response]}]]
    (let [{:keys [data errors]} response]
      {:db (cond-> db
             errors (errors/assoc-errors {:event event :errors errors})
             true (assoc-in [:content :admin-voting :fsm :loaded-votes?] true)
             true (update-in [:content :admin-voting :fsm]
                             transition-voting-fsm :received-votes)
             (nil? errors) (update-db-with-votes-query data))
       :fx [(when paging?
              [::window/scroll-to [0 0]])]})))

(re-frame/reg-sub ::vote-data
  ;; :doc Extract the votes with user names.
  :<- [::internal/reldb]
  (fn [reldb  _]
    (rel/q reldb [[:from :Vote]
                  [:join :User {:uid :uid}]
                  [:sort [[util/to-epoch-time :datetime] :desc]]])))

(re-frame/reg-sub ::vote-page-info
  ;; :doc Extract the page-info object from the db.
  :<- [::admin-voting-content]
  (fn [content _]
    (:page-info content)))

(re-frame/reg-sub ::votes
  ;; :doc The current list of votes, with formatted times.
  :<- [::vote-data]
  (fn [votes _]
    (let [options (clj->js {"year" "numeric"
                            "month" "numeric"
                            "day" "numeric"
                            "hour" "numeric"
                            "minute" "numeric"})
          formatter (js/Intl.DateTimeFormat. js/undefined options)
          format-date #(.format formatter (js/Date. %))]
      (map #(update % :datetime format-date) votes))))

(re-frame/reg-event-fx ::show-vote-type
  ;; :doc Change the type or types of votes being viewed.
  (fn-traced [{:keys [db]} [_ new-types]]
    (let [{:keys [name types]} (-> db :view :options)]
      (when-not (= new-types types)
        {:fx [[::routes/set-url
               (route-util/url-for db :admin/voting
                                   :name name
                                   :query-args {:types new-types})]]}))))

(re-frame/reg-sub ::viewed-user-stats
  ;; :doc Get the stats and info for the admin voting page user.
  :<- [::internal/reldb]
  :<- [:view/options]
  (fn [[reldb {:keys [name]}] _]
    (-> (rel/q reldb [[:from :User]
                      [:where [= name :name]]
                      [:join :UserStats {:uid :uid}]])
        first)))

(re-frame/reg-sub ::downvote-notifications-raw
  ;; :doc Extract unformatted downvote notifications from the db.
  :<- [::internal/reldb]
  (fn [reldb _]
    (rel/q reldb [[:from :DownvoteNotification]
                  [:sort [:time :desc]]])))

(re-frame/reg-sub ::downvote-notifications
  ;; :doc Produce downvote notifications with HTML content.
  :<- [::downvote-notifications-raw]
  (fn [notifs _]
    (map (fn [{:keys [content] :as notif}]
           (assoc notif :html-content (when content
                                        (util/markdown-to-html content))))
         notifs)))

(re-frame/reg-sub ::more-downvote-notifications?
  ;; :doc Extract whether more downvote notifications exist than are shown.
  :<- [::admin-voting-content]
  (fn [content _]
    (:more-notifs? content)))

(re-frame/reg-sub ::has-votes?
  ;; :doc Determine whether the user has made any votes.
  :<- [::viewed-user-stats]
  (fn [{:keys [upvotes-given downvotes-given]} _]
    (pos? (+ upvotes-given downvotes-given))))

(re-frame/reg-event-db ::toggle-confirm-remove-votes
  ;; :doc Show the confirmation modal for removing user votes.
  (fn-traced [db [_ _]]
    (update-in db [:ui-state :admin-voting :confirm-remove-votes?] not)))

(re-frame/reg-sub ::show-confirm-remove-votes?
  ;; :doc Extract whether the remove votes confirmation is visible.
  :<- [::ui/state]
  (fn-traced [state [_ _]]
    (-> state :admin-voting :confirm-remove-votes?)))

(re-frame/reg-event-fx ::remove-votes
  ;; :doc Start the process of removing votes.
  (fn-traced [{:keys [db]} [event _]]
    (let [name (-> db :view :options :name)]
      {:db (-> db
               (assoc-in [:ui-state :admin-voting :confirm-remove-votes?]
                         false)
               (update :reldb rel/transact [:delete :Vote])
               (update-in [:content :admin-voting] dissoc :page-info)
               (update-in [:content :admin-voting :fsm]
                          transition-voting-fsm :start-remove-votes))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :start-remove-votes
                         :variables {:name name}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event}]}]]]})))

(re-frame/reg-event-fx ::stop-remove-votes
  ;; :doc Interrupt the removal of votes on the server.
  (fn-traced [{:keys [db]} [event _]]
    (let [options (-> db :view :options)]
      {:db (-> db
               (update-in [:content :admin-voting :fsm] dissoc :loaded-votes?)
               (update-in [:content :admin-voting :fsm]
                          transition-voting-fsm :stop-remove-votes))
       :fx [[:dispatch [::graphql/mutate
                        {:graphql graphql
                         :name :stop-remove-votes
                         :variables {:name (:name options)}
                         :handler [::graphql/receive-mutation-handler
                                   {:event event}]}]]
            [:dispatch-later
             {:ms 1000
              :dispatch [::load-votes options]}]]})))
