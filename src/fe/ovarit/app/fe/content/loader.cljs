;; fe/content/loader.cljs -- Content loader state machine for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.content.loader
  (:require [clojure.set :as set]
            [clojure.spec.alpha :as spec]
            [day8.re-frame.tracing :refer-macros [fn-traced]]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.ui.window :as window]
            [ovarit.app.fe.util :as util]
            [ovarit.app.fe.util.relic :as rel]
            [re-frame.core :as re-frame]
            [statecharts.core :as fsm]
            [weavejester.dependency :as dep])
  (:require [ovarit.app.fe.content.loader.content-status-fields
             :as-alias content-status-fields]))

;; ContentStatus objects in the reldb.

(defmulti content-status-id
  "Create a unique identifier for a reldb ContentStatus object."
  :content-type)

;; relic doesn't like methods in its expressions.
(defn get-content-status-id
  "Create a unique identifier for a reldb ContentStatus object."
  [obj]
  (content-status-id obj))

;; Specs for ContentStatus objects in the reldb.

(defmulti content-status :content-type)
(spec/def ::content-status
  (spec/multi-spec content-status :content-type))
(spec/def ::content-status-fields/status #{:loading :success :failure})
(spec/def ::content-status-fields
  (spec/keys :req-un [::content-status-fields/status]))

;; Statechart for loading content.

(defn load-more-fx
  "Determine which events can be started.
  Remove them from the graph, and attach events for them to the :fx key."
  [{:keys [in-flight graph] :as state} _event]
  (let [nodes (dep/nodes graph)
        ready (set/difference nodes
                              (dep/transitive-dependents-set graph nodes)
                              in-flight)
        fx (map (fn [node] [:dispatch node])
                ready)]
    (-> state
        (update :fx concat fx)
        (update :in-flight set/union ready))))

(defn all-data-loaded?
  "Determine if all events that can be run have been run."
  [state event]
  (and (empty? (:in-flight state))
       (empty? (:fx (load-more-fx state event)))))

(defn any-errors?
  "Have any errors been recorded by completed tasks?"
  [state _event]
  (some #(= :error (val %)) (:completed state)))

(def content-loader-machine
  "A statechart for loading content."
  {:id :content
   :initial :initial
   :context nil
   :states
   {:initial {:on {:start [{:target :loaded
                            :guard all-data-loaded?}
                           {:target :loading
                            :actions [(fsm/assign load-more-fx)]}]}}
    :loading {:on {:task-completed [{:target :error
                                     :guard any-errors?}
                                    {:target :loaded
                                     :guard all-data-loaded?}
                                    {:target :loading
                                     :actions [(fsm/assign load-more-fx)]}]}}
    :error {:on {:task-completed [{:target :error}]}}
    :loaded {:on {:task-completed [{:target :loaded}]}}}})
(assert (= content-loader-machine (fsm/machine content-loader-machine)))

(re-frame/reg-event-fx ::init-content-loader
  ;; :doc Initialize the content loader with a dependency graph.
  ;; :doc Will dispatch any events from the graph which have
  ;; :doc no dependencies left.
  (fn-traced [{:keys [db]} [_ {:keys [graph]}]]
    (let [initial (fsm/initialize content-loader-machine
                                  {:context {:epoch (:epoch db)
                                             :graph graph
                                             :completed {}}})
          started (fsm/transition content-loader-machine initial
                                  {:type :start})]
      (log/debug {:state (:_state started) :events (:fx started)}
                 "Content loader initialized")
      {:db (assoc-in db [:status :content-loader] (dissoc started :fx))
       :fx (vec (:fx started))})))

(re-frame/reg-event-db ::cleanup
  ;; :doc Remove the content loader from the db.
  (fn-traced [db _]
    (-> db
        (update :status dissoc :content-loader)
        (update :status assoc :content :not-loaded))))

(re-frame/reg-event-fx ::update-on-completion
  ;; :doc Advance the content loader after a dependency is completed.
  ;; :doc Will dispatch any events from the graph which are now
  ;; :doc ready.
  (fn-traced [{:keys [db]} [_ completed-event status]]
    (let [error? (= :error status)
          state (some-> (get-in db [:status :content-loader])
                        (update :completed assoc completed-event
                                (if error? :error :done))
                        (update :in-flight disj completed-event)
                        (cond-> (not error?)
                          (update :graph dep/remove-all completed-event)))
          new-state (when state
                      (fsm/transition content-loader-machine state
                                      {:type :task-completed}))]
      (log/debug {:completed-event completed-event :status status
                  :new-state new-state} "Content loader updated")
      {:db (cond-> db
             new-state
             (assoc-in [:status :content-loader] (dissoc new-state :fx)))
       :fx (vec (:fx new-state))})))

(re-frame/reg-sub ::content-loader
  :<- [::window/statuses]
  (fn [statuses _]
    (:content-loader statuses)))

(re-frame/reg-sub ::content-loading?
  :<- [::content-loader]
  (fn [content-loader _]
    (let [state (:_state content-loader)]
      (or (nil? state) (= :loading state)))))

;; Register events to work with the content loader.

(defn- setup-content-status-fn [id make-content-status]
  (fn [{:keys [db]} [_ _]]
    (let [new-status (some-> (make-content-status db)
                             (assoc :status :loading))
          status-id (when new-status
                      (get-content-status-id new-status))
          existing (when status-id
                     (rel/row (:reldb db)
                              [[:from :ContentStatus]
                               [:where
                                [= [:_ status-id] [get-content-status-id]]]]))]
      (log/debug {:existing existing :new new-status}
                 "setup load status")
      (if (= :success (:status existing))
        {:fx [[:dispatch [::update-on-completion
                          [(util/add-prefix id :load)] :success]]]}
        {:db (cond-> db
               new-status
               (update :reldb rel/transact
                       [:insert-or-replace :ContentStatus new-status]))
         :fx [[:dispatch [(util/add-prefix id :internal)]]]}))))

(defn- complete-status-fn [id make-content-status]
  (fn-traced [{:keys [db]} [_ status]]
    (let [status-id (when make-content-status
                      (some-> db make-content-status get-content-status-id))]
      {:db (cond-> db
             status-id (update :reldb rel/transact
                               [:update :ContentStatus {:status [:_ status]}
                                [= [:_ status-id] [get-content-status-id]]]))
       :fx [[:dispatch [::update-on-completion
                        [(util/add-prefix id :load)] :success]]]})))

(defn reg-loader
  "Register a set of re-frame events to load some content."
  [id {:keys [make-content-status load receive]}]
  (when make-content-status
    (re-frame/reg-event-fx (util/add-prefix id :load)
      (setup-content-status-fn id make-content-status)))

  (re-frame/reg-event-fx (util/add-prefix id (if make-content-status
                                               :internal :load))
    load)

  (re-frame/reg-event-fx (util/add-prefix id :receive)
    receive)

  (re-frame/reg-event-fx (util/add-suffix id :completed)
    (complete-status-fn id make-content-status)))
