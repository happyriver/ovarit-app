;; fe/tr.cljs -- Translations for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.tr
  (:require
   [clojure.string :as str]
   [goog.string :as gstring]
   [goog.string.format]
   [re-frame.core :as re-frame]
   [ovarit.app.fe.log :as log])
  #_(:require-macros
     [ovarit.app.fe.tr :refer [inline-dict]]))

(def DICT
  {}
  #_{:es (inline-dict "translations/es/LC_MESSAGES/messages.po")
     :ru (inline-dict "translations/ru/LC_MESSAGES/messages.po")})

(re-frame/reg-sub ::lang
  ;; :doc Determine the current language from the db.
  (fn [db _]
    (or (get-in db [:current-user :language]) :en)))

(defn- lang [lang-or-db]
  (if (keyword? lang-or-db)
    lang-or-db
    (or (get-in lang-or-db [:current-user :language]) :en)))

(defn- tr- [lang s args]
  (let [string (or (get-in DICT [lang s]) s)]
    (apply gstring/format string args)))

(defn tr
  "Translate a string and interpolate arguments.
  For reactive contexts only."
  [s & args]
  (let [lang @(re-frame/subscribe [:settings/current-language])]
    (tr- lang s args)))

(defn trx
  "Version of `tr` for non-reactive contexts.
  Pass the language keyword if you have it, or the entire db."
  [lang-or-db s & args]
  (tr- (lang lang-or-db) s args))

(defn- trn-
  [lang strings count args]
  (let [[singular plural] (or (get-in DICT [lang strings]) strings)]
    (if (= 1 count)
      (apply gstring/format singular (conj args count))
      (apply gstring/format plural (conj args count)))))

(defn trn
  "Translate a string with singular and plural options.
  For reactive contexts only."
  [strings count & args]
  (let [lang @(re-frame/subscribe [:settings/current-language])]
    (trn- lang strings count args)))

(defn trnx
  "Version of `trn` for non-reactive contexts."
  [lang-or-db strings count & args]
  (trn- (lang lang-or-db) strings count args))

(defn- lookup
  "Return the value from `items` which corresponds to the key in `match`.
  Expects `items` to be a map and `match` to be a regular expression
  match vector, and `fmt` a string to use in the error message if
  the key is not present in `items`."
  [fmt items match]
  (let [tag (nth match 1)
        kw (keyword tag)]
    (if (contains? items kw)
      (kw items)
      (do
        (log/error {:tag tag :fmt fmt} "interpolated value not found")
        "error"))))

(defn- lookup-values
  "Return a list of values from 'items' matching the keys in `fmt`.
  A key is a variable name (alphanumerics, - and _) within curly
  braces."
  [fmt items]
  (map (partial lookup fmt items)
       (re-seq #"\{([-a-zA-Z_]+)\}" fmt)))

(defn- trm-
  [lang fmt m]
  (let [string (or (get-in DICT [lang fmt]) fmt)
        values (lookup-values fmt m)
        format-str (str/replace string #"\{[-a-zA-Z_]+\}" "s")]
    (apply gstring/format (into [format-str] values))))

(defn trm
  "Translate with simple string interpolation from map keys and values.
  Supply the keys to be interpolated as %{key} within the string
  and supply a map with keywords matching the strings within the
  curly braces.

  Use in reactive contexts only."
  [fmt m]
  (let [lang @(re-frame/subscribe [:settings/current-language])]
    (trm- lang fmt m)))

(defn trmx
  "Version of `trm` for non-reactive contexts."
  [lang-or-db fmt m]
  (trm- (lang lang-or-db) fmt m))

(defn- trm-html-
  [lang fmt m]
  (let [string (or (get-in DICT [lang fmt]) fmt)
        chunks (str/split string #"(%\{[a-zA-Z]+})")]
    (into [:<>]
          (map #(if-let [match (re-matches #"%\{([-a-zA-Z_]+)\}" %)]
                  (lookup fmt m match)
                  %)
               chunks))))

(defn trm-html
  "Translate with simple string interpolation from map keys and values.
  Return a hiccup fragment. Use in reactive contexts only."
  [fmt m]
  (let [lang @(re-frame/subscribe [:settings/current-language])]
    (trm-html- lang fmt m)))

(defn trmx-html
  "Version of `trm-html for non-reactive contexts`."
  [lang-or-db fmt m]
  (trm-html- (lang lang-or-db) fmt m))
