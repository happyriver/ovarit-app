;; fe/ui/window.cljs -- Global user-interface events for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.window
  (:require [clojure.string :as str]
            [day8.re-frame.tracing :refer-macros [fn-traced]]
            [goog.events :as events]
            [goog.net.Cookies]
            [ovarit.app.fe.content.load-more :as content]
            [ovarit.app.fe.errors :as errors]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.views.sub-bar :as-alias sub-bar]
            [ovarit.app.fe.views.util :refer [cls]]
            [re-frame.core :as re-frame])
  (:import [goog.events BrowserEvent]))

;; Translate JS events into re-frame events.

(def listeners (atom {}))

(defn- setup-listener! [event-type callback id]
  (let [key (events/listen js/window event-type
                           (if (fn? callback)
                             callback
                             #(re-frame/dispatch callback)))]
    (when id
      (swap! listeners assoc-in [event-type id] key))))

(defn- stop-listener! [event-type id]
  (when-let [key (get-in @listeners [event-type id])]
    (events/unlistenByKey key)
    (swap! listeners update event-type #(dissoc % id))))

(defn on-resize [{:keys [event debounce-ms]} timer]
  (js/clearTimeout @timer)
  (reset! timer (js/setTimeout
                 #(re-frame/dispatch
                   (into event [js/window.innerWidth js/window.innerHeight]))
                 (or debounce-ms 166))))

(re-frame/reg-fx :window/on-resize
  ;; :doc Set up an event to dispatch when the window is resized.
  (fn [{:keys [id] :as config}]
    (let [timer (atom nil)]
      (setup-listener! events/EventType.RESIZE
                       (partial on-resize config timer) id))))

(re-frame/reg-fx :window/stop-on-resize
  (fn [{:keys [id]}]
    (stop-listener! events/EventType.RESIZE id)))

(defn on-scroll [{:keys [event debounce-ms]} timer]
  (js/clearTimeout @timer)
  (reset! timer (js/setTimeout
                 #(re-frame/dispatch
                   (into event [js/window.pageYOffset
                                js/window.pageXOffset]))
                 (or debounce-ms 166))))

(re-frame/reg-fx :window/on-scroll
  (fn [{:keys [id] :as config}]
    (let [timer (atom nil)]
      (setup-listener! events/EventType.SCROLL
                       (partial on-scroll config timer) id))))

(re-frame/reg-fx :window/stop-on-scroll
  (fn [{:keys [id]}]
    (stop-listener! events/EventType.SCROLL id)))

(re-frame/reg-fx :window/on-focus
  (fn [{:keys [dispatch id]}]
    (setup-listener! events/EventType.FOCUS dispatch id)))

(re-frame/reg-fx :window/stop-on-focus
  (fn [{:keys [id]}]
    (stop-listener! events/EventType.FOCUS id)))

(re-frame/reg-fx :window/on-message
  (fn-traced [{:keys [dispatch id]}]
    (setup-listener! events/EventType.MESSAGE dispatch id)))

(re-frame/reg-fx :window/stop-on-message
  (fn-traced [{:keys [id]}]
    (stop-listener! events/EventType.MESSAGE id)))

(re-frame/reg-fx :window/on-becoming-visible
  (fn-traced [{:keys [event id]}]
    (setup-listener! events/EventType.VISIBILITYCHANGE
                     #(re-frame/dispatch [::becoming-visible-check event])
                     id)))

(re-frame/reg-fx :window/stop-on-becoming-visible
  (fn-traced [{:keys [id]}]
    (stop-listener! events/EventType.VISIBILITYCHANGE id)))

(re-frame/reg-cofx ::visibility
  (fn [coeffects _]
    (assoc coeffects :visibility (-> (.-visibilityState js/document)
                                     keyword))))

(re-frame/reg-event-fx ::becoming-visible-check
  ;; :doc Dispatch an event when the document becomes visible.
  [(re-frame/inject-cofx ::visibility)]
  (fn-traced [{:keys [db visibility]} [_ event]]
    (let [previous-val (get-in db [:window :visibility])]
      {:db (assoc-in db [:window :visibility] visibility)
       :fx [(when (and (= :visible visibility) (= :hidden previous-val))
              [:dispatch event])]})))

;; User Interface Events

(re-frame/reg-event-db ::toggle-menu
  (fn-traced [db [_ _]]
    (update-in db [:ui-state :menu] #(if (= % :closed) :open :closed))))

(re-frame/reg-event-db ::close-menu
  (fn-traced [db [_ _]]
    (assoc-in db [:ui-state :menu] :closed)))

(re-frame/reg-sub ::show-user-flair-modal?
  ;; :doc Whether to show the modal change user flair form.
  (fn [db _]
    (get-in db [:ui-state :user-flair-modal])))

(re-frame/reg-event-db ::toggle-user-flair-modal
  ;; :doc Toggle display of the modal change user flair form.
  (fn-traced [db [_ _]]
    (update-in db [:ui-state :user-flair-modal] not)))

(re-frame/reg-event-fx ::window-resized
  ;; :doc Dispatch necessary events when the window is resized.
  ;; :doc Dismiss the hamburger menu if open when the width changes,
  ;; :doc because that might affect its visibility.  If on an infinite
  ;; :doc scroll page, see if more content needs to be loaded.
  (fn-traced [{:keys [db]} [_ width height]]
    (let [old-width (get-in db [:ui-state :window :width])]
      {:db (update-in db [:ui-state :window] assoc
                      :width width :height height)
       :fx [(when (not= width old-width)
              [:dispatch [::close-menu]])
            [:dispatch [::content/check-load-more]]]})))

(re-frame/reg-event-fx ::window-scrolled
  (fn-traced [{:keys [db]} [_ scroll-y scroll-x]]
    {:db (update-in db [:ui-state :window] assoc
                    :scroll-y scroll-y :scroll-x scroll-x)
     :fx [[:dispatch [::content/check-load-more]]]}))

(re-frame/reg-fx ::scroll-to-elem
  ;; :doc Scroll to a DOM element.
  (fn [elem]
    (.scrollIntoView elem)))

(re-frame/reg-fx ::scroll-elem-to
  ;; :doc Scroll so the top of a DOM element is at the desired position.
  ;; :doc `y` should be in client (window) coordinates.
  (fn [{:keys [elem y]}]
    (when elem
      (let [top (.. elem getBoundingClientRect -top)]
        (.scrollBy js/window 0 (- top y))))))

(re-frame/reg-fx ::scroll-to
  ;; :doc Scroll to the given coordinates.
  (fn [[x y]]
    (.scrollTo js/window x y)))

(re-frame/reg-cofx ::height
  ;; :doc Get current window height.
  (fn [cofx _]
    (assoc cofx :window-height (.-innerHeight js/window))))

(re-frame/reg-sub ::dimensions
  ;; :doc Extract last saved window dimensions from the db.
  (fn [db _]
    (-> db
        (get-in [:ui-state :window])
        (select-keys [:height :width]))))

(re-frame/reg-sub ::menu-state
  (fn [db _]
    (get-in db [:ui-state :menu])))

(re-frame/reg-event-db ::after-load
  (fn-traced [db [_ dimensions]]
    (assoc-in db [:ui-state :window] dimensions)))

(re-frame/reg-event-fx ::start-resize-watcher
  (fn-traced [_ [_ _]]
    {:window/on-resize {:event [::window-resized]
                        :debounce-ms 200}}))

(re-frame/reg-event-fx ::start-scroll-watcher
  (fn-traced [_ [_ _]]
    {:window/on-scroll {:event [::window-scrolled]
                        :debounce-ms 200}}))

(re-frame/reg-event-fx ::start-focus-watcher
  (fn-traced [_ [_ _]]
    {:window/on-focus {:dispatch [::load-daynight-cookie]}}))

(re-frame/reg-event-fx ::load-daynight-cookie
  ;; :doc Get the day-night mode from the cookie and save it in the db.
  [(re-frame/inject-cofx ::daynight-cookie)]
  (fn-traced [{:keys [db daynight-cookie]} [_ _]]
    (let [db' (assoc-in db [:settings :day-night] daynight-cookie)]
      {:db db'
       ::update-body-class db'})))

(re-frame/reg-cofx ::daynight-cookie
  ;; :doc Get the value of the day-night cookie as a coeffect.
  (fn [cofx _]
    (assoc cofx :daynight-cookie
           (let [cookie (.get (goog.net.Cookies. js/document) "dayNight")]
             (if (#{"dark" "dank" "light"} cookie)
               (keyword cookie)
               (do
                 (when cookie
                   (log/warn {:cookie cookie} "Unrecognized daynight value"))
                 :light))))))

(re-frame/reg-event-fx ::toggle-daynight
  ;; :doc Toggle daynight mode.
  (fn-traced [{:keys [db]} [_ _]]
    (let [value (if (= :light (get-in db [:settings :day-night])) :dark :light)
          db' (assoc-in db [:settings :day-night] value)]
      {:db db'
       :fx [[::update-body-class db']
            [::set-daynight-cookie value]]})))

(re-frame/reg-event-fx ::set-daynight-to
  ;; :doc Set the daynight mode.
  (fn-traced [{:keys [db]} [_ strval]]
    (let [value (if (#{"dark" "dank" "light"} strval)
                  (keyword strval)
                  (do
                    (when strval
                      (log/warn {:strval strval} "Unrecognized daynight value"))
                    :light))
          db' (assoc-in db [:settings :day-night] value)]
      {:db db'
       :fx [[::update-body-class db']
            [::set-daynight-cookie value]]})))

(re-frame/reg-event-fx ::set-force-day-mode
  ;; :doc Set or clear an override of the day-night cookie
  (fn-traced [{:keys [db]} [_ val]]
    (let [db' (assoc-in db [:view :force-day-mode?] val)]
      {:db db'
       :fx [[::update-body-class db']]})))

(re-frame/reg-fx ::update-body-class
  ;; :doc Update the document body class for day-night mode.
  (fn-traced [{:keys [settings view current-user]}]
    (let [{:keys [day-night enable-totp]} settings
          {:keys [force-day-mode?]} view
          is-admin? (get-in current-user [:attributes :is-admin])
          class (str/join " "
                          [(cond
                             force-day-mode? ""
                             (= day-night :dark) "dark"
                             (= day-night :dank) "dark dank"
                             :else "")
                           (when (and enable-totp is-admin?)
                             "body_admin")])]
      (set! (.. js/document -body -classList) class))))

(re-frame/reg-fx ::set-daynight-cookie
  ;; :doc Set the day-night cookie.
  (let [one-year (* 365 24 60 60)]
    (fn-traced [value]
      (.set (goog.net.Cookies. js/document) "dayNight" (name value)
            #js {:maxAge one-year :path "/"}))))

(defn listen-day-night
  "Listen for a day-night message.
  Used when embedded as an iframe."
  [goog-event]
  (let [event (.getBrowserEvent ^BrowserEvent goog-event)
        msg (-> (.-data event)
                js/JSON.parse
                (js->clj :keywordize-keys true))
        {:keys [msgtype value]} msg]
    (when (and (= (.-origin event) js/location.origin) (= "dayNight" msgtype))
      (re-frame/dispatch [::set-daynight-to value]))))

(re-frame/reg-sub ::day-night
  ;; :doc Extract the day-night mode.
  (fn [db _]
    (get-in db [:settings :day-night])))

(re-frame/reg-sub ::view
  (fn [db _]
    (:view db)))

(re-frame/reg-sub ::force-day-mode?
  :<- [::view]
  (fn [view _]
    (:force-day-mode? view)))

(re-frame/reg-sub ::day?
  ;; :doc Determine whether we're in light mode.
  :<- [::day-night]
  :<- [::force-day-mode?]
  (fn [[day-night force-day-mode?] _]
    (or force-day-mode? (= :light day-night))))

(re-frame/reg-sub ::flair-colors
  ;; :doc Calculate flair colors.
  :<- [::day?]
  (fn [day? [_ selected?]]
    (cls (if day?
           (if selected?
             :white.bg-dark-gray.hover-bg-mid-gray
             :black.bg-light-gray.hover-bg-moon-gray)
           (cls :ba.bw1.hover-bg-mid-gray
                (if selected?
                  :light-gray.bg-near-black.b--mid-gray
                  :gray.bg-dark-gray.b--dark-gray))))))

(re-frame/reg-sub ::user-flair-colors
  ;; :doc Colors for user flairs.
  :<- [::day?]
  (fn [day? [_ {:keys [selectable? selected?]}]]
    (let [base (if day?
                 :dark-gray.bg-light-gray
                 :gray.bg-black.ba.bw1)
          hover (if day?
                  :hover-near-black.hover-bg-silver
                  :hover-mid-gray.hover-bg-moon-gray)
          selected (if day?
                     :white.bg-dark-gray.grow
                     :black.bg-light-gray)]
      (cls (if selected? selected base)
           (when selectable? hover)))))

(re-frame/reg-sub ::post-flair-colors
  ;; :doc Colors for post flairs.
  :<- [::day?]
  (fn [day? [_ {:keys [selected?]}]]
    (let [base (if day?
                 :dark-gray.bg-light-gray
                 :gray.bg-black.ba.bw1)
          hover (if day?
                  :hover-near-black.hover-bg-silver
                  :hover-mid-gray.hover-bg-moon-gray)
          selected (if day?
                     :white.bg-dark-gray.grow
                     :black.bg-light-gray)]
      (cls (if selected? selected base) hover))))

(re-frame/reg-sub ::distinguish-colors
  ;; :doc Colors for distinguishing authors
  :<- [::day?]
  (fn [day? [_ distinguish]]
    (when distinguish
      (cls :white.hover-white
           (get-in {:ADMIN {true :bg-orangey-brown
                            false :bg-reddish-brown}
                    :MOD {true :bg-green
                          false :bg-dark-green}}
                   [distinguish day?])))))

(re-frame/reg-sub ::emphasis-background
  :<- [::day?]
  (fn [day? _]
    (if day? :bg-moon-gray :bg-dark-gray)))

(def button-color-map
  {true  {:primary    :near-white.bg-purple.hover-bg-light-purple
          :sidebar    :near-white.bg-dark-gray.hover-near-white.hover-bg-mid-gray
          :secondary  :black.bg-light-gray.hover-black.hover-bg-near-white
          :modal      :bg-moon-gray.hover-bg-light-silver
          :subscribed :white.bg-purple.hover-bg-green
          :subscribe  :white.bg-green.hover-bg-purple
          :block      :white.bg-blue.hover-bg-dark-red
          :blocked    :white.bg-dark-red.hover-bg-blue}
   false {:primary    :silver.bg-purple.hover-bg-mid-gray
          :sidebar    :light-silver.bg-dark-purple.hover-light-silver.hover-bg-purple
          :secondary  :silver.bg-dark-gray.hover-light-silver.hover-bg-mid-gray
          :modal      :light-silver.bg-mid-gray.hover-bg.gray
          :subscribed :light-gray.bg-dark-purple.hover-bg-dark-green
          :subscribe  :light-gray.bg-dark-green.hover-bg-dark-purple
          :block      :light-gray.bg-dark-blue.hover-bg-reddish-brown
          :blocked    :light-gray.bg-reddish-brown.hover-bg-dark-blue}})

(re-frame/reg-sub ::button-colors
  ;; :doc Calculate button colors.
  ;; :doc Type can be :primary (colorful in day mode),
  ;; :doc :sidebar (indicating selection on the sidebar, colorful in night
  ;; :doc mode), :secondary (muted) or :modal (a little more contrasty for
  ;; :doc use on the backgrounds that ::form-background below makes.)
  :<- [::day?]
  (fn [day? [_ type]]
    (cls (get-in button-color-map [day? (or type :primary)]))))

(re-frame/reg-sub ::input-colors
  ;; :doc Calculate input colors.
  :<- [::day?]
  (fn [day? [_ disabled?]]
    (if day?
      (if disabled?
        :silver.bg-light-gray.b--light-silver
        :black.bg-white.b--moon-gray)
      (if disabled?
        :moon-gray.bg-mid-gray.b--gray
        :light-gray.bg-dark-gray.b--mid-gray))))

(re-frame/reg-sub ::drag-target-colors
  ;; :doc Calculate drag target colors.
  :<- [::day?]
  (fn [day? [_ targeting-state]]
    (let [bg (if day? :bg-light-gray :bg-dark-gray)]
      (case targeting-state
        :on :outline-green.bg-washed-green
        :off (cls bg)
        ;; ioS Safari won't tell you what's being dragged
        ;; until it's droppped on you.
        :no-file-detected (cls bg)))))

(re-frame/reg-sub ::form-background
  ;; :doc Calculate background colors for forms and modals.
  :<- [::day?]
  (fn [day? _]
    (if day?
      :bg-near-white
      :bg-near-black)))

(re-frame/reg-sub ::progress-bar-colors
  ;; :doc Calculate colors for progress bars.
  :<- [::day?]
  (fn [day? _]
    (if day?
      {:foreground :bg-purple
       :background :bg-moon-gray}
      {:foreground :bg-dark-purple
       :background :bg-mid-gray})))

(re-frame/reg-sub ::table-header-colors
  ;; :doc Colors for user-interface elements.
  :<- [::day?]
  (fn [day? _]
    (if day?
      :black.bg-light-silver
      :silver.bg-dark-gray)))

(re-frame/reg-sub ::table-label-colors
  :<- [::day?]
  (fn [day? _]
    (if day?
      :black.bg-light-gray
      :silver.bg-dark-gray)))

(re-frame/reg-sub ::table-body-colors
  :<- [::day?]
  (fn [day? _]
    (if day?
      :black.bg-near-white
      :silver.bg-near-black)))

(re-frame/reg-sub ::table-border-colors
  :<- [::day?]
  (fn [day? _]
    (if day?
      :b--moon-gray
      :b--mid-gray)))

(re-frame/reg-sub ::todo-highlight-color
  :<- [::day?]
  (fn [day? _]
    (if day?
      :bg-washed-yellow
      :bg-dark-reddish-brown)))

(re-frame/reg-sub ::select-input-colors
  :<- [::day-night]
  (fn [daynight _]
    (if (= :light daynight)
      :dark-gray.bg-moon-gray.b--moon-gray.focus-b--dark-gray
      :silver.bg-dark-gray.b--dark-gray.focus-b--silver)))

(re-frame/reg-sub ::success-colors
  ;; :doc Colors for success messages.
  :<- [::day?]
  (fn [day? _]
    (if day?
      :white.bg-light-green
      :white.bg-dark-green)))

(re-frame/reg-sub ::toast-colors
  ;; :doc Colors for error toasts.
  :<- [::day?]
  (fn [day? _]
    (if day?
      :black.bg-white
      :silver.bg-dark-gray)))

(re-frame/reg-sub ::toast-border-color
  ;; :doc Color of the border dividing toast tops and bottoms.
  :<- [::day?]
  (fn [day? _]
    (if day? :b--moon-gray :b--mid-gray)))

(re-frame/reg-sub ::toast-close-color
  ;; :doc Fill for the toast close icon.
  :<- [::day?]
  (fn [day? _]
    (when-not day? :fill-light-silver)))

(re-frame/reg-sub ::icon-fill
  ;; :doc Colors for icons.
  :<- [::day?]
  (fn [day? _]
    (if day?
      :fill-dark-gray
      :fill-moon-gray)))

(def color-map
  {true
   {::sub-bar/admin-alert         :white.bg-orangey-brown
    ::sub-bar/admin-alert.disable :black.bg-transparent
    ::sub-bar/bar                 :bg-near-black.b--near-black
    ::sub-bar/element             :light-gray
    ::sub-bar/icon-fill           :fill-light-gray
    ::sub-bar/link                :light-gray
    ::sub-bar/moremenu.icon-fill  :fill-light-gray
    ::sub-bar/moremenu.link       :dark-purple.hover-purple.hover-bg-near-black
    ::sub-bar/moremenu.menu       :bg-white.b--white}

   false
   {::sub-bar/admin-alert         :white.bg-brown
    ::sub-bar/admin-alert.disable :black.bg-transparent
    ::sub-bar/bar                 :bg-near-black.b--dark-purple
    ::sub-bar/element             :light-gray
    ::sub-bar/icon-fill           :fill-light-purple
    ::sub-bar/link                :light-gray.hover-light-purple
    ::sub-bar/moremenu.icon-fill  :fill-light-gray
    ::sub-bar/moremenu.link       :dark-purple.hover-purple.hover-bg-dark-gray
    ::sub-bar/moremenu.menu       :bg-black.b--dark-gray}})

(re-frame/reg-sub ::color
  ;; :doc Colors for UI elements.
  :<- [::day?]
  (fn [day? [_ elem-kw]]
    (get-in color-map [day? elem-kw])))

(re-frame/reg-event-fx ::clock-tick
  ;; :doc Update the time stored in the db every 5 seconds.
  [(re-frame/inject-cofx ::now)]
  (fn [{:keys [db now]} _]
    {:db (assoc db :now now)
     :fx [[:dispatch [::errors/retry-and-cleanup]]
          [:dispatch-later [{:ms 5000 :dispatch [::clock-tick]}]]]}))

(re-frame/reg-event-fx ::clock-update
  ;; :doc Update the time stored in the db.
  [(re-frame/inject-cofx ::now)]
  (fn [{:keys [db now]} _]
    {:db (assoc db :now now)}))

(re-frame/reg-cofx ::now
  (fn [cofx _]
    (assoc cofx :now (.getTime (js/Date.)))))

(re-frame/reg-sub ::now
  (fn [db _]
    (:now db)))

(re-frame/reg-sub ::statuses
  ;; :doc Get the progress of loading page content.
  (fn [db _]
    (:status db)))

(re-frame/reg-sub ::status
  ;; :doc Get the progress of loading page content.
  :<- [::statuses]
  (fn [statuses _]
    (:content statuses)))

(re-frame/reg-event-fx ::start-watchers
  ;; :doc Start watching JaveScript window and document events.
  (fn-traced [_ _]
    {:fx [[:dispatch [::start-resize-watcher]]
          [:dispatch [::start-scroll-watcher]]
          [:dispatch [::start-focus-watcher]]]}))
