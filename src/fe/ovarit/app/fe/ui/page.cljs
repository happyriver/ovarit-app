;; fe/ui/page.cljs -- Set page elements for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.page
  (:require [day8.re-frame.tracing :refer-macros [fn-traced]]
            [re-frame.core :as re-frame]))

(re-frame/reg-fx ::create-meta-robots
  ;; :doc Add a <meta name="robots" content="noindex"> to the HTML head.
  (fn-traced []
    (let [existing (. js/document (querySelector "meta[name=\"robots\"]"))]
      (when-not existing
        (let [elem (or existing (. js/document (createElement "meta")))
              head (. js/document -head)]
          (set! (.. elem -name) "robots")
          (set! (.. elem -content) "noindex")
          (. head (appendChild elem)))))))

(re-frame/reg-event-fx ::stop-robots
  ;; :doc Tell the googlebot not to index this page.
  (fn-traced [_ _]
    {:fx [[::create-meta-robots]]}))
