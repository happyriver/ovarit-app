;; fe/ui/forms/compose-message.cljs -- Compose message form for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.compose-message
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.message :as message]
   [ovarit.app.fe.content.modmail :as-alias modmail]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame])
  (:require
   [ovarit.app.fe.ui.forms.compose-message.compose-modmail
    :as-alias compose-modmail]
   [ovarit.app.fe.ui.forms.compose-message.contact-mods
    :as-alias contact-mods]
   [ovarit.app.fe.ui.forms.compose-message.modmail-reply
    :as-alias modmail-reply]))

(spec/def ::sub (spec/or :nothing-selected #{:nothing-selected}
                         :sub string?))
(spec/def ::send-to-user? boolean?)
(spec/def ::show-username? boolean?)
(spec/def ::username string?)
(spec/def ::subject string?)
(spec/def ::mid string?)
(spec/def ::thread-id string?)
(spec/def ::content string?)
(spec/def ::preview string?)
(spec/def ::preview-open? boolean?)

;; Compose message forms

(defn truncate-string
  [s num]
  (subs s 0 (min (count s) num)))

(defn reset-compose-modmail
  [{:keys [user subject sub report-type report-id]} state]
  (let [subject' (when subject (truncate-string subject 255))]
    (assoc state
           :sub            (or sub :nothing-selected)
           :send-to-user?  true
           :show-username? false
           :username       (or user "")
           :subject        (or subject' "")
           :report-type    report-type
           :report-id      report-id
           :content        ""
           :preview        ""
           :preview-open?  false)))

(defn initialize-compose-modmail-form
  [db]
  (let [options (get-in db [:view :options])]
    (->> {:id                  ::compose-form
          :form-type           ::compose-modmail
          :disable-on-success? false
          :reset-function      (partial reset-compose-modmail options)
          :validators
          [[#(= (:sub %) :nothing-selected)
            (trx db "Please select a circle.")]
           [#(and (:send-to-user? %) (or (< (count (:username %)) 1)
                                         (> (count (:username %)) 32)))
            (trx db "Username should be between 1 and 32 characters.")]
           [#(or (< (count (:subject %)) 1) (> (count (:subject %)) 255))
            (trx db "Subject should be between 1 and 255 characters.")]
           [#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Message should be between 1 and 16,384 characters.")]
           [#(util/markdown-blank? (:content %))
            (trx db "Message must contain letters,
                     not just formatting characters.")]]
          :server-actions {:send [::modmail/send-modmail]}
          :actions {:see-preview [::forms/see-preview {:field :content}]
                    :hide-preview [::forms/hide-preview]}}
         (reset-compose-modmail options)
         (forms/initialize-generic-form db))))

(spec/def ::compose-modmail/report-type (spec/nilable #{"post" "comment"}))
(spec/def ::compose-modmail/report-id (spec/nilable string?))
(spec/def ::compose-modmail/fields (spec/keys :req-un [::sub
                                                       ::send-to-user?
                                                       ::show-username?
                                                       ::username
                                                       ::subject
                                                       ::report-type
                                                       ::report-id
                                                       ::content
                                                       ::preview
                                                       ::preview-open?]))

(defmethod forms/form-type ::compose-modmail [_]
  (spec/merge ::compose-modmail/fields ::forms/fsm-form))

(defn- reset-modmail-reply-form
  [{:keys [sender-name receiver-name sub-name mtype
           thread-id]} state]
  (let [username (if (#{:USER_TO_MODS
                        :MOD_NOTIFICATION} mtype)
                   sender-name
                   (or receiver-name ""))]
    (assoc state
           :sub            sub-name
           :thread-id      thread-id
           :send-to-user?  false
           :show-username? false
           :username       username
           :subject        ""
           :content        ""
           :preview        ""
           :preview-open?  false)))

(defn- query-thread-with-users
  "Get the fields needed by `reset-modmail-reply-form`."
  [thread-id]
  [[:from :MessageThread]
   [:where [= :thread-id thread-id]]
   [:join :SubName {:sid :sid}]
   [:rename {:name :sub-name}]
   [:join :Message {:first-mid :mid}]
   [:left-join :User {:sender :uid}]
   [:rename {:name :sender-name}]
   [:left-join :User {:receiver :uid}]
   [:rename {:name :receiver-name}]])

(defn initialize-modmail-reply-form
  [db]
  (let [{:keys [thread-id mailbox]} (get-in db [:view :options])
        message (rel/q (:reldb db)
                       (query-thread-with-users thread-id))]
    (->> {:id                  ::compose-form
          :form-type           ::modmail-reply
          :disable-on-success? false
          :reset-function      (partial reset-modmail-reply-form message)
          :server-actions      {:send [::modmail/send-modmail-reply]}
          :actions             {:see-preview [::forms/see-preview
                                              {:field :content}]
                                :hide-preview [::forms/hide-preview]}
          :validators
          [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Message should be between 1 and 16,384 characters.")]
           [#(util/markdown-blank? (:content %))
            (trx db "Message must contain letters,
                     not just formatting characters.")]
           [#(and (= :notifications mailbox) (:send-to-user? %)
                  (or (< (count (:subject %)) 1) (> (count (:subject %)) 255)))
            (trx db "Subject should be between 1 and 255 characters.")]]}
         (reset-modmail-reply-form message)
         (forms/initialize-generic-form db))))

(spec/def ::modmail-reply/fields (spec/keys :req-un [::sub
                                                     ::thread-id
                                                     ::send-to-user?
                                                     ::show-username?
                                                     ::username
                                                     ::content
                                                     ::preview
                                                     ::preview-open?]))
(defmethod forms/form-type ::modmail-reply [_]
  (spec/merge ::modmail-reply/fields ::forms/fsm-form))

(defn- reset-contact-mods-form
  [sub state]
  (assoc state
         :sub           sub
         :subject       ""
         :content       ""
         :preview       ""
         :preview-open? false))

(defn initialize-contact-mods-form
  [db sub]
  (->> {:id                  ::compose-form
        :form-type           ::contact-mods
        :disable-on-success? true
        :reset-function      (partial reset-contact-mods-form sub)
        :validators
        [[#(or (< (count (:subject %)) 1) (> (count (:subject %)) 255))
          (trx db "Subject should be between 1 and 255 characters.")]

         [#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
          (trx db "Message should be between 1 and 16,384 characters.")]
         [#(util/markdown-blank? (:content %))
          (trx db "Message must contain letters,
                   not just formatting characters.")]]
        :server-actions      {:send [::message/send-contact-mods]}
        :actions             {:see-preview [::forms/see-preview
                                            {:field :content}]
                              :hide-preview [::forms/hide-preview]}}

       (reset-contact-mods-form sub)
       (forms/initialize-generic-form db)))

(spec/def ::contact-mods/fields (spec/keys :req-un [::sub
                                                    ::subject
                                                    ::content
                                                    ::preview
                                                    ::preview-open?]))
(defmethod forms/form-type ::contact-mods [_]
  (spec/merge ::contact-mods/fields ::forms/fsm-form))

;;; Extractors.

(re-frame/reg-sub ::ready-to-compose-message-to-mod
  (fn [db _]
    (= :loaded (get-in db [:status :subs]))))

