;; fe/ui/forms/moderation.cljs -- Moderation forms for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.moderation
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.comment.moderation :as-alias comment-mod]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.moderation :as-alias moderation]
   [ovarit.app.fe.content.post.moderation :as-alias post-mod]
   [ovarit.app.fe.content.subs :as-alias subs]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame :refer [subscribe]]
   [statecharts.core :as fsm])
  (:require
   [ovarit.app.fe.ui.forms.moderation.action
    :as-alias action]
   [ovarit.app.fe.ui.forms.moderation.create-post-flair
    :as-alias create-post-flair]
   [ovarit.app.fe.ui.forms.moderation.create-sub-rule
    :as-alias create-sub-rule]
   [ovarit.app.fe.ui.forms.moderation.delete-comment
    :as-alias delete-comment]
   [ovarit.app.fe.ui.forms.moderation.edit-post-flair
    :as-alias edit-post-flair]
   [ovarit.app.fe.ui.forms.moderation.edit-post-type
    :as-alias edit-post-type]))

;; Edit post type settings forms

(defn- get-current-namespace []
  (-> #'get-current-namespace meta :ns))

(defn- edit-post-type-form-id
  [sub-name post-type]
  (when (and sub-name post-type)
    (keyword (get-current-namespace)
             (str "edit-post-type." sub-name "."
                  (-> post-type name str/lower-case)))))

(re-frame/reg-sub ::edit-post-type-form-id
  :<- [:view/options]
  (fn [{:keys [sub]} [_ post-type]]
    (edit-post-type-form-id sub post-type)))

(defn- initialize-edit-post-type
  "Initialize one of the edit post type config forms."
  [db name post-type]
  (let [id (edit-post-type-form-id name post-type)
        conf (rel/row (:reldb db)
                      [[:from :SubPostTypeConfig]
                       [:where [:and
                                [= name :name]
                                [= [:_ post-type] :post-type]]]])
        {:keys [rules mods-only]} conf]
    (->> {:id id
          :form-type ::edit-post-type
          :name name
          :post-type post-type
          :mods-only? mods-only
          :rules rules
          :preview ""
          :preview-open? false
          :disable-on-success? false
          :reset-function nil
          :validators
          [[#(and (some? rules) (> (count (:rules %)) 16384))
            (trx db "Rules should be less than 16,384 characters long.")]]
          :actions {:see-preview [::forms/see-preview {:field :rules}]
                    :hide-preview [::forms/hide-preview]}
          :server-actions {:save [::moderation/update-post-type-config]}}
         (forms/initialize-generic-form db))))

(re-frame/reg-event-fx ::initialize-edit-post-types
  ;; :doc Create state in the db for the edit post type forms.
  (fn-traced [{:keys [db]} event]
    (let [sub-name (get-in db [:view :options :sub])]
      {:db (reduce #(initialize-edit-post-type %1 sub-name %2) db
                   [:LINK :POLL :TEXT :UPLOAD])
       :fx [[:dispatch [::loader/update-on-completion event :success]]]})))

;; Specs for the edit post types forms.
(spec/def ::edit-post-type/name :type.Sub/name)
(spec/def ::edit-post-type/post-type #{:LINK :TEXT :POLL :UPLOAD})
(spec/def ::edit-post-type/mods-only? boolean?)
(spec/def ::edit-post-type/preview string?)
(spec/def ::edit-post-type/preview-open? boolean?)
(spec/def ::edit-post-type/rules (spec/nilable string?))
(spec/def ::edit-post-type/fields
  (spec/keys :req-un [::edit-post-type/name
                      ::edit-post-type/post-type
                      ::edit-post-type/mods-only?
                      ::edit-post-type/preview
                      ::edit-post-type/preview-open?
                      ::edit-post-type/rules]))
(defmethod forms/form-type ::edit-post-type [_]
  (spec/merge ::edit-post-type/fields
              ::forms/fsm-form))

;; Form for creating a post flair.

(defn- reset-create-post-flair
  [state]
  (assoc state :flair ""))

(defn initialize-create-post-flair-form
  "Initial values and state for the moderator create post flair form."
  [db]
  (let [form-id ::create-post-flair]
    (->> {:id                  form-id
          :form-type           form-id
          :disable-on-success? false
          :reset-function      reset-create-post-flair
          :validators
          [[#(empty? (str/trim (:flair %)))
            (trx db "Please fill out this field.")]
           [#(> (count (str/trim (:flair %))) 25)
            (trx db "Flair is too long.")]]
          :server-actions {:create [::moderation/create-sub-post-flair]}}
         reset-create-post-flair
         (forms/initialize-generic-form db))))

;; Specs for the create post flair form.
(spec/def ::create-post-flair/flair string?)
(spec/def ::create-post-flair/fields
  (spec/keys :req-un [::create-post-flair/flair]))
(spec/def ::fields (spec/keys :req-un [::create-post-flair/flair]))
(defmethod forms/form-type ::create-post-flair [_]
  (spec/merge ::create-post-flair/fields
              ::forms/fsm-form))

;; Form for editing options for a post flair

(def edit-post-flair-state-machine
  "Create a statechart for the submit post form.
  Adds a region for the delete button."
  (-> forms/generic-form-machine
      (assoc :id :edit-post-flair
             :type :parallel)
      (dissoc :states :initial)
      (assoc :regions
             {:main (select-keys forms/generic-form-machine [:initial :states])
              :delete
              {:initial :READY
               :states {:SENDING
                        {:entry [(util/queue-fx forms/action-fx)]
                         :on {:response [{:target :SHOW-FAILURE
                                          :guard forms/send-errors-present?}
                                         {:target :READY}]}}
                        :READY
                        {:on {:send-delete [{:target :SENDING}]}}
                        :SHOW-FAILURE
                        {:on {:edit [{:target :READY}]
                              :send [{:target :READY}]
                              :send-delete [{:target :READY}]}
                         :exit [(fsm/assign forms/clear-errors)]}}}})))

(assert (= edit-post-flair-state-machine
           (fsm/machine edit-post-flair-state-machine)))

(defn initialize-edit-post-flair-fsm
  [db]
  (assoc-in db [:fsm ::edit-post-flair] edit-post-flair-state-machine))

(defn edit-post-flair-form-id
  [flair-id]
  (when flair-id
    (keyword (get-current-namespace)
             (str "edit-post-flair." flair-id))))

(re-frame/reg-sub ::edit-post-flair-form-id
  (fn [_ [_ id]]
    (edit-post-flair-form-id id)))

(def edit-post-flair-default-values
  "Default values for creation of an edit post flair form."
  {:form-type ::edit-post-flair
   :fsm-id ::edit-post-flair
   :errors []
   :send-errors []
   :disable-on-success? false
   :reset-function nil
   :validators []
   :server-actions {:save [::moderation/update-sub-post-flair]
                    :delete [::moderation/delete-sub-post-flair
                             {:event-type :send-delete}]}})

(defn initialize-edit-post-flair
  "Initialize one of the edit post flair options forms."
  [db flair-id]
  (let [id (edit-post-flair-form-id flair-id)
        flair (rel/row (:reldb db)
                       [[:from :SubPostFlair]
                        [:where [= flair-id :id]]])
        {:keys [mods-only post-types]} flair
        form (assoc edit-post-flair-default-values
                    :id id
                    :flair-id (:id flair)
                    :mods-only mods-only
                    :post-types post-types)
        fsm (-> db :fsm ::edit-post-flair)
        initialized-form (fsm/initialize fsm {:context form})]
    (update db :reldb rel/transact
            [:insert-or-replace :FormState initialized-form])))

(re-frame/reg-sub ::post-type-permitted?
  ;; :doc Determine whether a post type is permitted for a flair.
  (fn [[_ id _post-type]]
    (subscribe [::forms/field (edit-post-flair-form-id id) :post-types]))
  (fn [post-types [_ _id post-type]]
    (when post-types
      (some? (post-types post-type)))))

(re-frame/reg-event-db ::permit-flair-post-type
  ;; :doc Add or remove a post type from the post types set.
  (fn-traced [db [_ flair-id post-type val]]
    (let [id (edit-post-flair-form-id flair-id)]
      (-> db
          (update :reldb rel/transact
                  [:update :FormState #(if val
                                         (update % :post-types conj post-type)
                                         (update % :post-types disj post-type))
                   [= [:_ id] :id]])
          (forms/transition id {:type :edit})))))

;; Specs for the edit post flair forms.
(spec/def ::edit-post-flair/main ::forms/_state)
(spec/def ::edit-post-flair/delete #{:READY :SENDING :SHOW-FAILURE})
(spec/def ::edit-post-flair/_state
  (spec/keys :req-un [::edit-post-flair/main
                      ::edit-post-flair/delete]))
(spec/def ::edit-post-flair/flair-id :type.SubPostFlair/id)
(spec/def ::edit-post-flair/post-types
  (spec/and set? (spec/coll-of #{:LINK :TEXT :POLL :UPLOAD})))
(spec/def ::edit-post-flair/mods-only boolean?)
(spec/def ::edit-post-flair/fields
  (spec/keys :req-un [::edit-post-flair/_state
                      ::edit-post-flair/flair-id
                      ::edit-post-flair/mods-only
                      ::edit-post-flair/post-types]))
(defmethod forms/form-type ::edit-post-flair [_]
  (spec/merge ::edit-post-flair/fields
              ::forms/fsm-form-fields))

;; Form for creating a sub rule.

(defn- reset-create-sub-rule
  [state]
  (assoc state :rule ""))

(defn initialize-create-sub-rule-form
  "Initial values and state for the moderator create sub rule form."
  [db]
  (let [form-id ::create-sub-rule]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-create-sub-rule
          :validators
          [[#(empty? (str/trim (:rule %)))
            (trx db "Please fill out this field.")]
           [#(> (count (str/trim (:rule %))) 100)
            (trx db "Rule is too long.")]]
          :server-actions {:create [::moderation/create-sub-rule]}}
         reset-create-sub-rule
         (forms/initialize-generic-form db))))

;; Specs for the create sub rule form.
(spec/def ::create-sub-rule/rule string?)
(spec/def ::create-sub-rule/fields
  (spec/keys :req-un [::create-sub-rule/rule]))
(spec/def ::fields (spec/keys :req-un [::create-sub-rule/rule]))
(defmethod forms/form-type ::create-sub-rule [_]
  (spec/merge ::create-sub-rule/fields
              ::forms/fsm-form))

;; Mod action with reason form.

(defn- reset-action-form
  [pid cid state]
  (assoc state
         :reason ""
         :pid    pid
         :cid    cid))

(defn initialize-action-form
  "Initialize the form for a moderator to give a reason for an action."
  ([db action]
   (initialize-action-form db action nil))
  ([db action cid]
   (let [form-id ::action
         pid (get-in db [:view :options :pid])
         send-events {:delete-post [::post-mod/mod-delete-post]
                      :undelete-post [::post-mod/mod-undelete-post]
                      :undelete-comment [::comment-mod/mod-undelete-comment]}]
     (->> {:id                  form-id
           :form-type           form-id
           :server-actions      {:send (get send-events action)}
           :disable-on-success? false
           :reset-function      (partial reset-action-form pid cid)
           :validators
           [[#(or (< (count (:reason %)) 1) (> (count (:reason %)) 255))
             (trx db "Reason should be between 1 and 255 characters.")]]}
          (reset-action-form pid cid)
          (forms/initialize-generic-form db)))))

;; Specs for the mod action form.

(spec/def ::action/reason string?)
(spec/def ::action/pid string?)
(spec/def ::action/cid (spec/nilable string?))

(spec/def ::action/fields
  (spec/keys :req-un [::action/reason
                      ::action/pid
                      ::action/cid]))
(defmethod forms/form-type ::action [_]
  (spec/merge ::action/fields ::forms/fsm-form))


;; The delete comment form, which has a couple more fields.

(defn- reset-delete-comment-form
  [db pid cid state]
  (let [has-children? (-> (:reldb db)
                          (rel/row [[:from :CommentNode]
                                    [:where [= :cid cid]]])
                          :children seq some?)]
    (assoc state
           :reason ""
           :pid    pid
           :cid    cid
           :has-children? has-children?
           :delete-children? false
           :delete-children-reason
           (trx db "The comment that your comment is replying to was deleted."))))

(defn initialize-delete-comment-form
  "Initialize the form for a moderator to delete a comment."
  ([db cid]
   (let [form-id ::delete-comment
         pid (get-in db [:view :options :pid])]
     (->> {:id                  form-id
           :form-type           form-id
           :server-actions      {:send [::comment-mod/mod-delete-comment]}
           :disable-on-success? false
           :reset-function      (partial reset-delete-comment-form db pid cid)
           :validators
           [[#(or (< (count (str/trim (:reason %))) 1)
                  (> (count (str/trim (:reason %))) 255))
             (trx db "Reason should be between 1 and 255 characters.")]
            [#(and (:delete-children? %)
                   (or (< (count (str/trim (:delete-children-reason %))) 1)
                       (> (count (str/trim (:delete-children-reason %))) 255)))
             (trx db "Reason for deleting child comments should be between
                      1 and 255 characters.")]]}
          (reset-delete-comment-form db pid cid)
          (forms/initialize-generic-form db)))))

;; Specs for the mod delete-comment form.

(spec/def ::delete-comment/reason string?)
(spec/def ::delete-comment/pid string?)
(spec/def ::delete-comment/cid (spec/nilable string?))
(spec/def ::delete-comment/delete-children? boolean?)
(spec/def ::delete-comment/delete-children-reason string?)
(spec/def ::delete-comment/has-children? boolean?)

(spec/def ::delete-comment/fields
  (spec/keys :req-un [::delete-comment/reason
                      ::delete-comment/pid
                      ::delete-comment/cid
                      ::delete-comment/delete-children?
                      ::delete-comment/delete-children-reason
                      ::delete-comment/has-children?]))
(defmethod forms/form-type ::delete-comment [_]
  (spec/merge ::delete-comment/fields ::forms/fsm-form))
