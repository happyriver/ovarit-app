;; fe/ui/forms/user.cljs -- User forms for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.user
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as content]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user :as user]
   [re-frame.core :as re-frame])
  (:require
   [ovarit.app.fe.ui.forms.user.flair :as-alias flair]
   [ovarit.app.fe.ui.forms.user.preferences :as-alias preferences]))

(defn- reset-user-flair-form
  [state]
  state)

(defn initialize-user-flair-form
  "Initial values and state for the user flair form."
  [db form-id]
  (let [sub-name (get-in db [:view :options :sub])
        sub-info (content/sub-info-by-name db sub-name)
        {:keys [freeform-user-flairs user-flair-choices]} sub-info
        user-flair (content/current-user-sub-flair db)]
    (->> {:id                   form-id
          :form-type            ::flair
          :flair                (or user-flair "")
          :name                 (:name sub-info)
          :sid                  (:sid sub-info)
          :presets              user-flair-choices
          :freeform-user-flairs freeform-user-flairs
          :disable-on-success?  false
          :reset-function       reset-user-flair-form
          :validators
          [[#(empty? (str/trim (:flair %)))
            (trx db "Please fill out this field.")]
           [#(> (count (str/trim (:flair %))) 25)
            (trx db "Flair is too long.")]]
          :server-actions {:change [::user/change-user-flair]
                           :remove [::user/remove-user-flair]}}
         (forms/initialize-generic-form db))))

;; Specs for the user flair form.
(spec/def ::flair/flair string?)
(spec/def ::flair/name string?)
(spec/def ::flair/presets (spec/nilable (spec/* string?)))
(spec/def ::flair/freeform-user-flairs boolean?)
(spec/def ::flair/fields (spec/keys :req-un [::flair/flair
                                             ::flair/name
                                             ::flair/presets
                                             ::flair/freeform-user-flairs]))
(defmethod forms/form-type ::flair [_]
  (spec/merge ::flair/fields ::forms/fsm-form))

(re-frame/reg-event-db ::initialize-user-flair-form
  ;; :doc Initialize the user flair form.
  (fn-traced [db [_ _]]
    (initialize-user-flair-form db ::flair)))

(defn- nsfw-selection
  [{:keys [nsfw nsfw-blur]}]
  (cond
    (not nsfw) :hide
    nsfw-blur  :blur
    :else      :show))

(defn- make-display-names
  [lang]
  (js/Intl.DisplayNames. (clj->js [lang]) (clj->js {:type "language"})))

(defn- language-choices
  [db languages]
  (try
    (let [display-lang (or (-> db :current-user :language)
                           (-> db :settings :default-language)
                           "en")
          namer (make-display-names display-lang)]
      (concat [[:auto (trx db "Auto detect")]]
              (map (fn [lang]
                     [(keyword lang) (.of namer lang)])
                   languages)))
    (catch js/Error _
      ;; Do something reasonable if Intl.DisplayNames is not available,
      ;; since it's fairly new as of 2023.
      (concat [[:auto (trx db "Auto detect")]]
              (map (fn [lang]
                     [lang (if (= "en" lang) "English" (:name lang))])
                   languages)))))

(defn- initialize-preferences-form
  [db]
  (let [form-id ::preferences
        {:keys [attributes language]} (:current-user db)
        {:keys [languages]} (:settings db)]
    (->> {:id form-id
          :form-type form-id
          :nsfw (nsfw-selection attributes)
          :enable-experimental (:lab-rat attributes)
          :enable-collapse-bar (:enable-collapse-bar attributes)
          :block-dms (:block-dms attributes)
          :language (or (keyword language) :auto)
          :language-choices (language-choices db languages)
          :reset-function nil
          :disable-on-success? false
          :validators []
          :server-actions
          {:send [::user/send-preference-update]}}
         (forms/initialize-generic-form db))))

(re-frame/reg-event-fx ::initialize-preferences-form
  ;; :doc Initialize values and state for the user preferences form.
  (fn-traced [{:keys [db]} event]
    {:db (initialize-preferences-form db)
     :fx [[:dispatch [::loader/update-on-completion event :success]]]}))

;; Specs for the user preferences form.
(spec/def ::preferences/nsfw #{:show :hide :blur})
(spec/def ::preferences/enable-experimental boolean?)
(spec/def ::preferences/enable-collapse-bar boolean?)
(spec/def ::preferences/block-dms boolean?)
(spec/def ::preferences/language keyword?)
(spec/def ::preferences/language-choices
  (spec/coll-of (spec/tuple ::preferences/language string?)))
(spec/def ::preferences/fields
  (spec/keys :req-un [::preferences/nsfw
                      ::preferences/enable-experimental
                      ::preferences/enable-collapse-bar
                      ::preferences/block-dms
                      ::preferences/language
                      ::preferences/language-choices]))

(defmethod forms/form-type ::preferences [_]
  (spec/merge ::preferences/fields ::forms/fsm-form))
