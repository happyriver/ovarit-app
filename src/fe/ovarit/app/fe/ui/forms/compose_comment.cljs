;; fe/ui/forms/compose_comment.cljs -- Comment forms for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.compose-comment
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.comment :as-alias comment]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]))

;; Compose comment reply and comment edit forms

(defn form-id
  "Create a form identifier."
  [form-type cid]
  (keyword (namespace ::kw)
           (str "compose-" (name form-type) "-" (or cid "toplevel"))))

(def comment-common-values
  "Initial values common to the variations on the compose comment forms."
  {:preview        ""
   :preview-open?  false})

(defn- reset-compose-form
  [content state]
  (assoc state
         :content       (or content (:content state))
         :preview       ""
         :preview-open? false))

(defn initialize-comment-reply-form
  [db cid]
  (let [id (form-id :reply cid)
        existing-content (-> (rel/row (:reldb db)
                                      [[:from :FormState]
                                       [:where [= [:_ id] :id]]])
                             :content)
        content (or existing-content "")]
    (->> {:id             id
          :form-type      ::reply
          :parent-cid     cid
          :server-actions {:send [::comment/send-reply]}
          :actions        {:see-preview [::forms/see-preview
                                         {:field :content}]
                           :hide-preview [::forms/hide-preview]}
          :disable-on-success? false
          :reset-function (partial reset-compose-form "")
          :validators
          [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Reply should be between 1 and 16,384 characters.")]
           [#(and (> (count (:content %)) 0)
                  (util/markdown-blank? (:content %)))
            (trx db "Reply must contain letters,
                      not just formatting characters.")]]}
         (reset-compose-form content)
         (forms/initialize-generic-form db))))

(defn initialize-comment-edit-form
  [db cid]
  (let [id (form-id :edit cid)
        existing-content (-> (rel/row (:reldb db)
                                      [[:from :FormState]
                                       [:where [= [:_ id] :id]]])
                             :content)
        comment-content (-> (rel/row (:reldb db) :Comment [= :cid cid])
                            :content)
        content (or existing-content comment-content)]
    (->> {:id                  id
          :form-type           ::edit
          :cid                 cid
          :server-actions      {:send [::comment/send-comment-edit]}
          :actions             {:see-preview [::forms/see-preview
                                              {:field :content}]
                                :hide-preview [::forms/hide-preview]}
          :disable-on-success? false
          :reset-function      (partial reset-compose-form nil)
          :validators
          [[#(or (< (count (:content %)) 1) (> (count (:content %)) 16384))
            (trx db "Comment should be between 1 and 16,384 characters.")]
           [#(and (> (count (:content %)) 0)
                  (util/markdown-blank? (:content %)))
            (trx db "Comment must contain letters,
                     not just formatting characters.")]]}
         (reset-compose-form content)
         (forms/initialize-generic-form db))))

;; Specs for the comment reply and edit forms.

(spec/def ::content string?)
(spec/def ::preview string?)
(spec/def ::preview-open? boolean?)
(spec/def ::fields
  (spec/keys :req-un [::content ::preview ::preview-open?]))
(defmethod forms/form-type ::reply [_]
  (spec/merge ::fields ::forms/fsm-form))
(defmethod forms/form-type ::edit [_]
  (spec/merge ::fields ::forms/fsm-form))
