;; fe/ui/forms/post.cljs -- Post forms for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.post
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.graphql-spec]
   [ovarit.app.fe.content.internal :as content]
   [ovarit.app.fe.content.post :as-alias post]
   [ovarit.app.fe.content.post.db :as post-db]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.util :as util :refer [trimmed-count]])
  (:require
   [ovarit.app.fe.ui.forms.post.edit-content :as-alias edit-content]
   [ovarit.app.fe.ui.forms.post.edit-title :as-alias edit-title]
   [ovarit.app.fe.ui.forms.post.select-flair :as-alias select-flair]))

;; Select a flair for a post form

(defn initialize-select-flair-form
  "Initial values and state for the select a post flair form."
  [db]
  (let [form-id ::select-flair
        sub-name (get-in db [:view :options :sub])
        {:keys [pid flair type]} (post-db/single-post db)
        sub-info (content/sub-info-by-name db sub-name)
        {:keys [name user-must-flair]} sub-info
        is-mod-or-admin? (content/is-mod-or-admin? db sub-name)
        post-flairs (subs/permitted-flairs (:reldb db)
                                           {:sub-name sub-name
                                            :is-mod-or-admin? is-mod-or-admin?
                                            :post-type type})
        flair-id (when flair
                   (->> post-flairs
                        (filter #(= flair (:text %)))
                        first
                        :id))]
    (->> {:id                   form-id
          :form-type            form-id
          :flair                flair
          :selected-id          (or flair-id :nothing-selected)
          :name                 name
          :pid                  pid
          :presets              post-flairs
          :user-must-flair      user-must-flair
          :disable-on-success?  false
          :reset-function       nil
          :validators           []
          :server-actions       {:remove [::post/remove-post-flair]
                                 :change [::post/change-post-flair]}}
         (forms/initialize-generic-form db))))

;; Specs for the post flair form.
(spec/def ::select-flair/flair (spec/nilable string?))
(spec/def ::select-flair/selected-id
  (spec/or :not-found #(= :nothing-selected %)
           :found string?))
(spec/def ::select-flair/name string?)
(spec/def ::select-flair/pid string?)
(spec/def ::select-flair/presets
  (spec/nilable
   (spec/coll-of (spec/keys :req-un [:type.SubPostFlair/id
                                     :type.SubPostFlair/text]))))
(spec/def ::select-flair/user-must-flair boolean?)
(spec/def ::select-flair/fields (spec/keys :req-un [::select-flair/flair
                                                    ::select-flair/selected-id
                                                    ::select-flair/name
                                                    ::select-flair/pid
                                                    ::select-flair/presets
                                                    ::select-flair/user-must-flair]))
(defmethod forms/form-type ::select-flair [_]
  (spec/merge ::select-flair/fields
              ::forms/fsm-form))

;; Edit post title form

(defn- reset-edit-title-form
  [{:keys [pid title]} state]
  (assoc state
         :title  title
         :reason ""
         :pid    pid))

(defn initialize-edit-title-form
  "Initialize the edit title form."
  [db]
  (let [form-id ::edit-title
        {:keys [uid] :as post} (post-db/single-post db)
        is-author? (= uid (get-in db [:current-user :uid]))]
    (->> {:id form-id
          :form-type           form-id
          :disable-on-success? false
          :server-actions      {:send [::post/send-post-title-edit]}
          :reset-function      (partial reset-edit-title-form post)
          :validators
          [[#(or (< (trimmed-count (:title %)) 3)
                 (> (trimmed-count (:title %)) 255))
            (trx db
                 "Title should be between 3 and 255 characters long.")]
           [#(and (not is-author?)
                  (or (< (trimmed-count (:reason %)) 1)
                      (> (trimmed-count (:reason %)) 255)))
            (trx db
                 "Reason should be between 1 and 255 characters.")]]}
         (reset-edit-title-form post)
         (forms/initialize-generic-form db))))

;; Specs for the edit title form
(spec/def ::edit-title/title string?)
(spec/def ::edit-title/reason string?)
(spec/def ::edit-title/pid string?)
(spec/def ::edit-title/fields (spec/keys :req-un [::edit-title/pid
                                                  ::edit-title/reason
                                                  ::edit-title/title]))
(defmethod forms/form-type ::edit-title [_]
  (spec/merge ::edit-title/fields
              ::forms/fsm-form))


;; Edit post content form

(defn reset-edit-content-form
  [{:keys [content pid]} state]
  (assoc state
         :content (or (:content state) content)
         :pid pid
         :preview        ""
         :preview-open?  false))

(defn initialize-edit-content-form
  "Initialize the post content edit form.
  When reinitializing, use previously edited content if present."
  [db]
  (let [form-id ::edit-content
        post (post-db/single-post db)]
    (->> {:id                  form-id
          :form-type           form-id
          :disable-on-success? false
          :server-actions      {:send [::post/send-post-edit]}
          :actions             {:see-preview [::forms/see-preview
                                              {:field :content}]
                                :hide-preview [::forms/hide-preview]}
          :reset-function      (partial reset-edit-content-form post)
          :validators
          [[#(or (< (count (:content %)) 1) (> (count (:content %)) 65535))
            (trx db
                 "Post should be between 1 and 65,535 characters.")]
           [#(and (> (count (:content %)) 0)
                  (util/markdown-blank? (:content %)))
            (trx db "Post must contain letters,
                          not just formatting characters.")]]}
         (reset-edit-content-form post)
         (forms/initialize-generic-form db))))

;; Specs for the edit content form
(spec/def ::edit-content/content string?)
(spec/def ::edit-content/pid string?)
(spec/def ::edit-content/fields (spec/keys :req-un [::edit-content/pid
                                                    ::edit-content/content]))
(defmethod forms/form-type ::edit-content [_]
  (spec/merge ::edit-content/fields
              ::forms/fsm-form))
