;; fe/ui/forms/auth.cljs -- Authorization-related forms
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.auth
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.auth :as-alias auth]
   [ovarit.app.fe.content.internal :as content]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [re-frame.core :as re-frame])
  (:require
   [ovarit.app.fe.ui.forms.auth.change-email :as-alias change-email]
   [ovarit.app.fe.ui.forms.auth.change-password :as-alias change-password]
   [ovarit.app.fe.ui.forms.auth.delete-account :as-alias delete-account]
   [ovarit.app.fe.ui.forms.auth.login :as-alias login]
   [ovarit.app.fe.ui.forms.auth.registration :as-alias registration]
   [ovarit.app.fe.ui.forms.auth.rename :as-alias rename]
   [ovarit.app.fe.ui.forms.auth.reset-password :as-alias reset-password]
   [ovarit.app.fe.ui.forms.auth.start-reset :as-alias start-reset]
   [ovarit.app.fe.ui.forms.auth.username :as-alias username]))

(defn- password-too-simple?
  "Determine if a password meets the minimum standards."
  [{:keys [password-length password-complexity]} {:keys [password]}]
  (or (< (count password) password-length)
      (< (->> [(re-find #"[a-z]" password)
               (re-find #"[A-Z]" password)
               (re-find #"[0-9]" password)
               (re-find #"[^A-Za-z0-9]" password)]
              (remove nil?)
              count)
         password-complexity)))

(defn password-validators
  [db]
  (let [settings (get-in db [:settings :registration])
        {:keys [password-length]} settings]
    [[#(empty? (:password %))
      (trx db "Please enter your new password.")]
     [#(password-too-simple? settings %)
      (trx db "Please use a password that contains letters, numbers
               and symbols and is at least %s characters long."
           password-length)]
     [#(empty? (:confirm-password %))
      (trx db "Please type your password twice to confirm it.")]
     [#(not= (:password %) (:confirm-password %))
      (trx db "The passwords do not match.")]]))

(defn not-an-email?
  "Determine if a string could plausibly be an email address."
  [{:keys [email]}]
  (->> email
       str/trim
       (re-find #".+@.+\..+")
       nil?))

(comment
  (map #(not-an-email? {:email %})
       ["@"
        ""
        "@b.c"
        "a@b"
        "a@b."
        "a@.b"
        "newbie@example,com"
        "newbie@example.com"]))

;;; Change email form.

(defn initialize-change-email-form
  "Initial values and state for the edit email form."
  [db]
  (let [form-id ::change-email]
    (->> {:id                  form-id
          :form-type           form-id
          :current-email       (get-in db [:current-user :email])
          :email               (get-in db [:current-user :email])
          :username            (get-in db [:current-user :name])
          :current-password    ""
          :disable-on-success? true
          :reset-function      nil
          :validators
          [[#(empty? (:current-password %))
            (trx db "Please enter your current password.")]
           [not-an-email?
            (trx db "Please enter your new email address.")]
           [#(= (:email %) (:current-email %))
            (trx db "Your email address is the same as before.")]]
          :server-actions {:send [::auth/change-email]}}
         (forms/initialize-generic-form db))))

;; Specs for the edit email form
(spec/def ::change-email/current-email string?)
(spec/def ::change-email/email string?)
(spec/def ::change-email/username string?)
(spec/def ::change-email/current-password string?)
(spec/def ::change-email/fields
  (spec/keys :req-un [::change-email/current-email
                      ::change-email/email
                      ::change-email/username
                      ::change-email/current-password]))
(defmethod forms/form-type ::change-email [_]
  (spec/merge ::change-email/fields ::forms/fsm-form))

;; Change password form

(defn- reset-change-password
  [state]
  (assoc state
         :current-password ""
         :password         ""
         :confirm-password ""))

(defn initialize-change-password-form
  "Initial values and state for the edit password form."
  [db]
  (let [form-id ::change-password]
    (->>
     {:id               form-id
      :form-type        form-id
      :username         (get-in db [:current-user :name])
      :disable-on-success? true
      :reset-function   reset-change-password
      :validators
      (concat
       [[#(empty? (:current-password %))
         (trx db "Please enter your current password.")]]
       (password-validators db))
      :server-actions {:send [::auth/change-password]}}
     reset-change-password
     (forms/initialize-generic-form db))))

;; Specs for the edit password form
(spec/def ::change-password/username string?)
(spec/def ::change-password/current-password string?)
(spec/def ::change-password/password string?)
(spec/def ::change-password/confirm-password string?)
(spec/def ::change-password/fields
  (spec/keys :req-un [::change-password/current-password
                      ::change-password/username
                      ::change-password/password
                      ::change-password/confirm-password]))
(defmethod forms/form-type ::change-password [_]
  (spec/merge ::change-password/fields ::forms/fsm-form))

;; Delete account form

(defn initialize-delete-account-form
  "Initial values and state for the delete account form."
  [db]
  (let [form-id ::delete-account]
    (->>
     {:id                  form-id
      :form-type           form-id
      :username            (get-in db [:current-user :name])
      :current-password    ""
      :confirmation        ""
      :reset-function      nil
      :disable-on-success? true
      :validators
      [[#(empty? (:current-password %))
        (trx db "Please enter your current password.")]
       [#(not= "DELETE" (:confirmation %))
        (trx db "Please type DELETE to confirm that you want
                        to delete your account.")]]
      :server-actions      {:send [::auth/delete-account]}}
     (forms/initialize-generic-form db))))

;; Specs for the delete account form
(spec/def ::delete-account/username string?)
(spec/def ::delete-account/current-password string?)
(spec/def ::delete-account/confirmation string?)
(spec/def ::delete-account/fields (spec/keys :req-un [::delete-account/current-password
                                                      ::delete-account/username
                                                      ::delete-account/confirmation]))
(defmethod forms/form-type ::delete-account [_]
  (spec/merge ::delete-account/fields ::forms/fsm-form))

;; Registration form for new users

(defn- reset-registration-form [state]
  (assoc state
         :invite-code       ""
         :email             ""
         :username          ""
         :password          ""
         :confirm-password  ""
         :accept-tos?       false))

(defn initialize-registration-form
  "Initial values and state for the registration form."
  [db]
  (let [settings (get-in db [:settings :registration])
        {:keys [username-max-length invite-code-required]} settings
        form-id ::registration]
    (->>
     {:id                  form-id
      :form-type           form-id
      :disable-on-success? false
      :reset-function      reset-registration-form
      :validators
      (concat
       [[#(and invite-code-required
               (empty? (str/trim (:invite-code %))))
         (trx db "Please enter an invite code.")]
        [not-an-email?
         (trx db "Please enter your email address.")]
        [#(let [length (-> % :username str/trim count)]
            (or (< length 3)
                (> length username-max-length)))
         (trx db "Username must be between 3 and %s characters long."
              username-max-length)]
        [#(content/not-a-username? (:username %))
         (trx db "Username may only contain letters, numbers,
                          \"-\" and \"_\".")]
        [#(not (:accept-tos? %))
         (trx db "Please accept the terms of service.")]]
       (password-validators db))
      :server-actions {:send [::auth/register]}}
     reset-registration-form
     (forms/initialize-generic-form db))))

;; Specs for the registration form
(spec/def ::registration/invite-code string?)
(spec/def ::registration/email string?)
(spec/def ::registration/username string?)
(spec/def ::registration/password string?)
(spec/def ::registration/confirm-password string?)
(spec/def ::registration/accept-tos? boolean?)
(spec/def ::registration/fields
  (spec/keys :req-un [::registration/invite-code
                      ::registration/email
                      ::registration/username
                      ::registration/password
                      ::registration/confirm-password
                      ::registration/accept-tos?]))
(defmethod forms/form-type ::registration [_]
  (spec/merge ::registration/fields ::forms/fsm-form))

;; Start reset password form

(defn initialize-start-reset-form
  "Initial values and state for the start reset password form."
  [db]
  (let [form-id ::start-reset]
    (->>
     {:id form-id
      :form-type form-id
      :username            ""
      :email               ""
      :disable-on-success? true
      :reset-function      nil
      :validators
      [[#(empty? (str/trim (:username %)))
        (trx db "Please enter your username.")]
       [#(empty? (str/trim (:email %)))
        (trx db "Please enter the email address for this account.")]]
      :server-actions {:reset [::auth/reset]}}
     (forms/initialize-generic-form db))))

;; Specs for the start reset form
(spec/def ::start-reset/username string?)
(spec/def ::start-reset/email string?)
(spec/def ::start-reset/fields (spec/keys :req-un [::start-reset/username
                                                   ::start-reset/email]))
(defmethod forms/form-type ::start-reset [_]
  (spec/merge ::start-reset/fields ::forms/fsm-form))

;; Complete reset password form

(defn initialize-reset-form
  "Initial values and state for the reset form."
  [db]
  (let [form-id ::reset-password]
    (->>
     {:id form-id
      :form-type form-id
      :password            ""
      :confirm-password    ""
      :username            (get-in db [:view :options :username])
      :disable-on-success? true
      :reset-function      nil
      :validators          (password-validators db)
      :server-actions      {:send [::auth/complete-reset]}}
     (forms/initialize-generic-form db))))

;; Specs for the reset form
(spec/def ::reset-password/confirm-password string?)
(spec/def ::reset-password/password string?)
(spec/def ::reset-password/username string?)
(spec/def ::reset-password/fields
  (spec/keys :req-un [::reset-password/confirm-password
                      ::reset-password/password
                      ::reset-password/username]))
(defmethod forms/form-type ::reset-password [_]
  (spec/merge ::reset-password/fields ::forms/fsm-form))

;; The rename account form

(defn- reset-rename-form
  [{:keys [username] :as state}]
  (assoc state :old-username username))

(defn initialize-rename-form
  "Initial values and state for the rename forms."
  [db]
  (let [form-id ::rename
        username-max-length (get-in db [:settings :username-max-length])]
    (->>
     {:id form-id
      :form-type form-id
      :old-username (get-in db [:current-user :name])
      :username ""
      :password ""
      :disable-on-success? true
      :reset-function reset-rename-form
      :validators
      [[#(empty? (str/trim (:username %)))
        (trx db "Please enter your new username.")]
       [#(let [length (-> % :username str/trim count)]
           (or (< length 3)
               (> length username-max-length)))
        (trx db "Username must be between 3 and %s characters long."
             username-max-length)]
       [#(content/not-a-username? (:username %))
        (trx db "Username may only contain letters, numbers,
                          \"-\" and \"_\".")]
       [#(empty? (:password %))
        (trx db "Please enter your password.")]]
      :server-actions {:change [::auth/change-username]}}
     (forms/initialize-generic-form db))))

(re-frame/reg-event-fx ::initialize-rename-form
  (fn-traced [{:keys [db]} event]
    {:db (initialize-rename-form db)
     :fx [[:dispatch [::loader/update-on-completion event :success]]]}))

;; Specs for the rename form
(spec/def ::rename/username string?)
(spec/def ::rename/password string?)
(spec/def ::rename/fields (spec/keys :req-un [::rename/username
                                              ::rename/password]))
(defmethod forms/form-type ::rename [_]
  (spec/merge ::rename/fields ::forms/fsm-form))

;; Forms with only a username field

(defn- reset-username-form
  [state]
  (assoc state :username ""))

(defn initialize-username-form
  "Initial values and state for the username forms."
  [db]
  (let [form-id ::username]
    (->>
     {:id form-id
      :form-type form-id
      :disable-on-success? false
      :reset-function reset-username-form
      :validators
      [[#(empty? (str/trim (:username %)))
        (trx db "Please enter your username.")]]
      :server-actions {:reset [::auth/reset]
                       :resend [::auth/resend]}}
     reset-username-form
     (forms/initialize-generic-form db))))

;; Specs for the username form
(spec/def ::username/username string?)
(spec/def ::username/fields (spec/keys :req-un [::username/username]))
(defmethod forms/form-type ::username [_]
  (spec/merge ::username/fields ::forms/fsm-form))
