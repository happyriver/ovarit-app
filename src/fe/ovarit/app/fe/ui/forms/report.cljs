;; fe/ui/forms/report.cljs -- Report post or comment form for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.report
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.comment :as-alias comment]
   [ovarit.app.fe.content.post :as-alias post]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]))

;; Report post reason form.

(defn- reset-report-form
  [pid cid state]
  (assoc state
         :reason      :nothing-selected
         :rule        :nothing-selected
         :explanation ""
         :pid         pid
         :cid         cid))

(defn initialize-report-form
  "Initialize the report form."
  [db cid]
  (let [form-id ::report
        pid (get-in db [:view :options :pid])]
    (->> {:id                  form-id
          :form-type           form-id
          :disable-on-success? false
          :server-actions      {:send (if cid
                                        [::comment/report-comment]
                                        [::post/report-post])}
          :reset-function      (partial reset-report-form pid cid)
          :validators
          [[#(= :nothing-selected (:reason %))
            (trx db "Please select a reason.")]
           [#(and (= :sub-rule (:reason %))
                  (= :nothing-selected (:rule %)))
            (trx db "Please select a rule.")]
           [#(and (or (= :other (:reason %))
                      (and (= :sub-rule (:reason %)) (= :other (:rule %))))
                  (or (< (count (:explanation %)) 2)
                      (> (count (:explanation %)) 128)))
            (trx db
                 "Your explanation should be between 2 and 128 characters.")]]}
         (reset-report-form pid cid)
         (forms/initialize-generic-form db))))

;; Specs for the report form.

(spec/def ::reason #{:nothing-selected :site-rule :sub-rule :other})
(spec/def ::rule (spec/or :nothing-selected #(= :nothing-selected %)
                          :other #(= :other %)
                          :rule-id string?))
(spec/def ::explanation string?)
(spec/def ::pid string?)
(spec/def ::cid (spec/nilable string?))

(spec/def ::fields
  (spec/keys :req-un [::reason ::rule ::explanation ::pid ::cid]))
(defmethod forms/form-type ::report [_]
  (spec/merge ::fields ::forms/fsm-form))
