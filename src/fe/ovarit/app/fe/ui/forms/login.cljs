;; fe/ui/forms/login.cljs -- Login form for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.login
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.content.internal :as content]
   [ovarit.app.fe.content.login :as login-content]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.util :as util]
   [re-frame.core :as re-frame])
  (:require
   [ovarit.app.fe.ui.forms.auth.login :as-alias login]))

;; Login form

(defn reset-login-form
  [state]
  (assoc state
         :username            ""
         :password            ""
         :new-username        ""
         :remember-me?        false
         :name-change-message nil))

(defn initialize-login-form
  "Initial values and state for the login form."
  [db next-url]
  (let [form-id ::login]
    (->>
     {:id                  form-id
      :form-type           form-id
      :next-url            (or next-url "/")
      :disable-on-success? false
      :reset-function      nil
      :edit-hooks {:username [::clear-name-change]}
      :validators
      [[#(empty? (str/trim (:username %)))
        (trx db "Please enter your username.")]
       [#(empty? (:password %))
        (trx db "Please enter your password.")]
       [#(and (seq (:name-change-message %))
              (empty? (:new-username %)))
        (trx db "Please choose a new username.")]
       [#(and (seq (:name-change-message %))
              (content/not-a-username? (:new-username %)))
        (trx db "Username may only contain letters, numbers,
                 \"-\" and \"_\".")]]
      :server-actions {:send [::login-content/login]
                       :change-name [::login-content/change-name]}}
     reset-login-form
     (forms/initialize-generic-form db))))

;; Specs for the login form
(spec/def ::login/username string?)
(spec/def ::login/password string?)
(spec/def ::login/remember-me? boolean?)
(spec/def ::login/name-change-message (spec/nilable string?))
(spec/def ::login/new-username string?)
(spec/def ::login/next-url string?)
(spec/def ::login/fields (spec/keys :req-un [::login/username
                                             ::login/password
                                             ::login/remember-me?
                                             ::login/name-change-message
                                             ::login/new-username
                                             ::login/next-url]))
(defmethod forms/form-type ::login [_]
  (spec/merge ::login/fields ::forms/fsm-form))

(re-frame/reg-sub ::name-change-message-html
  ;; :doc Render the name change message into HTML.
  :<- [::forms/field ::login :name-change-message]
  (fn [msg _]
    (when msg
      (util/markdown-to-html msg))))

(re-frame/reg-event-db ::clear-name-change
  ;; :doc Clear the name change message.
  ;; :doc Used when the user changes the entered username.
  [(forms/form-path ::login)]
  (fn-traced [form [_ _]]
    (assoc form :name-change-message nil)))
