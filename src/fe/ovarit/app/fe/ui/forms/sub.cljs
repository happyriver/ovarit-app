;; fe/ui/forms/sub.cljs -- Sub forms for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.sub
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [goog.format :as gformat]
   [ovarit.app.fe.content.internal :as internal]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.moderation :as-alias moderation]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.graphql :as graphql]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.tr :refer [trx] :as tr]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :refer [subscribe] :as re-frame]
   [statecharts.core :as fsm]
   [weavejester.dependency :as dep])
  (:require
   [ovarit.app.fe.ui.forms.sub.submit-post :as-alias submit-post]))

;; Submit post form

(defn- link-edit?
  [_state event]
  (= :link (:field event)))

(defn submit-post-state-machine
  "Create a statechart for the submit post form.
  Adds a region for the grab title button."
  []
  (-> forms/generic-form-machine
      (assoc :id :submit-post
             :type :parallel)
      (dissoc :states :initial)
      (assoc :regions
             {:main (select-keys forms/generic-form-machine [:initial :states])
              :grab {:initial :READY
                     :states {:WAITING
                              {:on {:received-title [{:target :READY}]
                                    :edit [{:target :READY
                                            :guard link-edit?}]
                                    :no-title [{:target :SHOW-FAILURE}]}}

                              :READY
                              {:on {:send-grab [{:target :WAITING}]}}

                              :SHOW-FAILURE
                              {:on {:edit [{:target :READY}]}}}}})))

(let [mach (submit-post-state-machine)]
  (assert (= mach (fsm/machine mach))))

(defn- get-state [form]
  (let [{:keys [main grab]} (:_state form)]
    (if (= :SHOW-FAILURE grab)
      :SHOW-FAILURE
      main)))

(re-frame/reg-sub ::grab-state
  (fn [[_ form-id]]
    (subscribe [::forms/form form-id]))
  (fn [form _]
    (get-in form [:_state :grab])))

(defn- submit-post-form
  [db]
  (rel/row (:reldb db) [[:from :FormState]
                        [:where [= [:_ ::submit-post] :id]]]))

(defn- post-type-configs
  [db sub]
  (-> (:reldb db)
      (rel/q [[:from :SubPostTypeConfig] [:where [= sub :name]]])))

(defn- set-permitted-post-types
  [{:keys [sub] :as form} db {:keys [restricted]}]
  (let [configs (post-type-configs db sub)
        mod-only-types (->> (if restricted
                              configs
                              (-> (filter #(:mods-only %) configs)))
                            (map :post-type)
                            set)
        types (->> (cond
                     (internal/is-mod-or-admin? db sub) configs
                     restricted nil
                     :else (filter #(not (:mods-only %)) configs))
                   (map :post-type)
                   set)]
    (assoc form
           :permitted-post-types types
           :mod-only-types mod-only-types)))

(defn- set-selected-post-type
  [{:keys [selected-post-type permitted-post-types user-can-upload?] :as form}]
  (let [permitted? (if user-can-upload?
                     permitted-post-types
                     (disj permitted-post-types :UPLOAD))]
    (assoc form :selected-post-type
           (if (permitted? selected-post-type)
             selected-post-type
             ;; This is the same order as the radio buttons in the form.
             (some permitted? [:LINK :TEXT :UPLOAD :POLL])))))

(defn- reset-submit-post-form
  [db form]
  (let [sub (get-in db [:view :options :sub])
        sub-info (rel/row (:reldb db) [[:from :SubName]
                                       [:join :Sub {:sid :sid}]
                                       [:where [= sub :name]]])
        {:keys [nsfw user-attributes user-must-flair user-can-flair]} sub-info
        {sub-text-post-min-length :text-post-min-length} sub-info
        {:keys [text-post-min-length text-post-max-length]} (:settings db)]
    (-> form
        (assoc :sub sub  ; content of the text input
               :loaded-sub sub  ; current or previous sub with all data loaded
               :banned? (boolean (:banned user-attributes))
               :sub-nsfw? (boolean nsfw)
               :nsfw? (boolean nsfw)
               :user-must-flair (boolean user-must-flair)
               :user-can-flair (boolean user-can-flair)
               :is-mod-or-admin? (boolean (when sub
                                            (internal/is-mod-or-admin? db sub)))
               :selected-flair nil
               :text-post-min-length (max text-post-min-length
                                          sub-text-post-min-length)
               :text-post-max-length text-post-max-length)
        (dissoc :token)
        (set-permitted-post-types db sub-info)
        set-selected-post-type)))

(defn- submit-post-form-initial-values
  [db]
  (let [names (->> (rel/q (:reldb db) [[:from :SubName]])
                   (map :name)
                   (sort-by str/lower-case))]
    {:id ::submit-post
     :fsm-id :submit-post
     :form-type ::submit-post
     :title ""
     :link ""
     :content ""
     :poll-options ["" ""]
     :hide-results? false
     :close-poll? false
     :close-date nil
     :sub-choices names
     :user-can-upload? (user/can-upload? db)
     :preview ""
     :preview-open? false
     :file nil
     :uploaded-url nil
     :disable-on-success? true
     :reset-function nil
     :edit-hooks {:sub [::edit-sub]
                  :file [::clear-uploaded-url]}
     :get-state get-state
     :errors []
     :send-errors []
     :validators
     ;; Validators that apply to all posts.
     [[#(or (< (count (:title %)) 3) (> (count (:title %)) 255))
       (trx db "Title should be between 3 and 255 characters.")]
      [#(nil? ((->> % :sub-choices (map str/lower-case) set)
               (-> % :sub str/lower-case)))
       (trx db "Please choose an existing circle.")]
      [#(and (:user-must-flair %) (not (:is-mod-or-admin? %))
             (nil? (:selected-flair %)))
       (trx db "Please choose a flair.")]

      ;; Validators for link posts
      [#(and (= :LINK (:selected-post-type %))
             (or (< (count (:link %)) 10) (> (count (:link %)) 255)))
       (trx db "Link should be between 10 and 255 characters.")]
      [#(and (= :LINK (:selected-post-type %))
             (not (util/valid-url? (:link %))))
       (trx db "Please enter a valid URL.")]

      ;; Validators for text posts
      [#(and (#{:POLL :TEXT} (:selected-post-type %))
             (> (count (:content %)) (:text-post-max-length %)))
       (trx db "Post content must be shorter than %s characters."
            (-> db :settings :text-post-max-length))]
      ;; needs to get the current sub, so the string needs to be
      ;; a function of the db, so ...?
      #_[#(and (= :TEXT (:selected-post-type %))
               (< (count (:content %)) (:text-post-min-length %)))
         (trx db "Post content must be longer than %s characters."
              (-> db :settings :text-post-min-length))]
      [#(and (#{:POLL :TEXT} (:selected-post-type %))
             (> (count (:content %)) 0)
             (util/markdown-blank? (:content %)))
       (trx db "Content must contain letters, not just formatting characters.")]

      ;; Validators for poll posts
      [(fn [{:keys [selected-post-type poll-options]}]
         (and (= :POLL selected-post-type)
              (some #(> (count %) 127) poll-options)))
       (trx db "Poll options must be less than 128 characters.")]
      [(fn [{:keys [selected-post-type poll-options]}]
         (and (= :POLL selected-post-type)
              (> 1 (->> poll-options
                        (map count)
                        (filter pos?)
                        count))))
       (trx db "Polls must have at least two options.")]

      ;; Validators for upload posts
      [(fn [{:keys [selected-post-type file]}]
         (and (= :UPLOAD selected-post-type) (nil? file)))
       (trx db "Please select a file to upload.")]
      (let [max-size (get-in db [:settings :upload-max-size])]
        [(fn [{:keys [selected-post-type file]}]
           (and (= :UPLOAD selected-post-type)
                (some? file)
                (> (.-size file) max-size)))
         (trx db "File too large. Please choose a file smaller than %s."
              (gformat/numBytesToString max-size))])]

     :actions {:see-preview [::forms/see-preview {:field :content}]
               :hide-preview [::forms/hide-preview]}
     :server-actions {:submit [::subs/submit-post]}}))

(defn initialize-submit-post
  "Initial values and state for the submit post form."
  [db]
  (let [form (->> (or (submit-post-form db)
                      (submit-post-form-initial-values db))
                  (reset-submit-post-form db))
        fsm (submit-post-state-machine)
        initialized-form (fsm/initialize fsm {:context form})]
    (-> db
        (assoc-in [:fsm :submit-post] fsm)
        (update :reldb rel/transact
                [:insert-or-replace :FormState initialized-form]))))

(re-frame/reg-event-fx ::initialize-submit-post
  ;; :doc Create state in the db for the submit post form.
  (fn-traced [{:keys [db]} event]
    {:db (initialize-submit-post db)
     :fx [[:dispatch [::loader/update-on-completion event :success]]]}))

(re-frame/reg-sub ::rules
  ;; :doc Get the rules for the selected post type.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [loaded-sub selected-post-type]} reldb] _]
    (-> reldb
        (rel/row [[:from :SubPostTypeConfig]
                  [:where
                   [= loaded-sub :name]
                   [= [:_ selected-post-type] :post-type]]])
        :rules)))

(re-frame/reg-sub ::flairs
  ;; :doc Get the flairs for the selected post type.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [is-mod-or-admin? loaded-sub selected-post-type]} reldb] _]
    (subs/permitted-flairs reldb {:sub-name loaded-sub
                                  :is-mod-or-admin? is-mod-or-admin?
                                  :post-type selected-post-type})))

(def new-sub-deps
  "Initialization events for a new sub selection."
  (-> (dep/graph)
      ;; ::subs/check-sub will fix capitalization
      (dep/depend [::subs/load-info] [::subs/check-sub])
      (dep/depend [::subs/load-post-types] [::subs/check-sub])
      (dep/depend [::update-sub] [::subs/load-info])
      (dep/depend [::update-sub] [::subs/load-post-types])))

(re-frame/reg-event-fx ::edit-sub
  ;; :doc Handle an edit to the sub field in the submit post form.
  ;; :doc If the new sub is valid, fetch post types and info for it.
  (fn-traced [{:keys [db]} [_ old-sub new-sub]]
    (let [form (submit-post-form db)
          choices (->> form :sub-choices (map str/lower-case) set)
          valid? (choices (str/lower-case new-sub))
          changed? (not= old-sub new-sub)]
      (when (and valid? changed?)
        {:db (assoc-in db [:view :options :sub] new-sub)
         :fx [[:dispatch [::loader/init-content-loader
                          {:graph new-sub-deps}]]]}))))

(re-frame/reg-event-db ::clear-uploaded-url
  [(forms/form-path ::submit-post)]
  (fn-traced [form [_ _]]
    (assoc form :uploaded-url nil)))

(re-frame/reg-event-fx ::update-sub
  ;; :doc Update the submit post form for a new sub selection.
  ;; :doc Update the browser location bar with the new sub name.
  (fn-traced [{:keys [db]} event]
    (let [sub (get-in db [:view :options :sub])]
      {:db (update db :reldb rel/transact
                   [:update :FormState #(reset-submit-post-form db %)
                    [= [:_ ::submit-post] :id]])
       :fx [[:dispatch [::loader/update-on-completion event :success]]
            [::routes/replace-url
             (route-util/url-for db :subs/submit-post
                                 :query-args {:sub sub})]]})))

(re-frame/reg-sub ::loaded-sub
  ;; :doc Get the details for the last sub for which details were loaded.
  (fn [_]
    [(subscribe [::forms/form ::submit-post])
     (subscribe [::internal/reldb])])
  (fn [[{:keys [loaded-sub]} reldb] _]
    (rel/row reldb [[:from :SubName]
                    [:join :Sub {:sid :sid}]
                    [:where [= loaded-sub :name]]])))

(re-frame/reg-sub ::html-rules
  ;; :doc Produce formatted rules for the selected post type.
  :<- [::rules]
  (fn [rules _]
    (util/markdown-to-html rules)))

(re-frame/reg-event-fx ::grab-title
  (fn-traced [{:keys [db]} [_ _]]
    (let [{:keys [link _state]} (submit-post-form db)]
      (when (and (seq link) (= :READY (:grab _state)))
        {:db (forms/transition db ::submit-post {:type :send-grab})
         :fx [[:dispatch [::graphql/subscribe
                          {:graphql subs/graphql
                           :name :url-metadata
                           :id link
                           :variables {:url link}
                           :handler [::receive-grab-title-result
                                     {:url link}]}]]]}))))

(defn- assoc-grab-title-error
  [db]
  (update db :reldb rel/transact
          [:update :FormState #(update % :send-errors conj
                                       (trx db "Couldn't get title"))
           [= [:_ ::submit-post] :id]]))

(re-frame/reg-event-db ::receive-grab-title-result
  ;; :doc Receive the grabbed title from the server.
  (fn-traced [db [_ {:keys [url response]}]]
    (log/info {:response response :url url} "grab title result")
    (let [form (submit-post-form db)]
      (if-not (= url (:link form))
        ;; If the urls don't match, the user managed to edit the link
        ;; and possibly click grab title again while waiting, so
        ;; ignore this response.
        db
        (let [title (get-in response [:data :url-metadata :title])]
          (if title
            (-> db
                (update :reldb rel/transact
                        [:update :FormState {:title title}
                         [= [:_ ::submit-post] :id]])
                (forms/transition ::submit-post {:type :edit :field :title})
                (forms/transition ::submit-post {:type :received-title}))
            (-> db
                assoc-grab-title-error
                (forms/transition ::submit-post {:type :no-title}))))))))

(defn reset-input-on-grab-title!
  "Reset the value field in an atom when the grab title state changes.
  Use to make the title input input refetch its value from the db
  after a successful grab title."
  [state-atom {:keys [value grab-state] :as properties}]
  (if (= grab-state :READY)
    (when-not (:already-updated? @state-atom)
      (swap! state-atom assoc
             :value (value) :already-updated? true))
    (swap! state-atom assoc :already-updated? false))
  (dissoc properties :grab-state))

(re-frame/reg-event-fx ::edit-poll-option
  ;; :doc Set a value for one of the poll options.
  (fn-traced [{:keys [db]} [_ num val]]
    (let [options (:poll-options (submit-post-form db))]
      {:fx [[:dispatch [::forms/edit ::submit-post :poll-options
                        (assoc options num val)]]]})))

(re-frame/reg-sub ::enable-add-poll-option?
  ;; :doc Whether to enable the add poll option button.
  (fn [_]
    (subscribe [::forms/field ::submit-post :poll-options]))
  (fn [options _]
    (< (count options) 6)))

(re-frame/reg-event-fx ::add-poll-option
  ;; :doc Add another poll option.
  (fn-traced [{:keys [db]} _]
    (let [options (:poll-options (submit-post-form db))]
      {:fx [[:dispatch [::forms/edit ::submit-post :poll-options
                        (conj options "")]]]})))

(re-frame/reg-sub ::upload-filename
  (fn []
    [(subscribe [::tr/lang])
     (subscribe [::forms/field ::submit-post :file])])
  (fn [[lang file] _]
    (when file
      (or (.-name file) (trx lang "Clipboard Image")))))

;; Configuration fields
(spec/def ::submit-post/banned? boolean?)
(spec/def ::submit-post/is-mod-or-admin? boolean?)
(spec/def ::submit-post/loaded-sub (spec/nilable string?))
(spec/def ::submit-post/mod-only-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(spec/def ::submit-post/permitted-post-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(spec/def ::submit-post/sub-choices (spec/coll-of string?))
(spec/def ::submit-post/sub-nsfw? boolean?)
(spec/def ::submit-post/user-can-flair boolean?)
(spec/def ::submit-post/user-must-flair boolean?)
(spec/def ::submit-post/text-post-min-length nat-int?)
(spec/def ::submit-post/text-post-max-length nat-int?)

;; All types of posts
(spec/def ::submit-post/nsfw? boolean?)
(spec/def ::submit-post/selected-flair (spec/nilable string?))
(spec/def ::submit-post/selected-post-type (spec/nilable :reldb.Post/type))
(spec/def ::submit-post/sub (spec/nilable string?))
(spec/def ::submit-post/title string?)

;; Poll-specific fields
(spec/def ::submit-post/close-date
  (spec/nilable #(= (type %) js/Date)))
(spec/def ::submit-post/close-poll? boolean?)
(spec/def ::submit-post/hide-results? boolean?)

;; Text or poll fields
(spec/def ::submit-post/content string?)
(spec/def ::submit-post/preview string?)
(spec/def ::submit-post/preview-open? boolean?)

;; Link post fields
(spec/def ::submit-post/link string?)

;; Upload post fields
(spec/def ::submit-post/file (spec/nilable #(instance? js/Blob %)))
(spec/def ::submit-post/uploaded-url (spec/nilable string?))

(spec/def ::submit-post/main ::forms/_state)
(spec/def ::submit-post/grab #{:READY :SENDING :WAITING :SHOW-FAILURE})
(spec/def ::submit-post/_state
  (spec/keys :req-un [::submit-post/main
                      ::submit-post/grab]))
(spec/def ::submit-post/fields
  (spec/keys :req-un [::submit-post/banned?
                      ::submit-post/close-date
                      ::submit-post/close-poll?
                      ::submit-post/content
                      ::submit-post/file
                      ::submit-post/hide-results?
                      ::submit-post/is-mod-or-admin?
                      ::submit-post/link
                      ::submit-post/loaded-sub
                      ::submit-post/mod-only-types
                      ::submit-post/nsfw?
                      ::submit-post/permitted-post-types
                      ::submit-post/preview
                      ::submit-post/preview-open?
                      ::submit-post/selected-flair
                      ::submit-post/selected-post-type
                      ::submit-post/sub
                      ::submit-post/sub-choices
                      ::submit-post/sub-nsfw?
                      ::submit-post/title
                      ::submit-post/user-can-flair
                      ::submit-post/user-must-flair
                      ::submit-post/text-post-min-length
                      ::submit-post/text-post-max-length
                      ::submit-post/_state]))
(defmethod forms/form-type ::submit-post [_]
  (spec/merge ::submit-post/fields
              ::forms/fsm-form-fields))
