;; fe/ui/forms/core.cljs -- Form state machine for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.core
  (:require
   [clojure.spec.alpha :as spec]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [re-frame.core :as re-frame :refer [subscribe]]
   [re-frame.interceptor :refer [->interceptor
                                 get-coeffect assoc-coeffect
                                 get-effect assoc-effect]]
   [statecharts.core :as fsm]))

(defn- check-form-fields
  "Check the contents of the fields in the form.
  Return the list of errors, or an empty list if all the
  fields are valid."
  [{:keys [validators] :as form}]
  (let [msgs (map (fn [[test msg]]
                    (when (test form) msg))
                  validators)]
    (filter some? msgs)))

(defn- all-fields-validate?
  "Return true if all the validators pass."
  [form _event]
  (empty? (check-form-fields form)))

(defn- validation-errors
  "Return the list of messages associated with failing validators."
  [form _event]
  (assoc form :errors (check-form-fields form)))

(defn send-errors-present?
  "Return non-nil if any transmission errors are present."
  [form _event]
  (seq (:send-errors form)))

(defn clear-errors
  [form _event]
  (assoc form :send-errors [] :errors []))

(defn action-fx
  "Construct the re-frame fx for a form action."
  [form {:keys [event]}]
  (log/debug {:event event :form-id (:id form)}
             "action-fx")
  [:dispatch (update event 1 assoc :form form)])

(defn- reset
  "Run a form's reset function if it has one."
  [{:keys [reset-function] :as form} _]
  (if reset-function
    (reset-function form)
    form))

(def generic-form-machine
  "A statechart for forms."
  {:id :generic-form
   :initial :INCOMPLETE
   :context nil
   :states
   {:INCOMPLETE   {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}}
    :READY        {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}}
    :SHOW-ERROR   {:entry [(fsm/assign validation-errors)]
                   :on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]}}
    :SENDING      {:entry [(util/queue-fx action-fx)]
                   :on {:response [{:target :SHOW-FAILURE
                                    :guard send-errors-present?}
                                   {:target :SHOW-SUCCESS}]}}
    :SHOW-FAILURE {:on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]
                        :send    [{:target :SENDING
                                   :guard all-fields-validate?}
                                  {:target :SHOW-ERROR}]}
                   :exit [(fsm/assign clear-errors)]}
    :SHOW-SUCCESS {:entry [(fsm/assign reset)]
                   :on {:edit    [{:target :READY
                                   :guard all-fields-validate?}
                                  {:target :INCOMPLETE}]}}}})

(assert (= generic-form-machine
           (fsm/machine generic-form-machine)))

(defn initialize-generic-form
  [db context]
  (let [ctx (merge context {:fsm-id :generic-form
                            :errors []
                            :send-errors []})]
    (log/trace {:context context} "Initializing form")
    (update db :reldb rel/transact
            [:insert-or-replace :FormState
             (fsm/initialize generic-form-machine {:context ctx})])))

(defn transition
  [db id event]
  (try
    (update db :reldb rel/transact
            [:update :FormState
             (fn [{:keys [fsm-id] :as form}]
               (let [result (fsm/transition (get-in db [:fsm fsm-id])
                                            form event
                                            {:ignore-unknown-event? true})]
                 (log/debug {:form result :event event} "after transition")
                 result))
             [= [:_ id] :id]])
    (catch js/Error e
      (log/error {:exception e
                  :form-id id
                  :event event} "error in fsm transition")
      db)))

(re-frame/reg-event-fx ::edit
  ;; :doc Edit a form field.
  ;; :doc If the value is different, transition the state machine
  ;; :doc and dispatch the edit hook event if there is one.
  (fn-traced [{:keys [db]} [_ form-id field value]]
    (let [form (rel/row (:reldb db) [[:from :FormState]
                                     [:where [= [:_ form-id] :id]]])
          old-value (get form field)
          hook (get (:edit-hooks form) field)]
      (log/trace {:form-id form-id :field field
                  :value value :old-value old-value}
                 "Edit a form field")
      (when (not= value old-value)
        {:db (-> db
                 (update :reldb rel/transact
                         [:update :FormState {field [:_ value]}
                          [= [:_ form-id] :id]])
                 (transition form-id {:type :edit}))
         :fx [(when hook
                [:dispatch (into hook [(get form field) value])])]}))))

(defn reset-input-on-success!
  "Reset a text value in an atom when the form reaches the success state.
  Use to make text inputs refetch their values from the db after those
  values are cleared on a successful send."
  [state-atom {:keys [value form-state] :as properties}]
  (if (= form-state :SHOW-SUCCESS)
    (when-not (:already-updated? @state-atom)
      (swap! state-atom assoc
             :value (value) :already-updated? true))
    (swap! state-atom assoc :already-updated? false))
  (dissoc properties :form-state))

(defn reset-input-on-field-change!
  "Reset the value field in an atom when another field changes.
  Use the value function in `props` to get the new value.
  Include the other field in the component properties."
  [field state-atom {:keys [value] :as props}]
  (when-not (= (get props field) (get @state-atom field))
    (swap! state-atom assoc
           :value (value)
           field (get props field)))
  (dissoc props field :form-state))

;; Generic form events and subscriptions

(defn- <-form-id
  [[_ form-id _]]
  (subscribe [::form form-id]))

(re-frame/reg-sub ::form
  ;; :doc Get a form's state and context
  (fn [db [_ form-id]]
    (rel/row (:reldb db) [[:from :FormState]
                          [:where [= [:_ form-id] :id]]])))

(re-frame/reg-sub ::field
  ;; :doc Get the value of a form field.
  <-form-id
  (fn [form [_ _ field]]
    (field form)))

(re-frame/reg-sub ::errors
  ;; :doc Retrieve the list of errors in a form.
  <-form-id
  (fn [form _]
    (:errors form)))

(re-frame/reg-sub ::send-errors
  ;; :doc Retrieve the list of errors from the last send.
  <-form-id
  (fn [form _]
    (:send-errors form)))

(re-frame/reg-sub ::form-state
  ;; :doc Get the state of a form.
  <-form-id
  (fn [form _]
    (:_state form)))

(re-frame/reg-sub ::disable-on-success?
  ;; :doc Get whether the edit controls should be disabled on success.
  <-form-id
  (fn [form _]
    (:disable-on-success? form)))

(re-frame/reg-sub ::editing-disabled?
  ;; :doc Decide whether editing elements of the form should be disabled.
  (fn [[_ form-id]]
    [(subscribe [::form form-id])
     (subscribe [::form-state form-id])])
  (fn [[{:keys [disable-on-success?]} form-state] _]
    (or (= form-state :SENDING)
        (and disable-on-success? (= form-state :SHOW-SUCCESS)))))

(defn- action-transition
  [db form event]
  (let [{:keys [event-type]} (second event)]
    (if (= :simple event-type)
      (assoc form :fx [event])
      (let [{:keys [fsm-id]} form
            fsm (get-in db [:fsm fsm-id])]
        (fsm/transition fsm form {:type event-type :event event}
                        {:ignore-unknown-event? true})))))

(re-frame/reg-event-fx ::action
  ;; :doc Dispatch the event for an action keyword in the form.
  ;; :doc Events found in the :server-actions key of the form map
  ;; :doc will cause a form transition before dispatch, and then
  ;; :doc only be dispatched if the form transition causes them to
  ;; :doc be assoc'd into the :fx key of the form.
  (fn-traced [{:keys [db]} [_ form-id action & args]]
    (let [form (rel/row (:reldb db) [[:from :FormState]
                                     [:where [= [:_ form-id] :id]]])
          event (or (some-> (get-in form [:actions action])
                            (update 1 assoc
                                    :form form
                                    :event-type :simple
                                    :args args))
                    (-> (get-in form [:server-actions action])
                        (update-in [1 :event-type] #(or % :send))
                        (update 1 assoc :args args)))
          _ (when-not event
              (log/error {:form-id form-id :action action}
                         "Form action not found"))
          new-state (action-transition db form event)]
      (log/debug {:old-state form :new-state new-state :event event
                  :action action}
                 "Form action transition before dispatch")
      {:db (update db :reldb rel/transact
                   [:insert-or-replace :FormState (dissoc new-state :fx)])
       :fx (vec (:fx new-state))})))

(defn transition-after-response
  "Advance the form state.
  Use on completion of an action started by an event in :server-actions.
  Link error messages associated with `form-id` to the form."
  [db form-id]
  (let [error-descriptions (errors/get-errors db form-id)
        error-messages (map :msg error-descriptions)]
    (when (seq error-messages)
      (log/debug {:form-id form-id
                  :error-messages error-messages} "Form request errors"))
    (-> db
        (errors/clear-errors form-id)
        (update :reldb rel/transact
                [:update :FormState {:send-errors error-messages}
                 [= [:_ form-id] :id]])
        (transition form-id {:type :response}))))

(re-frame/reg-event-db ::generic-graphql-handler
  ;; :doc Handle a response to a graphql query or mutation sent from a form.
  ;; :doc Associate any errors with `form`, and call `restore` if it is
  ;; :doc set and an error occurs. Either way, transition the form.
  (fn-traced [db [_ {:keys [form restore response]}]]
    (let [errors (:errors response)]
      (-> db
          (errors/assoc-errors {:event (:id form) :errors errors})
          (cond-> (and errors restore) restore)
          (transition-after-response (:id form))))))

(defn form-path
  "Interceptor to substitute a form for the db.
  Like the re-frame interceptor `path`, except it extracts
  a form object based on the form id."
  [form-id]
  (->interceptor
   :id :form-path
   :before
   (fn [context]
     (let [form-id (or form-id
                       ;; For event-form-path below.
                       (-> (get-coeffect context :event)
                           (nth 1)
                           :form :id))
           original-db (get-coeffect context :db)
           form (rel/row (:reldb original-db) [[:from :FormState]
                                               [:where [= [:_ form-id]:id]]])]
       (-> context
           (update ::db-store-key conj original-db)
           (assoc-coeffect :db form))))

   :after
   (fn [context]
     (let [db-store (::db-store-key context)
           original-db (peek db-store)
           new-db-store (pop db-store)
           context' (-> context
                        (assoc ::db-store-key new-db-store)
                        (assoc-coeffect :db original-db))
           db (get-effect context :db ::not-found)]
       (if (= db ::not-found)
         context'
         (->> (update original-db :reldb rel/transact
                      [:insert-or-replace :FormState db])
              (assoc-effect context' :db)))))))

(def event-form-path
  "Interceptor to substitute a form for the db.
  Find the id of the form to use in the event vector, where
  the ::action handler puts it (assoc'd to the key :form in the second
  element of the event vector)."
  (form-path nil))

(re-frame/reg-event-db ::update-preview
  ;; :doc Show the html preview and update its content.
  [event-form-path]
  (fn-traced [form [_ {:keys [field]}]]
    (let [content (get form field)]
      (-> form
          (assoc :preview (util/markdown-to-html content))
          (assoc :preview-open? true)))))

(re-frame/reg-event-fx ::see-preview
  ;; :doc Show the html preview after a short delay.
  ;; :doc The delay allows the markdown editor's on-blur event to save the
  ;; :doc current text.
  (fn-traced [_ [_ info]]
    {:fx [[:dispatch-later
           {:ms 100
            :dispatch [::update-preview info]}]]}))

(re-frame/reg-event-db ::hide-preview
  ;; :doc Hide the html preview.
  [event-form-path]
  (fn-traced [form _]
    (assoc form :preview-open? false)))

;; Specs for the shared form fields.
(spec/def ::id keyword?)
(spec/def ::fsm-id keyword?)
(spec/def ::disable-on-success? boolean?)
(spec/def ::errors (spec/coll-of string?))
(spec/def ::send-errors
  (spec/coll-of
   (spec/or :message string?
            :html vector?)))
(spec/def ::reset-function (spec/nilable fn?))
(spec/def ::edit-hooks (spec/map-of keyword? vector?))
(spec/def ::actions (spec/map-of keyword? vector?))
(spec/def ::server-actions (spec/map-of keyword? vector?))
(spec/def ::validator (spec/cat :check-fn fn? :message string?))
(spec/def ::validators (spec/coll-of ::validator))
(spec/def ::_state
  #{:INCOMPLETE :READY :SHOW-ERROR :SENDING :SHOW-SUCCESS :SHOW-FAILURE})
(spec/def ::get-state fn?)

(spec/def ::fsm-form-fields (spec/keys :req-un [::disable-on-success?
                                                ::reset-function
                                                ::errors
                                                ::send-errors
                                                ::validators
                                                ::id
                                                ::fsm-id]
                                       :opt-un [::actions
                                                ::edit-hooks
                                                ::get-state
                                                ::server-actions]))

(spec/def ::fsm-form
  (spec/merge (spec/keys :req-un [::_state])
              ::fsm-form-fields))

(defmulti form-type :form-type)
(spec/def ::typed-form
  (spec/multi-spec form-type :form-type))
