;; fe/ui/forms/admin.cljs -- Admin forms for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.admin
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [ovarit.app.fe.content.admin :as admin]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms])
  (:require
   [ovarit.app.fe.ui.forms.admin.ban-username-string
    :as-alias ban-username-string]
   [ovarit.app.fe.ui.forms.admin.expire-invite-code
    :as-alias expire-invite-code]
   [ovarit.app.fe.ui.forms.admin.generate-invite-code
    :as-alias generate-invite-code]
   [ovarit.app.fe.ui.forms.admin.invite-code-settings
    :as-alias invite-code-settings]
   [ovarit.app.fe.ui.forms.admin.require-name-change
    :as-alias require-name-change]
   [ovarit.app.fe.ui.forms.admin.search-invite-code
    :as-alias search-invite-code]))

;;; Ban string in user names form

(defn- reset-ban-username-string
  [state]
  (assoc state :content ""))

(defn initialize-ban-username-string
  "Initial values and state for admin ban username string form."
  [db]
  (let [form-id ::ban-username-string]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-ban-username-string
          :validators
          [[#(or (< (count (:content %)) 2) (> (count (:content %)) 32))
            (trx db "String should be between 2 and 32 characters.")]
           [#(nil? (re-matches #"[a-zA-Z0-9-]+" (:content %)))
            (trx db "String may only contain letters, numbers and hyphens.")]]
          :server-actions {:send [::admin/ban-username-string]}}
         reset-ban-username-string
         (forms/initialize-generic-form db))))

(spec/def ::ban-username-string/content string?)
(spec/def ::ban-username-string/fields
  (spec/keys :req-un [::ban-username-string/content]))
(defmethod forms/form-type ::ban-username-string [_]
  (spec/merge ::ban-username-string/fields
              ::forms/fsm-form))

;;; Require username change form

(defn- reset-require-name-change
  [state]
  (merge state {:message ""
                :preview ""
                :preview-open? false}))

(defn initialize-require-name-change
  "Initial values and state for admin require username change form."
  [db]
  (let [form-id ::require-name-change]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-require-name-change
          :validators
          [[#(or (< (count (:message %)) 2) (> (count (:message %)) 1024))
            (trx db "Message should be between 2 and 1024 characters.")]]
          :server-actions {:send [::admin/send-require-name-change]}
          :actions {:see-preview [::forms/see-preview {:field :message}]
                    :hide-preview [::forms/hide-preview]}}
         reset-require-name-change
         (forms/initialize-generic-form db))))

(spec/def ::require-name-change/message string?)
(spec/def ::require-name-change/preview string?)
(spec/def ::require-name-change/preview-open? boolean?)
(spec/def ::require-name-change/fields
  (spec/keys :req-un [::require-name-change/message
                      ::require-name-change/preview
                      ::require-name-change/preview-open?]))
(defmethod forms/form-type ::require-name-change [_]
  (spec/merge ::require-name-change/fields
              ::forms/fsm-form))

;;; Expire invite codes form

(defn- reset-expire-invite-code
  [state]
  (merge state
         {:option      :now
          :expiration  nil
          :selected    #{}}))

(defn- initialize-expire-invite-code
  "Initial values and state for the admin expire invite code form."
  [db]
  (let [form-id ::expire-invite-code]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-expire-invite-code
          :validators
          [[#(and (= :at (:option %)) (nil? (:expiration %)))
            (trx db "Please enter an expiration date.")]
           [#(empty? (:selected %))
            (trx db "Please select one or more invite codes.")]]
          :server-actions {:change [::admin/expire-invite-codes]}}
         reset-expire-invite-code
         (forms/initialize-generic-form db))))

;; Specs for the expire invite code form.
(spec/def ::expire-invite-code/option #{:now :at :never})
(spec/def ::expire-invite-code/expiration (spec/nilable #(= (type %) js/Date)))
(spec/def ::expire-invite-code/selected (spec/and set? (spec/coll-of string?)))
(spec/def ::expire-invite-code/fields
  (spec/keys :req-un [::option ::expiration ::selected]))
(defmethod forms/form-type ::expire-invite-code [_]
  (spec/merge ::expire-invite-code/fields ::forms/fsm-form))

;;; Generate invite code form

(defn- reset-generate-invite-code
  [state]
  (merge state {:code ""
                :max-uses ""
                :expiration nil}))

(defn- initialize-generate-invite-code
  "Initial values and state for the admin generate invite code form."
  [db]
  (let [form-id ::generate-invite-code]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function reset-generate-invite-code
          :validators
          [[#(nil? (re-matches #"[0-9]+" (:max-uses %)))
            (trx db "Please enter a number for the number of uses.")]]
          :server-actions {:generate [::admin/generate-invite-code]}}
         reset-generate-invite-code
         (forms/initialize-generic-form db))))

(spec/def ::generate-invite-code/code string?)
(spec/def ::generate-invite-code/max-uses string?)
(spec/def ::generate-invite-code/expiration
  (spec/nilable #(= (type %) js/Date)))
(spec/def ::generate-invite-code/fields
  (spec/keys :req-un [::code ::max-uses ::expiration]))
(defmethod forms/form-type ::generate-invite-code [_]
  (spec/merge ::generate-invite-code/fields ::forms/fsm-form))

;;; Invite code settings form

(defn- reset-invite-code-settings
  [state]
  (merge state {:required? false
                :visible? false
                :minimum-level ""
                :per-user ""}))

(defn- initialize-invite-code-settings
  "Initial values and state for the invite code settings form."
  [db]
  (let [form-id ::invite-code-settings]
    (->> {:id form-id
          :form-type form-id
          :disable-on-success? false
          :reset-function nil
          :validators
          [[#(nil? (re-matches #"[0-9]+" (:minimum-level %)))
            (trx db "Please enter a number for the minimum level.")]
           [#(nil? (re-matches #"[1-9][0-9]*" (:per-user %)))
            (trx db "Please enter a number for the max invites per user.")]]
          :server-actions {:save-changes [::admin/update-invite-code-settings]}}
         reset-invite-code-settings
         (forms/initialize-generic-form db))))

;; Specs for the invite code settings form.
(spec/def ::invite-code-settings/required? boolean?)
(spec/def ::invite-code-settings/visible boolean?)
(spec/def ::invite-code-settings/minimum-level string?)
(spec/def ::invite-code-settings/per-user string?)
(spec/def ::invite-code-settings/fields
  (spec/keys :req-un [::invite-code-settings/required?
                      ::invite-code-settings/visible?
                      ::invite-code-settings/minimum-level
                      ::invite-code-settings/per-user]))
(defmethod forms/form-type ::invite-code-settings [_]
  (spec/merge ::invite-code-settings/fields
              ::forms/fsm-form))

;;; Search invite codes form

(defn- reset-search-invite-code
  [state]
  (assoc state :code ""))

(defn- initialize-search-invite-code
  "Initial values and state for the admin search invite code form."
  [db]
  (->> {:id ::search-invite-code
        :form-type ::search-invite-code
        :disable-on-success? false
        :reset-function nil
        :validators
        [[#(empty? (str/trim (:code %)))
          (trx db "Please enter a code to search for.")]]
        :actions {:search   [::admin/search-invite-code]
                  :show-all [::admin/show-all-invite-codes]}}
       reset-search-invite-code
       (forms/initialize-generic-form db)))

;; Specs for the search invite code form.
(spec/def ::search-invite-code/code string?)
(spec/def ::search-invite-code/fields
  (spec/keys :req-un [::search-invite-code/code]))
(defmethod forms/form-type ::search-invite-code [_]
  (spec/merge ::search-invite-code/fields
              ::forms/fsm-form))

(defn initialize-invite-code-forms
  [db]
  (-> db
      initialize-invite-code-settings
      initialize-generate-invite-code
      initialize-search-invite-code
      initialize-expire-invite-code))
