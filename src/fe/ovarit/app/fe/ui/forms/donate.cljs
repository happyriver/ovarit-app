;; fe/ui/forms/donate.cljs -- Create donate form for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.forms.donate
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.donate :as donate :refer [parse-amount]]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.core :as forms]))

(defn- reset-donate-form
  [state]
  (assoc state
         :custom-amount nil
         :selection     nil
         :recurring?    true))

(defn initialize-donate-form
  "Initial values and state for the donate form."
  [db]
  (let [form-id ::donate
        {:keys [donate-minimum donate-presets]} (:settings db)]
    (->> {:id                  form-id
          :form-type           form-id
          :disable-on-success? false
          :reset-function      reset-donate-form
          :validators
          [[#(nil? (:selection %))
            (trx db "Please choose an amount.")]
           [#(and (= (:selection %) :custom)
                  (zero? (parse-amount (:custom-amount %))))
            (trx db
                 "Please enter an amount to donate in US dollars, without cents.")]
           [#(and (= (:selection %) :custom)
                  (< (parse-amount (:custom-amount  %)) donate-minimum))
            (trx db "Please enter an amount greater than $1.")]
           [#(and (not= (:selection %) :custom)
                  (not ((set donate-presets) (:selection %))))
            (trx db "Invalid selection.")]]
          :server-actions {:continue [::donate/continue-to-checkout]}}
         reset-donate-form
         (forms/initialize-generic-form db))))

;; Specs for the donate form.
(spec/def ::custom-amount (spec/nilable integer?))
(spec/def ::selection (spec/nilable (spec/or :custom #(= % :custom)
                                             :value integer?)))
(spec/def ::recurring? boolean?)
(spec/def ::fields (spec/keys :req-un [::custom-amount
                                       ::selection
                                       ::recurring?]))
(defmethod forms/form-type ::donate [_]
  (spec/merge ::fields ::forms/fsm-form))
