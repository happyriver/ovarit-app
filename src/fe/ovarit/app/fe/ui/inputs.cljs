;; fe/ui/inputs.cljs -- Inputs for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.ui.inputs
  (:require
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.ui :as ui]
   [re-frame.core :as re-frame]
   [re-frame.std-interceptors :refer [path]]))

;;; Support the markdown editor insert hyperlink modal.

(re-frame/reg-sub ::markdown-link-modal-state
  ;; :doc Extract the state of the markdown editor link modal
  :<- [::ui/state]
  (fn [state _]
    (:markdown-link-modal state)))

(re-frame/reg-sub ::show-markdown-link-modal?
  ;; :doc Extract whether the hyperlink modal should be shown.
  :<- [::markdown-link-modal-state]
  (fn [state _]
    (:show? state)))

(re-frame/reg-sub ::markdown-link-modal-value
  ;; :doc Extract the value of the hyperlink modal text box
  :<- [::markdown-link-modal-state]
  (fn [state _]
    (:value state)))

(re-frame/reg-event-db ::markdown-link-modal-update
  ;; :doc Set the value of the text field in the hyperlink modal.
  [(path [:ui-state :markdown-link-modal])]
  (fn-traced [db [_ val]]
    (assoc db :value val)))

(re-frame/reg-event-db ::show-markdown-link-modal
  ;; :doc Show or hide the markdown editor hyperlink modal.
  ;; :doc Pass it the editor's save function and it will show the
  ;; :doc modal.  Pass it nil and it will hide the modal.
  [(path [:ui-state :markdown-link-modal])]
  (fn-traced [db [_ save-fn]]
    (if save-fn
      (assoc db
             :show? true
             :save-fn save-fn
             :value "")
      (assoc db :show? false))))

(re-frame/reg-event-fx ::insert-markdown-link
  ;; :doc Insert the entered hyperlink into the markdown editor.
  [(path [:ui-state :markdown-link-modal])]
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc db :show? false)
     :fx [[::markdown-editor-callback db]]}))

(re-frame/reg-fx ::markdown-editor-callback
  ;; :doc Call the markdown editor update function with the entered value.
  (fn [{:keys [save-fn value]}]
    (save-fn value)))
