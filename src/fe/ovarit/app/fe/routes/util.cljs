;; fe/routes/util.cljs -- Routing for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.routes.util
  (:require
   [bidi.bidi :as bidi]
   [clojure.string :as str]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.ui.inputs :as inputs]
   [re-frame.core :as re-frame])
  (:import
   [goog Uri]
   [goog.Uri QueryData]))

(defn- unpack-query-string
  "Return the URL query parameters as a map, given a goog.Uri."
  [uri]
  (let [query-data (.getQueryData ^Uri uri)
        params (zipmap ^js/Object (.getKeys query-data)
                       ^js/Object (.getValues query-data key))]
    ;; Keywordize the keys and ignore empty strings.
    (reduce-kv (fn [m k v]
                 (if (= "" v)
                   m
                   (assoc m (keyword k) v)))
               {} params)))

(defn parse-url
  "Parse a URL into handler keyword, route params and url params.
  Return a map."
  [routes url]
  (log/debug {:url url} "Matching url")
  (let [uri (Uri. url)
        uri-path (.getPath uri)
        ;; Remove the trailing "/" if there is one (except for the root path).
        path (cond
               (= uri-path "/") "/"
               (str/ends-with? uri-path "/") (apply str (butlast uri-path))
               :else uri-path)
        url-params (unpack-query-string uri)
        fragment (.getFragment uri)
        match (bidi/match-route routes path)]
    (log/debug {:url url :url-params url-params :path path
                :fragment fragment
                :match match
                :route-params (:route-params match)
                :handler (:handler match)}
               "Matched url")
    (assoc match :url-params url-params :fragment fragment)))

(defn pack-query-string
  "Return a query string (including the leading '?')."
  [params]
  (if (empty? params)
    ""
    (str "?" (.toString (QueryData/createFromMap
                         (clj->js params))))))

(defn- url
  [routes handler args]
  (let [{:keys [query-args]} (apply hash-map args)
        query-string (pack-query-string query-args)]
    (try
      (str
       (apply bidi/path-for (into [routes handler] args))
       query-string)
      (catch js/Error e
        (log/error {:handler handler :exception e} "url-for error")
        "/"))))

(defn url-for
  "Construct the URI for a route handler.
  If the key :query-string is in args use its value (a map) to
  construct a query string."
  [{:keys [routes] :as _db} handler & args]
  (url routes handler args))

(re-frame/reg-sub ::routes
  ;; :doc Extract the routing table.
  (fn [db _]
    (:routes db)))

(re-frame/reg-sub ::url-for
  ;; :doc Construct the URI for a route handler.
  ;; :doc If the key :query-string is in args use its value (a map) to
  ;; :doc construct a query string.
  :<- [::routes]
  (fn [routes [_ handler & args]]
    (url routes handler args)))

(def completes-routing
  "After this handler, mark routing as complete"
  (re-frame/->interceptor
   :id ::completes-routing
   :after
   (fn [{:keys [coeffects effects] :as context}]
     (let [db (-> (or (:db effects) (:db coeffects))
                  (assoc-in [:status :routing] :complete))]
       (assoc-in context [:effects :db] db)))))

(def hide-markdown-editor-on-leave
  "Add hiding the markdown editor to the on-leave events."
  (re-frame/->interceptor
   :id ::hide-markdown-editor-on-leave
   :after
   (fn [{:keys [coeffects effects] :as context}]
     (let [db (-> (or (:db effects) (:db coeffects))
                  (update-in [:view :on-leave] conj
                             [::inputs/show-markdown-link-modal]))]
       (assoc-in context [:effects :db] db)))))
