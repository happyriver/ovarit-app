;; fe/routes/auth.cljs -- Auth routes for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.routes.auth
  (:require [day8.re-frame.tracing :refer-macros [fn-traced]]
            [ovarit.app.fe.config :as config]
            [ovarit.app.fe.content.auth :as auth]
            [ovarit.app.fe.content.loader :as loader]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes.util :as route-util]
            [ovarit.app.fe.tr :refer [trx]]
            [ovarit.app.fe.ui.forms.auth :as auth-forms]
            [ovarit.app.fe.ui.forms.login :as login-form]
            [ovarit.app.fe.ui.page-title :as page-title]
            [ovarit.app.fe.ui.window :as window]
            [ovarit.app.fe.user :as user]
            [re-frame.core :as re-frame]
            [weavejester.dependency :as dep])
  (:import [goog Uri]))

(defn init []
  (log/info {} "Auth module loaded"))

(defn init-auth-route [event]
  (re-frame/dispatch event)
  (re-frame/dispatch [::window/set-force-day-mode true]))

(defn- host-and-scheme-match? [url]
  (let [uri (Uri. url)
        scheme (.getScheme uri)
        domain (.getDomain uri)]
    (and (or (empty? scheme) config/debug? (= scheme "https"))
         (or (empty? domain) (= domain (.. js/window -location -host))))))

(re-frame/reg-event-fx ::login
  ;; :doc Initialize the login page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [next notify]}]]
    (let [home-url (route-util/url-for db :home/index)
          ;; To avoid open redirects and 404s, only redirect to paths
          ;; with matching domains which can be found in our routing
          ;; table, otherwise send people to the home page.
          safe-next-url (when (and next
                                   (host-and-scheme-match? next)
                                   (:handler (route-util/parse-url
                                              (:routes db) next)))
                          next)
          next-url (or safe-next-url home-url)]
      {:db (-> db
               (assoc-in [:view :active-panel] :login)
               (assoc-in [:view :options] {:notify (keyword notify)
                                           :next-url next-url})
               (login-form/initialize-login-form next-url))
       :fx [[:dispatch [::page-title/update (trx db "Log in")]]]})))

(re-frame/reg-event-fx ::registration
  ;; :doc Initialize the registration page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :registration)
             (assoc-in [:view :options] {}))
     :fx [[:dispatch [::auth/prepare-auth-forms :registration]]
          [:dispatch [::page-title/update (trx db "Register")]]]}))

(re-frame/reg-event-fx ::verify-registration
  ;; :doc Initialize the verify registration page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [delete_code uid]}]]
    (let [{:keys [email
                  username]} (get-in db [:content :verify-registration uid])]
      {:db (-> db
               (assoc-in [:view :active-panel] :verify-registration)
               (assoc-in [:view :options] {:delete-code delete_code
                                           :uid uid
                                           :email email
                                           :username username
                                           :already-verified? false}))
       :fx [[:dispatch [::auth/prepare-auth-forms :verify-registration]]
            [:dispatch [::page-title/update (trx db "Registered")]]]})))

(re-frame/reg-event-fx ::confirm-registration
  ;; :doc Initialize the confirm registration page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :confirm-registration)
             (assoc-in [:view :options] {:code (or code "")
                                         :state :loading}))
     :fx [[:dispatch [::auth/start-confirmation :registration]]
          [:dispatch [::page-title/update (trx db "Confirm registration")]]]}))

(re-frame/reg-event-fx ::resend-verification-email
  ;; :doc Initialize the resend verification email page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :resend-verification-email)
             (assoc-in [:view :options] {})
             (auth-forms/initialize-username-form))
     :fx [[:dispatch [::page-title/update (trx db "Resend email")]]]}))

(re-frame/reg-event-fx ::start-reset-password
  ;; :doc Initialize the start reset password page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :start-reset-password)
             (assoc-in [:view :options] {})
             (auth-forms/initialize-start-reset-form))
     :fx [[:dispatch [::page-title/update (trx db "Reset password")]]]}))

(re-frame/reg-event-fx ::complete-reset-password
  ;; :doc Initialize the complete reset password page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [name code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :complete-reset-password)
             (assoc-in [:view :options] {:username name
                                         :code (or code "")
                                         :bad-code? false}))
     :fx [[:dispatch [::auth/prepare-auth-forms :complete-reset-password]]
          [:dispatch [::page-title/update (trx db "Reset password")]]]}))

(re-frame/reg-event-fx ::edit-account
  ;; :doc Initialize the edit account page.
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :edit-account)
             (assoc-in [:view :options] {}))
     :fx [(when authenticated?
            [:dispatch [::auth/prepare-auth-forms :edit-account]])
          (when authenticated?
            [:dispatch [::page-title/update (trx db "Edit account")]])]}))

(def rename-account-deps
  "Initialization events for the rename account page."
  (-> (dep/graph)
      (dep/depend
       [::auth-forms/initialize-rename-form] [::auth/load-rename-settings])))

(re-frame/reg-event-fx ::rename-account
  ;; :doc Initialze the rename account page.
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :rename-account)
             (assoc-in [:view :options] {}))
     :fx [(when authenticated?
            [:dispatch [::loader/init-content-loader
                        {:graph rename-account-deps}]])
          (when authenticated?
            [:dispatch [::page-title/update (trx db "Rename account")]])]}))

(re-frame/reg-event-fx ::confirm-email
  ;; :doc Initialize the confirm email page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [code]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :confirm-email)
             (assoc-in [:view :options] {:code code
                                         :state :loading}))
     :fx [[:dispatch [::auth/start-confirmation :email]]
          [:dispatch [::page-title/update (trx db "Confirm email")]]]}))

(re-frame/reg-event-fx ::delete-account
  ;; :doc Initialize the delete account page.
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :delete-account)
             (assoc-in [:view :options] {})
             (cond-> authenticated?
               (auth-forms/initialize-delete-account-form)))
     :fx [(when authenticated?
            [:dispatch [::page-title/update (trx db "Delete account")]])]}))
