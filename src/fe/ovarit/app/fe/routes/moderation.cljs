;; fe/routes/moderation.cljs -- Mod and admin routes for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.routes.moderation
  (:require [day8.re-frame.tracing :refer-macros [fn-traced]]
            [ovarit.app.fe.content.admin :as admin]
            [ovarit.app.fe.content.loader :as loader]
            [ovarit.app.fe.content.settings :as settings]
            [ovarit.app.fe.content.subs :as subs]
            [ovarit.app.fe.log :as log]
            [ovarit.app.fe.routes.util :as route-util]
            [ovarit.app.fe.ui.forms.admin :as admin-forms]
            [ovarit.app.fe.ui.forms.moderation :as mod-forms]
            [ovarit.app.fe.util :as util]
            [ovarit.app.fe.util.relic :as rel]
            [re-frame.core :as re-frame]
            [weavejester.dependency :as dep]))

;; Module management

(defn on-load []
  (re-frame/dispatch [::on-module-load]))

(re-frame/reg-event-db ::on-module-load
  (fn-traced [db [_ _]]
    (update-in db [:status :module] dissoc :mod)))

(defn init []
  (log/info {} "Loaded moderation module"))

;; Route initialization

(re-frame/reg-event-fx ::admin-stats
  ;; :doc Init the admin stats page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-stats)
             admin/init-dates)
     :fx [[:dispatch [::admin/load-stats]]
          [:dispatch [::admin/load-stats-timespan]]
          [:dispatch [::admin/load-visitor-counts :daily]]
          [:dispatch [::admin/load-visitor-counts :weekly]]]}))

(re-frame/reg-event-fx ::admin-voting
  ;; :doc Init the admin view user votes page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [name types first last before after]}]]
    (let [paging? (= :admin-voting (-> db :view :active-panel))
          types (or (-> types
                        keyword
                        #{:post :comment :all})
                    :all)
          options {:name name
                   :types types
                   :first (util/parse-int first)
                   :last (util/parse-int last)
                   :before before
                   :after after
                   :same-page? paging?}]
      {:db (-> db
               (assoc-in [:view :active-panel] :admin-voting)
               (assoc-in [:view :options] options)
               (dissoc [:content :admin-voting])
               admin/init-vote-removal-machine)
       :fx [[:dispatch [::admin/load-votes
                        (assoc options :paging? paging?)]]
            [:dispatch [::settings/subscribe
                        [::admin/subscribe-vote-removal name]]]]})))

(re-frame/reg-event-fx ::admin-banned-user-names
  ;; :doc Init the admin banned user names page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-banned-user-names)
             (assoc-in [:view :options] {:sort-typ :string
                                         :sort-asc? true})
             admin-forms/initialize-ban-username-string)
     :fx [[:dispatch [::admin/load-banned-username-strings]]]}))

(re-frame/reg-event-fx ::admin-require-name-change
  ;; :doc Init the admin require name change page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [name]}]]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-require-name-change)
             (assoc-in [:view :options] {:name name})
             admin-forms/initialize-require-name-change)
     :fx [[:dispatch [::admin/load-required-name-change]]]}))

(re-frame/reg-event-fx ::admin-invite-codes
  ;; :doc Init the admin invite codes page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} _]
    {:db (-> db
             (assoc-in [:view :active-panel] :admin-invite-codes)
             (assoc-in [:view :options] {:search :all})
             admin-forms/initialize-invite-code-forms)
     :fx [[:dispatch [::admin/load-invite-codes-and-settings]]]}))

(def mod-edit-sub-rules-deps
  "Initialization events for the mod edit sub rules panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-sub-rules] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])))

(re-frame/reg-event-fx ::mod-edit-sub-rules
  ;; :doc Init the edit sub rules panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ options]]
    {:db (-> db
             (assoc :view {:active-panel :mod-edit-sub-rules
                           :options (select-keys options [:sub])})
             mod-forms/initialize-create-sub-rule-form)
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-sub-rules-deps}]]]}))

(def mod-edit-post-flairs-deps
  "Initialization events for the mod edit post flairs panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-post-flairs] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])))

(re-frame/reg-event-fx ::mod-edit-post-flairs
  ;; :doc Init the edit sub flairs panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ options]]
    {:db (-> db
             (assoc :view {:active-panel :mod-edit-post-flairs
                           :options (select-keys options [:sub])})
             (update :reldb rel/transact [:delete :SubPostFlairState])
             mod-forms/initialize-create-post-flair-form
             mod-forms/initialize-edit-post-flair-fsm)
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-post-flairs-deps}]]]}))

(def mod-edit-post-types-deps
  "Initialization events for the mod edit post types panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/load-post-types] [::subs/check-sub])
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])
      (dep/depend
       [::mod-forms/initialize-edit-post-types] [::subs/load-post-types])))

(re-frame/reg-event-fx ::mod-edit-post-types
  ;; :doc Show the mod edit post types panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [sub]}]]
    {:db (assoc db :view {:active-panel :mod-edit-post-types
                          :options {:sub sub}})
     :fx [[:dispatch [::loader/init-content-loader
                      {:graph mod-edit-post-types-deps}]]]}))
