(ns ovarit.app.fe.polyfill
  (:require ["globalthis" :as globalthis]))

(defn init []
  (globalthis/shim))
