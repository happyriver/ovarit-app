;; fe/graphql.clj -- GraphQL macros for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.graphql
  (:require
   [cljs.analyzer :as ana]
   #_:clj-kondo/ignore [cljs.env :as env]
   [clojure.edn :as edn]
   [graphql-builder.core :as graphql-builder]
   [graphql-builder.parser :as parser]
   [com.rpl.specter :as s]
   [clojure.java.io :as io]))

(defn- read-query-hash-table
  [path]
  (->> (io/resource path)
       slurp
       edn/read-string
       (map (fn [elem] [(:query elem) elem]))
       (into {})))

(def query-hash-table (read-query-hash-table "query-hashes.edn"))

(defmacro inline-query-hash-table
  "Parses the query hash table from the given resource path."
  []
  query-hash-table)

(defn- find-hash
  [query-fn]
  (let [query (-> (query-fn) :graphql :query)
        {:keys [type hash]} (get query-hash-table query)]
    (if hash
      (str (name type) " " hash)
      query)))

(defmacro defgraphql [name path]
  (when-not (string? path)
    (throw (ana/error &env
                      (str "defgraphql requires a literal string argument"))))
  (let [parsed (-> (io/resource path)
                   slurp
                   parser/parse
                   (graphql-builder/query-map {:inline-fragments true}))
        hashed (s/transform [s/MAP-VALS s/MAP-VALS] #(find-hash %)
                            parsed)]
    `(def ~name ~hashed)))
