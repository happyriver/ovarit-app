;; fe/util.cljs -- Utility functions for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.util
  (:require
   ["markdown-it" :as markdown-it]
   ["markdown-it-link-attributes" :as markdown-it-link-attributes]
   ["markdown-it-reddit-spoiler" :as markdown-it-reddit-spoiler]
   ["markdown-it-sub" :as markdown-it-sub]
   ["markdown-it-sup" :as markdown-it-sup]
   [ajax.core :as ajax]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [goog.url]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes :as-alias routes]
   [re-frame.core :as re-frame]
   [re-frame.interceptor :refer [->interceptor
                                 get-coeffect assoc-coeffect
                                 get-effect assoc-effect]]
   [statecharts.core :as fsm]))

;; The ClojureScript and shadow.cljs methods for dynamically
;; loading modules are all built on goog.module.ModuleLoader,
;; which doesn't work unless you add `unsafe-eval` to your CSP.
(defn load-script
  "Add a URL as a script tag to the document, returning a promise."
  [url]
  (log/info {:url url} "Adding script tag")
  (let [script (js/document.createElement "script")
        promise (js/Promise.
                 (fn [on-load on-error]
                   (set! (.-onload script) #(on-load true))
                   (set! (.-onerror script) #(on-error %))))]

    (set! (.-src script) url)
    (.appendChild js/document.head script)
    promise))

(re-frame/reg-fx ::load-and-run
  ;; :doc Run a function from a module.
  ;; :doc If `module-url` is non-null, attach it to the doc in a
  ;; :doc script tag and wait for it to load before running the
  ;; :doc function.
  (fn [[module-url func]]
    (if-not module-url
      (func)
      (.then (load-script module-url)
             (fn [_] (func))
             (fn [_]
               (re-frame/dispatch [::routes/show-error :network-error]))))))

(re-frame/reg-event-fx ::load-module-and-run-fn
  ;; :doc Add a script tag for a module if it's not already done.
  ;; :doc After it's loaded, resolve `func` and call it.
  (fn-traced [{:keys [db]} [_ module func]]
    (let [module-url (-> db :status :manifest module)]
      {:db (-> db
               (update-in [:status :manifest] dissoc module)
               (update-in [:status :module] dissoc module))
       :fx [[::load-and-run [module-url func]]]})))

(defn parse-json
  "Parse JSON from a string and keywordize the keys."
  [str]
  (-> str
      js/JSON.parse
      (js->clj :keywordize-keys true)))

(defn zero-pad
  "Convert `num` to a string and pad it on the left with 0s to `width`."
  [num width]
  (. (str num) padStart width "0"))

(defn kebab-case-keys
  "Translate the keys in m from snake_case to kebab-case."
  [data]
  (cske/transform-keys csk/->kebab-case data))

(defn- loggable-response
  "Make response logging less verbose."
  [resp]
  (dissoc resp :status-text :original-text))

(re-frame/reg-fx ::ajax-post-internal
  ;; :doc Make an AJAX POST request.
  (fn-traced [[url data success-event error-event]]
    (let [origin (.. js/window -location -origin)
          handler (fn [resp]
                    (log/info {:url url
                               :data (dissoc data :csrf_token)
                               :resp resp}
                              "Received AJAX response")
                    (when success-event
                      (re-frame/dispatch (conj success-event resp))))
          error-handler (fn [{:keys [status failure original-text] :as resp}]
                          ;; ovarit-auth sends empty response bodies,
                          ;; and cljs-ajax does not approve.
                          (if (and (= status 202)
                                   (= failure :parse)
                                   (= original-text ""))
                            (handler resp)
                            (do
                              (log/warn {:url url
                                         :data (dissoc data :csrf_token)
                                         :resp (loggable-response resp)}
                                        "Received AJAX error response")
                              (when error-event
                                (re-frame/dispatch (conj error-event resp))))))]
      (ajax/POST (str origin url)
                 {:params data
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(re-frame/reg-event-fx ::ajax-form-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  ;; :doc This is the older API, use ::ajax-csrf-form-post instead.
  (fn-traced [{:keys [db]} [_ [url data success-event error-event]]]
    (let [csrf-token (get-in db [:settings :csrf-token])
          payload (assoc data :csrf_token csrf-token)]
      {::ajax-post-internal [url payload success-event error-event]})))

(re-frame/reg-fx ::ajax-delete
  ;; :doc Make an AJAX DELETE request.
  (fn-traced [[url success-event error-event]]
    (ajax/DELETE url
                 {:handler #(re-frame/dispatch (conj success-event %))
                  :error-handler #(re-frame/dispatch (conj error-event %))
                  :format :json
                  :response-format :json
                  :keywords? true})))

;; Multistage ajax get and post events

(defn- ajax-success-handler [{:keys [url payload success-event] :as state}]
  (fn [resp]
    (log/info {:url url :payload payload :resp resp}
              "Received AJAX response")
    (when success-event
      (re-frame/dispatch (->> (assoc state :response resp)
                              (conj success-event))))))

(defn- ajax-failure-handler [{:keys [url payload error-event] :as state}
                             success-handler]
  (fn [{:keys [status failure original-text] :as resp}]
    ;; ovarit-auth sends empty response bodies, and cljs-ajax does not
    ;; approve.
    (if (and (< status 300) (= failure :parse) (= original-text ""))
      (success-handler resp)
      (do
        (log/warn {:url url :payload payload :resp (loggable-response resp)}
                  "Received AJAX error response")
        (when error-event
          (re-frame/dispatch (->> (assoc state :error-response resp)
                                  (conj error-event))))))))

(re-frame/reg-fx ::ajax-get
  ;; :doc Make an AJAX GET request.
  (fn-traced [{:keys [url] :as state}]
    (let [handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/GET url
                {:handler handler
                 :error-handler error-handler
                 :response-format :json
                 :keywords? true}))))

(re-frame/reg-fx ::ajax-post
  ;; :doc Make an AJAX POST request.
  (fn-traced [{:keys [url payload] :as state}]
    (let [origin (.. js/window -location -origin)
          handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/POST (str origin url)
                 {:params payload
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(re-frame/reg-fx ::ajax-post-formdata
  ;; :doc Make an AJAX POST request with a js/FormData object.
  (fn-traced [{:keys [url payload] :as state}]
    (let [origin (.. js/window -location -origin)
          handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (ajax/POST (str origin url)
                 {:body payload
                  :response-format :json
                  :keywords? true
                  :format :json
                  :handler handler
                  :error-handler error-handler}))))

(re-frame/reg-fx ::ajax-put
  ;; :doc Make an AJAX PUT request.
  (fn-traced [{:keys [url payload] :as state} [_ _]]
    (let [origin (.. js/window -location -origin)
          handler (ajax-success-handler state)
          error-handler (ajax-failure-handler state handler)]
      (log/info {:url url} "ajax-put")
      (ajax/PUT url #_(str origin url)
                {:body payload
                 :handler handler
                 :error-handler error-handler}))))

(defn add-prefix
  "Add a prefix to a keyword, preserving the namespace if any."
  [id extra]
  (let [with-prefix (str (name extra) "-" (name id))]
    (if (namespace id)
      (keyword (namespace id) with-prefix)
      (keyword with-prefix))))

(defn add-suffix
  "Add a suffix to a keyword, preserving the namespace if any."
  [id extra]
  (let [with-suffix (str (name id) "-" (name extra))]
    (if (namespace id)
      (keyword (namespace id) with-suffix)
      (keyword with-suffix))))

(defn- ajax-handler-events
  "Add success and failure events to the ajax-* fx in the context."
  [success-event failure-event]
  (re-frame/->interceptor
   :id ::ajax-result-events
   :after (fn [context]
            (update-in context [:effects :fx]
                       (fn [fx]
                         (map #(cond-> %
                                 (#{::ajax-post ::ajax-get} (first %))
                                 (update 1 assoc
                                         :success-event success-event
                                         :error-event failure-event))
                              fx))))))

(defn reg-ajax-event-chain
  "Register a set of three re-frame events to handle an AJAX Post request.
  Arguments are a keyword `id` for naming the events, and the three
  functions in a map with keys :call, :success and :failure.  Any use
  of ::ajax-post in the :call function's :fx value will be modified to
  set up the :success function as the event handler for a successful
  response and the :failure function as the event handler for an error
  response."
  [id {:keys [call success failure]}]
  (let [success-id (add-suffix id :success)
        failure-id (add-suffix id :failure)
        call-interceptor (ajax-handler-events [success-id] [failure-id])]
    (re-frame/reg-event-fx id [call-interceptor] call)
    (re-frame/reg-event-fx success-id success)
    (re-frame/reg-event-fx failure-id failure)))

(re-frame/reg-event-fx ::ajax-csrf-form-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  (fn-traced [{:keys [db]} [_ state]]
    (let [csrf-token (get-in db [:settings :csrf-token])]
      {::ajax-post (update state :payload assoc :csrf_token csrf-token)})))

(re-frame/reg-event-fx ::ajax-csrf-formdata-post
  ;; :doc Make an AJAX POST request with the CSRF token included.
  ;; :doc The :payload field of `state` should be a js FormData object
  ;; :doc and the form will be sent as multipart/form-data.
  (fn-traced [{:keys [db]} [_ state]]
    (let [csrf-token (get-in db [:settings :csrf-token])]
      (.append (:payload state) "csrf_token" csrf-token)
      {::ajax-post-formdata state})))

(defn- ajax-form-handler-events
  "Add success and failure events to the ajax-* fx in the context."
  [success-event failure-event]
  (re-frame/->interceptor
   :id ::ajax-result-events
   :after
   (fn [context]
     (update-in context [:effects :fx]
                (fn [fx]
                  (map (fn [effect]
                         (let [[action & args] effect
                               event-kw (-> args first first)]
                           (cond-> effect
                             (and (= :dispatch action)
                                  (#{::ajax-csrf-formdata-post
                                     ::ajax-csrf-form-post} event-kw))
                             (update-in [1 1] assoc
                                        :success-event success-event
                                        :error-event failure-event))))
                       fx))))))

(defn reg-ajax-form-event-chain
  [id {:keys [call success failure]}]
  (let [success-id (add-suffix id :success)
        failure-id (add-suffix id :failure)
        call-interceptor (ajax-form-handler-events [success-id] [failure-id])]
    (re-frame/reg-event-fx id [call-interceptor] call)
    (re-frame/reg-event-fx success-id success)
    (re-frame/reg-event-fx failure-id failure)))

;; Translate markdown to html.

(set! (.. markdown-it-reddit-spoiler -openTag)
      "<spoiler>")
(set! (.. markdown-it-reddit-spoiler -closeTag) "</spoiler>")

(def re-mention #"^([a-zA-Z0-9_-]+)")

(defn make-mentions-handler
  "Define a handler for markdown-it's linkify to recognize mentions."
  [prefix link-prefix]
  (clj->js
   {:validate (fn [text pos _self]
                (let [tail (subs text pos)
                      match (re-find re-mention tail)]
                  (if match
                    (count (first match))
                    0)))
    :normalize (fn [match]
                 (let [url (. match -url)]
                   (set! (.. match -url)
                         (str link-prefix (subs url (count prefix))))))}))

(defn create-markdown-renderer
  "Create a customized markdown-it object."
  [{:keys [sub-prefix]}]
  (let [sub-mention (str "/" sub-prefix "/")
        obj (-> (markdown-it (clj->js {:linkify true}))
                ;; Disable the code blocks that start with leading spaces,
                ;; but not the backtick or tilde-fenced ones.
                (.disable (clj->js ["code"]))
                (.use markdown-it-reddit-spoiler/spoiler)
                (.use markdown-it-reddit-spoiler/blockquote)
                (.use markdown-it-sub)
                (.use markdown-it-sup)
                (.use markdown-it-link-attributes
                      (clj->js {:matcher (fn [href _]
                                           (not (str/starts-with? href "/")))
                                :attrs {:rel "noopener nofollow ugc"
                                        :target "_blank"}})))]
    (. (. obj -linkify) set (clj->js {:fuzzyLink false}))
    (. (. obj -linkify) add "@" (make-mentions-handler "@" "/u/"))
    (. (. obj -linkify) add "/u/" (make-mentions-handler "/u/" "/u/"))
    (. (. obj -linkify) add sub-mention (make-mentions-handler sub-mention
                                                               sub-mention))
    (log/info {:sub-prefix sub-prefix} "Initialized markdown renderer")
    obj))

(def md (atom nil))

(re-frame/reg-fx ::set-markdown-renderer
  ;; :doc Create the markdown renderer.
  (fn-traced [renderer]
    (reset! md renderer)))

(re-frame/reg-event-fx ::start-markdown-renderer
  ;; :doc Create the markdown renderer object.
  (fn-traced [_ [_ options]]
    {:fx [[::set-markdown-renderer (create-markdown-renderer options)]]}))

(defn markdown-to-html
  "Convert markdown to sanitized HTML."
  [text]
  (if (empty? text)
    ""
    (try
      (.render @md text)
      (catch js/Error e
        (log/error {:exception e} "Error rendering markdown")
        "<p class=\"p i gray\">Error showing content.
         Refresh the page, and if the problem persists, please
         report it to the admins.</p>"))))

(defn markdown-blank?
  "Determine if some markdown contains only formatting characters."
  [text]
  (try
    (let [html (markdown-to-html text)
          doc (.parseFromString (js/DOMParser.) html "text/html")
          content (.. doc -documentElement -textContent)]
      (str/blank? content))
    (catch js/Error e
      (log/error {:exception e} "Error checking for non-blank content")
      false)))

(defn case-insensitive-compare
  "Compare two strings, disregarding case."
  [a b]
  (compare (str/lower-case a) (str/lower-case b)))

(defn reorder-list
  "Return a new list containing the same elements as `elems`, with the
  item at `move-index` moved to `dest-index`."
  [elems move-index dest-index]
  (let [moved-elem (nth elems move-index)
        elems-before (take move-index elems)
        elems-after (take-last (- (count elems) move-index 1) elems )]
    (if (< dest-index move-index)
      (concat (take dest-index elems-before)
              [moved-elem]
              (take-last (- (count elems-before) dest-index) elems-before)
              elems-after)
      (concat  elems-before
               (take (- dest-index (count elems-before)) elems-after)
               [moved-elem]
               (take-last (- (count elems) dest-index 1) elems-after)))))

(defn update-or-add
  "Either replace or add a map to a sequence.
  If an item or items in `coll` matching `item` (using the `match?`
  funtion) are found, replace them with `item`, otherwise append
  `item` to the end of `coll`."
  [coll item match?]
  (let [existing (filter match? coll)]
    (if (empty? existing)
      (concat coll [item])
      (map #(if (match? %) item %) coll))))

;;; Re-frame Interceptors

(defn path-fn
  "Interceptor to substitute a path in the db for the db.
  Like the re-frame interceptor `path`, except it uses a function
  to calculate the path from the context."
  [calc-path]
  (->interceptor
   :id :comment-data
   :before
   (fn [context]
     (let [original-db (get-coeffect context :db)
           path (calc-path context)]
       (-> context
           (update ::db-store-key conj original-db)
           (update ::path conj path)
           (assoc-coeffect :db (get-in original-db path)))))

   :after
   (fn [context]
     (let [db-store (::db-store-key context)
           original-db (peek db-store)
           new-db-store (pop db-store)
           path (peek (::path context))
           context' (-> context
                        (assoc ::db-store-key new-db-store)
                        (update ::path pop)
                        (assoc-coeffect :db original-db))
           db (get-effect context :db ::not-found)]
       (if (= db ::not-found)
         context'
         (->> (assoc-in original-db path db)
              (assoc-effect context' :db)))))))

(defn trimmed-count
  "Return the number of characters in a string after trimming whitespace."
  [s]
  (-> s str/trim count))

(defn parse-int
  "Parse a string into a number.
  Return nil if the argument is nil."
  [s]
  (when s (js/parseInt s)))

(defn to-ISO-time
  "Convert a value in milliseconds to ISO 8601."
  [s]
  (when s
    (if (str/includes? (str s) "T")
      s
      (.toISOString (js/Date. (js/parseInt s))))))

(defn to-epoch-time
  "Convert a datetime value to an integer containing milliseconds."
  [s]
  (cond
    (and (string? s) (str/includes? s "T"))
    (.getTime (js/Date. s))

    (string? s) (js/parseInt s)

    :else
    s))

(comment
  (js/Date. "2023-11-21T01:51:03.786534556Z"))

(defn valid-url?
  "Return true if a URL can be parsed."
  [link]
  (try
    (goog.url/resolveUrl link)
    true
    (catch js/Error _ false)))

(defn set-of-keywords
  "Convert a collection of strings into a set of keywords."
  [items]
  (->> items
       (map keyword)
       (set)))

;;; Statecharts helpers

(defn queue-fx
  "Wrap a function that returns a re-frame event.
  The wrapped function will add the event to the :fx key in the state map,
  and use fsm/assign to ensure it is persisted."
  [event-fn]
  (fsm/assign (fn [state event]
                (->> (event-fn state event)
                     (update state :fx conj)))))

(defn queue-multi-fx
  "Wrap a function that returns multiple re-frame events.
  The wrapped function will add the events to the :fx key in the state map,
  and use fsm/assign to ensure it is persisted."
  [event-fn]
  (fsm/assign (fn [state event]
                (->> (event-fn state event)
                     (update state :fx concat)))))
