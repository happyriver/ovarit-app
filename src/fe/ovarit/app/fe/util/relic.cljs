;; fe/util/relic.cljs -- Catch and log relic errors for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.util.relic
  (:require
   [com.wotbrew.relic :as rel]
   [ovarit.app.fe.log :as log]))

(defn mat
  [db & queries]
  (try
    (apply rel/mat db queries)
    (catch js/Error e
      (log/error {:queries (str queries)
                  :exception e} "rel/mat")
      db)))

(defn q
  [db query & opts]
  (try
    (apply rel/q db query opts)
    (catch js/Error e
      (log/error {:query (str query)
                  :exception e} "rel/q")
      nil)))

(defn row
  [db q & where-clauses]
  (try
    (apply rel/row db q where-clauses)
    (catch js/Error e
      (log/error {:query (str q)
                  :where-clauses (str where-clauses)
                  :exception e} "rel/row")
      nil)))

(defn top
  [n expr]
  (try
    (rel/top n expr)
    (catch js/Error e
      (log/error {:n n
                  :expr expr
                  :exception e} "rel/top")
      nil)))

(defn transact
  [db & tx]
  (try
    (apply rel/transact db tx)
    (catch js/Error e
      (log/error {:tx (str (mapv #(take 2 %) tx))
                  :exception e} "rel/transact")
      db)))

(def sel rel/sel)
(def sel1 rel/sel1)
(def env rel/env)
