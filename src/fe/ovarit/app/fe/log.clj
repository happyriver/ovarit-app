;; fe/log.clj -- Logging macros for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.log
  (:require
   [cljs.env :as env]))

(def release? (= :advanced (get-in @env/*compiler* [:options :optimizations])))

(defn- log-expr [form level mdc msg]
  `(let [keyvals# ~mdc
         formatter# (:lambdaisland.com.glogi/formatter keyvals# identity)]
     (log (:lambdaisland.com.glogi/logger keyvals# ~(str *ns*))
          ~level
          (formatter#
           (-> keyvals#
               (assoc :message ~msg)
               (dissoc :lambdaisland.com.glogi/logger)
               (assoc :line ~(:line (meta form)))))
          (:exception keyvals#))))

(defmacro shout [mdc msg]
  (log-expr &form :shout mdc msg))

(defmacro error [mdc msg]
  (log-expr &form :error mdc msg))

(defmacro severe [mdc msg]
  (log-expr &form :severe mdc msg))

(defmacro warn [mdc msg]
  (log-expr &form :warn mdc msg))

(defmacro info [mdc msg]
  (when-not release?
    (log-expr &form :info mdc msg)))

(defmacro debug [mdc msg]
  (when-not release?
    (log-expr &form :debug mdc msg)))

(defmacro config [mdc msg]
  (when-not release?
    (log-expr &form :config mdc msg)))

(defmacro trace [mdc msg]
  (when-not release?
    (log-expr &form :trace mdc msg)))

(defmacro fine [mdc msg]
  (when-not release?
    (log-expr &form :fine mdc msg)))

(defmacro finer [mdc msg]
  (when-not release?
    (log-expr &form :finer mdc msg)))

(defmacro finest [mdc msg]
  (when-not release?
    (log-expr &form :finest mdc msg)))

(defmacro spy [form]
  (when-not release?
    (let [res (gensym)]
      `(let [~res ~form]
         ~(log-expr &form :debug [:spy `'~form
                                  :=> res] "spy")
         ~res))))
