;; fe/views.cljs -- Page rendering for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views
  (:require
   [ovarit.app.fe.routes :as-alias routes]
   [ovarit.app.fe.views.donate :as donate]
   [ovarit.app.fe.views.funding-progress :as funding-progress]
   [ovarit.app.fe.views.messages :as messages]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.post-listing :as post-listing]
   [ovarit.app.fe.views.single-post :as single-post]
   [ovarit.app.fe.views.submit-post :as submit-post]
   [ovarit.app.fe.views.user :as user]
   [ovarit.app.fe.views.util :refer [error-boundary]]
   [ovarit.app.fe.views.wiki :as wiki]
   [re-frame.core :refer [subscribe]]))

(defn show-panel [panel-name]
  (let [initializing? @(subscribe [::routes/initializing?])]
    (when-not initializing?
      (case panel-name
        :login                     [(resolve 'ovarit.app.fe.views.auth/login-panel)]
        :registration              [(resolve 'ovarit.app.fe.views.auth/registration-panel)]
        :verify-registration       [(resolve 'ovarit.app.fe.views.auth/verify-registration-panel)]
        :confirm-registration      [(resolve 'ovarit.app.fe.views.auth/confirm-registration-panel)]
        :resend-verification-email [(resolve 'ovarit.app.fe.views.auth/resend-verification-email-panel)]
        :start-reset-password      [(resolve 'ovarit.app.fe.views.auth/start-reset-password-panel)]
        :complete-reset-password   [(resolve 'ovarit.app.fe.views.auth/complete-reset-password-panel)]
        :edit-account              [(resolve 'ovarit.app.fe.views.auth/edit-account-panel)]
        :rename-account            [(resolve 'ovarit.app.fe.views.auth/rename-account-panel)]
        :confirm-email             [(resolve 'ovarit.app.fe.views.auth/confirm-email-panel)]
        :delete-account            [(resolve 'ovarit.app.fe.views.auth/delete-account-panel)]

        :initializing              [misc-panels/initializing-panel]
        :post-listing              [post-listing/post-listing-panel]
        :single-post               [single-post/single-post-panel]
        :submit-post               [submit-post/submit-post-panel]
        :wiki-page                 [wiki/wiki-panel]
        :error-page                [misc-panels/error-panel]
        :mod-dashboard             [(resolve 'ovarit.app.fe.views.moderation/mod-dashboard-panel)]
        :mod-edit-sub-rules        [(resolve 'ovarit.app.fe.views.moderation/mod-edit-rules-panel)]
        :mod-edit-post-flairs      [(resolve 'ovarit.app.fe.views.moderation/mod-edit-post-flairs-panel)]
        :mod-edit-post-types       [(resolve 'ovarit.app.fe.views.moderation/mod-edit-post-types-panel)]
        :modmail                   [(resolve 'ovarit.app.fe.views.modmail/modmail-panel)]
        :modmail-compose           [(resolve 'ovarit.app.fe.views.modmail/modmail-compose-panel)]
        :contact-mods              [messages/contact-mods-panel]
        :admin-stats               [(resolve 'ovarit.app.fe.views.admin/stats-panel)]
        :admin-banned-user-names   [(resolve 'ovarit.app.fe.views.admin/banned-user-names)]
        :admin-require-name-change [(resolve 'ovarit.app.fe.views.admin/require-name-change-panel)]
        :admin-invite-codes        [(resolve 'ovarit.app.fe.views.admin/invite-codes-panel)]
        :admin-voting              [(resolve 'ovarit.app.fe.views.admin/voting-panel)]
        :donate-setup              [donate/setup-panel]
        :donate-cancel             [donate/cancel-panel]
        :donate-success            [donate/success-panel]
        :donate-funding-progress   [funding-progress/funding-progress-content]
        :edit-user-preferences     [user/edit-user-preferences-panel]

        [misc-panels/page-not-found-panel]))))

(defn main-panel []
  (let [active-panel @(subscribe [:view/active-panel])]
    [error-boundary
     [show-panel active-panel]]))
