;; fe/routes.cljs -- Routing for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.routes
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.content.comment :as comment]
   [ovarit.app.fe.content.donate :as donate]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.modmail :as-alias modmail]
   [ovarit.app.fe.content.moderation :as-alias moderation]
   [ovarit.app.fe.content.post :as post]
   [ovarit.app.fe.content.posts :as posts]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.content.spec]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes.auth :as-alias auth-routes]
   [ovarit.app.fe.routes.moderation :as-alias mod-routes]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.tr :refer [trx]]
   [ovarit.app.fe.ui.forms.compose-message :as compose-message]
   [ovarit.app.fe.ui.forms.donate :as donate-form]
   [ovarit.app.fe.ui.forms.login :as login-form]
   [ovarit.app.fe.ui.forms.sub :as sub-forms]
   [ovarit.app.fe.ui.forms.user :as user-forms]
   [ovarit.app.fe.ui.page :as page]
   [ovarit.app.fe.ui.page-title :as page-title]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.util.relic :as rel]
   [pushy.core :as pushy]
   [re-frame.core :as re-frame]
   [re-frame.std-interceptors :refer [fx-handler->interceptor]]
   [weavejester.dependency :as dep])
  (:require
   [ovarit.app.fe.routes.spec.route :as-alias route]
   [ovarit.app.fe.routes.spec.show-admin-require-name-change
    :as-alias show-admin-require-name-change]
   [ovarit.app.fe.routes.spec.show-admin-voting :as-alias show-admin-voting]))

;; Spec the :route key in the db.
(spec/def ::route/event keyword?)
(spec/def ::route/params map?)
(spec/def ::route/parsed map?)
(spec/def ::route/url string?)
(spec/def ::route
  (spec/keys :opt-un [::route/event
                      ::route/url
                      ::route/parsed]))

;; Spec the :view key in the db.
(defmulti view-type :active-panel)
(spec/def ::view
  (spec/multi-spec view-type :active-panel))

(defmethod view-type :initializing [_]
  (spec/keys :req-un [::active-panel]))

(def handler-map
  "Map the keywords in the routing table to the events to dispatch."
  ;; First item in the returned vector is an event; the second element
  ;; is a map containing parameters which will be merged into the
  ;; route parameters from the URL and passed to the event.
  ;;
  ;; If a keyword maps to another keyword, look up the vector
  ;; associated with that keyword instead.
  {:admin/banned-user-names        [::show-admin-banned-user-names]
   :admin/require-name-change      [::show-admin-require-name-change]
   :admin/invitecodes              [::show-admin-invite-codes]
   :admin/stats                    [::show-admin-stats]
   :admin/voting                   [::show-admin-voting]
   :auth/delete-account            [::show-delete-account]
   :auth/login                     [::show-login]
   :auth/register                  [::show-registration]
   :auth/confirm-registration      [::show-confirm-registration]
   :auth/verify-registration       [::show-verify-registration]
   :auth/resend-verification-email [::show-resend-verification-email]
   :auth/start-reset               [::show-start-reset-password]
   :auth/complete-reset            [::show-complete-reset-password]
   :auth/edit-account              [::show-edit-account]
   :auth/rename-account            [::show-rename-account]
   :auth/confirm-email             [::show-confirm-email]
   :donate/cancel                  [::show-donate-cancel]
   :donate/funding-progress        [::show-funding-progress]
   :donate/setup                   [::show-donate-setup]
   :donate/success                 [::show-donate-success]
   :mod/admin                      [::show-mod-dashboard {:show-other-circles? true}]
   :mod/index                      [::show-mod-dashboard {:show-other-circles? false}]
   :modmail/compose                [::show-modmail-compose {}]
   :modmail/mailbox                [::show-modmail {}]
   :modmail/thread                 [::show-modmail {}]
   :sub/contact-mods               [::show-contact-mods]
   :sub/edit-post-types            [::show-mod-edit-post-types]
   :sub/edit-sub-flairs            [::show-mod-edit-post-flairs]
   :sub/edit-sub-rules             [::show-mod-edit-sub-rules]
   :sub/view-direct-link           [::show-single-post]
   :sub/view-single-post           [::show-single-post]
   :sub/view-single-post-test      [::show-single-post-test]
   :sub/view-direct-link-test      [::show-single-post-test]
   :subs/submit-post               [::show-submit-post]
   :user/edit-user                 [::show-edit-user-preferences]
   :wiki/show-license              [::show-wiki-license {}]})

(defn lookup-event
  "Look up the event handler in 'handler-map'."
  [handler]
  (when handler
    (loop [h handler]
      (let [map-entry (h handler-map)]
        (if (keyword? map-entry)
          (recur map-entry)
          map-entry)))))

(defn- url-event
  "Parse a URL and construct the event to dispatch for it."
  [routes url]
  (try
    (let [parsed (route-util/parse-url routes url)
          {:keys [handler route-params url-params]} parsed
          [event event-params] (lookup-event handler)
          result {:event event
                  :params (-> url-params
                              (merge route-params event-params))
                  :url url
                  :parsed parsed}]
      (cond
        event          result
        (nil? handler) {:event ::show-error
                        :params :unmatched-route
                        :parsed parsed
                        :url url}
        handler        (log/warn {:url url :handler handler}
                                 "Route not implemented by ovarit-app")))
    (catch js/Error e
      (log/error {:exception e :url url} "url-event matching-error"))))

(defn- dispatch-route [{:keys [event params] :as route}]
  (log/info {:event event} "dispatch-route")
  (re-frame/dispatch [::errors/clear-errors])
  (re-frame/dispatch [::window/close-menu])
  (re-frame/dispatch [::window/set-force-day-mode false])
  (re-frame/dispatch [::settings/init-subscriptions
                      (= ::show-funding-progress event)])
  (re-frame/dispatch [::settings/stop-subscriptions-on-routing])
  (re-frame/dispatch [::dispatch-on-leave-events])
  (re-frame/dispatch [::loader/cleanup])
  (re-frame/dispatch [::initialize-routing route])
  (re-frame/dispatch [event params])
  (re-frame/dispatch [::settings/choose-new-motto]))

(re-frame/reg-event-db ::initialize-routing
  ;; :doc Put routing into the initializing state.
  (fn-traced [db [_ route]]
    (-> db
        (update :epoch inc)
        (assoc :route route)
        (assoc-in [:status :routing] :initializing))))

(re-frame/reg-sub ::route
  (fn [db _]
    (:route db)))

(re-frame/reg-sub ::initializing?
  :<- [::window/statuses]
  (fn [statuses _]
    (and (:routing statuses) (not= (:routing statuses) :complete))))

(re-frame/reg-sub ::url
  :<- [::route]
  (fn [route _]
    (:url route)))

(re-frame/reg-event-fx ::dispatch-on-leave-events
  (fn-traced [{:keys [db]} [_ _]]
    {:db (update db :view dissoc :on-leave)
     :fx (map (fn [ev]
                [:dispatch ev]) (get-in db [:view :on-leave]))}))

(def history
  "Application pushState instance."
  (atom nil))

(defn app-routes [routes]
  (reset! history
          (pushy/pushy dispatch-route (partial url-event routes)))
  (pushy/start! @history))

(re-frame/reg-fx ::redirect
  ;; :doc Do a redirect.
  ;; :doc Switching to another ovarit-app page without reloading
  ;; :doc the index query can be done with ::set-url.
  (fn-traced [value]
    (log/debug {:value value} "redirecting")
    (set! (.. js/window -location -href) value)))

(re-frame/reg-fx ::set-url
  ;; :doc Change the page, and add it to the browser history.
  ;; :doc Only use this for ovarit-app pages.
  (fn-traced [url]
    (log/debug {:url url} "set-url switching location")
    (pushy/set-token! @history url)))

(re-frame/reg-fx ::replace-url
  ;; :doc Change the page, and don't add it to the browser history.
  ;; :doc Only use this for ovarit-app pages.
  (fn-traced [url]
    (log/debug {:url url} "replace-url updating location")
    (pushy/replace-token! @history url)))

(def show-error-fx-handler
  "Show the error page when the route could not be matched."
  (fn-traced [{:keys [db]} [_ error]]
    {:db (assoc db :view (-> (:view db)
                             (assoc :active-panel :error-page)
                             (assoc :options {:error error})))
     :fx [[:dispatch [::page-title/update
                      (case error
                        :unauthorized (trx db "Not authorized")
                        :unmatched-route (trx db "Not found")
                        :server-error (trx db "Something went wrong")
                        :network-error (trx db "Unable to contact server"))]]
          [:dispatch [::page/stop-robots]]]}))

(re-frame/reg-event-fx ::show-error
  ;; :doc Show the error page when the route could not be matched.
  [route-util/completes-routing]
  show-error-fx-handler)

;; Spec the view options for the error page.
(spec/def :routes.error-page/error #{:network-error
                                     :server-error
                                     :unauthorized
                                     :unmatched-route})
(spec/def :routes.error-page/options
  (spec/keys :req-un [:routes.error-page/error]))
(defmethod view-type :error-page [_]
  (spec/keys :req-un [::active-panel
                      :routes.error-page/options]))

(def mods-and-admins-only
  "If the user is not a mod or admin, swap in the error handler."
  (let [show-error (fx-handler->interceptor show-error-fx-handler)
        replace-elem (fn [elem]
                       (if (= (:id elem) :fx-handler)
                         show-error
                         elem))
        replace-handler (fn [q]
                          (->> q
                               (into [])
                               (map replace-elem)
                               (into #queue [])))]
    (re-frame/->interceptor
     :id ::mods-and-admins-only
     :before (fn [{:keys [coeffects] :as ctx}]
               (let [db (:db coeffects)]
                 (if (user/is-a-mod-or-admin? db)
                   ctx
                   (-> ctx
                       (update :queue replace-handler)
                       (assoc-in [:coeffects :event 1] :unauthorized))))))))

(re-frame/reg-event-fx ::show-post-listing
  ;; :doc Show a post listing page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ options]]
    {:db (assoc db :view (-> (:view db)
                             (assoc :active-panel :post-listing)
                             (assoc :options options)))
     :fx [[:dispatch [::posts/load-posts options]]
          [:dispatch [::page-title/update nil]]]}))

;; Spec the view options for the post listing page.
(spec/def :routes.post-listing/post-source #{:all :subscriptions})
(spec/def :routes.post-listing/sort #{:hot :top :new})
(spec/def :routes.post-listing/options
  (spec/keys :req-un [:routes.post-listing/post-source
                      :routes.post-listing/sort]))
(defmethod view-type :post-listing [_]
  (spec/keys :req-un [::active-panel
                      :routes.post-listing/options]))

(re-frame/reg-event-fx ::show-wiki-license
  ;; :doc Show the software license page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ _]]
    (merge
     {:db (assoc db :view (-> (:view db)
                              (assoc :active-panel :wiki-page)
                              (assoc :options {:page :license})))
      :fx [[:dispatch [::settings/load-software-licenses]]
           [:dispatch [::page-title/update (trx db "Licenses")]]]})))

;; Spec the view options for wiki pages.
(spec/def :routes.wiki-page/page #{:license})
(spec/def :routes.wiki-page/options
  (spec/keys :req-un [:routes.wiki-page/page]))
(defmethod view-type :wiki-page [_]
  (spec/keys :req-un [::active-panel
                      :routes.wiki-page/options]))

(re-frame/reg-event-fx ::show-donate-setup
  ;; :doc Show the donate setup page.
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} [_ _]]
    {:db (-> db
             (assoc :view {:active-panel :donate-setup :options nil})
             donate-form/initialize-donate-form
             (assoc-in [:status :content] (if authenticated?
                                            :loading
                                            :loaded))
             (login-form/initialize-login-form
              (route-util/url-for db :donate/setup)))
     :fx [(when authenticated?
            [:dispatch [::donate/load-subscription]])
          (when authenticated?
            [:dispatch [::donate/subscribe-funding-progress]])]}))

;; Spec the view options for the donate page.
(defmethod view-type :donate-setup [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-donate-success
  ;; :doc Show the donate acknowledgement page.
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [session_id]}]]
    (log/debug {:session_id session_id} "show-donate-success")
    {:db (-> db
             (assoc :view {:active-panel :donate-success
                           :options nil})
             (assoc-in [:content :donate-session] nil))
     :fx [(when session_id
            [:dispatch [::donate/load-session session_id]])]}))

;; Spec the view options for the donate acknowledgement page.
(defmethod view-type :donate-success [_]
  (spec/keys :req-un [::active-panel]))

;; TODO this needs to get cancel info from the server.
(re-frame/reg-event-db ::show-donate-cancel
  ;; :doc Show the donate cancel page.
  [route-util/completes-routing]
  (fn-traced [db [_ _]]
    (let [{:keys [amount recurring?]} (get-in db [:content :donation-intent])]
      (assoc db :view {:active-panel :donate-cancel
                       :options {:recurring? recurring?
                                 :amount amount}}))))

(defmethod view-type :donate-cancel [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-funding-progress
  ;; :doc Show the funding goal and progress (intended for an iframe).
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ _]]
    {:db (assoc db :view {:active-panel :donate-funding-progress})
     :fx [[:dispatch [::settings/subscribe
                      [::donate/subscribe-funding-progress]]]
          [:window/on-message {:id :listen-day-night
                               :dispatch window/listen-day-night}]]}))

(defmethod view-type :donate-funding-progress [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-mod-dashboard
  [mods-and-admins-only route-util/completes-routing]
  ;; :doc Show the moderator dashboard.
  (fn-traced [{:keys [db]} [_ options]]
    {:db (update db :view assoc
                 :active-panel :mod-dashboard
                 :options options)
     :fx [(if (:show-other-circles? options)
            [:dispatch [::subs/load-all-subs
                        [::moderation/load-admin-mod-stats]]]
            [:dispatch [::moderation/load-current-user-mod-stats]])
          [:dispatch [::page-title/update (trx db "Dashboard")]]]}))

;; Spec the view options for the moderator dashboard.
(spec/def :routes.mod-dashboard/show-other-circles? boolean?)
(spec/def :routes.mod-dashboard/options
  (spec/keys :req-un [:routes.mod-dashboard/show-other-circles?]))
(defmethod view-type :mod-dashboard [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-dashboard/options]))

(re-frame/reg-event-fx ::show-modmail
  ;; :doc Show one of the modmail views.
  [mods-and-admins-only
   route-util/completes-routing
   route-util/hide-markdown-editor-on-leave]
  (fn-traced [{:keys [db]} [_ {:keys [msg mailbox sub]}]]
    (let [mbox (keyword mailbox)
          options {:mailbox mbox
                   :sub (or sub :all-subs)
                   :thread-id msg}
          load-messages (if msg
                          [::modmail/load-modmails options
                           [::initialize-modmail-reply-form]]
                          [::modmail/load-modmail-category options])]
      (if (#{:all :archived :discussions :in-progress :new
             :notifications} mbox)
        {:db (update db :view merge {:active-panel :modmail
                                     :options options})
         ;; If we're asked to route on sub name, load the list of subs
         ;; first, if we don't have it yet.
         :fx [(if (and (not= (:sub options) :all-subs)
                       (= :not-loaded (get-in db [:status :subs])))
                [:dispatch [::subs/load-all-subs load-messages]]
                [:dispatch load-messages])
              [:dispatch [::page-title/update (trx db "Moderator Mail")]]]}
        {:fx [[:dispatch [::show-error :unmatched-route]]]}))))

;; Spec the view options for the modmail views.
(spec/def :routes.modmail/mailbox #{:all :archived :discussions :in-progress
                                    :new :notifications})
(spec/def :routes.modmail/sub (spec/or :name string? :all #{:all-subs}))
(spec/def :routes.modmail/msg (spec/nilable string?))
(spec/def :routes.modmail/options
  (spec/keys :req-un [:routes.modmail/mailbox
                      :routes.modmail/sub]
             :opt-un [:routes.modmail/msg]))
(defmethod view-type :modmail [_]
  (spec/keys :req-un [::active-panel
                      :routes.modmail/options]))

(re-frame/reg-event-db ::initialize-modmail-reply-form
  ;; :doc After a modmail thread is loaded, set up the reply form.
  (fn-traced [db _]
    (compose-message/initialize-modmail-reply-form db)))

(re-frame/reg-event-db ::show-modmail-compose
  ;; :doc Show the form for mods to compose a new moderator mail.
  [mods-and-admins-only
   route-util/completes-routing
   route-util/hide-markdown-editor-on-leave]
  (fn-traced [db [_ url-params]]
    (let [options (-> (select-keys url-params [:sub :report_type :report_id
                                               :user :subject])
                      util/kebab-case-keys)]
      (-> db
          (update :view assoc
                  :active-panel :modmail-compose
                  :options options)
          compose-message/initialize-compose-modmail-form))))

;; Spec the view options for the moderator mail compose form.
(defmethod view-type :modmail-compose [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-contact-mods
  ;; :doc Show the contact mods page.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [route-util/completes-routing route-util/hide-markdown-editor-on-leave]
  (fn-traced [{:keys [db]} [_ {:keys [sub] :as options}]]
    {:db (-> db
             (assoc :view {:active-panel :contact-mods
                           :options options})
             (compose-message/initialize-contact-mods-form sub))
     :fx [(when (= :not-loaded (get-in db [:status :subs]))
            [:dispatch [::subs/load-all-subs]])]}))

;; Spec the view options for the contact mods page.
(spec/def :routes.contact-mods/sub string?)
(spec/def :routes.contact-mods/options
  (spec/keys :req-un [:routes.contact-mods/sub]))
(defmethod view-type :contact-mods [_]
  (spec/keys :req-un [::active-panel
                      :routes.contact-mods/options]))

(re-frame/reg-event-fx ::show-mod-edit-sub-rules
  ;; :doc Show the edit sub rules panel.
  [mods-and-admins-only]
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::mod-routes/mod-edit-sub-rules options]]]}))

;; Spec the view options for the edit sub rules page.
(spec/def :routes.mod-edit-sub-rules/sub string?)
(spec/def :routes.mod-edit-sub-rules/options
  (spec/keys :req-un [:routes.mod-edit-sub-rules/sub]))
(defmethod view-type :mod-edit-sub-rules [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-sub-rules/options]))

(re-frame/reg-event-fx ::show-mod-edit-post-flairs
  ;; :doc Show the edit sub flairs panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [mods-and-admins-only]
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::mod-routes/mod-edit-post-flairs options]]]}))

;; Spec the view options for the edit sub flairs page.
(spec/def :routes.mod-edit-post-flairs/sub string?)
(spec/def :routes.mod-edit-post-flairs/options
  (spec/keys :req-un [:routes.mod-edit-post-flairs/sub]))
(defmethod view-type :mod-edit-post-flairs [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-post-flairs/options]))

(re-frame/reg-event-fx ::show-mod-edit-post-types
  ;; :doc Show the mod edit post types panel.
  ;; :doc Since we need to know if the sub name in the URL is valid or
  ;; :doc not, load the list of subs if that hasn't been done yet.
  [mods-and-admins-only route-util/hide-markdown-editor-on-leave]
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::mod-routes/mod-edit-post-types options]]]}))

;; Spec the view options for the edit sub post types page.
(spec/def :routes.mod-edit-post-types/sub string?)
(spec/def :routes.mod-edit-post-types/options
  (spec/keys :req-un [:routes.mod-edit-post-types/sub]))
(defmethod view-type :mod-edit-post-types [_]
  (spec/keys :req-un [::active-panel
                      :routes.mod-edit-post-types/options]))

(defn- cid-from-fragment
  "Parse the cid from the single post page url fragment, if there is one."
  [fragment]
  (let [prefix "comment-"]
    (when (and fragment
               (str/starts-with? fragment prefix)
               (> (count fragment) (count prefix)))
      (subs fragment (count prefix)))))

(re-frame/reg-event-fx ::show-single-post
  ;; :doc Show the single post panel.
  [route-util/completes-routing route-util/hide-markdown-editor-on-leave]
  (fn-traced [{:keys [db]} [_ {:keys [cid pid slug sort sub]}]]
    (let [fragment (get-in db [:route :parsed :fragment])
          target-cid (cid-from-fragment fragment)]
      {:db (post/init-single-post db {:sub sub :pid pid :cid cid
                                      :target-cid target-cid
                                      :slug slug :sort sort})
       :fx [[:dispatch [::post/load-single-post {:sub sub
                                                 :pid pid
                                                 :cid cid}]]
            [:dispatch [::user/load-content-blocks]]
            [:dispatch [::comment/init-comment-compose-form :reply nil]]
            [:dispatch [::comment/watch-scroll-and-resize]]
            [:dispatch [::donate/subscribe-funding-progress]]
            [:dispatch [::post/subscribe-post pid]]]})))

(re-frame/reg-event-fx ::show-single-post-test
  ;; :doc Redirect from the test URL to the normal URL
  [route-util/completes-routing]
  (fn-traced [{:keys [db]} [_ {:keys [cid pid slug sub]}]]
    {:fx [[::replace-url (post/post-url db {:sub sub
                                            :pid pid
                                            :slug slug
                                            :cid cid})]]}))

;; Spec the view options for the view single post panel.
(spec/def :routes.single-post/sub string?)
(spec/def :routes.single-post/pid string?)
(spec/def :routes.single-post/slug (spec/nilable string?))
(spec/def :routes.single-post/cid (spec/nilable string?))
(spec/def :routes.single-post/scroll-to-cid (spec/nilable string?))
(spec/def :routes.single-post/sort-choice (spec/nilable string?))
(spec/def :routes.single-post/sort #{:BEST :NEW :TOP})
(spec/def :routes.single-post/options
  (spec/keys :req-un [:routes.single-post/sub
                      :routes.single-post/pid
                      :routes.single-post/slug
                      :routes.single-post/cid
                      :routes.single-post/scroll-to-cid
                      :routes.single-post/sort-choice]
             :opt-un [:routes.single-post/sort]))
(defmethod view-type :single-post [_]
  (spec/keys :req-un [::active-panel
                      :routes.single-post/options]))

(def submit-post-deps
  "Initialization events for the submit post panel."
  (-> (dep/graph)
      (dep/depend
       [::subs/check-sub] [::subs/load-sub-names])
      (dep/depend
       [::subs/load-info] [::subs/check-sub])
      (dep/depend
       [::subs/load-post-types] [::subs/check-sub])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-submit-post-settings])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-info])
      (dep/depend
       [::sub-forms/initialize-submit-post] [::subs/load-post-types])))

(re-frame/reg-event-fx ::show-submit-post
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} [_ {:keys [sub]}]]
    (let [nav? (not= :submit-post (get-in db [:view :active-panel]))]
      {:db (-> db
               (assoc :view {:active-panel :submit-post
                             :options {:sub sub}})
               ;; Changing the sub field in the form changes the URL and
               ;; lands us back here. So only clear the old form if we're
               ;; coming from a different page.
               (cond-> nav?
                 (update :reldb rel/transact
                         [:delete :FormState
                          [= [:_ ::sub-forms/submit-post] :id]])))
       :fx [(when authenticated?
              [:dispatch [::loader/init-content-loader
                          {:graph submit-post-deps}]])
            (when authenticated?
              [:dispatch [::page-title/update (trx db "New post")]])]})))

;; Spec the view options for the submit post panel.
(spec/def :routes.submit-post/sub (spec/nilable string?))
(spec/def :routes.submit-post/options
  (spec/keys :req-un [:routes.submit-post/sub]))
(defmethod view-type :submit-post [_]
  (spec/keys :req-un [::active-panel
                      :routes.submit-post/options]))

(re-frame/reg-event-fx ::show-admin-stats
  ;; :doc Show the admin stats page.
  [mods-and-admins-only]
  (fn-traced [_ [_ _]]
    {:fx [[:dispatch [::mod-routes/admin-stats]]]}))

;; Spec the view options for the admin stats page.
(defmethod view-type :admin-stats [_]
  (spec/keys :req-un [::active-panel]))

(re-frame/reg-event-fx ::show-admin-voting
  ;; :doc Show the admin view user votes page.
  [mods-and-admins-only]
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::mod-routes/admin-voting options]]]}))

;; Spec the view options for the admin voting page.
(spec/def ::show-admin-voting/name string?)
(spec/def ::show-admin-voting/types #{:post :comment :all})
(spec/def ::show-admin-voting/first (spec/nilable pos-int?))
(spec/def ::show-admin-voting/last (spec/nilable pos-int?))
(spec/def ::show-admin-voting/before (spec/nilable string?))
(spec/def ::show-admin-voting/after (spec/nilable string?))
(spec/def ::show-admin-voting/options
  (spec/keys :req-un [::show-admin-voting/name
                      ::show-admin-voting/first
                      ::show-admin-voting/last
                      ::show-admin-voting/before
                      ::show-admin-voting/after
                      ::show-admin-voting/types]))

(defmethod view-type :admin-voting [_]
  (spec/keys :req-un [::active-panel
                      ::show-admin-voting/options]))

(re-frame/reg-event-fx ::show-admin-banned-user-names
  ;; :doc Show the admin banned user names page.
  [mods-and-admins-only]
  (fn-traced [_ _]
    {:fx [[:dispatch [::mod-routes/admin-banned-user-names]]]}))

;; Spec the view options for the admin banned user names page.
(spec/def :routes.admin-banned-user-names/sort-asc? boolean?)
(spec/def :routes.admin-banned-user-names/sort-typ #{:string :count})
(spec/def :routes.admin-banned-user-names/options
  (spec/keys :req-un [:routes.admin-banned-user-names/sort-asc?
                      :routes.admin-banned-user-names/sort-typ]))
(defmethod view-type :admin-banned-user-names [_]
  (spec/keys :req-un [::active-panel
                      :routes.admin-banned-user-names/options]))

(re-frame/reg-event-fx ::show-admin-require-name-change
  ;; :doc Show the admin view user votes page.
  [mods-and-admins-only route-util/hide-markdown-editor-on-leave]
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::mod-routes/admin-require-name-change options]]]}))

;; Spec the view options for the admin require name change page.
(spec/def ::show-admin-require-name-change/name string?)
(spec/def ::show-admin-require-name-change/options
  (spec/keys :req-un [::show-admin-require-name-change/name]))

(defmethod view-type :admin-require-name-change [_]
  (spec/keys :req-un [::active-panel
                      ::show-admin-require-name-change/options]))

(re-frame/reg-event-fx ::show-admin-invite-codes
  ;; :doc Show the admin invite codes page.
  [mods-and-admins-only]
  (fn-traced [_ _]
    {:fx [[:dispatch [::mod-routes/admin-invite-codes]]]}))

;; Spec the view options for the admin invite codes page.
(spec/def :routes.admin-invite-codes/search (spec/or :name string?
                                                     :all #{:all}))
(spec/def :routes.admin-invite-codes/options
  (spec/keys :req-un [:routes.admin-invite-codes/search]))
(defmethod view-type :admin-invite-codes [_]
  (spec/keys :req-un [::active-panel
                      :routes.admin-invite-codes/options]))

(re-frame/reg-event-fx ::load-auth-module
  (fn-traced [_ [_ route]]
    (let [init #((resolve 'ovarit.app.fe.routes.auth/init-auth-route) route)]
      {:fx [(if config/testing?
              [:dispatch route]
              [:dispatch [::util/load-module-and-run-fn :auth init]])]})))

;; TODO if they manage to navigate here when logged in,
;; redirect to the home page.
(re-frame/reg-event-fx ::show-registration
  ;; :doc Show the registration form.
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module [::auth-routes/registration]]]]}))

;; Spec the view options for the registration page.
(spec/def :routes.registration/options #(= % {}))
(defmethod view-type :registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.registration/options]))

(re-frame/reg-event-fx ::show-verify-registration
  ;; :doc Show a acknowledgement of a new registration.
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/verify-registration options]]]]}))

;; Spec the view options for the verify registration page.
(spec/def :routes.verify-registration.options/delete-code
  string?)
(spec/def :routes.verify-registration.options/username
  (spec/nilable string?))
(spec/def :routes.verify-registration.options/email
  (spec/nilable string?))
(spec/def :routes.verify-registration.options/already-verified?
  boolean?)
(spec/def :routes.verify-registration/options
  (spec/keys :req-un [:routes.verify-registration.options/delete-code
                      :routes.verify-registration.options/username
                      :routes.verify-registration.options/email
                      :routes.verify-registration.options/already-verified?]))
(defmethod view-type :verify-registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.verify-registration/options]))

(re-frame/reg-event-fx ::show-confirm-registration
  ;; :doc Confirm the user's registration, and show them the login form.
  ;; :doc Or if confirming their registration fails, show them an
  ;; :doc error page.
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/confirm-registration options]]]]}))

;; Spec the view options for the confirm registration page.
(spec/def :routes.confirm-registration.options/code string?)
(spec/def :routes.confirm-registration.options/state
  #{:loading :xmit-error :invalid-link})
(spec/def :routes.confirm-registration/options
  (spec/keys :req-un [:routes.confirm-registration.options/code
                      :routes.confirm-registration.options/state]))
(defmethod view-type :confirm-registration [_]
  (spec/keys :req-un [::active-panel
                      :routes.confirm-registration/options]))

(re-frame/reg-event-fx ::show-login
  ;; :doc Show the login page.
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::load-auth-module [::auth-routes/login options]]]]}))

;; Spec the view options for the login page.
(spec/def :routes.login.options/notify (spec/nilable keyword?))
(spec/def :routes.login/options
  (spec/keys :req-un [:routes.login.options/notify]))
(defmethod view-type :login [_]
  (spec/keys :req-un [::active-panel
                      :routes.login/options]))

(re-frame/reg-event-fx ::show-resend-verification-email
  ;; :doc Show the page for requesting a resent email.
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/resend-verification-email]]]]}))

;; Spec the view options for the resend verification email page.
(spec/def :routes.resend-verification-email/options #(= % {}))
(defmethod view-type :resend-verification-email [_]
  (spec/keys :req-un [::active-panel
                      :routes.resend-verification-email/options]))

(re-frame/reg-event-fx ::show-start-reset-password
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/start-reset-password]]]]}))

;; Spec the view options for the start reset password page.
(spec/def :routes.start-reset-password/options #(= % {}))
(defmethod view-type :start-reset-password [_]
  (spec/keys :req-un [::active-panel
                      :routes.start-reset-password/options]))

(re-frame/reg-event-fx ::show-complete-reset-password
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/complete-reset-password options]]]]}))

;; Spec the view options for the complete reset password page.
(spec/def :routes.complete-reset-password.options/username
  (spec/nilable string?))
(spec/def :routes.complete-reset-password.options/code
  (spec/nilable string?))
(spec/def :routes.complete-reset-password.options/bad-code? boolean?)
(spec/def :routes.complete-reset-password/options
  (spec/keys :req-un [:routes.complete-reset-password.options/username
                      :routes.complete-reset-password.options/code
                      :routes.complete-reset-password.options/bad-code?]))
(defmethod view-type :complete-reset-password [_]
  (spec/keys :req-un [::active-panel
                      :routes.complete-reset-password/options]))

(re-frame/reg-event-fx ::show-edit-account
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module [::auth-routes/edit-account]]]]}))

;; Spec the view options for the edit account page.
(spec/def :routes.edit-account/options map?)
(defmethod view-type :edit-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.edit-account/options]))

(def user-preferences-deps
  "Initialization events for the user preferences page."
  (-> (dep/graph)
      (dep/depend
       [::user-forms/initialize-preferences-form]
       [::user/load-preference-settings])))

(re-frame/reg-event-fx ::show-edit-user-preferences
  [user/authenticated? route-util/completes-routing]
  (fn-traced [{:keys [db authenticated?]} [_ _]]
    {:db (-> db
             (assoc-in [:view :active-panel] :edit-user-preferences)
             (assoc-in [:view :options] {}))
     :fx [(when authenticated?
            [:dispatch [::loader/init-content-loader
                        {:graph user-preferences-deps}]])
          (when authenticated?
            [:dispatch [::page-title/update (trx db "Edit preferences")]])]}))

;; Spec the view options for the edit user preferences page.
(spec/def :routes.edit-user-preferences/options map?)
(defmethod view-type :edit-user-preferences [_]
  (spec/keys :req-un [::active-panel
                      :routes.edit-user-preferences/options]))

(re-frame/reg-event-fx ::show-rename-account
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module [::auth-routes/rename-account]]]]}))

;; Spec the view options for the rename account page.
(spec/def :routes.rename-account/options map?)
(defmethod view-type :rename-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.rename-account/options]))

(re-frame/reg-event-fx ::show-confirm-email
  ;; :doc Confirm a user's email and show them an acknowledgement page.
  (fn-traced [_ [_ options]]
    {:fx [[:dispatch [::load-auth-module
                      [::auth-routes/confirm-email options]]]]}))

;; Spec the view options for the confirm email page.
(spec/def :routes.confirm-email.options/code string?)
(spec/def :routes.confirm-email.options/state
  #{:loading :confirmed :xmit-error :invalid-link})
(spec/def :routes.confirm-email/options
  (spec/keys :req-un [:routes.confirm-email.options/code
                      :routes.confirm-email.options/state]))
(defmethod view-type :confirm-email [_]
  (spec/keys :req-un [::active-panel
                      :routes.confirm-email/options]))

(re-frame/reg-event-fx ::show-delete-account
  ;;:doc Show the panel for deleting a user account.
  (fn-traced [_ _]
    {:fx [[:dispatch [::load-auth-module [::auth-routes/delete-account]]]]}))

;; Spec the view options for the delete account page..
(spec/def :routes.delete-account/options map?)
(defmethod view-type :delete-account [_]
  (spec/keys :req-un [::active-panel
                      :routes.delete-account/options]))

;; Extractors.  No computation, just data access.

(re-frame/reg-sub :view/active-panel
  (fn [db _]
    (get-in db [:view :active-panel])))

(re-frame/reg-sub :view/post-source
  (fn [db _]
    (get-in db [:view :options :post-source])))

(re-frame/reg-sub :view/sort
  (fn [db _]
    (get-in db [:view :options :sort])))

(re-frame/reg-sub :view/mailbox
  (fn [db _]
    (get-in db [:view :options :mailbox])))

(re-frame/reg-sub :view/mid
  (fn [db _]
    (get-in db [:view :options :mid])))

(re-frame/reg-sub :view/options
  (fn [db _]
    (get-in db [:view :options])))

;; Computational subscriptions.  Each should depend on an extractor,
;; not the entire db.

(re-frame/reg-sub :view/post-source-sub
  :<- [:view/post-source]
  (fn [post-source _]
    (and (not (keyword? post-source)) post-source)))
