;; fe/user/spec.cljs -- Current user specs for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.user.spec
  (:require
   [clojure.spec.alpha :as spec]
   [ovarit.app.fe.content.graphql-spec]))

(spec/def :current-user.attributes/is-admin boolean?)
(spec/def :current-user.attributes/can-admin
  :type.UserAttributes/can_admin)
(spec/def :current-user.attributes/totp-expiration (spec/nilable int?))
(spec/def :current-user.attributes/enable-collapse-bar
  :type.UserAttributes/enable_collapse_bar)
(spec/def :current-user.attributes/nsfw-blur
  :type.UserAttributes/nsfw_blur)
(spec/def :current-user/attributes
  (spec/keys :req-un [:current-user.attributes/can-admin
                      :current-user.attributes/totp-expiration
                      :current-user.attributes/enable-collapse-bar
                      :current-user.attributes/is-admin
                      :type.UserAttributes/nsfw
                      :current-user.attributes/nsfw-blur]))
(spec/def :current-user.subscriptions/status #{:SUBSCRIBED :BLOCKED})
(spec/def :current-user/subscriptions
  (spec/*
   (spec/keys :req-un [:type.Subscription/name
                       :current-user.subscriptions/status]
              :opt-un [:type.Subscription/order])))
(spec/def :current-user/unread-message-count
  :type.User/unread_message_count)
(spec/def :current-user/unread-notification-count
  :type.User/unread_notification_count)
(spec/def :current-user/content-block #{:HIDE :BLUR})
(spec/def :current-user/content-blocks
  (spec/map-of :type.User/uid :current-user/content-block))
(spec/def :current-user/language (spec/nilable string?))
(spec/def ::current-user
  (spec/nilable
   (spec/keys :req-un [:current-user/attributes
                       :current-user/language
                       :type.User/name
                       :type.User/score
                       :current-user/subscriptions
                       :type.User/uid
                       :current-user/unread-message-count
                       :current-user/unread-notification-count]
              :opt-un [:current-user/content-blocks])))
