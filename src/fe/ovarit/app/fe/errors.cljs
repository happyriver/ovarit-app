;; fe/errors.cljs -- Error processing for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.errors
  (:require
   [clojure.set :refer [union]]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.content.settings :as-alias settings]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.tr :as tr :refer [tr trx]]
   [ovarit.app.fe.ui.window :as-alias window]
   [re-frame.core :as re-frame]))

(defn describe-error
  "Return a user-facing message and status code for a GraphQL error."
  [description {:keys [extensions message]}]
  (let [status (:status extensions)]
    (cond
      (or (= status 500) (= message "Internal Server Error"))
      {:msg (tr "Server error")
       :status status}

      (= status 502)
      {:msg (tr "Could not contact server")
       :status status}

      (= status 503)
      {:msg (tr "Server busy")
       :status status}

      (= status 429)
      {:msg (str description " " (tr "too many attempts, please slow down"))
       :status status}

      (and config/debug? (nil? status))
      {:msg (str description " " (tr "query error"))
       :status 400}

      config/debug?
      {:msg (str description " " (tr "something went wrong") " " status)
       :status status})))

(defn describe-errors
  "Translate errors from a GraphQL query into messages to show the user.
  Return a set of maps containing a message, a status code and a timestamp."
  [errors event describe-error-fn]
  (let [timestamp (js/Date.)
        processed (->> errors
                       (map describe-error-fn)
                       (remove nil?)
                       (map #(assoc % :timestamp timestamp :event event)))]
    (when (and config/debug? (seq processed))
      (log/error {:count (count processed) :set (set processed) :event event}
                 "describe-errors"))
    (set processed)))

(defn- non-network-error?
  [err]
  (or (not (map? err))
      (not= (:message err) "Service Unavailable")
      (not= (:message err) "The HTTP call failed.")
      (nil? (#{429 502 503} (get-in err [:extensions :status])))))

(defn log-errors
  "Notify the server of errors not related to network transmission."
  [event errors]
  (when (or (and config/debug? (seq errors))
            (seq (filter non-network-error? errors)))
    (log/error {:event event :errors errors} "processing errors")))

(defn assoc-errors
  "Process errors from GraphQL and attach them to the db."
  [db {:keys [event              ;; The dispatched event.
              errors             ;; Errors from re-graph.
              describe-error-fn] ;; Function to extract messages.
       :or {describe-error-fn (partial describe-error "Error:")}
       :as error-event}]
  (let [recent-errors (get-in db [:content :recent-errors])
        new-errors (describe-errors errors event describe-error-fn)]
    (log-errors event errors)
    (if (seq errors)
      (-> db
          ;; Save the unprocessed error map for the developer to look at.
          (assoc :errors (when config/debug?
                           (cons (-> error-event
                                     (dissoc :describe-error-fn)
                                     (assoc :timestamp (js/Date.)))
                                 (:errors db))))
          ;; Put deduplicated and timestamped errors into db content.
          (assoc-in [:content :recent-errors]
                    (union recent-errors new-errors)))
      ;; No errors, so clear recent messages associated with this event.
      (assoc-in db [:content :recent-errors]
                (set (filter #(not= event (:event %)) recent-errors))))))

(defn get-errors
  "Return list of processed errors associated with an event."
  [db event]
  (let [recent-error-list (get-in db [:content :recent-errors])]
    (filter #(= event (:event %)) recent-error-list)))

(defn clear-errors
  "Clear errors associated with an event in the db."
  [db event]
  (assoc-errors db {:errors [] :event event}))

(defn errors-from-ajax-error-response
  "Make an error response from cljs-ajax into a list suitable for
  describe-errors."
  [{:keys [status status-text] :as response}]
  (log/error {:response response} "Ajax error")
  [{:message status-text
    :extensions {:status status}}])

(defn errors-from-do-api
  "Put errors from Python Throat into a list suitable for describe-errors."
  [errors]
  (when errors
    (log/error {:errors errors} "Error from Python /do api")
    (if (coll? errors)
      (map (fn [err] {:message err}) errors)
      [{:message errors}])))

(re-frame/reg-event-db ::clear-errors
  ;; :doc Clear all the recent errors.
  ;; :doc This is used by the routing code to make a clean slate for the new
  ;; :doc page, which means that if there was an error in the index query we
  ;; :doc should leave it.
  (fn [db _]
    (let [index-event ::settings/set-index-query-results
          is-index-query-filter #(= index-event (:event %))
          filter-errors #(filter is-index-query-filter %)]
      (update-in db [:content :recent-errors] #(set (filter-errors %))))))

(re-frame/reg-sub ::processed-errors
  ;; :doc Extract the set of processed errors
  (fn [db _]
    (get-in db [:content :recent-errors])))

(re-frame/reg-sub ::errors-seq
  ;; :doc Return a sequence of error messages, newest first.
  :<- [::processed-errors]
  (fn [errors _]
    (->> errors
         (sort-by :timestamp)
         reverse)))

(re-frame/reg-sub ::errors
  ;; :doc Return the recent errors with formatted times.
  :<- [::errors-seq]
  (fn [errors _]
    (map (fn [{:keys [timestamp] :as err}]
           (assoc err :iso-time (.toISOString timestamp)))
         errors)))

(re-frame/reg-event-db ::clear-error
  ;; :doc Remove an error from the recent errors.
  (fn-traced [db [_ err]]
    (->> (dissoc err :iso-time)
         (update-in db [:content :recent-errors] disj))))

(def error-timeout
  "Milliseconds before errors are removed from display."
  (* 6 60 1000))

(re-frame/reg-event-db ::retry-and-cleanup
  ;; :doc Remove old errors from the list of errors displayed.
  (fn-traced [db [_ _]]
    (let [errors (get-in db [:content :recent-errors])
          new-enough? (fn [{:keys [timestamp]}]
                        (> error-timeout (- (.getTime (js/Date.))
                                            (.getTime timestamp))))
          fresh (set (filter new-enough? errors))]
      (assoc-in db [:content :recent-errors] fresh))))

(re-frame/reg-event-db ::receive-ajax-error
  ;; :doc Handle an error response to an ajax post request.
  ;; :doc `restore` should be a function that acts on the db to undo
  ;; :doc any optimistic updating that was done before the request.
  (fn-traced [db [_ event-kw restore error-response]]
    (-> db
        restore
        (assoc-errors {:event event-kw
                       :errors (errors-from-ajax-error-response
                                error-response)}))))

(defn describe-auth-error
  "Translate ovarit_auth error codes into messages."
  [db {:keys [status status-text response]}]
  (if (and (>= status 400) (< status 500)
           (not= status 429))
    {:msg (or (:reason response)
              (trx db "An error occurred"))
     :status status}
    (describe-error (trx db "Error: ")
                    {:message status-text
                     :extensions {:status status}})))
