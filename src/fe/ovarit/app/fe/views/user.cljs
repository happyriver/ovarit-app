;; fe/views/user.cljs -- User pages for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
(ns ovarit.app.fe.views.user
  (:require
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.user :as user-forms]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.forms :refer [transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button checkbox select]]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.sidebar :refer [current-user-sidebar]]
   [re-frame.core :refer [dispatch subscribe]]))

(defn authed-only
  "Create a panel that is only for admins."
  [panel]
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        loading? @(subscribe [::loader/content-loading?])]
    (cond
      (not is-authenticated?) [misc-panels/unauthenticated-panel]
      loading?                nil
      :else                   panel)))

(defn preferences-form
  []
  (let [form @(subscribe [::forms/form ::user-forms/preferences])
        {:keys [nsfw enable-experimental enable-collapse-bar
                block-dms language language-choices]} form
        form-state @(subscribe [::forms/form-state ::user-forms/preferences])
        can-admin? @(subscribe [::user/can-admin?])]
    [:form.gr-auth-form.pv2.gr-items-start.pa1
     [:label {:for :show-nsfw} (tr "NSFW content:")]
     [:div.gr-justify-start
      [select {:id :show-nsfw
               :value nsfw
               :options [[:hide (tr "Hide from lists")]
                         [:show (tr "Show without blur")]
                         [:blur (tr "Blur until clicked")]]
               :event [::forms/edit ::user-forms/preferences :nsfw]}]]
     [:label {:for :enable-experimental}
      (tr "Enable experimental features:")]
     [checkbox {:id :enable-experimental
                :value enable-experimental
                :event [::forms/edit ::user-forms/preferences
                        :enable-experimental]}]
     [:label {:for :comment-collapse}
      (tr "Enable comment collapse bar:")]
     [checkbox {:id :comment-collapse
                :value enable-collapse-bar
                :event [::forms/edit ::user-forms/preferences
                        :enable-collapse-bar]}]
     (when-not can-admin?
       [:<>
        [:label {:for :block-dms} (tr "Block direct messages:")]
        [:div.flex
         [checkbox {:id :block-dms
                    :value block-dms
                    :event [::forms/edit ::user-forms/preferences :block-dms]}]
         (when block-dms
           [:span.i.gray.ml3
            (tr "Blocking direct messages prevents most other users
                 from sending you messages, and you from sending them
                 messages.  You will still be able to exchange
                 messages with circle moderators and site
                 admins.")])]])

     [:label {:for :language} (tr "Language:")]
     [:div.gr-justify-start
      [select {:id :language
               :value language
               :options language-choices
               :event [::forms/edit ::user-forms/preferences :language]}]]
     [:div.gr-full-width
      [validation-error-if-present ::user-forms/preferences]
      [transmission-error-if-present ::user-forms/preferences]]
     [button {:class :mv1
              :on-click #(do (.preventDefault %)
                             (dispatch
                              [::forms/action ::user-forms/preferences :send]))}
      (case form-state
        :SHOW-SUCCESS (tr "Saved!")
        :SENDING (tr "Saving")
        (tr "Save"))]]))

(defn edit-user-preferences-panel
  []
  (let [user @(subscribe [::user/current-user])]
    [authed-only
     [panel {:sidebar current-user-sidebar}
      [:h2 (tr "%s's preferences" (:name user))]
      [preferences-form]]]))
