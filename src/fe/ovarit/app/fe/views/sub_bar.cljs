;; fe/views/sub-bar.cljs -- Sub-bar element for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.sub-bar
  (:require
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.util :refer [cls url-for]]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(defn- sub-link
  []
  (let [this (reagent/current-component)
        {:keys [ref vertical?] :as props} (reagent/props this)
        props' (dissoc props :ref :vertical?)
        children (reagent/children this)]
    [:li {:ref ref
          :class (cls (if vertical? :db :dib.ml2.mr1))}
     (into [:a.custom props'] children)]))

(defn- edit-subscriptions-icon
  "Create the link to edit subscriptions."
  []
  (let [fill @(subscribe [::window/color ::icon-fill])]
    [:li.dib.mh1
     [:a {:href (url-for :user/edit-subs)
          :title (tr "Customize this bar")}
      [icon {:class (cls :dib.h1.w1.relative.top-015
                         fill)}
       "edit"]]]))

(defn- default-links
  "Create the all/new links, the divider, and the edit defaults link."
  [can-admin? is-authenticated?]
  (let [link-class (cls @(subscribe [::window/color ::link]))
        divider-class (cls @(subscribe [::window/color ::element]))]
    [:<>
     [sub-link {:class link-class
                   :href (url-for :home/all-hot)}
      (tr "all")]
     [sub-link {:class link-class
                   :href (url-for :home/all-new)}
      (tr "new")]
     (when can-admin?
       [sub-link {:class link-class
                     :href (url-for :admin/index)}
        (tr "admin")])
     [:li.dib.mh1 {:class divider-class} "|"]
     (when is-authenticated?
       [edit-subscriptions-icon])]))

(defn- sub-name-link
  "Create one of the links to a sub for the top bar and More menu."
  [{:keys [class vertical? collect-refs]} name]
  [sub-link {:ref #(apply collect-refs [name %])
                :class class
                :vertical? vertical?
                :href (url-for :sub/view-sub :sub name)}
   name])

(defn- sub-name-links
  "Create a list of links to subs, used for the top bar."
  [sub-names collect-refs]
  (let [link-class @(subscribe [::window/color ::link])
        props {:class (cls link-class)
               :collect-refs collect-refs
               :vertical? false}]
    (->> @sub-names
         (map #(sub-name-link props %))
         (into [:<>]))))

(defn- hidden-sub-name-links
  "Create a list of links to subs, used for the More menu."
  [sub-names collect-refs]
  (let [color @(subscribe [::window/color ::moremenu.link])
        props {:class (cls :db.w-100.pa1.f5 color)
               :collect-refs collect-refs
               :vertical? true}]
    (->> @sub-names
         (map #(sub-name-link props %))
         (into [:<>]))))

(defn- moremenu-icon
  "Create the downarrow icon to decorate the More menu toggle."
  []
  (let [color @(subscribe [::window/color ::moremenu.icon-fill])]
    [icon {:class (cls :dib.h1.w1.relative.top-015 color)}
     "down"]))

(defn- modal-moremenu
  "Create the modal menu containing subs that don't fit on the top bar."
  []
  (let [this (reagent/current-component)
        props (reagent/props this)
        children (reagent/children this)
        {:keys [open? cancel]} props
        colors @(subscribe [::window/color ::moremenu.menu])]
    [:div (when open?
            {:class (cls :fixed.absolute--fill.z-max.bg-transparent
                         :small-caps)
             :on-click cancel})
     (into [:ul
            {:class (cls (if open? :gr-moremenu :dn)
                         :absolute.top-1.right-0.fr.ph1.min-width4.mw-100
                         :content-start
                         :ba.bw1.br2.maxh80.lh-solid.shadow-8
                         colors)}]
           children)]))

(defn bar
  "Create the bar with links to all, new and the subs, including the
  overflow More menu."
  [sub-names is-authenticated? can-admin?]
  (let [moremenu-open (reagent/atom false)

        ;; Collect the elements in the component and once all are ready,
        ;; decide what to show and what to hide in the More menu.
        sub-bar-elements (reagent/atom {:moremenu nil
                                        :sub-bar-links (hash-map)
                                        :moremenu-links (hash-map)})

        ;; Callback functions to collect elements as they are ready.
        set-moremenu-ref (fn [ref] (swap! sub-bar-elements assoc :moremenu ref))
        set-ref-fn (fn [key]
                     (fn [name ref]
                       (swap! sub-bar-elements assoc-in [key name] ref)))

        ;; Detect collision between a link on the sub bar and the More menu.
        too-far-right?
        (fn [bar-link right-limit]
          (let [right-side (+ (.-offsetLeft bar-link) (.-clientWidth bar-link))]
            (> right-side right-limit)))

        hide-the-clutter!
        ;; Set the display property of the links so they appear either
        ;; on the sub bar or in the More menu.  Don't show the More menu
        ;; if it is empty.
        (fn [{:keys [moremenu sub-bar-links moremenu-links]}]
          (let [right-limit (.-offsetLeft moremenu)]
            (loop [names @sub-names
                   past-limit? false]
              (if (empty? names)
                (set! (-> moremenu .-style .-visibility)
                      (if past-limit? "visible" "hidden"))
                (let [name (first names)
                      bar-link (get sub-bar-links name)
                      menu-link (get moremenu-links name)
                      hit-limit? (or past-limit?
                                     (too-far-right? bar-link right-limit))]
                  (if hit-limit?
                    (do
                      (set! (-> bar-link .-style .-visibility) "hidden")
                      (set! (-> menu-link .-style .-display) "inline-block"))
                    (do
                      (set! (-> bar-link .-style .-visibility) "visible")
                      (set! (-> menu-link .-style .-display) "none")))
                  (recur (rest names) hit-limit?))))))]

    (reagent/create-class
     {:display-name "sub-bar"

      :component-did-mount
      (fn []
        (hide-the-clutter! @sub-bar-elements))

      :component-did-update
      (fn []
        (hide-the-clutter! @sub-bar-elements))

      :reagent-render
      (fn []
        (let [{:keys [width]} @(subscribe [::window/dimensions])
              colors @(subscribe [::window/color ::bar])
              toggle-color @(subscribe [::window/color ::element])]
          [:<>
           ;; Force a re-render when the window width changes.
           (when-not width [:span])

           [:div.gr-subbar
            {:class (cls :w-100.lh-copy.h2 colors
                         :small-caps.overflow-hidden.bb.bw1)}

            ;; Show the subs that fit in a row.
            [:ul.dib.nowrap.overflow-hidden.pa0.ma0.mt1.f09
             [default-links can-admin? is-authenticated?]
             [sub-name-links sub-names (set-ref-fn :sub-bar-links)]]

            ;; Show the subs that don't fit in a modal menu.
            [:div {:ref set-moremenu-ref}
             [:div
              {:class (cls :tc.pa0.mt1.f09.pointer toggle-color)
               :on-click #(swap! moremenu-open not)}
              (tr "More")
              [moremenu-icon]]]]
           [modal-moremenu {:open? @moremenu-open
                            :cancel #(reset! moremenu-open false)}
            [hidden-sub-name-links sub-names
             (set-ref-fn :moremenu-links)]]]))})))

(defn sub-bar []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        can-admin? @(subscribe [::user/can-admin?])
        is-admin? @(subscribe [::user/is-admin?])
        enable-totp? @(subscribe [::settings/enable-totp?])
        sub-names (subscribe [::user/topbar-sub-names])
        colors @(subscribe [::window/color ::admin-alert])
        disable-colors @(subscribe [::window/color ::admin-alert.disable])]
    [:<>
     (when (and is-admin? enable-totp?)
       [:div
        {:class (cls colors :fixed.top-0.w-100.db.h1p5.f1p1.tc.lh-copy.z-5)}
        (tr "Admin mode is currently enabled.")
        [:span {:id "logout"}
         [:button {:class (cls disable-colors :bn.underline.f1p1.pa0)
                   :on-click #(dispatch [::user/admin-logout])}
          (tr "Disable")]]])
     [bar sub-names is-authenticated? can-admin?]]))
