;; fe/views/inputs.cljs -- Reusable inputs for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.inputs
  (:require
   ["pikaday" :as pikaday]
   ["react-dom" :refer [findDOMNode]]
   [camel-snake-kebab.core :as csk]
   [camel-snake-kebab.extras :as cske]
   [cljs-time.core :as time]
   [clojure.string :as str]
   [goog.events.KeyCodes]
   [goog.string :as gstring]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.inputs :as inputs]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.common :refer [modal]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.util :refer [cls]]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(defn button
  "Create a button element.  The :button-class field in props should be
  one of :primary, :secondary or :modal.  The :class field, if set
  will be passed to views/util/cls along with the normal button styling."
  [{:keys [button-class class] :as props} & children]
  (let [colors @(subscribe [::window/button-colors button-class])
        classlist (if (or (string? class) (keyword? class))
                    (list class)
                    class)]
    (into [:button
           (merge (dissoc props :button-class)
                  {:class (apply cls :db.pv2.ph3.br1.bn.pointer colors
                                 classlist)})]
          children)))

(defn radio-button
  "Make a radio button with a label.
  Call 'value' to get the inital state of the radio button, and
  dispatch 'event' when it is selected."
  [{:keys [id label value form-value event class] :as properties}]
  [:label {:class class :for id}
   [:input (merge (dissoc properties :form-state :form-value :label :event
                          :class)
                  {:type "radio"
                   :on-change #(dispatch (conj event value))
                   :checked (= form-value value)})]
   label])

(defn checkbox
  "Make a checkbox with a label.
  Use `value` as the inital state of the checkbox, and dispatch
  `event` with the value of the checkbox when it is changed."
  [{:keys [id label label-class value event] :as properties}]
  [:label {:class label-class
           :for id}
   [:input (merge (dissoc properties :form-state :label :label-class :event)
                  {:type "checkbox"
                   :checked value
                   :on-change #(dispatch (conj event
                                               (-> % .-target .-checked)))})]
   label])

(defn- save-on-key-debounced
  "Save on keypresses, debounced to every 100 ms."
  [state save key]
  (if (#{goog.events.KeyCodes.TAB goog.events.KeyCodes.ENTER} (.-which key))
    (save)
    (when-not (:save-delay @state)
      (swap! state assoc :save-delay true)
      (js/setTimeout
       #(do (save)
            (swap! state assoc :save-delay false)) 100))))

(defn unstyled-text-input
  "Make a text input.
  Call `value` to get the initial value, and dispatch `event` with the
  new value when it is changed.  It's too slow to dispatch on every
  key press, so use a timeout to debounce saving on keypresses to 100
  ms.  Also dispatch the event on loss of focus.  The function
  `update-value!` allows the parent form to update the value of the text
  input when it needs to: it should update the binding of :value in
  the atom's map, and return a modified properties map."
  [{:keys [value update-value! event]}]
  (let [state (reagent/atom {:value (value)})
        trim str/trim
        save #(if (fn? event)
                (event (trim (:value @state)))
                (dispatch (conj event (trim (:value @state)))))
        save-on-key (partial save-on-key-debounced state save)
        update! #(swap! state assoc :value (-> % .-target .-value))
        ;; Update the value at intervals to catch pastes from
        ;; password managers.
        ref (atom nil)
        set-ref #(reset! ref %)
        update-and-save! #(when @ref
                            (let [new-value (.-value @ref)]
                              (swap! state assoc :value new-value)
                              (save)))]
    (reagent/create-class
     {:display-name "unstyled-text-input"
      ;; Depending on the browser (looking at you, FireFox)
      ;; we might not get a change event when a password manager
      ;; fills out a form field.
      :component-did-mount #(js/setTimeout update-and-save! 200)

      :reagent-render
      (fn [{:keys [type] :as properties}]
        [:input
         (-> (update-value! state properties)
             (dissoc :value :update-value! :event)
             (merge {:type (or type "text")
                     :value (:value @state)
                     :ref set-ref
                     :on-blur save
                     :on-change update!
                     :on-key-down save-on-key}))])})))

(defn text-input
  [{:keys [class disabled] :as props}]
  (let [input-colors @(subscribe [::window/input-colors disabled])]
    [unstyled-text-input
     (assoc props :class (cls :dib.lh-title.pa2.ba.bw1.br2.v-mid.shadow-6
                              input-colors class))]))

(defn textarea-input
  "Make a text input.
  Call `value` to get the initial value, and dispatch `event` with the
  new value when it is changed.  It's too slow to dispatch on every
  key press, so use a timeout to debounce saving on keypresses to 100
  ms.  Also dispatch the event on loss of focus.  The function
  `update-value!` allows the parent form to update the value of the text
  input when it needs to: it should update the binding of :value in
  the atom's map, and return a modified properties map."
  [{:keys [value update-value! event]}]
  (let [state (reagent/atom {:value (value)})
        save #(dispatch (conj event (str/trim (:value @state))))
        save-on-key (partial save-on-key-debounced state save)
        update! #(swap! state assoc :value (-> % .-target .-value))]
    (fn [{:keys [class disabled] :as properties}]
      (let [input-colors @(subscribe [::window/input-colors disabled])]
        [:textarea
         (-> (update-value! state properties)
             (dissoc :value :update-value! :event)
             (merge {:type "textarea"
                     :rows 2
                     :class (cls :dib.lh-title.pa2.ba.bw1.br2.v-mid.shadow-6
                                 input-colors class)
                     :value (:value @state)
                     :on-blur save
                     :on-change update!
                     :on-key-down save-on-key}))]))))

(defn make-suggestions
  "Return up to 10 substring matches for `value` from `choices`."
  [value choices]
  (when (> (count value) 2)
    (->> choices
         (filter #(str/includes? (str/lower-case %) (str/lower-case value)))
         (take 10))))

(defn- update-valid-choice [{:keys [value choices empty-is-valid?] :as state}]
  (assoc state :valid-choice
         (or (and (empty? value) empty-is-valid?)
             ((set choices) value))))

(defn- show-suggestions [state]
  (assoc state :show-suggestions? true :selected nil))

(defn- hide-suggestions [state]
  (assoc state :show-suggestions? false :selected nil))

(defn- next-suggestion [{:keys [selected choices value show-suggestions?]
                         :as state}]
  (let [suggestions (make-suggestions value choices)]
    (assoc state :selected
           (cond
             (not show-suggestions?) nil
             (nil? selected) 0
             (< selected (dec (count suggestions))) (inc selected)
             :else selected))))

(defn- previous-suggestion [{:keys [selected show-suggestions?] :as state}]
  (assoc state :selected
         (when (and show-suggestions? selected (> selected 0))
           (dec selected))))

(defn- save-selection [{:keys [value choices selected] :as state}]
  (assoc state :value
         (if (nil? selected)
           value
           (nth (make-suggestions value choices) selected))))

(defn suggesting-text-input
  "Create a text input that allows you to type and shows suggestions
  from a list of choices that match what you've typed in a dropdown below
  the input.

  From the property map, calls `value` to get the initial value and
  dispatch `event` with the new value when it is changed.  The
  function `update-value` allows the parent to update the value of the
  text input when it needs to.  `choices` should be a list of strings;
  once the user has typed more than two characters up to 10 matches from
  `choices` will be shown in a clickable list below the text input.

  Optionally supply a list of class names in `colors` and any other
  desired CSS class names in `class`."
  [{:keys [value update-value! event choices empty-is-valid?]}]
  (let [state (reagent/atom {:value (value)
                             :selected nil
                             :show-suggestions? false
                             :valid-choice ((set choices) (value))
                             :choices choices
                             :empty-is-valid? empty-is-valid?})
        save (fn []
               (let [value (:value @state)]
                 (dispatch (conj event (when value (str/trim value)))))
               (js/setTimeout #(swap! state hide-suggestions) 200)
               (js/setTimeout #(swap! state update-valid-choice) 200))
        save-selected (fn []
                        (swap! state save-selection)
                        (save))
        save-on-blur (fn []
                       (if-not (:show-suggestions? @state)
                         (save)
                         (do
                           ;; Allow time for a click event on a child
                           ;; before hiding the child.
                           (js/setTimeout #(swap! state hide-suggestions) 200)
                           (js/setTimeout #(swap! state update-valid-choice)
                                          200))))
        on-key #(condp = (.-which %)
                  goog.events.KeyCodes.ENTER (do
                                               (.preventDefault %)
                                               (save-selected))
                  goog.events.KeyCodes.TAB   (save)
                  goog.events.KeyCodes.ESC   (swap! state hide-suggestions)
                  goog.events.KeyCodes.DOWN  (swap! state next-suggestion)
                  goog.events.KeyCodes.UP    (swap! state previous-suggestion)
                  nil)
        update! (fn [ev]
                  (swap! state assoc :value (-> ev .-target .-value))
                  (swap! state show-suggestions))
        choose! (fn [val]
                  (swap! state assoc
                         :value val
                         :show-suggestions? false
                         :selected nil)
                  (save))
        wrapped-update-value! (fn [props]
                                (let [result (update-value! state props)]
                                  (swap! state update-valid-choice)
                                  result))]
    (fn [{:keys [colors disabled] :as properties}]
      (let [suggestions (make-suggestions (:value @state) choices)
            input-colors (or colors
                             @(subscribe [::window/input-colors disabled]))]
        [:div.relative
         [:input
          (-> (wrapped-update-value! properties)
              (dissoc :value :update-value! :event :choices :colors
                      :empty-is-valid?)
              (merge {:class (cls :dib.lh-title.pa2.ba.br2.v-mid.shadow-6
                                  (when (:show-suggestions? @state) :br--top)
                                  input-colors
                                  (:class properties)
                                  (when-not (:valid-choice @state) :b--red))
                      :type "text"
                      :value (:value @state)
                      :on-blur save-on-blur
                      :on-change update!
                      :on-key-down on-key
                      :auto-complete "off"}))]
         (when (and (seq suggestions)
                    (:show-suggestions? @state))
           [:ul {:class (cls :no-bullets.ba.bw1.br2.lh-copy.absolute.w-100
                             :br--bottom input-colors)}
            (doall
             (map-indexed (fn [num val]
                            [:li {:class (cls :lh-copy.ma0.ph2.pointer
                                              :hover-bg-light-silver
                                              (when (= num (:selected @state))
                                                :bg-light-silver))
                                  :key (str num val)
                                  :on-click #(choose! val)}
                             val])
                          suggestions))])]))))

(defn markdown-button
  "Create a button for the markdown editor."
  [name title markup disabled?]
  (let [day? @(subscribe [::window/day?])]
    [:div {:title title
           :class (cls :dib.pt1.ph1.w2.hover-bg-moon-gray
                       (when-not day? :fill-gray))
           :on-click (when-not disabled? markup)}
     [icon {} name]]))

(defn- apply-markup [state func]
  (let [{:keys [ref value]} @state
        start (.-selectionStart ref)
        end (.-selectionEnd ref)
        before-sel (subs value 0 start)
        sel (subs value start end)
        after-sel (subs value end (.-length value))
        [new-sel offset] (func sel)
        new-value (str before-sel new-sel after-sel)
        new-pos (+ (count before-sel) offset)]
    (swap! state assoc :value new-value)
    (set! (.. ref -value) new-value)
    (.setSelectionRange ref new-pos new-pos)
    (.focus ref)))

(defn- markup [state before after]
  (apply-markup state
                (fn [sel]
                  [(str before sel after)
                   (count (str before sel))])))

(defn- blockquote-markup [state]
  ;; Add blockquote markup to blank lines because that
  ;; makes a continueous blockquote bar for the whole quote,
  ;; instead of having gaps between paragraphs.
  (apply-markup state
                (fn [sel]
                  (let [lines (->> (str/split sel #"\n" -1)
                                   (map #(str "> " %)))]
                    [(str/join "\n" lines)
                     (+ (* 2 (count lines)) (count sel))]))))

(defn- spoiler-markup [state]
  ;; If we add spoiler markup to blank lines, the markdown engine
  ;; counts them as part of paragraphs, which will merge paragraphs
  ;; separated by blank lines.  So only spoiler the lines which are
  ;; not all whitespace.
  (apply-markup state
                (fn [sel]
                  (let [lines (str/split sel #"\n" -1)
                        num (->> lines
                                 (map str/trim)
                                 (filter seq)
                                 count)
                        spoilered (->> lines
                                       (map #(if (-> % str/trim empty?)
                                               %
                                               (str ">! " % " !<"))))]
                    [(str/join "\n" spoilered)
                     (+ (* 6 num) (count sel))]))))


(defn markdown-editor
  "Create a markdown editor input.
  Call `value` to get the initial value, and dispatch `event` with the
  new value when it is changed.  It's too slow to update the db on
  every key press, so the input keeps it in an atom and only updates
  the db on loss of focus.  The function `update-value!` allows the
  parent form to update the value of the text input when it needs to:
  it updates the binding of :value in the atom's map, and returns a
  modified properties map.  The `markdown-link-modal` component must
  be present on the page for the insert hyperlink button to work."
  [{:keys [value update-value! event]}]
  (let [state (reagent/atom {:value (value)})
        set-ref #(swap! state assoc :ref %)
        ;; Save to app-db.
        save #(dispatch (conj event (str/trim (:value @state))))

        markup-bold #(markup state "**" "**")
        markup-italic #(markup state "*" "*")
        markup-strikethrough #(markup state "~~" "~~")
        markup-href #(markup state "[" (str "](" % ")"))
        markup-link #(dispatch [::inputs/show-markdown-link-modal markup-href])
        markup-title #(markup state "# " "")
        markup-quote #(blockquote-markup state)
        markup-spoiler #(spoiler-markup state)
        markup-bulletlist #(markup state "- " "")
        markup-numberlist #(markup state "1. " "")
        markup-code #(markup state "`" "`")

        ;; Functions to handle key presses
        keymap [[#{:ctrl}        66  markup-bold]          ; ctrl-b
                [#{:ctrl :shift} 73  markup-italic]        ; ctrl-shift-i
                [#{:ctrl :shift} 83  markup-strikethrough] ; ctrl-shift-s
                [#{:ctrl :shift} 72  markup-title]         ; ctrl-shift-h
                [#{:ctrl :shift} 75  markup-link]          ; ctrl-shift-k
                [#{:ctrl :shift} 190 markup-quote]         ; ctrl-shift-.
                [#{}             9   save]                 ; TAB
                [#{:shift}       9   save]]                ; shift-TAB
        modifier-keys (fn [event]
                        (->> [(when (.-ctrlKey event) :ctrl)
                              (when (.-shiftKey event) :shift)]
                             (filter identity)
                             set))
        run-keymap (fn [event]
                     (doseq [[modifiers keycode action] keymap]
                       (when (and (= modifiers (modifier-keys event))
                                  (= keycode (.-which event)))
                         (action)
                         (when (not= 9 (.-which event))
                           (.preventDefault event)))))

        ;; Keep value updated in local state atom.
        update! #(swap! state assoc :value (-> % .-target .-value))

        ;; Update the value at intervals to catch pastes in all browsers.
        update-and-save! #(when (:ref @state)
                            (swap! state assoc :value (.-value (:ref @state)))
                            (save))]

    (reagent/create-class
     {:display-name "markdown-editor"
      :component-did-mount  #(js/setTimeout update-and-save! 200)

      :reagent-render
      (fn [{:keys [class buttonbar-class disabled] :as properties}]
        (let [markdown-btn (fn [name title markup]
                             [markdown-button name title markup disabled])
              day? @(subscribe [::window/day?])]
          [:div
           [:div
            {:class (cls :ba.bw1.lh-solid
                         (if day? :b--moon-gray :b--mid-gray)
                         buttonbar-class)}
            [markdown-btn "bold" (tr "Bold (ctrl-b)") markup-bold]
            [markdown-btn "italic" (tr "Italic (ctrl-shift-i)") markup-italic]
            [markdown-btn "strikethrough" (tr "Strikethrough (ctrl-shift-s)")
             markup-strikethrough]
            [markdown-btn "title" (tr "Title (ctrl-shift-h)") markup-title]
            [markdown-btn "gradient" (tr "Spoiler") markup-spoiler]
            [markdown-btn "link" (tr "Insert link (ctrl-shift-k)") markup-link]
            [markdown-btn "bulletlist" (tr "Bullet list") markup-bulletlist]
            [markdown-btn "numberlist" (tr "Number list") markup-numberlist]
            [markdown-btn "code" (tr "Code") markup-code]
            [markdown-btn "quote" (tr "Quote (ctrl-shift-.)") markup-quote]]
           [:textarea
            (-> (update-value! state properties)
                (dissoc :value :update-value! :save-fn :buttonbar-class)
                (merge {:type "textarea"
                        :class (cls :mt0.pa2.ba.bw1.b--moon-gray.br--top
                                    class)
                        :rows 10
                        :value (:value @state)
                        :ref set-ref
                        :on-blur save
                        :on-change update!
                        :on-key-down run-keymap}))]]))})))

(defn markdown-link-modal-content
  "Create the content for the markdown editor link modal."
  []
  (let [cancel #(dispatch [::inputs/show-markdown-link-modal])
        insert #(dispatch [::inputs/insert-markdown-link])
        value (fn [] @(subscribe [::inputs/markdown-link-modal-value]))]
    (fn [_]
      [:div
       [:p (tr "Insert hyperlink")]
       [text-input {:value value
                    :auto-focus true
                    :placeholder "https://"
                    :class :w-100.mb2
                    :required true
                    :update-value! (fn [_ props] props)
                    :event [::inputs/markdown-link-modal-update]}]
       [:div.pt2
        [button {:button-class :primary
                 :class :dib.mb2.mb0-ns.mr2
                 :on-click insert}
         (tr "OK")]
        [button {:button-class :modal
                 :class :dib
                 :on-click cancel}
         (tr "Cancel")]]])))

(defn markdown-link-modal
  "Create the modal form for entering a link for a markdown editor."
  []
  (let [toggle-modal #(dispatch [::inputs/show-markdown-link-modal])
        show-modal? (fn [] @(subscribe [::inputs/show-markdown-link-modal?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [markdown-link-modal-content]])))

(defn select
  "Create a select element with options."
  [{:keys [label value id options event label-class select-class]}]
  (let [colors @(subscribe [::window/select-input-colors])
        on-change #(dispatch (conj event
                                   (-> % .-target .-value keyword)))]
    [:div
     (when label
       [:label {:for id
                :class label-class} label])
     [:div.relative
      [:span {:class (cls :absolute.nopointer.right-05.top-05 colors)} " ▾"]
      (into [:select {:id id
                      :class (cls :input-reset.lh-copy.pl2.pr4.pv1.br1.bw1
                                  :outline-0.custom.w-100
                                  colors select-class)
                      :value value
                      :on-change on-change}]
            (map (fn [[val text]]
                   [:option {:value val} text])
                 options))]]))

(defn dropdown-menu-item
  "Create a single entry in a dropdown menu."
  [{:keys [key text disabled value event on-click]}]
  (let [nbsp (gstring/unescapeEntities "&nbsp;")
        day? @(subscribe [::window/day?])
        highlight (if day?
                    :hover-bg-light-gray
                    :hover-bg-dark-gray)]
    [:li.db.v-mid
     [:a {:class (cls :db.custom.pv1.ph2.f5.pointer.break-word highlight)
          :on-click #(when-not disabled
                       (on-click)
                       (dispatch (conj event key)))}
      (str (if (= key value)
             "✓"
             (str nbsp nbsp))) text]]))

(defn dropdown-menu
  "Create a dropdown menu for making a selection.
  Pass it 'choices', a list of tuples containing values and display
  text."
  []
  ;; Use a timeout to briefly stop rendering the dropdown
  ;; items, so that they will disappear when one is selected.
  (let [contents-hidden (reagent/atom false)
        timeout (atom nil)
        stop-timeout (when @timeout
                       (.clearTimeout js/window @timeout)
                       (reset! timeout nil))
        toggle-contents #(swap! contents-hidden not)
        hide-contents #(do
                         (toggle-contents)
                         (->> (.setTimeout js/window toggle-contents 50)
                              (reset! timeout)))]
    (reagent/create-class
     {:display-name "dropdown-menu"
      :component-will-unmount stop-timeout

      :reagent-render
      (fn [{:keys [id label choices value disabled event title-class title-fn
                   ul-class scrollable? wide?]}]
        (let [daynight @(subscribe [::window/day-night])
              title-colors (if (= :light daynight)
                             :mid-gray.bg-moon-gray.hover-bg-near-white
                             :silver.bg-dark-gray.hover-bg-near-black)
              menu-colors (if (= :light daynight)
                            :bg-white.b--moon-gray
                            :bg-near-black.b--gray)
              title (or title-fn #(str (get (into {} choices) %) " ▾"))]
          [:div.w100
           (when label
             [:span.pr1 label])
           [:ul.inline-flex.ma0.pa0.no-bullets
            [:li.dib.pa0.ma0.relative.v-mid.h-100.hover-db.pointer
             [:span {:id id
                     :class (cls :db.custom.pv1.ph2.f5.br1
                                 (or title-class title-colors))}
              (title value)]
             (when (and (not @contents-hidden) (not disabled))
               (into [:ul {:class (cls :dn.absolute.top-auto.left-0.ma0.pa0.z-3
                                       :hover-db.ba.bw1.br1
                                       (when wide?
                                         :w5.w6-ns)
                                       (when scrollable?
                                         :scrollable.mh-80vw)
                                       menu-colors
                                       ul-class)}
                      ]
                     (map (fn [[k v]]
                            (when (not= k :nothing-selected)
                              (dropdown-menu-item {:key k
                                                   :text v
                                                   :disabled disabled
                                                   :value value
                                                   :event event
                                                   :on-click hide-contents})))
                          choices)))]]]))})))

(defn datepicker-select [choices initial-value event]
  (let [select-colors @(subscribe [::window/select-input-colors])]
    [:select
     {:class (cls :ba.br2.bw1.pa2.mr2.shadow-6 select-colors)
      :value initial-value
      :on-change #(dispatch (conj event (-> % .-target .-value)))}
     (map (fn [x] [:option {:key x :value x} x]) choices)]))

(defn datepicker
  "Create a date selector component that lets you choose year,
  day and month individually."
  [{:keys [class start-year end-year subscription setter]}]
  (let [date @(subscribe subscription)
        days-in-month (time/day (time/last-day-of-the-month
                                 (:year date) (:month date)))]
    [:div {:class class}
     [datepicker-select
      (range start-year (inc end-year)) (:year date) (conj setter :year)]
     [datepicker-select (range 1 13) (:month date) (conj setter :month)]
     [datepicker-select
      (range 1 (inc days-in-month)) (:day date) (conj setter :day)]]))

(defn- transform-opts
  "Given a map of options, return a js object for a pikaday constructor
  argument."
  [opts]
  (clj->js (cske/transform-keys csk/->camelCaseString opts)))

(defn date-selector
  "Return a monthly calendar date-selector component.
  Takes a single map as its argument, with the following keys:

  date-atom: an atom (subscription) containing
             the date value represented by the picker.
  max-date-atom: atom (subscription) containing the maximum
             permitted date, or nil.
  min-date-atom: atom (subscription) containing the minimum
             permited date, or nil.
  on-select: a function to call when a new date is selected.
  pikaday-attrs: a map of options to be passed to the Pikaday constructor.
  input-attrs: a map of options to be used as <input> tag attributes."
  [{:keys [date-atom max-date-atom min-date-atom on-select pikaday-attrs _]}]
  (let [instance-atom (reagent/atom nil)]
    (reagent/create-class
     {:component-did-mount
      (fn [this]
        (let [default-opts
              {:field (findDOMNode this)
               :default-date @date-atom
               :set-default-date true
               :on-select on-select}
              opts (transform-opts (merge default-opts pikaday-attrs))
              instance (pikaday. opts)]
          (reset! instance-atom instance)
          (when date-atom
            (add-watch date-atom :update-instance
                       (fn [_ _ _ new]
                         ;; true pikaday to skip onSelect() callback
                         (.setDate instance new true))))
          (when min-date-atom
            (add-watch min-date-atom :update-min-date
                       (fn [_ _ _ new]
                         (.setMinDate instance new)
                         ;; If new max date is less than selected date,
                         ;; reset actual date to max
                         (when (< @date-atom new)
                           (reset! date-atom new)))))
          (when max-date-atom
            (add-watch max-date-atom :update-max-date
                       (fn [_ _ _ new]
                         (.setMaxDate instance new)
                         ;; If new max date is less than selected date,
                         ;; reset actual date to max
                         (when (> @date-atom new)
                           (reset! date-atom new)))))))
      :component-will-unmount
      (fn [_]
        (.destroy @instance-atom)
        (remove-watch instance-atom :update-instance)
        (remove-watch instance-atom :update-min-date)
        (remove-watch instance-atom :update-max-date)
        (reset! instance-atom nil))
      :display-name "pikaday-component"
      :reagent-render
      (fn [props]
        [:input (:input-attrs props)])})))
