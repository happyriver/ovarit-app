;; fe/views/donate.cljs -- Donation pages for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.donate
  (:require
   [ovarit.app.fe.content.donate :as donate]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.donate :as donate-form]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [modal]]
   [ovarit.app.fe.views.forms  :refer [transmission-error-if-present
                                       validation-error-if-present]]
   [ovarit.app.fe.views.funding-progress :refer [funding-progress]]
   [ovarit.app.fe.views.inputs :refer [button radio-button text-input]]
   [ovarit.app.fe.views.login :refer [sign-up-form-content]]
   [ovarit.app.fe.views.misc-panels :refer [page-not-found-panel
                                            waiting-panel]]
   [ovarit.app.fe.views.panels :refer [center-panel panel]]
   [ovarit.app.fe.views.sidebar :refer [footer-without-donate]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame.core :refer [subscribe dispatch]]
   [reagent.core :as reagent]))

(defn amount-selector
  "Create a clickable amount element."
  [{:keys [text value]}]
  (let [selection @(subscribe [::forms/field ::donate-form/donate :selection])
        selected? (= selection value)
        flair-colors @(subscribe [::window/flair-colors selected?])]
    [:div
     {:class (cls :di.pv2.w4-ns.w3.pv1.tc.ma1.br1 flair-colors)
      :on-click #(dispatch [::forms/edit ::donate-form/donate :selection
                            (if selected? nil value)])}
     text]))

(defn amounts
  "Create the amount selector for the donate form."
  []
  (let [amounts @(subscribe [::donate/formatted-preset-amounts])]
    [:div.flex.flex-wrap
     (map (fn [amt] ^{:key (:value amt)} [amount-selector amt]) amounts)
     [amount-selector {:text (tr "Custom")
                       :value :custom}]]))

(defn custom-amount
  "Create a text input for a custom amount."
  []
  (let [custom-amount (fn [] @(subscribe [::forms/field ::donate-form/donate
                                          :custom-amount]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state ::donate-form/donate])
            editing-disabled? @(subscribe [::forms/editing-disabled?
                                           ::donate-form/donate])
            selection @(subscribe [::forms/field
                                   ::donate-form/donate :selection])]
        (when (= selection :custom)
          [:div.pv3
           [:span (tr "Amount: ")]
           [text-input {:id "donate-custom-amount"
                        :value custom-amount
                        :required true
                        :update-value! forms/reset-input-on-success!
                        :form-state form-state
                        :disabled editing-disabled?
                        :event [::forms/edit
                                ::donate-form/donate :custom-amount]}]])))))

(defn recurring-or-one-time
  "Create a pair of radio buttons to choose the type of donation."
  []
  (let [recurring? @(subscribe [::forms/field ::donate-form/donate :recurring?])
        form-state @(subscribe [::forms/form-state ::donate-form/donate])
        editing-disabled? @(subscribe
                            [::forms/editing-disabled? ::donate-form/donate])]
    [:<>
     [radio-button
      {:name "donate-type" :id "donate-type-recurring"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label (tr "Recurring monthly donation")
       :value "recurring"
       :form-value (if recurring? "recurring" "one-time")
       :event [::forms/edit ::donate-form/donate :recurring? true]
       :form-state form-state}]
     [radio-button
      {:name "donate-type" :id "donate-type-one-time"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label (tr "One-time donation")
       :value "one-time"
       :form-value (if recurring? "recurring" "one-time")
       :event [::forms/edit ::donate-form/donate :recurring? false]
       :form-state form-state}]]))

(defn mission-link
  "Create a link to the site's mission."
  []
  [:a {:href (url-for :wiki/view :slug :about)
       :target "_blank"}
   (tr "mission")])

(defn cancel-subscription-confirmation
  "Create a dialog box to confirm subscription cancellation."
  [{:keys [cancel]}]
  (let [next-date @(subscribe [::donate/subscription-next-date])]
    [:div
     [:h2 "Are you sure?"]
     [:p (tr "If you cancel, we will not charge your card
                       again, but you will still see the subscriber
                       badge on your profile until %s." next-date)]
     [:div.pv2
      [button {:button-class :modal
               :on-click #(dispatch [::donate/cancel-subscription])}
       (tr "Yes, cancel my subscription")]]
     [:div.pv2
      [button {:button-class :modal
               :on-click cancel}
       (tr "No, go back")]]]))

(defn cancel-subscription-button
  "Create a button with a confirmation modal, to cancel a subscription."
  []
  (let [show-modal-state (reagent/atom false)
        show-modal? (fn [] @show-modal-state)
        toggle-modal #(swap! show-modal-state not)]
    (fn []
      (let [modal-colors @(subscribe [::window/form-background])]
        [:<>
         [button {:button-class :secondary
                  :class :mv2
                  :on-click toggle-modal}
          (tr "Cancel Donation Subscription")]
         [modal {:show show-modal?
                 :cancel toggle-modal
                 :foreground modal-colors
                 :show-cancel-x? false}
          [cancel-subscription-confirmation {:cancel toggle-modal}]]]))))

(defn blurb []
  (let [is-donor? @(subscribe [::donate/is-subscriber?])
        amount @(subscribe [::donate/formatted-subscribed-amount])
        next-date @(subscribe [::donate/subscription-next-date])
        status @(subscribe [::donate/payment-server-status])]
    [:span.mb2
     [:p
      (if is-donor?
        (tr "Thank you for supporting Ovarit! ")
        (tr "Thank you for considering donating! "))
      (trm-html "Ovarit is funded entirely by donations, and our
                 donors make it possible for us to run this project
                 according to our %{mission}, without ads."
                {:mission (mission-link)})]
     (cond
       (= status :error)
       [:p.dib.pa1.ba.br1.bw2.b--red.lh-copy
        (tr "Sorry, we are having trouble communicating with our payment server.
             Please try again later.")]

       is-donor?
       [:<>
        [:h2 (tr "Your donation subscription")]
        [:p (tr "You are currently subscribed to donate %s per month."
                amount)]
        #_[:p (tr "Your last subscription donation was on: %s" "TODO")]
        [:p (tr "Your next donation is scheduled for %s." next-date)]
        [cancel-subscription-button]
        [:h2.mt4.mb2 (tr "Make an additional one-time donation")]]

       :else
       [:<>
        [:p
         (trm-html "Your payment information will be handled securely
                    by our payment processor, Stripe, according to
                    %{privacy} and %{stripe}.  Donations to Ovarit LLC
                    are not tax-deductable."
                   {:privacy [:a
                              {:href (url-for :wiki/view
                                              :slug "privacy")
                               :target "_blank"}
                              (tr "our privacy policy")]
                    :stripe [:a {:href "https://stripe.com/privacy"
                                 :target "_blank"}
                             (tr "Stripe's privacy policy")]})]
        [:p (tr "Please select the amount in US dollars to donate and
                 whether you would like to set up an automatic,
                 recurring monthly donation, or make a one-time
                 donation.  You can change your donation settings at
                 any time.")]])]))

(defn continue-button
  "Create the continue button for the donation setup form."
  [_]
  (let [dispatch-send #(dispatch
                        [::forms/action ::donate-form/donate :continue])]
    (fn [text]
      (let [editing-disabled? @(subscribe [::forms/editing-disabled?
                                           ::donate-form/donate])]
        [:div.pv3
         [button
          {:button-class :primary
           :disabled editing-disabled?
           :on-click dispatch-send
           :id "continue"}
          text]]))))

(defn setup-form
  "Create the donation setup form."
  []
  (let [is-donor? @(subscribe [::donate/is-subscriber?])]
    [:div
     [amounts]
     [custom-amount]
     (when-not is-donor?
       [recurring-or-one-time])
     [validation-error-if-present ::donate-form/donate]
     [transmission-error-if-present ::donate-form/donate]
     [continue-button (tr "Continue to Payment")]]))

(defn sign-up-form
  "Create a form for unauthenticated users who are interested in donating."
  []
  (let [form-colors @(subscribe [::window/form-background])]
    [:div {:class (cls :measure-wide-ns.center.w7.pa3.br3.shadow-4-ns
                       form-colors)}
     [:h1 (tr "Donate to Ovarit")]
     [:div.left
      [:p
       (trm-html "Thank you for considering donating!
                     Ovarit is funded entirely by donations, and our
                     donors make it possible for us to run this
                     project according to our %{mission}, without
                     ads."
                 {:mission (mission-link)})]
      [:p (tr "To continue, please register or log in.")]
      [sign-up-form-content]]]))

(defn sign-up-panel
  "Create a page for unauthenticated users who are interested in donating."
  []
  [center-panel {:footer footer-without-donate}
   [sign-up-form]])

(defn setup-panel
  "Create the panel to allow the user to choose donation options."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        loaded? (= @(subscribe [::window/status]) :loaded)
        status @(subscribe [::donate/payment-server-status])]
    (cond
      (not is-authenticated?)
      [sign-up-panel]

      (not loaded?)
      [waiting-panel]

      :else
      [panel {:sidebar :<>
              :footer footer-without-donate}
       [:div.flex.flex-wrap.w-100
        [:div.w-100.w-70-ns.pr4
         [:h1 (tr "Donate")]
         [blurb]
         (when-not (= status :error)
           [setup-form])]
        [:div.w-100.w-30-ns.mt4
         [funding-progress]]]])))

(defn cancel-panel
  "Create the subscription cancellation acknowledgement page."
  []
  (let [form-colors @(subscribe [::window/form-background])]
    [center-panel {:footer footer-without-donate}
     [:div {:class (cls :measure-wide-ns.w7.pa3.br3 form-colors)}
      [:h2 (tr "Cancelled subscription")]
      [:p (tr "Your subscription has been cancelled.")]
      [:p (tr "Thank you again for your past donations!
                 Your support means so much to us.")]
      [:p (trm-html "You can re-start your subscription or make a
                       one-time donation at any time on the %{donate}."
                    {:donate [:a {:href (url-for :donate/setup)}
                              "donation page"]})]]]))

(defn acknowledgement-panel
  "Create a donation acknowledgement panel."
  []
  (let [amount @(subscribe [::donate/formatted-completed-amount])
        recurring? @(subscribe [::donate/completed-recurring?])
        form-colors @(subscribe [::window/form-background])]
    [center-panel {:footer footer-without-donate}
     [:div {:class (cls :measure-wide-ns.center.pa3.br3.shadow-4-ns
                        form-colors)}
      [:div.left
       [:h1 (tr "Donation Complete")]
       (if recurring?
         [:<>
          [:p (tr "Thank you for donating!
                     Your support means so much to us.")]
          [:p (tr "You are subscribed to donate %s each month,
                    starting today." amount)]
          [:p (trm-html "You can adjust or cancel your subscription at any
                          time on the %{donate}."
                        {:donate [:a {:href (url-for :donate/setup)}
                                  "donation page"]})]]
         [:<>
          [:p (tr "You donated %s today." amount)]
          [:p (tr "Thank you for donating!
                     Your support means so much to us.")]
          [:a {:href (url-for :home/index)}
           "Return to the front page."]])]]]))

(defn success-panel
  "Create the donation acknowledgement panel."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        completed-session @(subscribe [::donate/completed-session])
        loaded? (= @(subscribe [::window/status]) :loaded)
        status @(subscribe [::donate/payment-server-status])]
    (cond
      (not is-authenticated?)
      [sign-up-panel]

      (not loaded?)
      [waiting-panel]

      (= status :error)
      [setup-panel]

      (nil? completed-session)
      [page-not-found-panel]

      :else
      [acknowledgement-panel])))
