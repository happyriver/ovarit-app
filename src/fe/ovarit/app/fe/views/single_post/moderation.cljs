;; fe/views/single-post/moderation.cljs -- Single post panel moderation components
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.single-post.moderation
  (:require
   [ovarit.app.fe.content.comment.moderation :as comment-mod]
   [ovarit.app.fe.content.post.moderation :as post-mod]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.moderation :as mod-forms]
   [ovarit.app.fe.views.common :refer [modal]]
   [ovarit.app.fe.views.forms :refer [transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button checkbox text-input]]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(defn- mod-action-form-content
  "Create the form content for mods to enter deletion and undeletion reasons."
  [{:keys [cancel form]} & _]
  (let [send-fn #(do (.preventDefault %)
                     (dispatch [::forms/action form :send]))
        value (fn [] @(subscribe [::forms/field form :reason]))]
    (fn []
      (let [this (reagent/current-component)
            props (reagent/props this)
            {:keys [header button-text button-sending-text]} props
            children (reagent/children this)
            disabled? @(subscribe [::forms/editing-disabled? form])
            form-state @(subscribe [::forms/form-state form])]
        [:form {:on-submit (when-not disabled?
                             send-fn)}
         [:p header]
         [text-input {:value value
                      :class :w-100.mb2
                      :required true
                      :form-state form-state
                      :update-value! forms/reset-input-on-success!
                      :disabled disabled?
                      :event [::forms/edit form :reason]}]
         (into [:<>] children)
         [validation-error-if-present form]
         [transmission-error-if-present form]
         [:div.pt2
          [button {:button-class :primary
                   :type :submit
                   :class :dib.mb2.mb1-ns.mr3}
           (if (= :SENDING form-state)
             button-sending-text
             button-text)]
          [button {:button-class :modal
                   :type :button
                   :class :dib
                   :on-click cancel}
           (tr "Cancel")]]]))))

(defn- modal-mod-delete-post-form
  "Create the form for mods to enter a reason for deleting a post."
  []
  (let [toggle-modal #(dispatch [::post-mod/toggle-mod-delete-post-form])
        show-modal? (fn []
                      @(subscribe [::post-mod/show-mod-delete-post-form?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [mod-action-form-content {:cancel toggle-modal
                                 :form ::mod-forms/action
                                 :header (tr "Why are you deleting this post?")
                                 :button-text (tr "Delete post")
                                 :button-sending-text (tr "Deleting...")}]])))

(defn- modal-mod-undelete-post-form
  "Create the form for mods to enter a reason for un-deleting a post."
  []
  (let [toggle-modal
        #(dispatch [::post-mod/toggle-mod-undelete-post-form])

        show-modal?
        (fn [] @(subscribe [::post-mod/show-mod-undelete-post-form?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [mod-action-form-content
        {:cancel toggle-modal
         :form ::mod-forms/action
         :header (tr "Why are you undeleting this?")
         :button-text (tr "Restore post")
         :button-sending-text (tr "Restoring...")}]])))

(defn- modal-mod-delete-comment-form
  "Create the form for mods to enter a reason for deleting a comment."
  []
  (let [form ::mod-forms/delete-comment
        toggle-modal
        #(dispatch [::comment-mod/toggle-mod-delete-comment-form])

        show-modal?
        (fn []
          @(subscribe [::comment-mod/show-mod-delete-comment-form?]))

        reason
        (fn []
          @(subscribe [::forms/field form :delete-children-reason]))]
    (fn []
      (let [has-children? @(subscribe [::forms/field form :has-children?])
            delete-children? @(subscribe [::forms/field form :delete-children?])
            form-state @(subscribe [::forms/form-state form])
            disabled? @(subscribe [::forms/editing-disabled? form])]
        [modal {:show show-modal?
                :cancel toggle-modal
                :background :bg-black-50
                :width :w-90.w-80-ns.mw6}
         [mod-action-form-content
          {:cancel toggle-modal
           :form form
           :header (tr "Why are you deleting this comment?")
           :button-text (if delete-children?
                          (tr "Delete comment and children")
                          (tr "Delete comment"))
           :button-sending-text (tr "Deleting...")}
          (when has-children?
            [checkbox {:id :delete-children-checkbox
                       :class :mt2
                       :value delete-children?
                       :event [::forms/edit form :delete-children?]
                       :label [:span.ml1
                               (tr "Delete all child comments too?")]}])
          (when delete-children?
            [:div.mt3
             [:p (tr "Message for the authors of the child comments:")]
             [text-input {:value reason
                          :class :w-100.mb2
                          :required true
                          :form-state form-state
                          :update-value! forms/reset-input-on-success!
                          :disabled disabled?
                          :event [::forms/edit form :delete-children-reason]}]])]]))))

(defn- modal-mod-undelete-comment-form
  "Create the form for mods to enter a reason for un-deleting a comment."
  []
  (let [toggle-modal
        #(dispatch [::comment-mod/toggle-mod-undelete-comment-form])

        show-modal?
        (fn []
          @(subscribe [::comment-mod/show-mod-undelete-comment-form?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [mod-action-form-content
        {:cancel toggle-modal
         :form ::mod-forms/action
         :header (tr "Why are you undeleting this?")
         :button-text (tr "Restore comment")
         :button-sending-text (tr "Restoring...")}]])))

(defn mod-modals
  "Create the modals for mods on the single post panel."
  []
  [:<>
   [modal-mod-delete-post-form]
   [modal-mod-undelete-post-form]
   [modal-mod-delete-comment-form]
   [modal-mod-undelete-comment-form]])
