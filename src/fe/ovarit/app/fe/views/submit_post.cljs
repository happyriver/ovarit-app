;; fe/views/submit-post.cljs -- Submit post panel for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.submit-post
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.sub :as sub-forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [dangerous-html helper-text]]
   [ovarit.app.fe.views.flair :refer [post-flair]]
   [ovarit.app.fe.views.forms :refer [preview
                                      transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button checkbox date-selector
                                       markdown-editor markdown-link-modal
                                       radio-button
                                       suggesting-text-input text-input]]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]
   [reagent.dom.server :as reagent-dom]))

(def form ::sub-forms/submit-post)

(defn- input-label
  "Create a label for a section of the form."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        props (reagent/props this)]
    (into [:label.gr-align-start.mv2 props] children)))

(defn- sub-input
  "Create the sub input for the submit post form."
  []
  (let [sub (fn [] @(subscribe [::forms/field form :sub]))
        update-value! (partial forms/reset-input-on-field-change! :loaded-sub)
        id :submit-post-sub]
    (fn []
      (let [names @(subscribe [::subs/all-sub-names])
            loaded-sub @(subscribe [::forms/field form :loaded-sub])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:<>
         [input-label {:id id} (tr "Circle:")]
         [:span
          [suggesting-text-input {:id id
                                  :value sub
                                  :class :w-100
                                  :spell-check false
                                  :choices names
                                  :loaded-sub loaded-sub
                                  :update-value! update-value!
                                  :disabled editing-disabled?
                                  :event [::forms/edit form :sub]}]]]))))

(defn- post-type-radio-button
  [{:keys [ptype label]}]
  (let [form-values @(subscribe [::forms/form form])
        {:keys [permitted-post-types selected-post-type
                mod-only-types]} form-values]
    (when (and (permitted-post-types ptype) selected-post-type)
      [radio-button
       {:id (keyword (str (-> ptype name str/lower-case) "-post"))
        :class (cls :db.mv2)
        :label (if (mod-only-types ptype)
                 (tr "%s (mods only)" label)
                 label)
        :value (name ptype)
        :form-value (name selected-post-type)
        :event [::forms/edit form :selected-post-type ptype]}])))

(defn- post-type-selector
  "Show radio buttons for selecting the current post type."
  []
  (let [form-values @(subscribe [::forms/form form])
        user-can-upload? @(subscribe [::user/can-upload?])
        {:keys [permitted-post-types]} form-values
        editing-disabled? @(subscribe [::forms/editing-disabled? form])]
    (cond
      (nil? (:sub form-values))
      [:<>
       [:p]
       [:p (tr "Please choose a circle for your post.")]]

      (empty? permitted-post-types)
      [:<>
       [:p]
       [:p (tr "Only mods can post in this circle.")]]

      (and (= permitted-post-types #{:UPLOAD}) (not user-can-upload?))
      [:<>
       [:p]
       [:p (tr "This circle only allows upload posts, and you don't
                have permission to upload files yet.")]]

      :else
      [:<>
       [:p.gr-align-start.mv2 (tr "Post type:")]
       [:fieldset.pa0.bn
        {:disabled editing-disabled?}
        [post-type-radio-button {:ptype :LINK :label (tr "Link post")}]
        [post-type-radio-button {:ptype :TEXT :label (tr "Text post")}]
        (when user-can-upload?
          [post-type-radio-button {:ptype :UPLOAD :label (tr "Upload file")}])
        [post-type-radio-button {:ptype :POLL :label (tr "Poll")}]]])))

(defn- rules
  "Show the rules for the current post type in the current sub."
  []
  (let [html-rules @(subscribe [::sub-forms/html-rules])]
    (when (seq html-rules)
      [:<>
       [:p]
       [:div.bg-near-white.w-100.ph3
        [dangerous-html {} html-rules]]])))

(defn- title-input
  "Create an input for the post title."
  []
  (let [value (fn [] @(subscribe [::forms/field form :title]))
        id :submit-post-title]
    (fn []
      (let [grab-state @(subscribe [::sub-forms/grab-state form])
            disabled? @(subscribe [::forms/editing-disabled? form])]
        [:<>
         [input-label {:id id} (tr "Title:")]
         [text-input {:id id
                      :value value
                      :class :w-100
                      :grab-state grab-state
                      :disabled disabled?
                      :update-value! sub-forms/reset-input-on-grab-title!
                      :event [::forms/edit form :title]}]]))))

(defn- content-editor
  []
  (let [content (fn [] @(subscribe [::forms/field form :content]))
        id :submit-post-content]
    (fn []
      (let [form-state (:main @(subscribe [::forms/form-state form]))
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:<>
         [input-label {:id id} (tr "Content:")]
         [:div
          [markdown-editor
           {:id id
            :value content
            :buttonbar-class :mw7
            :class :mb2.w-100.mw7
            :update-value! forms/reset-input-on-success!
            :form-state form-state
            :disabled editing-disabled?
            :event [::forms/edit form :content]}]]]))))


(defn text-fields
  "Create the content fields for the text post type."
  []
  [content-editor])

(defn- link-fields
  "Create the content fields for the link post type."
  []
  (let [value (fn [] @(subscribe [::forms/field form :link]))
        grab #(do (.preventDefault %)
                  (dispatch [::sub-forms/grab-title]))
        id :submit-post-link]
    (fn []
      (let [form-state (:main @(subscribe [::forms/form-state form]))
            disabled? @(subscribe [::forms/editing-disabled? form])]
        [:<>
         [input-label {:id id} (tr "Link:")]
         [:div.flex
          [text-input {:id id
                       :value value
                       :class :w-100.mr2
                       :form-state form-state
                       :disabled disabled?
                       :update-value! forms/reset-input-on-success!
                       :event [::forms/edit form :link]}]
          [button
           {:button-class :secondary
            :class :w-20
            :on-click grab}
           (tr "Grab title")]]]))))

(defn poll-options
  "Create the text inputs for the poll options."
  []
  (let [options @(subscribe [::forms/field form :poll-options])
        form-state (:main @(subscribe [::forms/form-state form]))
        disabled? @(subscribe [::forms/editing-disabled? form])
        can-add-option? @(subscribe [::sub-forms/enable-add-poll-option?])]
    [:<>
     [:p.gr-align-start.mv2 (tr "Options:")]
     [:fieldset.pa0.bn
      (map (fn [option num]
             ^{:key num}
             [text-input {:value (fn [] option)
                          :class :w-100.mr2.mb1
                          :form-state form-state
                          :disabled disabled?
                          :update-value! forms/reset-input-on-success!
                          :event [::sub-forms/edit-poll-option num]}])
           options (range))
      (when can-add-option?
        [button
         {:button-class :secondary
          :class :dib
          :disabled disabled?
          :on-click #(do (.preventDefault %)
                         (dispatch [::sub-forms/add-poll-option]))}
         (tr "Add another option")])]]))

(defn- poll-hide-results-checkbox
  "Create the poll hide results option checkboxes."
  []
  (let [form-state (:main @(subscribe [::forms/form-state form]))
        disabled? @(subscribe [::forms/editing-disabled? form])
        hide? @(subscribe [::forms/field form :hide-results?])]
    [:<>
     [:p]
     [checkbox {:label (tr "Hide results until poll closes")
                :value hide?
                :disabled disabled?
                :event [::forms/edit form :hide-results?]
                :form-state form-state}]]))

(defn- poll-close-options
  "Create the checkbox and datepicker for closing a poll."
  []
  (let [form-state (:main @(subscribe [::forms/form-state form]))
        disabled? @(subscribe [::forms/editing-disabled? form])
        input-colors @(subscribe [::window/input-colors disabled?])
        close-poll? @(subscribe [::forms/field form :close-poll?])]
    [:<>
     [:p]
     [:div
      [checkbox {:label (tr "Set a date to close the poll")
                 :label-class :dib
                 :value close-poll?
                 :disabled disabled?
                 :event [::forms/edit form :close-poll?]
                 :form-state form-state}]
      (when close-poll?
        [date-selector
         {:date-atom (subscribe [::forms/field form :close-date])
          :on-select #(dispatch [::forms/edit form :close-date %])
          :pikaday-attrs {:min-date (js/Date.)
                          :format "MM/DD/YYYY"}
          :input-attrs {:placeholder (tr "Close")
                        :class (cls :dib.pa1.ml3.ba.bw1.br2
                                    :v-mid.shadow-6 input-colors)
                        :auto-complete "off"}}])]]))

(defn- poll-fields
  "Create the content fields for the poll post type."
  []
  [:<>
   [poll-options]
   [content-editor]
   [poll-hide-results-checkbox]
   [poll-close-options]])

(defn- use-pasted-image
  [mime-types items]
  (let [content-types #(-> (.-types %) js->clj set)
        get-image-type #(-> mime-types
                            (set/intersection (content-types %))
                            first)
        item (->> (for [i (range (.-length items))]
                    (aget items i))
                  (filter get-image-type)
                  first)]
    (log/info {:clipboard-items (for [i (range (.-length items))]
                                  (.-types (aget items i)))
               :item-chosen (when item
                              (get-image-type item))}
              "Received paste using async API")
    (when item
      (-> (.getType item (get-image-type item))
          (.then (fn [blob]
                   (dispatch [::forms/edit form :file blob])))))))

(defn- on-paste
  [mime-types ev]
  (.preventDefault ev)
  (.stopPropagation ev)
  ;; Prefer the async API since it doesn't block the browser.
  (if (some? (.. js/navigator -clipboard -read))
    (-> (js/navigator.clipboard.read)
        (.then (partial use-pasted-image mime-types)))
    (let [items (.. ev -clipboardData -items)
          item (->> (for [i (range (.-length items))]
                      (aget items i))
                    (filter #(mime-types (.-type %)))
                    first)]
      (log/info {:clipboard-items (for [i (range (.-length items))]
                                    [(-> items (aget i) (.-type))
                                     (-> items (aget i) (.-kind))])
                 :item-chosen (when item
                                (.-type item))}
                "Received paste using clipboardData")
      (when item
        (dispatch [::forms/edit form :file (.getAsFile item)])))))

(defn- drag-target-text
  []
  ;; Used to have a suggestion here that you could drag or paste an
  ;; image, but as of 2024 browsers behave very inconsistently so
  ;; don't make any promises.
  [:strong (tr "Choose an image file to upload, or paste or drag
                and drop an image here (experimental feature).")])

(defn- drag-target-label
  ;; Making the label content-editable allows Paste to appear on the
  ;; mouse menu for the element, but React complains if you create a
  ;; component that is content-editable with children managed by
  ;; React. Make it work by creating the component without children,
  ;; and adding them after the component mounts.  Since the render
  ;; uses `subscribe` (via `tr`) the rendering needs to be done in
  ;; the :reagent-render function, not in the :component-did-mount
  ;; function.  The other disadvantage of content-editable is that
  ;; the content is editable.  Deal with that by immediately reverting
  ;; all edits.
  [_]
  (let [ref (reagent/atom nil)
        set-ref! #(reset! ref %)]
    (reagent/create-class
     {:display-name "drag-target-label"

      :component-did-mount
      (fn []
        (let [{:keys [label html]} @ref]
          (set! (.-innerHTML label) html)))

      :reagent-render
      (fn [{:keys [drag-state] :as props}]
        (let [children (reagent-dom/render-to-string [drag-target-text])
              mime-types @(subscribe [::settings/upload-post-mime-types])
              colors @(subscribe [::window/drag-target-colors drag-state])
              revert-edits #(set! (.-innerHTML (.-target %)) children)]
          [:label (merge
                   {:content-editable true
                    :on-input revert-edits
                    :tab-index -1
                    :on-paste (partial on-paste mime-types)
                    :class (cls :db.pointer.pa3.oo-1.outline-dashed.z-1 colors)
                    :ref #(set-ref! {:label % :html children})}
                   (dissoc props :drag-state))]))})))

(defn- image-preview
  []
  (let [ref (reagent/atom nil)
        set-ref! #(reset! ref %)]
    (reagent/create-class
     {:display-name :image-preview

      :component-did-mount
      (fn []
        (let [reader (js/FileReader.)
              {:keys [img file]} @ref]
          (set! (.-onload reader)
                (fn [ev]
                  (let [data-url (.. ev -target -result)]
                    (.setAttribute img "src" data-url))))
          (.readAsDataURL reader file)))

      :reagent-render
      (fn []
        (let [file @(subscribe [::forms/field form :file])]
          [:img.db.upload-preview {:ref #(set-ref! {:img %
                                                    :file file})}]))})))

(defn- choose-file-input
  []
  (let [id :submit-post-file
        set-upload #(dispatch [::forms/edit form :file
                               (-> % .-target .-files (.item 0))])
        prevent-default #(.preventDefault %)
        mime-types @(subscribe [::settings/upload-post-mime-types])
        accept (str/join "," mime-types)
        lab-rat? @(subscribe [::user/lab-rat?])]
    (fn [{:keys [drag-state]}]
      (if lab-rat?
        [:div
         [:input {:id id
                  :class (cls :absolute.w-5.h-5.tr.z-2.o-0
                              :focus-label)
                  :type :file
                  :accept accept
                  :on-change set-upload}]
         [drag-target-label {:for id
                             :drag-state drag-state}]]
        [:div.relative.pa2
         [:input {:id id
                  :class (cls :relative.tr.z-2.o-0)
                  :type :file
                  :accept accept
                  :on-change set-upload}]
         [:div.absolute.z-1.top-0.left-0
          [button {:button-class :secondary
                   :class :dib
                   #_#_:disabled disabled?
                   :on-click prevent-default}
           (tr "Choose a file to upload")]]]))))

(defn- upload-fields
  "Create the content fields for the upload post type."
  [_]
  (let [clear-upload #(do (.preventDefault %)
                          (dispatch [::forms/edit form :file nil]))
        ]
    (fn [{:keys [_drag-state] :as props}]
      (let [filename @(subscribe [::sub-forms/upload-filename])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:<>
         [:div.gr-align-start.mv2 (tr "Content:")]
         (if (nil? filename)
           [choose-file-input props]
           [:div
            [image-preview]
            [:span filename]
            [button {:button-class :secondary
                     :class :db.mt2
                     :disabled editing-disabled?
                     :on-click clear-upload}
             (tr "Remove")]])]))))

(defn- nsfw-checkbox
  "Create the NSFW checkbox."
  []
  (let [nsfw? @(subscribe [::forms/field form :nsfw?])
        sub-nsfw? @(subscribe [::forms/field form :sub-nsfw?])
        form-state (:main @(subscribe [::forms/form-state form]))
        editing-disabled? @(subscribe [::forms/editing-disabled? form])]
    (when-not sub-nsfw?
      [:<>
       [:p]
       [checkbox {:disabled editing-disabled?
                  :label (tr "NSFW?")
                  :value nsfw?
                  :event [::forms/edit form :nsfw?]
                  :form-state form-state}]])))

(defn- flair-helper-text
  "Create some explanatory text for the flairs section."
  []
  (let [mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
        form-vals @(subscribe [::forms/form form])
        {:keys [sub user-must-flair user-can-flair]} form-vals
        sublink [:a {:href (url-for :sub/view-sub :sub sub)}
                 sub]]
    (cond
      mod-or-admin? nil

      user-must-flair
      [helper-text
       (trm-html
        "Flairs are required in %{sublink}. The flair you
         choose will be shown next to the title of your post."
        {:sublink sublink})]

      user-can-flair
      [helper-text
       (trm-html
        "Flairs are optional in %{sublink}. If you choose one,
         it will be shown next to the title of your post."
        {:sublink sublink})])))

(defn- flair-choices
  "Create the list of flair choices, and allow one to be selected."
  []
  (let [flairs @(subscribe [::sub-forms/flairs])
        {:keys [selected-flair user-must-flair]} @(subscribe [::forms/form form])
        editing-disabled? @(subscribe [::forms/editing-disabled? form])
        select-flair #(when-not editing-disabled?
                        (dispatch [::forms/edit form :selected-flair %]))
        deselect-flair #(when (and (= selected-flair %)
                                   (not editing-disabled?)
                                   (not user-must-flair))
                          (dispatch [::forms/edit form :selected-flair nil]))]
    (into [:<>]
          (map (fn [{:keys [id text]}]
                 ^{:key text}
                 [:<>
                  [:input.disappear.absolute.focus-label
                   {:type "radio"
                    :id (str "flair-" id)
                    :name "flair"
                    :on-change #(select-flair id)
                    :on-click #(deselect-flair id)
                    :checked (= selected-flair id)}]
                  [:label.pv1.mr2.mv1 {:for (str "flair-" id)}
                   [post-flair {:selected? (= id selected-flair)} text]]])
               flairs))))

(defn- flairs
  "Create the flair section of the submit post form, if needed."
  []
  (let [form-vals @(subscribe [::forms/form form])
        {:keys [user-must-flair user-can-flair]} form-vals
        mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
        flairs @(subscribe [::sub-forms/flairs])]
    (when (and (seq flairs) (or mod-or-admin? user-can-flair user-must-flair))
      [:<>
       (if mod-or-admin?
         [:p.gr-align-start.mv2 (tr "Flair:")]
         [:p])
       [:div.flex.flex-wrap.pb2
        [flair-helper-text]
        [flair-choices]]])))

(defn- errors-if-present
  "Create the form errors section, if any are present."
  []
  (let [form-states (-> @(subscribe [::forms/form-state form]) vals set)]
    (when (set/intersection #{:SHOW-ERROR :SHOW-FAILURE} form-states)
      [:<>
       [:div]
       [:div
        [validation-error-if-present form]
        [transmission-error-if-present form]]])))

(defn buttons
  "Create the buttons for the submit post form"
  []
  (let [post-type @(subscribe [::forms/field form :selected-post-type])
        editing-disabled? @(subscribe [::forms/editing-disabled? form])
        form-state (:main @(subscribe [::forms/form-state form]))]
    [:<>
     [:div]
     [:div.gr-justify-start
      [button
       {:button-class :primary
        :class (cls :dib.mr2)
        :disabled editing-disabled?
        :on-click #(do (.preventDefault %)
                       (dispatch [::forms/action form :submit]))}
       (if (#{:SENDING :SHOW-SUCCESS} form-state)
         (tr "Submitting...")
         (tr "Submit post"))]
      (when (#{:POLL :TEXT} post-type)
        [button
         {:button-class :secondary
          :class (cls :dib)
          :disabled editing-disabled?
          :on-click #(do (.preventDefault %)
                         (dispatch [::sub-forms/see-preview]))}
         (tr "Preview")])]]))

(defn post-preview
  []
  (let [preview-open? @(subscribe [::forms/field form :preview-open?])
        post-type @(subscribe [::forms/field form :selected-post-type])]
    (when (and preview-open? (#{:POLL :TEXT} post-type))
      [:<>
       [:div (tr "Preview:")]
       [:div.bg-near-white.ph3.pv1
        [preview {:form form}]]])))

(defn submit-post-form
  "Create the submit post form."
  [{:keys [drag-state]}]
  (let [post-type @(subscribe [::forms/field form :selected-post-type])]
    [:form.mw7.gr-auth-form.mv3
     [sub-input]
     [post-type-selector]
     (when post-type
       [:<>
        [rules]
        [title-input]])
     (case post-type
       :TEXT [text-fields]
       :LINK [link-fields]
       :POLL [poll-fields]
       :UPLOAD [upload-fields {:drag-state @drag-state}]
       nil)
     (when post-type
       [:<>
        [nsfw-checkbox]
        [flairs]
        [errors-if-present]
        [buttons]
        [post-preview]])]))

(defn- prevent-and-stop
  [ev]
  (.preventDefault ev)
  (.stopPropagation ev))

(defn- start-dragging
  [drag-state ev]
  (prevent-and-stop ev)
  (let [items (.. ev -dataTransfer -items)
        kinds (for [i (range (.-length items))]
                (.. (aget items i) -kind))]
    (when (= @drag-state :off)
      (log/debug {:event (.-type ev) :kinds kinds} "Drag detected"))
    (reset! drag-state (if (some #{"file"} kinds)
                         :on
                         :no-file-detected))))

(defn- stop-dragging
  [drag-state ev]
  (prevent-and-stop ev)
  (reset! drag-state :off))

(defn- receive-drop
  [drag-state mime-types ev]
  (stop-dragging drag-state ev)
  (let [files (.. ev -dataTransfer -files)
        image-file (->> (for [i (range (.-length files))]
                          (let [file (.item files i)]
                            (when (mime-types (.-type file))
                              file)))
                        (remove nil?)
                        first)]
    (log/debug {:event (.-type ev)
                :types (for [i (range (.-length files))]
                         (.-type (.item files i)))
                :image-file (.-type image-file)}
               "Received drop")
    (dispatch [::forms/edit form :file image-file])))

(defn submit-post-panel
  "Create the page to submit a new post."
  []
  (let [mime-types @(subscribe [::settings/upload-post-mime-types])
        drag-state (reagent/atom :off)
        drag-targeting-on (partial start-dragging drag-state)
        drag-targeting-off (partial stop-dragging drag-state)
        drag-drop (partial receive-drop drag-state mime-types)]
    (fn []
      (let [is-authenticated? @(subscribe [::user/is-authenticated?])
            sub @(subscribe[::subs/normalized-sub-name])
            loading? @(subscribe [::loader/content-loading?])
            form @(subscribe [::forms/form ::sub-forms/submit-post])
            post-type (:selected-post-type form)
            filename @(subscribe [::sub-forms/upload-filename])]
        (cond
          (not is-authenticated?)
          [misc-panels/unauthenticated-panel]

          (and loading? (nil? form))
          [misc-panels/waiting-panel]

          (nil? sub)
          [misc-panels/page-not-found-panel]

          :else
          [panel (merge
                  {:sidebar :<>}
                  (when (and (= :UPLOAD post-type) (nil? filename))
                    {:on-drag-over  drag-targeting-on
                     :on-drag-enter drag-targeting-on
                     :on-drag       prevent-and-stop
                     :on-drag-start prevent-and-stop
                     :on-drag-end   drag-targeting-off
                     :on-drag-leave drag-targeting-off
                     :on-drop       drag-drop
                     :on-paste      (when (= :UPLOAD post-type)
                                      (partial on-paste mime-types))}))
           [:div
            [:div.w-90.mw-90-s.mw7-ns.ph3.pv2
             [:h2 (tr "Submit a new post")]
             [submit-post-form {:drag-state drag-state}]
             [markdown-link-modal]]]])))))
