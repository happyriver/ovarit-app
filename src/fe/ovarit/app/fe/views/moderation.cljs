;; fe/views/moderation.cljs -- Moderation ui for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.moderation
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.moderation :as moderation]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.moderation :as mod-forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [helper-text table-cell]]
   [ovarit.app.fe.views.errors :refer [error-bar]]
   [ovarit.app.fe.views.forms :refer [form-error
                                      preview
                                      transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button checkbox
                                       markdown-editor markdown-link-modal
                                       radio-button text-input]]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.movable-list :refer [movable-list]]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.sidebar :refer [button-link]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame.core :refer [dispatch subscribe]]))

(defn mod-sidebar
  "Create the sidebar for the mod dashboard."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        is-admin? @(subscribe [::user/is-admin?])
        {:keys [show-other-circles?]} @(subscribe [:view/options])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     (when is-admin?
       [:<>
        [button-link {:href (url-for :mod/index)
                      :button-class (when-not show-other-circles?
                                      :sidebar)}
         (tr "My Circles")]
        [button-link {:href (url-for :mod/admin)
                      :button-class (when show-other-circles?
                                      :sidebar)}
         (tr "Other Circles")]
        [:hr]])
     [button-link {:href (url-for :mod/reports)}
      (tr "All Open Reports")]
     [button-link {:href (url-for :mod/closed)}
      (tr "All Closed Reports")]
     [button-link {:href (url-for :modmail/mailbox :mailbox :all)}
      (tr "All Mod Mail")]]))

(defn sidebar-button
  "Create one of the buttons on the sidebar menu.  If `panel` matches the current
  view, highlight the button."
  [panel url text]
  (let [active-panel @(subscribe [:view/active-panel])
        class ["sbm-post" "pure-button" (when (= panel active-panel)
                                          "button-secondary")]]
    [:a {:href url :class class} text]))

(defn mod-sub-sidebar
  "Create the sidebar for mod pages within a sub."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        sub @(subscribe [::subs/normalized-sub-name])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     [sidebar-button false (url-for :sub/edit-sub :sub sub)
      (tr "Settings")]
     [sidebar-button :mod-edit-post-flairs (url-for :sub/edit-sub-flairs :sub sub)
      (tr "Post flairs")]
     [sidebar-button false (url-for :sub/edit-sub-user-flairs :sub sub)
      (tr "User flairs")]
     [sidebar-button :mod-edit-sub-rules (url-for :sub/edit-sub-rules :sub sub)
      (tr "Rules")]
     [sidebar-button :mod-edit-post-types (url-for :sub/edit-post-types :sub sub)
      (tr "Post types")]
     [:hr]
     [sidebar-button false (url-for :sub/edit-sub-mods :sub sub)
      (tr "Moderators")]
     [sidebar-button false (url-for :sub/view-sub-bans :sub sub)
      (tr "Bans")]
     [:hr]
     [sidebar-button false (url-for :mod/reports-sub :sub sub)
      (tr "Open Reports")]
     [sidebar-button false (url-for :mod/reports-sub-closed :sub sub)
      (tr "Closed Reports")]]))

(defn subscriber-count-row
  "Create a table row for the sub subscriber count."
  [{:keys [subscriber-count]}]
  [:tr
   [table-cell (tr "Subscribers")]
   [table-cell subscriber-count]])

(defn mod-report-count-rows
  "Create table rows with report counts for mods."
  [name {:keys [open-report-count closed-report-count]}]
  (let [todo-highlight-color @(subscribe [::window/todo-highlight-color])]
    [:<>
     [:tr {:class (cls (when (pos? open-report-count)
                         todo-highlight-color))}
      [table-cell
       [:a {:href (url-for :mod/reports-sub :sub name)} (tr "Open Reports")]]
      [table-cell open-report-count]]
     [:tr
      [table-cell
       [:a {:href (url-for :mod/reports-sub-closed :sub name)}
        (tr "Closed Reports")]]
      [table-cell closed-report-count]]]))

(defn admin-report-count-rows
  "Create table rows with report counts for admins."
  [_ {:keys [open-report-count closed-report-count]}]
  (let [todo-highlight-color @(subscribe [::window/todo-highlight-color])]
    [:<>
     [:tr {:class (cls (when (pos? open-report-count)
                         todo-highlight-color))}
      [table-cell (tr "Open Reports")]
      [table-cell open-report-count]]
     [:tr
      [table-cell (tr "Closed Reports")]
      [table-cell closed-report-count]]]))

(defn mod-modmail-count-rows
  "Create the modmail count table rows for mods."
  [name {:keys [all-modmail-count unread-modmail-count]}]
  (let [todo-highlight-color @(subscribe [::window/todo-highlight-color])]
    [:<>
     [:tr
      [table-cell [:a {:href (url-for :modmail/mailbox :mailbox :all
                                      :query-args {:sub name})}
                   (tr "All Mod Mails")]]
      [table-cell all-modmail-count]]
     [:tr {:class (cls (when (pos? unread-modmail-count)
                         todo-highlight-color))}
      [table-cell [:a {:href (url-for :modmail/mailbox :mailbox :all
                                      :query-args {:sub name})}
                   (tr "Unread Mod Mails")]]
      [table-cell unread-modmail-count]]]))

(defn admin-new-modmail-count-row
  "Create a table row with the new modmail count."
  [name {:keys [new-modmail-count]}]
  (let [todo-highlight-color @(subscribe [::window/todo-highlight-color])]
    [:tr {:class (cls (when (pos? new-modmail-count)
                        todo-highlight-color))}
     [table-cell [:a {:href (url-for :modmail/mailbox :mailbox :new
                                     :query-args {:sub name})}
                  (tr "New Mod Mails")]]
     [table-cell new-modmail-count]]))

(defn sub-dashboard
  "Create the stats box for one sub."
  [{:keys [name] :as stats}]
  (let [subs @(subscribe [::user/subs-moderated-names])
        is-mod? (some #{name} subs)
        border-color @(subscribe [::window/table-border-colors])]
    [:div.ma2
     [:a {:href (url-for :sub/view-sub :sub name)}
      [:h3 name]]
     [:table  {:class (cls :collapse.ba.bw1 border-color)}
      [:tbody
       [subscriber-count-row stats]
       (if is-mod?
         [:<>
          [mod-report-count-rows name stats]
          [mod-modmail-count-rows name stats]]
         [:<>
          [admin-report-count-rows name stats]
          [admin-new-modmail-count-row name stats]])]]]))

(defn show-other-circles-option-for-admins-only
  "Create the appropriate panel for the admin show other circles option."
  [panel]
  (let [is-admin? @(subscribe [::user/is-admin?])
        {:keys [show-other-circles?]} @(subscribe [:view/options])]
    (if (and show-other-circles? (not is-admin?))
      [misc-panels/unauthorized-panel]
      panel)))

(defn mod-dashboard
  "Create the moderator dashboard."
  []
  (let [subs @(subscribe [::moderation/moderated-sub-stats])
        {:keys [show-other-circles?]} @(subscribe [:view/options])
        more? @(subscribe [::moderation/more-moderated-sub-stats-available?])
        options @(subscribe [:view/options])
        cursor @(subscribe [::moderation/moderated-sub-stats-cursor])
        load-more [::moderation/load-admin-mod-stats
                   (assoc options :cursor cursor)]]
    [:<>
     [:h2 {:style {:display "inline-block"}}
      (tr "Mod dashboard")]
     (when show-other-circles?
       [helper-text (tr "Showing the circles you do not moderate.")])
     [:div.admin.section
      [:div.col-12.admin-page-form
       [:div.admin.section.stats
        (into [:div.flex.flex-wrap.flex-start.items-center.mv0.mw9]
              (map sub-dashboard subs))
        (when more?
          [button {:class :dib
                   :button-class :secondary
                   :on-click #(dispatch load-more)}
           (tr "Load more")])]]]]))

(defn mod-dashboard-panel
  "Create a page to show the moderator dashboard, for mods and admins only."
  []
  [show-other-circles-option-for-admins-only
   [panel {:sidebar mod-sidebar}
    [mod-dashboard]]])

(defn mod-of-sub-or-admin-only
  "Create the appropriate moderation panel depending on the selected sub."
  [panel]
  (let [sub @(subscribe[::subs/normalized-sub-name])
        subs-moderated (set @(subscribe [::user/subs-moderated-names]))
        is-admin? @(subscribe [::user/is-admin?])
        sub-load-status @(subscribe [::subs/sub-load-status])
        content-status @(subscribe [::window/status])
        loading? (and @(subscribe [::loader/content-loader])
                      @(subscribe [::loader/content-loading?]))
        sub-names (set @(subscribe [::subs/all-sub-names]))]
    (cond
      (or (= sub-load-status :loading) (= content-status :loading) loading?)
      [misc-panels/waiting-panel]

      (nil? sub)
      [misc-panels/page-not-found-panel]

      (subs-moderated sub)
      panel

      (not is-admin?)
      [misc-panels/unauthorized-panel]

      (sub-names sub)
      panel

      :else
      [misc-panels/page-not-found-panel])))

(defn create-rule-form
  "Create a form to create a new sub rule."
  []
  (let [form ::mod-forms/create-sub-rule
        rule (fn [] @(subscribe [::forms/field form :rule]))
        create #(dispatch [::forms/action form :create])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [text-input {:value rule
                      :class (cls :w-100.mb2)
                      :placeholder (tr "Rule text")
                      :required true
                      :form-state form-state
                      :disabled editing-disabled?
                      :update-value! forms/reset-input-on-success!
                      :event [::forms/edit form :rule]}] " "
         [button {:class :dib
                  :button-class :primary
                  :disabled editing-disabled?
                  :on-click create}
          (if (= form-state :SENDING)
            (tr "Adding...")
            (tr "Add rule"))] " "
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn warning-for-admins
  "Create a warning messages for admins for subs they don't mod."
  []
  (let [sub @(subscribe [::subs/normalized-sub-name])
        subs-moderated (set @(subscribe [::user/subs-moderated-names]))
        is-admin? @(subscribe [::user/is-admin?])]
    (when (and is-admin? (not (subs-moderated sub)))
      [:div.important
       [:h3 (tr "Editing as Admin")]])))

(defn warning-for-admins-and-error-bar
  "Create both the admin moderation warning message and an error bar."
  []
  [:<>
   [warning-for-admins]
   [error-bar]])

(defn sub-rule
  "Create a rule for the list of sub rules."
  [{:keys [selected?] :as props} {:keys [id text]}]
  [(if selected? :p :li) (dissoc props :selected?)
   [:span.postrule {:data-movable-list-handle true} text] " "
   (when-not selected?
     [:div.st-flair [button {:class [util/reset-padding :f7.pv1.ph2]
                             :button-class :secondary
                             :on-click #(dispatch
                                         [::moderation/delete-sub-rule id])}
                     (tr "Delete")]])])

(defn render-rule-list
  "Create the list of sub rules."
  [props & children]
  (into [:ul props] children))

(defn mod-edit-rules
  "Create the sub rules and the form to edit them."
  []
  (let [sub @(subscribe [::subs/normalized-sub-name])
        sublink [:a {:href (url-for :sub/view-sub :sub sub)} sub]
        rules @(subscribe [::subs/rules])]
    [:div.mw7
     [:h2
      (trm-html "Edit circle rules for %{sub}" {:sub sublink})]
     [:h4
      (tr "Create new rule")]
     [helper-text
      (tr "Users will be able to report violations of this rule. The
           rule should be short and easy to understand.")]
     [helper-text
      (trm-html
       "You should include a more detailed version of your rules in
        the circle sidebar text on the %{settings} page, and you
        can also set up rules for each post type on the
        %{types} page."
       {:settings [:a {:href (url-for :sub/edit-sub :sub sub)}
                   (tr "Settings")]
        :types [:a {:href (url-for :sub/edit-post-types :sub sub)}
                (tr "Post types")]})]
     [create-rule-form]
     [:hr]
     [:h4 (tr "Rules")]
     [helper-text (tr "You may drag and drop the rules to change their order.")]
     [:div.col-12.admin-page-form
      [movable-list {:values rules
                     :change-event [::moderation/reorder-rules]
                     :list-comp render-rule-list
                     :item-comp sub-rule}]]]))

(defn mod-edit-rules-panel
  "Create the panel for showing and editing sub rules."
  []
  [mod-of-sub-or-admin-only
   [panel {:sidebar mod-sub-sidebar
           :error-bar warning-for-admins-and-error-bar}
    [mod-edit-rules]]])

(defn sub-flair-post-type-checkbox
  [{:keys [id post-type label disabled]}]
  (let [permitted @(subscribe [::mod-forms/post-type-permitted?
                               id post-type])]
    [checkbox {:id (str "sub-flair-" id "-"
                        (some-> post-type name str/lower-case))
               :class :mh3
               :label-class :db
               :value permitted
               :disabled disabled
               :event [::mod-forms/permit-flair-post-type id post-type]
               :label label}]))

(defn sub-flair-options
  "Create a form to edit the options for a flair."
  [id]
  (let [form-id (mod-forms/edit-post-flair-form-id id)
        save #(do (.preventDefault %)
                  (dispatch [::forms/action form-id :save]))
        cancel #(do (.preventDefault %)
                    (dispatch [::moderation/hide-post-flair-options id]))
        delete #(do (.preventDefault %)
                    (dispatch [::forms/action form-id :delete]))]
    (fn [id]
      (let [border @(subscribe [::window/table-border-colors])
            button-class [util/reset-padding :dib.f7.mt1.pv1.ph2]
            editing-disabled? @(subscribe [::forms/editing-disabled? form-id])
            form-state @(subscribe [::forms/form-state form-id])
            {main-state   :main
             delete-state :delete} form-state
            mods-only @(subscribe [::forms/field form-id :mods-only])]
        [:form {:class (cls :dib.mb2.pa2.ba.br1.bw1 border)}
         [checkbox {:id (str "sub-flair" id)
                    :class :mh3
                    :label-class :db
                    :value mods-only
                    :event [::forms/edit form-id :mods-only]
                    :disabled editing-disabled?
                    :label "Mods only"}]
         (when-not mods-only
           [:<>
            [sub-flair-post-type-checkbox {:id id
                                           :post-type :TEXT
                                           :disabled editing-disabled?
                                           :label "Text posts"}]
            [sub-flair-post-type-checkbox {:id id
                                           :post-type :LINK
                                           :disabled editing-disabled?
                                           :label "Link posts"}]
            [sub-flair-post-type-checkbox {:id id
                                           :post-type :UPLOAD
                                           :disabled editing-disabled?
                                           :label "Upload posts"}]
            [sub-flair-post-type-checkbox {:id id
                                           :post-type :POLL
                                           :disabled editing-disabled?
                                           :label "Polls"}]])
         [button {:class (conj button-class :mr2)
                  :button-class :primary
                  :disabled editing-disabled?
                  :on-click save}
          (cond
            (= :SHOW-SUCCESS main-state) (tr "Saved")
            (= :SENDING main-state) (tr "Saving")
            :else  (tr "Save"))]
         [button {:class (conj button-class :mr2)
                  :button-class :secondary
                  :on-click cancel}
          (if (= :SHOW-SUCCESS main-state)
            (tr "Close")
            (tr "Cancel"))]
         [button {:class button-class
                  :button-class :secondary
                  :disabled editing-disabled?
                  :on-click delete}
          (if (= :SENDING delete-state)
            (tr "Deleting")
            (tr "Delete"))]]))))

(defn sub-flair
  "Create a flair for the list of sub post flairs."
  [{:keys [selected?] :as props} {:keys [id text]}]
  (let [{:keys [options-open?]} @(subscribe
                                  [::moderation/post-flairs-ui-state id])
        flair-colors @(subscribe [::window/post-flair-colors
                                  {:selectable? true}])
        form-id (mod-forms/edit-post-flair-form-id id)]
    [(if selected? :span :li) (dissoc props :selected?)
     [:div.flex.items-start
      [:span {:class (cls :ph2.pv1.mb2.mr2.br2.f5.pointer flair-colors)
              :data-movable-list-handle true} text]
      (when-not selected?
        [:<>
         (if options-open?
           [sub-flair-options id]
           [button
            {:class [util/reset-padding :dib.mt1.mr2.f7.pv1.ph2]
             :button-class :secondary
             :on-click #(dispatch
                         [::moderation/show-post-flair-options id])}
            (tr "Edit")])])]
     (when (and options-open? (not selected?))
       [:<>
        [validation-error-if-present form-id]
        [transmission-error-if-present form-id]])]))

(defn render-flair-list
  "Create the list of sub flairs."
  [props & children]
  (into [:ul.no-bullets props] children))

(defn create-post-flair-form
  "Create a form to create a new sub post flair."
  []
  (let [form ::mod-forms/create-post-flair
        flair (fn [] @(subscribe [::forms/field form :flair]))
        create #(dispatch [::forms/action form :create])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [text-input {:value flair
                      :placeholder (tr "Flair text")
                      :required true
                      :form-state form-state
                      :disabled editing-disabled?
                      :update-value! forms/reset-input-on-success!
                      :class :mb2
                      :event [::forms/edit form :flair]}] " "
         [button {:class :db
                  :button-class :primary
                  :disabled editing-disabled?
                  :on-click create}
          (if (= form-state :SENDING)
            (tr "Adding...")
            (tr "Add flair"))] " "
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn mod-edit-post-flairs
  "Create a page to show sub flairs and allow editing them."
  []
  (let [sub @(subscribe [::subs/normalized-sub-name])
        sublink [:a {:href (url-for :sub/view-sub :sub sub)} sub]
        flairs @(subscribe [::subs/post-flairs])]
    [:<>
     [:h2 {:style {:display "inline-block"}}
      (trm-html "Edit post flairs for %{sub}" {:sub sublink})]
     [:h4
      (tr "Create new flair")]
     [create-post-flair-form]
     [:hr]
     [helper-text
      (tr "You may drag and drop the flairs to change the order.")]
     [:div.admin.section
      [:div.col-12.admin-page-form
       [movable-list {:values flairs
                      :change-event [::moderation/reorder-post-flairs]
                      :list-comp render-flair-list
                      :item-comp sub-flair}]]]]))

(defn mod-edit-post-flairs-panel
  "Create the panel for editing sub flairs."
  []
  [mod-of-sub-or-admin-only
   [panel {:sidebar mod-sub-sidebar
           :error-bar warning-for-admins-and-error-bar}
    [mod-edit-post-flairs]]])

(defn mods-only-radio-buttons
  "Create radio buttons for post type permissions."
  [{:keys [post-type form]}]
  (let [mods-only? @(subscribe [::forms/field form :mods-only?])]
    [:<>
     [radio-button
      {:id (str "mods-only-" (-> post-type name str/lower-case))
       :class (cls :dib.mr3)
       :label (tr "Mods only")
       :value true
       :form-value mods-only?
       :event [::forms/edit form :mods-only?]}]
     [radio-button
      {:id (str "everyone-" (-> post-type name str/lower-case))
       :class (cls :dib)
       :label (tr "Everyone")
       :value false
       :form-value mods-only?
       :event [::forms/edit form :mods-only?]}]]))

(defn rules-editor
  "Create a markdown editor to edit post type rules."
  [{:keys [form option]}]
  (let [rules (fn [] @(subscribe [::forms/field form :rules]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])
            sub (:sub @(subscribe [:view/options]))]
        [:<>
         [:h4 (tr "Rules:")]
         [:div.mb2
          [markdown-editor
           {:value rules
            :buttonbar-class :mw7.mt2
            :class :w-100.mw7
            :update-value! forms/reset-input-on-success!
            :form-state form-state
            :disabled editing-disabled?
            :event [::forms/edit form :rules]}]
          [preview {:form form}
           (tr "Rules preview")]]
         [helper-text
          (trm-html "This description of the rules will be shown on the
                     %{submitpost} page when %{option}"
                    {:submitpost [:a {:href (url-for :subs/submit-post
                                                     :query-args {:sub sub})}
                                  "Submit a post"]
                     :option option})]]))))

(defn edit-post-type
  "Create a section for one post type."
  [{:keys [post-type]}]
  (let [form @(subscribe [::mod-forms/edit-post-type-form-id post-type])
        form-action (fn [action]
                      #(do (.preventDefault %)
                           (dispatch [::forms/action form action])))
        save (form-action :save)
        see-preview (form-action :see-preview)]
    (fn [{:keys [label option]}]
      (let [form-state @(subscribe [::forms/form-state form])]
        [:<>
         [:span.gr-align-start.b label]
         [:form.mb3
          [mods-only-radio-buttons {:form form
                                    :post-type post-type}]
          [rules-editor {:form form
                         :option option}]
          [validation-error-if-present form]
          [transmission-error-if-present form]
          [button {:class :dib
                   :on-click save
                   :button-class :primary}
           (cond
             (= form-state :SENDING) (tr "Saving")
             (= form-state :SHOW-SUCCESS) (tr "Saved!")
             :else (tr "Save changes"))]
          [button {:class :ml2.dib
                   :on-click see-preview
                   :button-class :secondary}
           (tr "Preview")]]]))))

(defn mod-edit-post-types
  "Create a page to show sub types and allow editing them."
  []
  (let [sub @(subscribe [::subs/normalized-sub-name])
        sublink [:a {:href (url-for :sub/view-sub :sub sub)} sub]]
    [:<>
     [:h2 {:style {:display "inline-block"}}
      (trm-html "Edit post types in %{sub}" {:sub sublink})]
     [:div.mw7.gr-auth-form.mv2
      [edit-post-type {:post-type :TEXT
                       :label (tr "Text posts:")
                       :option (tr "the text post option is selected.")}]
      [edit-post-type {:post-type :LINK
                       :label (tr "Link posts:")
                       :option (tr "the link post option is selected.")}]
      [edit-post-type {:post-type :UPLOAD
                       :label (tr "Upload posts:")
                       :option (tr "the upload post option is selected.")}]
      [edit-post-type {:post-type :POLL
                       :label (tr "Polls:")
                       :option (tr "the poll option is selected.")}]
      [markdown-link-modal]]]))

;; TODO only LEAD_MODs can change things, are these shown
;; to all mods and they get errors? or what?
(defn mod-edit-post-types-panel
  "Create the panel for editing a sub's post type rules."
  []
  [mod-of-sub-or-admin-only
   [panel {:sidebar mod-sub-sidebar
           :error-bar warning-for-admins-and-error-bar}
    [mod-edit-post-types]]])
