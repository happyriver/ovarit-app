;; fe/views/errors.cljs -- Display errors for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.errors
  (:require
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.common :refer [relative-time]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.util :refer [cls]]
   [re-frame.core :refer [dispatch subscribe]]))

(defn error-bar
  "Show network or server errors."
  []
  (let [errors @(subscribe [::errors/errors])
        site-name @(subscribe [::settings/name])
        icons @(subscribe [::settings/icons])
        icon-href (get icons "16x16")
        colors @(subscribe [::window/toast-colors])
        border-color @(subscribe [::window/toast-border-color])
        close-color @(subscribe [::window/toast-close-color])]
    (when (seq errors)
      (into [:div.di.fixed.top-2.z-999
             ;; Put toasts on the left in dev mode so devtools can
             ;; have the right side.
             {:class (cls (if config/debug? :left-2 :right-2))}]
            (map (fn [{:keys [msg iso-time] :as err}]
                   [:div.mw5.mw6-ns.mb3.bw1.br2.b--black.shadow-1
                    {:class (cls colors)}
                    [:div.pa2.bb.flex.items-center
                     {:class (cls border-color)}
                     (when icon-href
                       [:img {:src icon-href
                              :class (cls :mr2)}])
                     [:div.w-100.flex.mt2
                      [:span.f5.b.mr-auto site-name]
                      [:span.f7.gray.ml3.mt1.pr2
                       [relative-time {:datetime iso-time}]]]
                     [icon {:class (cls :h1.w1.pointer.dib.self-start
                                        close-color)
                            :on-click #(dispatch [::errors/clear-error err])}
                      "close"]]
                    [:div.ph3.pv2
                     msg]])
                 errors)))))
