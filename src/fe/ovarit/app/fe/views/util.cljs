;; fe/views/util.cljs -- Page rendering utility functions for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.util
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes.util :as route-util]
   [ovarit.app.fe.tr :refer [tr]]
   [re-frame.core :as re-frame]
   [reagent.core :as reagent]))

(defn re?
  "Return true if the argument is a regular expression."
  [arg]
  (= (type arg) (type #"")))

(defn regex-filter
  "Given a list of strings and regular expressions,
  remove the regular expressions. For each regular
  expression, if there are matches to it before it in the
  list, remove those too."
  [elems]
  (loop [result []
         remaining elems]
    (if (seq remaining)
      (let [elem (first remaining)
            update (if (re? elem)
                     (filterv #(nil? (re-matches elem %)) result)
                     (conj result elem))]
        (recur update (rest remaining)))
      result)))

(comment
  (re? "a")
  (re? #"a")
  (regex-filter ["a" "b" "c"])
  (regex-filter ["pa2" "mv1" "dn" #"pa." "pa1"]))

(defn cls
  "Make its arguments into a list of classes.
  Accepts strings, lists of strings and keywords.  Keywords are
  converted to strings and split on '.'s. If any regular expressions
  are included, remove them and all matches to them that occur before
  them in the arguments. Return a single list of strings."
  [& things]
  (letfn [(listify [thing]
            (cond
              (keyword? thing) (-> thing
                                   name
                                   (str/split #"\."))
              (string? thing) [thing]
              (re? thing) [thing]
              (seq thing) (unpack thing)
              :else thing))
          (unpack [things]
            (->> things
                (map listify)
                (filter seq)
                (apply concat)))]
    (->> things
         unpack
         regex-filter)))

(def reset-padding
  "Regular expression to remove tachyon CSS padding applied earlier in
  the arguments to `cls`."
  #"^p[ahvtblr][0-9]$")

(comment
  (cls :foo.bar.baz ["ba1" "ba2"] [:zip.zop] "hello" nil)
  (cls :pa2.mv1.baz #"pa.")
  (cls :ph3.pv2.baz reset-padding)
  (cls :db.pv2.ph3.br1.bn reset-padding :f7.pv1.ph2))

(defn error-boundary
  "Create a wrapper around a component which could fail."
  [_comp]
  (let [error (reagent/atom nil)]
    (reagent/create-class
     {:component-did-catch (fn [_this e info]
                             (log/error {:exception e :info info }
                                        "View error"))
      :get-derived-state-from-error (fn [e]
                                      (reset! error e)
                                      #js {})
      :reagent-render (fn [comp]
                        (if @error
                          [:div
                           [:p.error (tr "Something went wrong")]
                           [:button
                            {:on-click #(reset! error nil)
                             :class (cls :hover-bg-moon-gray
                                         :db.ba.bw1.b--silver
                                         :pv2.ph3.br1.bn.pointer)}
                            (tr "Try again")]]
                          comp))})))

(defn url-for
  [handler & args]
  @(re-frame/subscribe (into [::route-util/url-for handler] args)))

(def formatter (js/Intl.DateTimeFormat. js/undefined
                                        (clj->js {"dateStyle" "long"})))

(defn format-date
  [iso-datetime]
  (.format formatter (js/Date. iso-datetime)))
