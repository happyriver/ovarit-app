;; fe/views/post-listing.cljs -- Post listing panel for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.post-listing
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.content.posts :as posts]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [relative-time userlink]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.util :refer [url-for]]
   [re-frame.core :refer [subscribe]]))

(defn expando
  "Create the expando button."
  [title _pid icon-name]
  [:div.expando {:title title}
   [icon {:class ["icon" "expando-btn"]} icon-name]])

(defn expando-maybe
  "Create the expando button depending on the post type and content."
  [{:keys [link type]
    :as post}]
  [:<>
   (cond
     (and link false) ;; if link domain in config.site.expando_sites
     [expando (tr "Embed video") post "play"]

     (and link
          (some #(str/ends-with? link %)
                [".png" ".jpg" ".gif" ".tiff" ".bmp" ".jpeg"]))
     [expando (tr "Show image") post "image"]

     (and link
          (some #(str/ends-with? link %)
                [".mp4" ".webm" ".gifv"]))
     [expando (tr "Play video") post "play"]

     (= type :POLL)
     [expando (tr "Show poll") post "text"]

     (= type :TEXT)
     [expando (tr "Show post content") post "text"])])

(defn vote-buttons
  "Create the vote buttons for a post."
  [{:keys [vote score is-archived author]}]
  [:div.votebuttons
   (if (or is-archived (= (:status author) :DELETED))
     [:div.score.archived score]
     [:span
      [:div {:title (tr "Upvote")}
       [icon {:class ["upvote" (when (= vote :UP) "upvoted")]} "upvote"]]
      [:div.score score]
      [:div {:title (tr "Downvote")}
       [icon {:class ["downvote" (when (= vote :DOWN) "downvoted")]}
        "downvote"]]])])

(defn thumbnail-link
  "Create the thumbnail link for a post."
  [{:keys [link thumbnail sub pid]}]
  [:div.thcontainer
   [:a
    (if link
      {:target "_blank" :rel "noopener nofollow ugc" :href link}
      {:href (url-for :sub/view-single-post :sub (:name sub) :pid pid
                      :slug "slug")})
    [:div.thumbnail
     (if link
       (if (and thumbnail (not= thumbnail ""))
         [:img {:src (str "https://s3-us-west-2.amazonaws.com/throat-test-all/"
                          thumbnail)}]
         [icon {:class "placeholder"} "link"])
       [icon {:class "placeholder"} "chat"])]]])

(defn post-heading
  "Create the heading for a post in a post list."
  [{:keys [announcement? type nsfw flair link sub pid title domain]}]
  [:div.post-heading
   (when announcement?
     [:span.announcement (tr "Announcement")])
   (when (= type :POLL)
     [:span.pollflair (tr "Poll")])
   (when nsfw
     [:div.nsfw {:alt (tr "Not safe for work")} (tr "NSFW")])
   (when flair
     [:span.postflair flair])
   (if (nil? link)
     [:a.title {:href (url-for :sub/view-single-post :sub (:name sub)
                               :pid pid :slug "slug")} title]
     [:span
      (when (str/starts-with? link "http:")
        [icon {:title (tr "not https") :class "p-icon"} "exclaim"])
      [:a.title {:target "_blank" :rel "noopener nofollow ugc"
                 :href link} title]
      [:a.domain {:href (url-for :home/all-domain-new :domain domain)}
       (str " (" domain ")")]])
   " "])

(defn post-author
  "Create the post author information (who, where, when)."
  [{:keys [author posted sub pid distinguish] :as post}]
  (let [source-sub @(subscribe [:view/post-source-sub])
        authorlink (if (= (:status author) :DELETED)
                     [:a.authorlink.deleted (tr "Deleted")]
                     [userlink {:class :authorlink}
                      (:name author)
                      (case distinguish
                        :MOD (tr " [speaking as mod]")
                        :ADMIN (tr " [speaking as admin]")
                        "")])
        sublink [:a {:href (url-for :sub/view-sub :sub (:name sub))}
                 (:name sub)]]
    [:div.author
     [expando-maybe post]
     (if source-sub
       (trm-html "posted %{date} by %{author}"
                 {:date [relative-time {:datetime posted}]
                  :author authorlink})
       (trm-html "posted %{date} by %{author} on %{sub}"
                 {:date [relative-time {:datetime posted}]
                  :author authorlink
                  :sub sublink}))

     (when (and source-sub (contains? (:sticky-pids source-sub) pid))
       [:span "-" [:span.stick (tr "sticky")]])
     (when (and source-sub (contains? (:wiki-pids source-sub) pid))
       [:span "-" [:span.stick (tr "wiki")]])]))

(defn post-links
  "Create the comment, block and report links for a post in the post
  list."
  [{:keys [sub pid author comment-count announcement? open-report-id
           open-report-count]}]
  (let [source-sub @(subscribe [:view/post-source-sub])
        current-user @(subscribe [::user/current-user])]
    [:div.links
     [:a.comments {:href (url-for :sub/view-post :sub (:name sub)
                                  :pid pid :slug "slug")}
      (if (> 0 comment-count)
        (tr "comments (%s)" comment-count)
        (tr "comment"))]
     [:span " "]
     (when (and (not source-sub) (not announcement?))
       [:a.unblk (tr "block sub")])
     [:span " "]
     (when (not= (:uid current-user) (:uid author))
       [:a.report-post (tr "report")])
     (when open-report-id
       [:li
        [:a.post-open-reports
         {:href (url-for :mod/report-details :sub (:name sub)
                         :type "post" :id open-report-id)}
         (tr "open reports (%s)" open-report-count)]])]))

(defn post-list-entry
  "Create one post in a list of posts."
  [post]
  [:div {:class ["post" (case (:distinguish post)
                          :MOD "mod"
                          :ADMIN "admin"
                          nil)]}
   [:div.misctainer
    [vote-buttons post]
    [thumbnail-link post]]
   [:div.pbody
    [post-heading post]
    [post-author post]
    [post-links post]]])

(defn post-list []
  (let [posts @(subscribe [::posts/posts])
        status @(subscribe [::window/status])]
    (if (empty? posts)
      (if (not= status :loaded)
        [:h4.noshit (tr "Loading...")]
        [:h1.noshit (tr "There are no posts here, yet.")])
      (into [:div.alldaposts] (map post-list-entry posts)))))

(defn announcement []
  (let [announcement @(subscribe [::posts/announcement])]
    (when announcement
      [:div
       [post-list-entry announcement]
       [:hr]])))

(defn post-listing []
  [:div {:id "container"}
   [announcement]
   [post-list]])

(defn post-listing-panel []
  [panel {:footer :<>}
   [post-listing]])
