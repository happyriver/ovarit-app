;; fe/views/wiki.cljs -- Wiki page rendering for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.wiki
  (:require
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.common :refer [table-cell table-label-cell]]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.util :refer [cls]]
   [re-frame.core :refer [subscribe]]))

(defn software-row
  "Create a row of the software table on the licenses page."
  [{:keys [name license-name license-link source-code-link]}]
  (let [no-bottom-border {:class (cls :bb-0)}]
    [:tr
     [table-cell no-bottom-border
      name]
     [table-cell no-bottom-border
      [:a {:href license-link} license-name]]
     [table-cell no-bottom-border
      [:a {:href source-code-link} source-code-link]]]))

(defn licenses
  "Create the content for the licenses page."
  []
  (let [lema @(subscribe [::settings/lema])
        software-licenses @(subscribe [::settings/software-licenses])
        border-color @(subscribe [::window/table-border-colors])]
    [:div.pa3
     [:h1 lema]
     [:h3 (tr "This website is powered by:")]
     [:table {:class (cls :collapse.ba.bw1 border-color)}
      [:thead
       [:tr
        [table-label-cell (tr "Software")]
        [table-label-cell (tr "License")]
        [table-label-cell (tr "Source Code")]]]
      (into [:tbody] (map software-row software-licenses))]]))

(defn wiki-panel
  "Create a page that shows a wiki entry."
  []
  [panel {:sidebar :<>}
   [licenses]])
