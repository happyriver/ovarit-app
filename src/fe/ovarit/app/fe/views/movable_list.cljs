;; fe/views/movable_list.cljs -- Rearrangable lists for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.movable-list
  (:require
   ["react-dom" :as react-dom]
   [goog.object :refer [getValueByKeys]]
   [re-frame.core :refer [dispatch]]
   [reagent.core :as reagent]))

(defn get-translate-offset
  "Calculate the offset by which to adjust the position of a child element
  when it is dragged."
  [elem]
  (let [styles (.getComputedStyle js/window elem)
        margin-bottom (-> (getValueByKeys styles #js ["margin-bottom"])
                          js/parseInt)
        margin-top (-> (getValueByKeys styles #js ["margin-top"])
                       js/parseInt)
        rect (.getBoundingClientRect elem)]
    (+ (max margin-bottom margin-top) (.-height rect))))

(defn calc-translate-offsets
  "Given a map with element values, return a new map with the same keys
  and the calculated translate offsets."
  [children]
  (update-vals children get-translate-offset))

(defn calc-top-offsets
  "Given a map with element values, return a new map with the same keys
  and the top position of each element."
  [children]
  (update-vals children #(.-top (.getBoundingClientRect %))))

(defn coords
  "Get the x and y coordinates of an event."
  [event is-touch?]
  (if is-touch?
    (let [touch (aget (. event -touches) 0)]
      {:x (.-clientX touch) :y (.-clientY touch)})
    {:x (.-clientX event) :y (.-clientY event)}))

(defn is-touch-event?
  "Determine if an event is a touch event."
  [event]
  (or (and (some? (.. event -touches))
           (pos? (.. event -touches -length)))
      (and (some? (.. event -changedTouches))
           (pos? (.. event -changedTouches -length)))))

(defn find-after-index
  "Return the key of the first element in top-offsets which is above y."
  [top-offsets y]
  (when-not (empty? top-offsets)
    (let [diffs (update-vals top-offsets #(- % y))
          elems-after (->> diffs
                           (filter #(pos? (val %)))
                           (sort-by second))]
      (if (empty? elems-after)
        (count top-offsets)
        (key (first elems-after))))))

(defn prevent-default [event]
  (when (.-cancelable event)
    (.preventDefault event)))

(defn targeting-handle-if-present?
  "Check if the child element has a handle (indicated with the
  data-movable-list-handle attribute), and return true if either there
  is no handle, or if the targeted element is the handle element."
  [target child]
  (let [handle (.querySelector child "[data-movable-list-handle]")]
    (or (nil? handle)
        (.contains handle target))))

(defn movable-list
  "Create a movable list component."
  [{:keys [change-event]}]
  (let [transition-default "200ms"
        state (reagent/atom
               {:dragged-index nil      ; index of the child being dragged
                :after-index nil        ; position child has been dragged to
                :target {:x 0 :y 0}     ; mouse or touch down coords
                :scroll-speed 0         ; pixels to move per scroll movement
                :scroll-window? false   ; whether we need to scroll
                :initial-offset 0       ; scrolling offset
                :cursor-offset {:x 0 :y 0}
                :after-offset nil       ; amount to move elems below cursor
                :top-offsets nil        ; tops of child elements
                :translate-offsets nil  ; child pos adjusted for margins
                :transition nil})       ; CSS transition value

        listeners (reagent/atom [])
        add-listener (fn [name func opts]
                       (.addEventListener js/document name func opts)
                       (swap! listeners conj [name func]))
        remove-listeners (fn []
                           (doseq [[name func] @listeners]
                             (.removeEventListener js/document name func))
                           (reset! listeners []))

        refs (reagent/atom {:self nil
                            :children nil
                            :cursor nil})
        set-ref (fn [ref]
                  (swap! refs assoc :self ref)
                  (when (nil? ref)
                    (remove-listeners)))
        set-child-ref (fn [num child]
                        (if child
                          (do
                            (swap! refs update :children assoc num child)
                            (swap! state update :translate-offsets assoc num
                                   (get-translate-offset child)))
                          (swap! refs update :children dissoc num)))
        set-cursor-ref #(swap! refs assoc :cursor %)
        scroll-offset #(+ (.-pageYOffset js/window)
                          (if (:self @refs)
                            (.-scrollTop (:self @refs)) 0))
        get-target-index (fn [target]
                           (some (fn [[k v]]
                                   (when (or (= v target) (.contains v target))
                                     k))
                                 (:children @refs)))
        finish-drop #(let [{:keys [dragged-index after-index]} @state]
                       (when (not= dragged-index after-index)
                         (dispatch (into change-event [dragged-index
                                                       after-index]))
                         (swap! state assoc :transition nil))
                       (swap! state assoc
                              :dragged-index nil
                              :after-index nil))
        scroll (fn [y]
                 (let [rect (.getBoundingClientRect (:self @refs))
                       viewport-height (or (.-innerHeight js/window)
                                           (.. js/document
                                               -documentElement -clientHeight))
                       dy (- viewport-height y)
                       scroll-offset 200
                       scroll-ratio 15]
                   (cond
                     (and (> (.-bottom rect) viewport-height)
                          (< dy scroll-offset))
                     (swap! state assoc
                            :scroll-speed (int (/ (- scroll-offset dy)
                                                  scroll-ratio))
                            :scroll-window? true)

                     (and (neg? (.-top rect)) (< y scroll-offset))
                     (swap! state assoc
                            :scroll-speed (int (/ (- y scroll-offset)
                                                  scroll-ratio))
                            :scroll-window? true)

                     :else
                     (swap! state assoc
                            :scroll-speed 0 :scroll-window? false))))
        move (fn [{:keys [x y]}]
               (let [{:keys [dragged-index initial top-offsets]} @state
                     cursor (:cursor @refs)]
                 (when dragged-index
                   (swap! state assoc
                          :cursor-offset {:x (- x (:x initial))
                                          :y (- y (:y initial))}))
                 (scroll y)
                 (when cursor
                   (let [rect (.getBoundingClientRect cursor)
                         {:keys [translate-offsets initial-offset]} @state
                         after-offset (get translate-offsets dragged-index)
                         current-offset (scroll-offset)
                         dy (- current-offset initial-offset)
                         subtract-dy #(- % dy)]
                     ;; Adjust offsets if we scrolled.
                     (when-not (zero? dy)
                       (swap! state update :top-offsets
                              #(update-vals % subtract-dy))
                       (swap! state assoc :initial-offset current-offset))
                     (swap! state assoc
                            :after-offset after-offset
                            :after-index (find-after-index top-offsets
                                                           (.-top rect)))))))
        mouse-move (fn [event]
                     (prevent-default event)
                     (move (coords event false)))
        touch-move (fn [event]
                     (prevent-default event)
                     (move (coords event true)))
        mouse-or-touch-end (fn [event]
                             (prevent-default event)
                             (remove-listeners)
                             (finish-drop))
        add-listeners (fn [is-touch?]
                        (if is-touch?
                          (let [opts (clj->js {:passive false})]
                            (add-listener "touchend" mouse-or-touch-end opts)
                            (add-listener "touchmove" touch-move opts)
                            (add-listener "touchcancel" mouse-or-touch-end opts))
                          (do
                            (add-listener "mousemove" mouse-move {})
                            (add-listener "mouseup" mouse-or-touch-end {}))))
        start (fn [target index {:keys [x y]}]
                (let [rect (.getBoundingClientRect target)
                      styles (.getComputedStyle js/window target)
                      margin-left (-> (getValueByKeys styles #js ["margin-left"])
                                      js/parseInt)
                      margin-top (-> (getValueByKeys styles #js ["margin-top"])
                                     js/parseInt)
                      children (:children @refs)]
                  (swap! state assoc
                         :dragged-index index
                         :after-index index
                         :target {:x (- (.-left rect) margin-left)
                                  :y (- (.-top rect) margin-top)}
                         :target-height (.-height rect)
                         :target-width (.-width rect)
                         :initial {:x x :y y}
                         :initial-offset (scroll-offset)
                         :cursor-offset {:x 0 :y 0}
                         :top-offsets (calc-top-offsets children)
                         :translate-offsets (calc-translate-offsets children)
                         :last-offset (.-pageYOffset js/window)
                         :scroll-speed 0
                         :scroll-window? false
                         :transition transition-default)))

        mouse-or-touch-start
        (fn [event]
          (let
              [is-touch? (is-touch-event? event)
               target (. event -target)
               index (get-target-index target)
               target-child (get-in @refs [:children index])]
            (when (and (or is-touch? (zero? (.-button event)))
                       (targeting-handle-if-present? target target-child))
              (prevent-default event)
              (add-listeners is-touch?)
              (start target-child index
                     (coords event is-touch?)))))

        base-style {:user-select "none"
                    :box-sizing "border-box"
                    :position "relative"
                    :cursor "move"}
        transform-item (fn [num]
                         (let [{:keys [dragged-index after-index
                                       after-offset]} @state]
                           (when (and after-index after-offset dragged-index
                                      (not= dragged-index after-index))
                             (cond
                               (and (< dragged-index after-index)
                                    (> num dragged-index)
                                    (<= num after-index))
                               (str "translate(0px," (- after-offset) "px)")

                               (and (< num dragged-index)
                                    (> dragged-index after-index)
                                    (>= num after-index))
                               (str "translate(0px," after-offset "px)")))))
        cursor-style (fn []
                       (let [{:keys [target cursor-offset]} @state]
                         (assoc base-style
                                :top  (+ (:y target) (:y cursor-offset))
                                :left (+ (:x target) (:x cursor-offset))
                                :width (:target-width @state)
                                :height (:target-height @state)
                                :position "fixed"
                                :margin-top 0)))]
    (reagent/create-class
     {:display-name "movable-list"
      :component-did-update
      (fn [_]
        (let [{:keys [scroll-speed scroll-window?]} @state]
          (when scroll-window?
            (.scrollTo js/window
                       (.-pageXOffset js/window)
                       (+ (.-pageYOffset js/window) scroll-speed)))))
      :reagent-render
      (fn [{:keys [values list-comp item-comp]}]
        (let [{:keys [dragged-index transition]} @state]
          [:<>
           (into [list-comp {:ref set-ref
                             :on-mouse-down mouse-or-touch-start
                             :on-touch-start mouse-or-touch-start}]
                 (map-indexed
                  (fn [num value]
                    [item-comp
                     {:style (assoc base-style
                                    :visibility (when (= num dragged-index)
                                                  "hidden")
                                    :transform (transform-item num)
                                    :transition
                                    (when (and dragged-index
                                               (not= num dragged-index))
                                      transition))
                      :data-key num
                      :ref #(set-child-ref num %)}
                     value])
                  values))
           (when dragged-index
             (react-dom/createPortal
              (reagent/as-element
               [item-comp {:style (cursor-style)
                           :selected? true
                           :ref set-cursor-ref}
                (nth values dragged-index)])
              (.-body js/document)))
           (comment (into [:div]
                          (map (fn [[k v]]
                                 ^{:key k} [:p k ":" (str v)]) @state)))]))})))
