;; fe/views/auth.cljs -- Registration and login for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.auth
  (:require
   [ovarit.app.fe.content.auth :as auth]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.tr :refer [tr trm-html trn]]
   [ovarit.app.fe.ui.forms.auth :as auth-forms]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.login :as login-form]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [dangerous-html]]
   [ovarit.app.fe.views.errors :refer [error-bar]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.inputs :refer [checkbox unstyled-text-input]]
   [ovarit.app.fe.views.misc-panels :refer [unauthenticated-panel]]
   [ovarit.app.fe.views.util :as util :refer [cls url-for]]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(defn- form-error
  "Create an error message for an authentication form."
  [content]
  [:div.mt3
   [:div.bg-white.medium-red.f5.b.ma0.pa2.ba.bw2.b--white
    content]])

(defn- validation-error-if-present
  "Create a validation error message, if there is one to show."
  [form]
  (let [form-state @(subscribe [::forms/form-state form])
        errors @(subscribe [::forms/errors form])]
    (when (= form-state :SHOW-ERROR)
      [form-error (first errors)])))

(defn- transmission-error-if-present
  "Create a transmission error message, if there is one to show."
  [form]
  (let [form-state @(subscribe [::forms/form-state form])
        send-errors @(subscribe [::forms/send-errors form])]
    (when (= form-state :SHOW-FAILURE)
      [form-error (first send-errors)])))

(defn- link
  "Create a link for an authentication form."
  [props content]
  [:a.custom.moon-gray.f5.pointer
   props content])

(defn- auth-input-class
  [disabled?]
  (cls :dib.lh-title.pl2.ba.br0.bw1.v-mid.shadow-6
       (if disabled?
         :gray.bg-light-gray.b--white
         :black.bg-white.b--white)))

(defn- auth-input
  "Create a text input for one of the authentication forms."
  [form {:keys [field event]} _]
  (let [value (fn [] @(subscribe [::forms/field form field]))]
    (fn [form {:keys [disabled update-value!] :as props} label]
      [:<>
       [:label {:for field}
        label]
       [unstyled-text-input
        (-> props
            (dissoc :field :form :label)
            (assoc :id field
                   :value value
                   :update-value! (or update-value!
                                      forms/reset-input-on-success!)
                   :event (or event [::forms/edit form field])
                   :class (auth-input-class disabled)
                   :required true))]])))

(defn- auth-button
  "Create a button for one of the auth forms."
  [{:keys [disabled] :as props} content]
  [:button
   (assoc props :class (cls :bg-purple.ba.bw2
                            (if disabled
                              :moon-gray.b--moon-gray
                              :white.b--white)
                            (when-not disabled
                              :tx-03.hover-bg-medium-purple.hover-b--moon-gray)
                            :f4.b.ttu
                            (when-not disabled :pointer)
                            :pv12px.ph3.w-100.gr-full-width))
   content])

(defn auth-form
  "Create a form for one of the auth pages."
  [& children]
  (into [:form.gr-auth-form] children))

(defn- password-advice []
  (let [settings @(subscribe [::auth/registration-settings])]
    [:p (tr "Passwords should be at least %s characters long and contain
             letters, numbers and symbols."
            (:password-length settings))]))

(defn- hidden-username-field [username]
  ;; :doc Create a hidden username input.
  ;; :doc This is for accessibility and password managers.
  [:input {:type :text
           :read-only true
           :auto-complete :username
           :value username
           :hidden true}])

(defn- return-link
  "Create a link back to the main page."
  []
  [:p [link {:href "/"} (tr "Return to the main page")]])

(defn- disabled-notice
  "Create the notice for disabled registration."
  []
  [:<>
   [:p.f4
    (tr "Registration of new users has been temporarily disabled.
         Please check back later.  We apologize for any
         inconvience.")]
   [return-link]])

(defn- registration-input
  "Create a text input for the registration form."
  [props label]
  [auth-input ::auth-forms/registration props label])

(defn- tos-checkbox-label
  "Create the label for the terms of service checkbox."
  []
  (trm-html "I accept the %{tos} and I will follow the %{rules}."
            {:tos [link {:href (url-for :wiki/view :slug "tos")
                         :target "_blank"}
                   (tr "Terms of Service")]
             :rules [link {:href (url-for :wiki/view :slug "rules")
                           :target "_blank"}
                     (tr "Sitewide Rules")]}))

(defn- registration-form
  "Create the registration form."
  []
  (let [form ::auth-forms/registration
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        form-state @(subscribe [::forms/form-state form])
        settings @(subscribe [::auth/registration-settings])
        accept-tos? (or @(subscribe [::forms/field form :accept-tos?]) false)]
    [auth-form
     (when (:invite-code-required settings)
       [registration-input {:field :invite-code
                            :auto-complete :off}
        (tr "Invite code:")])
     [registration-input {:field :email
                          :type :email}
      (tr "Email:")]
     [registration-input {:field :username
                          :auto-complete :username}
      (tr "Username:")]
     [registration-input {:field :password
                          :type :password
                          :auto-complete :new-password}
      (tr "Password:")]
     [registration-input {:field :confirm-password
                          :type :password
                          :auto-complete :new-password}
      (tr "Confirm Password:")]

     [:div.gr-full-width
      [password-advice]
      [checkbox {:id :tos-checkbox
                 :value accept-tos?
                 :event [::forms/edit form :accept-tos?]
                 :label [tos-checkbox-label]}]
      [:div
       [validation-error-if-present form]
       [transmission-error-if-present form]]]

     [auth-button {:on-click send}
      (if (= :SENDING form-state)
        (tr "Sending registration")
        (tr "Register"))]

     (when (:invite-code-required settings)
       [:div.tc.mt2
        [link {:href "/wiki/invites"}
         (tr "Need an invite code?")]])]))

(defn- auth-panel
  "Create one of the authentication pages."
  []
  (let [this (reagent/current-component)
        props (reagent/props this)
        children (reagent/children this)]
    [:main {:class (cls :bg-purple.white.system-sans-serif
                        :flex.flex-column.items-center.justify-center
                        :min-vh-100)}
     [error-bar]
     [:div.w-90.mw-90-s.mw6-ns.ph2
      (when-not (:hide-logo? props)
        [:a {:href "/"
             :aria-label (tr "Return to the home page")}
         [icon {:aria-label (tr "Logo")} "logo"]])
      (into [:div.relative] children)]]))

(defn registration-panel
  "Create the new user registration page."
  []
  (let [status @(subscribe [::auth/registration-settings-status])
        ready? @(subscribe [::auth/registration-settings-ready?])
        form-state @(subscribe [::forms/form-state ::auth-forms/registration])
        settings @(subscribe [::auth/registration-settings])]
    [auth-panel {:hide-logo? (not ready?)}
     (cond
       (not ready?)                     nil
       (nil? form-state)                nil
       (= :error status)                [return-link]
       (:registration-enabled settings) [registration-form]
       :else                            [disabled-notice])]))

(defn- verification-instructions
  "Create the instructions for verifying a newly registered email."
  []
  (let [name @(subscribe [::settings/name])
        {:keys [username email]} @(subscribe [:view/options])
        settings @(subscribe [::auth/registration-settings])
        start-over #(dispatch [::auth/start-over])]
    [:div
     [:p
      (tr "Welcome to %s! " name)
      ;; If we got here directly from /register, the username and email
      ;; are in the db. Only the uid and delete code are in the url,
      ;; so if this page was refreshed we might have to do without
      ;; the name and email.
      (if (and username email)
        [:<>
         (trm-html
          "You've registered as %{username} and we have sent an email to:"
          {:username [:strong username]})
         [:strong.db.tc.pt1.pb2 email]
         (tr "Please use the link in the email to finish activating your
              account.")]
        (tr "We sent you an email to confirm your email
             address. Please use the link in the email to finish
             activating your account."))]
     [:p (tr "If you don't see the email in your inbox, please check your
              spam folder.")]
     [:p (trm-html
          "If you used the wrong email address, or made a mistake
           entering your username, you can %{delete}. You can only
           start over from this page."
          {:delete [link {:on-click start-over}
                    (if (:invite-code-required settings)
                      (tr "start over with the same invite code by
                           clicking here")
                      (tr "start over by clicking here"))]})]]))

(defn- login-notice
  "Create a notice that an email address has been confirmed."
  []
  (let [name @(subscribe [::settings/name])]
    [:div
     [:p (tr "Welcome to %s! " name)]
     [:p "The link we sent to your email address has already been
          confirmed, and you are ready to log in."]
     [auth-button
      {:on-click #(dispatch [::auth/login-then-welcome])}
      (tr "Log in")]]))

(defn verify-registration-panel
  "Create the acknowledgement page for a successful registration.
  Either show instructions for what to do next, or a login button.
  Show the latter if they use the start-over link, but their account
  has already been verified."
  []
  (let [{:keys [already-verified?]} @(subscribe [:view/options])]
    [auth-panel
     (if already-verified?
       [login-notice]
       [verification-instructions])]))

(defn confirm-registration-panel
  "Create the page for confirming a user registration.
  Users will get here from clicking a link in their email. "
  []
  (let [{:keys [state]} @(subscribe [:view/options])
        retry #(dispatch [::auth/retry-confirm :auth/confirm-registration])]
    [auth-panel {:hide-logo? (= :loading state)}
     (case state
       :loading nil

       ;; This will be accompanied by a toast with error specifics.
       :xmit-error
       [:div
        [:p (tr "Your registration could not be confirmed due to an
                 error communicating with the server.")]
        [:p [link {:on-click retry}
             (tr "Try again.")]]
        [return-link]]

       :invalid-link
       [:div
        [:p (tr "The link you used is no longer valid.")]
        [return-link]])]))

(defn- login-input
  "Create a text input for the login form."
  [props label]
  [auth-input ::login-form/login props label])

(defn login-link
  [props label]
  [:div.mt2
   [link props label]])

(defn remember-me-checkbox
  "Create the remember me checkbox."
  []
  (let [form ::login-form/login
        remember-me? (fn [] @(subscribe [::forms/field form :remember-me?]))
        set-remember #(dispatch
                       [::forms/edit form :remember-me?
                        (-> % .-target .-checked)])]
    [:<>
     [:label {:for :remember-me}
      (tr "Remember me:")]
     [:input.gr-justify-start.ml0 {:type :checkbox
                                   :id :remember-me
                                   :value remember-me?
                                   :checked (remember-me?)
                                   :on-change set-remember}]]))

(defn- login-notification-if-any
  "Create a notification message for the login page, if necessary."
  []
  (when-let [notify (:notify @(subscribe [:view/options]))]
    [:p.b.f4
     (case notify
       :confirm (tr "Your new account is confirmed and you are ready
                     to log in.")
       :reset (tr "Your password has been changed.")
       :name-change (tr "Your username has been changed.")
       nil)]))

(defn- required-name-change-if-any
  "Create form elements for users required to change their username."
  []
  (let [form ::login-form/login
        html-message @(subscribe
                       [::login-form/name-change-message-html])]
    (when html-message
      [:<>
       [:div.gr-full-width.w-100
        [:h3 (tr "Username change required")]
        [:p.mb0
         (tr "The site administrators have determined that your
              current username is unacceptable. In order to login,
              you must first choose a new username.")]]
       [:label {:for :reason}
        (tr "Reason:")]
       [:p#reason
        [dangerous-html {} html-message]]
       [auth-input form {:field :new-username
                         :type :text
                         :auto-complete :username}
        (tr "New username:")]])))

(defn- login-links
  "Create links to other auth content for the login page."
  []
  [:section.tc.pt2
   [login-link {:href (url-for :auth/register)}
    (tr "Make an account")]
   [login-link {:href (url-for :auth/start-reset)}
    (tr "Reset password")]
   [login-link {:href (url-for :auth/resend-verification-email)}
    (tr "Resend verification email")]])

(defn login-panel
  "Create the login page."
  []
  (let [form ::login-form/login
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        change-name #(do (.preventDefault %)
                         (dispatch [::forms/action form :change-name]))
        form-state @(subscribe [::forms/form-state form])
        name-change-message @(subscribe [::forms/field form
                                         :name-change-message])]
    [auth-panel
     [login-notification-if-any]

     [auth-form
      [login-input {:field :username
                    :auto-complete :username
                    :update-value! (partial forms/reset-input-on-field-change!
                                            :name-change-message)
                    :name-change-message name-change-message}
       (tr "Username:")]
      [login-input {:type :password
                    :auto-complete :current-password
                    :field :password}
       (tr "Password:")]
      [remember-me-checkbox]
      [required-name-change-if-any]
      [:div.gr-full-width.w-100
       [validation-error-if-present form]
       [transmission-error-if-present form]]

      (if (nil? name-change-message)
        [auth-button {:on-click send}
         (if (#{:SENDING :SHOW-SUCCESS} form-state)
           (tr "Logging in")
           (tr "Login"))]
        [auth-button {:on-click change-name}
         (if (#{:SENDING :SHOW-SUCCESS} form-state)
           (tr "Changing")
           (tr "Change username"))])]
     [login-links]]))

(defn confirm-reset-password-notice
  "Create a confirmation that a reset password email was sent."
  []
  [:div.gr-full-width
   [:p (tr "If the username and email address which you entered match
            what we have on file, we have sent an email message to
            you. You can use the link in the email to create a new
            password for your account.")]
   [:p (tr "If you don't see the email in your inbox, please check your
            spam folder.")]])

(defn- start-reset-password-form
  "Create the form for starting the password reset process."
  []
  (let [form ::auth-forms/start-reset
        reset #(do (.preventDefault %)
                   (dispatch [::forms/action form :reset]))
        form-state @(subscribe [::forms/form-state form])
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:<>
     (when-not (= :SHOW-SUCCESS form-state)
       [:p "To reset your password, enter the username and email
            address for your account. If the email address matches the
            one we have on file for your username, we will send you a
            message containing a link which you can use to set a new
            password."])
     [auth-form
      [:<>
       [auth-input form {:field :username
                         :disabled disabled?
                         :auto-complete :username}
        (tr "Username:")]
       [auth-input form {:field :email
                         :disabled disabled?
                         :auto-complete :email}
        (tr "Email:")]]
      [:div.gr-full-width
       [validation-error-if-present form]
       [transmission-error-if-present form]]

      (when (= :SHOW-SUCCESS form-state)
        [confirm-reset-password-notice])

      [auth-button {:on-click reset
                    :disabled disabled?}
       (case form-state
         :SENDING (tr "Sending")
         :SHOW-SUCCESS (tr "Email sent")
         (tr "Reset password"))]
      (when (= :SHOW-SUCCESS form-state)
        [return-link])]]))

(defn start-reset-password-panel
  "Create the page to start the password reset process."
  []
  [auth-panel
   [start-reset-password-form]])

(defn- complete-reset-password-form
  "Create the password change form for the complete reset password page."
  []
  (let [form ::auth-forms/reset-password
        {:keys [bad-code? username]} @(subscribe [:view/options])
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        form-state @(subscribe [::forms/form-state form])
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:<>
     [:div.f3
      [:p (trm-html "Create a new password for %{username}"
                    {:username [:strong username]})]]
     [auth-form
      [hidden-username-field username]
      [auth-input form {:field :password
                        :type :password
                        :disabled disabled?
                        :auto-complete :new-password}
       (tr "New Password:")]
      [auth-input form {:field :confirm-password
                        :type :password
                        :disabled disabled?
                        :auto-complete :new-password}
       (tr "Confirm New Password:")]

      [:div.gr-full-width
       [password-advice]
       [:div
        [validation-error-if-present form]
        [transmission-error-if-present form]]]

      (when-not bad-code?
        [auth-button {:on-click send}
         (case form-state
           :SENDING (tr "Sending")
           :SHOW-SUCCESS (tr "Password reset email sent")
           (tr "Reset password"))])

      (when (= :SHOW-SUCCESS form-state)
        [return-link])]

     (when bad-code?
       [:<>
        [form-error
         (tr "The password reset link was invalid or expired.")]
        [login-link {:href (url-for :auth/login)}
         (tr "Log in")]
        [login-link {:href (url-for :auth/start-reset)}
         (tr "Send another password reset message")]])]))

(defn- password-reset-complete-notice
  []
  [:<>
   [:p (tr "Your password was successfully changed.")]
   [login-link {:href (url-for :auth/login)}
    (tr "Log in")]])

(defn complete-reset-password-panel
  "Create the form used to complete the password reset process."
  []
  (let [form ::auth-forms/reset-password
        form-state @(subscribe [::forms/form-state form])
        {:keys [bad-code?]} @(subscribe [:view/options])
        ready? @(subscribe [::auth/registration-settings-ready?])
        status @(subscribe [::auth/registration-settings-status])]
    [auth-panel {:hide-logo? (not ready?)}
     (cond
       (not ready?)                 nil
       (= :error status)            [return-link]
       bad-code?                    [complete-reset-password-form]
       (= :SHOW-SUCCESS form-state) [password-reset-complete-notice]
       :else                        [complete-reset-password-form])]))

(defn resend-verification-email-form
  "Create the form to get the username to send an email to."
  []
  (let [form ::auth-forms/username
        resend #(do (.preventDefault %)
                    (dispatch [::forms/action form :resend]))
        form-state @(subscribe [::forms/form-state form])]
    [:<>
     [:p "Before you can log into your account, we require a verified
          email address. To send a link to your email address which
          can be used to verify it, enter your username below."]
     [auth-form
      [auth-input form {:field :username
                        :auto-complete :username}
       (tr "Username:")]
      [:div.gr-full-width
       [validation-error-if-present form]
       [transmission-error-if-present form]]
      [auth-button {:on-click resend}
       (if (= :SENDING form-state)
         (tr "Sending")
         (tr "Resend verification email"))]]]))

(defn confirm-resend-verification-email-notice
  "Create a confirmation that a verification email was resent."
  []
  [:<>
   [:p (trm-html
        "We have sent another message to the email address you entered
         when you registered your account. You can use the link in the
         message to confirm your email address, so you can %{login} or
         %{reset}."
        {:login [link {:href (url-for :auth/login)}
                 (tr "log in")]
         :reset [link {:href (url-for :auth/start-reset)}
                 (tr "reset your password by email")]})]
   [:p (tr "If you don't see the email in your inbox, please check your
            spam folder.")]
   [return-link]])

(defn resend-verification-email-panel
  "Create the resend verification email page."
  []
  (let [form ::auth-forms/username
        form-state @(subscribe [::forms/form-state form])]
    [auth-panel
     (if (not= :SHOW-SUCCESS form-state)
       [resend-verification-email-form]
       [confirm-resend-verification-email-notice])]))

(defn- email-change-confirmation
  []
  (let [email @(subscribe [::forms/field ::auth-forms/change-email :email])]
    [:<>
     [:p (trm-html
          "We have sent an email to %{email} with a link to confirm
           your new email address. Please use the link in the email to
           confirm the change."  {:email [:strong email]})]
     [:p (tr "If you don't see the email in your inbox, please check your
            spam folder.")]
     [:p (tr
          "Until the new address is confirmed, we will continue to use
           the old one.")]]))

(defn- change-email-form-fields []
  (let [form ::auth-forms/change-email
        form-state @(subscribe [::forms/form-state form])
        email (fn [] @(subscribe [::forms/field form :email]))
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:<>
     [auth-input form {:field :email
                       :type :email
                       :auto-complete :email
                       :disabled disabled?
                       :value email}
      (tr "Email:")]

     [:div.gr-full-width.w-100
      (when (= :SHOW-SUCCESS form-state)
        [email-change-confirmation])
      [validation-error-if-present form]
      [transmission-error-if-present form]]

     [:div.gr-full-width.w-100.mb4-ns.mb3
      [auth-button {:on-click send
                    :disabled disabled?}
       (case form-state
         :SENDING (tr "Updating")
         :SHOW-SUCCESS (tr "Confirmation email sent")
         (tr "Update email"))]

      (when (= :SHOW-SUCCESS form-state)
        [return-link])]]))

(defn- edit-password-form-fields []
  (let [form ::auth-forms/change-password
        form-state @(subscribe [::forms/form-state form])
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:<>
     [auth-input form {:field :password
                       :type :password
                       :auto-complete :new-password
                       :disabled disabled?
                       :event [::forms/edit form :password]}
      (tr "New Password:")]
     [auth-input form {:field :confirm-password
                       :type :password
                       :auto-complete :new-password
                       :disabled disabled?
                       :event [::forms/edit form :confirm-password]}
      (tr "Confirm New Password:")]

     [:div.gr-full-width.w-100
      (when-not (= :SHOW-SUCCESS form-state)
        [password-advice])
      [:div
       [validation-error-if-present form]
       [transmission-error-if-present form]]]

     [auth-button {:on-click send
                   :disabled disabled?}
      (case form-state
        :SENDING (tr "Updating")
        :SHOW-SUCCESS (tr "Password updated")
        (tr "Update password"))]
     (when (= :SHOW-SUCCESS form-state)
       [return-link])]))

(defn- edit-account-form
  []
  (let [{:keys [name]} @(subscribe [::user/current-user])
        email-form ::auth-forms/change-email
        email-form-state @(subscribe [::forms/form-state email-form])
        email-form-disabled? @(subscribe [::forms/editing-disabled? email-form])
        password-form ::auth-forms/change-password
        password-form-state @(subscribe [::forms/form-state password-form])
        password-form-disabled? @(subscribe [::forms/editing-disabled?
                                             password-form])
        set-pw #(do
                  (dispatch [::forms/edit email-form :current-password %])
                  (dispatch [::forms/edit password-form :current-password %]))]
    [auth-form
     [:p.mv0 (tr "Username:") [:span.dn-ns.ml2 [:strong name]]]
     [:strong.dn.db-m.db-l name]
     [hidden-username-field name]

     ;; This uses the email form but the password is duplicated
     ;; into the password form by `set-pw`.
     [auth-input email-form {:type :password
                             :auto-complete :current-password
                             :field :current-password
                             :disabled (or email-form-disabled?
                                           password-form-disabled?)
                             :event set-pw}
      (tr "Current password:")]
     [:div.gr-full-width.w-100.mb2-ns.mb2
      [:p.mt1
       (tr "Your current password is required in order to make changes.")]]
     (when-not (= :SHOW-SUCCESS password-form-state)
       [change-email-form-fields])
     (when-not (= :SHOW-SUCCESS email-form-state)
       [edit-password-form-fields])]))

(defn edit-account-panel
  "Create the edit account page."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        status @(subscribe [::auth/edit-account-status])
        email-form ::auth-forms/change-email
        email-form-state @(subscribe [::forms/form-state email-form])
        password-form ::auth-forms/change-password
        password-form-state @(subscribe [::forms/form-state password-form])
        ready? (and (some? email-form-state) (some? password-form-state))]
    (if (not is-authenticated?)
      [unauthenticated-panel]
      [auth-panel {:hide-logo? (not ready?)}
       (cond
         (not ready?)                          nil
         (= :error status)                     [return-link]
         :else                                 [edit-account-form])])))

(defn rename-account-info
  "Create an explanatory blurb for the rename account page."
  []
  (let [form ::auth-forms/rename
        form-state @(subscribe [::forms/form-state form])
        success? (= :SHOW-SUCCESS form-state)
        old-username @(subscribe [::forms/field form :old-username])
        {limit-days :username-change-limit-days
         limit :username-change-limit} @(subscribe [::settings/site-config])
        recents @(subscribe [::auth/recent-rename-history-count])
        permitted @(subscribe [::auth/permitted-change-count])
        period (if (zero? limit-days)
                 ""
                 (trn [" per day" " in every %s days"] limit-days))]
    [:div.mv3
     (if success?
       [:p (trm-html "Your username has been changed to %{username}."
                     {:username [:span.f4 [:strong old-username]]})]
       [:<>
        [:p (trm-html "Your current username is %{username}."
                      {:username [:span.f4 [:strong old-username]]})]
        [:p
         (if (= 1 limit)
           (tr "Everyone is allowed to change their username %s time%s."
               limit period)
           (tr "Everyone is allowed to change their username %s times%s."
               limit period))]
        (when (pos? recents)
          [:p
           (if (zero? limit-days)
             (trn ["You have already changed your username %s time."
                   "You have already changed your username %s times."]
                  recents)
             (trn ["You have recently changed your username %s time."
                   "You have recently changed your username %s times."]
                  recents))])
        [:p
         (if (zero? limit-days)
           (if (zero? permitted)
             (tr "You have already used all your username changes.")
             (trn ["You can change your username %s time."
                   "You can change your username %s times."]
                  permitted))
           (if (zero? permitted)
             (tr "You are not currently allowed to change your username.")
             (trn ["You can currently change your username %s time."
                   "You can currently change your username %s times."]
                  permitted)))]])]))

(defn- rename-account-caveats
  "Create some additional explanation for the rename account page."
  []
  (let [form ::auth-forms/rename
        form-state @(subscribe [::forms/form-state form])
        success? (= :SHOW-SUCCESS form-state)
        old-username @(subscribe [::forms/field form :old-username])
        userlink [link {:href (url-for :user/view :user old-username)}
                  "your profile page"]
        {days :username-change-display-days} @(subscribe
                                               [::settings/site-config])]
    [:div.gr-full-width
     (when (pos? days)
       [:<>
        [:p (if (= 1 days)
              (trm-html "Your old username will be displayed to other
                        logged-in users on %{userlink} for one day."
                        {:userlink userlink})
              (trm-html "Your old username will be displayed to other
                         logged-in users on %{userlink} for %{days}
                         days."
                        {:userlink userlink :days days}))]
        (when-not success?
          [:p (tr "If you need to hide your connection with your old
                   username, you should create a new account.")])])]))

(defn- rename-account-form
  "Create the form that allows users to change their usernames."
  []
  (let [form ::auth-forms/rename
        form-state @(subscribe [::forms/form-state form])
        success? (= :SHOW-SUCCESS form-state)
        disabled? @(subscribe [::forms/editing-disabled? form])
        change #(do (.preventDefault %)
                    (dispatch [::forms/action form :change]))
        permitted @(subscribe [::auth/permitted-change-count])]
    [:<>
     [rename-account-info]
     (if (and (not success?) (zero? permitted))
       [return-link]
       [auth-form
        (when-not success?
          [:<>
           [auth-input form {:field :username
                             :type :text
                             :disabled disabled?
                             :auto-complete :username}
            (tr "New username:")]
           [auth-input form {:field :password
                             :type :password
                             :disabled disabled?
                             :auto-complete :current-password}
            (tr "Password:")]
           [:div.gr-full-width.w-100
            [validation-error-if-present form]
            [transmission-error-if-present form]]])
        [rename-account-caveats]
        [auth-button {:on-click change
                      :disabled (or success? disabled?)}
         (case form-state
           :SENDING (tr "Changing")
           :SHOW-SUCCESS (tr "Username changed")
           (tr "Change my username"))]])
     (when (= :SHOW-SUCCESS form-state)
       [return-link])]))

(defn rename-account-panel
  "Create the rename account page."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        status @(subscribe [::auth/edit-account-status])
        loading? @(subscribe [::loader/content-loading?])]
    (if (not is-authenticated?)
      [unauthenticated-panel]
      [auth-panel {:hide-logo? loading?}
       (cond
         loading?          nil
         ;; TODO direct to page not found if not configured
         (= :error status) [return-link]
         :else             [rename-account-form])])))

(defn confirm-email-panel
  "Create the page for confirming a new email.
  Users will get here from clicking a link in an email message."
  []
  (let [{:keys [state]} @(subscribe [:view/options])
        retry #(dispatch [::auth/retry-confirm :auth/confirm-email])]
    [auth-panel
     (case state
       :loading nil

       :confirmed
       [:div
        [:p "Your new password recovery email address is now confirmed."]
        [return-link]]

       ;; This will be accompanied by a toast with error specifics.
       :xmit-error
       [:div
        [:p (tr "Your email address could not be confirmed due to an
                 error communicating with the server.")]
        [:p [link {:on-click retry}
             (tr "Try again.")]]
        [return-link]]

       :invalid-link
       [:div
        [:p (tr "The link you used is no longer valid.")]
        [return-link]])]))

(defn- delete-account-form
  "Create the form for account deletion.
  When showing a successful deletion, hide the inputs
  and show a return link."
  []
  (let [{:keys [name]} @(subscribe [::user/current-user])
        form ::auth-forms/delete-account
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        form-state @(subscribe [::forms/form-state form])]
    [:<>
     [:h1 "Account deletion"]
     [:h2 "This action cannot be undone."]
     [auth-form
      [:p.mv0 (tr "Username:") [:span.dn-ns.ml2 [:strong name]]]
      [:strong.dn.db-m.db-l name]
      [hidden-username-field name]
      (when-not (= :SHOW-SUCCESS form-state)
        [:<>
         [auth-input form {:auto-complete :off
                           :field :confirmation}
          (trm-html "Type %{word} here:"
                    {:word [:span.b (tr "'DELETE'")]})]

         [auth-input form {:type :password
                           :auto-complete :current-password
                           :field :current-password}
          (tr "Password:")]])
      [:div.gr-full-width.w-100
       [:div
        [validation-error-if-present form]
        [transmission-error-if-present form]]]

      [auth-button {:on-click send
                    :disabled (= :SHOW-SUCCESS form-state)}
       (case form-state
         :SENDING      (tr "Deleting")
         :SHOW-SUCCESS (tr "Account deleted")
         (tr "Delete my account"))]
      (when (= :SHOW-SUCCESS form-state)
        [return-link])]]))

(defn delete-account-panel
  "Create the delete account page."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])]
    (if (not is-authenticated?)
      [unauthenticated-panel]
      [auth-panel
       [delete-account-form]])))
