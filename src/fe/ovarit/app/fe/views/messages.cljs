;; fe/views/messages.cljs -- Private message ui for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.messages
  (:require
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.forms.compose-message :as compose]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [dangerous-html]]
   [ovarit.app.fe.views.forms :refer [transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button markdown-editor
                                       markdown-link-modal text-input]]
   [ovarit.app.fe.views.misc-panels :refer [unauthenticated-panel
                                            page-not-found-panel
                                            waiting-panel]]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.util :refer [cls url-for]]
   [re-frame.core :refer [subscribe dispatch]]))

;;; Message composition forms
(def compose-form ::compose/compose-form)

(defn subject-editor
  "Create a field for editing the message subject."
  []
  (let [subject (fn [] @(subscribe [::forms/field compose-form :subject]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe [::forms/editing-disabled?
                                           compose-form])]
        [text-input {:placeholder (tr "subject")
                     :class (cls :w-100.w-80-ns.mb2)
                     :value subject
                     :required true
                     :update-value! forms/reset-input-on-success!
                     :form-state form-state
                     :disabled editing-disabled?
                     :event [::forms/edit compose-form :subject]}]))))

(defn content-editor
  "Create a markdown editor for editing message content."
  []
  (let [content (fn [] @(subscribe [::forms/field compose-form :content]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe [::forms/editing-disabled?
                                           compose-form])]
        [markdown-editor
         {:placeholder
          (tr
           "Write your message here. Styling with Markdown format is supported.")
          :value content
          :class :mb3.w-100.w-80-ns
          :buttonbar-class :w-100.w-80-ns
          :update-value! forms/reset-input-on-success!
          :form-state form-state
          :disabled editing-disabled?
          :event [::forms/edit compose-form :content]}]))))

(defn send-button
  "Create the send button for a message composition form."
  [_]
  (let [dispatch-send #(dispatch [::forms/action compose-form :send])]
    (fn [text]
      (let [form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe [::forms/editing-disabled?
                                           compose-form])]
        [button {:class :dib
                 :button-class :primary
                 :disabled editing-disabled?
                 :on-click dispatch-send}
         (if (= form-state :SENDING)
           (tr "Sending...")
           text)]))))

(defn preview-button
  "Create the preview button for a message composition form."
  []
  (let [see-preview #(dispatch [::forms/action compose-form :see-preview])]
    [button {:on-click see-preview
             :class :dib
             :button-class :secondary}
     (tr "Preview")]))

(defn preview
  "Create the content preview for a message composition form."
  []
  (let [preview-open? @(subscribe [::forms/field compose-form :preview-open?])
        preview-html @(subscribe [::forms/field compose-form :preview])
        hide-preview #(dispatch [::forms/action compose-form :hide-preview])]
    (when preview-open?
      [:div.mt3.mb2.relative
       [:h4 (tr "Message preview")]
       [:span
        {:class (cls :absolute.top-0.right-0.fr.f3.pointer)
         :on-click hide-preview} "×"]
       [:hr]
       [dangerous-html {} preview-html]])))

(defn success-message
  "Create a success message."
  []
  (let [message-colors @(subscribe [::window/success-colors])]
    [:div {:class (cls :pa2.mt2.mb3.br2 message-colors)}
     (tr "Message sent!")]))

(defn success-message-on-success
  "Create a success message, when the form gets to that state."
  []
  (let [form-state @(subscribe [::forms/form-state compose-form])]
    (when (= form-state :SHOW-SUCCESS)
      [success-message])))

(defn contact-mods-form
  "Create the form elements for the contact mods panel."
  []
  (let [sub-name @(subscribe [::subs/normalized-sub-name])
        sublink [:a {:href (url-for :sub/view-sub :sub sub-name)} sub-name]
        form-state @(subscribe [::forms/form-state compose-form])]
    [:div
     [subject-editor]
     [content-editor]
     [validation-error-if-present compose-form]
     [transmission-error-if-present compose-form]
     [:div
      [send-button (tr "Send")] " "
      [preview-button]
      (when (= form-state :SHOW-SUCCESS)
        [:<>
         [success-message]
         [:h4 (trm-html "Go back to %{sub}" {:sub sublink})]])
      [preview]]
     [markdown-link-modal]]))

(defn contact-mods-panel
  "Create the contact mods panel."
  []
  (let [ready @(subscribe [::compose/ready-to-compose-message-to-mod])
        sub-name @(subscribe [::subs/normalized-sub-name])
        is-authenticated? @(subscribe [::user/is-authenticated?])
        sublink (when sub-name
                  [:a {:href (url-for :sub/view-sub :sub sub-name)} sub-name])]
    (cond
      (not ready)
      [waiting-panel]

      (not is-authenticated?)
      [unauthenticated-panel]

      (nil? sub-name)
      [page-not-found-panel]

      :else
      [panel {:sidebar :<>}
       [:h2 (trm-html "Contact the mods of %{sub}" {:sub sublink})]
       [contact-mods-form]])))
