;; fe/views/common.cljs -- Shared view elements for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.common
  (:require
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame.core :refer [subscribe]]
   [reagent.core :as reagent]))

(defn helper-text
  "Make a suggestion in italics and a muted color."
  [& children]
  (into [:p.i.gray] children))

(defn reveal
  "Toggle the 'shown' class on a <spoiler> target."
  [this]
  (let [target (. this -target)
        ;; The spoilered element might have children
        ;; and one of the children could have been clicked.
        matching-parent (. target closest "spoiler")]
    (when (and matching-parent
               (. matching-parent matches "spoiler"))
      (. (. matching-parent -classList) toggle "shown"))))

(defn add-reveal-to-spoilers
  "Add an onclick handler to all the spoiler tags."
  [elem]
  (when elem
    (let [spoilers (.getElementsByTagName ^js/Element elem "spoiler")]
      (run! (fn [spoiler]
              (set! (. spoiler -onclick) reveal))
            spoilers))))

(defn dangerous-html
  "Create a span containing some html, and enliven the <spoiler> tags."
  [_ _]
  (let [self-ref (reagent/atom nil)
        set-ref! (fn [ref]
                   (add-reveal-to-spoilers ref)
                   (reset! self-ref ref))]
    (reagent/create-class
     {:display-name "dangerous-html"

      :component-did-update
      #(add-reveal-to-spoilers @self-ref)

      :reagent-render
      (fn [props html]
        [:span (merge props
                      {:dangerouslySetInnerHTML {:__html html}
                       :ref set-ref!})])})))

(defn blur-wrapper
  "Apply a blur class to the children, and remove it when clicked."
  []
  (let [clicked? (reagent/atom false)
        click! #(do (.preventDefault %)
                    (reset! clicked? true))]
    (fn [props & children]
      (if @clicked?
        (into [:span {:class (:class props)}] children)
        (into [:span {:class (cls (:class props) (:blur props))
                      :on-click click!}]
              children)))))

(def cell-cls :bl.bb.bw1.m0.ph3.pv2)

(defn table-cell
  "Create a table cell."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        props (reagent/props this)
        border-color @(subscribe [::window/table-border-colors])]
    (into [:td (merge {:class (cls cell-cls border-color (:class props))}
                      (dissoc props :class))]
          children)))

(defn table-label-cell
  "Create a table cell for a label."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        props (reagent/props this)
        label-colors @(subscribe [::window/table-label-colors])
        border-color @(subscribe [::window/table-border-colors])]
    (into [:td (merge {:class (cls cell-cls :bb label-colors border-color
                                   (:class props))}
                      (dissoc props :class))]
          children)))

(defn userlink
  "Create a link to a user.
  Use :prefix in props, to render /u/user instead of just user."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        name (first children)
        remaining-children (rest children)
        props (reagent/props this)
        prefix (:prefix props)]
    (into [:a (-> props
                  (dissoc :prefix)
                  (assoc :href (url-for :user/view :user name)))
           prefix name]
          remaining-children)))

(defn user-flair
  "Create a user flair.
  In properties, use :selected? to make it change color, and
  :selectable? to make it change on hover."
  []
  (let [this (reagent/current-component)
        props (reagent/props this)
        children (reagent/children this)
        user-flair-colors @(subscribe [::window/user-flair-colors props])]
    (into
     [:span {:class (cls :br1.f6.lh-title.pa1 user-flair-colors
                         (when (:selectable? props) :pointer))}]
     children)))

(defn nsfw-tag
  "Create a little colorful NSFW indicator."
  []
  (let [day? @(subscribe [::window/day?])
        red (if day? :dark-red :reddish-brown)]
    [:div
     {:class (cls red :ba.br2.dib.f7.lh-solid.pa1.mh2)
      :title (tr "Not safe for work")} (tr "NSFW")]))

(defn modal
  "Create a modal wrapper for some content.

  Customize with these keys in `properties`:

  :background     - Defaults to a dark background covering page content.
  :foreground     - Defaults to white/black depending on daymode.
  :width          - Defaults to fairly wide.
  :show-cancel-x? - Whether to show a cancel X, defaults to true.
  :class          - Arbitrary classes to add to outer div."
  [_ _]
  (let [stop-propagation #(.stopPropagation %)]
    (fn [{:keys [show cancel class background foreground width show-cancel-x?]
          :or {show-cancel-x? true}} child]
      (let [day? @(subscribe [::window/day?])
            form-colors (if day? :bg-white :bg-black)]
        (when (show)
          [:div
           {:class (cls :flex.justify-center.fixed.absolute--fill.z-max
                        (or background ::blur-backdrop.bg-black-80)
                        ;; Reset the letter spacing in case PureCSS messed it up.
                        :pure-u-1-1
                        class)
            :on-click cancel}
           [:div
            {:class (cls :self-start.mt4.mt6-ns.h-auto.pa2.br3
                         (or width :w-80.w-50-ns.mw6)
                         (or foreground form-colors))
             :on-click stop-propagation}
            [:div.pa3.pa4-ns.relative
             (when show-cancel-x?
               [icon {:class (cls :absolute.top-0.right-0.h2.w2.pointer
                                  (when-not day? :fill-light-silver))
                      :on-click cancel}
                "close"])
             child]]])))))

(defn relative-time
  [{:keys [datetime] :as params}]
  [:relative-time params
   (util/format-date datetime)])
