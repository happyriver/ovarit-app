;; fe/views/admin.cljs -- Administration ui for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
(ns ovarit.app.fe.views.admin
  (:require
   [ovarit.app.fe.content.admin :as admin]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.ui.forms.admin :as admin-forms]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.util :refer [zero-pad]]
   [ovarit.app.fe.views.common :refer [helper-text modal userlink table-cell
                                       table-label-cell dangerous-html
                                       relative-time]]
   [ovarit.app.fe.views.forms :refer [preview
                                      transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.inputs :refer [button checkbox datepicker
                                       markdown-editor markdown-link-modal
                                       radio-button
                                       text-input date-selector]]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.sidebar :refer [button-link]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(defn admin-sidebar
  "Create the sidebar for the admin pages."
  []
  (let [menu-state @(subscribe [::window/menu-state])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     [button-link {:href (url-for :admin/index)}
      (tr "Admin")]
     [:hr]
     [button-link {:href (url-for :admin/view)}
      (tr "Admins")]
     [button-link {:href (url-for :admin/users)}
      (tr "Users")]
     [button-link {:href (url-for :admin/userbadges)}
      (tr "User Badges")]
     [button-link {:href (url-for :admin/invitecodes)}
      (tr "Invite Codes")]
     [:hr]
     [button-link {:href (url-for :admin/subs)}
      (tr "Circles")]
     [button-link {:href (url-for :admin/posts)}
      (tr "Posts")]
     [button-link {:href (url-for :admin/reports)}
      (tr "Reports")]
     [:hr]
     [button-link {:href (url-for :admin/user-uploads)}
      (tr "Uploads")]
     [button-link {:href (url-for :admin/wiki)}
      (tr "Wiki")]
     [button-link {:href (url-for :admin/domains :domain-type "link")}
      (tr "Banned Domains")]
     [button-link {:href (url-for :admin/domains :domain-type "email")}
      (tr "Banned Email Domains")]
     [button-link {:href (url-for :admin/banned-user-names)}
      (tr "Banned User Names")]
     [:hr]
     [button-link {:href (url-for :admin/configure)}
      (tr "Site Configuration")]
     [button-link {:href (url-for :admin/stats)}
      (tr "Site Statistics")]
     [button-link {:href (url-for :site/view-sitelog)}
      (tr "Sitelog")]]))

(defn admins-only
  "Create a panel that is only for admins."
  [panel]
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        is-admin? @(subscribe [::user/is-admin?])]
    (cond
      (not is-authenticated?)
      [misc-panels/unauthenticated-panel]

      (not is-admin?)
      [misc-panels/unauthorized-panel]

      :else
      panel)))

(defn note-if-incomplete
  "Create some helper text if the date range includes today."
  [complete?]
  (when-not complete?
    [helper-text
     (tr "Time span includes the current day (GMT), so data may be incomplete.")]))

(defn stats-table-row
  [label data]
  [:tr
   [table-label-cell label]
   [table-cell data]])

(defn stats-table
  "Create a table for the site statistics."
  [{:keys [users subs posts comments upvotes downvotes]}]
  (let [border-color @(subscribe [::window/table-border-colors])]
    [:table {:class (cls :collapse.ba.bw1 border-color)}
     [:tbody
      [stats-table-row (tr "Users") users]
      [stats-table-row (tr "Circles") subs]
      [stats-table-row (tr "Posts") posts]
      [stats-table-row (tr "Comments") comments]
      [stats-table-row (tr "Votes")
       [:span "+" upvotes " | " "-" downvotes]]]]))

(defn recent-stats-table
  "Create a table for the recent site statistics."
  [{:keys [users subs posts comments upvotes downvotes
           users-who-posted users-who-commented users-who-voted]}]
  (let [border-color @(subscribe [::window/table-border-colors])]
    [:table {:class (cls :collapse.ba.bw1 border-color)}
     [:tbody
      [stats-table-row (tr "New users") users]
      [stats-table-row (tr "New circles") subs]
      [stats-table-row (tr "New posts") posts]
      [stats-table-row (tr "New comments") comments]
      [stats-table-row (tr "New votes") [:span "+" upvotes " | " "-" downvotes]]
      [stats-table-row (tr "Users who posted") users-who-posted]
      [stats-table-row (tr "Users who commented") users-who-commented]
      [stats-table-row (tr "Users who voted") users-who-voted]]]))

(defn visitor-stats-table
  "Create a daily or weekly visitor stats table."
  [option]
  (let [border-color @(subscribe [::window/table-border-colors])
        stats @(subscribe  [::admin/visitor-counts option])
        complete? @(subscribe [::admin/visitor-counts-complete? option])
        {:keys [entries average]} stats]
    [:<>
     [:table {:class (cls :collapse.ba.bw1 border-color)}
      [:tbody
       [:tr
        [table-label-cell (tr "Date")]
        [table-label-cell (tr "Visitors")]]
       (into [:<>]
             (map (fn [{:keys [month day year value]}]
                    [:tr
                     [table-cell
                      [:span
                       year "/"
                       (zero-pad month 2) "/"
                       (zero-pad day 2)]]
                     [table-cell (. value toLocaleString)]])
                  entries))
       [:tr
        [table-cell (tr "Average")]
        [table-cell (. average toLocaleString)]]]]
     (note-if-incomplete complete?)]))

(defn lifetime-stats
  "Create the site lifetime stats section."
  []
  (let [stats @(subscribe [::admin/stats])]
    [:div.pb3
     [:h2 (tr "Site lifetime")]
     (when stats
       [stats-table stats])]))

(defn recent-stats
  "Create the site stats recent activity section. "
  []
  (let [stats-timespan @(subscribe [::admin/stats-timespan])
        complete? @(subscribe [::admin/stats-timespan-complete?])
        this-year @(subscribe [::admin/this-year])]
    [:div.pb3
     [:h2 (tr "Recent activity")]
     [:table.bw0.mb2
      [:tbody
       [:tr
        [:td.pa0.bw0 (tr "From: ")]
        [:td.bw0 [datepicker {:start-year 2016
                              :end-year this-year
                              :subscription [::admin/stats-since-date]
                              :setter [::admin/set-stats-since-date]}]]]
       [:tr
        [:td.pa0.bw0 (tr "Until: ")]
        [:td.bw0 [datepicker {:start-year 2016
                              :end-year this-year
                              :subscription [::admin/stats-until-date]
                              :setter [::admin/set-stats-until-date]}]]]]]
     (when stats-timespan
       [:div
        [recent-stats-table stats-timespan]
        [note-if-incomplete complete?]])]))

(defn daily-visitors
  "Create the site stats daily visitors section."
  []
  (let [this-year @(subscribe [::admin/this-year])]
    [:div.pb3
     [:h2 (tr "Daily visitors")]
     [:div.mb3
      [:div.di-ns.mr2-ns.pb1 (tr "Week beginning: ")]
      [datepicker {:class (cls :di-ns)
                   :start-year 2020
                   :end-year this-year
                   :subscription [::admin/visitor-count-date :daily]
                   :setter [::admin/set-visitor-count-date :daily]}]]
     [visitor-stats-table :daily]]))

(defn weekly-visitors
  "Create the site stats weekly visitors section."
  []
  (let [this-year @(subscribe [::admin/this-year])]
    [:div
     [:h2 (tr "Weekly visitors")]
     [:div.mb3
      [:div.di-ns.mr2-ns.pb1 (tr "Month beginning: ")]
      [datepicker {:class (cls :di-ns)
                   :start-year 2020
                   :end-year this-year
                   :subscription [::admin/visitor-count-date :weekly]
                   :setter [::admin/set-visitor-count-date :weekly]}]]
     [visitor-stats-table :weekly]]))

(defn stats-panel
  "Create a page to show the the admin stats dashboard, to admins only."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [lifetime-stats]  [:hr]
    [recent-stats]    [:hr]
    [daily-visitors]  [:hr]
    [weekly-visitors]]])

(defn add-button
  "Create the add button for the banned user name form."
  []
  (let [form ::admin-forms/ban-username-string
        dispatch-send #(dispatch [::forms/action form :send])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [button {:class [util/reset-padding :pv2.ph4.mv1]
                 :disabled editing-disabled?
                 :on-click dispatch-send}
         (if (= form-state :SENDING)
           (tr "Adding...")
           (tr "Add"))]))))

(defn banned-username-string-form
  "Create a form to add a new string to ban in usernames."
  []
  (let [form ::admin-forms/ban-username-string
        new-string (fn [] @(subscribe [::forms/field form :content]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])]
        [:<>
         [:p (tr "Enter a string to ban in usernames:")]
         [:div
          [text-input {:placeholder ""
                       :value new-string
                       :required true
                       :update-value! forms/reset-input-on-success!
                       :form-state form-state
                       :disabled false
                       :event [::forms/edit form :content]}]
          [validation-error-if-present form]
          [transmission-error-if-present form]
          [:div
           [add-button]]]]))))

(defn banned-username-string-table-row
  "Create a row in the banned strings in usernames table."
  [{:keys [banned users show more?]}]
  (let [unban #(dispatch [::admin/unban-username-string banned])
        body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100.pa2.ba.bw1.data-name-ns
                          (str "w-" percent "-ns")
                          body-colors border-color))]
    ^{:key banned}
    [:li.flex-ns
     [:div {:class (cell-class 25)
            :data-name (tr "String: ")}
      banned]
     [:div {:class (cls (cell-class 10) :tr-ns)
            :data-name (tr "# Users: ")}
      (count users)]
     [:div {:class (cell-class 50)
            :data-name (tr "Users: ")}
      (doall (interpose (tr ", ")
                        (map (fn [name]
                               ^{:key (str banned name)}
                               [userlink name])
                             (take show users))))
      (when more?
        [button
         {:class [util/reset-padding :dib.f6.pv1.mh2]
          :button-class :modal
          :on-click #(dispatch [::admin/show-all-users banned])}
         (tr "Show all")])]
     [:div {:class (cls (cell-class 15) :dn.db-ns)}
      [:a {:on-click unban}
       (tr "[x]")]]
     [:div.mb3
      [button {:class :dn-ns.db.mt1
               :button-class :secondary
               :on-click unban}
       "Remove"]]]))

(defn banned-user-name-list
  "Create the list of strings banned in user names."
  []
  (let [string-sort #(dispatch [::admin/set-ban-username-string-sort :string])
        count-sort #(dispatch [::admin/set-ban-username-string-sort :count])]
    (fn []
      (let [banned-strings @(subscribe [::admin/banned-username-strings])
            options @(subscribe [::admin/banned-username-sort-options])
            {:keys [sort-typ sort-asc?]} options
            sort-indicator (if sort-asc? " ▾" " ▴")
            header-colors @(subscribe [::window/table-header-colors])
            border-colors @(subscribe [::window/table-border-colors])
            header-class (cls :dn.dib-ns.b.pa2.ba.bw1
                              header-colors border-colors)]
        [:<>
         [:h2 (tr "Banned User Names")]
         [banned-username-string-form]
         (if (zero? (count banned-strings))
           [helper-text (tr "No banned strings, yet.")]
           [:ol.ph0.pt3.w-100.bn.list
            [:li.flex
             [:div
              {:class (cls :w-100.w-25-ns header-class)
               :on-click string-sort}
              (tr "String")
              (when (= sort-typ :string)
                sort-indicator)]
             [:div
              {:class (cls :w-100.w-10-ns header-class)
               :on-click count-sort}
              (tr "# Users")
              (when (= sort-typ :count)
                sort-indicator)]
             [:div {:class (cls :w-100.w-50-ns header-class)}
              (tr "Users")]
             [:div {:class (cls :w-100.w-15-ns header-class)}
              (tr "Remove")]]
            (doall (map banned-username-string-table-row
                        banned-strings))])]))))

(defn banned-user-names
  "Create the admin banned user name list."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [banned-user-name-list]]])

(defn- message-preview
  []
  (let [require-form ::admin-forms/require-name-change
        preview-open? @(subscribe [::forms/field require-form :preview-open?])]
    (when preview-open?
      [:<>
       [:h3 (tr "Message preview:")]
       [:div.bg-near-white.ph3.pv1
        [preview {:form require-form}]]])))

(defn- require-name-change-form
  "Create a form to set up a required name change."
  []
  (let [require-form ::admin-forms/require-name-change
        message (fn [] @(subscribe [::forms/field require-form :message]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state require-form])
            editing-disabled? @(subscribe [::forms/editing-disabled?
                                           require-form])]
        [:form
         [markdown-editor
          {:placeholder
           (tr
            "Write your message here. Styling with Markdown format is supported.")
           :value message
           :class :mb3.w-100.w-80-ns
           :buttonbar-class :w-100.w-80-ns
           :update-value! forms/reset-input-on-success!
           :form-state form-state
           :disabled editing-disabled?
           :event [::forms/edit require-form :message]}]
         [validation-error-if-present require-form]
         [transmission-error-if-present require-form]
         [button {:button-class :primary
                  :class :dib-ns.db.mt1
                  :on-click #(do
                               (.preventDefault %)
                               (dispatch [::forms/action require-form :send]))}
          (if (= form-state :SENDING)
            (tr "Sending...")
            (tr "Require name change"))]
         [button {:on-click #(do
                               (.preventDefault %)
                               (dispatch
                                [::forms/action require-form :see-preview]))
                  :class (cls [util/reset-padding :dib-ns.db.mt1.ml2-ns])
                  :button-class :secondary}
          (tr "Preview")]
         [message-preview]]))))

(defn- existing-required-name-change
  "Show the existing requirement to change username.
  p  Include a button to cancel it."
  []
  (let [info @(subscribe [::admin/require-name-change-info])
        {:keys [required-at admin-name message]} info
        state @(subscribe [::admin/cancel-required-name-change-state])
        form-background @(subscribe [::window/form-background])]
    [:<>
     [:p (trm-html
          "A name change requirement was set %{date} by %{admin}."
          {:date [relative-time {:datetime required-at}]
           :admin [userlink admin-name]})]
     [:h4 (tr "Message:")]
     [:div
      {:class (cls :mb3.ph2.mw7 form-background)}
      [dangerous-html {:class :dib} message]]
     [button {:button-class :primary
              :class :dib-ns.db.mt1
              :on-click #(dispatch [::admin/cancel-required-name-change])}
      (if (= state :SENDING)
        (tr "Cancelling...")
        (tr "Cancel required name change"))]]))

(defn- username-history-row
  [{:keys [name changed required-by-admin]}]
  ^{:key changed}
  (let [body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100-s.w-100-m.pa2.ba.bw1.data-name-sm
                          (str "w-" percent "-l")
                          body-colors border-color))]
    [:li.flex-l.pv2.pv0-l.pv0-m.list
     [:div {:class (cell-class 30)
            :data-name (tr "Previous name: ")}
      name]
     [:div {:class (cell-class 15)
            :data-name (tr "Used until: ")}
      [relative-time {:datetime changed
                      :prefix ""}]]
     [:div {:class (cell-class 15)
            :data-name (tr "Required by admin: ")}
      (if required-by-admin (tr "Yes") (tr "No"))]]))

(defn- username-history
  "Create the user's name history for the require name change panel."
  []
  (let [{:keys [name]} @(subscribe [:view/options])
        history @(subscribe [::admin/name-change-history])
        header-colors @(subscribe [::window/table-header-colors])
        border-colors @(subscribe [::window/table-border-colors])
        header-class (fn [percent]
                       (cls :dn.w-100.dib-l.b.ph1.pv2.ba.bw1
                            (str "w-" percent "-l")
                            header-colors border-colors))]
    [:div.pt2
     (if (empty? history)
       [helper-text (trm-html "%{user} has no previous name changes."
                              {:user [userlink name]})]
       [:div
        [:h3 (trm-html "%{user}'s name change history"
                       {:user [userlink name]})]
        [:div
         (into [:ol.mv0.ph0.pt1.w-100.bn.list
                [:li.flex
                 [:div {:class (header-class 30)} (tr "Previous name")]
                 [:div {:class (header-class 15)} (tr "Used until")]
                 [:div {:class (header-class 15)} (tr "Required by admin")]]])
         (into [:<>]
               (map username-history-row history))]])]))

(defn require-name-change-panel
  "Create the admin require name change panel."
  []
  (let [{:keys [name]} @(subscribe [:view/options])
        status @(subscribe [::window/status])
        info @(subscribe [::admin/require-name-change-info])]
    [admins-only
     (when (= :loaded status)
       [panel {:sidebar admin-sidebar}
        [:h2 (trm-html "Require %{user} to change username"
                       {:user [userlink name]})]
        (if info
          [existing-required-name-change]
          [require-name-change-form])
        [username-history]
        [markdown-link-modal]])]))

(defn reset-input-on-ready!
  "Reset a field when the form reaches the ready state for the first time."
  [state-atom {:keys [value form-state] :as properties}]
  (if (= form-state :READY)
    (when-not (:already-updated? @state-atom)
      (swap! state-atom assoc
             :value (value) :already-updated? true))
    (swap! state-atom assoc :already-updated? false))
  (dissoc properties :form-state))

(defn invite-code-settings-form
  "Create the form to modify admin invite code settings."
  []
  (let [form ::admin-forms/invite-code-settings
        minimum-level (fn [] @(subscribe [::forms/field form :minimum-level]))
        per-user (fn [] @(subscribe [::forms/field form :per-user]))
        submit #(dispatch [::forms/action form :save-changes])]
    (fn []
      (let [required? @(subscribe [::forms/field form :required?])
            visible? @(subscribe [::forms/field form :visible?])
            form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [:h2 (tr "Invite code settings")]
         [:div
          [:div.lh-copy
           [checkbox
            {:label (tr "Enable invite code to register")
             :label-class (cls :db)
             :value required?
             :event [::forms/edit form :required?]
             :form-state form-state}]
           [checkbox
            {:label
             (tr "Allow users to see who they invited and who invited them")
             :label-class (cls :db)
             :value visible?
             :event [::forms/edit form :visible?]
             :form-state form-state}]]
          [:div.pv1
           [:label
            {:class (cls :dib-ns.w-20.tr.v-mid.pr3)}
            (tr "Minimum level to create invite codes")]
           [text-input {:value minimum-level
                        :required true
                        :form-state form-state
                        :disabled editing-disabled?
                        :update-value! reset-input-on-ready!
                        :event [::forms/edit form :minimum-level]}]]
          [:div
           [:label
            {:class (cls :dib-ns.w-20.tr.v-mid.pr3)}
            (tr "Max amount of invites per user")]
           [text-input {:value per-user
                        :required true
                        :form-state form-state
                        :disabled editing-disabled?
                        :update-value! reset-input-on-ready!
                        :event [::forms/edit form :per-user]}]]
          [validation-error-if-present form]
          [transmission-error-if-present form]
          [:div.pv2
           [button
            {:button-class :primary
             :disabled editing-disabled?
             :on-click submit}
            (case form-state
              :SENDING (tr "Saving...")
              :SHOW-SUCCESS (tr "Saved!")
              (tr "Save"))]]]]))))

(defn generate-invite-code-form
  []
  (let [form ::admin-forms/generate-invite-code
        code (fn [] @(subscribe [::forms/field form :code]))
        max-uses (fn [] @(subscribe [::forms/field form :uses]))
        submit #(dispatch [::forms/action form :generate])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe
                                [::forms/editing-disabled? form])
            input-colors @(subscribe [::window/input-colors editing-disabled?])]
        [:div.pb3
         [:h2 (tr "Generate a new invite code")]
         [:div.pb2
          [text-input {:placeholder
                       (tr "Code (empty to generate random)")
                       :value code
                       :class :w-100
                       :required true
                       :form-state form-state
                       :disabled editing-disabled?
                       :update-value! forms/reset-input-on-success!
                       :event [::forms/edit form :code]}]] " "
         [:div
          [text-input {:placeholder (tr "Uses")
                       :value max-uses
                       :required true
                       :form-state form-state
                       :disabled editing-disabled?
                       :update-value! forms/reset-input-on-success!
                       :event [::forms/edit form :max-uses]}] " "
          [date-selector
           {:date-atom (subscribe [::forms/field form :expiration])
            :on-select #(dispatch [::forms/edit form :expiration %])
            :pikaday-attrs {:min-date (js/Date.)
                            :format "MM/DD/YYYY"}
            :input-attrs {:placeholder (tr "Expiration")
                          :auto-complete "off"
                          :class (cls :dib.lh-title.pa2.mv1.ba.bw1.br2.v-mid
                                      :shadow-6
                                      input-colors)}}] " "
          [button
           {:button-class :primary
            :class :dib-ns.db.mt1
            :disabled editing-disabled?
            :on-click submit}
           (if (= form-state :SENDING)
             (tr "Generating...")
             (tr "Generate"))]]
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-search-form
  "Create a form to search for invite codes."
  []
  (let [form ::admin-forms/search-invite-code
        code (fn [] @(subscribe [::forms/field form :code]))
        submit #(dispatch [::forms/action form :search])
        show-all #(dispatch [::forms/action form :show-all])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div
         [:label
          (tr "Search for a code: ")]
         [text-input {:value code
                      :required true
                      :form-state form-state
                      :disabled editing-disabled?
                      :update-value! (fn [_state-atom props]
                                       (dissoc props :form-state))
                      :event [::forms/edit form :code]}] " "
         [:div.dib-ns.pv2
          [button {:button-class :primary
                   :class :dib
                   :disabled editing-disabled?
                   :on-click submit}
           (if (= form-state :SENDING)
             (tr "Searching...")
             (tr "Search"))] " "
          [button {:button-class :secondary
                   :class :dib
                   :disabled editing-disabled?
                   :on-click show-all}
           (tr "Show all codes")]]
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-expiration-form
  "Create a form to change the expiration of selected codes."
  []
  (let [form ::admin-forms/expire-invite-code
        submit #(dispatch [::forms/action form :change])]
    (fn []
      (let [option @(subscribe [::forms/field form :option])
            form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])
            input-colors @(subscribe [::window/input-colors editing-disabled?])]
        [:div
         [:p
          (tr "Change selected codes to expire: ")]
         [radio-button
          {:name "expire-option" :id "expire-option-never"
           :disabled editing-disabled?
           :label (tr "Never")
           :value "never"
           :form-value (name option)
           :event [::forms/edit form :option :never]
           :form-state form-state}] " "
         [radio-button
          {:name "expire-option" :id "expire-option-now"
           :disabled editing-disabled?
           :label (tr "Now")
           :value "now"
           :form-value (name option)
           :event [::forms/edit form :option :now]
           :form-state form-state}] " "
         [radio-button
          {:name "expire-option" :id "expire-option-at"
           :disabled editing-disabled?
           :label (tr "At: ")
           :value "at"
           :form-value (name option)
           :event [::forms/edit form :option :at]
           :form-state form-state}]
         [date-selector
          {:date-atom (subscribe [::forms/field form :expiration])
           :on-select #(dispatch [::forms/edit form :expiration %])
           :pikaday-attrs {:min-date (js/Date.)
                           :format "MM/DD/YYYY"}
           :input-attrs {:placeholder (tr "Expiration")
                         :class (cls :dib.lh-title.pa2.mv1.ba.bw1.br2.v-mid
                                     :shadow-6
                                     input-colors)
                         :auto-complete "off"}}] " "
         [button
          {:button-class :primary
           :class :dib
           :disabled editing-disabled?
           :on-click submit}
          (if (= form-state :SENDING)
            (tr "Changing...")
            (tr "Change Expiration"))] " "
         [validation-error-if-present form]
         [transmission-error-if-present form]]))))

(defn invite-code-user
  "Create a user link for the invite codes table."
  [{:keys [name status]}]
  ^{:key name}
  [userlink name
   (case status
     "DELETED" (tr " (Deleted)")
     "PROBATION" (tr " (Pending)")
     "BANNED" (tr " (Banned)")
     "")])

(defn invite-code-table-row
  "Create a row in the invite codes table."
  [id]
  (let [ic @(subscribe [::admin/invite-code id])
        {:keys [code created-by created-on expires used-by uses
                max-uses expired?]} ic
        form ::admin-forms/expire-invite-code
        selected @(subscribe [::forms/field form :selected])
        editing-disabled? @(subscribe [::forms/editing-disabled? form])
        form-state @(subscribe [::forms/form-state form])
        body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100-s.w-100-m.pa2.ba.bw1.data-name-sm
                          (str "w-" percent "-l")
                          body-colors border-color))]
    ^{:key id}
    [:li.flex-l.pv2.pv0-l
     [:div {:class (cell-class 5)
            :data-name (tr " ")}
      [checkbox {:disabled editing-disabled?
                 :label ""
                 :value (contains? selected id)
                 :event [::admin/select-invite-code id]
                 :form-state form-state}]]
     [:div {:class (cell-class 30)
            :data-name (tr "Code: ")}
      [:span {:class (cls (when (or expired? (not= (:status created-by) "ACTIVE"))
                            :gray.strike))} code]]
     [:div {:class (cell-class 15)
            :data-name (tr "Created by: ")}
      [invite-code-user created-by]]
     [:div {:class (cell-class 15)
            :data-name (tr "Created on: ")} created-on]
     [:div {:class (cell-class 15)
            :data-name (tr "Expires: ")} expires]
     [:div {:class (cls (cell-class 5) :tr-l)
            :data-name (tr "Uses: ")} uses]
     [:div {:class (cls (cell-class 5) :tr-l)
            :data-name (tr "Max uses: ")} max-uses]
     [:div {:class (cell-class 10)
            :data-name (tr "Used by: ")}
      (doall (interpose (tr ", ")
                        (map invite-code-user used-by)))]]))

(defn invite-code-table-body
  "Create the body of the invite code table."
  []
  (let [invite-code-ids @(subscribe [::admin/invite-code-ids])]
    (into [:span]
          (doall (map invite-code-table-row invite-code-ids)))))

(defn invite-code-table
  "Create a table of invite codes."
  []
  (let [on-load (fn [elem]
                  (reagent/after-render
                   #(dispatch [::admin/set-bottom-of-invite-code-table elem])))]
    (fn []
      (let [header-colors @(subscribe [::window/table-header-colors])
            border-colors @(subscribe [::window/table-border-colors])
            header-class (fn [percent]
                           (cls :dn.w-100.dib-l.b.ph1.pv2.ba.bw1
                                (str "w-" percent "-l")
                                header-colors border-colors))]
        [:div
         (into [:ol.ph0.pt3.w-100.bn.list
                [:li.flex
                 [:div {:class (header-class 5)}]
                 [:div {:class (header-class 30)} (tr "Code")]
                 [:div {:class (header-class 15)} (tr "Created by")]
                 [:div {:class (header-class 15)} (tr "Created on")]
                 [:div {:class (header-class 15)} (tr "Expires")]
                 [:div {:class (header-class 5)} (tr "Uses")]
                 [:div {:class (header-class 5)} (tr "Max uses")]
                 [:div {:class (header-class 10)} (tr "Used by")]]
                [invite-code-table-body]])
         [:p {:ref on-load}]]))))

(defn invite-codes-listing
  "Create the search, actions and invite code table."
  []
  (let [codes-empty? @(subscribe [::admin/invite-codes-empty?])
        search-code @(subscribe [::admin/invite-code-search-code])
        loading? (= @(subscribe [::window/status]) :loading)]
    [:div
     [:h2 (tr "Invite codes")]
     [invite-code-search-form]
     (when-not loading?
       (if codes-empty?
         (if (= search-code :all)
           [:h3 (tr "No invite codes have been created yet")]
           [:h3 (tr "No invite codes found matching '%s'" search-code)])
         [:<>
          [invite-code-expiration-form]
          [invite-code-table]]))]))

(defn invite-codes-panel
  "Create the admin invite code page."
  []
  [admins-only
   [panel {:sidebar admin-sidebar}
    [:div
     [invite-code-settings-form]
     [:hr]
     [generate-invite-code-form]
     [:hr]
     [invite-codes-listing]]]])

(defn vote-type-selector
  "Create a control to determine which types of votes to show."
  []
  (let [types (:types @(subscribe [:view/options]))]
    [:div
     [radio-button
      {:name "vote-type-posts" :id "vote-type-posts"
       :class (cls :db.dib-ns.ph2)
       :label (tr "Post votes")
       :value :post
       :form-value types
       :event [::admin/show-vote-type]}]
     [radio-button
      {:name "vote-type-comments" :id "vote-type-comments"
       :class (cls :db.dib-ns.ph2)
       :label (tr "Comment votes")
       :value :comment
       :form-value types
       :event [::admin/show-vote-type]}]
     [radio-button
      {:name "vote-type-all" :id "vote-type-all"
       :class (cls :db.dib-ns.ph2)
       :label (tr "All votes")
       :value :all
       :form-value types
       :event [::admin/show-vote-type]}]]))

(defn vote-table-row
  [{:keys [cid pid post-slug direction sub-name name datetime]}]
  (let [body-colors @(subscribe [::window/table-body-colors])
        border-color @(subscribe [::window/table-border-colors])
        cell-class (fn [percent]
                     (cls :w-100-s.w-100-m.pa2.ba.bw1.data-name-sm
                          :truncate
                          (str "w-" percent "-l")
                          body-colors border-color))]
    ^{:key (or cid pid)}
    [:li.flex-l.pv2.pv0-l
     [:div {:class (cell-class 25)
            :data-name (tr "Content: ")}
      (if cid
        [:a {:href (url-for :sub/view-direct-link
                            :sub sub-name
                            :pid pid
                            :slug (or post-slug "_")
                            :cid cid)}
         (tr "comment on %s" pid)]
        [:a {:href (url-for :sub/view-single-post
                            :sub sub-name
                            :pid pid
                            :slug (or post-slug "_"))}
         (tr "post %s" pid)])]
     [:div {:class (cell-class 10)
            :data-name (tr "Vote: ")}
      [:span (if (= :UP direction) (tr "up") (tr "down"))]]
     [:div {:class (cell-class 20)
            :data-name (tr "Author: ")} name]
     [:div {:class (cell-class 20)
            :data-name (tr "Circle: ")} sub-name]
     [:div {:class (cell-class 25)
            :data-name (tr "Date: ")} datetime]]))

(defn- vote-table-body
  []
  (let [votes @(subscribe [::admin/votes])]
    (into [:span]
          (doall (map vote-table-row votes)))))

(defn- vote-table
  []
  (let [header-colors @(subscribe [::window/table-header-colors])
        border-colors @(subscribe [::window/table-border-colors])
        header-class (fn [percent]
                       (cls :dn.w-100.dib-l.b.pa2.ba.bw1
                            (str "w-" percent "-l")
                            header-colors border-colors))]
    [:div
     (into [:ol.ph0.pt3.w-100.bn.list
            [:li.flex
             [:div {:class (header-class 25)} (tr "Content")]
             [:div {:class (header-class 10)} (tr "Vote")]
             [:div {:class (header-class 20)} (tr "Author")]
             [:div {:class (header-class 20)} (tr "Circle")]
             [:div {:class (header-class 25)} (tr "Date")]]
            [vote-table-body]])]))

(defn- vote-page-controls
  []
  (let [{:keys [has-next-page has-previous-page start-cursor
                end-cursor]} @(subscribe [::admin/vote-page-info])
        {:keys [name types]} @(subscribe [:view/options])
        colors @(subscribe [::window/button-colors :secondary])
        class (cls :dib.pa2.f6.br1.bn.pointer colors)]
    [:div
     (when has-previous-page
       [:a {:href (url-for :admin/voting :name name
                           :query-args {:last admin/votes-page-size
                                        :before start-cursor
                                        :types types})
            :class (cls class :mr1)}
        (tr "Previous")])
     (when has-next-page
       [:a {:href (url-for :admin/voting :name name
                           :query-args {:first admin/votes-page-size
                                        :after end-cursor
                                        :types types})
            :class class}
        (tr "Next")])]))

(defn- downvote-notifications
  []
  (let [notifs @(subscribe [::admin/downvote-notifications])
        more? @(subscribe [::admin/more-downvote-notifications?])
        bg @(subscribe [::window/emphasis-background])]
    (when (seq notifs)
      [:div.pt1.pb2.ph2.mv3.mw7.br1.nested-list-reset {:class bg}
       (into [:ul.list]
             (map (fn [{:keys [time html-content]}]
                    [:li.downvote-notifs.flex-ns
                     [:div.helper-text.nowrap
                      "[" [relative-time {:prefix ""
                                          :datetime time}] "]"]
                     [:div.ml2
                      [dangerous-html {:class :dib} html-content]]])
                  notifs))
       [:a {:href (url-for :modmail/thread :mailbox :notifications
                           :msg (-> notifs first :thread-id))}
        (if more?
          (tr "More in modmail notifications")
          (tr "Modmail notifications"))]])))

(defn- remove-votes-button
  []
  (let [{:keys [name]} @(subscribe [:view/options])]
    [button {:button-class :primary
             :class :mt3
             :on-click #(dispatch [::admin/toggle-confirm-remove-votes])}
     (tr "Remove all votes by %s" name)]))

(defn- vote-removal-progress-indicator
  []
  (let [percent @(subscribe [::admin/removal-percent])
        day? @(subscribe [::window/day?])]
    (when percent
      [:div.mw7
       [:div {:class (cls :pa0.bw0.w-100.tl.relative
                          (if day? :bg-white :bg-black))}
        [:div.min-h-100.absolute.br2
         {:class (cls (if day?
                        :bg-washed-purple
                        :bg-dark-gray))
          :style {:width (str percent "%")}}]
        [:div.min-h-100.w-100.absolute.ba.br2.bw1
         {:class (if day? :b--purple :b--dark-purple)}]
        [:div.absolute.min-h-100.w-100 {:class (if day? :black :gray)}
         [:div.pv2.ph2
          [:span.pr2 {:class (if day? :purple :dark-purple)}
           percent "%"]]]
        ;; Relative child shows up behind the absolute children,
        ;; but we need it to set the height of the parent.
        ;; Make it have invisible text because otherwise the text
        ;; looks smeary.
        [:div.w-100.transparent.pv2.ph2 percent "%"]]
       [helper-text
        "Vote removal will continue even if you leave this page.
         You may return to the page to check progress."]])))

(defn- stop-remove-votes-button
  []
  [button {:button-class :primary
           :class :mt3
           :on-click #(dispatch [::admin/stop-remove-votes])}
   "Stop removing votes"])

(defn- vote-listing
  "Create the listing of user votes."
  []
  (let [votes @(subscribe [::admin/votes])
        state @(subscribe [::admin/vote-removal-state])]
    [:div
     (case state
       :removing [:<>
                  [vote-removal-progress-indicator]
                  [stop-remove-votes-button]]
       :show-votes [:<>
                    [downvote-notifications]
                    [vote-type-selector]
                    (if (empty? votes)
                      [:h3 "No votes found"]
                      [:div
                       [vote-table]
                       [vote-page-controls]])]
       :finished [:p "Vote removal complete."]
       nil)]))

(defn- confirm-remove-votes
  [{:keys [cancel]}]
  [:div
   [:p (tr "Are you sure?")]
   [helper-text "This action can not be undone."]
   [button {:button-class :primary
            :class :dib.mb2.mb0-ns.mr3
            :on-click #(dispatch [::admin/remove-votes])}
    (tr "Remove votes")]
   [button {:button-class :secondary
            :class :dib.mr2.mb2.mb0-ns.mr3
            :on-click cancel}
    (tr "Cancel")]])

(defn- modal-confirm-remove-votes
  "Create a confirmation modal for removing votes."
  []
  (let [toggle-modal #(dispatch [::admin/toggle-confirm-remove-votes])
        show-modal? (fn []
                      @(subscribe [::admin/show-confirm-remove-votes?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [confirm-remove-votes
        {:cancel toggle-modal}]])))

(defn voting-panel
  "Create a page to show a user's voting history to admins."
  []
  (let [{:keys [name]} @(subscribe [:view/options])
        state @(subscribe [::admin/vote-removal-state])
        {:keys [upvotes-given
                downvotes-given]} @(subscribe [::admin/viewed-user-stats])]
    [admins-only
     [panel {:sidebar admin-sidebar}
      [:div.flex-ns.justify-between
       [:h2 (trm-html (if (= :removing state)
                        "Removing votes made by %{user}"
                        "Votes made by %{user}")
                      {:user [userlink name]})
        (when (and (= :show-votes state) upvotes-given)
          (tr " (+%s/-%s)" upvotes-given downvotes-given))]
       (when (and (= :show-votes state)
                  upvotes-given
                  (or (pos? upvotes-given) (pos? downvotes-given)))
         [:div.mb3.mb0-ns
          [remove-votes-button]])]
      [vote-listing]
      [modal-confirm-remove-votes]]]))
