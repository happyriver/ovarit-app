;; fe/views/panels.cljs -- Create panels for ovarit-app
;; Copyright (C) 2022-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.panels
  (:require
   [ovarit.app.fe.views.errors :refer [error-bar]]
   [ovarit.app.fe.views.menu :refer [menu]]
   [ovarit.app.fe.views.sidebar :refer [footer sidebar]]
   [ovarit.app.fe.views.sub-bar :refer [sub-bar]]
   [reagent.core :as reagent]))

(defn panel
  "Create a panel to show some content."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        props (reagent/props this)]
    [:span
     (dissoc props :sub-bar :menu :error-bar :sidebar :footer)
     [(or (:sub-bar props) sub-bar)]
     [(or (:menu props) menu)]
     [:div.relative.pure-g
      [:div.pure-u-1.pure-u-md-18-24
       [(or (:error-bar props) error-bar)]
       (into [:div.z-0.ph2.ph3-ns.pv3] children)]
      [(or (:sidebar props) sidebar)]]
     [(or (:footer props) footer)]]))

(defn center-panel
  "Create a panel with centered content and no sidebar."
  []
  (let [this (reagent/current-component)
        children (reagent/children this)
        props (reagent/props this)]
    [:span
     [(or (:sub-bar props) sub-bar)]
     [(or (:menu props) menu)]
     [:div
      [(or (:error-bar props) error-bar)]
      [:div.flex.justify-center
       (into [:div.ph2.ph4-ns.pv3] children)]]
     [(or (:footer props) footer)]]))
