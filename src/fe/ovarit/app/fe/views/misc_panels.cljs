;; fe/views/misc-panels.cljs -- Miscellaneous little panels for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.misc-panels
  (:require
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.routes :as routes]
   [ovarit.app.fe.tr :refer [tr trm-html]]
   [ovarit.app.fe.views.menu :refer [simplified-menu]]
   [ovarit.app.fe.views.panels :refer [center-panel]]
   [ovarit.app.fe.views.util :refer [url-for]]
   [re-frame.core :refer [subscribe]]))

(defn page-not-found-panel
  "Create a page to show a routing failure."
  []
  [center-panel {:menu simplified-menu}
   [:h1 (tr "Not found")]
   [:p (tr "The page you were looking for does not exist.")]])

(defn unauthenticated-panel
  "Create a page to tell people they have to log in first."
  []
  (let [url @(subscribe [::routes/url])]
    [center-panel
     [:h1 (tr "You are not logged in")]
     [:p (trm-html "Please %{login} to continue."
                   {:login [:a {:href (url-for :auth/login
                                               :query-args {:next url})} "log in"]})]]))

(defn unauthorized-panel
  "Create a page to tell people they're trying to go where they don't belong."
  []
  [center-panel
   [:h1 (tr "Authorized personnel only")]
   [:p (tr "The page you were looking for is not available to you.")]])

(defn waiting-panel
  "Create a page to distract people while talking to the server."
  []
  [center-panel {:menu simplified-menu}
   [:h4 (tr "Loading...")]])

(defn halt-and-catch-fire-panel
  "Create a page to show some other kind of an error."
  []
  [center-panel {:menu simplified-menu}
   [:h1 (tr "Something went wrong.")]])

(defn unable-to-contact-server-panel
  "Create a page to show a network error."
  []
  [center-panel {:menu simplified-menu}
   [:h1 (tr "Unable to contact server.")]])

(defn error-panel
  "Create a page to show an error."
  []
  (let [options @(subscribe [:view/options])]
    (case (:error options)
      :unauthorized [unauthorized-panel]
      :unmatched-route [page-not-found-panel]
      :network-error [unable-to-contact-server-panel]
      [halt-and-catch-fire-panel])))

(defn initializing-panel
  "Show a blank page in production, and logs to the developer."
  []
  [:div
   (when config/debug?
     (let [logs @(subscribe [::settings/logs])]
       (when (seq logs)
         [:div.ph2
          [:h3 "Logs"]
          [:p.i.gray "See the devtools console for more details"]
          (into [:ul.list]
                (map
                 (fn [{:keys [time level message logger-name]}]
                   ^{:key time}
                   [:li
                    [:span.ph2.red level]
                    [:span.ph2 logger-name]
                    [:span.ph2 (:message message)]])
                 logs))])))])
