;; fe/views/single-post.cljs -- Single post panel for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.single-post
  (:require
   [ovarit.app.fe.content.comment :as comment]
   [ovarit.app.fe.content.comment.moderation :as-alias comment-mod]
   [ovarit.app.fe.content.post :as post]
   [ovarit.app.fe.content.post.moderation :as-alias post-mod]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.tr :refer [tr trm-html trn]]
   [ovarit.app.fe.ui.forms.compose-comment :as comment-form]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.forms.post :as post-forms]
   [ovarit.app.fe.ui.forms.report :as report-form]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [blur-wrapper dangerous-html helper-text
                                       nsfw-tag modal user-flair userlink
                                       relative-time]]
   [ovarit.app.fe.views.flair :refer [post-flair]]
   [ovarit.app.fe.views.forms :refer [preview
                                      transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.inputs :refer [button dropdown-menu markdown-editor
                                       markdown-link-modal
                                       text-input textarea-input]]
   [ovarit.app.fe.views.login :refer [sign-up-form-content]]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.sidebar :refer [single-post-sidebar]]
   [ovarit.app.fe.views.util :refer [cls url-for] :as util]
   [re-frame-fx.dispatch :refer [dispatch-debounce]]
   [re-frame.core :refer [dispatch subscribe]]
   [reagent.core :as reagent]))

(def comment-reply-button-classes
  "Classes (for util/cls) for the small buttons on the edit forms."
  [util/reset-padding :dib.pa2.f7.mr1])

(def post-reply-button-classes
  "Classes (for util/cls) for the buttons on the post reply form."
  [util/reset-padding :dib.pa2.f6.mr1])

(def button-classes
  {:small comment-reply-button-classes
   :large post-reply-button-classes})

(defn- post-button
  "Create the post button for a comment composition form."
  [{:keys [form size]} text sending-text]
  (let [form-state @(subscribe [::forms/form-state form])
        editing-disabled? @(subscribe [::forms/editing-disabled? form])]
    [button {:class (button-classes size)
             :button-class :primary
             :disabled editing-disabled?
             :on-click #(dispatch [::forms/action form :send])}
     (if (= form-state :SENDING)
       sending-text
       text)]))

(defn- preview-button
  "Create the preview button for a comment composition form."
  [{:keys [form size button-class]}]
  [button {:on-click #(dispatch [::forms/action form :see-preview])
           :class (button-classes size)
           :button-class (or button-class :secondary)}
   (tr "Preview")])

(defn- cancel-link
  "Create the cancel link for a comment composition form."
  [{:keys [event size]}]
  [:div {:class (cls (button-classes size) :pointer)
         :on-click #(dispatch event)}
   "Cancel"])

(defn- edit-history-button
  "Create a button for the edit history widget."
  [{:keys [secondary? see-next]} text]
  [button {:class [util/reset-padding :dib.f6.ph1.mr2]
           :button-class (if secondary? :secondary :primary)
           :on-click (when-not secondary? see-next)}
   text])

(defn- edit-history
  "Create an edit history widget.
  - `see-older` an event vector to show an older version.
  - `see-newer` an event vector to show a newer version.
  - `state` a history state map, see ::comment/edit-history-state
  - `text` text content for the widget."
  [{:keys [see-older see-newer]} _]
  (let [see-older #(dispatch see-older)
        see-newer #(dispatch see-newer)]
    (fn [{:keys [state]} text]
      (let [{:keys [show-history? oldest? newest?]} state]
        (when show-history?
          [:div.pv1
           [edit-history-button
            {:secondary? oldest? :see-next see-older}
            "←"]
           [edit-history-button
            {:secondary? newest? :see-next see-newer}
            "→"]
           [:span.f6.gray
            text]])))))

(defn- post-vote-arrow
  "Create a vote arrow for the post header."
  [{:keys [class] :as props} icon-name]
  [icon (assoc props :class (cls :db.center.h1.w1 class))
   icon-name])

(defn- vote-colors
  "Create a list of classes for a vote arrow."
  [direction vote can-vote?]
  (let [day? @(subscribe [::window/day?])]
    (if can-vote?
      [(if (= vote direction)
         (if (= :UP direction)
           :fill-light-purple
           :fill-medium-blue)
         :fill-silver)
       (if (= :UP direction)
         :hover-stroke-light-purple
         :hover-stroke-medium-blue)
       :pointer]
      [(if (= vote direction)
         (if (= :UP direction)
           :stroke-light-purple
           :stroke-medium-blue)
         :stroke-silver)
       (if day? :fill-white :fill-black)])))

(defn- votebuttons
  "Create the vote buttons for the single post page."
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])
        is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
        event (if is-authenticated?
                [::post/vote-post]
                [::post/toggle-login-to-continue])
        {:keys [score upvotes downvotes]} @(subscribe [::post/single-post])
        vote (:vote @(subscribe [::post/user-attributes]))
        can-vote? @(subscribe [::post/can-vote?])]
    [:div.pv1.self-center
     [:<>
      [post-vote-arrow {:title (when can-vote? (tr "Vote up"))
                        :class (vote-colors :UP vote can-vote?)
                        :on-click (when can-vote?
                                    #(dispatch (conj event :UP)))}
       "upvote"]
      [:div.self-center.db.tc.f5.pv2.lh-title
       (if is-mod-or-admin?
         [:abbr {:title (tr "+%s/-%s" upvotes downvotes)} score]
         score)]
      [post-vote-arrow {:title (when can-vote? (tr "Vote down"))
                        :class (vote-colors :DOWN vote can-vote?)
                        :on-click (when can-vote?
                                    #(dispatch (conj event :DOWN)))}
       "downvote"]]]))

(defn- thumbnail
  "Create the post thumbnail for the single post page."
  []
  (let [{:keys [link thumbnail]} @(subscribe [::post/single-post])
        blur @(subscribe [::post/single-post-blur])
        embed? (or @(subscribe [::post/embeddable-image?])
                   @(subscribe [::post/embeddable-video?])
                   @(subscribe [::post/iframe-video-link]))
        make-icon (fn [name]
                    [icon {:class (cls :dn.dib-ns.w3.h3.self-center
                                       :fill-dark-gray)}
                     name])]
    (cond
      embed? (make-icon "link")
      (nil? link) (make-icon "chat")
      (empty? thumbnail) (make-icon "link")
      :else [blur-wrapper {:class (cls :dn.dib-ns)
                           :blur blur}
             [:img {:src thumbnail}]])))

(defn- post-flair-link
  "Create the flair link for the post title, if there is one."
  []
  (let [{:keys [flair]} @(subscribe [::post/single-post])
        sub-name @(subscribe [::subs/normalized-sub-name])
        flair-colors @(subscribe [::window/post-flair-colors
                                  {:selectable? true}])]
    (when flair
      [:a {:href (url-for :sub/view-sub-hot
                          :sub sub-name :query-args {:flair flair})
           :class (cls :custom.ph2.pv1.mr2.br2.f4.pointer flair-colors)}
       flair])))

(defn- post-title-edit-history
  "Create the edit history for the post title."
  []
  (let [state @(subscribe [::post/post-title-edit-history-state])
        {:keys [newest? oldest? index num-edits by-mod? author]} state
        by-mod (if by-mod?
                 (tr " [edited by %s]" (or author "[Deleted]"))
                 "")]
    [edit-history
     {:see-older [::post/post-title-see-older-version]
      :see-newer [::post/post-title-see-newer-version]
      :state state}
     (cond
       newest? (tr "Viewing most recent title edit (%s/%s)%s"
                   num-edits num-edits by-mod)
       oldest? (tr "Viewing original title (1/%s)" num-edits)
       :else (tr "Viewing title edit history (%s/%s)%s"
                 (inc index) num-edits by-mod))]))

(defn- post-title-and-history
  "Create the post title link, with blur, deletion indicator and history."
  []
  (let [{:keys [link status]} @(subscribe [::post/single-post])
        title @(subscribe [::post/post-title])
        blur @(subscribe [::post/single-post-blur])
        status-text (case status
                      :DELETED (tr "[Deleted]")
                      :DELETED_BY_USER (tr "[Deleted by User]")
                      :DELETED_BY_MOD (tr "[Deleted by Mod]")
                      :DELETED_BY_ADMIN (tr "[Deleted by Admin]")
                      nil)
        domain @(subscribe [::post/single-post-domain])]
    [:<>
     [:div.f3.pv1.dib
      [post-flair-link]
      (when status-text
        [:span.pr2 status-text])
      [blur-wrapper {:blur blur}
       (if link
         [:a {:class (cls :custom.purple.hover-light-purple)
              :target "_blank" :rel "noopener nofollow ugc"
              :href link}
          title]
         [:span title])]
      (when domain
        [:a.custom.pl1.mid-gray.f6.pointer.dib
         {:href (url-for :home/all-domain-new :domain domain)}
         "(" domain ")"])]
     [post-title-edit-history]]))

(defn- post-title-editor
  "Create the editor for the post title."
  []
  (let [form ::post-forms/edit-title
        title (fn [] @(subscribe [::forms/field form :title]))
        reason (fn [] @(subscribe [::forms/field form :reason]))]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            author? @(subscribe [::post/is-post-author?])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])
            day? @(subscribe [::window/day?])]
        [:div.pa2
         {:class (if day? :bg-near-white :bg-near-black)}
         [:p (tr "Update title:")]
         [textarea-input {:value title
                          :class :w-100.mb2
                          :required true
                          :form-state form-state
                          :update-value! forms/reset-input-on-success!
                          :disabled editing-disabled?
                          :event [::forms/edit form :title]}]
         (when-not author?
           [:div
            [:p (tr "Why are you editing the title of this post?")]
            [text-input {:value reason
                         :class :w-100.mb3
                         :required true
                         :form-state form-state
                         :update-value! forms/reset-input-on-success!
                         :disabled editing-disabled?
                         :event [::forms/edit form :reason]}]])
         [validation-error-if-present form]
         [transmission-error-if-present form]
         [post-button {:form form :size :small}
          "Save changes" "Saving..."]
         [cancel-link {:event [::post/toggle-edit-post-title]
                       :size :small}]]))))

(defn- post-title
  "Create the post title, or the editor to modify it."
  []
  (let [show-editor? @(subscribe [::post/show-post-title-editor?])]
    (if show-editor?
      [post-title-editor]
      [post-title-and-history])))

(defn- post-author-link
  "Create the author link for a post heading."
  []
  (let [post @(subscribe [::post/single-post])
        {:keys [distinguish]} post
        author @(subscribe [::post/single-post-author])
        {name :name author-status :status} author
        author-flair @(subscribe [::post/single-post-author-flair])
        colors @(subscribe [::window/distinguish-colors distinguish])]
    [:<>
     [:span {:class (cls colors :f6
                         (when distinguish
                           :pa05.br1))}
      (when name
        [userlink (when colors {:class (cls :custom colors)})
         name
         (when distinguish
           [:<> " "
            (case distinguish
              :ADMIN (tr "[speaking as admin]")
              :MOD (tr "[speaking as mod]")
              nil)])])
      (when (= author-status :DELETED)
        [:a (tr " [Deleted]")])]
     (when author-flair
       [:span.ml1
        [user-flair author-flair]])]))

(defn- post-author
  []
  (let [sub-name @(subscribe [::subs/normalized-sub-name])
        sub-link [:a {:href (url-for :sub/view-sub :sub sub-name)}
                  sub-name]
        {:keys [posted title-edited]} @(subscribe [::post/single-post])
        is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])]
    [:<>
     [:div.mt1.f6.mid-gray
      (trm-html "Posted %{posted} by %{user} in %{sub}"
                {:posted [relative-time {:datetime posted}]
                 :user [post-author-link]
                 :sub sub-link})]
     (when (and title-edited (not is-mod-or-admin?))
       [:div.mt1.f6.mid-gray
        (trm-html "Title edited %{edited} by a moderator"
                  {:edited
                   [relative-time {:datetime title-edited}]})])]))

(defn- post-flair-picker-header
  "Create some explanatory text for the post flair picker."
  []
  [:<>
   [:h3 (tr "Select flair")]
   [helper-text
    (tr "The selected flair will be shown next to the title of the post.")]])

(defn- flair-choices-map
  "Turn a list of flair objects into an ordered map for `dropdown-menu`.
  Include an entry for the case when there is no selection."
  [flairs]
  (->> flairs
       (map (fn [{:keys [id text]}] [id text]))
       (into [:nothing-selected
              (tr "Choose a flair")])
       flatten
       (apply array-map)))

(defn- post-flair-picker
  "Create a display and/or dropdown of flairs for the user to pick one."
  [form]
  (let [change-event [::forms/action form :change]
        change (fn [id] #(dispatch (conj change-event id)))]
    (fn [form]
      (let [disabled? @(subscribe [::forms/editing-disabled? form])
            selected-id @(subscribe [::forms/field form :selected-id])
            flairs @(subscribe [::forms/field form :presets])
            too-many-flairs-for-mobile? (> (count flairs) 20)]
        (when (seq flairs)
          [:<>
           [post-flair-picker-header]
           ;; When there are a lot of flairs, do a dropdown version
           ;; of the form for mobile.
           (when too-many-flairs-for-mobile?
             [:div.dn-ns.pb3
              [dropdown-menu {:choices (flair-choices-map flairs)
                              :ul-class :maxh7.overflow-auto
                              :disabled disabled?
                              :value selected-id
                              :event change-event
                              :scrollable? true
                              :wide? true}]])
           ;; Show the flairs as buttons if there aren't that many or if the
           ;; screen is large.
           [:div {:class (when too-many-flairs-for-mobile?
                           (cls :dn.db-m.db-l))}
            (into [:div.flex.flex-wrap.pb3]
                  (map (fn [{:keys [id text]}]
                         ^{:key id}
                         [:span.pr1.pv2.mr2
                          {:on-click (when-not disabled? (change id))}
                          [post-flair {:selected? (= id selected-id)} text]]))
                  flairs)]])))))

(defn- post-flair-content
  "Show a modal for the user to remove or change post flair."
  [{:keys [cancel]}]
  (let [form ::post-forms/select-flair
        remove-fn #(dispatch [::forms/action form :remove])]
    (fn [_]
      (let [disabled? @(subscribe [::forms/editing-disabled? form])
            flair @(subscribe [::forms/field form :flair])
            user-must-flair @(subscribe [::forms/field form :user-must-flair])
            is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])]
        [:div
         [post-flair-picker form]
         [validation-error-if-present form]
         [transmission-error-if-present form]
         [:div.pt2
          (when (and flair (or (not user-must-flair) is-mod-or-admin?))
            [button {:button-class :modal
                     :class :dib.mb2.mb0-ns.mr3
                     :on-click (when-not disabled?
                                 remove-fn)}
             (if disabled?
               (tr "Updating")  ; Could be setitng or removing flair.
               (tr "Remove flair"))])
          [button {:button-class :modal
                   :class :dib
                   :on-click cancel}
           (tr "Cancel")]]]))))

(defn- modal-post-flair-form
  "Create the form that allows the user to change the post flair."
  []
  (let [toggle-modal #(dispatch [::post/edit-single-post-flair])
        show-modal? (fn [] @(subscribe [::post/show-single-post-flair-modal?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal
              :background :bg-black-50}
       [post-flair-content {:cancel toggle-modal}]])))

(defn- report-form-field-label
  "Create a field label for a report form."
  [text]
  (let [day? @(subscribe [::window/day?])]
    [:p {:class (when-not day? :purple)}
     text]))

(defn- report-reason-picker
  "Create a dropdown to choose a reporting reason."
  [form]
  (let [reason @(subscribe [::forms/field form :reason])
        reason-choices {:nothing-selected (tr "Select one...")
                        :site-rule (tr "Sitewide Rule violation")
                        :sub-rule (tr "Circle Rule violation")
                        :other (tr "Other")}
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:<>
     [report-form-field-label
      (tr "Select a reason to report this content:")]
     [dropdown-menu {:value reason
                     :title-class [:ba.bw1.br2.b--moon-gray
                                   util/reset-padding :pa2]
                     :ul-class [util/reset-padding :pa2]
                     :choices reason-choices
                     :disabled? disabled?
                     :event [::forms/edit form :reason]
                     :wide? true}]]))

(defn- report-explanation-field
  "Create a field to enter a report explanation."
  [form]
  (let [value (fn [] @(subscribe [::forms/field form :explanation]))]
    (fn [_]
      (let [form-state @(subscribe [::forms/form-state form])
            disabled? @(subscribe [::forms/editing-disabled? form])]
        [:div.pv2
         [report-form-field-label
          (tr "Explain why you're reporting this content:")]
         [text-input {:value value
                      :class :w-100.mb2
                      :required true
                      :form-state form-state
                      :update-value! forms/reset-input-on-success!
                      :disabled disabled?
                      :event [::forms/edit form :explanation]}]]))))

(defn- report-rule-picker
  "Create a dropdown to choose a sub rule."
  [form]
  (let [rule @(subscribe [::forms/field form :rule])
        rules @(subscribe [::subs/rules])
        rule-choices (concat [[:nothing-selected (tr "Select one...")]]
                             (map (fn [{:keys [id text]}]
                                    [id text])
                                  rules)
                             [[:other (tr "Other Circle Rule")]])
        disabled? @(subscribe [::forms/editing-disabled? form])]
    [:div.pv2
     [report-form-field-label
      (tr "Which circle rule did this content violate?")]
     [dropdown-menu {:value rule
                     :title-class [:ba.bw1.br2.b--moon-gray
                                   util/reset-padding :pa2]
                     :ul-class [util/reset-padding :pa2]
                     :choices rule-choices
                     :disabled? disabled?
                     :event [::forms/edit form :rule]
                     :scrollable? true
                     :wide? true}]
     (when (= :other rule)
       [report-explanation-field form])]))

(defn- report-form-content
  "Create the report content form content."
  [{:keys [cancel form]}]
  (let [send-fn #(dispatch [::forms/action form :send])]
    (fn [_]
      (let [disabled? @(subscribe [::forms/editing-disabled? form])
            form-state @(subscribe [::forms/form-state form])
            reason @(subscribe [::forms/field form :reason])]
        [:<>
         [:h2 (tr "Report Content")]
         [helper-text (tr "This report will be sent to the circle moderators.")]
         [report-reason-picker form]
         (case reason
           :sub-rule [report-rule-picker form]
           :other [report-explanation-field form]
           nil)
         [validation-error-if-present form]
         [transmission-error-if-present form]
         [:div.pt3
          [button {:button-class :primary
                   :class :dib.mb2.mb0-ns.mr3
                   :on-click (when-not disabled?
                               send-fn)}
           (if (= :SENDING form-state)
             (tr "Submitting...")
             (tr "Submit report"))]
          [button {:button-class :modal
                   :class :dib
                   :on-click cancel}
           (tr "Cancel")]]]))))

(defn- report-success-notice
  "Create the notice that a report was submitted."
  []
  [:p
   "Your report has been sent and will be reviewed by the circle moderators."])

(defn- modal-report-form
  "Create the form to enter a report reason."
  []
  (let [toggle-modal #(dispatch [::post/toggle-report-content-form])
        show-modal? (fn [] @(subscribe [::post/show-report-content-form?]))
        ]
    (fn []
      (let [form-state @(subscribe [::forms/form-state ::report-form/report])]
        [modal {:show show-modal?
                :cancel toggle-modal
                :width :w-80.mw7
                :background :bg-black-50}
         (if (= :SHOW-SUCCESS form-state)
           [report-success-notice]
           [report-form-content {:cancel toggle-modal
                                 :form ::report-form/report}])]))))

(defn- bottombar-menu-elem
  [{:keys [class bottombar-class strike-test snug?] :as props} text]
  (let [strike? (when strike-test
                  @(subscribe strike-test))
        colors @(subscribe [::post/bottombar-colors bottombar-class])]
    [:li.dib.pointer.noselect
     {:class (cls (if snug? :pr1 :pr3))}
     [:a.fw7.f6 (-> props
                    (dissoc :bottombar-class :strike-test :snug?)
                    (assoc :class (cls :custom colors
                                       (when strike? :strike)
                                       class)))
      text]]))

(defn- bottombar-menu-link
  [{:keys [test] :as props} text]
  (when (or (nil? test) @(subscribe test))
    [bottombar-menu-elem (dissoc props :test)
     text]))

(defn- bottombar-menu-action
  [{action-event :action} _]
  (let [action #(dispatch action-event)]
    (fn [{:keys [test] :as props} text]
      (when (or (nil? test) @(subscribe test))
        [bottombar-menu-elem (-> props
                                 (assoc :on-click action)
                                 (dissoc :test :action))
         text]))))

(defn- bottombar-menu-action-option
  "Create a bottombar link that offers further options."
  [{:keys [hide slash?] :as props} {:keys [action text]}]
  (let [option-fn (if (= :hide action)
                    hide
                    #(do
                       (hide)
                       (dispatch action)))
        day? @(subscribe [::window/day?])
        emphasis (cls (if day? :dark-red :reddish-brown))]
    [:<>
     [bottombar-menu-elem (-> props
                              (dissoc :hide :slash?)
                              (assoc :on-click option-fn
                                     :snug? slash?))
      text]
     (when slash?
       [:span.mr1.f6 {:class emphasis} "/"])]))

(defn- bottombar-menu-with-options
  "Create a bottombar link with an options menu."
  [_]
  (let [confirm-open? (reagent/atom false)
        toggle-confirm #(swap! confirm-open? not)]
    (fn [{:keys [test option-text options] :as props} text]
      (let [props' (dissoc props :test :option-text :options)
            option-props (assoc props'
                                :hide toggle-confirm
                                :slash? true)
            day? @(subscribe [::window/day?])
            emphasis (cls (if day? :dark-red :reddish-brown))]
        (when (or (nil? test) @(subscribe test))
          (if @confirm-open?
            [:div.dib
             [:span.mr2.f6
              {:class emphasis}
              option-text]
             (into [:<>]
                   (map #(bottombar-menu-action-option option-props %)
                        (butlast options)))
             [bottombar-menu-action-option (dissoc option-props :slash?)
              (last options)]]
            [bottombar-menu-elem (assoc props' :on-click toggle-confirm)
             text]))))))

(defn- bottombar-menu-action-confirm
  "Create an action link with a confirmation."
  [{:keys [action] :as props} text]
  [bottombar-menu-with-options
   (-> props
       (dissoc :action)
       (assoc
        :option-text (tr "Are you sure?")
        :options [{:action action
                   :text (tr "yes")}
                  {:action :hide
                   :text (tr "no")}]))
   text])

(defn- post-bottombar-menu
  "Create the menu of action links for a post."
  []
  (let [{:keys [distinguish locked nsfw
                sticky]} @(subscribe [::post/single-post])]
    [:ul.mv1.dib.ph0.lh-para
     [bottombar-menu-action
      {:test [::post/can-show-post-source?]
       :action [::post/toggle-post-source]
       :strike-test [::post/show-post-source?]}
      (tr "source")]
     [bottombar-menu-action
      {:test [::post/can-save-post?]
       :action [::post/toggle-post-saved]}
      (if @(subscribe [::post/single-post-saved?])
        (tr "unsave")
        (tr "save"))]
     [bottombar-menu-action
      {:test [::post/can-flair-post?]
       :action [::post/edit-single-post-flair]}
      (tr "flair")]
     [bottombar-menu-action
      {:test [::post/can-edit-post-content?]
       :action [::post/toggle-edit-post-content]
       :strike-test [::post/show-post-content-editor?]}
      (tr "edit")]
     [bottombar-menu-action
      {:test [::post/can-edit-post-title?]
       :action [::post/toggle-edit-post-title]
       :strike-test [::post/show-post-title-editor?]}
      (tr "edit title")]
     [bottombar-menu-action-confirm
      {:test [::post/can-delete-post?]
       :action [::post/delete-post]}
      (tr "delete")]
     [bottombar-menu-action-confirm
      {:test [::post/can-tag-post-nsfw?]
       :action [::post/toggle-post-nsfw]}
      (if nsfw
        (tr "remove nsfw")
        (tr "tag as nsfw"))]
     [bottombar-menu-action
      {:test [::post/can-close-poll?]
       :action [::post/close-poll]}
      (tr "close poll")]
     [bottombar-menu-action-confirm
      {:test [::post/can-stick-or-unstick-post?]
       :action [::post-mod/toggle-post-sticky]}
      (if sticky
        (tr "unstick")
        (tr "make sticky"))]
     [bottombar-menu-action-confirm
      {:test [::post/can-set-announcement-post?]
       :action [::post-mod/set-announcement-post]}
      (tr "make announcement")]
     [bottombar-menu-action-confirm
      {:test [::post/can-change-sticky-post-sort?]
       :action [::post-mod/change-sticky-post-sort]}
      (case @(subscribe [::post/next-sticky-post-sort])
        :NEW  (tr "sort by new")
        :TOP  (tr "sort by top")
        :BEST (tr "sort by best"))]
     [bottombar-menu-action-confirm
      {:test [::post/can-lock-comments?]
       :action [::post-mod/toggle-post-comments-locked]}
      (if locked
        (tr "unlock comments")
        (tr "lock comments"))]
     [bottombar-menu-action
      {:test [::post/can-report-post?]
       :action [::post/toggle-report-content-form nil]}
      (tr "report")]
     [bottombar-menu-link
      {:test [::post/show-post-reports-link?]
       :href (when-let [id @(subscribe [::post/first-open-post-report-id])]
               (url-for :mod/report-details
                        :sub @(subscribe [::subs/normalized-sub-name])
                        :report-type "post"
                        :report-id id))
       :bottombar-class :alert}
      (tr "open reports (%s)" @(subscribe [::post/open-post-report-count]))]
     [bottombar-menu-action-confirm
      {:test [::post/can-undelete-post?]
       :action [::post-mod/toggle-mod-undelete-post-form]}
      (tr "un-delete")]
     [bottombar-menu-action
      {:test [::post/can-distinguish-post-author-oneshot?]
       :action [::post-mod/edit-post-author-distinguish]}
      (if distinguish
        (tr "undistinguish")
        (tr "distinguish"))]
     [bottombar-menu-with-options
      {:test [::post/can-distinguish-post-author-choice?]
       :option-text (tr "distinguish as:")
       :options [{:action [::post-mod/edit-post-author-distinguish :admin]
                  :text "admin"}
                 {:action [::post-mod/edit-post-author-distinguish :mod]
                  :text "mod"}]}
      (tr "distinguish")]]))

(defn- post-bottombar
  []
  (let [is-authenticated? @(subscribe [::user/is-authenticated?])]
    (when is-authenticated?
      [post-bottombar-menu])))

(defn- post-heading
  []
  (let [nsfw? @(subscribe [::post/single-post-nsfw?])]
    [:div
     (when nsfw?
       [nsfw-tag])
     [:div.pl2
      [post-title]
      [post-author]
      [post-bottombar]]]))

(defn- post-content-editor
  "Create the editor for the post content."
  []
  (let [form ::post-forms/edit-content
        content (fn [] @(subscribe [::forms/field form :content]))
        editor-height @(subscribe [::post/post-editor-height])]
    (fn []
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [markdown-editor
         {:value content
          :buttonbar-class :mw7
          :class :mb2.w-100.mw7
          :style (when editor-height
                   {:height (str editor-height "px")})
          :update-value! forms/reset-input-on-success!
          :form-state form-state
          :disabled editing-disabled?
          :event [::forms/edit form :content]}]))))

(defn- post-content-edit-form
  "Create the edit form for the post content."
  []
  (let [form ::post-forms/edit-content]
    [:div
     [post-content-editor]
     [validation-error-if-present form]
     [transmission-error-if-present form]
     [:div
      [post-button {:form form :size :small}
       (tr "Save changes") (tr "Saving...")]
      [preview-button {:form form :size :small :button-class :modal}]
      [cancel-link {:event [::post/toggle-edit-post-content] :size :small}]
      [preview {:form form}
       (tr "Post preview")]]]))

(defn- post-content-source
  "Create the unformatted version of a post's content.
  Make it just a tad taller than the height of the formatted version
  of the content."
  []
  (let [content @(subscribe [::post/post-content])
        html-height @(subscribe [::post/post-html-height])]
    [:div.w-100
     [:textarea.w-100.resize-v.overflow-auto
      (merge {:read-only true
              :value content}
             (when html-height
               {:style {:height (str (+ 28 html-height) "px")}}))]]))

(defn- update-height [elem]
  (when elem
    (dispatch
     [::post/record-post-html-content-height (.-clientHeight elem)])))

(defn- post-html-content
  "Create the formatted version of a post's content."
  []
  (let [self-ref (reagent/atom nil)
        set-ref! (fn [ref]
                   (update-height ref)
                   (reset! self-ref ref))]
    (reagent/create-class
     {:display-name "post-html-content"
      :component-did-update #(update-height @self-ref)
      :reagent-render
      (fn []
        (let [html-content @(subscribe [::post/post-html-content])]
          [:div.mw7.break-word {:ref set-ref!}
           [dangerous-html {} html-content]]))})))

(defn- poll-option
  "Create a poll option."
  [{:keys [option-id text percent]} color]
  (let [show-results? @(subscribe [::post/show-poll-results?])
        {:keys [poll-vote]} @(subscribe [::post/user-attributes])]
    [:div.pv2.ph2
     (when (and show-results? percent)
       [:span.pr2
        {:class color} percent "%"])
     text
     (when (= option-id poll-vote)
       [:span.pl2
        [icon {:class (cls :dib.h1.w1.fill-purple)} "ccheck"]])]))

(defn- poll-options
  "Create the list of poll options."
  []
  (let [day? @(subscribe [::window/day?])
        show-results? @(subscribe [::post/show-poll-results?])
        can-vote? @(subscribe [::post/can-vote-poll?])
        options @(subscribe [::post/poll-options])]
    (into [:ul.pa0]
          (map (fn [{:keys [option-id percent] :as option}]
                 [:li.list.mv2
                  [(if can-vote? :button :div)
                   {:class (cls :pa0.bw0.w-100.tl.relative
                                (if day? :bg-white :bg-black)
                                (when can-vote?
                                  [:pointer
                                   (if day?
                                     :hover-bg-washed-purple
                                     :hover-bg-dark-gray)]))
                    :on-click (when can-vote?
                                #(dispatch [::post/vote-poll option-id]))}
                   (when (and show-results? percent)
                     [:div.min-h-100.absolute.br2
                      {:class (cls (if day?
                                     :bg-washed-purple
                                     :bg-dark-gray))
                       :style {:width (str percent "%")}}])
                   [:div.min-h-100.w-100.absolute.ba.br2.bw1
                    {:class (if day? :b--purple :b--dark-purple)}]
                   [:div.absolute.min-h-100.w-100
                    {:class (if day? :black :gray)}
                    [poll-option option (if day? :purple :dark-purple)]]
                   ;; Relative child shows up behind the absolute children,
                   ;; but we need it to set the height of the parent.
                   ;; Make it have invisible text because otherwise the text
                   ;; looks smeary.
                   [:div.w-100.transparent
                    [poll-option option nil]]]])
               options))))

(defn- post-poll-content
  "Create the content of a poll post."
  []
  (let [{:keys [hide-results poll-open poll-votes
                poll-closes-time]} @(subscribe [::post/single-post])
        show-see-results-button? @(subscribe [::post/show-see-results-option?])
        show-source? @(subscribe [::post/show-post-source?])
        show-editor? @(subscribe [::post/show-post-content-editor?])
        can-withdraw? @(subscribe [::post/can-withdraw-poll-vote?])
        day? @(subscribe [::window/day?])]
    [:div
     [:div.br2.mt2.pv2.ph3
      {:class (if day? :bg-white :bg-black)}
      (if poll-open
        [:<>
         (when show-see-results-button?
           [button {:button-class :modal
                    :class :mv2.mb0-ns.mr3
                    :on-click #(dispatch [::post/see-poll-results])}
            (tr "Show results")])
         (when poll-closes-time
           [:p
            (trm-html "This poll will close %{timeuntil}."
                      {:timeuntil [relative-time
                                   {:datetime poll-closes-time}]})])
         (when hide-results
           [:p.i (tr "Results will be hidden until the poll closes.")])]
        [:p (trm-html "This poll is now %{state}."
                      {:state [:b (tr "closed")]})])
      [poll-options]
      [:p.silver (tr "%s votes" poll-votes)]
      (when can-withdraw?
        [button {:button-class :modal
                 :class :mv2.mb0-ns.mr3
                 :on-click #(dispatch [::post/withdraw-poll-vote])}
         (tr "Withdraw vote")])]
     (cond
       show-editor? [:div.pv2 [post-content-edit-form]]
       show-source? [:div.pv2 [post-content-source]]
       :else [post-html-content])]))

(defn- image-content-wrapper
  "Create a border for some image content."
  [child]
  (let [day? @(subscribe [::window/day?])]
    [:div.pa3.br2.dib
     {:class (cls (if day? :bg-near-white :bg-near-black))}
     child]))

(defn- post-embed-image
  "Create the content of a post that links to an image file."
  []
  (let [link (:link @(subscribe [::post/single-post]))
        blur @(subscribe [::post/single-post-blur])]
    [blur-wrapper {:blur blur}
     [image-content-wrapper
      [:a {:target "_blank"
           :rel "noopener nofollow ugc"
           :href link}
       [:img.mw-100 {:src link
                     :draggable false}]]]]))

(defn- post-embed-video
  "Create the content of a post that links to a video file."
  []
  (let [link (:link @(subscribe [::post/single-post]))
        blur @(subscribe [::post/single-post-blur])]
    [blur-wrapper {:blur blur}
     [image-content-wrapper
      [:video {:src link
               :class :mw-100
               :preload "auto"
               :auto-play false
               :controls true}
       (tr "Video format not supported.")]]]))

(defn- post-iframe-video
  []
  (let [link @(subscribe [::post/iframe-video-link])
        blur @(subscribe [::post/single-post-blur])
        day? @(subscribe [::window/day?])]
    [:div.pa3.br2
     {:class (cls (if day? :bg-near-white :bg-near-black))}
     [blur-wrapper {:blur blur}
      [:div.relative.w-100.aspect-16-9
       [:iframe.absolute.w-100.h-100.top-0.left-0
        {:src link
         :allow "fullscreen"}]]]]))

(defn- post-edit-history
  "Create the edit history for the post's content."
  []
  (let [state @(subscribe [::post/post-content-edit-history-state])
        {:keys [newest? oldest? index num-edits]} state]
    [edit-history
     {:see-older [::post/post-content-see-older-version]
      :see-newer [::post/post-content-see-newer-version]
      :state state}
     (cond
       newest? (tr "Viewing most recent edit (%s/%s)" num-edits num-edits)
       oldest? (tr "Viewing original content (1/%s)" num-edits)
       :else (tr "Viewing edit history (%s/%s)" (inc index) num-edits))]))

(defn- post-content
  []
  (let [is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
        {:keys [type content status]} @(subscribe [::post/single-post])
        highlight-delete? (and is-mod-or-admin? (not= :ACTIVE status))
        note-delete? (and (not highlight-delete?) (not= :ACTIVE status))
        embed-image? @(subscribe [::post/embeddable-image?])
        embed-video? @(subscribe [::post/embeddable-video?])
        iframe-link @(subscribe [::post/iframe-video-link])
        show-source? @(subscribe [::post/show-post-source?])
        show-editor? @(subscribe [::post/show-post-content-editor?])
        day? @(subscribe [::window/day?])]
    [:div.pt1.pb2.br2
     {:class (cls (if (or embed-image? embed-video? iframe-link)
                    :mw8 :mw7)
                  (when (and (or (seq content)
                                 (= :POLL type))
                             (= :ACTIVE status))
                    [:ph2
                     (if day? :bg-near-white :bg-near-black)]))}
     (cond
       note-delete? (tr "[Post deleted]")
       (= :POLL type) [post-poll-content]
       show-editor? [post-content-edit-form]
       show-source? [post-content-source]
       embed-image? [post-embed-image]
       embed-video? [post-embed-video]
       iframe-link [post-iframe-video]
       (#{:LINK :UPLOAD} type) nil
       :else [post-html-content])
     (when-not show-editor?
       [post-edit-history])]))

(defn- comment-sort-picker
  "Create a dropdown menu to control comment sorting."
  []
  (let [sort @(subscribe [::post/post-comment-sort])
        post @(subscribe [::post/single-post])
        best-sort-enabled? (:best-sort-enabled post)
        sort-choices (merge (when best-sort-enabled?
                              {:BEST (tr "Best")})
                            {:TOP (tr "Top")
                             :NEW (tr "New")})
        title (fn [val]
                [:span.f6.pa2
                 (trm-html "Sort by: %{sort} ▾"
                           {:sort [:b (get sort-choices val)]})])]
    [dropdown-menu {:value sort
                    :choices sort-choices
                    :title-fn title
                    :event [::post/set-comment-sort]}]))

(defn- comment-author
  [cid]
  (let [{:keys [sid]} @(subscribe [::post/single-post])
        details @(subscribe [::comment/details cid])
        {:keys [status distinguish uid]} details
        author-flair @(subscribe [::subs/sub-user-flair sid uid])
        {name :name
         author-status :status} @(subscribe [::comment/author cid])
        content-block @(subscribe [::comment/content-block cid])
        {post-author :name} @(subscribe [::post/single-post-author])
        colors @(subscribe [::window/distinguish-colors distinguish])]
    [:<>
     [:span {:class (cls colors :mr1.f6.fw7
                         (when distinguish
                           :pa05.br1))}
      (comment ;; Useful for debugging marking comments viewed
        (when config/debug?
          (let [ui-state @(subscribe [::comment/ui-state cid])]
            (when (:marked-viewed? ui-state)
              " * "))))
      (when name
        [(if (= :HIDE content-block) :a userlink)
         (when colors
           {:class (cls :custom colors)})
         (if (= :HIDE content-block)
           (tr "[Blocked]")
           name)
         (when distinguish
           [:<> " "
            (case distinguish
              :ADMIN (tr "[speaking as admin]")
              :MOD (tr "[speaking as mod]")
              nil)])
         (when (= name post-author)
           [:<> " " (tr "[OP]")])
         (when (= :BLUR content-block)
           [:span.i.gray " " (tr "[Blocked]")])])

      (when (or (= author-status :DELETED)
                (= status :DELETED)
                (and (= status :DELETED_BY_MOD) (nil? name)))
        [:a (tr " [Deleted]")])]
     (when (and name (nil? content-block) author-flair)
       [:span.mr2
        [user-flair author-flair]])]))

(defn- comment-time
  [cid]
  (let [{:keys [status time edited]} @(subscribe [::comment/details cid])]
    [:<>
     [:span.f6.fw7.mid-gray.b
      [relative-time {:datetime time}]]
     (when (and edited (not= status :DELETED))
       [:span.f6.mid-gray.b.ml2
        (trm-html "(Edited %{edited})"
                  {:edited [relative-time {:datetime edited}]})])]))

(defn- comment-stick
  [sticky?]
  (when sticky?
    [:<> " - " [:span.green.f6 (tr "sticky")]]))

(defn- deletion-type
  "Create the notice for mods and admins of how a comment got deleted."
  [status]
  [:div.pv3.mid-gray
   (case status
     :DELETED_BY_USER (tr "[comment deleted by user]")
     :DELETED_BY_MOD (tr "[comment deleted by mod]")
     :DELETED_BY_ADMIN (tr "[comment deleted by admin]")
     "")])

(defn- collapse-toggle-color
  "Get the color for a collapse toggle for a comment.
  `typ` should be :fill for svgs or :border."
  [cid typ]
  (let [day? @(subscribe [::window/day?])
        hover? @(subscribe [::comment/collapse-hover cid])]
    (get-in {:day {:normal {:fill :fill-medium-gray
                            :border :b--medium-gray}
                   :hover {:fill :fill-light-purple
                           :border :b--light-purple}}
             :night {:normal {:fill :fill-dark-gray
                              :border :b--dark-gray}
                     :hover {:fill :fill-dark-purple
                             :border :b--dark-purple}}}
            [(if day? :day :night)
             (if hover? :hover :normal)
             typ])))

(defn- collapse-toggle
  "Create the collapse control."
  [cid]
  (let [toggle #(dispatch [::comment/toggle-collapse cid])
        on-mouse-over #(dispatch [::comment/set-collapse-hover cid true])
        on-mouse-out #(dispatch [::comment/set-collapse-hover cid false])]
    (fn [cid]
      (let [color (collapse-toggle-color cid :fill)
            collapsed? @(subscribe [::comment/collapsed? cid])]
        [icon {:class (cls :self-end.center.mb1.ml05.h1.w1.pointer color)
               :on-click toggle
               :on-mouse-over on-mouse-over
               :on-mouse-out on-mouse-out}
         (if collapsed? "circle-cross" "circle-bar")]))))

(defn- vertical-collapse-bar
  "Create the vertical comment collapse control."
  [cid]
  (let [enable? @(subscribe [::user/enable-collapse-bars])
        toggle #(dispatch [::comment/toggle-collapse cid])
        on-mouse-over #(dispatch [::comment/set-collapse-hover cid true])
        on-mouse-out #(dispatch [::comment/set-collapse-hover cid false])
        props (when enable?
                {:class :pointer
                 :on-click toggle
                 :on-mouse-over on-mouse-over
                 :on-mouse-out on-mouse-out})]
    (fn [cid]
      (let [color (collapse-toggle-color cid :border)]
        [:div.gr-comment-collapse.flex
         [:div.w-36 props]
         [:div (assoc props :class (cls :bl.bw02 color))]
         [:div.w-20 props]
         [:div.flex-grow-1]]))))

(defn- vote-arrow
  "Create a vote arrow for the comment bottom bar."
  [{:keys [class] :as props} icon-name]
  [icon (assoc props :class (cls :dib.h1.w1 class))
   icon-name])

(defn- comment-votebuttons
  "Create the score and votebuttons for a comment."
  [cid]
  (let [details @(subscribe [::comment/details cid])
        {:keys [score upvotes downvotes user-attributes]} details
        vote (:vote user-attributes)
        can-vote? @(subscribe [::comment/can-vote? cid])
        is-authenticated? @(subscribe [::user/is-authenticated?])
        event (if is-authenticated?
                [::comment/vote cid]
                [::post/toggle-login-to-continue])]
    [:div.dib.pr3
     [vote-arrow {:title (when can-vote? (tr "Vote up"))
                  :class (vote-colors :UP vote can-vote?)
                  :on-click (when can-vote?
                              #(dispatch (conj event :UP)))}
      "upvote"]
     [:div.dib.silver.lh-title
      [:span.pl2.pr1.f6.fw7
       score
       (if (and upvotes downvotes)
         [:span.ph1 "(+" upvotes "|-" downvotes ")"]
         [:span.pr1])]]
     [vote-arrow {:title (when can-vote? (tr "Vote down"))
                  :class (vote-colors :DOWN vote can-vote?)
                  :on-click (when can-vote?
                              #(dispatch (conj event :DOWN)))}
      "downvote"]]))

(defn- comment-bottombar-menu
  [cid]
  (let [sub @(subscribe [::subs/viewed-sub-info])
        slug (:slug @(subscribe [:view/options]))
        post @(subscribe [::post/single-post])
        {:keys [distinguish sticky]} @(subscribe [::comment/details cid])]
    [:ul.dib.ph0.pr0.mb1.mt0
     [comment-votebuttons cid]
     [bottombar-menu-action
      {:test [::comment/can-reply? cid]
       :action [::comment/toggle-reply-editor cid]
       :strike-test [::comment/show-reply-editor? cid]}
      (tr "reply")]
     [bottombar-menu-link
      {:test [::comment/show-blocked-users-link? cid]
       :href (url-for :user/view-ignores :url-params {:menu "user"})
       :bottombar-class :subdued}
      (tr "blocked users")]
     [bottombar-menu-action
      {:test [::comment/can-check-off? cid]
       :action [::comment-mod/check-off cid]
       :bottombar-class :subdued}
      (tr "check off")]
     [bottombar-menu-action
      {:test [::comment/can-uncheck? cid]
       :action [::comment-mod/uncheck cid]
       :bottombar-class :subdued}
      (tr "uncheck")]
     [bottombar-menu-link
      {:href (str (url-for :sub/view-direct-link
                           :sub (:name sub)
                           :pid (:pid post)
                           :slug (or slug "_")
                           :cid cid) "#comment-" cid)
       :bottombar-class :subdued}
      (tr "direct link")]
     [bottombar-menu-action
      {:test [::comment/can-show-source? cid]
       :action [::comment/toggle-source cid]
       :strike-test [::comment/show-source? cid]
       :bottombar-class :subdued}
      (tr "source")]
     [bottombar-menu-action
      {:test [::comment/can-report? cid]
       :action [::post/toggle-report-content-form cid]
       :bottombar-class :subdued}
      (tr "report")]
     [bottombar-menu-action
      {:test [::comment/can-edit? cid]
       :action [::comment/toggle-editor cid]
       :strike-test [::comment/show-editor? cid]
       :bottombar-class :subdued}
      (tr "edit")]
     [bottombar-menu-action-confirm
      {:test [::comment/can-delete? cid]
       :action [::comment/delete-comment cid]
       :bottombar-class :subdued}
      (tr "delete")]
     [bottombar-menu-action
      {:test [::comment/can-undelete? cid]
       :action [::comment-mod/toggle-mod-undelete-comment-form cid]
       :bottombar-class :subdued}
      (tr "un-delete")]
     [bottombar-menu-action
      {:test [::comment/can-distinguish-author-oneshot? cid]
       :action [::comment-mod/edit-distinguish cid]
       :bottombar-class :subdued}
      (if distinguish
        (tr "undistinguish")
        (tr "distinguish"))]
     [bottombar-menu-with-options
      {:test [::comment/can-distinguish-author-choice? cid]
       :option-text (tr "distinguish as:")
       :options [{:action [::comment-mod/edit-distinguish cid :admin]
                  :text "admin"}
                 {:action [::comment-mod/edit-distinguish cid :mod]
                  :text "mod"}]
       :bottombar-class :subdued}
      (tr "distinguish")]
     [bottombar-menu-action-confirm
      {:test [::comment/can-sticky? cid]
       :action [::comment-mod/toggle-sticky cid]
       :bottombar-class :subdued}
      (if sticky
        (tr "unstick")
        (tr "make sticky"))]]))

(defn- comment-editor
  "Create one of the editors (edit or reply) for a comment."
  [form-type cid]
  (let [form (comment-form/form-id form-type cid)
        content (fn [] @(subscribe [::forms/field form :content]))
        html-height (when (= :edit form-type)
                      (:content-height @(subscribe [::comment/ui-state cid])))]
    (fn [_ _]
      (let [form-state @(subscribe [::forms/form-state form])
            editing-disabled? @(subscribe [::forms/editing-disabled? form])]
        [markdown-editor
         {:value content
          :auto-focus (some? cid)
          :buttonbar-class :mw7
          :class :mb2.w-100.mw7
          :style (when html-height
                   {:height (str (+ 28 html-height) "px")})
          :update-value! forms/reset-input-on-success!
          :form-state form-state
          :disabled editing-disabled?
          :event [::forms/edit form :content]}]))))

;; If a post is updated and becomes deleted or locked, don't simply
;; remove the reply editors, because that would shift up the layout
;; and hide anything the user had already typed.  Instead replace the
;; submit button with an appropriate message.  Does not apply to mods.

(def ^:private update-notice :p.bg-washed-pink.w-70)

(defn- unless-locked-by-update
  [content]
  (let [{:keys [locked-update]} @(subscribe [::post/single-post])
        is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])]
    (if (and locked-update (not is-mod-or-admin?))
      [update-notice
       (tr "This post has been locked by the moderators.
            Comments are closed.")]
      content)))

(defn- unless-deleted-by-update
  [content]
  (let [{:keys [status-update]} @(subscribe [::post/single-post])
        is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])]
    (if (and (some? status-update)
             (not= :ACTIVE status-update)
             (not is-mod-or-admin?))
      [update-notice
       (tr "This post has been deleted. Comments are closed.")]
      content)))

(defn- reply-form-controls
  [{:keys [form-id label active-label size cancel-event]}]
  [:div
   [post-button {:form form-id :size size}
    label active-label]
   [preview-button {:form form-id :size size}]
   (when cancel-event
     [cancel-link {:event cancel-event
                   :size size}])
   [preview {:form form-id}
    (tr "Comment preview")]])

(defn- post-reply-form
  "Create the reply form for a post."
  []
  (let [reply-form (comment-form/form-id :reply nil)
        show? @(subscribe [::comment/show-reply-editor? nil])]
    (when show?
      [:div.mt3
       [comment-editor :reply nil]
       [validation-error-if-present reply-form]
       [transmission-error-if-present reply-form]
       [unless-deleted-by-update
        [unless-locked-by-update
         [reply-form-controls {:form-id reply-form
                               :size :large
                               :label (tr "Post comment")
                               :active-label (tr "Posting...")}]]]])))

(defn- comment-reply-form
  "Create the reply form for a comment."
  [cid]
  (let [reply-form (comment-form/form-id :reply cid)
        show? @(subscribe [::comment/show-reply-editor? cid])]
    (when show?
      [:div
       [comment-editor :reply cid]
       [validation-error-if-present reply-form]
       [transmission-error-if-present reply-form]
       [unless-deleted-by-update
        [unless-locked-by-update
         [reply-form-controls
          {:form-id reply-form
           :size :small
           :label (tr "Post comment")
           :active-label (tr "Posting...")
           :cancel-event [::comment/toggle-reply-editor cid]}]]]])))

(defn- comment-edit-form
  "Create the edit form for a comment."
  [cid]
  (let [edit-form (comment-form/form-id :edit cid)
        form-state @(subscribe [::forms/form-state edit-form])
        show? @(subscribe [::comment/show-editor? cid])]
    (when (and show? form-state)
      [:div
       [comment-editor :edit cid]
       [validation-error-if-present edit-form]
       [transmission-error-if-present edit-form]
       [unless-deleted-by-update
        [reply-form-controls
         {:form-id edit-form
          :size :small
          :label (tr "Save changes")
          :active-label (tr "Saving...")
          :cancel-event [::comment/toggle-editor cid]}]]])))

(defn- comment-bottombar
  "Create the score, votebuttons and action menu for a comment."
  [cid]
  [:div.lh-para
   [comment-bottombar-menu cid]
   [comment-reply-form cid]])

(declare comment-tree)

(defn- comment-edit-history
  "Create the controls to see the comment edit history."
  [cid]
  (let [state @(subscribe [::comment/edit-history-state cid])
        {:keys [newest? oldest? index num-edits]} state]
    [edit-history
     {:see-older [::comment/see-older-version cid]
      :see-newer [::comment/see-newer-version cid]
      :state state}
     (cond
       newest? (tr "Viewing most recent edit (%s/%s)" num-edits num-edits)
       oldest? (tr "Viewing original content (1/%s)" num-edits)
       :else (tr "Viewing edit history (%s/%s)" (inc index) num-edits))]))

(defn- comment-content-source
  "Create the unformatted version of a comment's content.
  Make it just a tad taller than the height of the formatted version
  of the content."
  [cid]
  (let [content @(subscribe [::comment/content cid])
        html-height (:content-height  @(subscribe [::comment/ui-state cid]))]
    [:div.w-100
     [:textarea.w-100.resize-v.overflow-auto
      (merge {:read-only true
              :value content}
             (when html-height
               {:style {:height (str (+ 28 html-height) "px")}}))]]))

(defn- comment-html-content
  "Create the formatted version of a comment's content."
  [cid]
  (let [html-content-ref #(when %
                            (dispatch
                             [::comment/record-html-content-height
                              cid (.-clientHeight %)]))]
    (fn [cid]
      (let [html-content @(subscribe [::comment/html-content cid])]
        [:div.break-word {:ref html-content-ref}
         [dangerous-html {} html-content]]))))

(defn- comment-checkoff
  "Create the comment checkoff."
  [cid]
  (let [checkoff @(subscribe [::comment/checkoff cid])]
    (when checkoff
      [:div.pb1.f6.gray
       (tr "✓ by %s" (get-in checkoff [:user :name]))])))

(defn- comment-content
  "Create the content section of a comment."
  [cid]
  (let [update-ref #(dispatch [::comment/record-content-dom-node cid %])]
    (fn [cid]
      (let [{:keys [status]} @(subscribe [::comment/details cid])
            {:keys [name]} @(subscribe [::comment/author cid])
            blocked? (= :HIDE @(subscribe [::comment/content-block cid]))
            show-source? @(subscribe [::comment/show-source? cid])
            is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
            highlight-delete? (and is-mod-or-admin? (not= :ACTIVE status))
            note-delete? (and (not highlight-delete?) (not= :ACTIVE status))
            editing? @(subscribe [::comment/show-editor? cid])]
        [:div.mw7 {:ref update-ref}
         (when highlight-delete?
           [deletion-type status])
         (cond
           editing? [comment-edit-form cid]
           note-delete? [helper-text (tr "[Comment deleted]")]
           blocked? [helper-text
                     (if name
                       (trm-html "[You have blocked %{user}]"
                                 {:user [userlink name]})
                       (tr "[You have blocked a deleted user]"))]
           show-source? [comment-content-source cid]
           :else [comment-html-content cid])
         [comment-edit-history cid]
         [comment-checkoff cid]]))))

(defn- comment-branch
  "Create a comment and its children."
  [{:keys [cid]} _path]
  (let [on-ref (fn [elem]
                 (when elem
                   (dispatch [::comment/check-scroll cid elem])
                   (dispatch-debounce {:id :comment-did-mount
                                       :timeout 250
                                       :action :dispatch
                                       :event [::comment/mark-viewed]})))]
    (fn [{:keys [cid]} path]
      (let [details @(subscribe [::comment/details cid])
            {:keys [sticky]} details
            collapsed? @(subscribe [::comment/collapsed? cid])
            background (cls @(subscribe [::comment/background cid]))]
        ^{:key cid}
        [:div.gr-comment
         {:class background
          :ref on-ref}
         [collapse-toggle cid]
         [:div.self-center.pt2
          [comment-author cid]
          [comment-time cid]
          [comment-stick sticky]]
         (when-not collapsed?
           [:<>
            [vertical-collapse-bar cid]
            [comment-content cid]
            [comment-bottombar cid]
            [:div [comment-tree (cons cid path)]]])]))))

(defn- load-more
  "Create a load-more link in the comment tree."
  [path]
  (let [unloaded-count @(subscribe [::comment/unloaded-child-count path])
        unchecked-count @(subscribe [::comment/unchecked-child-count path])
        new-count @(subscribe [::comment/new-child-count path])
        is-mod-or-admin? @(subscribe [::user/is-mod-or-admin?])
        is-authenticated? @(subscribe [::user/is-authenticated?])
        highlight-unchecked? (and is-mod-or-admin? (pos? unchecked-count))
        highlight-new? (and is-authenticated? (pos? new-count))
        day? @(subscribe [::window/day?])
        loading @(subscribe [::comment/load-more-status path])
        text (if (not (or highlight-unchecked? highlight-new?))
               (trn ["load more (%s comment)" "load more (%s comments)"]
                    unloaded-count)
               (str (tr "load more comments (%s" unloaded-count)
                    (when highlight-unchecked?
                      (tr " / %s unchecked" unchecked-count))
                    (when highlight-new?
                      (tr " / %s new" new-count))
                    (tr ")")))]
    (when (> unloaded-count 0)
      [:div.pt3
       (if (not= :loading loading)
         [:a
          {:class (cls :pointer.f6.fw7
                       (when highlight-unchecked?
                         (if day? :bg-washed-green :bg-dark-bluish-green))
                       (when highlight-new?
                         (if day? :custom.purple :custom.dark-purple)))
           :on-click #(dispatch [::comment/load-more path])}
          text]
         [:a.f6.fw7 (tr "loading...")])])))

(defn- comment-tree
  "Create the comments for the single post page comment listing."
  [path]
  (let [comments @(subscribe [::comment/child-comments path])]
    [:div
     (when (seq comments)
       (into [:div] (map (fn [comment]
                           [comment-branch comment path]) comments)))
     [load-more path]]))

(defn- direct-link-blurb
  []
  (let [{:keys [pid sub slug]} @(subscribe [:view/options])
        root-level @(subscribe [::comment/root-level])]
    [:div
     [:h4
      (if (zero? root-level)
        (tr "You are viewing a single comment thread.")
        (tr "You are viewing a comment thread without its full context."))
      " "
      [:a {:href (str (url-for :sub/view-single-post
                               :sub sub
                               :pid pid) "/" slug)}
       (tr "Show all comments.")]]]))

(defn- comment-listing
  "Create the comment listing with sort method picker."
  []
  (let [update-viewed #(dispatch [::comment/mark-viewed])]
    (reagent/create-class
     {:display-name "comment-listing"
      ;; When all the child components have mounted, check if any are
      ;; visible to the user.
      :component-did-mount update-viewed
      :component-did-update update-viewed

      :reagent-render
      (fn []
        (let [num @(subscribe [::comment/comment-count])
              is-authenticated? @(subscribe [::user/is-authenticated?])
              post @(subscribe [::post/single-post])
              {:keys [is-archived locked status]} post
              loaded? @(subscribe [::post/comment-tree-loaded?])
              single-comment? @(subscribe [::post/show-single-comment-thread?])]
          [:div
           (when (and is-authenticated? (= :ACTIVE status))
             (cond
               is-archived [:h4 (tr "This post is closed to new comments.
                               It has been automatically archived.")]
               locked [:h4 (tr "This post is closed to new comments.
                          It has been locked by the moderators.")]))
           (cond
             (not loaded?) [:div.pv2
                            [:h3 (tr "Loading comments...")]]

             (= num 0) [:h3 (if (or is-archived locked)
                              (tr "No comments")
                              (tr "No comments, yet..."))]

             single-comment? [direct-link-blurb]

             :else [:div.pv2
                    [:h2 (tr "%s comments" num)]
                    [comment-sort-picker]])
           [comment-tree]]))})))

(defn- modal-login-to-continue
  "Create a login form for unauthenticated users."
  []
  (let [toggle-modal #(dispatch [::post/toggle-login-to-continue])
        show-modal? (fn [] @(subscribe [::post/show-login-to-continue?]))]
    (fn []
      [modal {:show show-modal?
              :cancel toggle-modal}
       [:div
        [:h2.purple
         (tr "Sign in or register to participate")]
        [sign-up-form-content]]])))

(defn- single-post-page-modals
  "Create the modal overlays for the single post page."
  []
  (let [is-mod? @(subscribe [::user/is-mod-or-admin?])]
    [:<>
     [modal-post-flair-form]
     [modal-report-form]
     [modal-login-to-continue]
     [markdown-link-modal]
     (when is-mod?
       [(resolve 'ovarit.app.fe.views.single-post.moderation/mod-modals)])]))

(defn single-post-panel
  "Create the single post page."
  []
  (let [day? @(subscribe [::window/day?])
        {:keys [status]} @(subscribe [::post/single-post])
        loaded? @(subscribe [::post/loaded?])
        author @(subscribe [::post/single-post-author])]
    (when loaded?
      [panel {:sidebar single-post-sidebar}
       [:div {:class (cls (when (and day?
                                     (not (#{:ACTIVE :DELETED} status))
                                     (not= :DELETED (:status author)))
                            :bg-washed-pink))}
        [:div.pv1.gr-single-post-header
         {:class (cls (when (and (not day?) (not= status :ACTIVE))
                        :ba.bw1.br3.b--red))}
         [votebuttons]
         [thumbnail]
         [post-heading]]
        [post-content]]
       [post-reply-form]
       [single-post-page-modals]
       [comment-listing]])))
