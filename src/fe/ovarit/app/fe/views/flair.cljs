;; fe/views/flair.cljs -- Post flair rendering for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
(ns ovarit.app.fe.views.flair
  (:require
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.util :refer [cls] :as util]
   [re-frame.core :refer [subscribe]]
   [reagent.core :as reagent]))

(defn post-flair
  "Create a post flair without the href.
  In properties, use :selected? to make it change color."
  []
  (let [this (reagent/current-component)
        props (reagent/props this)
        children (reagent/children this)
        flair-colors @(subscribe [::window/post-flair-colors props])]
    (into
     [:a {:class (cls :custom.ph2.pv1.lh-title.br2
                      :f5.pointer flair-colors)}]
     children)))
