;; fe/views/funding-progress.cljs -- Funding progress views for ovarit-app
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.funding-progress
  (:require
   [ovarit.app.fe.content.donate :as donate]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.util :refer [cls]]
   [re-frame.core :refer [subscribe]]))

(defn funding-progress-bar
  "Create the progress bar for the funding goal."
  []
  (let [percent @(subscribe [::donate/funding-completion-percentage])
        colors @(subscribe [::window/progress-bar-colors])]
    [:div {:class (cls :br-pill.h1.overflow-y-hidden (:background colors))}
     [:div {:class (cls :br-pill.h1.shadow-1 (:foreground colors))
            :style {:width percent}}]]))

(defn funding-progress-legend
  "Create the legend for the funding goal."
  []
  (let [goal @(subscribe [::donate/funding-goal])
        amount @(subscribe [::donate/funds-raised])]
    [:p.pb1 (tr "%s raised so far this month towards our goal of %s"
                (or amount "$") goal)]))

(defn funding-progress-content
  "Create a progress bar and legend for the funding goal iframe.
  This version lacks a background and headline."
  []
  [:div.ph3.pv2
   [funding-progress-bar]
   [funding-progress-legend]])

(defn funding-progress
  "Show a headline, progress bar and legend for the funding goal,
  if a funding goal is configured, with a shaded background."
  []
  (let [show? @(subscribe [::donate/show-funding-progress?])
        form-colors @(subscribe [::window/form-background])]
    (when show?
      [:div {:class (cls :ph3.pv2.br3 form-colors)}
       [:h4.center (tr "Funding Goal")]
       [funding-progress-bar]
       [funding-progress-legend]])))
