;; fe/views/icon.cljs -- Icon rendering for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.icon
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.content.settings :as settings]
   [re-frame.core :refer [subscribe]]
   [shadow.resource :as rc]))

(defn remove-dimensions
  "Remove width and height from SVG html."
  [svg-html]
  (-> svg-html
      (str/replace #" width=\"\d+\"" "")
      (str/replace #" height=\"\d+\"" "")))

(def svg
  (-> {:mail          (rc/inline "svg/mail.svg")
       :shield        (rc/inline "svg/shield.svg")
       :cog           (rc/inline "svg/cog.svg")
       :sun           (rc/inline "svg/sun-fill.svg")
       :moon          (rc/inline "svg/moon-fill.svg")
       :play          (rc/inline "svg/play_arrow.svg")
       :image         (rc/inline "svg/image.svg")
       :text          (rc/inline "svg/file-text.svg")
       :close         (rc/inline "svg/close.svg")
       :exclaim       (rc/inline "svg/exclaim.svg")
       :chat          (rc/inline "svg/chat.svg")
       :link          (rc/inline "svg/link.svg")
       :upvote        (rc/inline "svg/up.svg")
       :downvote      (rc/inline "svg/down.svg")
       :updown        (rc/inline "svg/updown.svg")
       :search        (rc/inline "svg/search.svg")
       :check         (rc/inline "svg/check.svg")
       :add           (rc/inline "svg/add.svg")
       :remove        (rc/inline "svg/remove.svg")
       :owner         (rc/inline "svg/star.svg")
       :bold          (rc/inline "svg/bold.svg")
       :italic        (rc/inline "svg/italic.svg")
       :underline     (rc/inline "svg/underline.svg")
       :strikethrough (rc/inline "svg/strikethrough.svg")
       :title         (rc/inline "svg/title.svg")
       :bulletlist    (rc/inline "svg/bulletlist.svg")
       :numberlist    (rc/inline "svg/numberlist.svg")
       :code          (rc/inline "svg/code.svg")
       :quote         (rc/inline "svg/quote.svg")
       :copyright     (rc/inline "svg/copyright.svg")
       :edit          (rc/inline "svg/edit.svg")
       :down          (rc/inline "svg/caret-down.svg")
       :up            (rc/inline "svg/caret-up.svg")
       :logo          (rc/inline "svg/logo.svg")
       :gradient      (rc/inline "svg/gradient.svg")
       :filebox       (rc/inline "svg/filebox.svg")
       :reply         (rc/inline "svg/reply-arrow.svg")
       :ccheck        (rc/inline "svg/ccheck.svg")
       :circle-bar    (rc/inline "svg/circle-bar.svg")
       :circle-cross  (rc/inline "svg/circle-cross.svg")}
      (update-vals remove-dimensions)))

(defn icon
  "Create the icon for `name` with `props`."
  [props name]
  (let [logo-svg @(subscribe [::settings/logo-svg])
        svg-html (if (and (= name "logo") logo-svg)
                   logo-svg
                   ((keyword name) svg))]
    [:span (merge props
                  {:role :img
                   :data-icon name
                   :dangerouslySetInnerHTML {:__html (if svg-html
                                                       svg-html
                                                       (:close svg))}})]))
