;; fe/views/modmail.cljs -- Modmail for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.modmail
  (:require
   [clojure.string :as str]
   [ovarit.app.fe.content.modmail :as modmail]
   [ovarit.app.fe.content.settings :as settings]
   [ovarit.app.fe.content.subs :as subs]
   [ovarit.app.fe.tr :refer [tr trm-html trn]]
   [ovarit.app.fe.ui.forms.compose-message :as compose]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user :as user]
   [ovarit.app.fe.views.common :refer [dangerous-html helper-text
                                       relative-time userlink]]
   [ovarit.app.fe.views.forms :refer [transmission-error-if-present
                                      validation-error-if-present]]
   [ovarit.app.fe.views.icon :refer [icon]]
   [ovarit.app.fe.views.inputs :refer [button checkbox dropdown-menu radio-button
                                       markdown-link-modal text-input]]
   [ovarit.app.fe.views.messages :as view-messages]
   [ovarit.app.fe.views.misc-panels :as misc-panels]
   [ovarit.app.fe.views.panels :refer [panel]]
   [ovarit.app.fe.views.sidebar :refer [button-link footer]]
   [ovarit.app.fe.views.util :refer [cls url-for]]
   [re-frame.core :refer [subscribe dispatch]]))

(def compose-form ::compose/compose-form)
(def icon-class (cls :dib.mh1.w16px.h16px.pointer.relative.z-1))

(defn- modmail-sortbutton-class
  [button-type]
  (let [sort-type @(subscribe [:view/sort])]
    ["sbm-post" "pure-button" "button-xsmall" "pure-u-md-6-24"
     (when (= button-type sort-type)
       "pure-button-primary")]))

(defn- modmail-sortbutton-link
  [_button-type]
  (url-for :modmail/mailbox :mailbox :all))

(defn- modmail-sortbutton
  "Create a single sort button for the modmail sidebar"
  [button-type text]
  [:a {:class (modmail-sortbutton-class button-type)
       :href (modmail-sortbutton-link button-type)}
   text])

(defn- modmail-sortbuttons
  "Create the sort buttons on the modmail sidebar."
  []
  [:div.pure-button-group  {:id "sortbuttons" :role "group"}
   [:div.pure-g
    [modmail-sortbutton :recent (tr "Recent")]
    [modmail-sortbutton :unread (tr "Unread")]
    [modmail-sortbutton :mod (tr "Mod")]
    [modmail-sortbutton :user (tr "User")]]])

(defn- modmail-mailbox-link
  [mailbox-type text]
  (let [sub-selection @(subscribe [::modmail/modmail-sub-selection])
        mailbox @(subscribe [:view/mailbox])
        unread-count @(subscribe [::modmail/unread-by-mailbox mailbox-type])
        url (if (= sub-selection :all-subs)
              (url-for :modmail/mailbox :mailbox mailbox-type)
              (url-for :modmail/mailbox :mailbox mailbox-type
                       :query-args {:sub sub-selection}))]
    [button-link {:href url
                  :button-class (if (= mailbox-type mailbox)
                                  :sidebar
                                  :secondary)}
     [:span
      text
      (when (pos-int? unread-count)
        [:div {:class (cls :dib.ml2.br4.f6.lh-copy.m-w-135
                           :bg-light-red.white.hover-black)}
         unread-count])]]))

(defn modmail-sidebar
  "Create the sidebar for modmail."
  []
  (let [menu-state @(subscribe [::window/menu-state])
        active-panel @(subscribe [:view/active-panel])
        supervising-admin? @(subscribe [::modmail/supervising-admin?])
        {:keys [admin-sub]} @(subscribe [::settings/site-config])
        {:keys [sub]} @(subscribe [:view/options])]
    [:div {:class ["sidebar" "pure-u-1" "pure-u-md-6-24"
                   (when (= menu-state :open) "open")]}
     #_(when (= active-panel :modmail)
         [modmail-sortbuttons])
     #_[search "Search Modmail"]
     (when-not supervising-admin?
       [:<>
        [button-link {:button-class (when (= active-panel :modmail-compose)
                                      :sidebar)
                      :href (url-for :modmail/compose)}
         (tr "Compose a message")]
        [:hr]])
     [modmail-mailbox-link :all (tr "All")]
     [modmail-mailbox-link :new (tr "New")]
     [modmail-mailbox-link :in-progress (tr "In progress")]
     [:hr]
     [modmail-mailbox-link :archived (tr "Archived")]
     #_[modmail-mailbox-link :highlighted (tr "Highlighted")]
     [modmail-mailbox-link :discussions (tr "Mod discussions")]
     (when (= sub admin-sub)
       [modmail-mailbox-link :notifications (tr "Notifications")])
     [:hr]
     [button-link
      {:href (url-for :wiki/view :slug "modmail-help")}
      (tr "Modmail help")]
     [footer]]))

(defn modmail-menu-icon
  "Create an icon for the icon menu on a modmail message."
  [{:keys [flag class title-on title-off icon-name event]}]
  (let [icon-fill @(subscribe [::window/icon-fill])]
    [icon
     {:class (cls icon-class (or class icon-fill))
      :title (if flag title-on title-off)
      :on-click #(dispatch event)}
     icon-name]))

(defn sender-and-receiver
  [{:keys [receiver receiver-name receiver-is-mod?
           sender-name sender-is-mod?
           sent-with-name-hidden?]}]
  [:<>
   (if (nil? sender-name)
     (tr "[Deleted]")
     [userlink {:class (when sender-is-mod? (cls :custom.green))
                :prefix "/u/"}
      sender-name
      (when sent-with-name-hidden? " [as mod]")])
   (when receiver
     (if (nil? receiver-name)
       (tr " ➜ [Deleted]")
       [userlink {:class (when receiver-is-mod? (cls :custom.green))
                  :prefix " ➜ /u/"}
        receiver-name]))])

(defn- message-class
  [unread? mod-discussion?]
  (let [day? @(subscribe [::window/day?])]
    (cls :pa3.mv2.br3.bw3.br--left.shadow-7
         (when unread?
           [:bl (if day?
                  :b--orangey-brown :b--reddish-brown)])
         (if mod-discussion?
           (if day? :bg-washed-purple :bg-grim-purple)
           (if day? :alternating-near-white :alternating-near-black)))))

(defn modmail-message-in-thread-view
  "Create a modmail message for a message thread."
  [{:keys [mid html-content time receiver unread? mtype] :as message}]
  (let [supervising-admin? @(subscribe [::modmail/supervising-admin?])
        mod-discussion? (or (= mtype :MOD_DISCUSSION)
                            (and (= mtype :MOD_TO_USER_AS_MOD)
                                 (nil? receiver)))]
    [:article {:class (message-class unread? mod-discussion?)}
     [:div.mid-gray.f6.mb2
      [sender-and-receiver message]
      " - "
      [relative-time {:datetime time :prefix ""}]]
     [dangerous-html {} html-content]
     [:div.flex.mt1
      [:div.w-100.w-80-ns]
      [:div.w-100.w-20-ns.mid-gray
       [:div.fr
        (when-not supervising-admin?
          [modmail-menu-icon
           {:class (when unread? :fill-light-green)
            :icon-name "ccheck"
            :title-on (tr "Mark read")
            :title-off (tr "Mark unread")
            :event [::modmail/set-unread mid (not unread?)]}])]]]]))

(defn modmail-thread-url
  "Calculate the URL for a modmail thread, given the mid of the parent
  message, and the current mailbox and sub selections."
  [mailbox mid]
  (let [sub-selection @(subscribe [::modmail/modmail-sub-selection])]
    (if (= :all-subs sub-selection)
      (url-for :modmail/thread :mailbox mailbox :msg mid)
      (url-for :modmail/thread :mailbox mailbox :msg mid
               :query-args {:sub sub-selection}))))

(defn- modmail-log-in-thread-view
  [{:keys [action name time mailbox related-thread-id reference-type]}]
  (let [user [userlink {:class :b} name]
        convo (when related-thread-id
                (modmail-thread-url :all related-thread-id))
        reltime [relative-time {:datetime time}]]
    [:div
     (cond
       (and (= action :CHANGE_MAILBOX)
            (= mailbox :ARCHIVED))
       [helper-text (trm-html "%{user} archived this conversation %{reltime}"
                              {:user user
                               :reltime reltime})]

       (and (= action :CHANGE_MAILBOX)
            (= mailbox :INBOX))
       [helper-text (trm-html "%{user} un-archived this conversation %{reltime}"
                              {:user user
                               :reltime reltime})]

       (and (= action :RELATED_THREAD)
            (= reference-type :NEW))
       [helper-text (trm-html "%{user} started a %{convolink} %{reltime}"
                              {:user user
                               :convolink [:a {:href convo}
                                           (tr "conversation with the user")]
                               :reltime reltime})])]))

(defn- modmail-message-or-log-in-thread-view
  [{:keys [type] :as message-or-log}]
  (case type
    :message [modmail-message-in-thread-view message-or-log]
    :log [modmail-log-in-thread-view message-or-log]))

(defn modmail-message-in-mailbox-list
  "Create the latest message in the thread for the mailbox list panel."
  [{:keys [thread-id subject sub-name sub-color-class
           mid html-content time receiver unread? mtype archived?
           reply-count] :as message-thread}
   mailbox]
  (let [supervising-admin? @(subscribe [::modmail/supervising-admin?])
        icon-fill @(subscribe [::window/icon-fill])
        mod-discussion? (or (= mtype :MOD_DISCUSSION)
                            (and (= mtype :MOD_TO_USER_AS_MOD)
                                 (nil? receiver)))]
    ^{:key thread-id}
    [:article {:class (message-class unread? mod-discussion?)}
     [:div.relative
      [:div.dib.b
       [icon {:class (cls icon-class sub-color-class)} "mail"]
       [:a.dib.b.ml2 {:href (url-for :sub/view-sub :sub sub-name)}
        " " sub-name]]
      [:p.bb.bw1.b--moon-gray.ml4 subject]
      [:div.mid-gray.f6.mb2
       [sender-and-receiver message-thread]]
      [dangerous-html {} html-content]
      [:div.mid-gray.f6.flex.mt1
       [:div.w-100.w-80-ns
        (trn ["%s reply" "%s replies"] reply-count) " - "
        [relative-time {:datetime time :prefix ""}]]
       [:div.w-100.w-20-ns
        [:div.fr
         (when-not supervising-admin?
           [:<>
            [modmail-menu-icon
             {:class (when unread? :fill-light-green)
              :icon-name "ccheck"
              :title-on (tr "Mark read")
              :title-off (tr "Mark unread")
              :event [::modmail/set-unread mid (not unread?)]}]
            (when (not= mailbox :discussions)
              [modmail-menu-icon
               {:class (when archived? :fill-blue)
                :icon-name "filebox"
                :title-on (tr "Unarchive")
                :title-off (tr "Archive")
                :event [::modmail/set-archived thread-id (not archived?)]}])
            [:a {:title (tr "See thread and reply")
                 :href (modmail-thread-url mailbox thread-id)}
             [icon {:class (cls icon-class icon-fill)} "reply"]]])
         [:a.absolute.absolute--fill
          {:href (modmail-thread-url mailbox thread-id)}]]]]]]))

(defn show-username-checkbox
  "Create the show username checkbox for the modmail compose forms."
  []
  (let [show-username? @(subscribe [::forms/field compose-form :show-username?])
        form-state @(subscribe [::forms/form-state compose-form])
        editing-disabled? @(subscribe
                            [::forms/editing-disabled? compose-form])]
    [:div.inline-flex
     [checkbox {:disabled editing-disabled?
                :label (tr "Show recipient my username")
                :label-class :mv2
                :value show-username?
                :event [::forms/edit compose-form :show-username?]
                :form-state form-state}]]))

(defn send-to-user-or-mods
  "Create a pair of radio buttons to choose the destination of a modmail."
  [to-mods-label to-user-label]
  (let [send-to-user? @(subscribe [::forms/field compose-form :send-to-user?])
        form-state @(subscribe [::forms/form-state compose-form])
        editing-disabled? @(subscribe [::forms/editing-disabled? compose-form])]
    [:<>
     [radio-button
      {:name "modmail-recipient" :id "modmail-recipient-mod"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label to-mods-label
       :value "mod"
       :form-value (if send-to-user? "user" "mod")
       :event [::forms/edit compose-form :send-to-user? false]
       :form-state form-state}]
     [radio-button
      {:name "modmail-recipient" :id "modmail-recipient-user"
       :class (cls :db.mv2)
       :disabled editing-disabled?
       :label to-user-label
       :value "user"
       :form-value (if send-to-user? "user" "mod")
       :event [::forms/edit compose-form :send-to-user? true]
       :form-state form-state}]]))

(defn modmail-reply-form
  "Create the form elements for the modmail reply panel."
  []
  (let [mailbox @(subscribe [:view/mailbox])
        send-to-user? @(subscribe [::forms/field compose-form :send-to-user?])]
    [:div.pt2
     (when (and (= :notifications mailbox) send-to-user?)
       [view-messages/subject-editor])
     [view-messages/content-editor]
     [validation-error-if-present compose-form]
     (when (not= :discussions mailbox)
       [:<>
        [:div.pa2
         [send-to-user-or-mods
          (tr "Add a private moderator note")
          (if (= :notifications mailbox)
            (tr "Start a new conversation with user")
            (tr "Reply to user"))]]
        (when send-to-user?
          [show-username-checkbox])])
     [transmission-error-if-present compose-form]
     [:div.pb2
      [view-messages/send-button
       (cond
         (and send-to-user? (= :notifications mailbox))
         (tr "Send")

         (and send-to-user? (not= :discussions mailbox))
         (tr "Reply")

         :else
         (tr "Add note"))] " "
      [view-messages/preview-button]
      [view-messages/success-message-on-success]]
     [view-messages/preview]]))

(defn modmail-sub-menu
  "Create a sub picker for the modmail mailbox list view."
  []
  (let [supervising-admin? @(subscribe [::modmail/supervising-admin?])
        subs @(subscribe (if supervising-admin?
                           [::user/subs-not-moderated-names]
                           [::user/subs-moderated-names]))
        sub @(subscribe [::modmail/modmail-sub-selection])]
    [:div.fr-ns.mt3-ns.mb1
     [dropdown-menu {:value sub
                     :choices (into {:all-subs (if supervising-admin?
                                                 (tr "My circles")
                                                 (tr "All circles"))}
                                    (zipmap subs subs))
                     :event [::modmail/set-modmail-sub-selection]}]]))

(defn modmail-list
  "Create the content for the modmail list panel."
  []
  (let [options @(subscribe [:view/options])
        threads @(subscribe [::modmail/modmail-mailbox-threads])
        mailbox @(subscribe [:view/mailbox])
        pag @(subscribe [::modmail/modmail-pagination])
        {:keys [more-items-available? cursor]} pag
        status @(subscribe [::window/status])
        page-size @(subscribe [::settings/page-size])
        supervising-admin? @(subscribe [::modmail/supervising-admin?])]
    [:<>
     [:div
      [:h2.w-100.w-50-ns.dib
       (tr "Moderator Mail")]
      [:div.w-100.w-50-ns.dib
       [modmail-sub-menu]]]
     (when supervising-admin?
       [helper-text "You are viewing this circle's messages as the admin."])
     (if (and (= :loaded status) (empty? threads))
       [:h4 (tr "Nothing here, yet...")]
       (into [:div]
             (map (fn [thread]
                    [modmail-message-in-mailbox-list thread mailbox])
                  (if (= :loaded status)
                    threads
                    (take page-size threads)))))
     (when more-items-available?
       [button
        {:button-class :secondary
         :on-click #(dispatch [::modmail/load-modmail-category
                               (assoc options :cursor cursor)])}
        (tr "Load more")])]))

(defn modmail-list-panel
  "Create the modmail list panel."
  []
  [panel {:sidebar modmail-sidebar
          :footer :<>}
   [modmail-list]])

(defn- related-mod-link
  "Create a link with the shield icon for related mod content."
  [{:keys [href]} text]
  [:a.db.ml3.pb2 {:href href}
   [icon {:class (cls icon-class :fill-purple)} "shield"]
   [:span.fw7.f6 text]])

(defn- related-item
  []
  (let [{:keys [sub-name]} @(subscribe [::modmail/modmail-thread-info])
        item @(subscribe [::modmail/modmail-linked-item])
        {:keys [report-id rtype related-thread-id]} item]
    (cond
      report-id
      [related-mod-link {:href (url-for :mod/report-details
                                        :sub sub-name
                                        :report-type (-> rtype
                                                         name
                                                         str/lower-case)
                                        :report-id report-id)}
       (if (= rtype :POST)
         (tr "Linked post report")
         (tr "Linked comment report"))]

      related-thread-id
      [related-mod-link {:href (modmail-thread-url :notifications
                                                   related-thread-id)}
       "Linked modmail notification"])))

(defn modmail-thread
  "Create the content for the single-thread modmail panel."
  []
  (let [options @(subscribe [:view/options])
        mailbox @(subscribe [:view/mailbox])
        thread @(subscribe [::modmail/modmail-thread-info])
        {:keys [thread-id sub-name subject sub-color-class]} thread
        pag @(subscribe [::modmail/modmail-pagination])
        {:keys [more-items-available? cursor]} pag
        archived? (= (:mailbox thread) :ARCHIVED)
        page-size @(subscribe [::settings/page-size])
        parent-message @(subscribe [::modmail/modmail-thread-parent])
        messages-and-logs @(subscribe [::modmail/modmail-thread])
        any-unread? @(subscribe [::modmail/modmail-thread-any-unread])
        status @(subscribe [::window/status])
        supervising-admin? @(subscribe [::modmail/supervising-admin?])
        icon-fill @(subscribe [::window/icon-fill])]
    [:<>
     (when (= :loaded status)
       (if (and (nil? parent-message) (empty? messages-and-logs))
         [:h4 (tr "Nothing here, yet...")]
         [:div
          (when supervising-admin?
            [helper-text
             "You are viewing this circle's messages as the admin."])
          [:div.pb2
           [:div.w-100.w-50-ns.dib
            [icon {:class (cls :w16px.h16px.dib sub-color-class)} "mail"]
            [:a.dib.ml2.b.f4
             {:href (url-for :sub/view-sub :sub sub-name)}
             sub-name]]
           (when-not supervising-admin?
             [:div.w-100.w-50-ns.pv2.pv1-ns.dib
              (when (not= :discussions mailbox)
                [:a.mr3.fr-ns
                 {:on-click #(dispatch [::modmail/set-archived
                                        thread-id (not archived?)])}
                 [icon {:class (cls icon-class (if archived?
                                                 "fill-blue"
                                                 icon-fill))}
                  "filebox"]
                 (if archived?
                   (tr "Un-archive thread")
                   (tr "Archive thread"))])
              [:a.mr3.fr-ns
               {:on-click #(dispatch [::modmail/set-thread-unread
                                      thread-id (not any-unread?)])}
               [icon {:class (cls icon-class (if any-unread?
                                               "fill-light-green"
                                               icon-fill))}
                "ccheck"]
               (if any-unread?
                 (tr "Mark all read")
                 (tr "Mark all unread"))]])]
          [:div {:class (cls :ml3.mb3.b.bb.b--moon-gray)}
           subject]
          [related-item]
          (into [:div
                 (modmail-message-in-thread-view parent-message)
                 (when more-items-available?
                   [:a
                    {:on-click #(dispatch [::modmail/load-modmails
                                           (assoc options :cursor cursor)])}
                    "▹ "
                    [:span.i (tr "Show older replies")]])]
                (map modmail-message-or-log-in-thread-view
                     (if (= :loaded status)
                       messages-and-logs
                       (take-last page-size messages-and-logs))))
          (when-not supervising-admin?
            [:<>
             [modmail-reply-form]
             [markdown-link-modal]])]))]))

(defn modmail-thread-panel
  "Create the modmail thread panel."
  []
  [panel {:sidebar modmail-sidebar
          :footer :<>}
   [modmail-thread]])

(defn mod-of-sub-or-admin-only
  "Create the appropriate panel for modmail depending on the selected sub."
  [panel]
  (let [sub @(subscribe[::modmail/modmail-sub-selection])
        subs-moderated (set @(subscribe [::user/subs-moderated-names]))
        is-admin? @(subscribe [::user/is-admin?])
        sub-load-status @(subscribe [::subs/sub-load-status])
        content-status @(subscribe [::window/status])
        sub-names (set @(subscribe [::subs/all-sub-names]))]
    (cond
      (= sub :all-subs)
      panel

      (subs-moderated sub)
      panel

      (not is-admin?)
      [misc-panels/unauthorized-panel]

      (or (= sub-load-status :loading) (= content-status :loading))
      [misc-panels/waiting-panel]

      (sub-names sub)
      panel

      :else
      [misc-panels/page-not-found-panel])))

(defn modmail-panel
  "Create a modmail panel listing messages in a mailbox or in a thread."
  []
  (let [options @(subscribe [:view/options])]
    [mod-of-sub-or-admin-only
     (if (:thread-id options)
       [modmail-thread-panel]
       [modmail-list-panel])]))

(defn modmail-compose-report-link
  "Create the link to a report referenced by a modmail, if any."
  []
  (when @(subscribe [::modmail/linkable-to-report?])
    (let [sub @(subscribe [::forms/field compose-form :sub])
          report-type @(subscribe [::forms/field compose-form :report-type])
          report-id @(subscribe [::forms/field compose-form :report-id])
          form-state @(subscribe [::forms/form-state compose-form])
          reflink [:a {:href (url-for :mod/report-details
                                      :sub sub
                                      :report-type report-type
                                      :report-id report-id)}
                   (if (= report-type "comment")
                     (tr "Comment Report")
                     (tr "Post Report"))]]
      [:p.i (if (not= :SUCCESS form-state )
              (trm-html "Message will be linked to: %{reflink}"
                        {:reflink reflink})
              (trm-html "Message was linked to: %{reflink}"
                        {:reflink reflink}))])))

(defn modmail-compose-form
  "Create the form elements for the modmail compose new message panel."
  []
  (let [username (fn [] @(subscribe [::forms/field compose-form :username]))]
    (fn []
      (let [send-to-user? @(subscribe
                            [::forms/field compose-form :send-to-user?])
            subs-moderated @(subscribe [::user/subs-moderated-names])
            sub @(subscribe [::forms/field compose-form :sub])
            form-state @(subscribe [::forms/form-state compose-form])
            editing-disabled? @(subscribe
                                [::forms/editing-disabled? compose-form])]
        [:div
         [dropdown-menu {:id "modmail-compose-circle"
                         :label "Send from: "
                         :disabled editing-disabled?
                         :choices
                         (into {:nothing-selected (tr "Choose a circle")}
                               (zipmap subs-moderated subs-moderated))
                         :value sub
                         :event [::forms/edit compose-form :sub]}]
         [:div
          [send-to-user-or-mods
           (tr "Mod Discussion")
           (tr "To User")]
          (when send-to-user?
            [:div.inline-flex.pv1
             [text-input {:id "modmail-compose-name"
                          :placeholder (tr "username")
                          :value username
                          :required true
                          :update-value! forms/reset-input-on-success!
                          :form-state form-state
                          :disabled editing-disabled?
                          :event [::forms/edit compose-form :username]}]
             [show-username-checkbox]])]
         [view-messages/subject-editor]
         [view-messages/content-editor]
         [markdown-link-modal]
         [validation-error-if-present compose-form]
         [transmission-error-if-present compose-form]
         [modmail-compose-report-link]
         [:div.pv2
          [view-messages/send-button (tr "Send")] " "
          [view-messages/preview-button]
          [view-messages/success-message-on-success]
          [view-messages/preview]]]))))

(defn modmail-compose-panel
  "Create the modmail compose new message panel."
  []
  [panel {:sidebar modmail-sidebar
          :footer :<>}
   [:h2 (tr "New Modmail Message")]
   [modmail-compose-form]])
