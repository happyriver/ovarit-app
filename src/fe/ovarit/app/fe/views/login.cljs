;; fe/views/auth.cljs -- Registration and login for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.views.login
  (:require
   [ovarit.app.fe.ui.forms.login :as login-form]
   [ovarit.app.fe.ui.forms.core :as forms]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.forms :as form-views]
   [ovarit.app.fe.views.inputs :refer [checkbox button text-input]]
   [ovarit.app.fe.tr :refer [tr]]
   [ovarit.app.fe.views.util :as util :refer [cls url-for]]
   [re-frame.core :refer [dispatch subscribe]]))

(defn sign-up-form-content
  "Create content for the login or register form."
  []
  (let [form ::login-form/login
        username (fn [] @(subscribe [::forms/field form :username]))
        password (fn [] @(subscribe [::forms/field form :password]))
        remember-me? @(subscribe [::forms/field form :remember-me?])
        send #(do (.preventDefault %)
                  (dispatch [::forms/action form :send]))
        colors @(subscribe [::window/button-colors])]
    [:<>
     [:div.pb3
      [:h3 (tr "Log in")]
      [:form
       [text-input {:auto-complete :username
                    :class [#"^dib$" :db.mb2]
                    :value username
                    :update-value! forms/reset-input-on-success!
                    :event [::forms/edit form :username]
                    :placeholder (tr "Username")}]
       [text-input {:type :password
                    :auto-complete :current-password
                    :class [#"^dib$" :db.mb2]
                    :value password
                    :event [::forms/edit form :password]
                    :update-value! forms/reset-input-on-success!
                    :placeholder (tr "Password")}]
       [checkbox {:id :remember-me
                  :value remember-me?
                  :event [::forms/edit form :remember-me?]
                  :label (tr "Remember me")}]
       [form-views/validation-error-if-present form]
       [form-views/transmission-error-if-present form]
       [button {:class :mv3
                :on-click send}
        (tr "Log in")]]]

     [:a {:href (url-for :auth/start-reset)}
      (tr "Forgot your password?")]

     [:div.pv2
      [:h3 (tr "Register")]
      [:p (tr "Don't have an account?")]
      [:a.custom.dib.pv2.ph3.br1.bn.pointer
       {:href (url-for :auth/register)
        :class (cls colors)}
       (tr "Register now!")]]]))
