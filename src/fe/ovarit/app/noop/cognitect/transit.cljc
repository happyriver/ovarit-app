(ns ovarit.app.noop.cognitect.transit
  (:refer-clojure :exclude [read]))

#?(:clj (defmacro read [& _opts] nil))
#?(:clj (defmacro reader [& _opts] nil))
