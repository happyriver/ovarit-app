(ns ovarit.app.noop.clojure.spec.alpha
  (:refer-clojure :exclude [* and assert cat keys merge or])
  #?(:cljs (:require-macros ovarit.app.noop.clojure.spec.alpha)))

#?(:clj (defmacro * [& _forms]))
#?(:clj (defmacro and [& _forms]))
#?(:clj (defmacro assert [& _forms]))
#?(:clj (defmacro cat [& _forms]))
#?(:clj (defmacro coll-of [& _forms]))
#?(:clj (defmacro def [& _forms]))
#?(:clj (defmacro fdef [& _forms]))
#?(:clj (defmacro gen [& _forms]))
#?(:clj (defmacro keys [& _forms]))
#?(:clj (defmacro map-of? [& _forms]))
#?(:clj (defmacro merge [& _forms]))
#?(:clj (defmacro multi-spec [& _forms]))
#?(:clj (defmacro nilable [& _forms]))
#?(:clj (defmacro or [& _forms]))
#?(:clj (defmacro valid? [& _forms]))
#?(:clj (defmacro with-gen [& _forms]))
