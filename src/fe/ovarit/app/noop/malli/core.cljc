(ns ovarit.app.noop.malli.core
  #?(:cljs (:require-macros ovarit.app.noop.malli.core)))

#?(:clj (defmacro validate [_ _val] true))
#?(:clj (defmacro decode [_ val & _forms] val))
#?(:clj (defmacro explain [& _forms]))
