;; fe/portfolio/sub-bar.cljs -- Sample sub bars for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.sub-bar
  (:require
   [ovarit.app.fe.views.sub-bar :refer [bar]]
   [portfolio.reagent :refer-macros [defscene]]
   [reagent.core :as reagent]))

(defn init
  "Initialize any state here."
  [])

(def sshort (reagent/atom ["Fish" "Frogs" "forks"]))
(def smedium (reagent/atom  ["Fish" "Frogs" "forks" "fondue" "Flippers"]))
(def slong (reagent/atom
            ["10000YearsOfBill" "ActualFootball" "AdultSwim" "africa" "AlanWatts"
             "Albums" "Alive" "all" "AMA" "Amaranth" "Amazon" "Ambient"
             "Anarcho_Capitalism" "Android" "AndroidThoughts" "Animals"
             "Animation" "Anime" "ANNOUNCEMENT" "announcements" "AnnoyingSongs"
             "AnonTalk" "Apple" "Aquariums" "Archaeology" "architecture"
             "ArmsNArmor" "Art" "ArtisanVideos" "Asia" "AskPhuks" "ASMR"
             "Asparagus" "Astronomy" "Atheism" "audiobooks" "AudiovisualJunkyard"
             "AussieBanter" "Australia" "Austria" "Aww" "BadCopNoDonut"
             "BadMovies"]))

(defn unauthenticated-sub-bar
  []
  [bar sshort nil nil])

(defn authenticated-sub-bar
  []
  [bar sshort true nil])

(defn admin-sub-bar
  []
  [bar smedium true true])

(defn long-sub-bar
  []
  [bar slong true nil])

(defscene unauthenticated-sub-bar [unauthenticated-sub-bar])
(defscene authenticated-sub-bar [authenticated-sub-bar])
(defscene admin-sub-bar [admin-sub-bar])
(defscene long-sub-bar [long-sub-bar])
