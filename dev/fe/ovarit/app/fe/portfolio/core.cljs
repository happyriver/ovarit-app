;; fe/portfolio/core.cljs -- Start the portfolio ui for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.core
  (:require
   ["@github/relative-time-element"]
   [lambdaisland.glogi :as glogi]
   [lambdaisland.glogi.console :as glogi-console]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.db :as db]
   [ovarit.app.fe.portfolio.automod :as automod]
   [ovarit.app.fe.portfolio.inputs :as inputs]
   [ovarit.app.fe.portfolio.markdown :as markdown]
   [ovarit.app.fe.portfolio.modmail :as modmail]
   [ovarit.app.fe.portfolio.movable-list :as movable-list]
   [ovarit.app.fe.portfolio.sub-bar :as sub-bar]
   [ovarit.app.fe.portfolio.forms.user-flair :as user-flair-form]
   [ovarit.app.fe.ui.window :as window]
   [portfolio.ui :as ui]
   [re-frame.core :as re-frame]))

(defn dev-setup []
  (when config/debug?
    (glogi-console/install!)
    (glogi/set-levels
     ;; Set a root logger level, this will be inherited by all loggers
     {:glogi/root :info})))

(defn init []
  (dev-setup)
  (re-frame/dispatch-sync [::db/initialize-db])
  (re-frame/dispatch [::window/clock-tick])
  (re-frame/dispatch [::window/start-resize-watcher])
  (re-frame/dispatch [::window/start-focus-watcher])

  (automod/init)
  (inputs/init)
  (markdown/init)
  (modmail/init)
  (movable-list/init)
  (sub-bar/init)
  (user-flair-form/init)

  (ui/start!
   {:config {:css-paths ["/css/purecss/base.css"
                         "/css/pikaday/pikaday.css"
                         "/css/tachyons.css"
                         "/css/main.css"
                         "/css/dark.css"
                         "/css/ovarit-app.css"
                         "/css/custom.css"]}}))
