;; fe/portfolio/automod.cljs -- Automod prototype for ovarit-app
;; Copyright (C) 2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.automod
  (:require
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.views.common :refer [helper-text]]
   [ovarit.app.fe.views.inputs :refer [text-input textarea-input select]]
   [ovarit.app.fe.views.util :refer [cls]]
   [portfolio.reagent :refer-macros [defscene]]
   [re-frame.core :as re-frame :refer [dispatch subscribe]]))

(defn init
  "Initialize any state here."
  [])

(re-frame/reg-sub ::edit-automod-value
  (fn [db _]
    (get-in db [:portfolio ::automod :value])))

(re-frame/reg-sub ::automod-state
  (fn [db _]
    (get-in db [:portfolio ::automod :state])))

(re-frame/reg-event-db ::set-automod-value
  (fn [db [_ val]]
    (-> db
        (assoc-in [:portfolio ::automod :value] val)
        (assoc-in [:portfolio ::automod :state] :updated))))

(re-frame/reg-event-db ::reset-automod-state
  (fn [db [_ _]]
    (-> db
        (assoc-in [:portfolio ::automod :value] "")
        (assoc-in [:portfolio ::automod :state] :reset))))

(defn- clear-on-reset!
  "Helper function for the text input scenes."
  [input-state props]
  (if (= (:form-state props) :reset)
    (when-not (:already-updated? @input-state)
      (swap! input-state assoc
             :value ""
             :already-updated? true))
    (swap! input-state assoc :already-updated? false))
  (dissoc props :form-state))

(re-frame/reg-event-db ::reset-action-picker
  (fn [db [_ _]]
    (assoc-in db [:portfolio ::action-picker] :nothing-selected)))

(re-frame/reg-event-db ::set-action-picker
  (fn [db [_ value]]
    (assoc-in db [:portfolio ::action-picker] value)))

(re-frame/reg-sub ::action-picker
  (fn [db _]
    (get-in db [:portfolio ::action-picker])))

(defn create-moderation-rule
  []
  (let [content (fn [] @(subscribe [::edit-automod-value]))
        form-state @(subscribe [::automod-state])
        reset #(dispatch [::reset-automod-state])
        day? @(subscribe [::window/day?])
        form-colors (if day? :bg-white :bg-black)
        action (fn [] @(subscribe [::action-picker]))
        names ["Delete"
               "Do nothing"
               "Report"
               "Hide and Report"
               "Discuss"
               "Hide and Discuss"]]
    [:div.pa4.bg-gray
     [:div {:class (cls :w-80.h-auto.pa2.br3 form-colors)}
      [:div.pa4
       [:div
        [:h2 "Create a new moderation rule"]
        [:p "Rule name"]
        [text-input
         {:id "text"
          :class :w-100
          :placeholder "Placeholder"
          :value content
          :form-state form-state
          :update-value! clear-on-reset!
          :event [::set-automod-value]}]
        [:p "Words or phrases that will activate this rule"]
        [textarea-input {:value content
                         :class :w-100.mb2.h6
                         :required true
                         :form-state form-state
                         :update-value! clear-on-reset!
                         :event [::set-automod-value]}]
        [:div.pure-form.pure-control-group.w-50.mt2
         [select {:label "Select an action:"
                  :id "m-id"
                  :options (into [[:nothing-selected "Choose..."]]
                                 (zipmap names names))
                  :event [::set-action-picker]}]]
        [helper-text "This action will be applied to posts and comments."]

        ]]]]
    ))

(defscene create-moderation-rule
  [create-moderation-rule])
