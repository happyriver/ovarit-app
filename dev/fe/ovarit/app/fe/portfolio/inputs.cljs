;; fe/portfolio/inputs.cljs -- Samples for input widgets
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.inputs
  (:require
   [cljs-time.core :as time]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.views.common :refer [dangerous-html helper-text]]
   [ovarit.app.fe.views.inputs :refer [button radio-button text-input
                                       suggesting-text-input
                                       markdown-editor markdown-link-modal
                                       datepicker date-selector
                                       select]]
   [portfolio.reagent :refer-macros [defscene]]
   [re-frame.core :as re-frame :refer [reg-sub reg-event-db subscribe
                                       dispatch]]

   [reagent.core :as reagent]))

(defn init
  "Initialize db state here."
  []
  (dispatch [::set-radio-group "one"])
  (dispatch [::reset-edit-text-state])
  (dispatch [::reset-suggesting-text-state])
  (dispatch [::reset-edit-markdown-state])
  (dispatch [::reset-datepicker])
  (dispatch [::reset-sub-picker]))

(defn day-night []
  [button
   {:button-class :primary
    :id "reset-text-editor"
    :style {:display "inline-block"}
    :on-click #(dispatch [::window/toggle-daynight])} "Toggle Night Mode"]  )

(defscene day-night [day-night])

(reg-sub
 ::radio-group
 (fn [db _]
   (get-in db [:portfolio ::radio-group])))

(reg-event-db
 ::set-radio-group
 (fn [db [_ val]]
   (assoc-in db [:portfolio ::radio-group] val)))

(defn radio-group []
  (let [form-value @(subscribe [::radio-group])]
    [:div (str {:form-value form-value})
     [:div.pure-form
      [:fieldset
       [radio-button {:id "radio-one"
                      :class "pure-radio"
                      :name "radio"
                      :label "One"
                      :value "one"
                      :form-value form-value
                      :event [::set-radio-group]}]
       [radio-button {:id "radio-two"
                      :class "pure-radio"
                      :name "radio"
                      :label "Two"
                      :value "two"
                      :form-value form-value
                      :event [::set-radio-group]}]
       [radio-button {:id "radio-three"
                      :class "pure-radio"
                      :name "radio"
                      :label "Three"
                      :value "three"
                      :form-value form-value
                      :event [::set-radio-group]}]]]]))

(defscene radio-group [radio-group])

(defn- clear-on-reset!
  "Helper function for the text input scenes."
  [input-state props]
  (if (= (:form-state props) :reset)
    (when-not (:already-updated? @input-state)
      (swap! input-state assoc
             :value ""
             :already-updated? true))
    (swap! input-state assoc :already-updated? false))
  (dissoc props :form-state))

(reg-sub
 ::edit-text-value
 (fn [db _]
   (get-in db [:portfolio ::edit-text :value])))

(reg-sub
 ::edit-text-state
 (fn [db _]
   (get-in db [:portfolio ::edit-text :state])))

(reg-event-db
 ::set-edit-text-value
 (fn [db [_ val]]
   (-> db
       (assoc-in [:portfolio ::edit-text :value] val)
       (assoc-in [:portfolio ::edit-text :state] :updated))))

(reg-event-db
 ::reset-edit-text-state
 (fn [db [_ _]]
   (-> db
       (assoc-in [:portfolio ::edit-text :value] "")
       (assoc-in [:portfolio ::edit-text :state] :reset))))

(defn edit-text []
  (let [content (fn [] @(subscribe [::edit-text-value]))
        form-state @(subscribe [::edit-text-state])
        reset #(dispatch [::reset-edit-text-state])]
    [:div
     [:p (str {:value (content) :form-state form-state})]
     [:div.pure-form
      [:div.fieldset
       [text-input
        {:id "text"
         :placeholder "Placeholder"
         :value content
         :form-state form-state
         :update-value! clear-on-reset!
         :event [::set-edit-text-value]}]
       [:div.pure-controls
        [button {:class :mv2
                 :button-class :primary
                 :id "reset-text-editor"
                 :style {:display "inline-block"}
                 :on-click reset} "Reset"]]]]]))

(defscene edit-text [edit-text])

(reg-sub
 ::suggesting-text-value
 (fn [db _]
   (get-in db [:portfolio ::suggesting-text :value])))

(reg-sub
 ::suggesting-text-state
 (fn [db _]
   (get-in db [:portfolio ::suggesting-text :state])))

(reg-event-db
 ::set-suggesting-text-value
 (fn [db [_ val]]
   (-> db
       (assoc-in [:portfolio ::suggesting-text :value] val)
       (assoc-in [:portfolio ::suggesting-text :state] :updated))))

(reg-event-db
 ::reset-suggesting-text-state
 (fn [db [_ _]]
   (-> db
       (assoc-in [:portfolio ::suggesting-text :value] "")
       (assoc-in [:portfolio ::suggesting-text :state] :reset))))

(defn suggesting-text []
  (let [content (fn [] @(subscribe [::suggesting-text-value]))
        form-state @(subscribe [::suggesting-text-state])
        reset #(dispatch [::reset-suggesting-text-state])
        choices ["aluminum" "enumerate" "misvalue" "MAGNUM" "molybdenum"
                 "monument" "niobium" "Numb" "Number" "numeral" "platinum"
                 "quaalude" "salute" "sphagnum" "sternum" "value"]]
    [:div
     [:p (str {:value (content) :form-state form-state
               :choices choices})]
     [:div
      [suggesting-text-input
       {:id "text"
        :class ["mb1"]
        :placeholder "Placeholder"
        :value content
        :choices choices
        :form-state form-state
        :update-value! clear-on-reset!
        :event [::set-suggesting-text-value]}]
      [:div.pure-controls
       [button
        {:button-class :primary
         :id "reset-suggesting-text--editor"
         :style {:display "inline-block"}
         :on-click reset} "Reset"]]]]))

(defscene suggesting-text [suggesting-text])

(reg-sub
 ::edit-markdown-value
 (fn [db _]
   (get-in db [:portfolio ::edit-markdown :value])))

(reg-sub
 ::edit-markdown-state
 (fn [db _]
   (get-in db [:portfolio ::edit-markdown :state])))

(reg-event-db
 ::set-edit-markdown-value
 (fn [db [_ val]]
   (-> db
       (assoc-in [:portfolio ::edit-markdown :value] val)
       (assoc-in [:portfolio ::edit-markdown :state] :updated))))

(reg-event-db
 ::reset-edit-markdown-state
 (fn [db [_ _]]
   (-> db
       (assoc-in [:portfolio ::edit-markdown :value] "")
       (assoc-in [:portfolio ::edit-markdown :state] :reset))))

(defn edit-markdown []
  (let [content (fn [] @(subscribe [::edit-markdown-value]))
        form-state @(subscribe [::edit-markdown-state])
        reset #(dispatch [::reset-edit-markdown-state])]
    [:div
     [:p (str {:value (content) :form-state form-state})]
     [:div
      [markdown-editor
       {:id "markdown"
        :placeholder "Placeholder"
        :value content
        :form-state form-state
        :update-value! clear-on-reset!
        :class :w-100
        :event [::set-edit-markdown-value]}]
      [:div.pure-controls
       [button {:button-class :primary
                :id "reset-markdown-editor"
                :style {:display "inline-block"}
                :on-click reset} "Reset"]]
      [:div
       [:p "Click anywhere outside the text area for preview:"]
       [dangerous-html {} (util/markdown-to-html (content))]]
      [markdown-link-modal]]]))

(defscene edit-markdown [edit-markdown])

(re-frame/reg-event-db ::reset-datepicker
  (fn [db [_ _]]
    (assoc-in db [:portfolio ::datepicker] {:year 2021 :month 8 :day 5})))

(re-frame/reg-event-db ::set-datepicker
  (fn [db [_ key value]]
    (let [previous (get-in db [:portfolio ::datepicker])
          date (assoc previous key (js/parseInt value))
          days-in-month (time/day (time/last-day-of-the-month (:year date) (:month date)))
          new {:year (:year date)
               :month (:month date)
               :day (if (> (:day date) days-in-month) 1 (:day date))}]
      (assoc-in db [:portfolio ::datepicker] new))))

(re-frame/reg-sub ::datepicker-current-date
  (fn [db _]
    (get-in db [:portfolio ::datepicker])))

(defn choose-date []
  (let [date @(subscribe [::datepicker-current-date])]
    [:div
     [datepicker {:start-year 2015
                  :end-year (:year date)
                  :subscription [::datepicker-current-date]
                  :setter [::set-datepicker]}]
     [:br]
     [:div "Year: " (:year date) " | Month: " (:month date) " | Day: " (:day date)]]))

(defscene choose-date [choose-date])

(re-frame/reg-event-db ::reset-sub-picker
  (fn [db [_ _]]
    (assoc-in db [:portfolio ::sub-picker] :nothing-selected)))

(re-frame/reg-event-db ::set-sub-picker
  (fn [db [_ value]]
    (assoc-in db [:portfolio ::sub-picker] value)))

(re-frame/reg-sub ::sub-picker-subs
  (fn [db _]
    (get-in db [:portfolio ::sub-picker])))

(def names ["Announcements" "Bugs" "Lisp"])

(defn choose-sub-menu []
  (let [sub (fn [] @(subscribe [::sub-picker-subs]))]
    [:div.mw5
     [select {:label "Circle: "
              :id "m-id"
              :value (or sub :nothing-selected)
              :options (into [[:nothing-selected "Choose..."]]
                             (zipmap names names))
              :event [::set-sub-picker]}]]))

(defscene choose-sub-menu [choose-sub-menu])

(defn- before
  "Return a new js/Date which is the given number of days before the given date"
  [date days]

  (js/Date. (.getFullYear date) (.getMonth date) (- (.getDate date) days)))

(defn date? [x]
  (= (type x) js/Date))

(def today (js/Date.))
(def yesterday (before today 1))
(def last-week (before today 7))
(def last-week-yesterday (before today 8))

(defonce start-date (reagent/atom last-week-yesterday))

(defonce end-date (reagent/atom yesterday))

(defn set-date!
  "Set a date. which is either :start or :end."
  [which date]
  (reset! (which {:start start-date :end end-date}) date))

(defn get-date! [which]
  (let [js-date @(which {:start start-date :end end-date})]
    (if (date? js-date)
      (.toLocaleDateString js-date "en" "%d-%b-%Y")
      "unselected")))

(defn date-picker []
  [:form.pure-form
   [helper-text
    "Select start and end dates. "
    "End date can be no later than today."]
   [:div
    [:label {:for "start"} "Start: "]
    [date-selector
     {:date-atom start-date
      :max-date-atom end-date
      :on-select #(reset! start-date %)
      :pikaday-attrs {:max-date today
                      :format "MM/DD/YYYY"}
      :input-attrs {:id "start"
                    :placeholder "Start date"
                    :auto-complete "off"}}]]
   [:br]
   [:div
    [:label {:for "end"} "End: "]
    [date-selector
     {:date-atom end-date
      :min-date-atom start-date
      :on-select #(reset! end-date %)
      :pikaday-attrs {:max-date today
                      :format "MM/DD/YYYY"}
      :input-attrs {:id "end"
                    :placeholder "End date"
                    :auto-complete "off"}}]]
   [:div
    [:p "Selection: " (get-date! :start) " to " (get-date! :end)]
    [:p
     [button
      {:class :dib
       :button-class :secondary
       :on-click #(set-date! :start today)} "Start today"] " "
     [button
      {:class :dib
       :button-class :secondary
       :on-click #(set-date! :start last-week)} "Start last week"] " "
     [button
      {:class :dib
       :button-class :secondary
       :on-click #(do (set-date! :start nil) (set-date! :end nil))} "Unset both"]]]])


(defscene date-picker [date-picker])
