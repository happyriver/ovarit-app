;; fe/portfolio/modmail.cljs -- Samples for ovarit-app modmail messages
;; Copyright (C) 2021-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.modmail
  (:require
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.views.modmail :as modmail]
   [portfolio.reagent :refer-macros [defscene]]
   [re-frame.core :refer [dispatch-sync]]))

(defn init
  "Initialize any state here."
  []
  (dispatch-sync [::util/start-markdown-renderer {:sub-prefix "s"}]))

(defn mark-it-down [msg]
  (assoc msg :html-content
         (util/markdown-to-html (:content msg))))

(def unread-message-from-user
  {:mid "mid"
   :mtype :USER_TO_MOD
   :content (str "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. ")
   :time "2023-11-21T01:51:03.786534556Z"
   :receiver nil
   :sender {:name "user"}
   :sender-is-mod? false
   :unread? true})

(def thread-with-unread-message-from-user
  {:id "xxxxx"
   :latest-message unread-message-from-user
   :subject "Why so many jars?"
   :sub {:name "Jars"}
   :reply-count 0})

(def read-message-to-user
  {:mid "mid"
   :mtype :MOD_TO_USER_AS_MOD
   :content (str "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. "
                 "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. ")
   :time "2023-11-21T01:51:03.786534556Z"
   :receiver {:name "user"}
   :sender {:name "mod"}
   :sender-is-mod? true
   :sent-with-name-hidden? true
   :unread? false})

(def thread-with-read-message-to-user
  {:id "yyyyy"
   :subject "Jar memes"
   :latest-message read-message-to-user
   :sub {:name "Jars"}
   :reply-count 1
   :mailbox :INBOX})

(def read-mod-discussion
  {:mid "mid"
   :mtype :MOD_DISCUSSION
   :content  (str "No archive button on this one.")
   :time "2023-11-21T01:51:03.786534556Z"
   :receiver {:name "user"}
   :sender {:name "mod"}
   :sender-is-mod? true
   :sent-with-name-hidden? false})

(def thread-with-read-mod-discussion
  {:id "zzzzz"
   :subject "Jar memes"
   :sub {:name "Jars"}
   :reply-count 4
   :latest-message read-mod-discussion})

(defn- unread-modmail-message-from-user
  [_ _]
  [modmail/modmail-message-in-mailbox-list
   (-> thread-with-unread-message-from-user
       (update :latest-message mark-it-down)) :all])

(defn read-modmail-message-to-user
  [_ _]
  [modmail/modmail-message-in-mailbox-list
   (-> thread-with-read-message-to-user
       (update :latest-message mark-it-down)) :all])

(defn read-modmail-message-to-user-threaded-view
  [_ _]
  [modmail/modmail-message-in-thread-view (mark-it-down read-message-to-user)])

(defn archived-modmail-message-to-user
  [_ _]
  [modmail/modmail-message-in-mailbox-list
   (-> thread-with-read-message-to-user
       (assoc :mailbox :ARCHIVED)
       (update :latest-message mark-it-down)) :archived])

(defn read-modmail-mod-discussion
  [_ _]
  [modmail/modmail-message-in-mailbox-list
   (-> thread-with-read-mod-discussion
       (update :latest-message mark-it-down)) :discussions])

(defscene unread-modmail-message-from-user unread-modmail-message-from-user)
(defscene read-modmail-message-to-user read-modmail-message-to-user)
(defscene read-modmail-message-to-user-threaded-view read-modmail-message-to-user-threaded-view)
(defscene archived-modmail-message-to-user archived-modmail-message-to-user)
(defscene read-modmail-mod-discussion read-modmail-mod-discussion)
