;; fe/portfolio/movable-list.cljs -- Sample movable list input
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.movable-list
  (:require
   [ovarit.app.fe.util :as util]
   [ovarit.app.fe.views.movable-list :refer [movable-list]]
   [portfolio.reagent :refer-macros [defscene]]
   [re-frame.core :as re-frame :refer [subscribe dispatch]]))

(def lorem ["Lorem" "ipsum" "dolor" "sit" "amet," "consectetur" "adipiscing"
            "elit," "sed" "do" "eiusmod" "tempor" "incididunt" "ut" "labore"
            "et" "dolore" "magna" "aliqua." "Ut" "enim" "ad" "minim"
            "veniam," "quis" "nostrud" "exercitation" "ullamco" "laboris"
            "nisi" "ut" "aliquip" "ex" "ea" "commodo" "consequat." "Duis"
            "aute" "irure" "dolor" "in" "reprehenderit" "in" "voluptate"
            "velit" "esse" "cillum" "dolore" "eu" "fugiat" "nulla"
            "pariatur." "Excepteur" "sint" "occaecat" "cupidatat" "non"
            "proident," "sunt" "in" "culpa" "qui" "officia" "deserunt"
            "mollit" "anim" "id" "est" "laborum."])

(defn init
  "Initialize any state here."
  []
  (dispatch [::set-values (take 5 lorem)]))

(defn render-item
  [{:keys [selected?] :as props} text]
  [(if selected? :p :li) (dissoc props :selected?)
   [:span.postrule {:data-movable-list-handle true} text] " "])

(defn render-list
  [props & children]
  (into [:ul props] children))

(re-frame/reg-event-db ::set-values
  (fn [db [_ val]]
    (assoc-in db [:portfolio ::values] val)))

(re-frame/reg-sub ::values
  (fn [db _]
    (get-in db [:portfolio ::values])))

(re-frame/reg-event-db ::reorder-values
  (fn [db [_ move-index dest-index]]
    (let [values (get-in db [:portfolio ::values])
          reordered-values (util/reorder-list values move-index dest-index)]
      (assoc-in db [:portfolio ::values] reordered-values))))

(defn rearrangable-list []
  (let [values @(subscribe [::values])]
    [:div
     [:button {:on-click #(dispatch [::set-values (take 5 lorem)])} "Short"] " "
     [:button {:on-click #(dispatch [::set-values lorem])} "Long"]
     [movable-list {:values values
                    :change-event [::reorder-values]
                    :list-comp render-list
                    :item-comp render-item}]]))

(defscene rearrangable-list [rearrangable-list])
