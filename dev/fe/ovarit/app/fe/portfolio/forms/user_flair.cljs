;; fe/portfolio/forms/user_flair.cljs -- Prototype for user flair form variations
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.portfolio.forms.user-flair
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [clojure.test.check.generators :as gen]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.routes]
   [ovarit.app.fe.ui.forms.user :refer [initialize-user-flair-form]]
   [ovarit.app.fe.ui.window :as window]
   [ovarit.app.fe.user.spec :as user]
   [ovarit.app.fe.util.relic :as rel]
   [ovarit.app.fe.views.inputs :refer [button]]
   [ovarit.app.fe.views.sidebar :refer [user-flair-form-content]]
   [ovarit.app.fe.views.util :refer [cls]]
   [portfolio.reagent :refer-macros [defscene]]
   [re-frame.core :as re-frame :refer [dispatch subscribe]]))

(defn init
  "Initialize any state here."
  []
  (dispatch [::initialize-custom])
  (dispatch [::initialize-presets])
  (dispatch [::initialize-custom-and-presets]))

(defscene day-night
  [button
   {:button-class :primary
    :class :dib
    :id "day-night"
    :on-click #(dispatch [::window/toggle-daynight])} "Toggle Night Mode"])

(def current-user
  "A current-user object for the db that passes the specs."
  (-> ::user/current-user
      spec/gen
      (as-> $ (gen/fmap #(assoc % :name "username") $))
      gen/generate))

(defn init-form
  [db form-id {:keys [sid name] :as sub}]
  (log/info {:form-id form-id} "initializing form")
  (-> db
      (assoc :current-user current-user)
      (assoc-in [:view :options :sub] name)
      (update :reldb rel/transact [:insert-or-replace :Sub sub])
      (update :reldb rel/transact
              [:insert :SubName
               {:sid sid
                :name name
                :lc-name (str/lower-case name)}])
      (initialize-user-flair-form form-id)
      (update :reldb rel/transact
              [:update :FormState {:actions {}} [= [:_ form-id] :id]])))

(defn reset-button [event]
  [button {:button-class :secondary
           :class :dib.mv2
           :on-click #(dispatch event)}
   "Reset"])

(re-frame/reg-event-db ::initialize-custom
  (fn [db [_ _]]
    (init-form db ::custom {:name "Custom"
                            :sid "custom"
                            :freeform-user-flairs true
                            :user-flair "Flair"
                            :user-flair-choices nil})))

(defn- form-wrapper
  [form]
  (let [day? @(subscribe [::window/day?])
        form-colors (if day? :bg-white :bg-black)]
    [:div {:class (cls :w-80.h-auto.pa2.br3 form-colors)}
     [:div.pa4
      form]]))

(defscene user-flair-form-custom
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form ::custom}]]
   [reset-button [::initialize-custom]]])

(re-frame/reg-event-db ::initialize-presets
  (fn [db [_ _]]
    (init-form db ::presets
               {:name "Presets"
                :sid "presets"
                :user-flair "Flair"
                :user-flair-choices ["Choice 1"
                                     "Choice 2"
                                     "Some flair"
                                     "Some other flair"
                                     "More flair"
                                     "Flair"
                                     "aaaaaaaaaaaaaaaaaaaaaaaa"]
                :user-can-flair-self true
                :freeform-user-flairs false})))

(defscene user-flair-form-presets
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form ::presets}]]
   [reset-button [::initialize-presets]]])

(re-frame/reg-event-db ::initialize-custom-and-presets
  (fn [db [_ _]]
    (init-form db ::custom-and-presets
               {:name "PresetsAndCustom"
                :sid "presetsandcustom"
                :user-flair nil
                :user-flair-choices ["Flair"
                                     "More Flair"
                                     "Some flair"
                                     "Some other flair"
                                     "This flair"
                                     "Flair 😼"
                                     "aaaaaaaaaaaaaaaaaaaaaaaa"]
                :freeform-user-flairs true})))

(defscene user-flair-form-custom-and-presets
  [:div.pa4.bg-gray
   [form-wrapper [user-flair-form-content {:form ::custom-and-presets}]]
   [reset-button [::initialize-custom-and-presets]]])
