;; fe/spec/reldb.cljs -- Validate reldb spec for ovarit-app
;; Copyright (C) 2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.spec.reldb
  (:require
   [clojure.spec.alpha :as spec]
   [expound.alpha :as expound]
   [ovarit.app.fe.content.graphql-spec :as gspec]
   [ovarit.app.fe.content.loader :as loader]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.ui.forms.core :as forms]))

(defn valid? [spec data]
  (if-let [error (spec/explain-data spec data)]
    (do
      (log/error {:expound (expound/expound-str spec data)
                  :data data} "reldb spec error")
      (throw (ex-info "reldb spec error" error)))
    true))

;;; Specs for relational data

(spec/def ::iso-datetime
  #(re-matches #"\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d\.\d+Z" %))

(spec/def :reldb.Comment.content-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Comment/content-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Comment.content-history/content]))))
(spec/def :reldb.Comment/distinguish (spec/nilable #{:ADMIN :MOD}))
(spec/def :reldb.Comment/edited (spec/nilable ::iso-datetime))
(spec/def :reldb.Comment/status
  #{:ACTIVE :DELETED :DELETED_BY_ADMIN :DELETED_BY_MOD :DELETED_BY_USER})
(spec/def :reldb.Comment/time (spec/nilable ::iso-datetime))
(def CommentSchema
  [[:from :Comment]
   [:constrain
    [:req
     :cid
     :sticky
     :score
     :status
     :time
     :uid]
    [:check
     [spec/valid? [:_ :type.Comment/cid]              :cid]
     [spec/valid? [:_ :type.Comment/content]          :content]
     [spec/valid? [:_ :reldb.Comment/edited]          :edited]
     [spec/valid? [:_ :type.Comment/sticky]           :sticky]
     [spec/valid? [:_ :type.Comment/score]            :score]
     [spec/valid? [:_ :type.Comment/upvotes]          :upvotes]
     [spec/valid? [:_ :type.Comment/downvotes]        :downvotes]
     [spec/valid? [:_ :reldb.Comment/status]          :status]
     [spec/valid? [:_ :reldb.Comment/time]            :time]
     [spec/valid? [:_ :type.User/uid]                 :uid]
     [spec/valid? [:_ :type.Comment/author_flair]     :author-flair]
     [spec/valid? [:_ :reldb.Comment/distinguish]     :distinguish]
     [spec/valid? [:_ :type.Post/pid]                 :pid]
     [spec/valid? [:_ :reldb.Comment/content-history] :content-history]]]])

(spec/def :reldb.CommentNode/checked (spec/nilable boolean?))
(spec/def :reldb.CommentNode/children (spec/coll-of :type.Comment/cid))
(spec/def :reldb.CommentNode/level nat-int?)
(spec/def :reldb.CommentNode/loaded? boolean?)
(spec/def :reldb.CommentNode/ordering int?)
(spec/def :reldb.CommentNode/sort
  (spec/or :direct-link :type.Comment/cid
           :all-comments #{:BEST :TOP :NEW}))
(def CommentNodeSchema
  "Schema for nodes in the comment tree.
  Contains the things we need to know about both loaded and unloaded
  comments."
  [[:from :CommentNode]
   [:constrain
    [:req :cid :children :level :loaded? :ordering :pid :sort :status
     :time :uid]
    [:check
     [spec/valid? [:_ :type.Comment/cid]            :cid]
     [spec/valid? [:_ :type.Post/pid]               :pid]
     [spec/valid? [:_ :reldb.CommentNode/checked]   :checked]
     [spec/valid? [:_ :reldb.CommentNode/children]  :children]
     [spec/valid? [:_ :reldb.CommentNode/level]     :level]
     [spec/valid? [:_ :reldb.CommentNode/loaded?]   :loaded?]
     [spec/valid? [:_ :reldb.CommentNode/ordering]  :ordering]
     [spec/valid? [:_ :reldb.CommentNode/sort]      :sort]
     [spec/valid? [:_ :reldb.Comment/status]        :status]
     [spec/valid? [:_ ::iso-datetime]               :time]
     [spec/valid? [:_ :type.User/uid]               :uid]]]])

(def ContentStatusSchema
  [[:from :ContentStatus]
   [:constrain
    [:req
     :content-type
     :status]
    [:check [#(valid? ::loader/content-status %)]]]])

(def DownvoteNotificationSchema
  [[:from :DownvoteNotification]
   [:constrain
    [:req :mid :time :content :thread-id]
    [:check
     [spec/valid? [:_ :type.Message/mid]      :mid]
     [spec/valid? [:_ :type.Message/content]  :content]
     [spec/valid? [:_ ::iso-datetime]         :time]
     [spec/valid? [:_ :type.MessageThread/id] :thread-id]]]])

(spec/def :reldb.RequireNameChange/message string?)
(def RequireNameChangeSchema
  [[:from :RequireNameChange]
   [:constrain
    [:req :name :message :admin-name :required-at]
    [:check
     [spec/valid? [:_ :type.User/name]                  :name]
     [spec/valid? [:_ :type.User/name]                  :admin-name]
     [spec/valid? [:_ :reldb.RequireNameChange/message] :message]
     [spec/valid? [:_ ::iso-datetime]                   :required-at]]]])

(def FormStateSchema
  [[:from :FormState]
   [:constrain
    [:req
     :id
     :form-type]
    [:check [#(valid? ::forms/typed-form %)]]]])

(spec/def :reldb.Message/mtype #{:USER_TO_MODS
                                 :MOD_TO_USER_AS_USER
                                 :MOD_TO_USER_AS_MOD
                                 :MOD_DISCUSSION
                                 :USER_NOTIFICATION
                                 :MOD_NOTIFICATION})
(spec/def :reldb.Message/receiver
  (spec/nilable :type.User/uid))
(spec/def :reldb.Message/receiver-is-mod? boolean?)
(spec/def :reldb.Message/sender
  (spec/nilable :type.User/uid))
(spec/def :reldb.Message/sender-is-mod? boolean?)
(spec/def :reldb.Message/sent-with-name-hidden? boolean?)
(spec/def :reldb.Message/unread? boolean?)
(def MessageSchema
  [[:from :Message]
   [:constrain
    [:req :mid :unread? :content :mtype :time :thread-id
     :sent-with-name-hidden? :receiver-is-mod? :sender-is-mod?]
    [:check
     [spec/valid? [:_ :type.Message/mid]               :mid]
     [spec/valid? [:_ :reldb.Message/unread?]          :unread?]
     [spec/valid? [:_ :type.Message/content]           :content]
     [spec/valid? [:_ :reldb.Message/mtype]            :mtype]
     [spec/valid? [:_ :reldb.Message/receiver]         :receiver]
     [spec/valid? [:_ :reldb.Message/receiver-is-mod?] :receiver-is-mod?]
     [spec/valid? [:_ :reldb.Message/sender]           :sender]
     [spec/valid? [:_ :reldb.Message/sender-is-mod?]   :sender-is-mod?]
     [spec/valid?
      [:_ :reldb.Message/sent-with-name-hidden?]       :sent-with-name-hidden?]
     [spec/valid? [:_ :type.MessageThread/id]          :thread-id]
     [spec/valid? [:_ ::iso-datetime]                  :time]]]])

(spec/def :reldb.MessageThread/mailbox #{:INBOX :ARCHIVED})
(spec/def :reldb.MessageThread/reply-count nat-int?)
(spec/def :reldb.MessageThread/report-id
  (spec/nilable ::gspec/non-empty-string))
(spec/def :reldb.MessageThread/rtype
  (spec/nilable #{:POST :COMMENT}))
(def MessageThreadSchema
  [[:from :MessageThread]
   [:constrain
    [:req :thread-id :first-mid :subject :sid :mailbox]
    [:check
     [spec/valid? [:_ :type.MessageThread/id]           :thread-id]
     [spec/valid? [:_ :type.Message/mid]                :first-mid]
     [spec/valid? [:_ :type.MessageThread/subject]      :subject]
     [spec/valid? [:_ :type.Sub/sid]                    :sid]
     [spec/valid? [:_ :reldb.MessageThread/mailbox]     :mailbox]
     [spec/valid? [:_ :reldb.MessageThread/reply-count] :reply-count]
     [spec/valid? [:_ :reldb.MessageThread/report-id]   :report-id]
     [spec/valid? [:_ :reldb.MessageThread/rtype]       :rtype]]]])

(spec/def :reldb.PollOption/order nat-int?)
(def PollOptionSchema
  [[:from :PollOption]
   [:constrain
    [:req :option-id :pid :text :order]
    [:check
     [spec/valid? [:_ :type.PollOption/id]     :option-id]
     [spec/valid? [:_ :type.Post/pid]          :pid]
     [spec/valid? [:_ :type.PollOption/text]   :text]
     [spec/valid? [:_ :type.PollOption/votes]  :votes]
     [spec/valid? [:_ :reldb.PollOption/order] :order]]]])

(spec/def :reldb.Post.content-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Post/content-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.content-history/content]))))
(spec/def :reldb.Post/default-sort #{:BEST :NEW :TOP})
(spec/def :reldb.Post/distinguish (spec/nilable #{:MOD :ADMIN}))
(spec/def :reldb.Post/edited (spec/nilable ::iso-datetime))
(spec/def :reldb.Post/hide-results (spec/nilable boolean?))
(spec/def :reldb.Post/locked-update (spec/nilable boolean?))
(spec/def :reldb.Post.open-reports/id ::gspec/non-empty-string)
(spec/def :reldb.Post/open-reports
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.open-reports/id]))))
(spec/def :reldb.Post/poll-closes-time (spec/nilable ::iso-datetime))
(spec/def :reldb.Post/poll-open (spec/nilable boolean?))
(spec/def :reldb.Post.poll-options/id string?)
(spec/def :reldb.Post.poll-options/text string?)
(spec/def :reldb.Post.poll-options/votes (spec/nilable nat-int?))
(spec/def :reldb.Post/poll-options
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.poll-options/id
                        :reldb.Post.poll-options/text
                        :reldb.Post.poll-options/votes]))))
(spec/def :reldb.Post/poll-votes (spec/nilable nat-int?))
(spec/def :reldb.Post/posted ::iso-datetime)
(spec/def :reldb.Post/status #{:ACTIVE
                               :DELETED
                               :DELETED_BY_USER
                               :DELETED_BY_MOD
                               :DELETED_BY_ADMIN})
(spec/def :reldb.Post/status-update (spec/nilable :reldb.Post/status))
(spec/def :reldb.Post/title-edited (spec/nilable ::iso-datetime))
(spec/def :reldb.Post.title-history/content
  :type.ContentHistory/content)
(spec/def :reldb.Post/title-history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:reldb.Post.title-history/content]))))
(spec/def :reldb.Post/type #{:LINK :TEXT :POLL :UPLOAD})
(def PostSchema
  [[:from :Post]
   [:constrain
    [:req
     :pid
     :uid
     :status
     :nsfw
     :posted
     :is-archived
     :locked
     :sticky
     :type
     :score
     :comment-count
     :best-sort-enabled
     :default-sort]
    [:check
     [spec/valid? [:_ :type.Post/pid]               :pid]
     [spec/valid? [:_ :type.Post/content]           :content]
     [spec/valid? [:_ :reldb.Post/edited]           :edited]
     [spec/valid? [:_ :type.Post/link]              :link]
     [spec/valid? [:_ :type.Post/nsfw]              :nsfw]
     [spec/valid? [:_ :reldb.Post/posted]           :posted]
     [spec/valid? [:_ :type.Post/is_archived]       :is-archived]
     [spec/valid? [:_ :type.Post/locked]            :locked]
     [spec/valid? [:_ :reldb.Post/locked-update]    :locked-update]
     [spec/valid? [:_ :type.Post/sticky]            :sticky]
     [spec/valid? [:_ :reldb.Post/status]           :status]
     [spec/valid? [:_ :reldb.Post/status-update]    :status-update]
     [spec/valid? [:_ :reldb.Post/type]             :type]
     [spec/valid? [:_ :type.Post/score]             :score]
     [spec/valid? [:_ :type.Post/upvotes]           :upvotes]
     [spec/valid? [:_ :type.Post/downvotes]         :downvotes]
     [spec/valid? [:_ :type.Post/thumbnail]         :thumbnail]
     [spec/valid? [:_ :type.Post/title]             :title]
     [spec/valid? [:_ :type.Post/slug]              :slug]
     [spec/valid? [:_ :type.Post/flair]             :flair]
     [spec/valid? [:_ :reldb.Post/distinguish]      :distinguish]
     [spec/valid? [:_ :type.Post/comment_count]     :comment-count]
     [spec/valid? [:_ :type.Post/best_sort_enabled] :best-sort-enabled]
     [spec/valid? [:_ :reldb.Post/default-sort]     :default-sort]
     [spec/valid? [:_ :type.User/uid]               :uid]
     [spec/valid? [:_ :reldb.Post/content-history]  :content-history]
     [spec/valid? [:_ :reldb.Post/title-history]    :title-history]
     [spec/valid? [:_ :reldb.Post/open-reports]     :open-reports]
     [spec/valid? [:_ :reldb.Post/poll-options]     :poll-options]
     [spec/valid? [:_ :reldb.Post/poll-open]        :poll-open]
     [spec/valid? [:_ :reldb.Post/poll-closes-time] :poll-closes-time]
     [spec/valid? [:_ :reldb.Post/poll-votes]       :poll-votes]
     [spec/valid? [:_ :reldb.Post/hide-results]     :hide-results]]]])

(spec/def :reldb.PostUserAttributes/viewed (spec/nilable ::iso-datetime))
(spec/def :reldb.PostUserAttributes/vote (spec/nilable #{:UP :DOWN}))
(def PostUserAttributesSchema
  [[:from :PostUserAttributes]
   [:constrain
    [:req :pid :is-saved]
    [:check
     [spec/valid? [:_ :type.Post/pid]                     :pid]
     [spec/valid? [:_ :type.PostUserAttributes/is_saved]  :is-saved]
     [spec/valid? [:_ :type.PostUserAttributes/poll_vote] :poll-vote]
     [spec/valid? [:_ :reldb.PostUserAttributes/viewed]   :viewed]
     [spec/valid? [:_ :reldb.PostUserAttributes/vote]     :vote]]]])

(def SubSchema
  [[:from :Sub]
   [:constrain
    [:req
     :sid
     :creation
     #_:creator-uid
     :freeform-user-flairs
     :nsfw
     :post-count
     :restricted
     :sub-banned-users-private
     :sublog-private
     :subscriber-count
     :title
     :user-can-flair
     :user-can-flair-self
     :user-must-flair]
    [:check
     [spec/valid? [:_ :type.Sub/sid]                      :sid]
     [spec/valid? [:_ ::iso-datetime]                     :creation]
     #_[spec/valid? [:_ :type.User/uid] :creator-uid]
     [spec/valid? [:_ :type.Sub/freeform_user_flairs]     :freeform-user-flairs]
     [spec/valid? [:_ :type.Sub/nsfw]                     :nsfw]
     [spec/valid? [:_ :type.Sub/post_count]               :post-count]
     [spec/valid? [:_ :type.Sub/restricted]               :restricted]
     [spec/valid? [:_ :type.Sub/sidebar]                  :sidebar]
     [spec/valid? [:_ :type.Sub/sub_banned_users_private] :sub-banned-users-private]
     [spec/valid? [:_ :type.Sub/sublog_private]           :sublog-private]
     [spec/valid? [:_ :type.Sub/subscriber_count]         :subscriber-count]
     [spec/valid? [:_ :type.Sub/text_post_min_length]     :text-post-min-length]
     [spec/valid? [:_ :type.Sub/title]                    :title]
     [spec/valid? [:_ :type.Sub/user_can_flair]           :user-can-flair]
     [spec/valid? [:_ :type.Sub/user_can_flair_self]      :user-can-flair-self]
     [spec/valid? [:_ :type.Sub/user_flair_choices]       :user-flair-choices]
     [spec/valid? [:_ :type.Sub/user_must_flair]          :user-must-flair]]]])

(spec/def :reldb.SubMessageLog/action
  #{:CHANGE_MAILBOX :HIGHLIGHT :MOD_NOTIFICATION :RELATED_REPORT
    :RELATED_THREAD})
(spec/def :reldb.SubMessageLog/highlighted (spec/nilable boolean?))
(spec/def :reldb.SubMessageLog/mailbox
  (spec/nilable #{:INBOX :ARCHIVED}))
(spec/def :reldb.SubMessageLog/notification-type
  (spec/nilable #{:DOWNVOTE :REQUIRED_NAME_CHANGE :DM_BLOCKING_ABUSE}))
(spec/def :reldb.SubMessageLog/reference-type
  (spec/nilable #{:NOTIFICATION :NEW}))
(spec/def :reldb.SubMessageLog/report-id (spec/nilable string?))
(spec/def :reldb.SubMessageLog/rtype (spec/nilable #{:POST :COMMENT}))
(spec/def :reldb.SubMessageLog/related-thread-id (spec/nilable string?))
(def SubMessageLogSchema
  [[:from :SubMessageLog]
   [:constrain
    [:req :action :thread-id :time :uid]
    [:check
     [spec/valid? [:_ :reldb.SubMessageLog/action]            :action]
     [spec/valid? [:_ :type.MessageThread/id]                 :thread-id]
     [spec/valid? [:_ ::iso-datetime]                         :time]
     [spec/valid? [:_ :type.User/uid]                         :uid]
     [spec/valid? [:_ :reldb.SubMessageLog/highlighted]       :highlighted]
     [spec/valid? [:_ :reldb.SubMessageLog/mailbox]           :mailbox]
     [spec/valid? [:_ :reldb.SubMessageLog/notification-type] :notification-type]
     [spec/valid? [:_ :reldb.SubMessageLog/reference-type]    :reference-type]
     [spec/valid? [:_ :reldb.SubMessageLog/report-id]         :report-id]
     [spec/valid? [:_ :reldb.SubMessageLog/rtype]             :rtype]
     [spec/valid? [:_ :reldb.SubMessageLog/related-thread-id] :thread-id]]]])

(spec/def :reldb.SubMod/moderation-level #{:OWNER :MODERATOR :JANITOR})
(def SubModSchema
  [[:from :SubMod]
   [:constrain
    [:req :sid :uid :moderation-level]
    [:check
     [spec/valid? [:_ :type.Sub/sid]                  :sid]
     [spec/valid? [:_ :type.User/uid]                 :uid]
     [spec/valid? [:_ :reldb.SubMod/moderation-level] :moderation-level]]]])

(def SubModNotificationCountSchema
  [[:from :SubModNotificationCountSchema]
   [:constrain
    [:req
     :sid
     :open-report-count
     :unread-modmail-count
     :new-unread-modmail-count
     :in-progress-unread-modmail-count
     :all-unread-modmail-count
     :discussion-unread-modmail-count
     :notification-unread-modmail-count]
    [:check
     [spec/valid? [:_ :type.Sub/sid] :sid]
     [nat-int? :open-report-count]
     [nat-int? :unread-modmail-count]
     [nat-int? :new-unread-modmail-count]
     [nat-int? :in-progress-unread-modmail-count]
     [nat-int? :all-unread-modmail-count]
     [nat-int? :discussion-unread-modmail-count]
     [nat-int? :notification-unread-modmail-count]]]])

(def SubNameSchema
  [[:from :SubName]
   [:constrain
    [:req :sid :name]
    [:check
     [spec/valid? [:_ :type.Sub/sid]  :sid]
     [spec/valid? [:_ :type.Sub/name] :name]]]])

(spec/def :reldb.SubPostFlair/order nat-int?)
(spec/def :reldb.SubPostFlair/post-types
  (spec/and set? (spec/coll-of :reldb.Post/type)))
(def SubPostFlairSchema
  [[:from :SubPostFlair]
   [:constrain
    [:req :id :text :sid :mods-only :post-types :order]
    [:check
     [spec/valid? [:_ :type.SubPostFlair/id]          :id]
     [spec/valid? [:_ :type.SubPostFlair/text]        :text]
     [spec/valid? [:_ :type.SubPostFlair/mods_only]   :mods-only]
     [spec/valid? [:_ :reldb.SubPostFlair/post-types] :post-types]
     [spec/valid? [:_ :type.Sub/sid]                  :sid]
     [spec/valid? [:_ :reldb.SubPostFlair/order]      :order]]]])

(spec/def :reldb.SubPostFlairState/options-open? boolean?)
(def SubPostFlairStateSchema
  "UI state for post flairs on the mod edit post flairs page."
  [[:from :SubPostFlairState]
   [:constrain
    [:req :id :options-open?]
    [:check
     [spec/valid? [:_ :type.SubPostFlair/id] :id]
     [spec/valid? [:_ :reldb.SubPostFlairState/options-open?]
      :options-open?]]]])

(def SubPostTypeConfigSchema
  [[:from :SubPostTypeConfig]
   [:constrain
    [:req :name :post-type :mods-only]
    [:check
     [spec/valid? [:_ :type.Sub/name]                    :name]
     [spec/valid? [:_ :type.SubPostTypeConfig/mods_only] :mods-only]
     [spec/valid? [:_ :reldb.Post/type]                  :post-type]
     [spec/valid? [:_ :type.SubPostTypeConfig/rules]     :rules]]]])

(spec/def :reldb.SubRule/order nat-int?)
(def SubRuleSchema
  [[:from :SubRule]
   [:constrain
    [:req :id :text :sid :order]
    [:check
     [spec/valid? [:_ :type.SubRule/id]     :id]
     [spec/valid? [:_ :type.SubRule/text]   :text]
     [spec/valid? [:_ :type.Sub/sid]        :sid]
     [spec/valid? [:_ :reldb.SubRule/order] :order]]]])

(spec/def :reldb.SubUserFlair/text ::gspec/non-empty-string)
(def SubUserFlairSchema
  [[:from :SubUserFlair]
   [:constrain
    [:req :text :sid :uid]
    [:check
     [spec/valid? [:_ :type.Sub/sid]            :sid]
     [spec/valid? [:_ :type.User/uid]           :uid]
     [spec/valid? [:_ :reldb.SubUserFlair/text] :text]]]])

(spec/def :reldb.User/status #{:ACTIVE :BANNED :DELETED})
(def UserSchema
  [[:from :User]
   [:constrain
    [:req :uid :status]
    [:check
     [spec/valid? [:_ :type.User/uid]     :uid]
     [spec/valid? [:_ :type.User/name]    :name]
     [spec/valid? [:_ :reldb.User/status] :status]]]])

(spec/def :reldb.UserNameHistory/changed (spec/nilable nat-int?))
(spec/def :reldb.UserNameHistory/required-by-admin (spec/nilable boolean?))
(def UserNameHistorySchema
  [[:from :UserNameHistory]
   [:constrain
    [:req :uid :name]
    [:check
     [spec/valid? [:_ :type.User/uid]                 :uid]
     [spec/valid? [:_ :type.User/name]                :name]
     [spec/valid? [:_ :reldb.UserNameHistory/changed] :changed]
     [spec/valid? [:_ :reldb.UserNameHistory/required-by-admin]
      :required-by-admin]]]])

(def UserStatsSchema
  [[:from :UserStats]
   [:constrain
    [:req :uid :joindate :score :level :upvotes-given :downvotes-given]
    [:check
     [spec/valid? [:_ :type.User/uid]             :uid]
     [spec/valid? [:_ ::iso-datetime]             :joindate]
     [spec/valid? [:_ :type.User/score]           :score]
     [spec/valid? [:_ :type.User/level]           :level]
     [spec/valid? [:_ :type.User/upvotes_given]   :upvotes-given]
     [spec/valid? [:_ :type.User/downvotes_given] :downvotes-given]]]])

(spec/def :reldb.VoteSchema/cid
  (spec/nilable :type.Comment/cid))
(spec/def :reldb.VoteSchema/direction #{:UP :DOWN})
(spec/def :reldb.VoteSchema/post-slug :type.Post/slug)
(spec/def :reldb.VoteSchema/sub-name :type.Sub/name)
(def VoteSchema
  [[:from :Vote]
   [:constrain
    [:req :direction :datetime :pid :uid :sub-name]
    [:check
     [spec/valid? [:_ :reldb.VoteSchema/cid]       :cid]
     [spec/valid? [:_ :type.Post/pid]              :pid]
     [spec/valid? [:_ :type.User/uid]              :uid]
     [spec/valid? [:_ :reldb.VoteSchema/direction] :direction]
     [spec/valid? [:_ ::iso-datetime]              :datetime]]]])
