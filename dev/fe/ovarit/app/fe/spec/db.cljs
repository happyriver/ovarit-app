;; fe/spec/db.cljs -- Validate app-db spec for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.spec.db
  (:require
   [clojure.spec.alpha :as spec]
   [com.wotbrew.relic :as rel]
   [expound.alpha :as expound]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.content.graphql-spec]
   [ovarit.app.fe.content.spec]
   [ovarit.app.fe.db :as db]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.spec.graphql]
   [ovarit.app.fe.spec.reldb :as reldb]
   [re-frame.core :as re-frame]))

(defn validate
  "Returns nil if the db conforms to the spec, throws an exception otherwise"
  [event db]
  (when (or config/debug? config/testing?)
    (try
      (update db :reldb #(-> %
                             (rel/mat reldb/CommentSchema)
                             (rel/mat reldb/CommentNodeSchema)
                             (rel/mat reldb/ContentStatusSchema)
                             (rel/mat reldb/DownvoteNotificationSchema)
                             (rel/mat reldb/RequireNameChangeSchema)
                             (rel/mat reldb/FormStateSchema)
                             (rel/mat reldb/MessageSchema)
                             (rel/mat reldb/MessageThreadSchema)
                             (rel/mat reldb/PollOptionSchema)
                             (rel/mat reldb/PostSchema)
                             (rel/mat reldb/PostUserAttributesSchema)
                             (rel/mat reldb/SubSchema)
                             (rel/mat reldb/SubMessageLogSchema)
                             (rel/mat reldb/SubModSchema)
                             (rel/mat reldb/SubModNotificationCountSchema)
                             (rel/mat reldb/SubNameSchema)
                             (rel/mat reldb/SubPostFlairSchema)
                             (rel/mat reldb/SubPostFlairStateSchema)
                             (rel/mat reldb/SubPostTypeConfigSchema)
                             (rel/mat reldb/SubRuleSchema)
                             (rel/mat reldb/SubUserFlairSchema)
                             (rel/mat reldb/UserSchema)
                             (rel/mat reldb/UserNameHistorySchema)
                             (rel/mat reldb/UserStatsSchema)
                             (rel/mat reldb/VoteSchema)))
      (catch js/Error error
        (log/error {:reldb (:reldb db) :event event}
                   "reldb constraint failure")
        (throw (ex-info
                (str "reldb constraint validation failed after " event)
                error))))
    (when-let [error (spec/explain-data ::db/db db)]
      (log/error {:expound (try (expound/expound-str ::db/db db)
                                (catch js/Error e e))
                  :db db}
                 "DB spec error")
      (throw (ex-info
              (str "DB spec validation failed after " event)
              error)))))

(def validator
  "Interceptor to check the integrity of the db after every event."
  (re-frame.core/->interceptor
   :id :validate
   :before (fn [context]
             (log/trace {:event (get-in context [:coeffects :event])}
                        "before event")
             context)
   :after (fn [context]
            (log/trace {:event (get-in context [:coeffects :event])}
                       "after event")
            (when (contains? (:effects context) :db)
              (validate (first (get-in context [:coeffects :event]))
                        (get-in context [:effects :db])))
            context)))

(re-frame/reg-global-interceptor validator)
