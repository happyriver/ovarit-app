;; fe/spec/graphql.cljs -- Check specs for the graphql queries
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.spec.graphql
  (:require
   [clojure.spec.alpha :as spec]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [expound.alpha :as expound]
   [ovarit.app.fe.config :as config]
   [ovarit.app.fe.errors :as errors]
   [ovarit.app.fe.log :as log]
   [re-frame.core :as re-frame]))

(defn- spec-name [typ query-key]
  (keyword 'ovarit.app.fe.content.graphql-spec
           (str (name typ) "." (name query-key))))

(re-frame/reg-event-db ::check-graphql-spec
  ;; :doc Check the result of a graphql query or mutation against its spec.
  ;; :doc Does nothing if the query returns an error, assuming that will
  ;; :doc be handled elseware.
  (fn-traced [db [_ {:keys [type query-key response]}]]
    (if-not (or config/debug? config/testing?)
      db
      (let [{:keys [data errors]} response
            spec (spec-name type query-key)
            failure (when (and data (nil? errors))
                      (cond
                        (nil? (spec/get-spec spec))
                        "No spec defined for query"

                        (not (spec/valid? spec data))
                        "Query result does not match spec"))]
        (when failure
          (log/warn {:type type :query query-key :spec spec
                     :expound (when (spec/get-spec spec)
                                (expound/expound-str spec data))}
                    failure))
        (errors/assoc-errors db {:event query-key
                                 :errors (when failure [failure])
                                 :describe-error-fn (fn [msg] {:msg msg})})))))
