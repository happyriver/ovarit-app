;; fe/test_utils.cljs -- Shared testing utitilies for ovarit-app
;; Copyright (C) 2022-2023 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns ovarit.app.fe.test-utils
  (:require
   [cljs.test :as test]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [lambdaisland.glogi :as glogi]
   [lambdaisland.glogi.console :as glogi-console]
   [ovarit.app.fe.graphql :as graphql]
   [ovarit.app.fe.log :as log]
   [ovarit.app.fe.spec.db]
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph])
  (:require-macros [ovarit.app.fe.graphql :refer [inline-query-hash-table]])
  ;; These are the module roots which are loaded dynamically.
  ;; Require them here so they are included in the test build.
  (:require
   [ovarit.app.fe.routes.auth]
   [ovarit.app.fe.routes.moderation]
   [ovarit.app.fe.views.admin]
   [ovarit.app.fe.views.auth]
   [ovarit.app.fe.views.moderation]
   [ovarit.app.fe.views.modmail]))

(glogi-console/install!)

(defn init-logging
  "Set logging levels. Pass a namespaced keyword."
  ([kw] (init-logging kw {}))
  ([kw levels]
   (glogi/set-levels
    ;; Set a root logger level, this will be inherited by all loggers
    (merge {:glogi/root :warn
            (namespace kw) :info
            're-frame.core :error
            'ovarit.app.fe.routes :error} levels))))

(defn mute-error-logging [mute]
  (glogi/set-levels {'ovarit.app.fe.errors (if mute :shout :warn)}))

;; Stop re-frame from complaining about event handler redefs.
(re-frame.core/set-loggers!  {:warn (constantly nil)})

(defn test-name
  "Return the name(s) of the currently running tests."
  []
  (-> (test/testing-vars-str {})
      (str/split #"\(|\)")
      second))

(defn query-name-for-test
  "Given a query processed by `prepare-query`, return the name
  of the query.  Used by the tests."
  [hashed-query]
  (let [query-hash-table (inline-query-hash-table)
        query-name-regex #"\s*([a-z0-9]+) ?([a-z0-9_]*)"
        query-regex #"\s*(mutation|query|subscription) ([a-z0-9_]+)"
        [_ word1 word2] (re-find query-name-regex hashed-query)
        unhashed (first (s/select [s/MAP-VALS #(= (:hash %) word2)]
                                  query-hash-table))
        [_ _ unhashed-name] (when unhashed
                              (->> (:query unhashed)
                                   (re-find query-regex)))]
    (or unhashed-name (if (#{"query" "mutation"} word1)
                        word2 word1))))

(defn setup-regraph-subscribe-logging
  "Register a graphql subscription handler that just logs names."
  []
  (re-frame/reg-event-fx ::re-graph/subscribe
    (fn [_ [_ {:keys [query _]}]]
      (let [gql-query (query-name-for-test query)]
        (log/info {:subscription gql-query} "subscribe")))))
