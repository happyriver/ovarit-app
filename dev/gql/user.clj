;; user.clj -- Development support for ovarit-app
;; Copyright (C) 2020-2023  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns user
  (:require
   [cambium.codec :as codec]
   [cambium.core  :as log]
   [cambium.logback.json.flat-layout :as flat]
   [cheshire.core :as cheshire]
   [cider.nrepl.middleware]
   [clj-http.client :as client]
   [clojure.core :refer [require]]
   [clojure.pprint]
   [clojure.repl :refer [dir doc source]]
   [clojure.spec.alpha :as spec]
   [clojure.test :refer [run-tests]]
   [clojure.tools.namespace.repl :refer [refresh]]
   [com.stuartsierra.component :as component]
   [dotenv]
   [expound.alpha :as expound]
   [nrepl.server :as nrepl-server]
   [ns-tracker.core :as tracker]
   [ovarit.app.gql.elements.user.query :as user-query]
   [ovarit.app.gql.system :as system]
   [ovarit.app.gql.system.server.flask-compat :as compat]
   [ovarit.app.gql.protocols.image-manager :as image-manager]
   [clojure.java.io :as io]))

(alter-var-root #'spec/*explain-out* (constantly expound/printer))

(spec/check-asserts true)
(flat/set-decoder! codec/destringify-val)  ; configure backend with the codec
(comment (log/debug "user logging started"))
(comment require dir doc source run-tests) ; Suppress clj-kondo warnings

(defonce system (system/new-system #{:webserver
                                     :taskrunner
                                     :image-service}
                                   {:env :dev}))

(def server-host (or (dotenv/env :SERVER_HOST)
                     "localhost"))
(def server-url (format "http://%s:8888/" server-host))

(defn q
  "Execute a graphql query."
  ([query] (q query {}))
  ([query vars] (q query vars nil))
  ([query vars username]
   (let [secret-key (get-in system [:server :secret-key])
         csrf-token (compat/new-csrf-token)
         {:keys [uid resets]} (when username
                                (-> (:db system)
                                    (user-query/select-user-by-name
                                     {:names username
                                      :personal? true})))
         session {:_fresh true
                  :_id "session id"
                  :csrf_token csrf-token
                  :language "en"
                  :remember_me false
                  :user_id uid
                  :resets resets}
         session-cookie (str "session="
                             (compat/encode-session session secret-key))]
     (-> {:method :post
          :url (str server-url "graphql")
          :headers {"Content-Type" "application/json"
                    "Cookie" session-cookie}
          :body (cheshire/generate-string {:query query
                                           :variables vars})
          :throw-exceptions false}
         client/request
         (update :body #(try
                          (cheshire/parse-string % true)
                          (catch Exception _
                            %)))
         (select-keys [:status :body])))))

(defn start
  "Start all parts of the system."
  []
  (alter-var-root #'system (fn [_]
                             (-> (system/new-system #{:webserver
                                                      :taskrunner
                                                      :image-service}
                                                    {:env :dev})
                                 component/start-system)))
  (println "Server started at " server-url)
  :started)

(defn stop
  "Stop the system."
  []
  (when (some? system)
    (component/stop-system system)
    (alter-var-root #'system (constantly nil)))
  :stopped)

(defn reset
  "Stop the system, refresh the REPL, and start again.
  If this fails (because of a compilation error) use (refresh)
  then (reset)."
  []
  (stop)
  (refresh :after 'user/start))

(def taps
  "Save the last 20 things caught by the tap> function.  Evaluate
  @user/taps in any namespace."
  (atom '()))

(defn add-a-tap [x]
  (swap! taps (fn [values]
                (let [new-values (conj values x)]
                  (take 20 new-values)))))

(add-tap add-a-tap)

(comment
  @taps
  (reset! taps '()))

(defn print-threads [& {:keys [headers pre-fn]
                        :or {pre-fn identity}}]
  (let [thread-set (keys (Thread/getAllStackTraces))
        thread-data (mapv bean thread-set)
        headers (or headers (-> thread-data first keys))]
    (clojure.pprint/print-table headers (pre-fn thread-data))))

(defn print-threads-str [& args]
  (with-out-str (apply print-threads args)))


(comment
  ;; how many active threads are there
  (Thread/activeCount)

  ;;print all properties for all threads
  (print-threads)

  ;;print name,state,alive,daemon properties for all threads
  (print-threads :headers [:name :state :alive :daemon])

  ;;print name,state,alive,daemon properties for non-daemon threads
  (print-threads :headers [:name :state :alive :daemon] :pre-fn (partial filter #(false? (:daemon %))))

  ;;log name,state,alive,daemon properties for non-daemon threads
  (log/debug  (print-threads-str :headers [:name :state :alive :daemon] :pre-fn (partial filter #(false? (:daemon %)))))
  )

(def custom-nrepl-handler
  "A custom nrepl handler including refactor-nrepl middleware."
  (apply nrepl-server/default-handler
         (conj cider.nrepl.middleware/cider-middleware
               'refactor-nrepl.middleware/wrap-refactor)))

(defonce nrepl-server (nrepl-server/start-server
                       :bind "0.0.0.0"
                       :port 8778
                       :handler custom-nrepl-handler))

(defn restart
  []
  (stop)
  (start))

(defn- ns-reload [track]
  (try
    (doseq [ns-sym (track)]
      (log/debug {:ns-sym ns-sym} "Detected code change, reloading")
      (ns-sym :reload))
    (catch Throwable e (.printStackTrace e))))

(defn watch
  ([] (watch ["src"]))
  ([src-paths]
   (let [track (tracker/ns-tracker src-paths)
         done (atom false)]
     (doto
         (Thread. (fn []
                    (while (not @done)
                      (ns-reload track)
                      (Thread/sleep 500))))
       (.setDaemon true)
       (.start))
     (fn [] (swap! done not)))))

(defn -main [& _args]
  (log/debug {} "Starting code watcher")
  (start)
  (watch))
