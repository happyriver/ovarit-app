let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.jdk17 pkgs.clojure pkgs.clojure-lsp
    pkgs.clj-kondo pkgs.nodejs
  ];
}
