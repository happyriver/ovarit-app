--
-- PostgreSQL database dump
--

-- Dumped from database version 15.6 (Debian 15.6-1.pgdg120+2)
-- Dumped by pg_dump version 15.6 (Debian 15.6-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: proletarian; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA proletarian;


--
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

-- *not* creating schema, since initdb creates it


--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS '';


--
-- Name: hll; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS hll WITH SCHEMA public;


--
-- Name: EXTENSION hll; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION hll IS 'type for storing hyperloglog data';


--
-- Name: best_score(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.best_score(upvotes integer, downvotes integer, views integer) RETURNS real
    LANGUAGE sql IMMUTABLE
    AS $$
  with
  vars as (
    select greatest(views, 1)::real as n,
           (upvotes - downvotes + 1)::real as score,
           1.96 as z
  ),
  calc as (
    select least(abs(score), n) / n as phat
      from vars
  )
  select ((phat
          + z * z / (2 * n)
          - z * sqrt((phat * (1 - phat) + z * z / (4 * n)) / n))
          / (1 + z * z / n)
          * sign(score)) :: real
  from vars, calc
$$;


--
-- Name: hot(integer, double precision); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION public.hot(score integer, date double precision) RETURNS numeric
    LANGUAGE sql IMMUTABLE
    AS $_$
  select round(cast(log(greatest(abs($1), 1)) * sign($1) + ($2 - 1134028003) / 45000.0 AS numeric), 7)
$_$;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: archived_job; Type: TABLE; Schema: proletarian; Owner: -
--

CREATE TABLE proletarian.archived_job (
    job_id uuid NOT NULL,
    queue text NOT NULL,
    job_type text NOT NULL,
    payload text NOT NULL,
    attempts integer NOT NULL,
    enqueued_at timestamp without time zone NOT NULL,
    process_at timestamp without time zone NOT NULL,
    status text NOT NULL,
    finished_at timestamp without time zone NOT NULL
);


--
-- Name: job; Type: TABLE; Schema: proletarian; Owner: -
--

CREATE TABLE proletarian.job (
    job_id uuid NOT NULL,
    queue text NOT NULL,
    job_type text NOT NULL,
    payload text NOT NULL,
    attempts integer NOT NULL,
    enqueued_at timestamp without time zone NOT NULL,
    process_at timestamp without time zone NOT NULL
);


--
-- Name: api_token; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.api_token (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    token character varying(255) NOT NULL,
    can_post boolean NOT NULL,
    can_mod boolean NOT NULL,
    can_message boolean NOT NULL,
    can_upload boolean NOT NULL,
    is_active boolean DEFAULT true NOT NULL,
    is_ip_restricted boolean DEFAULT false NOT NULL
);


--
-- Name: api_token_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.api_token_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: api_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.api_token_id_seq OWNED BY public.api_token.id;


--
-- Name: api_token_settings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.api_token_settings (
    id integer NOT NULL,
    token_id integer NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL
);


--
-- Name: api_token_settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.api_token_settings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: api_token_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.api_token_settings_id_seq OWNED BY public.api_token_settings.id;


--
-- Name: badge; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.badge (
    bid integer NOT NULL,
    name character varying(34) NOT NULL,
    alt character varying(255) NOT NULL,
    icon character varying(255) NOT NULL,
    score integer NOT NULL,
    rank integer NOT NULL,
    trigger character varying(255)
);


--
-- Name: badge_bid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.badge_bid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: badge_bid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.badge_bid_seq OWNED BY public.badge.bid;


--
-- Name: comment_report_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.comment_report_log (
    lid integer NOT NULL,
    id integer NOT NULL,
    action integer,
    "desc" character varying(255),
    link character varying(255),
    "time" timestamp without time zone NOT NULL,
    uid character varying(40),
    target_uid character varying(40)
);


--
-- Name: comment_report_log_lid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.comment_report_log_lid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: comment_report_log_lid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.comment_report_log_lid_seq OWNED BY public.comment_report_log.lid;


--
-- Name: customer_update_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.customer_update_log (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    action text NOT NULL,
    value text,
    created timestamp without time zone NOT NULL,
    completed timestamp without time zone,
    success boolean
);


--
-- Name: customer_update_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.customer_update_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: customer_update_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.customer_update_log_id_seq OWNED BY public.customer_update_log.id;


--
-- Name: daily_uniques; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.daily_uniques (
    date date,
    ip_hashes public.hll
);


--
-- Name: disallow_flair_post_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.disallow_flair_post_type (
    id integer NOT NULL,
    flair_id integer NOT NULL,
    ptype integer NOT NULL
);


--
-- Name: disallow_flair_post_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.disallow_flair_post_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: disallow_flair_post_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.disallow_flair_post_type_id_seq OWNED BY public.disallow_flair_post_type.id;


--
-- Name: invite_code; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.invite_code (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    created timestamp without time zone NOT NULL,
    code character varying(64) NOT NULL,
    expires timestamp without time zone,
    uses integer NOT NULL,
    max_uses integer NOT NULL
);


--
-- Name: invite_code_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.invite_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: invite_code_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.invite_code_id_seq OWNED BY public.invite_code.id;


--
-- Name: message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.message (
    mid integer NOT NULL,
    content text,
    mtype integer,
    posted timestamp without time zone,
    receivedby character varying(40),
    sentby character varying(40),
    first boolean NOT NULL,
    mtid integer NOT NULL
);


--
-- Name: message_mid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.message_mid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_mid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.message_mid_seq OWNED BY public.message.mid;


--
-- Name: message_thread; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.message_thread (
    mtid integer NOT NULL,
    replies integer NOT NULL,
    subject character varying(255) NOT NULL,
    sid character varying(40)
);


--
-- Name: message_thread_mtid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.message_thread_mtid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: message_thread_mtid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.message_thread_mtid_seq OWNED BY public.message_thread.mtid;


--
-- Name: notification; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notification (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    sid character varying(40),
    pid integer,
    cid character varying(40),
    sentby character varying(40),
    receivedby character varying(40),
    read timestamp without time zone,
    content text,
    created timestamp without time zone NOT NULL
);


--
-- Name: notification_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notification_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notification_id_seq OWNED BY public.notification.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.payments (
    payment_id character varying(40) NOT NULL,
    uid character varying(40) NOT NULL,
    paid boolean NOT NULL,
    amount integer NOT NULL,
    date timestamp without time zone NOT NULL
);


--
-- Name: post_report_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.post_report_log (
    lid integer NOT NULL,
    id integer NOT NULL,
    action integer,
    "desc" character varying(255),
    link character varying(255),
    "time" timestamp without time zone NOT NULL,
    uid character varying(40),
    target_uid character varying(40)
);


--
-- Name: post_report_log_lid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.post_report_log_lid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_report_log_lid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.post_report_log_lid_seq OWNED BY public.post_report_log.lid;


--
-- Name: post_type_config; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.post_type_config (
    id integer NOT NULL,
    sid character varying(40) NOT NULL,
    ptype integer NOT NULL,
    rules text DEFAULT ''::text NOT NULL,
    mods_only boolean NOT NULL
);


--
-- Name: post_type_config_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.post_type_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: post_type_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.post_type_config_id_seq OWNED BY public.post_type_config.id;


--
-- Name: required_name_change; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.required_name_change (
    id integer NOT NULL,
    uid text NOT NULL,
    admin_uid text NOT NULL,
    message text NOT NULL,
    required_at timestamp without time zone NOT NULL,
    completed_at timestamp without time zone,
    notification_mtid integer
);


--
-- Name: required_name_change_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.required_name_change_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: required_name_change_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.required_name_change_id_seq OWNED BY public.required_name_change.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version character varying(128) NOT NULL
);


--
-- Name: site_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.site_log (
    lid integer NOT NULL,
    action integer,
    "desc" character varying(255),
    link character varying(255),
    "time" timestamp without time zone NOT NULL,
    uid character varying(40),
    target_uid character varying(40)
);


--
-- Name: site_log_lid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.site_log_lid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: site_log_lid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.site_log_lid_seq OWNED BY public.site_log.lid;


--
-- Name: site_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.site_metadata (
    xid integer NOT NULL,
    key character varying(255),
    value character varying(255)
);


--
-- Name: site_metadata_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.site_metadata_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: site_metadata_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.site_metadata_xid_seq OWNED BY public.site_metadata.xid;


--
-- Name: sub; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub (
    sid character varying(40) NOT NULL,
    name character varying(32) NOT NULL,
    nsfw boolean DEFAULT false NOT NULL,
    sidebar text DEFAULT ''::text NOT NULL,
    status integer,
    title character varying(50),
    sort character varying(32),
    creation timestamp without time zone NOT NULL,
    subscribers integer DEFAULT 1 NOT NULL,
    posts integer DEFAULT 0 NOT NULL
);


--
-- Name: sub_ban; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_ban (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    sid character varying(40) NOT NULL,
    created timestamp without time zone,
    reason character varying(128) NOT NULL,
    expires timestamp without time zone,
    effective boolean NOT NULL,
    created_by_id character varying(40)
);


--
-- Name: sub_ban_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_ban_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_ban_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_ban_id_seq OWNED BY public.sub_ban.id;


--
-- Name: sub_flair; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_flair (
    xid integer NOT NULL,
    sid character varying(40),
    text character varying(255),
    "order" integer,
    mods_only boolean NOT NULL
);


--
-- Name: sub_flair_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_flair_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_flair_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_flair_xid_seq OWNED BY public.sub_flair.xid;


--
-- Name: sub_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_log (
    lid integer NOT NULL,
    action integer,
    "desc" character varying(255),
    link character varying(255),
    sid character varying(40),
    uid character varying(40),
    target_uid character varying(40),
    admin boolean DEFAULT false NOT NULL,
    "time" timestamp without time zone NOT NULL
);


--
-- Name: sub_log_lid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_log_lid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_log_lid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_log_lid_seq OWNED BY public.sub_log.lid;


--
-- Name: sub_message_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_message_log (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    updated timestamp without time zone,
    action integer NOT NULL,
    "desc" character varying(255),
    mtid integer
);


--
-- Name: sub_message_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_message_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_message_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_message_log_id_seq OWNED BY public.sub_message_log.id;


--
-- Name: sub_message_mailbox; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_message_mailbox (
    id integer NOT NULL,
    mailbox integer NOT NULL,
    highlighted boolean NOT NULL,
    mtid integer NOT NULL
);


--
-- Name: sub_message_mailbox_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_message_mailbox_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_message_mailbox_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_message_mailbox_id_seq OWNED BY public.sub_message_mailbox.id;


--
-- Name: sub_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_metadata (
    xid integer NOT NULL,
    key character varying(255),
    sid character varying(40),
    value character varying(255)
);


--
-- Name: sub_metadata_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_metadata_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_metadata_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_metadata_xid_seq OWNED BY public.sub_metadata.xid;


--
-- Name: sub_mod; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_mod (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    sid character varying(40) NOT NULL,
    power_level integer NOT NULL,
    invite boolean NOT NULL
);


--
-- Name: sub_mod_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_mod_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_mod_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_mod_id_seq OWNED BY public.sub_mod.id;


--
-- Name: sub_post; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post (
    pid integer NOT NULL,
    content text,
    deleted integer NOT NULL,
    link character varying(255),
    nsfw boolean,
    posted timestamp without time zone NOT NULL,
    edited timestamp without time zone,
    ptype integer,
    score integer NOT NULL,
    upvotes integer DEFAULT 0 NOT NULL,
    downvotes integer DEFAULT 0 NOT NULL,
    sid character varying(40) NOT NULL,
    thumbnail character varying(255),
    title character varying(255) NOT NULL,
    comments integer NOT NULL,
    uid character varying(40) NOT NULL,
    flair character varying(25),
    distinguish integer,
    slug character varying(255) NOT NULL
);


--
-- Name: sub_post_comment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment (
    cid character varying(40) NOT NULL,
    content text,
    lastedit timestamp without time zone,
    parentcid character varying(40),
    pid integer,
    score integer,
    upvotes integer DEFAULT 0 NOT NULL,
    downvotes integer DEFAULT 0 NOT NULL,
    status integer,
    "time" timestamp without time zone,
    uid character varying(40),
    distinguish integer,
    views integer NOT NULL
);


--
-- Name: sub_post_comment_checkoff; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment_checkoff (
    id integer NOT NULL,
    cid character varying(40) NOT NULL,
    uid character varying(40) NOT NULL,
    datetime timestamp without time zone NOT NULL
);


--
-- Name: sub_post_comment_checkoff_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_comment_checkoff_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_comment_checkoff_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_comment_checkoff_id_seq OWNED BY public.sub_post_comment_checkoff.id;


--
-- Name: sub_post_comment_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment_history (
    id integer NOT NULL,
    cid character varying(40) NOT NULL,
    datetime timestamp without time zone NOT NULL,
    content text
);


--
-- Name: sub_post_comment_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_comment_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_comment_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_comment_history_id_seq OWNED BY public.sub_post_comment_history.id;


--
-- Name: sub_post_comment_report; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment_report (
    id integer NOT NULL,
    cid character varying(40) NOT NULL,
    uid character varying(40) NOT NULL,
    datetime timestamp without time zone NOT NULL,
    reason character varying(128) NOT NULL,
    open boolean NOT NULL,
    send_to_admin boolean NOT NULL
);


--
-- Name: sub_post_comment_report_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_comment_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_comment_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_comment_report_id_seq OWNED BY public.sub_post_comment_report.id;


--
-- Name: sub_post_comment_view; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment_view (
    id integer NOT NULL,
    cid character varying(40) NOT NULL,
    uid character varying(40) NOT NULL,
    pid integer NOT NULL
);


--
-- Name: sub_post_comment_view_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_comment_view_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_comment_view_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_comment_view_id_seq OWNED BY public.sub_post_comment_view.id;


--
-- Name: sub_post_comment_vote; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_comment_vote (
    xid integer NOT NULL,
    datetime timestamp without time zone,
    cid character varying(255),
    positive integer,
    uid character varying(40)
);


--
-- Name: sub_post_comment_vote_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_comment_vote_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_comment_vote_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_comment_vote_xid_seq OWNED BY public.sub_post_comment_vote.xid;


--
-- Name: sub_post_content_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_content_history (
    id integer NOT NULL,
    pid integer,
    content text,
    datetime timestamp without time zone NOT NULL
);


--
-- Name: sub_post_content_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_content_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_content_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_content_history_id_seq OWNED BY public.sub_post_content_history.id;


--
-- Name: sub_post_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_metadata (
    xid integer NOT NULL,
    key character varying(255),
    pid integer,
    value character varying(255)
);


--
-- Name: sub_post_metadata_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_metadata_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_metadata_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_metadata_xid_seq OWNED BY public.sub_post_metadata.xid;


--
-- Name: sub_post_pid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_pid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_pid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_pid_seq OWNED BY public.sub_post.pid;


--
-- Name: sub_post_poll_option; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_poll_option (
    id integer NOT NULL,
    pid integer NOT NULL,
    text character varying(255) NOT NULL
);


--
-- Name: sub_post_poll_option_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_poll_option_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_poll_option_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_poll_option_id_seq OWNED BY public.sub_post_poll_option.id;


--
-- Name: sub_post_poll_vote; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_poll_vote (
    id integer NOT NULL,
    pid integer NOT NULL,
    uid character varying(40) NOT NULL,
    vid integer NOT NULL
);


--
-- Name: sub_post_poll_vote_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_poll_vote_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_poll_vote_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_poll_vote_id_seq OWNED BY public.sub_post_poll_vote.id;


--
-- Name: sub_post_report; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_report (
    id integer NOT NULL,
    pid integer NOT NULL,
    uid character varying(40) NOT NULL,
    datetime timestamp without time zone NOT NULL,
    reason character varying(128) NOT NULL,
    open boolean NOT NULL,
    send_to_admin boolean NOT NULL
);


--
-- Name: sub_post_report_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_report_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_report_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_report_id_seq OWNED BY public.sub_post_report.id;


--
-- Name: sub_post_title_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_title_history (
    id integer NOT NULL,
    pid integer,
    title text,
    datetime timestamp without time zone NOT NULL,
    uid character varying(40) NOT NULL
);


--
-- Name: sub_post_title_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_title_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_title_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_title_history_id_seq OWNED BY public.sub_post_title_history.id;


--
-- Name: sub_post_view; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_view (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    pid integer NOT NULL,
    datetime timestamp without time zone
);


--
-- Name: sub_post_view_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_view_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_view_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_view_id_seq OWNED BY public.sub_post_view.id;


--
-- Name: sub_post_vote; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_post_vote (
    xid integer NOT NULL,
    datetime timestamp without time zone,
    pid integer,
    positive integer,
    uid character varying(40)
);


--
-- Name: sub_post_vote_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_post_vote_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_post_vote_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_post_vote_xid_seq OWNED BY public.sub_post_vote.xid;


--
-- Name: sub_rule; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_rule (
    rid integer NOT NULL,
    sid character varying(40),
    text character varying(255),
    "order" integer
);


--
-- Name: sub_rule_rid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_rule_rid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_rule_rid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_rule_rid_seq OWNED BY public.sub_rule.rid;


--
-- Name: sub_subscriber; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_subscriber (
    xid integer NOT NULL,
    "order" integer,
    sid character varying(40),
    status integer,
    "time" timestamp without time zone NOT NULL,
    uid character varying(40)
);


--
-- Name: sub_subscriber_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_subscriber_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_subscriber_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_subscriber_xid_seq OWNED BY public.sub_subscriber.xid;


--
-- Name: sub_uploads; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_uploads (
    id integer NOT NULL,
    sid character varying(40) NOT NULL,
    fileid character varying(255) NOT NULL,
    thumbnail character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    size integer NOT NULL
);


--
-- Name: sub_uploads_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_uploads_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_uploads_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_uploads_id_seq OWNED BY public.sub_uploads.id;


--
-- Name: sub_user_flair; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_user_flair (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    sid character varying(40) NOT NULL,
    flair character varying(25) NOT NULL,
    flair_choice_id integer
);


--
-- Name: sub_user_flair_choice; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sub_user_flair_choice (
    id integer NOT NULL,
    sid character varying(40) NOT NULL,
    flair character varying(25) NOT NULL
);


--
-- Name: sub_user_flair_choice_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_user_flair_choice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_user_flair_choice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_user_flair_choice_id_seq OWNED BY public.sub_user_flair_choice.id;


--
-- Name: sub_user_flair_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sub_user_flair_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sub_user_flair_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sub_user_flair_id_seq OWNED BY public.sub_user_flair.id;


--
-- Name: url_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.url_metadata (
    id integer NOT NULL,
    log_id integer NOT NULL,
    key text NOT NULL,
    value text NOT NULL
);


--
-- Name: url_metadata_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.url_metadata_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: url_metadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.url_metadata_id_seq OWNED BY public.url_metadata.id;


--
-- Name: url_metadata_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.url_metadata_log (
    id integer NOT NULL,
    url text NOT NULL,
    fetched timestamp without time zone NOT NULL,
    error text
);


--
-- Name: url_metadata_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.url_metadata_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: url_metadata_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.url_metadata_log_id_seq OWNED BY public.url_metadata_log.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    uid character varying(40) NOT NULL,
    joindate timestamp without time zone,
    name character varying(64),
    score integer DEFAULT 0 NOT NULL,
    given integer DEFAULT 0 NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    resets integer DEFAULT 0 NOT NULL,
    language character varying(11),
    upvotes_given integer,
    downvotes_given integer
);


--
-- Name: user_content_block; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_content_block (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    target character varying(40) NOT NULL,
    date timestamp without time zone NOT NULL,
    method integer NOT NULL
);


--
-- Name: user_content_block_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_content_block_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_content_block_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_content_block_id_seq OWNED BY public.user_content_block.id;


--
-- Name: user_login; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_login (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    email character varying(255) NOT NULL,
    parsed_hash character varying(255) NOT NULL,
    pending_action jsonb
);


--
-- Name: user_login_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_login_id_seq OWNED BY public.user_login.id;


--
-- Name: user_message_block; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_message_block (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    target character varying(40) NOT NULL,
    date timestamp without time zone NOT NULL
);


--
-- Name: user_message_block_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_message_block_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_message_block_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_message_block_id_seq OWNED BY public.user_message_block.id;


--
-- Name: user_message_mailbox; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_message_mailbox (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    mid integer NOT NULL,
    mailbox integer NOT NULL
);


--
-- Name: user_message_mailbox_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_message_mailbox_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_message_mailbox_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_message_mailbox_id_seq OWNED BY public.user_message_mailbox.id;


--
-- Name: user_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_metadata (
    xid integer NOT NULL,
    key character varying(255),
    uid character varying(40),
    value character varying(255)
);


--
-- Name: user_metadata_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_metadata_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_metadata_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_metadata_xid_seq OWNED BY public.user_metadata.xid;


--
-- Name: user_saved; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_saved (
    xid integer NOT NULL,
    pid integer,
    uid character varying(40)
);


--
-- Name: user_saved_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_saved_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_saved_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_saved_xid_seq OWNED BY public.user_saved.xid;


--
-- Name: user_unread_message; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_unread_message (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    mid integer NOT NULL
);


--
-- Name: user_unread_message_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_unread_message_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_unread_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_unread_message_id_seq OWNED BY public.user_unread_message.id;


--
-- Name: user_uploads; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_uploads (
    xid integer NOT NULL,
    pid integer,
    uid character varying(40),
    fileid character varying(255),
    thumbnail character varying(255),
    status integer NOT NULL
);


--
-- Name: user_uploads_xid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_uploads_xid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_uploads_xid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_uploads_xid_seq OWNED BY public.user_uploads.xid;


--
-- Name: username_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.username_history (
    id integer NOT NULL,
    uid character varying(40) NOT NULL,
    name text,
    changed timestamp without time zone,
    required_id integer
);


--
-- Name: username_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.username_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: username_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.username_history_id_seq OWNED BY public.username_history.id;


--
-- Name: visit; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.visit (
    id bigint NOT NULL,
    datetime timestamp with time zone DEFAULT now() NOT NULL,
    ipaddr text NOT NULL
);


--
-- Name: visit_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.visit_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: visit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.visit_id_seq OWNED BY public.visit.id;


--
-- Name: wiki; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.wiki (
    id integer NOT NULL,
    is_global boolean NOT NULL,
    sid character varying(40),
    slug character varying(128) NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    created timestamp without time zone NOT NULL,
    updated timestamp without time zone NOT NULL
);


--
-- Name: wiki_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.wiki_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: wiki_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.wiki_id_seq OWNED BY public.wiki.id;


--
-- Name: api_token id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token ALTER COLUMN id SET DEFAULT nextval('public.api_token_id_seq'::regclass);


--
-- Name: api_token_settings id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token_settings ALTER COLUMN id SET DEFAULT nextval('public.api_token_settings_id_seq'::regclass);


--
-- Name: badge bid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.badge ALTER COLUMN bid SET DEFAULT nextval('public.badge_bid_seq'::regclass);


--
-- Name: comment_report_log lid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment_report_log ALTER COLUMN lid SET DEFAULT nextval('public.comment_report_log_lid_seq'::regclass);


--
-- Name: customer_update_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_update_log ALTER COLUMN id SET DEFAULT nextval('public.customer_update_log_id_seq'::regclass);


--
-- Name: disallow_flair_post_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.disallow_flair_post_type ALTER COLUMN id SET DEFAULT nextval('public.disallow_flair_post_type_id_seq'::regclass);


--
-- Name: invite_code id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invite_code ALTER COLUMN id SET DEFAULT nextval('public.invite_code_id_seq'::regclass);


--
-- Name: message mid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message ALTER COLUMN mid SET DEFAULT nextval('public.message_mid_seq'::regclass);


--
-- Name: message_thread mtid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message_thread ALTER COLUMN mtid SET DEFAULT nextval('public.message_thread_mtid_seq'::regclass);


--
-- Name: notification id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification ALTER COLUMN id SET DEFAULT nextval('public.notification_id_seq'::regclass);


--
-- Name: post_report_log lid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_report_log ALTER COLUMN lid SET DEFAULT nextval('public.post_report_log_lid_seq'::regclass);


--
-- Name: post_type_config id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_type_config ALTER COLUMN id SET DEFAULT nextval('public.post_type_config_id_seq'::regclass);


--
-- Name: required_name_change id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.required_name_change ALTER COLUMN id SET DEFAULT nextval('public.required_name_change_id_seq'::regclass);


--
-- Name: site_log lid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_log ALTER COLUMN lid SET DEFAULT nextval('public.site_log_lid_seq'::regclass);


--
-- Name: site_metadata xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_metadata ALTER COLUMN xid SET DEFAULT nextval('public.site_metadata_xid_seq'::regclass);


--
-- Name: sub_ban id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_ban ALTER COLUMN id SET DEFAULT nextval('public.sub_ban_id_seq'::regclass);


--
-- Name: sub_flair xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_flair ALTER COLUMN xid SET DEFAULT nextval('public.sub_flair_xid_seq'::regclass);


--
-- Name: sub_log lid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_log ALTER COLUMN lid SET DEFAULT nextval('public.sub_log_lid_seq'::regclass);


--
-- Name: sub_message_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_log ALTER COLUMN id SET DEFAULT nextval('public.sub_message_log_id_seq'::regclass);


--
-- Name: sub_message_mailbox id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_mailbox ALTER COLUMN id SET DEFAULT nextval('public.sub_message_mailbox_id_seq'::regclass);


--
-- Name: sub_metadata xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_metadata ALTER COLUMN xid SET DEFAULT nextval('public.sub_metadata_xid_seq'::regclass);


--
-- Name: sub_mod id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_mod ALTER COLUMN id SET DEFAULT nextval('public.sub_mod_id_seq'::regclass);


--
-- Name: sub_post pid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post ALTER COLUMN pid SET DEFAULT nextval('public.sub_post_pid_seq'::regclass);


--
-- Name: sub_post_comment_checkoff id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_checkoff ALTER COLUMN id SET DEFAULT nextval('public.sub_post_comment_checkoff_id_seq'::regclass);


--
-- Name: sub_post_comment_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_history ALTER COLUMN id SET DEFAULT nextval('public.sub_post_comment_history_id_seq'::regclass);


--
-- Name: sub_post_comment_report id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_report ALTER COLUMN id SET DEFAULT nextval('public.sub_post_comment_report_id_seq'::regclass);


--
-- Name: sub_post_comment_view id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_view ALTER COLUMN id SET DEFAULT nextval('public.sub_post_comment_view_id_seq'::regclass);


--
-- Name: sub_post_comment_vote xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_vote ALTER COLUMN xid SET DEFAULT nextval('public.sub_post_comment_vote_xid_seq'::regclass);


--
-- Name: sub_post_content_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_content_history ALTER COLUMN id SET DEFAULT nextval('public.sub_post_content_history_id_seq'::regclass);


--
-- Name: sub_post_metadata xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_metadata ALTER COLUMN xid SET DEFAULT nextval('public.sub_post_metadata_xid_seq'::regclass);


--
-- Name: sub_post_poll_option id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_option ALTER COLUMN id SET DEFAULT nextval('public.sub_post_poll_option_id_seq'::regclass);


--
-- Name: sub_post_poll_vote id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_vote ALTER COLUMN id SET DEFAULT nextval('public.sub_post_poll_vote_id_seq'::regclass);


--
-- Name: sub_post_report id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_report ALTER COLUMN id SET DEFAULT nextval('public.sub_post_report_id_seq'::regclass);


--
-- Name: sub_post_title_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_title_history ALTER COLUMN id SET DEFAULT nextval('public.sub_post_title_history_id_seq'::regclass);


--
-- Name: sub_post_view id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_view ALTER COLUMN id SET DEFAULT nextval('public.sub_post_view_id_seq'::regclass);


--
-- Name: sub_post_vote xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_vote ALTER COLUMN xid SET DEFAULT nextval('public.sub_post_vote_xid_seq'::regclass);


--
-- Name: sub_rule rid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_rule ALTER COLUMN rid SET DEFAULT nextval('public.sub_rule_rid_seq'::regclass);


--
-- Name: sub_subscriber xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_subscriber ALTER COLUMN xid SET DEFAULT nextval('public.sub_subscriber_xid_seq'::regclass);


--
-- Name: sub_uploads id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_uploads ALTER COLUMN id SET DEFAULT nextval('public.sub_uploads_id_seq'::regclass);


--
-- Name: sub_user_flair id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair ALTER COLUMN id SET DEFAULT nextval('public.sub_user_flair_id_seq'::regclass);


--
-- Name: sub_user_flair_choice id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair_choice ALTER COLUMN id SET DEFAULT nextval('public.sub_user_flair_choice_id_seq'::regclass);


--
-- Name: url_metadata id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.url_metadata ALTER COLUMN id SET DEFAULT nextval('public.url_metadata_id_seq'::regclass);


--
-- Name: url_metadata_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.url_metadata_log ALTER COLUMN id SET DEFAULT nextval('public.url_metadata_log_id_seq'::regclass);


--
-- Name: user_content_block id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_content_block ALTER COLUMN id SET DEFAULT nextval('public.user_content_block_id_seq'::regclass);


--
-- Name: user_login id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_login ALTER COLUMN id SET DEFAULT nextval('public.user_login_id_seq'::regclass);


--
-- Name: user_message_block id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_block ALTER COLUMN id SET DEFAULT nextval('public.user_message_block_id_seq'::regclass);


--
-- Name: user_message_mailbox id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_mailbox ALTER COLUMN id SET DEFAULT nextval('public.user_message_mailbox_id_seq'::regclass);


--
-- Name: user_metadata xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_metadata ALTER COLUMN xid SET DEFAULT nextval('public.user_metadata_xid_seq'::regclass);


--
-- Name: user_saved xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_saved ALTER COLUMN xid SET DEFAULT nextval('public.user_saved_xid_seq'::regclass);


--
-- Name: user_unread_message id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_unread_message ALTER COLUMN id SET DEFAULT nextval('public.user_unread_message_id_seq'::regclass);


--
-- Name: user_uploads xid; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_uploads ALTER COLUMN xid SET DEFAULT nextval('public.user_uploads_xid_seq'::regclass);


--
-- Name: username_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.username_history ALTER COLUMN id SET DEFAULT nextval('public.username_history_id_seq'::regclass);


--
-- Name: visit id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visit ALTER COLUMN id SET DEFAULT nextval('public.visit_id_seq'::regclass);


--
-- Name: wiki id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki ALTER COLUMN id SET DEFAULT nextval('public.wiki_id_seq'::regclass);


--
-- Data for Name: archived_job; Type: TABLE DATA; Schema: proletarian; Owner: -
--



--
-- Data for Name: job; Type: TABLE DATA; Schema: proletarian; Owner: -
--



--
-- Data for Name: api_token; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: api_token_settings; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: badge; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: comment_report_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: customer_update_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: daily_uniques; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: disallow_flair_post_type; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: invite_code; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: message_thread; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: notification; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: post_report_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: post_type_config; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: required_name_change; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.schema_migrations (version) VALUES ('20230613191133');
INSERT INTO public.schema_migrations (version) VALUES ('20230614002754');
INSERT INTO public.schema_migrations (version) VALUES ('20230614021938');
INSERT INTO public.schema_migrations (version) VALUES ('20230808143208');
INSERT INTO public.schema_migrations (version) VALUES ('20230914185449');
INSERT INTO public.schema_migrations (version) VALUES ('20230923200328');
INSERT INTO public.schema_migrations (version) VALUES ('20230925220622');
INSERT INTO public.schema_migrations (version) VALUES ('20231002145806');
INSERT INTO public.schema_migrations (version) VALUES ('20231002151707');
INSERT INTO public.schema_migrations (version) VALUES ('20231005144625');
INSERT INTO public.schema_migrations (version) VALUES ('20231012185704');
INSERT INTO public.schema_migrations (version) VALUES ('20231013031012');
INSERT INTO public.schema_migrations (version) VALUES ('20231104213203');
INSERT INTO public.schema_migrations (version) VALUES ('20231202164644');
INSERT INTO public.schema_migrations (version) VALUES ('20240104160217');
INSERT INTO public.schema_migrations (version) VALUES ('20240326165155');


--
-- Data for Name: site_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: site_metadata; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.site_metadata (xid, key, value) VALUES (1, 'site.name', 'Throat');
INSERT INTO public.site_metadata (xid, key, value) VALUES (2, 'site.lema', 'Throat: Open discussion ;D');
INSERT INTO public.site_metadata (xid, key, value) VALUES (3, 'site.copyright', 'Umbrella Corp');
INSERT INTO public.site_metadata (xid, key, value) VALUES (4, 'site.placeholder_account', 'Site');
INSERT INTO public.site_metadata (xid, key, value) VALUES (5, 'site.allow_uploads', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (6, 'site.allow_video_uploads', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (7, 'site.upload_min_level', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (8, 'site.enable_chat', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (9, 'site.sitelog_public', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (10, 'site.force_sublog_public', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (11, 'site.front_page_submit', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (12, 'site.block_anon_stalking', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (13, 'site.changelog_sub', '');
INSERT INTO public.site_metadata (xid, key, value) VALUES (14, 'site.title_edit_timeout', '300');
INSERT INTO public.site_metadata (xid, key, value) VALUES (15, 'site.sub_creation_min_level', '2');
INSERT INTO public.site_metadata (xid, key, value) VALUES (16, 'site.sub_creation_admin_only', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (17, 'site.sub_ownership_limit', '20');
INSERT INTO public.site_metadata (xid, key, value) VALUES (18, 'site.edit_history', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (19, 'site.anonymous_modding', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (20, 'site.send_pm_to_user_min_level', '3');
INSERT INTO public.site_metadata (xid, key, value) VALUES (21, 'site.daily_sub_posting_limit', '10');
INSERT INTO public.site_metadata (xid, key, value) VALUES (22, 'site.daily_site_posting_limit', '25');
INSERT INTO public.site_metadata (xid, key, value) VALUES (23, 'site.archive_post_after', '60');
INSERT INTO public.site_metadata (xid, key, value) VALUES (24, 'site.recent_activity.enabled', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (25, 'site.recent_activity.defaults_only', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (26, 'site.recent_activity.comments_only', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (27, 'site.recent_activity.max_entries', '10');
INSERT INTO public.site_metadata (xid, key, value) VALUES (29, 'site.enable_posting', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (30, 'site.enable_registration', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (31, 'site.invitations_visible_to_users', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (32, 'site.invite_level', '3');
INSERT INTO public.site_metadata (xid, key, value) VALUES (33, 'site.invite_max', '10');
INSERT INTO public.site_metadata (xid, key, value) VALUES (34, 'site.require_invite_code', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (35, 'site.notifications_on_icon', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (36, 'site.archive_sticky_posts', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (37, 'site.nsfw.anon.show', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (38, 'site.nsfw.anon.blur', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (39, 'site.nsfw.new_user_default.show', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (40, 'site.nsfw.new_user_default.blur', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (41, 'site.username_max_length', '32');
INSERT INTO public.site_metadata (xid, key, value) VALUES (42, 'site.enable_modmail', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (43, 'site.show_votes', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (44, 'site.top_posts.show_score', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (45, 'site.self_voting.posts', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (46, 'site.self_voting.comments', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (47, 'site.description', 'A link aggregator and discussion platform');
INSERT INTO public.site_metadata (xid, key, value) VALUES (48, 'sub_post_view_init', '2024-03-03 14:38:10.61001+00');
INSERT INTO public.site_metadata (xid, key, value) VALUES (49, 'best_comment_sort_init', '2024-03-03 14:38:10.61001+00');
INSERT INTO public.site_metadata (xid, key, value) VALUES (50, 'site.username_change.limit', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (51, 'site.username_change.limit_days', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (52, 'site.username_change.display_days', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (53, 'site.downvotes.personal.count', '10');
INSERT INTO public.site_metadata (xid, key, value) VALUES (54, 'site.downvotes.personal.days', '7');
INSERT INTO public.site_metadata (xid, key, value) VALUES (55, 'site.downvotes.count', '100');
INSERT INTO public.site_metadata (xid, key, value) VALUES (56, 'site.downvotes.days', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (57, 'site.downvotes.silence', '1');
INSERT INTO public.site_metadata (xid, key, value) VALUES (58, 'site.downvotes.thread.percent', '50');
INSERT INTO public.site_metadata (xid, key, value) VALUES (59, 'site.downvotes.thread.comments', '10');
INSERT INTO public.site_metadata (xid, key, value) VALUES (60, 'site.text_post.min_length', '0');
INSERT INTO public.site_metadata (xid, key, value) VALUES (61, 'site.text_post.max_length', '65535');


--
-- Data for Name: sub; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_ban; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_flair; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_message_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_message_mailbox; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_metadata; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_mod; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment_checkoff; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment_report; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment_view; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_comment_vote; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_content_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_metadata; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_poll_option; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_poll_vote; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_report; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_title_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_view; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_post_vote; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_rule; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_subscriber; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_uploads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_user_flair; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: sub_user_flair_choice; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: url_metadata; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: url_metadata_log; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_content_block; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_login; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_message_block; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_message_mailbox; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_metadata; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_saved; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_unread_message; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: user_uploads; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: username_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: visit; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: wiki; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.wiki (id, is_global, sid, slug, title, content, created, updated) VALUES (1, true, NULL, 'tos', 'Terms of Service', 'Put your terms of service here', '2024-03-03 14:38:10.61001', '2024-03-03 14:38:10.61001');
INSERT INTO public.wiki (id, is_global, sid, slug, title, content, created, updated) VALUES (2, true, NULL, 'privacy', 'Privacy Policy', 'Put your privacy policy here', '2024-03-03 14:38:10.61001', '2024-03-03 14:38:10.61001');
INSERT INTO public.wiki (id, is_global, sid, slug, title, content, created, updated) VALUES (3, true, NULL, 'canary', 'Warrant canary', 'Put your canary here', '2024-03-03 14:38:10.61001', '2024-03-03 14:38:10.61001');
INSERT INTO public.wiki (id, is_global, sid, slug, title, content, created, updated) VALUES (4, true, NULL, 'donate', 'Donate', 'Donation info goes here!', '2024-03-03 14:38:10.61001', '2024-03-03 14:38:10.61001');


--
-- Name: api_token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.api_token_id_seq', 1, false);


--
-- Name: api_token_settings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.api_token_settings_id_seq', 1, false);


--
-- Name: badge_bid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.badge_bid_seq', 1, false);


--
-- Name: comment_report_log_lid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.comment_report_log_lid_seq', 1, false);


--
-- Name: customer_update_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.customer_update_log_id_seq', 1, false);


--
-- Name: disallow_flair_post_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.disallow_flair_post_type_id_seq', 1, false);


--
-- Name: invite_code_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.invite_code_id_seq', 1, false);


--
-- Name: message_mid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.message_mid_seq', 1, false);


--
-- Name: message_thread_mtid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.message_thread_mtid_seq', 1, false);


--
-- Name: notification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.notification_id_seq', 1, false);


--
-- Name: post_report_log_lid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.post_report_log_lid_seq', 1, false);


--
-- Name: post_type_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.post_type_config_id_seq', 1, false);


--
-- Name: required_name_change_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.required_name_change_id_seq', 1, false);


--
-- Name: site_log_lid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.site_log_lid_seq', 1, false);


--
-- Name: site_metadata_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.site_metadata_xid_seq', 61, true);


--
-- Name: sub_ban_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_ban_id_seq', 1, false);


--
-- Name: sub_flair_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_flair_xid_seq', 1, false);


--
-- Name: sub_log_lid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_log_lid_seq', 1, false);


--
-- Name: sub_message_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_message_log_id_seq', 1, false);


--
-- Name: sub_message_mailbox_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_message_mailbox_id_seq', 1, false);


--
-- Name: sub_metadata_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_metadata_xid_seq', 1, false);


--
-- Name: sub_mod_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_mod_id_seq', 1, false);


--
-- Name: sub_post_comment_checkoff_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_comment_checkoff_id_seq', 1, false);


--
-- Name: sub_post_comment_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_comment_history_id_seq', 1, false);


--
-- Name: sub_post_comment_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_comment_report_id_seq', 1, false);


--
-- Name: sub_post_comment_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_comment_view_id_seq', 1, false);


--
-- Name: sub_post_comment_vote_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_comment_vote_xid_seq', 1, false);


--
-- Name: sub_post_content_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_content_history_id_seq', 1, false);


--
-- Name: sub_post_metadata_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_metadata_xid_seq', 1, false);


--
-- Name: sub_post_pid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_pid_seq', 1, false);


--
-- Name: sub_post_poll_option_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_poll_option_id_seq', 1, false);


--
-- Name: sub_post_poll_vote_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_poll_vote_id_seq', 1, false);


--
-- Name: sub_post_report_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_report_id_seq', 1, false);


--
-- Name: sub_post_title_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_title_history_id_seq', 1, false);


--
-- Name: sub_post_view_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_view_id_seq', 1, false);


--
-- Name: sub_post_vote_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_post_vote_xid_seq', 1, false);


--
-- Name: sub_rule_rid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_rule_rid_seq', 1, false);


--
-- Name: sub_subscriber_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_subscriber_xid_seq', 1, false);


--
-- Name: sub_uploads_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_uploads_id_seq', 1, false);


--
-- Name: sub_user_flair_choice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_user_flair_choice_id_seq', 1, false);


--
-- Name: sub_user_flair_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.sub_user_flair_id_seq', 1, false);


--
-- Name: url_metadata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.url_metadata_id_seq', 1, false);


--
-- Name: url_metadata_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.url_metadata_log_id_seq', 1, false);


--
-- Name: user_content_block_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_content_block_id_seq', 1, false);


--
-- Name: user_login_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_login_id_seq', 1, false);


--
-- Name: user_message_block_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_message_block_id_seq', 1, false);


--
-- Name: user_message_mailbox_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_message_mailbox_id_seq', 1, false);


--
-- Name: user_metadata_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_metadata_xid_seq', 1, false);


--
-- Name: user_saved_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_saved_xid_seq', 1, false);


--
-- Name: user_unread_message_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_unread_message_id_seq', 1, false);


--
-- Name: user_uploads_xid_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.user_uploads_xid_seq', 1, false);


--
-- Name: username_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.username_history_id_seq', 1, false);


--
-- Name: visit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.visit_id_seq', 1, false);


--
-- Name: wiki_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.wiki_id_seq', 4, true);


--
-- Name: archived_job archived_job_pkey; Type: CONSTRAINT; Schema: proletarian; Owner: -
--

ALTER TABLE ONLY proletarian.archived_job
    ADD CONSTRAINT archived_job_pkey PRIMARY KEY (job_id);


--
-- Name: job job_pkey; Type: CONSTRAINT; Schema: proletarian; Owner: -
--

ALTER TABLE ONLY proletarian.job
    ADD CONSTRAINT job_pkey PRIMARY KEY (job_id);


--
-- Name: api_token api_token_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token
    ADD CONSTRAINT api_token_pkey PRIMARY KEY (id);


--
-- Name: api_token_settings api_token_settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token_settings
    ADD CONSTRAINT api_token_settings_pkey PRIMARY KEY (id);


--
-- Name: badge badge_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.badge
    ADD CONSTRAINT badge_pkey PRIMARY KEY (bid);


--
-- Name: comment_report_log comment_report_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment_report_log
    ADD CONSTRAINT comment_report_log_pkey PRIMARY KEY (lid);


--
-- Name: customer_update_log customer_update_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_update_log
    ADD CONSTRAINT customer_update_log_pkey PRIMARY KEY (id);


--
-- Name: daily_uniques daily_uniques_date_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.daily_uniques
    ADD CONSTRAINT daily_uniques_date_key UNIQUE (date);


--
-- Name: disallow_flair_post_type disallow_flair_post_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.disallow_flair_post_type
    ADD CONSTRAINT disallow_flair_post_type_pkey PRIMARY KEY (id);


--
-- Name: invite_code invite_code_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invite_code
    ADD CONSTRAINT invite_code_pkey PRIMARY KEY (id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (mid);


--
-- Name: message_thread message_thread_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message_thread
    ADD CONSTRAINT message_thread_pkey PRIMARY KEY (mtid);


--
-- Name: notification notification_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (payment_id);


--
-- Name: post_report_log post_report_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_report_log
    ADD CONSTRAINT post_report_log_pkey PRIMARY KEY (lid);


--
-- Name: post_type_config post_type_config_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_type_config
    ADD CONSTRAINT post_type_config_pkey PRIMARY KEY (id);


--
-- Name: required_name_change required_name_change_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.required_name_change
    ADD CONSTRAINT required_name_change_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: site_log site_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_log
    ADD CONSTRAINT site_log_pkey PRIMARY KEY (lid);


--
-- Name: site_metadata site_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_metadata
    ADD CONSTRAINT site_metadata_pkey PRIMARY KEY (xid);


--
-- Name: sub_ban sub_ban_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_ban
    ADD CONSTRAINT sub_ban_pkey PRIMARY KEY (id);


--
-- Name: sub_flair sub_flair_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_flair
    ADD CONSTRAINT sub_flair_pkey PRIMARY KEY (xid);


--
-- Name: sub_log sub_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_log
    ADD CONSTRAINT sub_log_pkey PRIMARY KEY (lid);


--
-- Name: sub_message_log sub_message_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_log
    ADD CONSTRAINT sub_message_log_pkey PRIMARY KEY (id);


--
-- Name: sub_message_mailbox sub_message_mailbox_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_mailbox
    ADD CONSTRAINT sub_message_mailbox_pkey PRIMARY KEY (id);


--
-- Name: sub_metadata sub_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_metadata
    ADD CONSTRAINT sub_metadata_pkey PRIMARY KEY (xid);


--
-- Name: sub_mod sub_mod_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_mod
    ADD CONSTRAINT sub_mod_pkey PRIMARY KEY (id);


--
-- Name: sub sub_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub
    ADD CONSTRAINT sub_pkey PRIMARY KEY (sid);


--
-- Name: sub_post_comment_checkoff sub_post_comment_checkoff_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_checkoff
    ADD CONSTRAINT sub_post_comment_checkoff_pkey PRIMARY KEY (id);


--
-- Name: sub_post_comment_history sub_post_comment_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_history
    ADD CONSTRAINT sub_post_comment_history_pkey PRIMARY KEY (id);


--
-- Name: sub_post_comment sub_post_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment
    ADD CONSTRAINT sub_post_comment_pkey PRIMARY KEY (cid);


--
-- Name: sub_post_comment_report sub_post_comment_report_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_report
    ADD CONSTRAINT sub_post_comment_report_pkey PRIMARY KEY (id);


--
-- Name: sub_post_comment_view sub_post_comment_view_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_view
    ADD CONSTRAINT sub_post_comment_view_pkey PRIMARY KEY (id);


--
-- Name: sub_post_comment_vote sub_post_comment_vote_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_vote
    ADD CONSTRAINT sub_post_comment_vote_pkey PRIMARY KEY (xid);


--
-- Name: sub_post_content_history sub_post_content_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_content_history
    ADD CONSTRAINT sub_post_content_history_pkey PRIMARY KEY (id);


--
-- Name: sub_post_metadata sub_post_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_metadata
    ADD CONSTRAINT sub_post_metadata_pkey PRIMARY KEY (xid);


--
-- Name: sub_post sub_post_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post
    ADD CONSTRAINT sub_post_pkey PRIMARY KEY (pid);


--
-- Name: sub_post_poll_option sub_post_poll_option_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_option
    ADD CONSTRAINT sub_post_poll_option_pkey PRIMARY KEY (id);


--
-- Name: sub_post_poll_vote sub_post_poll_vote_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_vote
    ADD CONSTRAINT sub_post_poll_vote_pkey PRIMARY KEY (id);


--
-- Name: sub_post_report sub_post_report_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_report
    ADD CONSTRAINT sub_post_report_pkey PRIMARY KEY (id);


--
-- Name: sub_post_title_history sub_post_title_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_title_history
    ADD CONSTRAINT sub_post_title_history_pkey PRIMARY KEY (id);


--
-- Name: sub_post_view sub_post_view_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_view
    ADD CONSTRAINT sub_post_view_pkey PRIMARY KEY (id);


--
-- Name: sub_post_vote sub_post_vote_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_vote
    ADD CONSTRAINT sub_post_vote_pkey PRIMARY KEY (xid);


--
-- Name: sub_rule sub_rule_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_rule
    ADD CONSTRAINT sub_rule_pkey PRIMARY KEY (rid);


--
-- Name: sub_subscriber sub_subscriber_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_subscriber
    ADD CONSTRAINT sub_subscriber_pkey PRIMARY KEY (xid);


--
-- Name: sub_uploads sub_uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_uploads
    ADD CONSTRAINT sub_uploads_pkey PRIMARY KEY (id);


--
-- Name: sub_user_flair_choice sub_user_flair_choice_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair_choice
    ADD CONSTRAINT sub_user_flair_choice_pkey PRIMARY KEY (id);


--
-- Name: sub_user_flair sub_user_flair_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair
    ADD CONSTRAINT sub_user_flair_pkey PRIMARY KEY (id);


--
-- Name: url_metadata_log url_metadata_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.url_metadata_log
    ADD CONSTRAINT url_metadata_log_pkey PRIMARY KEY (id);


--
-- Name: url_metadata url_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.url_metadata
    ADD CONSTRAINT url_metadata_pkey PRIMARY KEY (id);


--
-- Name: user_content_block user_content_block_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_content_block
    ADD CONSTRAINT user_content_block_pkey PRIMARY KEY (id);


--
-- Name: user_login user_login_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_login
    ADD CONSTRAINT user_login_pkey PRIMARY KEY (id);


--
-- Name: user_message_block user_message_block_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_block
    ADD CONSTRAINT user_message_block_pkey PRIMARY KEY (id);


--
-- Name: user_message_mailbox user_message_mailbox_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_mailbox
    ADD CONSTRAINT user_message_mailbox_pkey PRIMARY KEY (id);


--
-- Name: user_metadata user_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_metadata
    ADD CONSTRAINT user_metadata_pkey PRIMARY KEY (xid);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (uid);


--
-- Name: user_saved user_saved_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_saved
    ADD CONSTRAINT user_saved_pkey PRIMARY KEY (xid);


--
-- Name: user_unread_message user_unread_message_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_unread_message
    ADD CONSTRAINT user_unread_message_pkey PRIMARY KEY (id);


--
-- Name: user_uploads user_uploads_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_uploads
    ADD CONSTRAINT user_uploads_pkey PRIMARY KEY (xid);


--
-- Name: username_history username_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.username_history
    ADD CONSTRAINT username_history_pkey PRIMARY KEY (id);


--
-- Name: visit visit_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visit
    ADD CONSTRAINT visit_pkey PRIMARY KEY (id);


--
-- Name: wiki wiki_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki
    ADD CONSTRAINT wiki_pkey PRIMARY KEY (id);


--
-- Name: job_queue_process_at; Type: INDEX; Schema: proletarian; Owner: -
--

CREATE INDEX job_queue_process_at ON proletarian.job USING btree (queue, process_at);


--
-- Name: apitoken_token; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX apitoken_token ON public.api_token USING btree (token);


--
-- Name: apitoken_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX apitoken_uid ON public.api_token USING btree (uid);


--
-- Name: apitokensettings_token_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX apitokensettings_token_id ON public.api_token_settings USING btree (token_id);


--
-- Name: badge_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX badge_name ON public.badge USING btree (name);


--
-- Name: commentreportlog_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX commentreportlog_id ON public.comment_report_log USING btree (id);


--
-- Name: commentreportlog_target_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX commentreportlog_target_uid ON public.comment_report_log USING btree (target_uid);


--
-- Name: commentreportlog_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX commentreportlog_uid ON public.comment_report_log USING btree (uid);


--
-- Name: customerupdatelog_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX customerupdatelog_uid ON public.customer_update_log USING btree (uid);


--
-- Name: disallowflairposttype_flairid_ptype; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX disallowflairposttype_flairid_ptype ON public.disallow_flair_post_type USING btree (flair_id, ptype);


--
-- Name: invite_code_code; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invite_code_code ON public.invite_code USING btree (code);


--
-- Name: invitecode_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX invitecode_uid ON public.invite_code USING btree (uid);


--
-- Name: message_mtid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_mtid ON public.message USING btree (mtid);


--
-- Name: message_posted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_posted ON public.message USING btree (posted);


--
-- Name: message_receivedby; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_receivedby ON public.message USING btree (receivedby);


--
-- Name: message_sentby; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX message_sentby ON public.message USING btree (sentby);


--
-- Name: messagethread_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX messagethread_sid ON public.message_thread USING btree (sid);


--
-- Name: notification_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX notification_cid ON public.notification USING btree (cid);


--
-- Name: notification_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX notification_pid ON public.notification USING btree (pid);


--
-- Name: notification_receivedby; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX notification_receivedby ON public.notification USING btree (receivedby);


--
-- Name: notification_sentby; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX notification_sentby ON public.notification USING btree (sentby);


--
-- Name: notification_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX notification_sid ON public.notification USING btree (sid);


--
-- Name: payments_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX payments_uid ON public.payments USING btree (uid);


--
-- Name: posttypeconfig_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX posttypeconfig_sid ON public.post_type_config USING btree (sid);


--
-- Name: posttypeconfig_sid_ptype; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX posttypeconfig_sid_ptype ON public.post_type_config USING btree (sid, ptype);


--
-- Name: requirednamechange_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX requirednamechange_uid ON public.required_name_change USING btree (uid);


--
-- Name: sitelog_target_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sitelog_target_uid ON public.site_log USING btree (target_uid);


--
-- Name: sitelog_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sitelog_uid ON public.site_log USING btree (uid);


--
-- Name: sub_message_log_mtid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_message_log_mtid ON public.sub_message_log USING btree (mtid);


--
-- Name: sub_message_log_updated; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_message_log_updated ON public.sub_message_log USING btree (updated);


--
-- Name: sub_message_mailbox_mtid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_message_mailbox_mtid ON public.sub_message_mailbox USING btree (mtid);


--
-- Name: sub_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX sub_name ON public.sub USING btree (name);


--
-- Name: sub_name_lower; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX sub_name_lower ON public.sub USING btree (lower((name)::text));


--
-- Name: sub_post_comment_best_score; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_comment_best_score ON public.sub_post_comment USING btree (public.best_score(upvotes, downvotes, views));


--
-- Name: sub_post_comment_report_datetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_comment_report_datetime ON public.sub_post_comment_report USING btree (datetime);


--
-- Name: sub_post_comment_time; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_comment_time ON public.sub_post_comment USING btree ("time");


--
-- Name: sub_post_comment_vote_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_comment_vote_cid ON public.sub_post_comment_vote USING btree (cid);


--
-- Name: sub_post_comment_vote_cid_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX sub_post_comment_vote_cid_uid ON public.sub_post_comment_vote USING btree (cid, uid);


--
-- Name: sub_post_comment_vote_datetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_comment_vote_datetime ON public.sub_post_comment_vote USING btree (datetime);


--
-- Name: sub_post_link; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_link ON public.sub_post USING btree (link);


--
-- Name: sub_post_posted; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_posted ON public.sub_post USING btree (posted);


--
-- Name: sub_post_report_datetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_report_datetime ON public.sub_post_report USING btree (datetime);


--
-- Name: sub_post_title_history_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_title_history_uid ON public.sub_post_title_history USING btree (uid);


--
-- Name: sub_post_vote_datetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sub_post_vote_datetime ON public.sub_post_vote USING btree (datetime);


--
-- Name: sub_post_vote_pid_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX sub_post_vote_pid_uid ON public.sub_post_vote USING btree (pid, uid);


--
-- Name: subban_created_by_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subban_created_by_id ON public.sub_ban USING btree (created_by_id);


--
-- Name: subban_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subban_sid ON public.sub_ban USING btree (sid);


--
-- Name: subban_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subban_uid ON public.sub_ban USING btree (uid);


--
-- Name: subflair_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subflair_sid ON public.sub_flair USING btree (sid);


--
-- Name: sublog_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sublog_sid ON public.sub_log USING btree (sid);


--
-- Name: sublog_target_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sublog_target_uid ON public.sub_log USING btree (target_uid);


--
-- Name: sublog_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sublog_uid ON public.sub_log USING btree (uid);


--
-- Name: submessagelog_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX submessagelog_uid ON public.sub_message_log USING btree (uid);


--
-- Name: submetadata_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX submetadata_sid ON public.sub_metadata USING btree (sid);


--
-- Name: submod_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX submod_sid ON public.sub_mod USING btree (sid);


--
-- Name: submod_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX submod_uid ON public.sub_mod USING btree (uid);


--
-- Name: subpost_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpost_sid ON public.sub_post USING btree (sid);


--
-- Name: subpost_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpost_uid ON public.sub_post USING btree (uid);


--
-- Name: subpostcomment_parentcid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcomment_parentcid ON public.sub_post_comment USING btree (parentcid);


--
-- Name: subpostcomment_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcomment_pid ON public.sub_post_comment USING btree (pid);


--
-- Name: subpostcomment_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcomment_uid ON public.sub_post_comment USING btree (uid);


--
-- Name: subpostcommentcheckoff_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX subpostcommentcheckoff_cid ON public.sub_post_comment_checkoff USING btree (cid);


--
-- Name: subpostcommentcheckoff_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentcheckoff_uid ON public.sub_post_comment_checkoff USING btree (uid);


--
-- Name: subpostcommenthistory_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommenthistory_cid ON public.sub_post_comment_history USING btree (cid);


--
-- Name: subpostcommentreport_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentreport_cid ON public.sub_post_comment_report USING btree (cid);


--
-- Name: subpostcommentreport_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentreport_uid ON public.sub_post_comment_report USING btree (uid);


--
-- Name: subpostcommentview_cid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentview_cid ON public.sub_post_comment_view USING btree (cid);


--
-- Name: subpostcommentview_cid_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX subpostcommentview_cid_uid ON public.sub_post_comment_view USING btree (cid, uid);


--
-- Name: subpostcommentview_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentview_pid ON public.sub_post_comment_view USING btree (pid);


--
-- Name: subpostcommentview_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentview_uid ON public.sub_post_comment_view USING btree (uid);


--
-- Name: subpostcommentvote_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcommentvote_uid ON public.sub_post_comment_vote USING btree (uid);


--
-- Name: subpostcontenthistory_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostcontenthistory_pid ON public.sub_post_content_history USING btree (pid);


--
-- Name: subpostmetadata_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostmetadata_pid ON public.sub_post_metadata USING btree (pid);


--
-- Name: subpostpolloption_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostpolloption_pid ON public.sub_post_poll_option USING btree (pid);


--
-- Name: subpostpollvote_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostpollvote_pid ON public.sub_post_poll_vote USING btree (pid);


--
-- Name: subpostpollvote_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostpollvote_uid ON public.sub_post_poll_vote USING btree (uid);


--
-- Name: subpostpollvote_vid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostpollvote_vid ON public.sub_post_poll_vote USING btree (vid);


--
-- Name: subpostreport_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostreport_pid ON public.sub_post_report USING btree (pid);


--
-- Name: subpostreport_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostreport_uid ON public.sub_post_report USING btree (uid);


--
-- Name: subposttitlehistory_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subposttitlehistory_pid ON public.sub_post_title_history USING btree (pid);


--
-- Name: subpostview_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostview_pid ON public.sub_post_view USING btree (pid);


--
-- Name: subpostview_pid_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX subpostview_pid_uid ON public.sub_post_view USING btree (pid, uid);


--
-- Name: subpostview_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostview_uid ON public.sub_post_view USING btree (uid);


--
-- Name: subpostvote_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostvote_pid ON public.sub_post_vote USING btree (pid);


--
-- Name: subpostvote_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subpostvote_uid ON public.sub_post_vote USING btree (uid);


--
-- Name: subrule_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subrule_sid ON public.sub_rule USING btree (sid);


--
-- Name: subsubscriber_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsubscriber_sid ON public.sub_subscriber USING btree (sid);


--
-- Name: subsubscriber_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subsubscriber_uid ON public.sub_subscriber USING btree (uid);


--
-- Name: subuploads_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subuploads_sid ON public.sub_uploads USING btree (sid);


--
-- Name: subuserflair_flair_choice_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subuserflair_flair_choice_id ON public.sub_user_flair USING btree (flair_choice_id);


--
-- Name: subuserflair_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subuserflair_sid ON public.sub_user_flair USING btree (sid);


--
-- Name: subuserflair_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subuserflair_uid ON public.sub_user_flair USING btree (uid);


--
-- Name: subuserflairchoice_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX subuserflairchoice_sid ON public.sub_user_flair_choice USING btree (sid);


--
-- Name: url_metadata_log_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX url_metadata_log_id ON public.url_metadata USING btree (log_id);


--
-- Name: url_metadata_log_url; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX url_metadata_log_url ON public.url_metadata_log USING btree (url);


--
-- Name: url_metadata_value; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX url_metadata_value ON public.url_metadata USING btree (value);


--
-- Name: user_content_block_target; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_content_block_target ON public.user_content_block USING btree (target);


--
-- Name: user_message_block_target; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX user_message_block_target ON public.user_message_block USING btree (target);


--
-- Name: user_name; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_name ON public."user" USING btree (name);


--
-- Name: user_name_lower; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX user_name_lower ON public."user" USING btree (lower((name)::text));


--
-- Name: usercontentblock_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usercontentblock_uid ON public.user_content_block USING btree (uid);


--
-- Name: userlogin_code_newemail; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userlogin_code_newemail ON public.user_login USING btree ((((pending_action -> 'NewEmail'::text) ->> 'code'::text)));


--
-- Name: userlogin_code_reset; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userlogin_code_reset ON public.user_login USING btree ((((pending_action -> 'Reset'::text) ->> 'code'::text)));


--
-- Name: userlogin_code_verify; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userlogin_code_verify ON public.user_login USING btree ((((pending_action -> 'Verify'::text) ->> 'code'::text)));


--
-- Name: userlogin_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userlogin_uid ON public.user_login USING btree (uid);


--
-- Name: usermessageblock_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usermessageblock_uid ON public.user_message_block USING btree (uid);


--
-- Name: usermessagemailbox_mid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usermessagemailbox_mid ON public.user_message_mailbox USING btree (mid);


--
-- Name: usermessagemailbox_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usermessagemailbox_uid ON public.user_message_mailbox USING btree (uid);


--
-- Name: usermetadata_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usermetadata_uid ON public.user_metadata USING btree (uid);


--
-- Name: usernamehistory_name; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usernamehistory_name ON public.username_history USING btree (name);


--
-- Name: usernamehistory_name_lower; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usernamehistory_name_lower ON public.username_history USING btree (lower(name));


--
-- Name: usernamehistory_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usernamehistory_uid ON public.username_history USING btree (uid);


--
-- Name: usersaved_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usersaved_uid ON public.user_saved USING btree (uid);


--
-- Name: userunreadmessage_mid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userunreadmessage_mid ON public.user_unread_message USING btree (mid);


--
-- Name: userunreadmessage_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userunreadmessage_uid ON public.user_unread_message USING btree (uid);


--
-- Name: useruploads_pid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX useruploads_pid ON public.user_uploads USING btree (pid);


--
-- Name: useruploads_uid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX useruploads_uid ON public.user_uploads USING btree (uid);


--
-- Name: visit_datetime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX visit_datetime ON public.visit USING btree (datetime);


--
-- Name: wiki_sid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX wiki_sid ON public.wiki USING btree (sid);


--
-- Name: wiki_sid_slug; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX wiki_sid_slug ON public.wiki USING btree (sid, slug);


--
-- Name: api_token_settings api_token_settings_token_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token_settings
    ADD CONSTRAINT api_token_settings_token_id_fkey FOREIGN KEY (token_id) REFERENCES public.api_token(id);


--
-- Name: api_token api_token_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.api_token
    ADD CONSTRAINT api_token_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: comment_report_log comment_report_log_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment_report_log
    ADD CONSTRAINT comment_report_log_id_fkey FOREIGN KEY (id) REFERENCES public.sub_post_comment_report(id);


--
-- Name: comment_report_log comment_report_log_target_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment_report_log
    ADD CONSTRAINT comment_report_log_target_uid_fkey FOREIGN KEY (target_uid) REFERENCES public."user"(uid);


--
-- Name: comment_report_log comment_report_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.comment_report_log
    ADD CONSTRAINT comment_report_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: customer_update_log customer_update_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.customer_update_log
    ADD CONSTRAINT customer_update_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: disallow_flair_post_type disallow_flair_post_type_flair_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.disallow_flair_post_type
    ADD CONSTRAINT disallow_flair_post_type_flair_id_fkey FOREIGN KEY (flair_id) REFERENCES public.sub_flair(xid);


--
-- Name: invite_code invite_code_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.invite_code
    ADD CONSTRAINT invite_code_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: message message_mtid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_mtid_fkey FOREIGN KEY (mtid) REFERENCES public.message_thread(mtid);


--
-- Name: message message_receivedby_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_receivedby_fkey FOREIGN KEY (receivedby) REFERENCES public."user"(uid);


--
-- Name: message message_sentby_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_sentby_fkey FOREIGN KEY (sentby) REFERENCES public."user"(uid);


--
-- Name: message_thread message_thread_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.message_thread
    ADD CONSTRAINT message_thread_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: notification notification_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_cid_fkey FOREIGN KEY (cid) REFERENCES public.sub_post_comment(cid);


--
-- Name: notification notification_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: notification notification_receivedby_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_receivedby_fkey FOREIGN KEY (receivedby) REFERENCES public."user"(uid);


--
-- Name: notification notification_sentby_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_sentby_fkey FOREIGN KEY (sentby) REFERENCES public."user"(uid);


--
-- Name: notification notification_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT notification_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: payments payments_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: post_report_log post_report_log_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_report_log
    ADD CONSTRAINT post_report_log_id_fkey FOREIGN KEY (id) REFERENCES public.sub_post_report(id);


--
-- Name: post_report_log post_report_log_target_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_report_log
    ADD CONSTRAINT post_report_log_target_uid_fkey FOREIGN KEY (target_uid) REFERENCES public."user"(uid);


--
-- Name: post_report_log post_report_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_report_log
    ADD CONSTRAINT post_report_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: post_type_config post_type_config_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.post_type_config
    ADD CONSTRAINT post_type_config_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: required_name_change required_name_change_admin_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.required_name_change
    ADD CONSTRAINT required_name_change_admin_uid_fkey FOREIGN KEY (admin_uid) REFERENCES public."user"(uid);


--
-- Name: required_name_change required_name_change_notification_mtid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.required_name_change
    ADD CONSTRAINT required_name_change_notification_mtid_fkey FOREIGN KEY (notification_mtid) REFERENCES public.message_thread(mtid);


--
-- Name: required_name_change required_name_change_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.required_name_change
    ADD CONSTRAINT required_name_change_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: site_log site_log_target_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_log
    ADD CONSTRAINT site_log_target_uid_fkey FOREIGN KEY (target_uid) REFERENCES public."user"(uid);


--
-- Name: site_log site_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.site_log
    ADD CONSTRAINT site_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_ban sub_ban_created_by_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_ban
    ADD CONSTRAINT sub_ban_created_by_id_fkey FOREIGN KEY (created_by_id) REFERENCES public."user"(uid);


--
-- Name: sub_ban sub_ban_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_ban
    ADD CONSTRAINT sub_ban_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_ban sub_ban_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_ban
    ADD CONSTRAINT sub_ban_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_flair sub_flair_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_flair
    ADD CONSTRAINT sub_flair_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_log sub_log_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_log
    ADD CONSTRAINT sub_log_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_log sub_log_target_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_log
    ADD CONSTRAINT sub_log_target_uid_fkey FOREIGN KEY (target_uid) REFERENCES public."user"(uid);


--
-- Name: sub_log sub_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_log
    ADD CONSTRAINT sub_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_message_log sub_message_log_mtid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_log
    ADD CONSTRAINT sub_message_log_mtid_fkey FOREIGN KEY (mtid) REFERENCES public.message_thread(mtid);


--
-- Name: sub_message_log sub_message_log_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_log
    ADD CONSTRAINT sub_message_log_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_message_mailbox sub_message_mailbox_mtid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_message_mailbox
    ADD CONSTRAINT sub_message_mailbox_mtid_fkey FOREIGN KEY (mtid) REFERENCES public.message_thread(mtid);


--
-- Name: sub_metadata sub_metadata_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_metadata
    ADD CONSTRAINT sub_metadata_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_mod sub_mod_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_mod
    ADD CONSTRAINT sub_mod_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_mod sub_mod_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_mod
    ADD CONSTRAINT sub_mod_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_comment_checkoff sub_post_comment_checkoff_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_checkoff
    ADD CONSTRAINT sub_post_comment_checkoff_cid_fkey FOREIGN KEY (cid) REFERENCES public.sub_post_comment(cid);


--
-- Name: sub_post_comment_checkoff sub_post_comment_checkoff_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_checkoff
    ADD CONSTRAINT sub_post_comment_checkoff_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_comment_history sub_post_comment_history_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_history
    ADD CONSTRAINT sub_post_comment_history_cid_fkey FOREIGN KEY (cid) REFERENCES public.sub_post_comment(cid);


--
-- Name: sub_post_comment sub_post_comment_parentcid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment
    ADD CONSTRAINT sub_post_comment_parentcid_fkey FOREIGN KEY (parentcid) REFERENCES public.sub_post_comment(cid);


--
-- Name: sub_post_comment sub_post_comment_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment
    ADD CONSTRAINT sub_post_comment_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_comment_report sub_post_comment_report_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_report
    ADD CONSTRAINT sub_post_comment_report_cid_fkey FOREIGN KEY (cid) REFERENCES public.sub_post_comment(cid);


--
-- Name: sub_post_comment_report sub_post_comment_report_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_report
    ADD CONSTRAINT sub_post_comment_report_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_comment sub_post_comment_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment
    ADD CONSTRAINT sub_post_comment_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_comment_view sub_post_comment_view_cid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_view
    ADD CONSTRAINT sub_post_comment_view_cid_fkey FOREIGN KEY (cid) REFERENCES public.sub_post_comment(cid);


--
-- Name: sub_post_comment_view sub_post_comment_view_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_view
    ADD CONSTRAINT sub_post_comment_view_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_comment_view sub_post_comment_view_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_view
    ADD CONSTRAINT sub_post_comment_view_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_comment_vote sub_post_comment_vote_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_comment_vote
    ADD CONSTRAINT sub_post_comment_vote_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_content_history sub_post_content_history_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_content_history
    ADD CONSTRAINT sub_post_content_history_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_metadata sub_post_metadata_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_metadata
    ADD CONSTRAINT sub_post_metadata_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_poll_option sub_post_poll_option_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_option
    ADD CONSTRAINT sub_post_poll_option_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_poll_vote sub_post_poll_vote_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_vote
    ADD CONSTRAINT sub_post_poll_vote_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_poll_vote sub_post_poll_vote_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_vote
    ADD CONSTRAINT sub_post_poll_vote_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_poll_vote sub_post_poll_vote_vid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_poll_vote
    ADD CONSTRAINT sub_post_poll_vote_vid_fkey FOREIGN KEY (vid) REFERENCES public.sub_post_poll_option(id);


--
-- Name: sub_post_report sub_post_report_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_report
    ADD CONSTRAINT sub_post_report_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_report sub_post_report_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_report
    ADD CONSTRAINT sub_post_report_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post sub_post_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post
    ADD CONSTRAINT sub_post_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_post_title_history sub_post_title_history_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_title_history
    ADD CONSTRAINT sub_post_title_history_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_title_history sub_post_title_history_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_title_history
    ADD CONSTRAINT sub_post_title_history_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post sub_post_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post
    ADD CONSTRAINT sub_post_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_view sub_post_view_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_view
    ADD CONSTRAINT sub_post_view_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_view sub_post_view_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_view
    ADD CONSTRAINT sub_post_view_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_post_vote sub_post_vote_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_vote
    ADD CONSTRAINT sub_post_vote_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: sub_post_vote sub_post_vote_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_post_vote
    ADD CONSTRAINT sub_post_vote_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_rule sub_rule_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_rule
    ADD CONSTRAINT sub_rule_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_subscriber sub_subscriber_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_subscriber
    ADD CONSTRAINT sub_subscriber_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_subscriber sub_subscriber_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_subscriber
    ADD CONSTRAINT sub_subscriber_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: sub_uploads sub_uploads_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_uploads
    ADD CONSTRAINT sub_uploads_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_user_flair_choice sub_user_flair_choice_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair_choice
    ADD CONSTRAINT sub_user_flair_choice_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_user_flair sub_user_flair_flair_choice_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair
    ADD CONSTRAINT sub_user_flair_flair_choice_id_fkey FOREIGN KEY (flair_choice_id) REFERENCES public.sub_user_flair_choice(id);


--
-- Name: sub_user_flair sub_user_flair_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair
    ADD CONSTRAINT sub_user_flair_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: sub_user_flair sub_user_flair_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sub_user_flair
    ADD CONSTRAINT sub_user_flair_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: url_metadata url_metadata_log_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.url_metadata
    ADD CONSTRAINT url_metadata_log_id_fkey FOREIGN KEY (log_id) REFERENCES public.url_metadata_log(id);


--
-- Name: user_content_block user_content_block_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_content_block
    ADD CONSTRAINT user_content_block_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_login user_login_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_login
    ADD CONSTRAINT user_login_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_message_block user_message_block_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_block
    ADD CONSTRAINT user_message_block_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_message_mailbox user_message_mailbox_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_mailbox
    ADD CONSTRAINT user_message_mailbox_mid_fkey FOREIGN KEY (mid) REFERENCES public.message(mid);


--
-- Name: user_message_mailbox user_message_mailbox_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_message_mailbox
    ADD CONSTRAINT user_message_mailbox_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_metadata user_metadata_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_metadata
    ADD CONSTRAINT user_metadata_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_saved user_saved_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_saved
    ADD CONSTRAINT user_saved_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_unread_message user_unread_message_mid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_unread_message
    ADD CONSTRAINT user_unread_message_mid_fkey FOREIGN KEY (mid) REFERENCES public.message(mid);


--
-- Name: user_unread_message user_unread_message_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_unread_message
    ADD CONSTRAINT user_unread_message_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: user_uploads user_uploads_pid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_uploads
    ADD CONSTRAINT user_uploads_pid_fkey FOREIGN KEY (pid) REFERENCES public.sub_post(pid);


--
-- Name: user_uploads user_uploads_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_uploads
    ADD CONSTRAINT user_uploads_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: username_history username_history_required_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.username_history
    ADD CONSTRAINT username_history_required_id_fkey FOREIGN KEY (required_id) REFERENCES public.required_name_change(id);


--
-- Name: username_history username_history_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.username_history
    ADD CONSTRAINT username_history_uid_fkey FOREIGN KEY (uid) REFERENCES public."user"(uid);


--
-- Name: wiki wiki_sid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.wiki
    ADD CONSTRAINT wiki_sid_fkey FOREIGN KEY (sid) REFERENCES public.sub(sid);


--
-- Name: SCHEMA proletarian; Type: ACL; Schema: -; Owner: -
--

GRANT USAGE ON SCHEMA proletarian TO throat_be;


--
-- Name: TABLE archived_job; Type: ACL; Schema: proletarian; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE proletarian.archived_job TO throat_be;


--
-- Name: TABLE job; Type: ACL; Schema: proletarian; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE proletarian.job TO throat_be;


--
-- Name: TABLE api_token; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.api_token TO throat_py;


--
-- Name: SEQUENCE api_token_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.api_token_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_id_seq TO ovarit_subs;


--
-- Name: TABLE api_token_settings; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.api_token_settings TO throat_py;


--
-- Name: SEQUENCE api_token_settings_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.api_token_settings_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_settings_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_settings_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.api_token_settings_id_seq TO ovarit_subs;


--
-- Name: TABLE badge; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.badge TO ovarit_subs;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.badge TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.badge TO throat_py;


--
-- Name: SEQUENCE badge_bid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.badge_bid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.badge_bid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.badge_bid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.badge_bid_seq TO ovarit_subs;


--
-- Name: TABLE comment_report_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.comment_report_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.comment_report_log TO throat_py;


--
-- Name: SEQUENCE comment_report_log_lid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.comment_report_log_lid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.comment_report_log_lid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.comment_report_log_lid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.comment_report_log_lid_seq TO ovarit_subs;


--
-- Name: TABLE customer_update_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.customer_update_log TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.customer_update_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.customer_update_log TO throat_py;


--
-- Name: SEQUENCE customer_update_log_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.customer_update_log_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.customer_update_log_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.customer_update_log_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.customer_update_log_id_seq TO ovarit_subs;


--
-- Name: TABLE daily_uniques; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.daily_uniques TO throat_be;


--
-- Name: TABLE disallow_flair_post_type; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.disallow_flair_post_type TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.disallow_flair_post_type TO throat_py;


--
-- Name: SEQUENCE disallow_flair_post_type_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.disallow_flair_post_type_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.disallow_flair_post_type_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.disallow_flair_post_type_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.disallow_flair_post_type_id_seq TO ovarit_subs;


--
-- Name: TABLE invite_code; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.invite_code TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.invite_code TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.invite_code TO throat_py;


--
-- Name: SEQUENCE invite_code_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.invite_code_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.invite_code_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.invite_code_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.invite_code_id_seq TO ovarit_subs;


--
-- Name: TABLE message; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.message TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.message TO throat_py;


--
-- Name: SEQUENCE message_mid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.message_mid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.message_mid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.message_mid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.message_mid_seq TO ovarit_subs;


--
-- Name: TABLE message_thread; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.message_thread TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.message_thread TO throat_py;


--
-- Name: SEQUENCE message_thread_mtid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.message_thread_mtid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.message_thread_mtid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.message_thread_mtid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.message_thread_mtid_seq TO ovarit_subs;


--
-- Name: TABLE notification; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.notification TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.notification TO throat_py;


--
-- Name: SEQUENCE notification_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.notification_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.notification_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.notification_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.notification_id_seq TO ovarit_subs;


--
-- Name: TABLE payments; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.payments TO ovarit_subs;


--
-- Name: TABLE post_report_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.post_report_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.post_report_log TO throat_py;


--
-- Name: SEQUENCE post_report_log_lid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.post_report_log_lid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.post_report_log_lid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.post_report_log_lid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.post_report_log_lid_seq TO ovarit_subs;


--
-- Name: TABLE post_type_config; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.post_type_config TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.post_type_config TO throat_py;


--
-- Name: SEQUENCE post_type_config_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.post_type_config_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.post_type_config_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.post_type_config_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.post_type_config_id_seq TO ovarit_subs;


--
-- Name: TABLE required_name_change; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.required_name_change TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.required_name_change TO ovarit_auth;


--
-- Name: SEQUENCE required_name_change_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.required_name_change_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.required_name_change_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.required_name_change_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.required_name_change_id_seq TO ovarit_subs;


--
-- Name: TABLE site_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.site_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.site_log TO throat_py;


--
-- Name: SEQUENCE site_log_lid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.site_log_lid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.site_log_lid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.site_log_lid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.site_log_lid_seq TO ovarit_subs;


--
-- Name: TABLE site_metadata; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.site_metadata TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.site_metadata TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.site_metadata TO throat_py;


--
-- Name: SEQUENCE site_metadata_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.site_metadata_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.site_metadata_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.site_metadata_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.site_metadata_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub TO throat_py;


--
-- Name: TABLE sub_ban; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_ban TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_ban TO throat_py;


--
-- Name: SEQUENCE sub_ban_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_ban_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_ban_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_ban_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_ban_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_flair; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_flair TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_flair TO throat_py;


--
-- Name: SEQUENCE sub_flair_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_flair_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_flair_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_flair_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_flair_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_log TO throat_py;


--
-- Name: SEQUENCE sub_log_lid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_log_lid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_log_lid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_log_lid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_log_lid_seq TO ovarit_subs;


--
-- Name: TABLE sub_message_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_message_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_message_log TO throat_py;


--
-- Name: SEQUENCE sub_message_log_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_message_log_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_log_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_log_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_log_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_message_mailbox; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_message_mailbox TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_message_mailbox TO throat_py;


--
-- Name: SEQUENCE sub_message_mailbox_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_message_mailbox_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_mailbox_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_mailbox_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_message_mailbox_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_metadata; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_metadata TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_metadata TO throat_py;


--
-- Name: SEQUENCE sub_metadata_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_metadata_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_metadata_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_metadata_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_metadata_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub_mod; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_mod TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_mod TO throat_py;


--
-- Name: SEQUENCE sub_mod_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_mod_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_mod_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_mod_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_mod_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post TO throat_py;


--
-- Name: TABLE sub_post_comment; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment TO throat_py;


--
-- Name: TABLE sub_post_comment_checkoff; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_checkoff TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_checkoff TO throat_py;


--
-- Name: SEQUENCE sub_post_comment_checkoff_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_checkoff_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_checkoff_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_checkoff_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_checkoff_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_comment_history; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_history TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_history TO throat_py;


--
-- Name: SEQUENCE sub_post_comment_history_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_history_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_history_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_history_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_history_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_comment_report; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_report TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_report TO throat_py;


--
-- Name: SEQUENCE sub_post_comment_report_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_report_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_report_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_report_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_report_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_comment_view; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_view TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_view TO throat_py;


--
-- Name: SEQUENCE sub_post_comment_view_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_view_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_view_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_view_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_view_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_comment_vote; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_vote TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_comment_vote TO throat_py;


--
-- Name: SEQUENCE sub_post_comment_vote_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_vote_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_vote_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_vote_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_comment_vote_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_content_history; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_content_history TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_content_history TO throat_py;


--
-- Name: SEQUENCE sub_post_content_history_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_content_history_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_content_history_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_content_history_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_content_history_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_metadata; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_metadata TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_metadata TO throat_py;


--
-- Name: SEQUENCE sub_post_metadata_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_metadata_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_metadata_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_metadata_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_metadata_xid_seq TO ovarit_subs;


--
-- Name: SEQUENCE sub_post_pid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_pid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_pid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_pid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_pid_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_poll_option; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_poll_option TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_poll_option TO throat_py;


--
-- Name: SEQUENCE sub_post_poll_option_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_option_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_option_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_option_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_option_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_poll_vote; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_poll_vote TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_poll_vote TO throat_py;


--
-- Name: SEQUENCE sub_post_poll_vote_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_vote_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_vote_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_vote_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_poll_vote_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_report; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_report TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_report TO throat_py;


--
-- Name: SEQUENCE sub_post_report_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_report_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_report_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_report_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_report_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_title_history; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_title_history TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_title_history TO throat_py;


--
-- Name: SEQUENCE sub_post_title_history_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_title_history_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_title_history_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_title_history_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_title_history_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_view; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_view TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_view TO throat_py;


--
-- Name: SEQUENCE sub_post_view_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_view_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_view_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_view_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_view_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_post_vote; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_vote TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_post_vote TO throat_py;


--
-- Name: SEQUENCE sub_post_vote_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_post_vote_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_vote_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_vote_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_post_vote_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub_rule; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_rule TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_rule TO throat_py;


--
-- Name: SEQUENCE sub_rule_rid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_rule_rid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_rule_rid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_rule_rid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_rule_rid_seq TO ovarit_subs;


--
-- Name: TABLE sub_subscriber; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_subscriber TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_subscriber TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_subscriber TO throat_py;


--
-- Name: SEQUENCE sub_subscriber_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_subscriber_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_subscriber_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_subscriber_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_subscriber_xid_seq TO ovarit_subs;


--
-- Name: TABLE sub_uploads; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_uploads TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_uploads TO throat_py;


--
-- Name: SEQUENCE sub_uploads_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_uploads_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_uploads_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_uploads_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_uploads_id_seq TO ovarit_subs;


--
-- Name: TABLE sub_user_flair; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_user_flair TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_user_flair TO throat_py;


--
-- Name: TABLE sub_user_flair_choice; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_user_flair_choice TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.sub_user_flair_choice TO throat_py;


--
-- Name: SEQUENCE sub_user_flair_choice_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_choice_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_choice_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_choice_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_choice_id_seq TO ovarit_subs;


--
-- Name: SEQUENCE sub_user_flair_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.sub_user_flair_id_seq TO ovarit_subs;


--
-- Name: TABLE url_metadata; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.url_metadata TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.url_metadata TO throat_py;


--
-- Name: SEQUENCE url_metadata_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_id_seq TO ovarit_subs;


--
-- Name: TABLE url_metadata_log; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.url_metadata_log TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.url_metadata_log TO throat_py;


--
-- Name: SEQUENCE url_metadata_log_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_log_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_log_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_log_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.url_metadata_log_id_seq TO ovarit_subs;


--
-- Name: TABLE "user"; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public."user" TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public."user" TO ovarit_subs;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public."user" TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public."user" TO throat_py;


--
-- Name: TABLE user_content_block; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_content_block TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_content_block TO throat_py;


--
-- Name: SEQUENCE user_content_block_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_content_block_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_content_block_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_content_block_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_content_block_id_seq TO ovarit_subs;


--
-- Name: TABLE user_login; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_login TO ovarit_auth;


--
-- Name: SEQUENCE user_login_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_login_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_login_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_login_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_login_id_seq TO ovarit_subs;


--
-- Name: TABLE user_message_block; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_message_block TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_message_block TO throat_py;


--
-- Name: SEQUENCE user_message_block_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_message_block_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_block_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_block_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_block_id_seq TO ovarit_subs;


--
-- Name: TABLE user_message_mailbox; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_message_mailbox TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_message_mailbox TO throat_py;


--
-- Name: SEQUENCE user_message_mailbox_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_message_mailbox_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_mailbox_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_mailbox_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_message_mailbox_id_seq TO ovarit_subs;


--
-- Name: TABLE user_metadata; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_metadata TO ovarit_auth;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_metadata TO ovarit_subs;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_metadata TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_metadata TO throat_py;


--
-- Name: SEQUENCE user_metadata_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_metadata_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_metadata_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_metadata_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_metadata_xid_seq TO ovarit_subs;


--
-- Name: TABLE user_saved; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_saved TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_saved TO throat_py;


--
-- Name: SEQUENCE user_saved_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_saved_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_saved_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_saved_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_saved_xid_seq TO ovarit_subs;


--
-- Name: TABLE user_unread_message; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_unread_message TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_unread_message TO throat_py;


--
-- Name: SEQUENCE user_unread_message_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_unread_message_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_unread_message_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_unread_message_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_unread_message_id_seq TO ovarit_subs;


--
-- Name: TABLE user_uploads; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_uploads TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.user_uploads TO throat_py;


--
-- Name: SEQUENCE user_uploads_xid_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.user_uploads_xid_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.user_uploads_xid_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.user_uploads_xid_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.user_uploads_xid_seq TO ovarit_subs;


--
-- Name: TABLE username_history; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.username_history TO ovarit_auth;
GRANT SELECT ON TABLE public.username_history TO throat_be;
GRANT SELECT ON TABLE public.username_history TO throat_py;


--
-- Name: SEQUENCE username_history_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.username_history_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.username_history_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.username_history_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.username_history_id_seq TO ovarit_subs;


--
-- Name: TABLE visit; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.visit TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.visit TO throat_py;


--
-- Name: SEQUENCE visit_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.visit_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.visit_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.visit_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.visit_id_seq TO ovarit_subs;


--
-- Name: TABLE wiki; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.wiki TO throat_be;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE public.wiki TO throat_py;


--
-- Name: SEQUENCE wiki_id_seq; Type: ACL; Schema: public; Owner: -
--

GRANT SELECT,USAGE ON SEQUENCE public.wiki_id_seq TO throat_py;
GRANT SELECT,USAGE ON SEQUENCE public.wiki_id_seq TO throat_be;
GRANT SELECT,USAGE ON SEQUENCE public.wiki_id_seq TO ovarit_auth;
GRANT SELECT,USAGE ON SEQUENCE public.wiki_id_seq TO ovarit_subs;


--
-- Name: DEFAULT PRIVILEGES FOR SEQUENCES; Type: DEFAULT ACL; Schema: public; Owner: -
--

ALTER DEFAULT PRIVILEGES FOR ROLE throat IN SCHEMA public GRANT SELECT,USAGE ON SEQUENCES  TO throat_py;
ALTER DEFAULT PRIVILEGES FOR ROLE throat IN SCHEMA public GRANT SELECT,USAGE ON SEQUENCES  TO throat_be;
ALTER DEFAULT PRIVILEGES FOR ROLE throat IN SCHEMA public GRANT SELECT,USAGE ON SEQUENCES  TO ovarit_auth;
ALTER DEFAULT PRIVILEGES FOR ROLE throat IN SCHEMA public GRANT SELECT,USAGE ON SEQUENCES  TO ovarit_subs;


--
-- PostgreSQL database dump complete
--

