-- :name select-links :? :*
-- :doc Get links that don't have metadata fetches
select distinct(link) as url
  from sub_post
 where ptype = 1
   and not exists (select xid
                     from user_uploads
                    where sub_post.pid = user_uploads.pid)
   and not exists (select id
                     from url_metadata_log
                    where url = sub_post.link)
 limit :lim;
